## KALAHI NCCDP

README FILE I will write it later after some debugging



##DEPLOYING THE SYSTEM TO THE LOCALHOST FOR DEVELOPMENT

Our current process is to put it inside the htdocs folder instead of creating your own vhost in your local environment..
i havent tested it in the current configuration of the kalahi repo.

##RUNNING USING XAMPP

after deploying you may run it
http://localhost/kalahi

##DRIVE
the full package is located in our google drive folder just request an access to Ms Karen Caday


##ADDING NEW MODULES

when writing a new route module add it to 
Authentication Route Group in route.php 

All route inside the Route Group will be checked by an auth filter


##Repository Design Pattern

since 09/27/2014
I add a Repository Design Pattern for cleanier code
inside kalahi/app/Repositories ..

If want to create your own repository Class and Interfaces
register it inside the app/binds.php ..
for example
                Interface                       Class
App::bind('Repositories\GRS\GRSInterface', 'Repositories\GRS\GRSRepository');