<?php
	include "queries.php";	
	if(!empty($_POST['table'])){
		
	
	
		$data = queries($_POST['table']);

		if($_POST['type']=="json"){
			$json = json_encode($data,JSON_FORCE_OBJECT);

			header('Content-disposition: attachment; filename=file.json');
			header('Content-type: application/json');
			echo $json;
		}
		if($_POST['type']=="csv"){
				$fp = fopen('sample.csv', 'w');
				

				foreach ($data as $tablename => $list) {
					echo "<pre>";
					
					foreach ($list as $fields) {
					    fputcsv($fp, $fields);
					}
					fclose($fp);
					$file = 'sample.csv'; //path to the file on disk

				    if (file_exists($file)) {

				        //set appropriate headers
				        header('Content-Description: File Transfer');
				        header('Content-Type: application/csv');
				        header('Content-Disposition: attachment; filename='.basename($tablename));
				        header('Expires: 0');
				        header('Cache-Control: must-revalidate');
				        header('Pragma: public');
				        header('Content-Length: ' . filesize($file));
				        ob_clean();
				        flush();

				        //read the file from disk and output the content.
				        readfile($file);
				        exit;
				    }
				}
		}
	}else{
		header('Content-disposition: attachment; filename=file.json');
			header('Content-type: application/json');
			echo 'no table selected';
	}
?>	
