<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>	
     <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
 		<title>kalahi ncddp</title>
 		<link href="/kalahi_ncddp/public/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/kalahi_ncddp/public/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="/kalahi_ncddp/public/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="/kalahi_ncddp/public/css/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="/kalahi_ncddp/public/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="/kalahi_ncddp/public/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="/kalahi_ncddp/public/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="/kalahi_ncddp/public/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="/kalahi_ncddp/public/css/AdminLTE.css" rel="stylesheet" type="text/css" />

     <link href="/kalahi_ncddp/public/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="/kalahi_ncddp/public/css/custom.css" rel="stylesheet" type="text/css" />
    
    <link href="/kalahi_ncddp/public/css/custom/styles.css" rel="stylesheet" type="text/css" />




    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="/kalahi_ncddp/public/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="/kalahi_ncddp/public/js/bootstrap.min.js" type="text/javascript"></script>
  </head>
<body class="skin-black">
	  <div  id="wrapper" class="wrapper row-offcanvas row-offcanvas-left">
	  	<div class="navbar navbar-default navbar-cls-top role="navigation" style="margin-bottom: 0"">
	  		 <div class="navbar-header">

              <a href="/kalahi_ncddp" class="navbar-brand logo">KALAHI</a>
                <span class="hidden" id="url"></span>
	              <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	              </button>
	          </div>
	  	          <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav">
                  <li class="dropdown">
                        <a href="/kalahi_ncddp" id="themes">Go back to database <span class="fa fa-arrow-left"></span></a>
                   </li>
           	</ul>	     
	  	</div>

	  </div>
		
