<?php include "connect.php" ?>

<?php 
	$connect = connect();
	/**
	* Structure of array
	*    the base module or table is the index
	*    the submodules in the value array
	* example : $var = ["table"=> ["subtable","subtable2"] ]
	*/
	$tablenames = [
	"kc_bassembly" => ['kc_activityissues','kc_lguactivity','kc_sectorsrep','kc_sitiorep'],
	"kc_volunteers"=>['kc_beneficiary','kc_memotherorg','kc_volcommmem','kc_memothertrn','kc_lguactivity']
	];

?>
<?php include "header.php" ?>

<div class="content">
	
	<div class="row">
		<div class="col-md-12">
			
			<div class="box box-info">
				<div class=" box-header with-border">
					<h3 class="box-title">Export Table Data</h3>
					<div class="box-tools pull-right">
					</div>
				</div>
				<div class=" box-body">
					<div class="panel">
						<div class="panel-body">
							<div class="col-md-5">
								
									<table class="table table-bordered">
										<tr>
											<td>Export Date Range</td>
											<td> </td>
										</tr>
										<tr>
											<td>Number of table to be export</td>
											<td><span class="tablecount"></span></td>
										</tr>
										<tr>
											<td><button class="btn btn-primary exportjson">Export <span class="tablecount"></span> to JSON</button>
										</tr>
									</table>							
							</div>	

						</div>
					</div>
					<div class="table-responsive">
						<table class="table ">
							<thead>
								<tr>
									<td>Table Name</td>
									<td>Count</td>
									<td>Option
										(select all <input type="checkbox" class="selectall">)
									</td>
								</tr>
							</thead>
							<tbody class="tablebody">
								<form action="exportdata.php" name="table" class="export" method="POST">
									<input type="hidden" class="hidden type" name="type" value="">
								<?php foreach($tablenames as $parentTableName => $subtables): ?>
										<?php 
											$count = $connect->query("SELECT Count(*) as count FROM ".$parentTableName)->fetch(PDO::FETCH_ASSOC);;
										?>
										<tr class="maintable">
											<td><?php echo $parentTableName ?></td>
											<td><?php echo $count['count'] ?></td>
											<td>
												<input type="checkbox" data-tablename="<?php echo $parentTableName ?>" class="tablecheckbox p<?php echo $parentTableName ?>" name="table[<?php echo $parentTableName;?>]">
											</td>

										</tr>
									
										
										<?php foreach($subtables as $key => $subtableName): ?>
										
											<tr class="subtable" data-tablename="<?php echo $subtableName ?>">
												<td style="padding-left:30px"><?php echo $key+1 ?>. <?php echo $subtableName ?></td>
												<td></td>
												<td>
													<input type="checkbox" class="tablecheckbox <?php echo $parentTableName ?>" data-parent="<?php echo $parentTableName ?>" name="table[<?php echo $subtableName;?>]">
												</td>
											</tr>
										<?php endforeach; ?>
										
								<?php endforeach; ?>
								</form>
							
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script>
	$(document).ready(function(){
		$('.selectall').on('click',function(){
			if($('.selectall')[0].checked==true){
				$('.tablecheckbox').prop('checked',true);
			}else{
				$('.tablecheckbox').prop('checked',false);
			}

			var selectcount=0;
			$.each($('.tablecheckbox'),function(e,i){
				if(i.checked){
					selectcount++;
				}
			});
			$('.tablecount').text(selectcount);

		});
		$('.tablecheckbox').on('change',function(){
			
			var parent = $(this).data('tablename');
			
			if( $(this)[0].checked==true){
				$('.'+parent).prop('checked',true);
			}else{
				$('.'+parent).prop('checked',false);
			}

			if($(this).data('parent') != undefined) 
			{
				var parentname = $(this).data('parent')
				$('.p'+parentname).prop('checked',true);
			}

			var selectcount=0;
			$.each($('.tablecheckbox'),function(e,i){
				if(i.checked){
					selectcount++;
				}
			});
			$('.tablecount').text(selectcount);
		

		});

		$('.exportjson').on('click',function(){
			$('.type').val('json');
			$('.export').submit();
		});
		$('.exportcsv').on('click',function(){
			$('.type').val('csv');
			$('.export').submit();
		});
	});
</script>
<?php include "footer.php" ?>
