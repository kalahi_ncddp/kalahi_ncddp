<?php
	include "connect.php";	
	/**
		* This will query every table based on the filter set	
		*/
	function queries($form)
	{
		$connect = connect();
		$data = [];
		if(isset($form['kc_bassembly']))
		{
		
			$data['kc_bassembly'] = $connect->query("SELECT *
									 FROM kc_bassembly
									 left join rf_approved 
									 on rf_approved.activity_id=kc_bassembly.activity_id
									 where rf_approved.act_reviewed != '0000-00-00 00:00:00' ")->fetch(PDO::FETCH_ASSOC);
			//sub table of kc_bassembly
			if(!isset($form['kc_activityissues']))
			{
				$data['kc_activityissues'] = $connect->query("SELECT kc_activityissues.* FROM kc_activityissues
																left join kc_bassembly 
																on kc_activityissues.activity_id=kc_bassembly.activity_id
																left join rf_approved 
																on kc_bassembly.activity_id=rf_approved.activity_id
																where rf_approved.act_reviewed != '0000-00-00 00:00:00' ")->fetch(PDO::FETCH_ASSOC);
			}
			if(!isset($form['kc_lguactivity']))
			{
				$data['kc_lguactivity'] = $connect->query("SELECT kc_lguactivity.* FROM kc_lguactivity
																left join kc_bassembly 
																on kc_lguactivity.activity_id=kc_bassembly.activity_id
																left join rf_approved 
																on kc_bassembly.activity_id=rf_approved.activity_id
																where rf_approved.act_reviewed != '0000-00-00 00:00:00' ")->fetch(PDO::FETCH_ASSOC);
			}
			if(!isset($form['kc_sectorsrep']))
			{
				$data['kc_sectorsrep'] = $connect->query("SELECT kc_sectorsrep.* FROM kc_sectorsrep
																left join kc_bassembly 
																on kc_sectorsrep.activity_id=kc_bassembly.activity_id
																left join rf_approved 
																on kc_bassembly.activity_id=rf_approved.activity_id
																where rf_approved.act_reviewed != '0000-00-00 00:00:00' ")->fetch(PDO::FETCH_ASSOC);
			}
			if(!isset($form['kc_sitiorep']))
			{
				$data['kc_sitiorep'] = $connect->query("SELECT kc_sitiorep.* FROM kc_sitiorep
																left join kc_bassembly 
																on kc_sitiorep.activity_id=kc_bassembly.activity_id
																left join rf_approved 
																on kc_bassembly.activity_id=rf_approved.activity_id
																where rf_approved.act_reviewed != '0000-00-00 00:00:00' ")->fetch(PDO::FETCH_ASSOC);
			}
			 
		}

	    if(isset($form['kc_volunteers'])){
		
			$data['kc_volunteers'] = $connect->query("SELECT *
									 FROM kc_volunteers
									 left join rf_approved 
									 on rf_approved.activity_id=kc_volunteers.volunteer_id
									 where rf_approved.act_reviewed != '0000-00-00 00:00:00' ")->fetch(PDO::FETCH_ASSOC);
			//kc_beneficiary
			if(!isset($form['kc_beneficiary']))
			{
				$data['kc_beneficiary'] = $connect->query("SELECT kc_beneficiary.* FROM kc_beneficiary
																left join kc_volunteers 
																on kc_beneficiary.volunteer_id=kc_volunteers.volunteer_id
																left join rf_approved 
																on kc_volunteers.volunteer_id=rf_approved.activity_id
																where rf_approved.act_reviewed != '0000-00-00 00:00:00' ")->fetch(PDO::FETCH_ASSOC);
			}
				//kc_memotherorg
			if(!isset($form['kc_memotherorg']))
			{
				$data['kc_memotherorg'] = $connect->query("SELECT kc_memotherorg.* FROM kc_memotherorg
																left join kc_beneficiary 
																on kc_memotherorg.beneficiary_id=kc_beneficiary.beneficiary_id
																left join kc_volunteers 
																on kc_beneficiary.volunteer_id=kc_volunteers.volunteer_id
																left join rf_approved 
																on kc_volunteers.volunteer_id=rf_approved.activity_id
																where rf_approved.act_reviewed != '0000-00-00 00:00:00' ")->fetch(PDO::FETCH_ASSOC);
			}
				//kc_memotherorg
			if(!isset($form['kc_memothertrn']))
			{
				$data['kc_memothertrn'] = $connect->query("SELECT kc_memothertrn.* FROM kc_memothertrn
																left join kc_beneficiary 
																on kc_memothertrn.beneficiary_id=kc_beneficiary.beneficiary_id
																left join kc_volunteers 
																on kc_beneficiary.volunteer_id=kc_volunteers.volunteer_id
																left join rf_approved 
																on kc_volunteers.volunteer_id=rf_approved.activity_id
																where rf_approved.act_reviewed != '0000-00-00 00:00:00' ")->fetch(PDO::FETCH_ASSOC);
			}
				//kc_volcommmem
			if(!isset($form['kc_volcommmem']))
			{
				$data['kc_volcommmem'] = $connect->query("SELECT kc_volcommmem.* FROM kc_volcommmem
																left join kc_volunteers 
																on kc_volcommmem.volunteer_id=kc_volunteers.volunteer_id
																left join rf_approved 
																on kc_volunteers.volunteer_id=rf_approved.activity_id
																where rf_approved.act_reviewed != '0000-00-00 00:00:00' group by kc_volcommmem.committee,kc_volcommmem.position  ")->fetch(PDO::FETCH_ASSOC);
			}
			if(!isset($form['kc_lguactivity']))
			{
				$data['kc_lguactivity'] = $connect->query("SELECT kc_lguactivity.* FROM kc_lguactivity
																left join kc_volunteers 
																on kc_lguactivity.activity_id=kc_volunteers.volunteer_id
																left join rf_approved 
																on kc_volunteers.volunteer_id=rf_approved.activity_id
																where rf_approved.act_reviewed != '0000-00-00 00:00:00' ")->fetch(PDO::FETCH_ASSOC);
			}
		}
		return $data;
		
	}
