var grsController = (function($){

	var elements = {
		municipal : $('.municipal') ,
		barangay  : $('.barangay')  ,
		province  : $('.province')  ,
		region    : $('.region')    ,
		kc_class  : $('.kc_class')  ,
		program	  : $('.program')	,
		modal_barangay : $('#show-barangay'),
		modal_municipality : $('#show-municipality')
	}

	var grs = {

		getBarangayDataById : function(){
			//  get the current the jsoon data fromt he suser 
			var responseData;

			//jQuery elem
			var elem = $(this);
			$.get( 'rest-barangay' , { 'barangay_id' : elem.val() } ,function( response ){
				
				elements.municipal.val( response.municipality.municipality );				
				elements.province.val( response.province.province );
				elements.kc_class.val( response.municipality.kc_class );
				elements.region.val( response.province.region.region_name );

			});
			
		},
		getFundSource : function(){

			var responseData;

			var elem = $(this);

			// $.get('rest-fund',{'cycle_id':elem.val() } , function( response ) {
			// 	console.log(response);
			// 	elements.program.val( response.program );
			// });
		},
		getGenerateInstallationNumberBarangay : function(){
			var elem = $('.installation');
			elem.val('G'+'-'+Math.ceil(Math.random()*10000));
		},
		showBarangay : function(){
			var modal = elements.modal_barangay;
			var id = $(this).data('psgc');
            var cycle = $(this).data('cycle');
			modal.modal('show');
			modal.find('.modal-title').text('loading...');

			$.get('grs-barangay/'+id,{'cycle':cycle},function(response){
				$('.edit-url').attr('href','grs-barangay/'+response.psgc_id+'/' +response.cycle_id +'/edit');
				$('.delete-barangay').attr('href','grs-barangay/delete/'+response.psgc_id+'/' +response.cycle_id);
				modal.find('.modal-title').text('Full Details of '+response.reference_no);
				$.each(response,function(i,e){
					$('.barangay_list').find('.'+i).text(e);
				});
			});
		},
		showMunicipal : function(){
			var modal = elements.modal_municipality;
			var id = $(this).data('psgc');
            var cycle = $(this).data('cycle');
			modal.modal('show');
			modal.find('.modal-title').text('loading...');

			$.get('grs-municipality/'+id,{'cycle':cycle},function(response){
				$('.edit-url').attr('href','grs-municipality/'+response.psgc_id+'/' +response.cycle_id+'/edit');
				$('.delete-municipality').attr('href','grs-municipality/delete/'+response.psgc_id+'/' +response.cycle_id);
				modal.find('.modal-title').text('Full Details of '+response.reference_no);
				$.each(response,function(i,e){
					$('.barangay_list').find('.'+i).text( e );
				});
			});
		},
		deleteBarangay : function(){
			
			var notice = confirm('Are you sure to delete this');
			
			if(notice){
				var notice_ = confirm('This will delete the item permanently');
				if(notice_)
					return true;
				else
					return false
			} else {
				return false;
			}

		},
		deleteMunicipality : function(){
			
			var notice = confirm('Are you sure to delete this');
			
			if(notice){
				var notice_ = confirm('This will delete the item permanently');
				if(notice_)
					return true;
				else
					return false
			} else {
				return false;
			}

		},
		getFundSourceMuni : function(){

			var responseData;

			var elem = $(this);

			$.get('rest-fund',{'cycle_id':elem.val() } , function( response ) {
				elements.program.val( response.program );
			});
		}

	}

	return grs;

})(jQuery);


var processController = (function($){

    var process = {
        approved : function(){
            var c = confirm('Approve the selected documents?');
            var module = $('.module').text();
            if(c)
            {
                var arr = [];
                if($('.checkbox1').length==0)
                {

                }
                $('.checkbox1').each(function(i) {
                    if ( this.checked) {
                        arr[i] = $(this).val();
                    }

                });
                var url = $('#url').text()+'/approved';
                $.post(url,{ 'check':arr ,'module_type': module } , function(res) {
                    alert('Reviewed success');
                    window.location.reload();
                });
            }
            else
            {

            }
        }

    }
    return process;
})(jQuery);
// event handler in every dom in the page 
 
$('.barangay').change(grsController.getBarangayDataById);
$('.cycle').change( grsController.getFundSource );


$('.generate').on('click',grsController.getGenerateInstallationNumberBarangay );
$('.view').on('click',grsController.showBarangay );
$('.view-muni').on('click',grsController.showMunicipal );
$('.delete-barangay').on('click',grsController.deleteBarangay );
$('.delete-municipality').on('click',grsController.deleteMunicipality );

$('#approved').on('click',processController.approved);
// plugins misc
$('#datetimepicker4').datetimepicker({
            pick12HourFormat: false
        });
$('.date').datetimepicker({pick12HourFormat: false, pickTime:false});
function numberIt(str) {
    //number before the decimal point

    str.replace(/,/g , "");
    num = str.substring(0,str.length-2);
    //number after the decimal point
    dec = str.substring(str.length-2,str.length)
    //connect both parts while comma-ing the first half
    output = num + "." + dec;

    return output;
}
function number_format  (number, decimals, dec_point, thousands_sep) {

        number = number.replace(/,/g,"");
        number = (number + '').replace(/[^0-9+-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/B(?=(?:d{3})+(?!d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }
$('#selecctall').click(function(event) {  //on click
    if(this.checked) { // check select status
        $('.checkbox1').each(function() { //loop through each checkbox
            this.checked = true;  //select all checkboxes with class "checkbox1"
        });
    }else{
        $('.checkbox1').each(function() { //loop through each checkbox
            this.checked = false; //deselect all checkboxes with class "checkbox1"
        });
    }
});



