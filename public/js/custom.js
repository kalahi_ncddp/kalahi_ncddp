


/*=============================================================
    Authour URI: www.binarycart.com
    License: Commons Attribution 3.0

    http://creativecommons.org/licenses/by/3.0/

    100% To use For Personal And Commercial Use.
    IN EXCHANGE JUST GIVE US CREDITS AND TELL YOUR FRIENDS ABOUT US
   
    ========================================================  */


    "use strict";
    var mainApp = (function ($) {
       return {
        main_fun: function () {
                /*====================================
                METIS MENU 
                ======================================*/
               
                /*====================================
                  LOAD APPROPRIATE MENU BAR
               ======================================*/
                $(window).bind("load resize", function () {
                    if ($(this).width() < 768) {
                        $('div.sidebar-collapse').addClass('collapse')
                    } else {
                        $('div.sidebar-collapse').removeClass('collapse')
                    }
                });

                $('select').find('option[value=""]').prop('disabled',true);


            },

            initialization: function () {
                mainApp.main_fun();

            }

        };

    // Initializing ///
})(jQuery);

$(document).ready(function () {
    mainApp.main_fun();
    $('.download').on('click',function(){
        var con = confirm('This will export/download approved data by the AC');
        if(con){
            var con2  = confirm('Are you sure to this action?')
            if(con2){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    });
    $.fn.loading = function(prompt){
        
        if(prompt){
            var template = ' <div class="loading-img"></div>';
            this.append(template);
        }else{
            $('.loading-img').remove();
        }
    }
    // $.fn.

    $('button.btn-danger').on('click',function(e){
        var confirms = confirm('Are you sure that do you want to delete this data?');

        if(confirms){
            return true;
        }
        return false;
    });
 

    $('.unreview').on('click',function(){
        var id = $(this).data('id');
        var url = $(this).data('url');

        $('.untagconfirm').prop('href',url+'?activity_id='+id);
        $('#unreviewed').modal('show');
    });


});
