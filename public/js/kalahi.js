$(document).ready ( function(){

    $('.date-conducted').datepicker();



	$('.profile-table tbody tr').click(function() {
		//editProfile(this);
		
		//$('#profile-modal').modal('show');
    	});	

	$('#add-volunteer-profile').click(function() {
		
		addVolunteerProfile();
		$(location).attr('href','/kalahi/volunteer');
		//$('#add-profile-modal').modal('show');
    	});

	$('#add-profile-to-record').click(function() {
		
		//addProfile();
		
    	});

	$('#select-sector-rep').click(function(){
		$('#sector-rep').modal('show');
	
	});
	
	

	$("#generate-volunteer-id").click(function() {
		
    	});
	generateVolID();

	$('#bspmc').click(function(){
		var a = $( "#startdate" ).prop("disabled");
		if(a){
			$( "#startdate" ).prop("disabled",false);
			$( "#enddate" ).prop("disabled",false);
		}else{
			$( "#startdate" ).prop("disabled",true);
			$( "#enddate" ).prop("disabled",true);
		}

	});

});


function generateVolID(){
	var num = Math.floor((Math.random() * 99999) + 10000);

	var volNum = 'V-'+num+'-'+$('#volunteerno').val();
	$('#volunteerno').val(volNum);
}

/*function async_brgy(brgy_id){

	 $.ajax({
                    type: "POST",
                    url : "get_brgy_name",
                    data : {'brgy_id':brgy_id},
                    success : function(data){
                        $('#barangay').val(data);
                    }
                },"json");

}*/


var header = ["No.","Volunteer ID","KC Class","Cycle","Region","Province","Municipality","Surname","Firstname","Fund Source"];
function editProfile(element){
	var content = "";
	var subcontent = "";
	$(element).children('td').each(function(i) { 
		if(i>0){
			var new_id = header[i].replace(" ", "");
			subcontent += "<td>"+header[i]+"</td>";
			subcontent += "<td>" +'<input type="text" class="span2" id="'+new_id+'" value="'+$(this).html()+'"></td>';
		
			if(i%2==0){
				content += appendTR(subcontent);
				subcontent = "";
			}
		}
	});
	content = "<table class='table'>"+content+"</table>";
	//alert(content);
	$(".volunteer-profile").html(content);

	$("#delete-profile").click(function() {
		deleteProfile($('#VolunteerID').val());
		//alert();
    	});	

	$("#update-profile").click(function() {
		updateProfile();
    	});	

}


function addVolunteerProfile(){
	var profile = {};
	profile["firstname"] = $("#firstname").val(); 
	profile["lastname"] = $("#lastname").val(); 
	profile["middlename"] = $("#middlename").val(); 
	profile["birthday"] = $("#birthday").val(); 
	profile["age"] = $("#age").val(); 
	profile["sex"] = $("#sex").val(); 
	profile["status"] = $("#status").val(); 
	profile["children"] = $("#children").val(); 
	profile["education"] = $("#education").val(); 
	profile["category"] = $("#category").val(); 
	profile["occupation"] = $("#occupation").val(); 
	profile["education"] = $("#education").val(); 
	profile["contact"] = $("#contact").val(); 
	profile["address"] = $("#address").val(); 
	profile["committee"] = $("#committee").val(); 
	profile["position"] = $("#position").val(); 
	profile["vdate"] = $("#vdate").val(); 
	profile["volunteerid"] = $('#volunteerno').val(); 
	profile["ip"] = $('#ip').is(":checked");
	profile["bspmc"] = $('#bspmc').is(":checked")?1:0;
	profile["startdate"] = $("#startdate").val(); 
	profile["enddate"] = $("#enddate").val(); 
	profile["brgypsgc"] = $("#brgy").val();
	profile["sectorrep"] = $("input[name=sectorrep]:checked").val()
	//alert(JSON.stringify(profile));

	insertProfile(profile);
}


function insertProfile(profile){

	 $.ajax({
                    type: "POST",
                    url : "insert",
                    data : profile,
                    success : function(data){
                      // alert(JSON.stringify(data));
			//alert(JSON.stringify(data));
                    }
         },"json");

}

function appendTR(tbody){
	return "<tr>"+tbody+"</tr>";

}

function deleteProfile(volunteerID){
	//alert(volunteerID);
	location.reload();
	//AJAX for deleting profile
}

function updateProfile(){
	var profile = {};
	profile["volunteerID"] = $("#VolunteerID").val(); 
	profile["kcclass"] = $("#KCClass").val(); 
	profile["cycle"] = $("#Cycle").val(); 
	profile["region"] = $("#Region").val(); 
	profile["province"] = $("#Province").val(); 
	profile["municipality"] = $("#Municipality").val(); 
	//profile["brgyID"] = $("#BrgyID").val(); 
	profile["surname"] = $("#Surname").val(); 
	profile["firstname"] = $("#Firstname").val(); 
	profile["fundsource"] = $("#FundSource").val(); 
	//alert(JSON.stringify(profile));
	//AJAX for updating profile
}

function addProfile(){
	var profile = {};
	profile["addVolunteerID"] = $("#addVolunteerID").val(); 
	profile["addKcclass"] = $("#addKCClass").val(); 
	profile["addCycle"] = $("#addCycle").val(); 
	profile["addRegion"] = $("#addRegion").val(); 
	profile["addProvince"] = $("#addProvince").val(); 
	profile["addMunicipality"] = $("#addMunicipality").val(); 
	//profile["addBrgyID"] = $("#addBrgyID").val(); 
	profile["addSurname"] = $("#addSurname").val(); 
	profile["addFirstname"] = $("#addFirstname").val(); 
	profile["addFundsource"] = $("#addFundSource").val(); 
	alert(JSON.stringify(profile));
	//AJAX for updating profile
}



// kaala