DELIMITER //
CREATE TRIGGER `spi_actual_pow_items_tai` AFTER INSERT ON `spi_actual_pow_items`
 FOR EACH ROW BEGIN
    DECLARE new_noncumm_actl_physical, new_noncumm_actl_financial FLOAT DEFAULT 0;
    DECLARE v_physical, v_financial, new_cumm_actl_physical, new_cumm_actl_financial, t_percent FLOAT DEFAULT 0;
    DECLARE new_acc_report_id, cur_id VARCHAR(255);
    DECLARE v_month_num FLOAT;
    /* 
      1. update current summary date
    */    
    SELECT acc_report_id, month_num
    INTO new_acc_report_id, v_month_num
    FROM spi_planned_pow_items
    WHERE id = NEW.id;

    BLOCK2: 
    BEGIN
      DECLARE before_summary_date CURSOR FOR 
      SELECT spi_actual_pow_items.physical, spi_actual_pow_items.financial 
      FROM spi_actual_pow_items
      INNER JOIN spi_planned_pow_items
      ON 
        spi_planned_pow_items.id = spi_actual_pow_items.id
      WHERE 
        spi_planned_pow_items.acc_report_id = new_acc_report_id AND
        spi_planned_pow_items.month_num < v_month_num;

      OPEN before_summary_date;
      BEGIN
        DECLARE done FLOAT DEFAULT 0;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        before_summary_date_loop: LOOP
          FETCH before_summary_date INTO v_physical, v_financial;
          IF done THEN LEAVE before_summary_date_loop;
          END IF;
          SET new_cumm_actl_physical = new_cumm_actl_physical + v_physical;
          SET new_cumm_actl_financial = new_cumm_actl_financial + v_financial;
        END LOOP before_summary_date_loop;
      END;
      CLOSE before_summary_date;
    END BLOCK2;

    SELECT id, COALESCE(noncumm_actl_physical, 0), COALESCE(noncumm_actl_financial, 0)
    INTO cur_id, new_noncumm_actl_physical , new_noncumm_actl_financial
    FROM spi_summary_pow_items 
    WHERE 
      acc_report_id = new_acc_report_id AND
      month_num = v_month_num;

    SET new_noncumm_actl_physical  = new_noncumm_actl_physical  + NEW.physical;
    SET new_noncumm_actl_financial = new_noncumm_actl_financial + NEW.financial;
    SET new_cumm_actl_physical = new_cumm_actl_physical + new_noncumm_actl_physical;
    SET new_cumm_actl_financial = new_cumm_actl_financial + new_noncumm_actl_financial;
    UPDATE spi_summary_pow_items 
    SET 
      noncumm_actl_physical  = new_noncumm_actl_physical,
      noncumm_actl_financial = new_noncumm_actl_financial,
      cumm_actl_physical     = new_cumm_actl_physical,
      cumm_actl_financial    = new_cumm_actl_financial
    WHERE id = cur_id;

    /* 
      2. update all after summary entries
    */    
    SET new_cumm_actl_physical = 0;
    SET new_cumm_actl_financial = 0;
    BLOCK3: 
    BEGIN
      DECLARE now_id VARCHAR(255);
      DECLARE v_cumm_actl_physical, v_cumm_actl_financial FLOAT DEFAULT 0;
      DECLARE after_summary_date CURSOR FOR 
      SELECT id, cumm_actl_physical, cumm_actl_financial FROM spi_summary_pow_items 
      WHERE 
        acc_report_id = new_acc_report_id AND
        month_num > v_month_num;


      OPEN after_summary_date;
      BEGIN
        DECLARE done INT DEFAULT 0;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        after_summary_date_loop: LOOP
          FETCH after_summary_date INTO now_id, v_cumm_actl_physical, v_cumm_actl_financial;
          IF done THEN LEAVE after_summary_date_loop;
          END IF;
          INSERT INTO debug values(CONCAT(now_id, '-', v_cumm_actl_physical, '-', v_cumm_actl_financial));
          SET new_cumm_actl_physical  = v_cumm_actl_physical + NEW.physical;
          SET new_cumm_actl_financial = v_cumm_actl_financial + NEW.financial;
          UPDATE spi_summary_pow_items 
          SET 
            cumm_actl_physical  = new_cumm_actl_physical,
            cumm_actl_financial = new_cumm_actl_financial
          WHERE id = now_id;
        END LOOP after_summary_date_loop;
      END;
      CLOSE after_summary_date;
    END BLOCK3;

  BLOCK4: 
    BEGIN
    DECLARE v_percent FLOAT DEFAULT 0;
    DECLARE total_percentages_per_month CURSOR FOR 
      SELECT COALESCE(cumm_actl_physical,0)
      FROM spi_summary_pow_items 
      WHERE acc_report_id = new_acc_report_id
      ORDER BY month_num DESC;
    SET t_percent = 0;
    OPEN total_percentages_per_month;
    BEGIN
      DECLARE done INT DEFAULT 0;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
      total_percentages_per_month_loop: LOOP
        FETCH total_percentages_per_month INTO v_percent;
        IF done THEN LEAVE total_percentages_per_month_loop;
        END IF;
        SET t_percent  = t_percent + v_percent;
        END LOOP total_percentages_per_month_loop;
    END;
    CLOSE total_percentages_per_month;
    
    UPDATE spi_accomplishment_report
    SET
      accomplished = t_percent
    WHERE acc_report_id = new_acc_report_id;
  END BLOCK4;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `spi_actual_pow_items_tau`;
DELIMITER //
CREATE TRIGGER `spi_actual_pow_items_tau` AFTER UPDATE ON `spi_actual_pow_items`
 FOR EACH ROW BEGIN
    DECLARE new_noncumm_actl_physical, new_noncumm_actl_financial FLOAT DEFAULT 0;
    DECLARE v_physical, v_financial, new_cumm_actl_physical, new_cumm_actl_financial, t_percent FLOAT DEFAULT 0;
    DECLARE new_acc_report_id, cur_id VARCHAR(255);
    DECLARE v_month_num INT;
    /* 
      1. update current summary date
    */    
    SELECT acc_report_id, month_num
    INTO new_acc_report_id, v_month_num
    FROM spi_planned_pow_items
    WHERE id = NEW.id;

    BLOCK2: 
    BEGIN
      DECLARE before_summary_date CURSOR FOR 
      SELECT spi_actual_pow_items.physical, spi_actual_pow_items.financial 
      FROM spi_actual_pow_items
      INNER JOIN spi_planned_pow_items
      ON 
        spi_planned_pow_items.id = spi_actual_pow_items.id
      WHERE 
        spi_planned_pow_items.acc_report_id = new_acc_report_id AND
        spi_planned_pow_items.month_num < v_month_num;

      OPEN before_summary_date;
      BEGIN
        DECLARE done INT DEFAULT 0;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        before_summary_date_loop: LOOP
          FETCH before_summary_date INTO v_physical, v_financial;
          IF done THEN LEAVE before_summary_date_loop;
          END IF;
          SET new_cumm_actl_physical = new_cumm_actl_physical + v_physical;
          SET new_cumm_actl_financial = new_cumm_actl_financial + v_financial;
        END LOOP before_summary_date_loop;
      END;
      CLOSE before_summary_date;
    END BLOCK2;

    SELECT id, COALESCE(noncumm_actl_physical, 0), COALESCE(noncumm_actl_financial, 0)
    INTO cur_id, new_noncumm_actl_physical , new_noncumm_actl_financial
    FROM spi_summary_pow_items 
    WHERE 
      acc_report_id = new_acc_report_id AND
      month_num = v_month_num;

    SET new_noncumm_actl_physical  = GREATEST(new_noncumm_actl_physical  - OLD.physical, 0) +  NEW.physical;
    SET new_noncumm_actl_financial = GREATEST(new_noncumm_actl_financial - OLD.financial, 0) + NEW.financial;
    SET new_cumm_actl_physical = new_cumm_actl_physical + new_noncumm_actl_physical;
    SET new_cumm_actl_financial = new_cumm_actl_financial + new_noncumm_actl_financial;
    UPDATE spi_summary_pow_items 
    SET 
      noncumm_actl_physical  = new_noncumm_actl_physical,
      noncumm_actl_financial = new_noncumm_actl_financial,
      cumm_actl_physical     = new_cumm_actl_physical,
      cumm_actl_financial    = new_cumm_actl_financial
    WHERE id = cur_id;

    /* 
      2. update all after summary entries
    */    
    SET new_cumm_actl_physical = 0;
    SET new_cumm_actl_financial = 0;
    BLOCK3: 
    BEGIN
      DECLARE now_id VARCHAR(255);
      DECLARE v_cumm_actl_physical, v_cumm_actl_financial FLOAT DEFAULT 0;
      DECLARE after_summary_date CURSOR FOR 
      SELECT id, cumm_actl_physical, cumm_actl_financial FROM spi_summary_pow_items 
      WHERE 
        acc_report_id = new_acc_report_id AND
        month_num > v_month_num;


      OPEN after_summary_date;
      BEGIN
        DECLARE done INT DEFAULT 0;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        after_summary_date_loop: LOOP
          FETCH after_summary_date INTO now_id, v_cumm_actl_physical, v_cumm_actl_financial;
          IF done THEN LEAVE after_summary_date_loop;
          END IF;
          INSERT INTO debug values(CONCAT(now_id, '-', v_cumm_actl_physical, '-', v_cumm_actl_financial));
          SET new_cumm_actl_physical  = GREATEST(v_cumm_actl_physical - OLD.physical, 0) + NEW.physical;
          SET new_cumm_actl_financial = GREATEST(v_cumm_actl_financial - OLD.financial, 0) + NEW.financial;
          UPDATE spi_summary_pow_items 
          SET 
            cumm_actl_physical  = new_cumm_actl_physical,
            cumm_actl_financial = new_cumm_actl_financial
          WHERE id = now_id;
        END LOOP after_summary_date_loop;
      END;
      CLOSE after_summary_date;
    END BLOCK3;

    /* UPDATE TOTAL PERCENTAGE */
  BLOCK4: 
    BEGIN
    DECLARE v_percent FLOAT DEFAULT 0;
    DECLARE total_percentages_per_month CURSOR FOR 
      SELECT COALESCE(cumm_actl_physical,0)
      FROM spi_summary_pow_items 
      WHERE acc_report_id = new_acc_report_id
      ORDER BY month_num DESC;
    SET t_percent = 0;
    OPEN total_percentages_per_month;
    BEGIN
      DECLARE done INT DEFAULT 0;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
      total_percentages_per_month_loop: LOOP
        FETCH total_percentages_per_month INTO v_percent;
        IF done THEN LEAVE total_percentages_per_month_loop;
        END IF;
        SET t_percent  = t_percent + v_percent;
        END LOOP total_percentages_per_month_loop;
    END;
    CLOSE total_percentages_per_month;
    
    UPDATE spi_accomplishment_report
    SET
      accomplished = t_percent
    WHERE acc_report_id = new_acc_report_id;
  END BLOCK4;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `spi_planned_pow_items_tad`;
DELIMITER //
CREATE TRIGGER `spi_planned_pow_items_tad` AFTER DELETE ON `spi_planned_pow_items`
 FOR EACH ROW BEGIN
  DECLARE new_cumm_plnd_physical, new_cumm_plnd_financial FLOAT DEFAULT 0;
  DECLARE new_noncumm_plnd_physical, new_noncumm_plnd_financial FLOAT DEFAULT 0;
  DECLARE cur_id, now_id VARCHAR(255);
  DECLARE v_physical, v_financial, v_cumm_plnd_physical, v_cumm_plnd_financial FLOAT DEFAULT 0;
  DECLARE before_summary_date CURSOR FOR 
  SELECT physical, financial FROM spi_planned_pow_items 
  WHERE 
    acc_report_id = OLD.acc_report_id AND
    month_num < OLD.month_num;
  DECLARE after_summary_date CURSOR FOR 
    SELECT id, cumm_plnd_physical, cumm_plnd_financial FROM spi_summary_pow_items 
    WHERE 
      acc_report_id = OLD.acc_report_id AND
      month_num > OLD.month_num;
  /* 
    1. insert/update current summary date
  */    
  OPEN before_summary_date;
  BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    before_summary_date_loop: LOOP
      FETCH before_summary_date INTO v_physical, v_financial;
      IF done THEN LEAVE before_summary_date_loop;
      END IF;
      SET new_cumm_plnd_physical = new_cumm_plnd_physical + v_physical;
      SET new_cumm_plnd_financial = new_cumm_plnd_financial + v_financial;
    END LOOP before_summary_date_loop;
  END;
  CLOSE before_summary_date;

  SELECT id, noncumm_plnd_physical, noncumm_plnd_financial
  INTO cur_id, new_noncumm_plnd_physical , new_noncumm_plnd_financial
  FROM spi_summary_pow_items 
  WHERE 
    acc_report_id = OLD.acc_report_id AND
    month_num     = OLD.month_num;

  SET new_noncumm_plnd_physical  = new_noncumm_plnd_physical - OLD.physical;
  SET new_noncumm_plnd_financial = new_noncumm_plnd_financial - OLD.financial;
  SET new_cumm_plnd_physical     = new_cumm_plnd_physical + new_noncumm_plnd_physical;
  SET new_cumm_plnd_financial    = new_cumm_plnd_financial + new_noncumm_plnd_financial;

  UPDATE spi_summary_pow_items 
  SET 
    noncumm_plnd_physical = new_noncumm_plnd_physical,
    noncumm_plnd_financial = new_noncumm_plnd_financial,
    cumm_plnd_physical = new_cumm_plnd_physical,
    cumm_plnd_financial = new_cumm_plnd_financial
  WHERE id = cur_id;

    /* 
      2. update all after summary entries
    */    
    SET new_cumm_plnd_physical = 0;
    SET new_cumm_plnd_financial = 0;
    OPEN after_summary_date;
    BEGIN
      DECLARE done INT DEFAULT 0;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

      after_summary_date_loop: LOOP
        FETCH after_summary_date INTO now_id, v_cumm_plnd_physical, v_cumm_plnd_financial;
        IF done THEN LEAVE after_summary_date_loop;
        END IF;
        SET new_cumm_plnd_physical  = v_cumm_plnd_physical - OLD.physical;
        SET new_cumm_plnd_financial = v_cumm_plnd_financial - OLD.financial;
        UPDATE spi_summary_pow_items 
        SET 
          cumm_plnd_physical  = new_cumm_plnd_physical,
          cumm_plnd_financial = new_cumm_plnd_financial
        WHERE id = now_id;
      END LOOP after_summary_date_loop;
    END;
    CLOSE after_summary_date;

  /*
  3. DELETE summary date if no planned and actual
  */
  DELETE FROM spi_summary_pow_items 
  WHERE 
    acc_report_id = OLD.acc_report_id AND
    (noncumm_plnd_physical = 0 OR noncumm_plnd_physical IS NULL) AND
    (noncumm_plnd_financial = 0 OR noncumm_plnd_financial IS NULL) AND
    (noncumm_actl_physical = 0 OR noncumm_actl_physical IS NULL) AND
    (noncumm_actl_financial = 0 OR noncumm_actl_financial IS NULL);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `spi_planned_pow_items_tai`;

DELIMITER //
CREATE TRIGGER `spi_planned_pow_items_tai` AFTER INSERT ON `spi_planned_pow_items`
 FOR EACH ROW BEGIN
    DECLARE new_noncumm_plnd_physical, new_noncumm_plnd_financial, new_cumm_plnd_physical, new_cumm_plnd_financial FLOAT DEFAULT 0;
    DECLARE v_physical, v_financial, v_cumm_plnd_physical, v_cumm_plnd_financial FLOAT DEFAULT 0;
    DECLARE cur_id, v_remarks, now_id VARCHAR(255);
    DECLARE count INT DEFAULT 1;
    DECLARE before_summary_date CURSOR FOR 
    SELECT physical, financial FROM spi_planned_pow_items 
    WHERE 
      acc_report_id = NEW.acc_report_id AND
      month_num < NEW.month_num;
    DECLARE after_summary_date CURSOR FOR 
    SELECT id, cumm_plnd_physical, cumm_plnd_financial FROM spi_summary_pow_items 
    WHERE 
      acc_report_id = NEW.acc_report_id AND
      month_num > NEW.month_num;
    /* 
      1. insert/update current summary date
    */    
    OPEN before_summary_date;
    BEGIN
      DECLARE done INT DEFAULT 0;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

      before_summary_date_loop: LOOP
        FETCH before_summary_date INTO v_physical, v_financial;
        IF done THEN LEAVE before_summary_date_loop;
        END IF;
        SET new_cumm_plnd_physical = new_cumm_plnd_physical + v_physical;
        SET new_cumm_plnd_financial = new_cumm_plnd_financial + v_financial;
      END LOOP before_summary_date_loop;
    END;
    CLOSE before_summary_date;

    SELECT id, noncumm_plnd_physical, noncumm_plnd_financial
    INTO cur_id, new_noncumm_plnd_physical , new_noncumm_plnd_financial
    FROM spi_summary_pow_items 
    WHERE 
      acc_report_id = NEW.acc_report_id AND
      month_num = NEW.month_num;

    IF cur_id IS NULL THEN
      SET new_noncumm_plnd_physical = NEW.physical;
      SET new_noncumm_plnd_financial = NEW.financial;
      SET new_cumm_plnd_physical = new_cumm_plnd_physical + new_noncumm_plnd_physical;
      SET new_cumm_plnd_financial = new_cumm_plnd_financial + new_noncumm_plnd_financial;

      /* 1. */
      /* 2. */
      INSERT INTO spi_summary_pow_items(acc_report_id, month_num, noncumm_plnd_physical, noncumm_plnd_financial, cumm_plnd_physical, cumm_plnd_financial, remarks) 
      VALUES (NEW.acc_report_id, NEW.month_num, new_noncumm_plnd_physical, new_noncumm_plnd_financial, new_cumm_plnd_physical, new_cumm_plnd_financial, v_remarks);
    ELSE
      SET new_noncumm_plnd_physical = new_noncumm_plnd_physical + NEW.physical;
      SET new_noncumm_plnd_financial = new_noncumm_plnd_financial + NEW.financial;
      SET new_cumm_plnd_physical = new_cumm_plnd_physical + new_noncumm_plnd_physical;
      SET new_cumm_plnd_financial = new_cumm_plnd_financial + new_noncumm_plnd_financial;

      UPDATE spi_summary_pow_items 
      SET 
        noncumm_plnd_physical = new_noncumm_plnd_physical,
        noncumm_plnd_financial = new_noncumm_plnd_financial,
        cumm_plnd_physical = new_cumm_plnd_physical,
        cumm_plnd_financial = new_cumm_plnd_financial,
        remarks = v_remarks
      WHERE id = cur_id;
    END IF;

    /* 
      2. update all after summary entries
    */    
    SET new_cumm_plnd_physical = 0;
    SET new_cumm_plnd_financial = 0;
    OPEN after_summary_date;
    BEGIN
      DECLARE done INT DEFAULT 0;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

      select FOUND_ROWS() INTO count;
      INSERT INTO debug values(count);
      after_summary_date_loop: LOOP
        FETCH after_summary_date INTO now_id, v_cumm_plnd_physical, v_cumm_plnd_financial;
        IF done THEN LEAVE after_summary_date_loop;
        END IF;
        INSERT INTO debug values(CONCAT(now_id, '-', v_cumm_plnd_physical, '-', v_cumm_plnd_financial));
        SET new_cumm_plnd_physical  = v_cumm_plnd_physical + NEW.physical;
        SET new_cumm_plnd_financial = v_cumm_plnd_financial + NEW.financial;
        UPDATE spi_summary_pow_items 
        SET 
          cumm_plnd_physical  = new_cumm_plnd_physical,
          cumm_plnd_financial = new_cumm_plnd_financial,
          remarks = count
        WHERE id = now_id;
      END LOOP after_summary_date_loop;
    END;
    CLOSE after_summary_date;
END
//
DELIMITER ;