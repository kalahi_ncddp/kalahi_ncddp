TRUNCATE rf_natureofwork;
REPLACE INTO `rf_natureofwork` (`nature_of_work`, `type`) VALUES ('Foreman/Project Supervisor','skilled'),('Carpenter','skilled'),('Mason','skilled'),('Plumber','skilled'),('Electrician','skilled'),('Laborer/Helper','unskilled'),('Other Skilled','skilled'),('Other Unskilled','unskilled');
TRUNCATE rf_civilstatus;
REPLACE INTO `rf_civilstatus` (`name`) VALUES ('Annulled'),('Legally Separated'),('Married'),('Single'),('Widowed');
TRUNCATE rf_functionality_sp;
REPLACE INTO `rf_functionality_sp` (`functionality`) VALUES('Partly Damage'),('Heavily Damage'),('Condemnable');


TRUNCATE rf_complaints;

REPLACE INTO `rf_complaints` (`complaints`) VALUES('No comment'),('Not satisfied'),('Satisfied'),('Very Satisfied');