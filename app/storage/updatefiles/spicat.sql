TRUNCATE spi_category;


-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2015 at 06:35 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kc_ncddp`
--

--
-- Dumping data for table `spi_category`
--

INSERT INTO `spi_category` (`sub_category`, `category`, `unit`) VALUES
('Access Trail/Footpath', 'Basic Access Infrastructure', 'km.'),
('Community Transport', 'Community Production, Economic Support and Common Service Facility', 'unit'),
('Day Care Center', 'Basic Social Services', 'sq.m.'),
('Drainage Structures', 'Environmental Protection and Conservation', 'ln.m.'),
('Eco-Tourism', 'Lighthouse/Eco-Tourism', NULL),
('Economic/Livelihood Support', 'Community Production, Economic Support and Common Service Facility', 'sq.m.'),
('Electrification', 'Basic Social Services', 'HH'),
('Environmental Preservation', 'Environmental Protection and Conservation', 'unit'),
('Foot/Small Bridges', 'Basic Access Infrastructure', 'ln.m.'),
('GIG - GAD Related Training/Orientation', 'Gender Incentive Grant', NULL),
('GIG - Non Traditional Skills Training', 'Gender Incentive Grant', NULL),
('GIG - Traditional Skills Training', 'Gender Incentive Grant', NULL),
('GIG - Women Friendly Facilities', 'Gender Incentive Grant', NULL),
('Health Station', 'Basic Social Services', 'sq.m.'),
('Lighthouse', 'Lighthouse/Eco-Tourism', NULL),
('Multi-use/Purpose Building/Facility', 'Community Production, Economic Support and Common Service Facility', 'sq.m.'),
('Pre and Post Production Facility', 'Community Production, Economic Support and Common Service Facility', 'unit/sq.m.'),
('River Control and Flood Control', 'Environmental Protection and Conservation', 'ln.m.'),
('Road', 'Basic Access Infrastructure', 'km.'),
('Sanitation Facilities', 'Environmental Protection and Conservation', 'unit'),
('School', 'Basic Social Services', 'sq.m.'),
('SeaWall', 'Environmental Protection and Conservation', 'ln.m.'),
('Skills Training', 'Skills Training/Capability Building/Community Library', 'participants'),
('Small Scale Irrigation', 'Community Production, Economic Support and Common Service Facility', 'has.'),
('Soil/Slope Protection (Riprap)', 'Environmental Protection and Conservation', 'ln.m.'),
('Tribal Housing/Shelter', 'Basic Social Services', 'unit'),
('Water System', 'Basic Social Services', 'system');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
