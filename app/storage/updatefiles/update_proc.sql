DELETE FROM `spi_procmode` WHERE 1;
INSERT INTO `spi_procmode` (`mode_id`, `mode`) VALUES
('1', 'Community Shopping for Goods'),
('2', 'Community Shopping for Works'),
('3', 'Community Bidding for Works'),
('4', 'Community Bidding for Goods'),
('5', 'Community Direct Contracting'),
('6', 'Small Value Procurement'),
('7', 'Repeat Order');



DELETE FROM `spi_procstat` WHERE 1;

INSERT INTO `spi_procstat` (`status_id`, `status`) VALUES
('1', 'On-going'),
('2', 'Awarded'),
('3', 'Terminated');


ALTER TABLE `kc_grsintake` CHANGE `sender_designation` `sender_designation` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
