ALTER TABLE `kc_projproposal` CHANGE `kc_amount` `kc_amount` REAL NULL DEFAULT NULL;
ALTER TABLE `kc_projproposal` CHANGE `lcc_amount` `lcc_amount` REAL NULL DEFAULT NULL;

UPDATE `kc_ncddp`.`kc_barangay` SET `barangay` = 'Catug-an' WHERE `kc_barangay`.`barangay_psgc` = '071247013';
UPDATE `kc_ncddp`.`kc_barangay` SET `barangay` = 'Ginopolan' WHERE `kc_barangay`.`barangay_psgc` = '071247017';
UPDATE `kc_ncddp`.`kc_barangay` SET `barangay` = 'Banderahan' WHERE `kc_barangay`.`barangay_psgc` = '071247035';

ALTER TABLE `kc_brgyprofile` CHANGE `alloc_env` `alloc_env` REAL NULL DEFAULT '0', CHANGE `alloc_econ` `alloc_econ` REAL NULL DEFAULT '0', CHANGE `alloc_infra` `alloc_infra` REAL NULL DEFAULT '0', CHANGE `alloc_basic` `alloc_basic` REAL NULL DEFAULT '0', CHANGE `alloc_educ` `alloc_educ` REAL NULL DEFAULT '0', CHANGE `alloc_peace` `alloc_peace` REAL NULL DEFAULT '0', CHANGE `alloc_inst` `alloc_inst` REAL NULL DEFAULT '0', CHANGE `alloc_gender` `alloc_gender` REAL NULL DEFAULT '0', CHANGE `alloc_drrm` `alloc_drrm` REAL NULL DEFAULT '0', CHANGE `alloc_others` `alloc_others` REAL NULL DEFAULT '0';
