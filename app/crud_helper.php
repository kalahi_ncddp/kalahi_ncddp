<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/15/2015
 * Time: 11:08 PM
 */


function delete($url,$id,$message)
{
    /*html helper for delete*/
    return ?>
        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Delete <?php echo $id ?> }}</h4>
                    </div>
                    <div class="modal-body">
                        <?php echo Form::open(array('url' => $url .'/'. $id)) ?>
                        <?php echo  Form::hidden('_method', 'DELETE') ?>
                        <?php echo $message ?>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
                        <?php echo Form::submit('Delete', array('class' => 'btn btn-warning'))?>
                        <?php echo Form::close() ?>
                    </div>
                </div>
            </div>
        </div>
    <?php
}