<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/





/*TEMPORARY ROUTES*/
Route::get('/bugfix',function(){
	Volunteer::where('cycle_id','NCDDP')->update(['cycle_id'=>'NCDDP-1']);
	 Session::flash('message','1.4.x BUG FIX');
	 return Redirect::to('/reports');
});


Route::get('/updatedb',function(){

		$update_status = false;
		try{
			DB::statement("ALTER TABLE kc_activityissues CHANGE activity_id activity_id VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL");
			DB::statement("ALTER TABLE spi_mlccmain ADD program_id VARCHAR(20) NULL , ADD cycle_id VARCHAR(20) NULL;");

		}catch(Exception $e){
			// dd($e->getMessage());
			
		}
		//1.6.2 mlccmain update for the table

		
		if($update_status){

			Session::flash('must_update',true);
		}

		Session::flash('message','Software update has been completed');
		return Redirect::to('auth');
});
/*TEMPORARY ROUTES*/

Route::post('/bassembly/pincos', array('uses' => 'PSAController@pincos'));
Route::post('/pincos', array('uses' => 'PSAController@pincos'));


Route::get('bassemblies', array('as' => 'bassembly', 'uses' => 'BAssemblyController@index'));





Route::get('bassembly/{id}/set', 'BAssemblyController@setFinal');
Route::get('psa/{id}/set', 'PSAController@setFinal');

Route::resource('bassembly', 'BAssemblyController');




Route::resource('psa', 'PSAController');


Route::get('mibf/{id}/show_proposals/{purpose}', array('as' => 'show_proposals', 'uses' => 'MIBFController@show_proposals'));
Route::match(array('GET', 'POST', 'PUT'), "mibf/{id}/edit_proposal/{purpose}", array('as' => 'edit_proposal', 'uses' => 'MIBFController@edit_proposal'));
Route::match(array('GET', 'POST', 'PUT'), "mibf/{id}/add_proposal/{purpose}", array('as' => 'add_proposal', 'uses' => 'MIBFController@add_proposal'));

Route::post('mibf/submit_proposal', array('uses' => 'MIBFController@submit_proposal'));

/*Volunteer Profile Route */
Route::resource('volunteer', 'VolunteerController');
Route::get('volunteer/create_volunteer/{id}', array('uses' => 'VolunteerController@create_volunteer'));
Route::post('volunteer/store_volunteer', array('uses' => 'VolunteerController@store_volunteer'));

/*Training*/

Route::resource('mun_trainings', 'MunTrainingController');
Route::get('mun_trainings/{id}/enlist_participants', array('as' => 'enlist_participants', 'uses' => 'MunTrainingController@enlist_participants'));
Route::get('mun_trainings/{id}/add_participant', array('as' => 'add_participant', 'uses' => 'MunTrainingController@add_participant'));
Route::post('mun_trainings/submit_participant', array('uses' => 'MunTrainingController@submit_participant'));
Route::post('mun_trainings/delete_participant', array('uses' => 'MunTrainingController@delete_participant'));
Route::get('api/volunteer', array('as'=>'api.volunteer', 'uses'=>'MunTrainingController@getVolunteer'));
Route::get('api/participant', array('as'=>'api.participant', 'uses'=>'MunTrainingController@getParticipant'));


Route::resource('brgy_trainings', 'BrgyTrainingController');
Route::get('brgy_trainings/{id}/enlist_participants', array('as' => 'enlist_participants', 'uses' => 'BrgyTrainingController@enlist_participants'));
Route::get('brgy_trainings/{id}/add_participant', array('as' => 'add_participant', 'uses' => 'BrgyTrainingController@add_participant'));
Route::post('brgy_trainings/submit_participant', array('uses' => 'BrgyTrainingController@submit_participant'));
Route::post('brgy_trainings/delete_participant', array('uses' => 'BrgyTrainingController@delete_participant'));

/*Brgy Profile*/


Route::get('brgyprofile/{id}/show_bdcp/', array('as' => 'show_bdcp', 'uses' => 'BrgyProfileController@show_bdcp'));
Route::get('brgyprofile/{id}/show_person/', array('as' => 'show_person', 'uses' => 'BrgyProfileController@show_person'));
Route::get('brgyprofile/{id}/create_person/', array('as' => 'create_person', 'uses' => 'BrgyProfileController@create_person'));
Route::post('brgyprofile/submit_person', array('uses' => 'BrgyProfileController@submit_person'));
Route::get('brgyprofile/{id}/edit_person' , 'BrgyProfileController@edit_person');
Route::post('brgyprofile/{id}/show_bdcp/delete/', 'BrgyProfileController@delete_bdcp');
Route::post('brgyprofile/update_person', array('uses' => 'BrgyProfileController@update_person'));

Route::get('brgyprofile/{id}/show_blgu/', array('as' => 'show_blgu', 'uses' => 'BrgyProfileController@show_blgu'));
Route::get('brgyprofile/{id}/show_person_blgu/', array('as' => 'show_person_blgu', 'uses' => 'BrgyProfileController@show_person_blgu'));
Route::get('brgyprofile/{id}/create_person_blgu/', array('as' => 'create_person_blgu', 'uses' => 'BrgyProfileController@create_person_blgu'));
Route::post('brgyprofile/submit_person_blgu', array('uses' => 'BrgyProfileController@submit_person_blgu'));
Route::get('brgyprofile/{id}/edit_person_blgu' , 'BrgyProfileController@edit_person_blgu');
Route::post('brgyprofile/{id}/show_blgu/delete/', 'BrgyProfileController@delete_blgu');
Route::post('brgyprofile/update_person_blgu', array('uses' => 'BrgyProfileController@update_person_blgu'));

Route::get('brgyprofile/{id}/show_bdcmeetings/', array('as' => 'show_bdcmeetings', 'uses' => 'BrgyProfileController@show_bdcmeetings'));
Route::get('brgyprofile/{id}/show_meeting/', array('as' => 'show_meeting', 'uses' => 'BrgyProfileController@show_meeting'));
Route::get('brgyprofile/{id}/create_meeting/', array('as' => 'create_meeting', 'uses' => 'BrgyProfileController@create_meeting'));
Route::post('brgyprofile/submit_meeting', array('uses' => 'BrgyProfileController@submit_meeting'));
Route::post('brgyprofile/{id}/show_bdcmeetings/delete/', 'BrgyProfileController@delete_meeting');
Route::get('brgyprofile/{id}/edit_meeting' , 'BrgyProfileController@edit_meeting');
Route::post('brgyprofile/update_meeting', array('uses' => 'BrgyProfileController@update_meeting'));

Route::get('brgyprofile/{id}/show_orgs/', array('as' => 'show_orgs', 'uses' => 'BrgyProfileController@show_orgs'));
Route::get('brgyprofile/{id}/show_org/', array('as' => 'show_org', 'uses' => 'BrgyProfileController@show_org'));
Route::get('brgyprofile/{id}/create_org/', array('as' => 'create_org', 'uses' => 'BrgyProfileController@create_org'));
Route::post('brgyprofile/submit_org', array('uses' => 'BrgyProfileController@submit_org'));
Route::post('brgyprofile/{id}/show_orgs/delete/', 'BrgyProfileController@delete_org');
Route::get('brgyprofile/{id}/edit_org' , 'BrgyProfileController@edit_org');
Route::post('brgyprofile/update_org', array('uses' => 'BrgyProfileController@update_org'));

	// Route::get('grs-municipality/delete/{id}/{cycle}', array('as' => 'GRSMunicipality', 'uses' => 'GrsMunicipalityController@destroy'));


Route::get('brgyprofile/{id}/show_devprojs/', array('as' => 'show_devprojs', 'uses' => 'BrgyProfileController@show_devprojs'));
Route::get('brgyprofile/{id}/show_devproj/', array('as' => 'show_devproj', 'uses' => 'BrgyProfileController@show_devproj'));
Route::get('brgyprofile/{id}/create_devproj/', array('as' => 'create_devproj', 'uses' => 'BrgyProfileController@create_devproj'));
Route::post('brgyprofile/submit_devproj', array('uses' => 'BrgyProfileController@submit_devproj'));
Route::post('brgyprofile/{id}/show_devprojs/delete/', 'BrgyProfileController@delete_devproj');
Route::get('brgyprofile/{id}/edit_devproj' , 'BrgyProfileController@edit_devproj');
Route::post('brgyprofile/update_devproj', array('uses' => 'BrgyProfileController@update_devproj'));

/* Municipal Profile */

Route::get('muniprofile/{id}/show_mdcp/', array('as' => 'show_mdcp', 'uses' => 'MuniProfileController@show_mdcp'));
Route::get('muniprofile/{id}/show_person/', array('as' => 'mdcp_show_person', 'uses' => 'MuniProfileController@show_person'));
Route::get('muniprofile/{id}/create_person/', array('as' => 'mdcp_create_person', 'uses' => 'MuniProfileController@create_person'));
Route::post('muniprofile/submit_person', array('uses' => 'MuniProfileController@submit_person'));
Route::get('muniprofile/{id}/edit_person' , 'MuniProfileController@edit_person');
Route::post('muniprofile/{id}/show_mdcp/delete/', 'MuniProfileController@delete_mdcp');

Route::post('muniprofile/update_person', array('uses' => 'MuniProfileController@update_person'));

Route::get('muniprofile/{id}/show_mlgu/', array('as' => 'show_mlgu', 'uses' => 'MuniProfileController@show_mlgu'));
Route::get('muniprofile/{id}/show_person_mlgu/', array('as' => 'mlgu_show_person', 'uses' => 'MuniProfileController@show_person_mlgu'));
Route::get('muniprofile/{id}/create_person_mlgu/', array('as' => 'mlgu_create_person', 'uses' => 'MuniProfileController@create_person_mlgu'));
Route::post('muniprofile/submit_person_mlgu', array('uses' => 'MuniProfileController@submit_person_mlgu'));
Route::get('muniprofile/{id}/edit_person_mlgu' , 'MuniProfileController@edit_person_mlgu');
Route::post('muniprofile/{id}/show_mlgu/delete/', 'MuniProfileController@delete_mlgu');

Route::post('muniprofile/update_person_mlgu', array('uses' => 'MuniProfileController@update_person_mlgu'));

/*LGU Engagement*/
Route::resource('lguengagement', 'LGUEngagementController');

Route::get('reference_approved','ReferenceController@approved');
Route::get('reference_approved_grs','ReferenceController@approvedGRS');

/*
|  + Authentication Route Group +
|  Middlelayer between authentication no need to check the auth in every
|  controller all authentication are handle in filters
|
*/
Route::get('/' , function() {
	return Redirect::to('/auth');
} );

Route::group(array('prefix' => 'auth') , function() {

	Route::get( '/' , 'AuthenticationController@index' );
	Route::post( '/login' , 'AuthenticationController@login' );
	Route::get( '/logout' , 'AuthenticationController@logout' );

}  );

/*
|  + Authentication Route Group +
|             END
*/



/*
|    + Module Routes Group +
|    all routes related must be inside in this group
|    these are the routes that requires authencation and
|    handled by the authentication controller
|
 */

Route::group(array( 'before' => 'auth') ,
    function() {
	/*
	| GRS Module
	|  - GRS Municipality
	|  - GRS Barangay
	|  - GRS Installer
	*/
	
// Route::get('/reset',function(){
// 	DB::table('rf_approved')->delete();	
//      Session::flash('message','You may now delete your dummy data');
// 	 return Redirect::to('/reports');
// });


Route::get('reports', array('as' => 'reports' ,'uses'=>'reports@index'));
Route::get('', array('as' => 'reports' ,'uses'=>'reports@index'), function(){
	if(Auth::user()){
		$user = Auth::user();
		$now = new DateTime();
		$user->last_activity = $now;
		$user->save();
	}
});

	Route::get('resolved',function(){
		$firstname = Input::get('firstname');
		$lastname = Input::get('lastname');

		$middlename = Input::get('middlename');


		$bene = Beneficiary::where('firstname',$firstname)
							->where('lastname',$lastname)
							->where('middlename',$middlename)
							->first();

		if(!empty($bene)){
			Trnparticipants::where('firstname',$firstname)
				->where('lastname',$lastname)
				->where('middlename',$middlename)
				->update(['beneficiary_id'=>$bene->beneficiary_id]);
			return Redirect::back()->with('message','Data resolved');
		}else{
			$trnpart = Trnparticipants::where('firstname',$firstname)
				->where('lastname',$lastname)
				->where('middlename',$middlename)->first()->toArray();

				unset($trnpart['reference_no']);
				unset($trnpart['psgc_id']);
				unset($trnpart['age']);
				unset($trnpart['is_volunteer']);
				unset($trnpart['participant_id']);
				unset($trnpart['age']);

			$bene_id = Beneficiary::generateBeneficiaryId($trnpart);
			Trnparticipants::where('firstname',$firstname)
				->where('lastname',$lastname)
				->where('middlename',$middlename)->update(['beneficiary_id'=>$bene_id]);
		}
		return Redirect::back();
	});
    Route::get('/comments/get','reports@getComments');
	/* added route must be inside*/
//        Route::post('grs-intake/{id}','GrsIntakeController@destroy');
        Route::resource('grs-intake','GrsIntakeController');
        Route::get('pincos-grievances','GrsIntakeController@pincos');
        //
        Route::post('ba/approved','BAssemblyController@approved');
        Route::post('psa/approved','PSAController@approved');
        Route::post('mibf/approved','MIBFController@approved');
	// rest api for GRS Barangay installation
	Route::get('grs-barangay/rest-barangay','GrsBarangayController@barangayData');
	Route::get('ceac/plans/barangay/rest-barangay','CeacPlansBrgyController@barangayData');
	Route::get('ceac/plans/barangay/rest-fund','CeacPlansBrgyController@lguCycles');

	Route::get('ceac/plans/municipality/rest-barangay','CeacPlansMunController@barangayData');
	Route::get('ceac/plans/municipality/rest-fund','CeacPlansMunController@lguCycles');


	Route::get('grs-barangay/rest-fund','GrsBarangayController@lguCycles');
	Route::get('grs-municipality/rest-fund','GrsBarangayController@lguCycles');

	// grs intake

	Route::resource('grs-intake','GrsIntakeController');
    Route::get('pincos-grievances','GrsIntakeController@pincos');

	/* GRS Municipality  routes */


	Route::get('grs-municipality', array('as' => 'GRSMunicipality', 'uses' => 'GrsMunicipalityController@index'));
	Route::get('grs-municipality/create', array('as' => 'GRSMunicipality', 'uses' => 'GrsMunicipalityController@create'));
	Route::post('grs-municipality/create', array('as' => 'GRSMunicipality', 'uses' => 'GrsMunicipalityController@store'));

	Route::get('grs-municipality/{id}/edit', array('as' => 'GRSMunicipality', 'uses' => 'GrsMunicipalityController@edit'));
	Route::get('grs-municipality/{id}', array('as' => 'GRSMunicipality', 'uses' => 'GrsMunicipalityController@show'));

	Route::post('grs-municipality/{id}/edit', array('as' => 'GRSMunicipality', 'uses' => 'GrsMunicipalityController@update'));

	Route::resource('grs-municipality',  'GrsMunicipalityController');


	/* GRS Barangay   routes */
        Route::resource('grs-barangay',  'GrsBarangayController');

	Route::get('grs-barangay', array('as' => 'GRSbarangay', 'uses' => 'GrsBarangayController@index'));
	Route::get('grs-barangay/create', array('as' => 'GRSbarangay', 'uses' => 'GrsBarangayController@create'));
	Route::post('grs-barangay/create', array('as' => 'GRSbarangay', 'uses' => 'GrsBarangayController@store'));
	Route::get('grs-barangay/{id}', array('as' => 'GRSbarangay', 'uses' => 'GrsBarangayController@show'));

	Route::post('grs-barangay/{id}', array('as' => 'GRSbarangay', 'uses' => 'GrsBarangayController@update'));


	/* CEAC routes */
	Route::get('ceac',array('as' => 'Ceac', 'uses' => 'GrsBarangayController@index'));
	Route::get('ceac/plans',array('as' => 'Ceac', 'uses' => 'CeacPlansController@index'));


        Route::resource('ceac/monitoring','CeacMonitoringController');
        Route::get('ceac/monitoring',array('as' => 'Ceac', 'uses' => 'CeacMonitoringController@index'));
        Route::post('ceac/monitoring/create','CeacMonitoringController@store');
	Route::get('ceac/monitoring/{cycle}/{year}/{id}',array('as' => 'Ceac', 'uses' => 'CeacMonitoringController@show'));
	Route::post('ceac/monitoring','CeacMonitoringController@update');
	Route::get('ceac/barangay/{cycle}/{year}/{muni_id}/{brgy_id}',array('as' => 'Ceac', 'uses' => 'CeacPlansBrgyController@show'));
	Route::post('ceac/barangay/{cycle}/{year}/{muni_id}/{brgy_id}','CeacPlansBrgyController@update');


	/*Volunteer Profile Route */
	Route::resource('volunteer', 'VolunteerController');
	Route::post('volunteer/edit', 'VolunteerController@edit');
	Route::get('volunteer/create_volunteer/{id}', array('uses' => 'VolunteerController@create_volunteer'));
	Route::post('volunteer/store_volunteer', array('uses' => 'VolunteerController@store_volunteer'));
	Route::get('volunteer/view', 'VolunteerController@view');
	Route::get('volunteer/view/{vid}', 'VolunteerController@view');

	// mibf


	Route::get('mibf/generate-report', array('as' => 'mibf.reports', 'uses' => 'MIBFController@generate_report'));

	Route::resource('mibf', 'MIBFController');
	Route::get('mibf/{id}/show_proposals', array('as' => 'show_proposals', 'uses' => 'MIBFController@show_proposals'));
	Route::get('mibf/{id}/add_proposal', array('as' => 'add_proposal', 'uses' => 'MIBFController@add_proposal'));
	Route::post('mibf/submit_proposal', array('uses' => 'MIBFController@submit_proposal'));

	Route::get('mibf/{id}/edit_proposal' , 'MIBFController@edit_proposal');
	/*Training*/

	Route::resource('mun_trainings', 'MunTrainingController');
	Route::get('mun_trainings/{id}/enlist_participants', array('as' => 'enlist_participants', 'uses' => 'MunTrainingController@enlist_participants'));
	Route::get('mun_trainings/{id}/add_participant', array('as' => 'add_participant', 'uses' => 'MunTrainingController@add_participant'));
	Route::post('mun_trainings/submit_participant', array('uses' => 'MunTrainingController@submit_participant'));
	Route::post('mun_trainings/delete_participant', array('uses' => 'MunTrainingController@delete_participant'));

		// add edit
	Route::get('mun_trainings/{id}/edit_participant', array('uses' => 'MunTrainingController@edit_participant'));
	Route::put('mun_trainings/{id}/edit_participant', array('uses' => 'MunTrainingController@updateParticipants'));


	Route::resource('brgy_trainings', 'BrgyTrainingController');
	Route::get('brgy_trainings/{id}/enlist_participants', array('as' => 'enlist_participants', 'uses' => 'BrgyTrainingController@enlist_participants'));
	Route::get('brgy_trainings/{id}/add_participant', array('as' => 'add_participant', 'uses' => 'BrgyTrainingController@add_participant'));
	Route::post('brgy_trainings/submit_participant', array('uses' => 'BrgyTrainingController@submit_participant'));
	Route::post('brgy_trainings/delete_participant', array('uses' => 'BrgyTrainingController@delete_participant'));
	// add edit
	Route::get('brgy_trainings/{id}/edit_participant', array('uses' => 'BrgyTrainingController@edit_participant'));
	Route::put('brgy_trainings/{id}/edit_participant', array('uses' => 'BrgyTrainingController@updateParticipants'));

	/* MLPRAP */
	Route::resource('mlprap', 'MLPRAPController', array('except' => array('show', 'destroy')));
	Route::get('mlprap/delete/{id}', array('as'=>'mlprap.delete', 'uses'=>'MLPRAPController@delete'));

	Route::get('script/dropdown', array('as' => 'mlprap.dropdown', 'uses' => 'MLPRAPController@dropdown'));

	/* MLPRAP Details */
	Route::resource('mlprapdetails', 'MLPRAPDetailsController', array('except' => array('create', 'destroy')));

	Route::get('mlprapdetails/delete/{id}', array('as'=>'mlprapdetails.delete', 'uses'=>'MLPRAPDetailsController@delete'));
	Route::get('mlprapdetails/create/{id}', array('as'=>'mlprapdetails.create', 'uses'=>'MLPRAPDetailsController@create'));

	/* Downlaod JSON File - Barangay Assembly */
	Route::get('download/json', array('as'=>'download.json', 'uses' => 'SyncController@download'));
	Route::get('upload/json', array('as'=>'upload.json', 'uses' => 'SyncController@upload'));


        Route::get('approved/untag','ApprovalController@untag');
        Route::resource('approved','ApprovalController');

        Route::post('approvedCeac','ApprovalController@ceac');
        /* Brgy/Muni Profile */
    Route::resource('brgyprofile', 'BrgyProfileController');
	Route::post('brgyprofile/submit_org', array('uses' => 'BrgyProfileController@submit_org'));
	Route::post('brgyprofile/submit_brgydevproj', array('uses' => 'BrgyProfileController@submit_brgydevproj'));

    Route::resource('muniprofile', 'MuniProfileController');

    Route::resource('sync','SyncController');

    Route::resource('user','UserManagementController');

    /*Volunteer*/
	Route::get('api/volunteers', array('as'=>'api.volunteers', 'uses'=>'VolunteerController@getVolunteer'));

	/*PSA API*/
	Route::get('api/communitytraining', array('as'=>'api.commtraining', 'uses'=>'PSAController@getCommunityTraining'));
   /*MIBF API */
   Route::get('api/muntraining', array('as'=>'api.muntraining', 'uses'=>'MIBFController@getMunicipalTraining'));
	/*END PSA*/
    /* Sub Project worker API*/
    Route::get('spi-subworkers/api-typeofwork','SPISbPrjWrkrsController@apiTypeofwork');
    /*LGU Engagement*/
	Route::resource('lguengagement', 'LGUEngagementController');

	/* REPORT GENERATION PDF , XLS */

	Route::get('reports/ba','ReportController@ba');
	Route::get('reports/ba/generate','ReportController@BApdf');

	Route::get('reports/psa','ReportController@psa');
	Route::get('reports/psa/generate','ReportController@PSApdf');
	Route::get('reports/grievances','ReportController@grievances');
	Route::get('reports/grievances/generate','ReportController@grievancespdf');

	// GRS Concerns
	Route::get('reports/grs_concerns', 'ReportController@grs_concerns');
	Route::get('reports/grs_concerns/generate', 'ReportController@grs_concerns_pdf');

	// GRS Gender
	Route::get('reports/grs_gender', 'ReportController@grs_gender');
	Route::get('reports/grs_gender/generate', 'ReportController@grs_gender_pdf');

	// GRS Mode of Filing
	Route::get('reports/grs_mode_of_filing', 'ReportController@grs_mode_of_filing');
	Route::get('reports/grs_mode_of_filing/generate', 'ReportController@grs_mode_of_filing_pdf');


	// GRS Status
	Route::get('reports/grs_status', 'ReportController@grs_status');
	Route::get('reports/grs_status/generate', 'ReportController@grs_status_pdf');

	// Prioritized SPs by MIBF
	Route::get('reports/sp_by_mibf', 'ReportController@sp_by_mibf');
	Route::get('reports/sp_by_mibf/generate', 'ReportController@sp_by_mibf_pdf');

	// BA total Attendies
	Route::get('reports/ba_total_attendies','ReportController@batotalattendees');
	Route::get('reports/ba_total_attendees/generate', 'ReportController@BATotalAttendeespdf' );
	 // Volunteer Communitee
	Route::get('reports/volunteer_communitee','ReportController@volunteercomitee');
	Route::get('reports/volunteer_communitee/generate', 'ReportController@volunteercomiteepdf' );
	 // Training Participants
	Route::get('reports/training-participants','ReportController@trainingparticipants');
	Route::get('reports/training_participants/generate', 'ReportController@trainingparticipantspdf' );
	//MIBF
	Route::get('reports/mibf','ReportController@mibf');
	Route::get('reports/mibf/generate','ReportController@mibf_pdf');

	 // ceac
	Route::get('reports/ceac','ReportController@ceac');
	Route::get('reports/ceac/generate', 'ReportController@ceacpdf' );

	/* ASSET MANAGEMENT */
	Route::resource('asset', 'AssetController');

    /*SPI MODULES ROUTES FOLLOWS BELOW*/
    /*
     * SPI MODULE
     * @description:
     *
     */
    Route::resource('spi-comm',           'SPICommChkLstController');
    Route::resource('spi-subworkers',     'SPISbPrjWrkrsController');
    Route::resource('spi-erslist',        'SPIERSListController');
    Route::resource('spi-ers',            'SPIERSController');
    Route::resource('spi-munsusplan',     'SPIMunSustnPlanController');
    Route::resource('spi-techassist',     'SPITechAssistController');

    Route::resource('spi-fnddpsa',        'SPIFundedPSAController');
    Route::controller('fnddpsa',        'SPIFundedPSAController');

    Route::resource('spi-accomplishment', 'SPISbPrjAcmplshmntController');
    Route::resource('spi-disaster',       'SubProjDisasterController');

    Route::get('spi-worker/generateid', 'SPISbPrjWrkrsController@genid');

    Route::get('spi-disaster/{id}/export','SubProjDisasterController@export');
	 // spi profile
	Route::post('spi_profile/create', array( 'as' => 'spi_profile.create', 'uses' => 'SPIProfileController@create'));
	Route::get('spi_profile/build', array( 'as' => 'spi_profile.build', 'uses' => 'SPIProfileController@build'));
	Route::resource('spi_profile', 'SPIProfileController', array('except' => array('create')));

  // completion report
    Route::get('spi_profile/{spi_profile}/spcr', array('as' => 'spi_profile.spcr.show', 'uses' => 'SPICompletionReportController@show'));
    Route::get('spi_profile/{spi_profile}/spcr/edit', array('as' => 'spi_profile.spcr.edit', 'uses' => 'SPICompletionReportController@edit'));
    Route::patch('spi_profile/{spi_profile}/spcr', array('as' => 'spi_profile.spcr.update', 'uses' => 'SPICompletionReportController@update'));
    Route::resource('spi_profile.spcr', 'SPICompletionReportController', array('only' => array('create', 'store')));

  // SET
  Route::resource('spi_profile.set', 'SPISETController');

  // Certificate
  Route::get('spi_profile/{spi_profile}/certificate', array('as' => 'spi_profile.certificate', 'uses' => 'SPIProfileController@certificate'));
  Route::get('spi_profile/{spi_profile}/certificate/edit', array('as' => 'spi_profile.edit_certificate', 'uses' => 'SPIProfileController@edit_certificate'));
  Route::patch('spi_profile/{spi_profile}/certificate', array('as' => 'spi_profile.update_certificate', 'uses' => 'SPIProfileController@update_certificate'));

  // Household
  Route::get('spi_profile/{spi_profile}/household', array('as' => 'spi_profile.household', 'uses' => 'SPIHouseholdBeneficiariesController@show'));
  Route::get('spi_profile/{spi_profile}/household/edit', array('as' => 'spi_profile.edit_household', 'uses' => 'SPIHouseholdBeneficiariesController@edit'));
  Route::patch('spi_profile/{spi_profile}/household', array('as' => 'spi_profile.update_household', 'uses' => 'SPIHouseholdBeneficiariesController@update'));

  // Community Procurement Monitoring
  Route::get('spi_profile/{spi_profile}/procurement', array('as' => 'spi_profile.procurement.show', 'uses' => 'SPICommunityProcurementController@show'));
  Route::get('spi_profile/{spi_profile}/procurement/edit', array('as' => 'spi_profile.procurement.edit', 'uses' => 'SPICommunityProcurementController@edit'));
  Route::patch('spi_profile/{spi_profile}/procurement', array('as' => 'spi_profile.procurement.update', 'uses' => 'SPICommunityProcurementController@update'));
  Route::resource('spi_profile.procurement', 'SPICommunityProcurementController', array('only' => array('create', 'store')));

  // tranche
  Route::get('spi_profile/{spi_profile}/tranche', array('as' => 'spi_profile.tranche.show', 'uses' => 'SPITrancheController@show'));
  Route::get('spi_profile/{spi_profile}/tranche/edit', array('as' => 'spi_profile.tranche.edit', 'uses' => 'SPITrancheController@edit'));
  Route::patch('spi_profile/{spi_profile}/tranche', array('as' => 'spi_profile.tranche.update', 'uses' => 'SPITrancheController@update'));
  Route::resource('spi_profile.tranche', 'SPITrancheController', array('only' => array('create', 'store')));

  // MLCC
  Route::resource('mlcc', 'MLCCController');

  Route::post('spi_category/unit', array( 'uses' => 'SPIProfileController@get_category_unit'));

  // Project Coverage and Staffing
  Route::resource('project-coverage','ProjectCoverageAndStaffingController');
  Route::resource('supplier','SupplierController');
  Route::resource('project-training', 'ProjectTrainingController');
//   Route::resource('service-provider', 'ServiceProviderController');


  // online web app user controller for adding user SRPMO , NRPMO
  Route::resource('user-admin','UserAtOnlineController');

    Route::get('retrieve-comment','CommentsController@getComments');

//added psa problem and one to many solutions
        Route::get('psa/{id}/priority-problems','PSAProblemsAndSolutionController@index');
        Route::get('psa/{id}/priority-problems/create','PSAProblemsAndSolutionController@create');
        Route::post('psa/{id}/priority-problems/','PSAProblemsAndSolutionController@store');
        Route::get('psa/{id}/priority-problems/{ids}','PSAProblemsAndSolutionController@show');
        Route::get('psa/{id}/priority-problems/{ids}/edit','PSAProblemsAndSolutionController@edit');
        Route::post('psa/{id}/priority-problems/{ids}/edit','PSAProblemsAndSolutionController@update');
        Route::post('psa/{id}/priority-problems/{ids}/delete','PSAProblemsAndSolutionController@destroy');



        /* newly added feature */


        include 'routes/new_feature_route.php';
} );		











