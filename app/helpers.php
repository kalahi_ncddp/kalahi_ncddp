<?php
/*
|--------------------------------------------------------------------------
| HELPER FIle
|--------------------------------------------------------------------------
|
|
*/

// 
View::share('user', Auth::user());

 function version(){
     $version = '1.7.6';
     return  $version;
 }  
View::share('version',version());



function toDate($date)
{
	if($date!=null ){
       if(strtotime($date)=='0000-00-00')
        {
            return "";
        }

         return date('m/d/Y',strtotime($date));

    }
	else{

        return '';
    }
}

function deFormat($date)
{
    return date('Y-m-d',strtotime($date));
}


function getDurationDay($startDate, $endDate , $differenceFormat = '%a' )
{
    $datetime1 = date_create($startDate);
    $datetime2 = date_create($endDate);
    $interval = date_diff($datetime1, $datetime2);

    return $interval->format($differenceFormat) + 1;

}

/* return string this will format your data into required format for currencies */
function currency_format($curr = 0)
{
    return number_format($curr, 2, '.', ',');
}


function is_review($activity_id,$level = 'ACT')
{
    $session_user = \Session::get('username');
//        $user_id = $this->user->getPsgcId($session_user);
    $user = \User::where('username',$session_user)->first();
    $mode = \Session::get('accelerated');
    $approved = \Approval::where( 'activity_id',$activity_id )
        ->where('kc_mode',$mode)->first();

    if($approved!=null)
    {
        if($level == 'ACT')
        {

            if ( !isset($approved->act_reviewed )  ) {
                return false;
            }
            else {
                return true;
            }
        }

    }
    else
    {

        if($level == 'ACT')
        {
            if ( !isset( $approved->act_reviewed )  ) {
               return false;
            }
            else {
                return true;
            }
        }

    }

}
/**
 * @param $model
 * @return string
 */
function review($model,$id)
{
    if($model->is_draft==0) {
        if ($model->reference) {
            if ($model->reference->isApproved()) {
                return '<input type="checkbox" name="check[]" class="checkbox1" value="' . $id . '"/>
                            <span class="label label-info">for review</span>';

            } else {
                return '<span class="label label-success">reviewed</span>';
            }
        } else {
            return '<input type="checkbox" name="check[]" class="checkbox1" value="' . $id . '"/><span class="label label-info">for review</span>';
        }
    }
    else
    {
        return '<span class="label label-warning">draft</span>';
    }
}

function reviewCeac($ref,$program,$cycle,$psgc_id)
{
    $exist = Approval::where('cycle_id',$cycle)
        ->where('program_id',$program)
        ->where('activity_id',$ref)
        ->where('kc_mode',Session::get('accelerated'))
    ->where('psgc_id',$psgc_id)->exists();

    return $exist;

}
function commented($activity_id, $office)
{
    $mode = Session::get('accelerated');
    $username = Session::get('username');
    $user = User::where('username',$username)->first();
    $comment =  Comments::where('kc_mode',$mode)->where('office',$office)->where('activity_id',$activity_id)->first();

    if($comment){
        return '<span class="label label-info"> commented </span>';
    } else {
        return '<span class="label label-warning"> not commented </span>';
    }
}

function hasComment($id){
    $mode = Session::get('accelerated');
    $username = Session::get('username');
    $user = User::where('username',$username)->first();
    $comment =  Comments::where('kc_mode',$mode)->where('activity_id',$id)->exists();
    if($comment){
        return '(commented)';
    }
}

function viewComment($id){
    $mode = Session::get('accelerated');
    $comment = Comments::where('activity_id',$id)->where('kc_mode',$mode)->get();


    if(Comments::where('activity_id',$id)->where('kc_mode',$mode)->exists()){
        ?>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Comments</h3>
                <div class="box-tools pull-right" >
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                </div>
            </div>
            <div class="box-body">
                <?php foreach($comment as $comments) : ?>
                <div class="item">
                    <p class="message">
                        <a href="#" class="name">
                            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?php echo toDate($comments->created_at) ?></small>
                            (<?php echo $comments->office ?>) :
                        </a>
                        <?php echo $comments->comments ?>
                    </p>
                </div>
                <?php endforeach ?>
            </div>
        </div>
        <?php
    }
}

function getMunicipalityList($psgc_id="")
{
    $input = $psgc_id;

    if($input==''){

        return [''=>'']+\Municipality::get()->lists('municipality','municipality_psgc');

    }

    $municipality = \Municipality::where('province_psgc','like',substr($psgc_id,0,4).'%')->lists('municipality','municipality_psgc');

    return [''=>'']+$municipality;
}

function getBarangayList($psgc_id="")
{
    $input =$psgc_id;

    if($input==''){

        return [''=>'']+Barangay::get()->lists('barangay','barangay_psgc');

    }
    $barangay = [''=>'']+\Barangay::where('muni_psgc','like',substr($psgc_id,0,9).'%')->get()->lists('barangay','barangay_psgc');
    return [''=>'']+$barangay;
}

function getProvinceList($psgc_id="")
{
    $input =$psgc_id;

    if($input==''){

        return [''=>'']+\Provinces::orderBy('province','desc')->get()->lists('province','province_psgc');

    }
    $municipality = \Provinces::where('region_psgc','like',substr($psgc_id,0,2).'%')->orderBy('province','asc')->lists('province','province_psgc');

    return [''=>'']+$municipality;
}


function getProvince($psgc_id="1720000",$name=true){
    $prov = Provinces::where('province_psgc','like',substr($psgc_id,0,4).'%')->first();
    if(!$prov){
        return "";
    }
    if($name){
        return $prov->province;
    }else{
        return $prov->province_psgc;
    }
}

function getRegion($psgc_id="",$name=true){
    $region = Regions::where('psgc_id','like',substr($psgc_id,0,2).'%')->first();
    if(!$region){
        return "";
    }
    if($name){

        return $region->region_name;
    }else{
        return $region->psgc_id;
    }
}

function getMunicipality($psgc_id="1720000",$name=true){
    $muni = Municipality::where('municipality_psgc','like',substr($psgc_id,0,6).'%')->first();
    if(!$muni){
        return "";
    }
    if($name){

        return $muni->municipality;
    }else{
        return $muni->municipality_psgc;
    }
}


function getBarangay($psgc_id="1720000"){
    $barangay = Barangay::where('barangay_psgc','like',substr($psgc_id,0,9).'%')->first();
    if(!$barangay){
        return "";
    }
    return $barangay->barangay;
}