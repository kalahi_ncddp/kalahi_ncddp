<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class SPIFundedPSAPrio extends Eloquent {

	protected $table = 'spi_fundedpsaprio';
//	use SoftDeletingTrait;
	protected $primaryKey    = 'id';
	public    $incrementing  = false;

	protected $fillable = [
		'prob_project_id',
		'kcsoltn_project_id',
		'pmnsoltn_project_id',
        'kc_ncddp_sub_project_id',
        'kc_fund',
        'funder',
        'fund',
        'kc_mode',
        'psgc_id',
        'sub_project',
	];

    public function problem(){
        return PSAProjects::where('project_id',$this->prob_project_id)->first();
    }

    public function solution(){
        return Psasolutions::where('solution_id',$this->kcsoltn_project_id)->first();
    }

    public function spi()
    {
        return $this->hasOne('SPIProfile','project_id','kc_ncddp_sub_project_id');
    }
}
