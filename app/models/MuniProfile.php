<?php

class MuniProfile extends Eloquent {
	//use SoftDeletingTrait;
	protected $table = 'kc_muniprofile';
	protected $primaryKey = 'profile_id';
	//protected $dates = ['deleted_at'];
	protected $fillable = [
		'is_draft'
	];

	
	
	public static function get_muni_profiles($username, $mode){
		$office = Report::get_office($username);
		$psgc_id = Report::get_muni_psgc($username);
		
		if($office === "ACT"){
			$data = MuniProfile::where('municipal_psgc', $psgc_id)
			->where('kc_mode', $mode)
			->get();		
		}
		else if($office === "RPMO" or $office === "SRPMO"){
			$psgc_id = Report::get_muni_psgc_id($psgc_id);
			$data = MuniProfile::whereIn('municipal_psgc', $psgc_id)->get();	
		} 
		else{
			$data = MuniProfile::get();
		}
		
		return $data;
	}
	
	public static function get_muni_profile_existing($psgc_id, $program_id, $cycle_id,$mode){
		$muni_profile = MuniProfile::where('municipal_psgc', $psgc_id)
						->where('cycle_id', $cycle_id)
						->where('program_id', $program_id)
                        ->where('kc_mode',$mode)
						->pluck('profile_id');
			
		return $muni_profile;
	}
	
	public static function get_muni_profile_by_psgc($psgc_id){
		$muni_profile = MuniProfile::where('municipal_psgc', $psgc_id)->orderBy('profile_id', 'desc')->pluck('profile_id');
		
		return $muni_profile;
	}
	
	public static function get_muni_profile_other_data($table, $profile_id){
		$data = DB::table($table)->where('profile_id', $profile_id)->get();
		
		return $data;
	}
	
	public static function save_muni_other_data($table, $data){
		DB::table($table)->insert($data);
	}	
	
	public static function delete_muni_other_data($table, $id){
		DB::table($table)->where('profile_id', $id)->delete();
	}

	public static function get_mdcp_id($id){
		$persons = DB::table('kc_munimdcp')->where('profile_id', $id)->orderBy('person_id', 'desc')->pluck('person_id');

		return $persons;
	}

	public static function get_person($id){
		$person = DB::table('kc_munimdcp')->where('person_id', $id)->first();

		return $person;
	}

	public static function get_mlgu_id($id){
		$persons = DB::table('kc_munimlgu')->where('profile_id', $id)->orderBy('person_id', 'desc')->pluck('person_id');

		return $persons;
	}

	public static function get_person_mlgu($id){
		$person = DB::table('kc_munimlgu')->where('person_id', $id)->first();

		return $person;
	}

    public function techres()
    {
        return $this->hasMany('Munitechres','profile_id','profile_id');
    }

    public function fund()
    {
        return $this->hasMany('Munifund','profile_id','profile_id');
    }

    public function income()
    {
        return $this->hasMany('Muniincome','profile_id','profile_id');
    }

    public function mdcp()
    {
        return $this->hasMany('Munimcdp','profile_id','profile_id');
    }

    public function mlgu()
    {
        return $this->hasMany('Munimlgu','profile_id','profile_id');
    }

    public function empowerment()
    {
        return $this->hasMany('Muniempowerment','profile_id','profile_id');
    }

    public function drmfund()
    {
        return $this->hasMany('Munidrmfund','profile_id','profile_id');
    }

    public function gadfund()
    {
        return $this->hasMany('Munigadfund','profile_id','profile_id');
    }

    public function munitranspo()
    {
        return $this->hasMany('Munimodetranspo','profile_id','profile_id');
    }


}
