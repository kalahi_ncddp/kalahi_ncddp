<?php

class PSAnalysis extends Eloquent {
	
	protected $table = 'kc_psanalysis';
	protected $primaryKey = 'activity_id';

    public function lguactivity()
    {
        return $this->hasOne('LguActivity', 'activity_id','activity_id');
    }


    public function getTraining()
    {
    	$training = Trainings::where('psgc_id',$this->psgc_id)
    							->where('cycle_id',$this->cycle_id)
    							->where('program_id',$this->program_id)
    							->first();
       return $training;
    }

    public function barangay(){
        return Barangay::where('barangay_psgc',$this->psgc_id)->first();
    }
    public function sitiorep()
    {
        return $this->hasMany('SitioRep', 'activity_id', 'activity_id');
    }

    public function sectorsrep()
    {
        return $this->hasMany('SectorsRep', 'activity_id', 'activity_id');
    }

    public function activityissues()
    {
        return $this->hasMany('ActivityIssues', 'activity_id', 'activity_id');
    }

    /* Sub (PSA) */
    public function psaproject()
    {
        return $this->hasMany('PSAProjects', 'activity_id', 'activity_id');
    }
    
    public function psaproblems()
    {
        return $this->hasOne('PSAProjects', 'activity_id', 'activity_id');

    }

    public function psadocuments()
    {
        return $this->hasMany('PSADocuments', 'activity_id', 'activity_id');
    }

}
