<?php

class NCDDP extends Eloquent
{
	public static function get_prov_psgc($muni_psgc){
		$prov_psgc = DB::table('kc_barangay')->where('muni_psgc', $muni_psgc)->pluck('prov_psgc');

		return $prov_psgc;
	}

	public static function get_psgc_id($muni_psgc){
		$psgc_id = DB::table('kc_barangay')->where('muni_psgc', $muni_psgc)->lists('barangay_psgc');

		return $psgc_id;
	}

	public static function get_prov_psgc_id($prov_psgc){
		$psgc_id = DB::table('kc_barangay')->where('prov_psgc', $prov_psgc)->lists('barangay_psgc');

		return $psgc_id;
	}

	public static function get_brgy_assembly($username) {
		$office = NCDDP::get_office($username);
		$muni_psgc = NCDDP::get_muni_psgc($username);
		$prov_psgc = NCDDP::get_prov_psgc($muni_psgc);

		if($office == "ACT"){
			$psgc_id = NCDDP::get_psgc_id($muni_psgc);
			$data = DB::table('kc_bassembly')->whereIn('psgc_id', $psgc_id)->get();
		}else if($office == "SRPMO"){
			$psgc_id = NCDDP::get_prov_psgc_id($prov_psgc);
			$data = DB::table('kc_bassembly')
	            ->whereExists(function($query) use($muni_psgc)
	            {
	                $query->select(DB::raw(1))
	                      ->from('kc_barangay')
	                      ->whereRaw("kc_bassembly.act_reviewed = 1 AND kc_barangay.muni_psgc = ?
	                      	AND kc_bassembly.psgc_id = kc_barangay.barangay_psgc", array($muni_psgc));
	            })
	            ->get();
		}else if($office == "RPMO"){
			$psgc_id = NCDDP::get_prov_psgc_id($prov_psgc);
			$data = DB::table('kc_bassembly')
	            ->whereExists(function($query) use($muni_psgc)
	            {
	                $query->select(DB::raw(1))
	                      ->from('kc_barangay')
	                      ->whereRaw("kc_bassembly.srpmo_reviewed = 1 AND kc_barangay.muni_psgc = ?
	                      	AND kc_bassembly.psgc_id = kc_barangay.barangay_psgc", array($muni_psgc));
	            })
	            ->get();
		}else{
			$data = DB::table('kc_bassembly')->where('rpmo_reviewed', 1)->get();
		}
		return $data;
	}

	public static function get_volunteers($username) {
		$office = NCDDP::get_office($username);
		$muni_psgc = NCDDP::get_muni_psgc($username);
		$prov_psgc = NCDDP::get_prov_psgc($muni_psgc);

		if($office == "ACT"){
			$psgc_id = NCDDP::get_psgc_id($muni_psgc);
			$data = DB::table('kc_volunteers')->whereIn('psgc_id', $psgc_id)->get();
		}else if($office == "SRPMO"){
			$psgc_id = NCDDP::get_prov_psgc_id($prov_psgc);
			$data = DB::table('kc_volunteers')
	            ->whereExists(function($query) use($muni_psgc)
	            {
	                $query->select(DB::raw(1))
	                      ->from('kc_barangay')
	                      ->whereRaw("kc_volunteers.act_reviewed = 1 AND kc_barangay.muni_psgc = ?
	                      	AND kc_volunteers.psgc_id = kc_barangay.barangay_psgc", array($muni_psgc));
	            })
	            ->get();
		}else if($office == "RPMO"){
			$psgc_id = NCDDP::get_prov_psgc_id($prov_psgc);
			$data = DB::table('kc_volunteers')
	            ->whereExists(function($query) use($muni_psgc)
	            {
	                $query->select(DB::raw(1))
	                      ->from('kc_barangay')
	                      ->whereRaw("kc_volunteers.srpmo_reviewed = 1 AND kc_barangay.muni_psgc = ?
	                      	AND kc_volunteers.psgc_id = kc_barangay.barangay_psgc", array($muni_psgc));
	            })
	            ->get();
		}else{
			$data = DB::table('kc_volunteers')->where('rpmo_reviewed', 1)->get();
		}
		return $data;
	}

	public static function get_beneficiary($beneficiary_id){
		$beneficiary = DB::table('kc_beneficiary')->where('beneciary_id', $beneficiary_id)->first();

		return $beneficiary;
	}

	/*public static function get_beneficiaries_by_id($psgc_id){

		$beneficiaries = function($query) use($psgc_id)
			            {
			                $query->select(DB::raw(1))
			                      ->from('kc_beneficiary')
			                      ->whereRaw("substr(kc_beneficiary.beneficiary_id, -4) = ?", $muni_psgc);
			            }

		return $beneficiaries;
	}*/

	public static function get_age($birthdate){
		$date = new DateTime($birthdate);
 		$now = new DateTime();
 		$interval = $now->diff($date);

 		return $interval->y;
	}

	public static function get_barangay($psgc_id){
		//$barangay = kc_barangay::select('barangay')->where('barangay_psgc', $psgc_id);
		$barangay = DB::table('kc_barangay')->where('barangay_psgc', $psgc_id)->pluck('barangay');

		return $barangay;
	}

	public static function get_rep_name($psgc_id){
		//$barangay = kc_barangay::select('barangay')->where('barangay_psgc', $psgc_id);
		$barangay = DB::table('kc_barangay')->where('barangay_psgc', $psgc_id)->pluck('barangay');

		return $barangay;
	}

	public static function get_province($psgc_id){
		$prov_psgc = DB::table('kc_barangay')->where('barangay_psgc', $psgc_id)->pluck('prov_psgc');
		$province = DB::table('kc_province')->where('province_psgc', $prov_psgc)->pluck('province');

		return $province;
	}

	public static function get_municipality($psgc_id){
		$muni_psgc = DB::table('kc_barangay')->where('barangay_psgc', $psgc_id)->pluck('muni_psgc');
		$municipality = DB::table('kc_municipality')->where('municipality_psgc', $muni_psgc)->pluck('municipality');

		return $municipality;
	}

	public static function get_date_conducted($activity_id){
		$date_conducted = DB::table('kc_lguactivity')->where('activity_id', $activity_id)->pluck('startdate');
		$date = date('d/m/Y',strtotime($date_conducted));

		return $date;
	}

	public static function get_region($psgc_id){
		$prov_psgc = DB::table('kc_barangay')->where('barangay_psgc', $psgc_id)->pluck('prov_psgc');
		$region_psgc = DB::table('kc_province')->where('province_psgc', $prov_psgc)->pluck('region_psgc');
		$region_name = DB::table('kc_regions')->where('psgc_id', $region_psgc)->pluck('region_name');

		return $region_name;
	}

	public static function get_office($username){
		$office = DB::table('users')->where('username', $username)->pluck('office_level');

		return $office;
	}

	public static function get_muni_psgc($username){
		$muni_psgc = DB::table('users')->where('username', $username)->pluck('psgc_id');

		return $muni_psgc;
	}

	public static function get_staff_municipality($muni_psgc){
		$municipality = DB::table('kc_municipality')->where('municipality_psgc', $muni_psgc)->pluck('municipality');

		return $municipality;
	}

	public static function get_staff_province($muni_psgc){
		$prov_psgc = DB::table('kc_municipality')->where('municipality_psgc', $muni_psgc)->pluck('province_psgc');
		$province = DB::table('kc_province')->where('province_psgc', $prov_psgc)->pluck('province');

		return $province;
	}

	public static function get_staff_region($muni_psgc){
		$prov_psgc = DB::table('kc_municipality')->where('municipality_psgc', $muni_psgc)->pluck('province_psgc');
		$region_psgc = DB::table('kc_province')->where('province_psgc', $prov_psgc)->pluck('region_psgc');
		$region_name = DB::table('kc_regions')->where('psgc_id', $region_psgc)->pluck('region_name');

		return $region_name;
	}

	public static function get_brgy_assembly_by_id($activity_id){
		$brgy_assembly = DB::table('kc_bassembly')->where('activity_id', $activity_id)->first();

		return $brgy_assembly;
	}

	public static function get_brgy_rep_by_id($activity_id){
		$brgy_rep = DB::table('kc_brgyrep')->where('activity_id', $activity_id)->get();

		return $brgy_rep;
	}


	public static function get_psa_by_id($activity_id){

		$psa = PSAnalysis::where('activity_id', $activity_id)->first();


		return $psa;
	}

	public static function get_mibf_by_id($activity_id){

		$mibf = MunicipalForum::where('activity_id', $activity_id)->first();


		return $mibf;
	}

	public static function get_mun_training_by_id($reference_no){
		$mun_training = DB::table('kc_trainings')->where('reference_no', $reference_no)->first();

		return $mun_training;
	}

	public static function get_sectors_by_id($activity_id){
		$sectors = DB::table('kc_sectorsrep')->where('activity_id', $activity_id)->lists('sector');

		return $sectors;
	}

	public static function get_sitios_by_id($activity_id){
		$sitios = DB::table('kc_sitiorep')->where('activity_id', $activity_id)->lists('sitio');

		return $sitios;
	}

	public static function get_psa_documents_by_id($activity_id){
		$documents = DB::table('kc_psadocuments')->where('activity_id', $activity_id)->get();

		return $documents;
	}

	public static function get_issues_by_id($activity_id){

		$issues = DB::table('kc_activityissues')->where('activity_id', $activity_id)->get();


		return $issues;
	}

	public static function get_psa_problems_by_id($activity_id){
		$problems = DB::table('kc_psaproject')->where('activity_id', $activity_id)->get();

		return $problems;
	}

	public static function get_sectors(){
		$sitios = DB::table('rf_sectors')->lists('sectors');

		return $sitios;
	}

	public static function get_civilstatus(){
		$status = DB::table('rf_civilstatus')->lists('name', 'name');

		return $status;
	}

	public static function get_ipgroup(){
		$group = DB::table('rf_ipgroup')->lists('name', 'name');

		return $group;
	}

	public static function get_educlevel(){
		$level = DB::table('rf_educlevel')->orderBy('educ_level_seq', 'asc')->lists('educ_level', 'educ_level');


		return $level;
	}

	public static function get_problem_cat(){
		$problem_cat = DB::table('rf_problemcat')->lists('problem_cat','problem_cat');

		return [''=>'Select Problem Category']+$problem_cat;
	}

	public static function get_solution_cat(){
		$solution_cat = DB::table('rf_probsolcat')->lists('solution_cat','solution_cat');

		return [''=>'Select Solution Category']+$solution_cat;
	}

	public static function get_BA_purpose_code(){
		$purpose = DB::table('rf_bapurpose')->lists('code');

		return $purpose;
	}

	public static function get_BA_purpose_description(){
		$purpose = DB::table('rf_bapurpose')->lists('description','code');

		return $purpose;
	}

	public static function get_cycles(){
		$cycles = DB::table('rf_cycles')->lists('name', 'name');

		return $cycles;
	}

	public static function get_training_cat(){
		$training_cat = DB::table('rf_trainingcat')->lists('description');

		return $training_cat;
	}

	public static function get_programs(){
		$programs = DB::table('kc_programs')->lists('program_id', 'program_id');

		return $programs;
	}

	public static function get_MIBFpurposes(){

		$purposes = DB::table('rf_mibfpurpose')->lists( 'purpose','code');


		return $purposes;
	}


	public static function get_brgy_rep(){
		$brgy_rep = DB::table('rf_blgupost')->lists('name', 'name');

		return $brgy_rep;
	}

	public static function get_barangay_name($psgc_id){
		if(!empty($psgc_id)){

			$barangay_name = DB::table('kc_barangay')->wherein('barangay_psgc', $psgc_id)->lists('barangay');
			return $barangay_name;
		}else{
			return [];
		}
	}

	public static function count_barangays($psgc_id){
		$count = DB::table('kc_barangay')->where('muni_psgc', $psgc_id)->count();

		return $count;
	}

	public static function count_barangays_rep($id){
		$count = DB::table('kc_brgyrep')->where('activity_id', $id)->count();

		return $count;
	}

	public static function get_municipality_name($psgc_id){
		$muni_name = DB::table('kc_municipality')->wherein('municipality_psgc', $psgc_id)->lists('municipality');

		return $muni_name;
	}


	public static function get_proposals($id, $type){
		$proposals = ProjProposal::where('activity_id', $id)
			->where('type', $type)
			->get();

		return $proposals;
	}

	public static function get_proposals_by_activity_program_id($activity_id, $program_id, $type){
		$proposals =  ProjProposal::where('activity_id', $activity_id)
			->where('program_id', $program_id)
			->where('type', $type)
			->get();

		return $proposals;
	}

	public static function get_criteria($id){

			$criteria = DB::table('kc_criteria')->where('activity_id', $id)->get();
		return $criteria;
	}

	public static function get_proposal_by_id($id){
		$proposal = DB::table('kc_projproposal')->where('project_id', $id)->first();

		return $proposal;
	}

	public static function get_participants($id){
		$participants = DB::table('kc_trnparticipants')->where('reference_no', $id)->get();

		return $participants;
	}

	public static function get_proposals_id($id){
		$proposals = DB::table('kc_projproposal')->where('activity_id', $id)->orderBy('project_id', 'desc')->pluck('project_id');

		return $proposals;
	}

	public static function get_unique_barangay_id_proposals($activity_id, $type)
	{
		$proposals = DB::table('kc_projproposal')
			->select('lead_brgy')
			->where('activity_id', $activity_id)
			->where('type', $type)
			->groupBy('lead_brgy')
			->get();
		return $proposals;
	}

	public static function get_proposals_per_barangay($brgy_id, $type)
	{
		$query = 'SUM(kc_amount) as kc_amount,
			SUM(lcc_amount) as lcc_amount,
			SUM(pamana_amount) as pamana_amount,
			COUNT(*) as total_prioritization';

		$proposals = DB::table('kc_projproposal')
			->select(DB::raw($query))
			->where('lead_brgy', $brgy_id)
			->where('type', $type)
			->first();
		return $proposals;
	}

}