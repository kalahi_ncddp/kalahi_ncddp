<?php

class Beneficiary extends Eloquent {
	
	protected $table = 'kc_beneficiary';
	
	protected $fillable = [
		'beneficiary_id',
		'lastname',
		'firstname',
		'middlename',
		'birthdate',
		'sex',
		'civil_status',
		'no_children',
		'address',
		'contact_no',
		'is_ip',
		'is_leader',
		'educ_attainment',
		'occupation',
		'created_at',
		'updated_at',
		'deleted_at',

	];


	public function memotherorg()
	{
		return $this->hasMany('MemOtherOrg', 'beneficiary_id', 'beneficiary_id');
	}

	public function memothertrn()
	{
		return $this->hasMany('MemOtherTrn', 'beneficiary_id', 'beneficiary_id');
	}


	public function participant()
	{
		return $this->belongsTo('Trnparticipants', 'beneficiary_id', 'beneficiary_id');
	}

	public function getVolunteer()
	{
		return $this->hasOne('Volunteer','beneficiary_id','beneficiary_id');
	}
	public function volunteer()
	{
		return $this->belongsTo('Volunteer', 'beneficiary_id', 'beneficiary_id');
	}


	public function is_ip()
	{
		return $this->is_ip == 1 ? 'true' : 'false';
	}

	public function is_ipleader()
	{
		return $this->is_ipleader == 1 ? 'true' : 'false';
	}

	public static function generateBeneficiaryId($data)
	{
		$psgc_id = Auth::user()->psgc_id;

		$bene = Beneficiary::create($data);
		$newsquence = sprintf("%06d", $bene->id);
		
		$bene_id = 'BN'.$psgc_id.''.$newsquence;
		Beneficiary::where('id',$bene->id)->update(['beneficiary_id'=>$bene_id]);
		//  $newsquence = DB::table('kc_beneficiary')->where('beneficiary_id','like','%'.substr($psgc_id,0,6).'%')->count();
		// // generate new benefiaciary ID based on Current Sequence of the Municipal kc table 
		// $newsquence = sprintf("%06d", $newsquence+1);
		// $beneficiary_id = 'B'.$psgc_id.''.$newsquence;
		// $query = DB::table('kc_beneficiary')->where('beneficiary_id','B144409000000474')->exists();
		// if($query)
		// {
		// 	for ($i=0; $i < 100; $i++) { 
						
					
		// 			$newsquence = intval(substr($beneficiary_id, -5)) + 1;
		// 		    $newsquence = sprintf("%06d", $newsquence);

		// 			$beneficiary_id = 'B'.$psgc_id.''.$newsquence;
		// 			$data["beneficiary_id"] = $beneficiary_id;
					
		// 			$querybene = DB::table('kc_beneficiary')->where('beneficiary_id',$beneficiary_id)->exists();
					
		// 			if(!$querybene){
		// 				Report::save_ncddp('kc_beneficiary', $data);
		// 				return $data['beneficiary_id'];
		// 				break;
		// 			}
		// 		}	
		// }else{
		// 	$data["beneficiary_id"] = $beneficiary_id;

		// 	Report::save_ncddp('kc_beneficiary', $data);
		// 	return $data['beneficiary_id'];

		// }
		// Report::save_ncddp('kc_beneficiary', $data);

		return $bene_id;
		
	}

}
