<?php

class Staff extends Eloquent {
	
	protected $table = 'kc_staff';

	protected $fillable = [
		'employee_id',
		'lastname',
		'firstname',
		'psgc_id',
		'cycle',
		'offices',
		'middlename',
		'sex',
		'birthday',
		'place_birth',
		'curr_position',
		'contract_start',
		'contract_end',
		'salary_grade',
		'currency',
		'fund_source',
		'office_id',
		'emp_status',
		'start_atproject',
		'ipc_rating',
		'ipcr_rating',
	];

 	 
}
