<?php

class SPITranche extends Eloquent {
	protected $table = 'spi_tranche';
	protected $primaryKey = 'project_id';

	protected $fillable = array(
		'project_id',
		'planned_amt_1',
		'planned_amt_2',
		'planned_amt_3',
		'planned_percent_1',
		'planned_percent_2',
		'planned_percent_3',
		'release_amt_1',
		'release_amt_2',
		'release_amt_3',
		'release_date_1',
		'release_date_2',
		'release_date_3',
		'release_percent_1',
		'release_percent_2',
		'release_percent_3',
		'kc_amount',
		'lcc_amount',
        'erfr_sp_id',
        'final_status'
	);
}