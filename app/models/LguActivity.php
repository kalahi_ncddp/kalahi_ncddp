<?php
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
class LguActivity extends Eloquent {
	protected $table = 'kc_lguactivity';

	protected $primaryKey = 'activity_id';
	protected $fillable = [
		'activity_id',
		'psgc_id',
		'activity_type',
		'activity_name',
		'startdate',
		'enddate',
        'kc_mode',
        'cycle_id',
        'program_id',
		'last_updated',
        'ac_approved'
	];

	// public function baasembly()
	// {
	// 	return $this->hasOne('BarangayAssembly','activity_id','activity_id');
	// }

    public function isApproved()
    {
        return   $this->ac_approved == '0000-00-00 00:00:00' ;
    }

	public function barangay()
	{
		return $this->hasOne('Barangay','barangay_psgc','psgc_id');
	}
	public function baasembly() 
	{
	      return $this->hasMany('BarangayAssembly');
	}


	/* Filters for ac_approved */

	public function approved_bassembly()
	{
		return $this->hasOne('BarangayAssembly', 'activity_id', 'activity_id');
	}


	public function approved_mibf()
	{
		return $this->hasOne('MunicipalForum', 'activity_id', 'activity_id');
	}

	public function approved_psa()
	{
		return $this->hasOne('PSAnalysis', 'activity_id', 'activity_id');
	}





	/* Sub-tables */
	public function sitiorep()
	{
		return $this->hasMany('SitioRep', 'activity_id', 'activity_id');
	}

	public function sectorsrep()
	{
		return $this->hasMany('SectorsRep', 'activity_id', 'activity_id');
	}

	public function activityissues()
	{
		return $this->hasMany('ActivityIssues', 'activity_id', 'activity_id');
	}

	/* Sub (PSA) */
	public function psaproject()
	{
		return $this->hasMany('PSAProjects', 'activity_id', 'activity_id');
	}

	public function psadocuments()
	{
		return $this->hasMany('PSADocuments', 'activity_id', 'activity_id');
	}


	/* Sub (MIBF) */
	public function projproposal()
	{
		return $this->hasMany('ProjProposal', 'activity_id', 'activity_id');
	}

    /**
     * @return mixed
     */
    public function activityOne()
    {
        return $this->hasOne('ActivityIssues','activity_id','activity_id');
    }
}
