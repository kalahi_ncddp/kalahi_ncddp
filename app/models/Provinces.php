<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
class Provinces extends Eloquent {
	use SoftDeletingTrait;
	protected $table = 'kc_province';
	protected $primaryKey = 'province_psgc';
	
	public function region(){
		return $this->hasOne('Regions','psgc_id','region_psgc');
	}

    public function municipality()
    {
        return $this->hasMany('Municipality','province_psgc');
    }
}
