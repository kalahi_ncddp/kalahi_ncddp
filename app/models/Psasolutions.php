<?php
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
class Psasolutions extends Eloquent
{

    protected $table = 'kc_psaprojectsolutions';
    protected $fillable = [
        'project_id',
        'activity_id',
        'solution_id',
        'solution',
        'solution_cat',
    ];

    public function problem()
    {
        return $this->hasOne('PSAProjects','project_id','project_id');
    }

    public function spi(){
//        dd($this->activity_id);
//        return $this->hasOne('PSAnalysis','activity_id','activity_id');
        $psa = PSAnalysis::where('activity_id',$this->activity_id)->first();
//        dd($this->solution_cat);
//
        $spi = SPIProfile::where('cycle_id',$psa->cycle_id)
            ->where('program_id',$psa->program_id)
            ->where('barangay_id',$psa->psgc_id)
            ->where('proj_subcategory',$this->solution_cat)
            ->get();
        return $spi;
    }
}