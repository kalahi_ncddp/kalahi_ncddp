<?php
use Nicolaslopezj\Searchable\SearchableTrait;

class SPIProfile extends Eloquent {
	use SearchableTrait;

	protected $table = 'spi_profile';
	protected $primaryKey = 'project_id';

	protected $fillable = array(
		'project_id',
		'project_name',
		'municipal_id',
		'barangay_id',
		'program_id',
		'cycle_id',
        'kc_mode',
		'is_draft',
		'mibf_refno',
		'mibf_rank',
		'no_households',
		'population',
		'percent_male',
		'percent_female',
		'proj_subcategory',
		'proj_statement',
		'proj_purpose',
		'phys_target',
		'phys_target_unit',
		'date_started',
		'date_completed',
		'gps_lattitude',
		'gps_longitude',
		'project_date',
		'cost_infra',
		'grant_infra',
		'lcc_infra',
		'cost_training',
		'grant_training',
		'lcc_training',
		'cost_women',
		'grant_women',
		'lcc_women',
		'cost_mgmnt',
		'grant_mgmnt',
		'lcc_mgmnt',
		'cost_others',
		'grant_others',
		'lcc_others',
		'set_util',
		'set_mgmnt',
		'set_linkage',
		'set_finance',
		'set_tech',
		'set_overall',
		'skilled_male',
		'skilled_female',
		'nonskilled_male',
		'nonskilled_female',
		'cost_parameter',
		'date_started',
		'gps_latitude',
		'gps_longitude',
		'male_members',
		'female_members',
		'total_families',
		'total_ip',
		'total_4p',
        'total_slp',
        'actual_male_members',
        'actual_female_members',
        'actual_total_families',
        'actual_total_ip',
        'actual_total_4p',
        'actual_total_slp',
        'target_household',
        'actual_household',
        'is_final_inspection',
        'planned_date_completed'
	);

 	protected $searchable = [
        'columns' => [
            'program_id' => 10,
            'cycle_id' => 5,
            'project_name' => 10,
            'kc_barangay.barangay' => 10,
            'proj_subcategory' => 10
        ],
        'joins' => [
            'kc_barangay' => ['kc_barangay.barangay_psgc','spi_profile.barangay_id']
        ],
    ];
	public static function get_municipality_projects($psgc_id,$query) {
		return SPIProfile::search($query)
					->where([ 'municipal_id'=>$psgc_id ])
					->where('kc_mode',Session::get('accelerated'))->orderBy('created_at','desc')->paginate(10);
	}

	public function barangay()
	{
		return $this->hasOne('Barangay','barangay_psgc','barangay_id');
	}


	public function municipality()
	{
		return $this->hasOne('Municipality','municipality_psgc','municipal_id');
	}

	public function barangays_covered() {
		return $this->hasMany('SPIBarangay','project_id','project_id');
	}

	public function ers()
	{
		return $this->hasMany('SPIERSList','project_id','project_id');
	}

	public function brgy_coverage_list() {
		$brgys = $this->barangays_covered()->get();

		$list = [];
		foreach ($brgys as $brgy) {
			array_push($list, Report::get_barangay($brgy->barangay_id));
		}

		return join(", ", $list);
	}
	public function workers()
	{
		return $this->belongsToMany('SPIWorker', 'spi_projects_workers', 'project_id', 'worker_id');
	}


	public function report()
	{
		return $this->hasOne('SPIAccomplishmentReport','acc_report_id', 'project_id');
	}

    public function tranche()
    {
        return $this->hasOne('SPITranche','project_id','project_id');
    }

    public function spcr()
    {
        return $this->hasOne('SPICompletionReport','project_id','project_id');
    }

    public function utilization()
    {
    	return $this->hasOne('SPUtilization','project_id','project_id');
    }
    public  function procurement()
    {
        return $this->hasMany('SPIProcurementMonitoring','project_id','project_id');
    }

    public function set()
    {
        return $this->hasMany('SPISET','project_id','project_id');
    }

    public function barangay_id()
    {
    	$mibf_refno = $this->mibf_refno;
    	$proj = ProjProposal::where('project_id',$mibf_refno)->first();
    	if(isset($proj))
    	{
    		return $proj->lead_brgy;
    	}

    	return $this->barangay_id;
    }

    public function accomplished()
    {
    	$project_id = $this->project_id;
    	$summary = SPISummaryPOWItem::allFromProject($project_id)->get();
    	$accomplished = 0;

		foreach($summary as $months_summary)
		{
			$accomplished += $months_summary->noncumm_actl_physical;
		}

		return $accomplished;
    }
}
