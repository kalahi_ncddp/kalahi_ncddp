<?php

class ProjCoverage extends Eloquent {
	
	protected $table = 'kc_projcoverage';

	// protected $fillable = [];

	public function projproposal()
	{
		return $this->belongsTo('ProjProposal', 'project_id', 'project_id');
	}
	

}
