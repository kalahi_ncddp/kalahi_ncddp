<?php

class EmpHistory extends Eloquent {
	
	protected $table = 'kc_emphist';

	protected $fillable = [
		'record_id',
		'employee_id',
		'position',
		'startdate',
		'enddate',
	];


	
}
