<?php
use Nicolaslopezj\Searchable\SearchableTrait;
class Volunteer extends Eloquent {
	use SearchableTrait;
	protected $table = 'kc_volunteers';	

	protected $fillable = [
		'volunteer_id',
		'beneficiary_id',
		'psgc_id',
		'program_id',
		'cycle_id',
		'date_appointed',
		'is_ppbene',
		'is_ipleader',
		'is_slpbene',
		'is_slpleader',
		'is_slpbspmc',
		'aschair_start',
		'aschair_end',
		'sector_represented',
		'is_draft',
		'kc_mode'
	];

	 protected $searchable = [
        'columns' => [
            'kc_beneficiary.firstname' => 10,
            'kc_beneficiary.lastname' => 10,
            'kc_barangay.barangay'=> 10,
            'kc_province.province'=> 10,
            'program_id' => 2,
            'cycle_id' => 5
        ],
        'joins' => [
            'kc_beneficiary' => ['kc_beneficiary.beneficiary_id','kc_volunteers.beneficiary_id'],
            'kc_barangay' => ['kc_barangay.barangay_psgc','kc_volunteers.psgc_id'],
            'kc_province' => ['kc_province.province_psgc','kc_barangay.prov_psgc'],
        ],
    ];


	public function beneficiary()
	{
		return $this->hasOne('Beneficiary', 'beneficiary_id', 'beneficiary_id');

	}

    public function volcommmem()
    {
        return $this->hasMany('VolCommMem','volunteer_id','volunteer_id');
    }

    public function getListVolunteer()
    {

    }

	public static function get_volunteers($username, $mode,$search){
		$office = Report::get_office($username);
		$psgc_id = Report::get_muni_psgc($username);
		
		if($office === "ACT"){
			$psgc_id = Report::get_psgc_id($psgc_id);
			$total_count = Volunteer::whereIn('psgc_id', $psgc_id)
			->where('kc_mode', $mode)->count();

			$volunteers = Volunteer::whereIn('psgc_id', $psgc_id)
			->search($search)
			->where('kc_mode', $mode)
			->orderBy('created_at','desc')
			->with('beneficiary')
			->paginate(10);		

			$data['volunteers'] = $volunteers;			
			$data['count'] = $total_count;
		}
		else if($office === "RPMO" or $office === "SRPMO"){
			$psgc_id = Report::get_prov_psgc_id($psgc_id);
			$data = Volunteer::whereIn('psgc_id', $psgc_id)->get();	
		} 
		else{
			$data = Volunteer::paginate(10);
		}
		
		return $data;
	}
	
	public static function get_volunteers_list(){
		$data = DB::table('KC_Beneficiary')->get();
		
		return $data;
	}
	
	public static function get_last_name($beneficiary_id){
		$name = DB::table('KC_Beneficiary')->where('beneficiary_id', $beneficiary_id)->pluck('lastname');

		return $name;
	}
	
	public static function get_first_name($beneficiary_id){
		$name = DB::table('KC_Beneficiary')->where('beneficiary_id', $beneficiary_id)->pluck('firstname');

		return $name;
	}
	
	public static function get_beneficiary_existing($lastname, $firstname, $middlename, $birthdate){
		$beneficiary = DB::table('KC_Beneficiary')->where('lastname', $lastname)
						->where('firstname', $firstname)
						->where('middlename', $middlename)
						->where('birthdate', $birthdate)
						->pluck('beneficiary_id');
			
		return $beneficiary;
	}
	
	public static function get_volunteer_existing($beneficiary_id, $psgc_id, $program_id, $cycle_id, $mode){
		$volunteer = DB::table('kc_volunteers')->where('beneficiary_id', $beneficiary_id)
						->where('psgc_id', $psgc_id)
						->where('cycle_id', $cycle_id)
						->where('program_id', $program_id)
                        ->where('kc_mode', $mode)
						->pluck('volunteer_id');
			
		return $volunteer;
	}
	
	public static function get_beneficiary_count(){
		$count = DB::table('KC_Beneficiary')->count();
		
		return $count;
	}
	
	public static function get_beneficiary_last(){
		$beneficiary = DB::table('KC_Beneficiary')->orderBy('beneficiary_id', 'desc')->pluck('beneficiary_id');
		
		return $beneficiary;
	}
	
	public static function get_volunteer_count($psgc_id){
		$count = DB::table('KC_Volunteers')->where('psgc_id', $psgc_id)->count();
		
		return $count;
	}
	
	public static function get_volunteer_last($psgc_id){
		$volunteer = DB::table('KC_Volunteers')->where('psgc_id', $psgc_id)->orderBy('volunteer_id', 'desc')->pluck('volunteer_id');
		
		return $volunteer;
	}
	
	public static function get_beneficiary_by_id($id){
		$beneficiary = DB::table('kc_beneficiary')->where('beneficiary_id', $id)->first();
		
		return $beneficiary;
	}
	
	public static function get_volunteer_by_id($id){
		$volunteer = DB::table('kc_volunteers')->where('volunteer_id', $id)->first();
		
		return $volunteer;
	}
	
	public static function get_volunteers_by_beneficiary_id($id){
		$volunteers = DB::table('KC_Volunteers')->where('beneficiary_id', $id)->lists('volunteer_id');
		
		return $volunteers;
	}
	
	
	public static function get_educ_level_list(){
		$educ_level_list = DB::table('RF_EducLevel')->lists('educ_level');
		
		return $educ_level_list;
	}

	public static function get_position_blgu(){
		$position_blgu = DB::table('rf_blgupost')->lists('name','name');
		
		return $position_blgu;
	}
	
	public static function get_volcomm_list_id(){
		$volcomm_list = DB::table('RF_VolComm')->lists('committee_id');
		
		return $volcomm_list;
	}
	
	public static function get_volcomm_list_description(){
		$volcomm_list = DB::table('RF_VolComm')->lists('description');
		
		return $volcomm_list;
	}
	
	public static function get_volpost_list(){
		$volpost_list = DB::table('RF_VolPost')->lists('position');
		
		return $volpost_list;
	}
	
	public static function get_civilstatus_list(){
		$status = DB::table('rf_civilStatus')->lists('name');
		
		return $status;
	}
	
	public static function get_memothertrn_by_id($id){
		$training_data = DB::table('KC_MemOtherTrn')->where('beneficiary_id', $id)->get();
		
		return $training_data;
	}
	
	public static function get_memotherorg_by_id($id){
		$org_data = DB::table('KC_MemOtherOrg')->where('beneficiary_id', $id)->get();
		
		return $org_data;
	}
	
	public static function get_volcommmem_by_id($id)
	{

		$comm_data = DB::table('KC_VolCommMem')->where('volunteer_id', $id)->get();
		return $comm_data;

	}
	
	public static function get_committee_description($id)
	{

		$committee = DB::table('RF_VolComm')->where('committee_id', $id)->pluck('description');

		return $committee;

	}
	
	public static function delete_beneficiary($table, $id)
	{

		Beneficiary::where('beneficiary_id', $id)->delete();	

	}	
	
	public static function update_beneficiary($table, $data)
	{
		DB::table($table)
            ->where('beneficiary_id', $data['beneficiary_id'])
            ->update($data);
	}
	
	public static function delete_volunteer($table, $id){
		DB::table($table)->where('volunteer_id', $id)->delete();
	}	
	
	public static function update_volunteer($table, $data){
		DB::table($table)
            ->where('volunteer_id', $data['volunteer_id'])
            ->update($data);
	}
	public function reference()
	{
		return $this->hasOne('References','reference_no','volunteer_id');
	}
}
