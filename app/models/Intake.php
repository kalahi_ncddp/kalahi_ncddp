<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

/*
 *GRS Intake models
 */
class Intake extends Eloquent {
    use SoftDeletingTrait;
    protected $table = 'kc_grsintake';

    protected $fillable = array(
        'reference_no',
        'psgc_id',
        'barangay_psgc',
        'program_id',
        'cycle_id',
        'is_draft',
        'date_intake',
        'intake_date',
        'grievance_form',
        'intake_level',
        'intake_date',
        'intake_officer',
        'off_desigination',
        'sender_designation',
        'filing_mode',
        'sender',
        'sender_sex',
        'sender_org',
        'sender_designation',
        'is_sender_ip',
        'sender_ipgroup',
        'sender_contact',
        'resolution_status',
        'resolution_date',
        'resolution_level',
        'concern_nature',
        'concern_category',
        'subject_complaint',
        'narrative_summ',
        'actions',
        'recommendations',
        'kc_mode'
    );

    public function grsupdates()
    {
        return $this->hasMany('Grsupdates','reference_no','reference_no');
    }
    public function municipality()
    {
        return $this->hasOne('Municipality','municipality_psgc','psgc_id');
    }

    public function barangay()
    {
        return $this->hasOne('Barangay','barangay_psgc','barangay_psgc');
    }
    public function updates()
    {
        return $this->hasMany('Grsupdates','reference_no','reference_no');
    }
    public function resolution()
    {
        if (!isset($this->concern_nature)) {
            return '';
        } else {
            return \DB::table('rf_grsresolution')->where('name', $this->concern_nature)->first()->description;

        }
    }
    public function sex()
    {
        if(!isset($this->sender_sex))
        {
            return '';
        }
        else {
            $arr = ['M' => 'Male', 'F' => 'Female', 'U' => 'unknown', 'O' => 'Organization/Group/Institution'];
            return $arr[$this->sender_sex];
        }
    }

    public function resolution_status()
    {
        if(!isset($this->resolution_status))
        {
            return '';
        }
        else
        {
            $arr = [''=>'Select Status','On Going'=>'On Going','Pending'=>'Pending','Resolved'=>'Resolved'];
            return $arr[$this->resolution_status];
        }
    }

    public function resolution_level()
    {
        if(!isset($this->resolution_level))
        {
            return '';
        }
        else
        {
            $arr = [''=>'Select Feedback',' No comment'=>'No comment','Not satisfied'=>'Not satisfied','Satisfied'=>'Satisfied','Very Satisfied'=>'Very Satisfied'];
            return $arr[$this->resolution_level];
        }
    }



}
