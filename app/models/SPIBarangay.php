<?php

class SPIBarangay extends Eloquent {
	
	protected $table = 'spi_barangays';

	protected $fillable = array(
		'project_id',
		'barangay_id'
	);

	public function spi_profile()
	{
		return $this->belongsTo('SPIProfile', 'project_id', 'project_id');
	}
}