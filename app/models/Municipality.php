<?php

class Municipality extends Eloquent {
	
	protected $table = 'kc_municipality';

	protected $primaryKey = 'municipality_psgc';

    public function munis()
    {
        return $this->hasOne('Munis','nscb_code','municipality_psgc');
    }
	public function province()
	{
		return $this->hasOne('Provinces','province_psgc','province_psgc');
	}

	public function barangay()
	{
		return $this->hasMany('Barangay','muni_psgc');
	}

    public function intake()
    {
        return $this->hasMany('Intake');
    }
	
    public function isProvince()
    {
        return $this->belongsTo('Provinces','province_psgc','province_psgc');
    }


    public function barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,$purpose)
{

    $barangay_psgc =\Municipality::where('municipality_psgc',$this->municipality_psgc)->first()->barangay->lists('barangay_psgc');

    $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
        ->where('program_id', $fundsource)
        ->where('cycle_id', $cycle)
        ->where('kc_mode',$mode)
        ->where('purpose',$purpose)
        ->whereIn('psgc_id',$barangay_psgc)->get();


    $total_household = 0;
    $no_household = 0;
    foreach ($barangay_assembly as $ba) {
        $total_household += $ba->total_household;
        $no_household += $ba->no_household;
    }
    if($no_household == 0 ){
        return '0%';
    }
    else
    {
        $percentage = ($no_household / $total_household) * 100;
        return round($percentage).'%';
    }

}

public function barangayAssembliesTotalNumberAttendees($office_level,$fundsource,$cycle,$mode,$purpose)
{

    $barangay_psgc =\Municipality::where('municipality_psgc',$this->municipality_psgc)->first()->barangay->lists('barangay_psgc');

    $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
        ->where('program_id', $fundsource)
        ->where('cycle_id', $cycle)
        ->where('kc_mode',$mode)
        ->where('purpose',$purpose)
        ->whereIn('psgc_id',$barangay_psgc)->get();


    $total_household = 0;
    $no_household = 0;
    foreach ($barangay_assembly as $ba) {
        
        $total_household += $ba->total_household;
        $no_household += $ba->no_household;
    }
    if($no_household == 0 ){
        return '0%';
    }
    else
    {
        $percentage = ($no_household / $total_household) * 100;
        return round($percentage).'%';
    }

}

    public function countBarangay()
    {
        return Barangay::where('muni_psgc',$this->municipality_psgc)->count();
    }

}
