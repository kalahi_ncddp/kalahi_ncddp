<?php
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class LguCycles extends Eloquent {
	use SoftDeletingTrait;
	protected $table = 'kc_lgucycles';
	protected $primaryKey = 'psgc_id';
	public function municipality()
	{
		return $this->hasOne('Municipality','municipality_psgc','psgc_id');
	}

	public function users() 
	{
	      return $this->hasMany('User');
	}
}
