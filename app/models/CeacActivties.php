<?php
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class CeacActivties extends Eloquent {
	use SoftDeletingTrait;
	protected $table = 'kc_ceactivities';


	protected $fillable = [
		'activity_no',
		'activity_code',
		'activity_name',
		'middlename',
		'is_regular',
		'is_accelerated',
		'is_brgy_activity',
		'is_muni_activity',
		'created_at',
		'updated_at',
		'deleted_at',
		

	];

	public function lgu()
	{
		return $this->hasOne('LguActivity','activity_name','activity_code');	
	}

	public function planMuni($psgc_id,$cycle_id,$program_id)
	{
        $mode = Session::get('accelerated');
		return CeacMuni::where([ 'psgc_id'=>$psgc_id, 'activity_code'=>$this->activity_code,'cycle_id'=>$cycle_id,'program_id'=>$program_id,'kc_mode'=>$mode])->first();
	}


	public function planBrgy($psgc_id,$cycle_id,$program_id)
	{
        $mode = Session::get('accelerated');
        return CeacBrgy::where([ 'psgc_id'=>$psgc_id, 'activity_code'=>$this->activity_code,'cycle_id'=>$cycle_id,'program_id'=>$program_id,'kc_mode'=>$mode])->first();
	}

	public function get_actual_date_brgy($psgc_id,$program ,$cycle)
	{

			return LguActivity::where('psgc_id',$psgc_id)
					->where('activity_name',$this->activity_code)
                    ->whereIn('program_id',[$program,''])
                    ->WhereIn('cycle_id',[$cycle,''])
                    ->where('kc_mode',Session::get('accelerated'))
					->first();

		


	}

}
