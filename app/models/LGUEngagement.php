<?php

class LGUEngagement extends Eloquent {
	//use SoftDeletingTrait;
	protected $table = 'kc_lguengagement';
	protected $primaryKey = 'engagement_id';
	//protected $dates = ['deleted_at'];
	
	protected $fillable = [
		
		'is_draft'
		
	];

    public function lguequipments()
    {
        return $this->hasMany('Lgumdcequipments','engagement_id','engagement_id');
    }

    public function lgudates()
    {
        return $this->hasMany('lguengagmentdates','engagement_id','engagement_id');
    }
	public static function get_lgu_engagements($username, $mode){
		$office = Report::get_office($username);
		$psgc_id = Report::get_muni_psgc($username);
		
		if($office === "ACT"){
			$data = LGUEngagement::where('municipal_psgc', $psgc_id)
			->where('kc_mode', $mode)
			->get();		
		}
		else if($office === "RPMO" or $office === "SRPMO"){
			$psgc_id = Report::get_muni_psgc_id($psgc_id);
			$data = LGUEngagement::whereIn('municipal_psgc', $psgc_id)->get();	
		} 
		else{
			$data = LGUEngagement::get();
		}
		
		return $data;
	}
	
	public static function get_lgu_engagement_existing( $psgc_id, $program_id, $cycle_id , $mode ){
		$lguengagement = LGUEngagement::where('municipal_psgc', $psgc_id)
						->where('cycle_id', $cycle_id)
						->where('program_id', $program_id)
                        ->where('kc_mode',$mode)
						->pluck('engagement_id');
			
		return $lguengagement;
	}
	
	public static function get_lgu_engagement_by_psgc($psgc_id){
		$lguengagement = LGUEngagement::where('municipal_psgc', $psgc_id)->orderBy('engagement_id', 'desc')->pluck('engagement_id');
		
		return $lguengagement;
	}
	
	public static function get_lgu_engagement_other_data($table, $engagement_id){
		$data = DB::table($table)->where('engagement_id', $engagement_id)->get();
		
		return $data;
	}
	
	public static function save_lgu_engagement_other_data($table, $data){
		DB::table($table)->insert($data);
	}	
	
	public static function delete_lgu_engagement_other_data($table, $id){
		DB::table($table)->where('engagement_id', $id)->delete();
	}
}
