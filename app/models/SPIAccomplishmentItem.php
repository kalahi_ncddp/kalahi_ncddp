<?php
class SPIAccomplishmentItem extends Eloquent
{
	protected $table         = 'spi_accomplishment_items';
	protected $primaryKey    = 'id';
	public    $incrementing  = false;
	protected $fillable      = array('acc_report_id', 'item_no', 'item_id', 'unit_cost', 'quantity', 'p_weight');

	public function item()
	{
		return $this->hasOne('SPIWorkItem','item_no','item_no');
	}

	public function accomplishment_report()
    {
        return $this->belongsTo('SPIAccomplishmentReport', 'acc_report_id', 'acc_report_id');
	}
} //--end of SPI_OCComm