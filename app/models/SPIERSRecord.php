<?php

class SPIERSRecord extends Eloquent
{
	protected $table         = 'spi_ersrecords';
    protected $primaryKey    = 'id';
    public    $incrementing  = false;
	protected $fillable      = array('current_work','record_id','worker_id', 'rate_hour', 'rate_day',
                                     'work_hours', 'work_days', 'plan_cash', 'plan_lcc',
                                     'actual_cash', 'actual_lcc');

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model)
            {
                $model->{$model->getKeyName()} = (string)$model->generateNewId();
            });
    }

    public function generateNewId()
    {
        $key      = '';
        $key      = $this->record_id . '-' . $this->worker_id;
        return $key;
    }

    public function worker()
    {
        return $this->belongsTo('SPIWorker', 'worker_id', 'worker_id');
    }

    public function ers_list()
    {
        return $this->belongsTo('SPIERSList', 'record_id', 'record_id');
    }

    public function scopeAllFromERSList($query, $record_id)
    {
        return $query
            ->select('spi_workers.worker_id', 'spi_workers.firstname', 'spi_workers.middlename', 'spi_workers.lastname',
                     'spi_workers.nature_work', 'spi_workers.sex', 'spi_workers.age', 'spi_workers.birthday', 
                     'spi_ersrecords.id','spi_ersrecords.current_work', 'spi_ersrecords.rate_hour', 'spi_ersrecords.rate_day', 'spi_ersrecords.work_hours',
                     'spi_ersrecords.work_days', 'spi_ersrecords.plan_cash', 'spi_ersrecords.actual_cash', 
                     'spi_ersrecords.plan_lcc', 'spi_ersrecords.actual_lcc')
            ->join('spi_workers', 'spi_workers.worker_id', '=', 'spi_ersrecords.worker_id')
            ->where('spi_ersrecords.record_id', '=', $record_id); 
    }
} //--end of SPI_OCComm	