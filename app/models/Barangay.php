<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/

class Barangay extends Eloquent {
	use SoftDeletingTrait;
	protected $table = 'kc_barangay';
	
	protected $primaryKey = 'barangay_psgc';

	public function municipality()
	{
		return $this->hasOne('Municipality','municipality_psgc','muni_psgc');
	}

	public function province()
	{
		return $this->hasOne('Provinces','province_psgc','prov_psgc');
	}

	public function municipalities() 
	{
	  return $this->belongsTo('Municipality', 'municipality_psgc','muni_psgc');
	}

	public function getTrainingParticipantCount($reference_no)
	{
		return Trnparticipants::where('psgc_id', $this->barangay_psgc)->where('reference_no',$reference_no)->get()->count();
	}

    public function barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,$purpose)
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
            ->where('program_id', $fundsource)
            ->where('cycle_id', $cycle)
            ->where('kc_mode',$mode)
            ->where('purpose',$purpose)
            ->whereIn('psgc_id',[$barangay_psgc])->get();

        $total_household = 0;
        $no_household = 0;
        foreach ($barangay_assembly as $ba) {
            $total_household += $ba->total_household;
            $no_household += $ba->no_household;
        }
        if($no_household == 0 ){
            return '0%';
        }
        else
        {
            $percentage = ($no_household / $total_household) * 100;
            return round($percentage).'%';
        }

    }


     public function barangayAssembliesTotalMaleNumberAttendees($office_level,$fundsource,$cycle,$mode,$purpose)
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
            ->where('program_id', $fundsource)
            ->where('cycle_id', $cycle)
            ->where('kc_mode',$mode)
            ->where('purpose',$purpose)
            ->whereIn('psgc_id',[$barangay_psgc])->get();

        $no_atnmale = 0;
      
        foreach ($barangay_assembly as $ba) {
            $no_atnmale += $ba->no_atnmale;
            
        }
        if($no_atnmale == 0 ){
            return '0';
        }
        
        else
        {
            $male = ($no_atnmale);
            return round($male).'';
            
            // dd($male);
        }

    }

    public function barangayAssembliesTotalMaleNumberAttendeesPercent($office_level,$fundsource,$cycle,$mode,$purpose)
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
            ->where('program_id', $fundsource)
            ->where('cycle_id', $cycle)
            ->where('kc_mode',$mode)
            ->where('purpose',$purpose)
            ->whereIn('psgc_id',[$barangay_psgc])->get();

        $no_atnmale = 0;
        $no_ipmale = 0;
        foreach ($barangay_assembly as $ba) {
            $no_atnmale += $ba->no_atnmale;
            $no_ipmale += $ba->no_ipmale;
        }
        if($no_ipmale == 0 ){
            return '0%';
        }
        else
        {
            $percentage = ($no_ipmale / $no_atnmale) * 100;
            return round($percentage).'%';
        }

    }

     public function barangayAssembliesTotalFemaleNumberAttendees($office_level,$fundsource,$cycle,$mode,$purpose)
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
            ->where('program_id', $fundsource)
            ->where('cycle_id', $cycle)
            ->where('kc_mode',$mode)
            ->where('purpose',$purpose)
            ->whereIn('psgc_id',[$barangay_psgc])->get();

        $no_atnfemale = 0;
      
        foreach ($barangay_assembly as $ba) {
            $no_atnfemale += $ba->no_atnfemale;
            
        }
        if($no_atnfemale == 0 ){
            return '0';
        }
        
        else
        {
            $female = ($no_atnfemale);
            return round($female).'';
            
            // dd($male);
        }

    }

    public function barangayAssembliesTotalFeMaleNumberAttendeesPercent($office_level,$fundsource,$cycle,$mode,$purpose)
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
            ->where('program_id', $fundsource)
            ->where('cycle_id', $cycle)
            ->where('kc_mode',$mode)
            ->where('purpose',$purpose)
            ->whereIn('psgc_id',[$barangay_psgc])->get();

        $no_atnfemale = 0;
        $no_ipfemale = 0;

        // dd($barangay_assembly);
        foreach ($barangay_assembly as $ba) {
            $no_atnfemale += $ba->no_atnfemale;
            $no_ipfemale += $ba->no_ipfemale;
        }

        if($no_ipfemale == 0 ){
            return '0%';
        }
        else
        {
            $percentage = ($no_ipfemale / $no_atnfemale) * 100;
            return round($percentage).'%';
        }

    }

public function volunteer_number_female($office_level,$fundsource,$cycle,$mode,$sex = 'F') 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $ceac_muni = \Volunteer::where('is_draft', '=', 0)
                            ->where('program_id', $fundsource)
                            ->where('cycle_id', $cycle)
                            // ->where('kc_mode',$mode)
                            ->whereIn('psgc_id',[$barangay_psgc])
                            ->get();
                            
        $female_sex = 0;
        // $no_ipfemale = 0;
        
        foreach ($ceac_muni as $volunteer) {
            // $beneficiary = \Beneficiary::where('beneficiary_id',$beneficiary->beneficiary_id)->first();
            $female_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex',$sex)->exists() ? 1 : 0;
            // $female_sex = $beneficiary->sex;
        }
    
        if($female_sex == 0 ){
            return '0';
        }
        else
        {
            $female_sex_total = ($female_sex);
            return round($female_sex_total);
        }

    }


public function volunteer_number_male($office_level,$fundsource,$cycle,$mode,$sex = 'M') 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $ceac_muni = \Volunteer::where('is_draft', '=', 0)
                            ->where('program_id', $fundsource)
                            ->where('cycle_id', $cycle)
                            // ->where('kc_mode',$mode)
                            ->whereIn('psgc_id',[$barangay_psgc])
                            ->get();
                          
        $male_sex = 0;
        // $no_ipfemale = 0;
        
        foreach ($ceac_muni as $volunteer) {
            // $beneficiary = \Beneficiary::where('beneficiary_id',$beneficiary->beneficiary_id)->first();
            $male_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex',$sex)->exists() ? 1 : 0;
            // $female_sex = $beneficiary->sex;
        }
    
        if($male_sex == 0 ){
            return '0';
        }
        else
        {
            $male_sex_total = ($male_sex);
            return round($male_sex_total);
        }

    }


public function volunteer_number_female_percentage($office_level,$fundsource,$cycle,$mode,$sex = 'F') 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $ceac_muni = \Volunteer::where('is_draft', '=', 0)
                            ->where('program_id', $fundsource)
                            ->where('cycle_id', $cycle)
                            // ->where('kc_mode',$mode)
                            ->whereIn('psgc_id',[$barangay_psgc])
                            ->get();
                          
        $female_sex = 0;
        $male_sex = 0;
        // $no_ipfemale = 0;
        
        foreach ($ceac_muni as $volunteer) {
            // $beneficiary = \Beneficiary::where('beneficiary_id',$beneficiary->beneficiary_id)->first();
            $male_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','M')->exists() ? 1 : 0;
            $female_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','F')->exists() ? 1 : 0;
           
        }
    
        if($female_sex == 0 ){
            return '0';
        }
        else
        {
          
            $male_female_sex_total = ($male_sex + $female_sex);
            $female_total_percentage = ($female_sex / $male_female_sex_total) * 100;

            return round($female_total_percentage).'%';
            
        }

    }



public function volunteer_number_male_percentage($office_level,$fundsource,$cycle,$mode) 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $ceac_muni = \Volunteer::where('is_draft', '=', 0)
                            ->where('program_id', $fundsource)
                            ->where('cycle_id', $cycle)
                            // ->where('kc_mode',$mode)
                            ->whereIn('psgc_id',[$barangay_psgc])
                            ->get();
                            
        $male_sex = 0;
        $female_sex = 0;
        // $no_ipfemale = 0;
        
        foreach ($ceac_muni as $volunteer) {
            // $beneficiary = \Beneficiary::where('beneficiary_id',$beneficiary->beneficiary_id)->first();
            $male_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','M')->exists() ? 1 : 0;
            $female_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','F')->exists() ? 1 : 0;
           
        }
    
        if($male_sex == 0 ){
            return '0';
        }
        else
        {
          
            $male_female_sex_total = ($male_sex + $female_sex);
            $male_total_percentage = ($male_sex / $male_female_sex_total) * 100;

            return round($male_total_percentage).'%';
            
        }

    }




public function volunteer_number_male_female_total($office_level,$fundsource,$cycle,$mode) 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $ceac_muni = \Volunteer::where('is_draft', '=', 0)
                            ->where('program_id', $fundsource)
                            ->where('cycle_id', $cycle)
                            // ->where('kc_mode',$mode)
                            ->whereIn('psgc_id',[$barangay_psgc])
                            ->get();
                            
        $male_sex = 0;
        $female_sex  = 0;
        // $no_ipfemale = 0;
        
        foreach ($ceac_muni as $volunteer) {
            // $beneficiary = \Beneficiary::where('beneficiary_id',$beneficiary->beneficiary_id)->first();
            $male_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','M')->exists() ? 1 : 0;
           
           $female_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','F')->exists() ? 1 : 0;
           

        }
    
        if($male_sex == 0 ){
            return '0';
        } 

         
        
        else
        {
            $male_female_sex_total = ($male_sex + $female_sex);
            return round($male_female_sex_total);
            
        }

    }




// end of ceac_muni



    public function volunteer_number_female_leader($office_level,$fundsource,$cycle,$mode,$sex = 'F') 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $ceac_muni = \Volunteer::where('is_draft', '=', 0)
                            ->where('program_id', $fundsource)
                            ->where('cycle_id', $cycle)
                            ->where('is_ppleader',1)
                            ->whereIn('psgc_id',[$barangay_psgc])
                            ->get();
                            
        $female_sex = 0;
        // $no_ipfemale = 0;
        
        foreach ($ceac_muni as $volunteer) {
            // $beneficiary = \Beneficiary::where('beneficiary_id',$beneficiary->beneficiary_id)->first();
            $female_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex',$sex)->exists() ? 1 : 0;
            // $female_sex = $beneficiary->sex;
        }
    
        if($female_sex == 0 ){
            return '0';
        }
        else
        {
            $female_sex_total = ($female_sex);
            return round($female_sex_total);
        }

    }


public function volunteer_number_male_leader($office_level,$fundsource,$cycle,$mode,$sex = 'M') 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $ceac_muni = \Volunteer::where('is_draft', '=', 0)
                            ->where('program_id', $fundsource)
                            ->where('cycle_id', $cycle)
                            ->where('is_ppleader',1)
                            ->whereIn('psgc_id',[$barangay_psgc])
                            ->get();
                          
        $male_sex = 0;
        // $no_ipfemale = 0;
        
        foreach ($ceac_muni as $volunteer) {
            // $beneficiary = \Beneficiary::where('beneficiary_id',$beneficiary->beneficiary_id)->first();
            $male_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex',$sex)->exists() ? 1 : 0;
            // $female_sex = $beneficiary->sex;
        }
    
        if($male_sex == 0 ){
            return '0';
        }
        else
        {
            $male_sex_total = ($male_sex);
            return round($male_sex_total);
        }

    }


public function volunteer_number_female_percentage_leader($office_level,$fundsource,$cycle,$mode,$sex = 'F') 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $ceac_muni = \Volunteer::where('is_draft', '=', 0)
                            ->where('program_id', $fundsource)
                            ->where('cycle_id', $cycle)
                            ->where('is_ppleader',1)
                            ->whereIn('psgc_id',[$barangay_psgc])
                            ->get();
                          
        $female_sex = 0;
        $male_sex = 0;
        // $no_ipfemale = 0;
        
        foreach ($ceac_muni as $volunteer) {
            // $beneficiary = \Beneficiary::where('beneficiary_id',$beneficiary->beneficiary_id)->first();
            $male_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','M')->exists() ? 1 : 0;
            $female_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','F')->exists() ? 1 : 0;
           
        }
    
        if($female_sex == 0 ){
            return '0';
        }
        else
        {
          
            $male_female_sex_total = ($male_sex + $female_sex);
            $female_total_percentage = ($female_sex / $male_female_sex_total) * 100;

            return round($female_total_percentage).'%';
            
        }

    }



public function volunteer_number_male_percentage_leader($office_level,$fundsource,$cycle,$mode) 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $ceac_muni = \Volunteer::where('is_draft', '=', 0)
                            ->where('program_id', $fundsource)
                            ->where('cycle_id', $cycle)
                            ->where('is_ppleader',1)
                            ->whereIn('psgc_id',[$barangay_psgc])
                            ->get();
                            
        $male_sex = 0;
        $female_sex = 0;
        // $no_ipfemale = 0;
        
        foreach ($ceac_muni as $volunteer) {
            // $beneficiary = \Beneficiary::where('beneficiary_id',$beneficiary->beneficiary_id)->first();
            $male_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','M')->exists() ? 1 : 0;
            $female_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','F')->exists() ? 1 : 0;
           
        }
    
        if($male_sex == 0 ){
            return '0';
        }
        else
        {   
          
            $male_female_sex_total = ($male_sex + $female_sex);
            $male_total_percentage = ($male_sex / $male_female_sex_total) * 100;

            return round($male_total_percentage).'%';
            
        }

    }




public function volunteer_number_male_female_total_leader($office_level,$fundsource,$cycle,$mode) 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $ceac_muni = \Volunteer::where('is_draft', '=', 0)
                            ->where('is_ppleader',1)
                            ->where('program_id', $fundsource)
                            ->where('cycle_id', $cycle)
                           
                            ->whereIn('psgc_id',[$barangay_psgc])
                            ->get();
                           
        $male_sex = 0;
        $female_sex  = 0;
        // $no_ipfemale = 0;
        
        foreach ($ceac_muni as $volunteer) {
            // $beneficiary = \Beneficiary::where('beneficiary_id',$beneficiary->beneficiary_id)->first();
            $male_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','M')->exists() ? 1 : 0;
           
           $female_sex += \Beneficiary::where('beneficiary_id',$volunteer->beneficiary_id)->where('sex','F')->exists() ? 1 : 0;
           

        }
    
        

        if($female_sex == 0 ){
            return '0';
        }
        
        else
        {
            $male_female_sex_total = ($male_sex + $female_sex);
            return round($male_female_sex_total);
            

        }


        

    }





    



 public function ceac_municipality($office_level,$fundsource,$cycle,$mode,$activity_code)
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $ceac_municipalities = \CeacMuni::where('program_id',$fundsource)
            ->where('cycle_id', $cycle)
            ->where('activity_code',$activity_code)
            ->whereIn('psgc_id',[$barangay_psgc])->get();
// dd($ceac_municipalities);
        $startdate = 0;
      
        foreach ($ceac_municipalities as $cm) {
            $startdate += $cm->startdate;
            
        }


        if($startdate == 0 ){
            return 'date';
        }
        
        else
        {
            $start_date = ($startdate);
            return ($start_date);
            
            // dd($startdate);
        }

    }





public function barangayActivityBA1($office_level,$fundsource,$cycle,$mode,$purpose) 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
            ->where('program_id', $fundsource)
            ->where('cycle_id', $cycle)
            ->where('kc_mode',$mode)
            ->where('purpose',$purpose)
            ->whereIn('psgc_id',[$barangay_psgc])->get();

        $purpose = 0;
        
       foreach ($barangay_assembly as $ba) {

            
             $purpose += $ba->where('purpose','1st BA')->exists() ? 1 : 0;
           
        }
    
        if($purpose == 0 ){
            return '0';
        }
        else
        {
            $number_ba = ($purpose);
            return round($number_ba).'';
        }

    }


public function barangayActivityBA2($office_level,$fundsource,$cycle,$mode,$purpose) 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
            ->where('program_id', $fundsource)
            ->where('cycle_id', $cycle)
            ->where('kc_mode',$mode)
            ->where('purpose',$purpose)
            ->whereIn('psgc_id',[$barangay_psgc])->get();

        $purpose = 0;
        
       foreach ($barangay_assembly as $ba) {

            
             $purpose += $ba->where('purpose','2nd BA')->exists() ? 1 : 0;
           
        }
    
        if($purpose == 0 ){
            return '0';
        }
        else
        {
            $number_ba = ($purpose);
            return round($number_ba).'';
        }

    }


public function barangayActivityBA3($office_level,$fundsource,$cycle,$mode,$purpose) 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
            ->where('program_id', $fundsource)
            ->where('cycle_id', $cycle)
            ->where('kc_mode',$mode)
            ->where('purpose',$purpose)
            ->whereIn('psgc_id',[$barangay_psgc])->get();

        $purpose = 0;
        
       foreach ($barangay_assembly as $ba) {

            
             $purpose += $ba->where('purpose','3rd BA')->exists() ? 1 : 0;
           
        }
    
        if($purpose == 0 ){
            return '0';
        }
        else
        {
            $number_ba = ($purpose);
            return round($number_ba).'';
        }

    }



public function barangayActivityBA4($office_level,$fundsource,$cycle,$mode,$purpose) 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
            ->where('program_id', $fundsource)
            ->where('cycle_id', $cycle)
            ->where('kc_mode',$mode)
            ->where('purpose',$purpose)
            ->whereIn('psgc_id',[$barangay_psgc])->get();

        $purpose = 0;
        
       foreach ($barangay_assembly as $ba) {

            
             $purpose += $ba->where('purpose','4th BA')->exists() ? 1 : 0;
           
        }
    
        if($purpose == 0 ){
            return '0';
        }
        else
        {
            $number_ba = ($purpose);
            return round($number_ba).'';
        }

    }


public function barangayActivityBA5($office_level,$fundsource,$cycle,$mode,$purpose) 
    {

        $barangay_psgc = $this->barangay_psgc;
//        dd($barangay_psgc);
        $barangay_assembly = \BarangayAssembly::where('is_draft', '=', 0)
            ->where('program_id', $fundsource)
            ->where('cycle_id', $cycle)
            ->where('kc_mode',$mode)
            ->where('purpose',$purpose)
            ->whereIn('psgc_id',[$barangay_psgc])->get();

        $purpose = 0;
        
       foreach ($barangay_assembly as $ba) {

            
             $purpose += $ba->where('purpose','5th BA')->exists() ? 1 : 0;
           
        }
    
        if($purpose == 0 ){
            return '0';
        }
        else
        {
            $number_ba = ($purpose);
            return round($number_ba).'';
        }

    }


}
