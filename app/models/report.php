<?php

class Report extends Eloquent {

  public static function get_prov_psgc($muni_psgc){
    $prov_psgc = DB::table('kc_barangay')->where('muni_psgc', $muni_psgc)->pluck('prov_psgc');

    return $prov_psgc;
  }

  public static function get_psgc_id($muni_psgc){
    $psgc_id = DB::table('kc_barangay')->where('muni_psgc', $muni_psgc)->lists('barangay_psgc');

    return $psgc_id;
  }

  public static function get_prov_psgc_id($prov_psgc){
    $psgc_id = DB::table('kc_barangay')->where('prov_psgc', $prov_psgc)->lists('barangay_psgc');

    return $psgc_id;
  }

  public static function get_all_brgy_psgc_id(){
    $psgc_id = DB::table('kc_barangay')->lists('barangay_psgc');

    return $psgc_id;
  }

  public static function get_muni_psgc_id($muni_psgc){
    $prov_psgc = DB::table('kc_municipality')->where('municipality_psgc', $muni_psgc)->pluck('province_psgc');
    $psgc_id = DB::table('kc_municipality')->where('province_psgc', $prov_psgc)->lists('municipality_psgc');

    return $psgc_id;
  }

  public static function get_mibf($username, $mode){
    $mode = Session::get('accelerated');
    $mibf = MunicipalForum::where('psgc_id',$username)->where('kc_mode',$mode)->get();


    return $mibf;
  }

  public static function get_all_mibf_priority_proposals_act($username, $type, $cycle){
    $data = [];
    $count  = 0;
    $total_pamana_fund = 0;

    //get all the programs(fund source)
    $list_of_programs = NCDDP::get_programs();
    foreach($list_of_programs as $programs){
      $list_mibf_activity = MunicipalForum::where('psgc_id', $username)
      ->where('program_id', $programs)
      ->where('cycle_id', $cycle)
      ->select('activity_id','program_id')
      ->get()
      ->toArray();
      //get all the mibf under that programs(fund source)
      foreach($list_mibf_activity as $activity){
        $list_of_proposals =  NCDDP::get_unique_barangay_id_proposals($activity['activity_id'],$type);
        //get all the priority proposals

        foreach($list_of_proposals as $proposals){

          $data[$activity['program_id']]['barangay'][$count]['barangay_name'] = NCDDP::get_barangay($proposals->lead_brgy);

          $barangay_proposals = NCDDP::get_proposals_per_barangay($proposals->lead_brgy, $type) ;

          $data[$activity['program_id']]['barangay'][$count]['total_kc_fund'] =
            $barangay_proposals->kc_amount;

          $data[$activity['program_id']]['barangay'][$count]['total_lcc_fund'] =
            $barangay_proposals->lcc_amount;

          $data[$activity['program_id']]['barangay'][$count]['total_pamana_fund'] =
            $barangay_proposals->pamana_amount;

          $data[$activity['program_id']]['barangay'][$count]['total_prioritization'] =
            $barangay_proposals->total_prioritization;

          $data[$activity['program_id']]['barangay'][$count]['total_amount'] =
            $barangay_proposals->kc_amount  +
            $barangay_proposals->lcc_amount +
            $barangay_proposals->pamana_amount;


          $count++;
        }
      }
    }
    return $data;
  }


  public static function get_comm_trainings($username){
    $comm_trainings = DB::table('kc_trainings')->get();

    return $comm_trainings;
  }

  public static function get_mun_trainings($username){
    $mun_trainings = DB::table('kc_trainings')->get();

    return $mun_trainings;
  }

  public static function get_brgy_assembly($username,$mode) {
    $office = Report::get_office($username);
    $muni_psgc = Report::get_muni_psgc($username);
    $prov_psgc = Report::get_prov_psgc($muni_psgc);

    if($office == "ACT"){
      $psgc_id = Report::get_psgc_id($muni_psgc);
      try {

     $data = BarangayAssembly::whereIn('psgc_id', $psgc_id)
                  ->where('kc_mode',$mode)->get(); 
      }catch(Exception $e){
        $data = array();
      }
     
    }else if($office == "SRPMO"){
      $psgc_id = Report::get_prov_psgc_id($prov_psgc);
      $data = BarangayAssembly::whereExists(function($query) use($muni_psgc)
              {
                  $query->select(DB::raw(1))
                        ->from('kc_barangay')
                        ->whereRaw("kc_bassembly.act_reviewed = 1 AND kc_barangay.muni_psgc = ?
                          AND kc_bassembly.psgc_id = kc_barangay.barangay_psgc", array($muni_psgc));
              })
              ->get();
    }else if($office == "RPMO"){
      $psgc_id = Report::get_prov_psgc_id($prov_psgc);
      $data = BarangayAssembly::whereExists(function($query) use($muni_psgc)
              {
                  $query->select(DB::raw(1))
                        ->from('kc_barangay')
                        ->whereRaw("kc_bassembly.srpmo_reviewed = 1 AND kc_barangay.muni_psgc = ?
                          AND kc_bassembly.psgc_id = kc_barangay.barangay_psgc", array($muni_psgc));
              })
              ->get();
    }else{
      $data = BarangayAssembly::where('rpmo_reviewed', 1)->get();
    }

    return $data;
  }

  public static function get_brgy_assembly_by_user($username) {
    $office = Report::get_office($username);
    $psgc_id = Report::get_muni_psgc($username);

    if($office === "ACT"){
      $psgc_id = Report::get_psgc_id($psgc_id);
      $data = DB::table('kc_bassembly')->whereIn('psgc_id', $psgc_id)->get();
    }
    else if($office === "RPMO" or $office === "SRPMO"){
      $psgc_id = Report::get_prov_psgc_id($psgc_id);
      $data = DB::table('kc_bassembly')->whereIn('psgc_id', $psgc_id)->get();
    }
    else{
      $data = DB::table('kc_bassembly')->get();
    }

    return $data;
  }

  public static function get_psa($username, $mode){
    $office = Report::get_office($username);
    $psgc_id = Report::get_muni_psgc($username);
    if($office === "ACT"){
      $psgc_id = Report::get_psgc_id($psgc_id);

      $data = PSAnalysis::whereIn('psgc_id', $psgc_id)->where('kc_mode',$mode)->get();
    }
    else if($office === "RPMO" or $office === "SRPMO"){
      $psgc_id = Report::get_prov_psgc_id($psgc_id);
      $data = PSAnalysis::whereIn('psgc_id', $psgc_id)->get();
    }
    else{
      $data = PSAnalysis::get();
    }

    return $data;
  }

  public static function get_volunteers($username) {
    $office = Report::get_office($username);
    $muni_psgc = Report::get_muni_psgc($username);
    $prov_psgc = Report::get_prov_psgc($muni_psgc);

    if($office == "ACT"){
      $psgc_id = Report::get_psgc_id($muni_psgc);
      $data = DB::table('kc_volunteers')->whereIn('psgc_id', $psgc_id)->get();
    }else if($office == "SRPMO"){
      $psgc_id = Report::get_prov_psgc_id($prov_psgc);
      $data = DB::table('kc_volunteers')
              ->whereExists(function($query) use($muni_psgc)
              {
                  $query->select(DB::raw(1))
                        ->from('kc_barangay')
                        ->whereRaw("kc_volunteers.act_reviewed = 1 AND kc_barangay.muni_psgc = ?
                          AND kc_volunteers.psgc_id = kc_barangay.barangay_psgc", array($muni_psgc));
              })
              ->get();
    }else if($office == "RPMO"){
      $psgc_id = Report::get_prov_psgc_id($prov_psgc);
      $data = DB::table('kc_volunteers')
              ->whereExists(function($query) use($muni_psgc)
              {
                  $query->select(DB::raw(1))
                        ->from('kc_barangay')
                        ->whereRaw("kc_volunteers.srpmo_reviewed = 1 AND kc_barangay.muni_psgc = ?
                          AND kc_volunteers.psgc_id = kc_barangay.barangay_psgc", array($muni_psgc));
              })
              ->get();
    }else{
      $data = DB::table('kc_volunteers')->where('rpmo_reviewed', 1)->get();
    }
    return $data;
  }

  public static function get_beneficiary($beneficiary_id){
    $beneficiary = DB::table('KC_Beneficiary')->where('beneciary_id', $beneficiary_id)->first();

    return $beneficiary;
  }

  public static function get_age($birthdate){
    $date = new DateTime($birthdate);
    $now = new DateTime();
    $interval = $now->diff($date);

    return $interval->y;
  }

  public static function get_barangay($psgc_id){
    //$barangay = kc_barangay::select('barangay')->where('barangay_psgc', $psgc_id);
    $barangay = DB::table('kc_barangay')->where('barangay_psgc', $psgc_id)->pluck('barangay');

    return $barangay;
  }

  public static function get_proj_coverage(){
    $coverage = DB::table('kc_projcoverage')->get();

    return $coverage;
  }

  public static function get_proj_coverage_by_id($id){
    $coverage = DB::table('kc_projcoverage')->where('project_id', $id)->get();

    return $coverage;
  }

  public static function get_password(){
    //$barangay = kc_barangay::select('barangay')->where('barangay_psgc', $psgc_id);
    $password = DB::table('users');

    return $password;
  }

  public static function get_province($psgc_id){
    // $prov_psgc = DB::table('kc_barangay')->where('barangay_psgc', $psgc_id)->pluck('prov_psgc');
    // $province = DB::table('kc_province')->where('province_psgc', $prov_psgc)->pluck('province');
    $province = Municipality::find($psgc_id)->province->province;

    return $province;
  }

  public static function get_municipality($psgc_id){
    // $muni_psgc = DB::table('kc_barangay')->where('barangay_psgc', $psgc_id)->pluck('muni_psgc');
    // $municipality = DB::table('kc_municipality')->where('municipality_psgc', $muni_psgc)->pluck('municipality');
    
    $municipality = Municipality::find($psgc_id)->municipality;
    return $municipality;
  }

  public static function get_province_by_brgy_id($psgc_id)
  {
    $province = Barangay::find($psgc_id)->province->province;
    return $province;
  }
  public static function get_municipality_by_brgy_id($psgc_id){
    $municipality = Barangay::find($psgc_id)->municipality->municipality;
    return $municipality;
  }

  public static function get_date_conducted($activity_id){
    $date_conducted = DB::table('kc_lguactivity')->where('activity_id', $activity_id)->pluck('startdate');
    $date = $date_conducted ? date('m/d/Y',strtotime($date_conducted)) : date('m/d/Y');

    return $date;
  }

  public static function change_date_format($date_input){
    $date = $date_input ? date('m/d/Y',strtotime($date_input)) : date('m/d/Y');

    return $date;
  }


  public static function get_end_date($activity_id){
    $date_conducted = DB::table('kc_lguactivity')->where('activity_id', $activity_id)->pluck('enddate');
    $date_conducted = $date_conducted ? date('m/d/Y',strtotime($date_conducted)) : date('m/d/Y');
    return $date_conducted;
  }

  public static function get_region($psgc_id)
  {

    // $prov_psgc = DB::table('kc_barangay')->where('barangay_psgc', $psgc_id)->pluck('prov_psgc');
    // $region_psgc = DB::table('kc_province')->where('province_psgc', $prov_psgc)->pluck('region_psgc');
    // $region_name = DB::table('kc_regions')->where('psgc_id', $region_psgc)->pluck('region_name');

    $region_name = Municipality::find($psgc_id)->province->region->region_name;
    return $region_name;
  }

  public static function get_region_by_brgy_id($psgc_id)
  {
    $region = Barangay::find($psgc_id)->province->region->region_name;
    return $region;
  }

  public static function get_office($username){
    $office = DB::table('users')->where('username', $username)->pluck('office_level');

    return $office;
  }

  public static function get_muni_psgc($username){
    $muni_psgc = DB::table('users')->where('username', $username)->pluck('psgc_id');

    return $muni_psgc;
  }

  public static function get_staff_municipality($muni_psgc){
    $municipality = DB::table('kc_municipality')->where('municipality_psgc', $muni_psgc)->pluck('municipality');

    return $municipality;
  }

  public static function get_staff_province($muni_psgc){
    $prov_psgc = DB::table('kc_municipality')->where('municipality_psgc', $muni_psgc)->pluck('province_psgc');
    $province = DB::table('kc_province')->where('province_psgc', $prov_psgc)->pluck('province');

    return $province;
  }

  public static function get_staff_region($muni_psgc){
    $prov_psgc = DB::table('kc_municipality')->where('municipality_psgc', $muni_psgc)->pluck('province_psgc');
    $region_psgc = DB::table('kc_province')->where('province_psgc', $prov_psgc)->pluck('region_psgc');
    $region_name = DB::table('kc_regions')->where('psgc_id', $region_psgc)->pluck('region_name');

    return $region_name;
  }


  public static function get_brgy_assembly_existing($psgc_id, $program, $cycle, $purpose,$mode){

    $brgy_assembly = DB::table('kc_bassembly')->where('psgc_id', $psgc_id)
            ->where('program_id', $program)
            ->where('cycle_id', $cycle)
            ->where('purpose', $purpose)
            ->where('kc_mode',$mode)
            ->pluck('activity_id');

    return $brgy_assembly;
  }

  public static function get_psa_existing($psgc_id, $program, $cycle ,$mode){
    $psa = DB::table('kc_psanalysis')->where('psgc_id', $psgc_id)
            ->where('kc_mode', $mode)
            ->where('program_id', $program)
            ->where('cycle_id', $cycle)
            ->pluck('activity_id');

    return $psa;
  }

  public static function get_mibf_existing($psgc_id, $program, $cycle, $purpose,$mode){

    $mibf = DB::table('kc_municipalforum')->where('psgc_id', $psgc_id)
            ->where('program_id', $program)
            ->where('cycle_id', $cycle)
            ->where('purpose', $purpose)
            ->where('kc_mode',$mode)
            ->pluck('activity_id');

    return $mibf;
  }

  public static function get_mun_training_existing($psgc_id, $training_title, $cycle_id, $training_cat, $date_conducted){
    $mibf = DB::table('kc_trainings')->where('psgc_id', $psgc_id)
            ->where('training_title', $training_title)
            ->where('cycle_id', $cycle_id)
            ->where('training_cat', $training_cat)
            ->where('date_conducted', $date_conducted)
            ->pluck('reference_no');

    return $mibf;
  }

  public static function save_ncddp($table, $data){
    DB::table($table)->insert($data);
  }

  public static function delete_ncddp($table, $id){
    DB::table($table)->where('activity_id', $id)->delete();
  }

  public static function delete_project_coverage($id){
    DB::table('kc_projcoverage')->where('project_id', $id)->delete();
  }

  public static function delete_proposals($id){
    DB::table('kc_projproposal')->where('project_id', $id)->delete();
  }

  public static function update_ncddp($table, $data){
    DB::table($table)
            ->where('activity_id', $data['activity_id'])
            ->update($data);
  }

  public static function update_proposal($data){
    DB::table('kc_projproposal')
            ->where('project_id', $data['project_id'])
            ->update($data);
  }

  public static function get_brgy_assembly_by_psgc($psgc_id){
    $brgy_assembly = DB::table('kc_bassembly')->where('psgc_id', $psgc_id)->orderBy('activity_id', 'desc')->pluck('activity_id');

    return $brgy_assembly;
  }

  public static function get_psa_by_psgc($psgc_id){
    $psa = DB::table('kc_psanalysis')->where('psgc_id', $psgc_id)->orderBy('activity_id', 'desc')->pluck('activity_id');

    return $psa;
  }

  public static function get_mibf_by_psgc($psgc_id){
    $mibf = DB::table('kc_municipalforum')->where('psgc_id', $psgc_id)->orderBy('activity_id', 'desc')->pluck('activity_id');

    return $mibf;
  }

  public static function get_mun_training_by_psgc($psgc_id){
    $mun_training = DB::table('kc_trainings')->where('psgc_id', $psgc_id)->orderBy('reference_no', 'desc')->pluck('reference_no');

    return $mun_training;
  }

  public static function get_current_cycle($psgc_id){
    $cycle = DB::table('kc_lgucycles')->where('psgc_id',$psgc_id)->where('current', 1)->pluck('cycle_id');

    return $cycle;

  }

  public static function get_current_program($psgc_id){
    $program = DB::table('kc_lgucycles')->where('psgc_id',$psgc_id)->where('current', 1)->pluck('program');

    return $program;

  }

  public static function get_available_barangays($user) {
    $psgc_id = Report::get_muni_psgc($user);
    $office = Report::get_office($user);
    if($office === 'ACT'){
      $barangay_id = Report::get_psgc_id($psgc_id);
    }
    else if($office === 'RPMO' or $office === 'SRPMO'){
      $barangay_id = Report::get_prov_psgc_id($psgc_id);
    }
    else{
      $barangay_id = Report::get_all_brgy_psgc_id();
    }

    return $barangay_id;
  }


}