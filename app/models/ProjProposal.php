<?php
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
class ProjProposal extends Eloquent {
	
	protected $table = 'kc_projproposal';
	protected $primaryKey = 'project_id';
    protected $fillable = [
        'project_id',
        'activity_id',
        'rank',
        'project_name',
        'priority',
        'kc_amount',
        'lcc_amount',
        'pamana_amount',
        'type',
        'lead_brgy',
        'added_to_spi'
    ];
	public function municipality()
	{
		return $this->hasOne('Municipality','municipality_psgc','muni_psgc');
	}

    public function special()
    {
        return $this->hasOne('Projproposalspecial','project_id','project_id');
    }
	public function province()
	{
		return $this->hasOne('Provinces','province_psgc','prov_psgc');
	}

	public function projcoverage()
	{
		return $this->hasMany('ProjCoverage', 'project_id', 'project_id');
	}

	public function municipal_forum() {
		return $this->belongsTo('MunicipalForum', 'activity_id', 'activity_id');
	}

	public function proj_coverage_list() {
		$brgys = $this->projcoverage()->get();

		$list = [];
		foreach ($brgys as $brgy) {
			array_push($list, Report::get_barangay($brgy->name));
		}
		return join(", ", $list);
	}

	public static function get_current_proposals($psgc_id, $mode) {
		$forums = MunicipalForum::where([ 'psgc_id' => $psgc_id,'kc_mode' => $mode ])->lists('activity_id');
		// $forums = isset($forums) ? $forums : 'non';
		if(!empty($forums)){
			$rf_approved = DB::table('rf_approved')->whereIn('activity_id',$forums)->exists();
			if($rf_approved){
				$proj_proposals = ProjProposal::whereIn( 'activity_id', $forums )->where('added_to_spi', 'F')->where('priority','=',1)->get();
			}else{
				$proj_proposals = [];
			}
			return $proj_proposals;
		}else{
			return [];
		}

	}

}

