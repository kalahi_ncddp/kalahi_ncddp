<?php


class SPIWorkItem extends Eloquent
{
	protected $table         = 'spi_workitems';
	protected $primaryKey    = 'item_no';
	public    $incrementing  = false;
	protected $fillable      = array('scope_of_work','unit');


    protected static function boot()
    {
        parent::boot();

        static::creating(function($model)
            {
                $model->{$model->getKeyName()} = (string)$model->generateNewId();
            });
    }

    public function generateNewId()
    {
        $key      = '';
		$user         = Session::get('username');
		$muni_psgc    = Report::get_muni_psgc($user);
		$total_count  = DB::table('spi_workitems')->where('item_no', 'like', $muni_psgc . '%')->count();
        $key      = $muni_psgc . '-' . (intval($total_count) + 1);
        return $key;
    }

    public function accomplishment_item()
    {
        return $this->belongsTo('SPIAccomplishmentItem', 'item_no', 'item_no');
    }

    public function scopeAllFromMunicipality($query, $muni_psgc)
    {
        return $query->where('item_no', 'like', $muni_psgc . '%'); 
    }
} //--end of SPI_OCComm