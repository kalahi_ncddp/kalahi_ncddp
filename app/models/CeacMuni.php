<?php
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
class CeacMuni extends Eloquent {
	
	protected $table = 'kc_ceacmuni';
	protected $fillable = [
		'psgc_id',
		'program_id',
		'cycle_id',
		'activity_code',
		'startdate',
		'enddate',
		'remarks',
		'actual_start',
		'actual_end',
        'kc_mode',
        'reference_no'
    ];
	// protected $primaryKey = ['psgc_id', 'program_id', 'cycle_id','activity_code'];


	public function municipality()
	{
		return $this->hasOne('Municipality','municipality_psgc','muni_psgc');
	}

	public function province()
	{
		return $this->hasOne('Provinces','province_psgc','prov_psgc');
	}

	
}
