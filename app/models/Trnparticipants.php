<?php

class Trnparticipants extends Eloquent {
	
	protected $table = 'kc_trnparticipants';
	protected $primaryKey = 'reference_no';
	
    protected $fillable = [
        'reference_no',
        'psgc_id',
        'beneficiary_id',
        'lastname',
        'firstname',
        'age',
        'is_volunteer',
        'participant_id',
        'middlename',
        'birthdate',
        'sex',
        'sector',
        'is_ip',
        'is_pantawid',
        'is_slp',
        'is_lguofficial'
    ];
	public function beneficiary()
	{
		return $this->hasOne('Beneficiary', 'beneficiary_id', 'beneficiary_id');
	}

	public function getbarangay()
	{	
		$psgc_id = $this->psgc_id;

		$psgc_id = str_pad($psgc_id, 9,'0',STR_PAD_LEFT );
		return Barangay::where('barangay_psgc',$psgc_id)->first();
	}

	public function is_ip()
	{
		return $this->is_ip == 1 ? 'true' : 'false';
	}

	public function is_ipleader()
	{
		return $this->is_ipleader == 1 ? 'true' : 'false';
	}

	public static function insertParticipant($data,$psgc_id,$base_id)
	{

	            $Trnparticipants = Trnparticipants::create($data);
				$newsquence = sprintf("%06d", $Trnparticipants->participant_id);
		        $reference_no =  $base_id.$psgc_id.$newsquence;
				
				// set new volunteer id from incremental id
				Trnparticipants::where('participant_id',$Trnparticipants->participant_id)->update(['reference_no'=>$reference_no]);
			return $reference_no;
	}

	
}