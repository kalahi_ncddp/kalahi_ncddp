<?php

class MemOtherTrn extends Eloquent {
	
	protected $table = 'kc_memothertrn';
	// protected $primaryKey = 'reference_no';
	
	protected $fillable = [
	];


	public function beneficiary()
	{
		return $this->belongsTo('Beneficiary', 'beneficiary_id', 'beneficiary_id');
	}
}