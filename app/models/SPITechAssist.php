<?php


class SPITechAssist extends Eloquent
{
	protected $table      = 'spi_techassist';
	protected $fillable   = array(
	   'id', 'municipal_psgc', 'barangay_psgc', 'program_id', 'cycle_id', 'startdate',
	    'enddate', 'activity_code', 'provider', 'provider_to', 'tech_details', 'tech_category','$purpose');
} //--end of SPITechAssist