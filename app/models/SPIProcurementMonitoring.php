<?php

class SPIProcurementMonitoring extends Eloquent {
	protected $table = 'spi_procmon';
	protected $primaryKey = 'id';

	protected $fillable = array(
		'project_id',
		'package_no',
		'package',
		'mode',
		'nol_issued',
		'supplier_id',
		'contract_amnt',
		'eproc_date',
		'canvas_start',
		'canvas_end',
		'prebid_date',
		'open_date',
		'pqual_date',
		'act_date',
		'rpmo_date',
		'status'
	);
}