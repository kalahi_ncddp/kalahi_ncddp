<?php

class SPISummaryPOWItem extends Eloquent
{
	protected $table         = 'spi_summary_pow_items';
	protected $primaryKey    = 'id';
	public    $incrementing  = false;
	protected $fillable      = array('acc_report_id', 'month', 'year', 'cumm_plnd_physical', 'noncumm_plnd_physical', 
		'cumm_actl_physical', 'noncumm_actl_physical', 'cumm_plnd_financial', 'noncumm_plnd_financial',
		'cumm_actl_financial', 'noncumm_actl_financial', 'slippage');


	public function accomplishment_report()
    {
        return $this->belongsTo('SPIAccomplishmentReport', 'acc_report_id', 'acc_report_id');
	}

	public function scopeAllFromProject($query, $acc_report_id)
    {
        return $query
        	->orderBy('month_num', 'asc')
        	->orderBy('id', 'asc')
            ->where('acc_report_id', '=', $acc_report_id); 
    }
} //--end of SPI_OCComm