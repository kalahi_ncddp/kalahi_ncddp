<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
class VolunteerNumber extends Eloquent {
	use SoftDeletingTrait;
    protected $table = 'kc_beneficiary';
//    protected $dates = ['deleted_at'];
    protected $timestamp = false;

    protected $primaryKey = 'beneficiary_id';
	protected $fillable = [
		'beneficiary_id',
		'lastname',
		'firstname',
		'middlename',
		'birthdate',
		'sex',
		'civil_status',
		'no_children',
		'address',
		'contact_no',
		'current_position_blgu',
		'is_ip',
		'is_leader',
		'educ_attainment',
		'occupation',
		'created_at',
		'updated_at',
		'deleted_at',

	];

	// public function lguactivity() 
	// {
	//   return $this->hasOne('LguActivity', 'activity_id','activity_id');
	// }

}
