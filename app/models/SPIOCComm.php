<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SPIOCComm extends Eloquent
{
	use SoftDeletingTrait;
	protected $table    = 'spi_occomm';
	protected $fillable = array(
	    'id', 'program_id', 'cycle_id', 'lgu_psgc', 'lgu_covered', 'comm_name',
	    'no_male', 'no_female', 'date_org', 'freq_meetings','kc_mode');

	public static function ins($program_id,
								  $cycle_id,
								  $lgu_psgc,
								  $lgu_covered,
								  $comm_name,
								  $no_male,
								  $no_female,
								  $date_org, // in any date format
								  $freq_meetings)
	{
		$id = DB::table('spi_occomm')->insertGetId(array(
			'program_id'    => $program_id,
			'cycle_id'    	=> $cycle_id,
			'lgu_psgc'    	=> $lgu_psgc,
			'lgu_covered' 	=> $lgu_covered,
			'comm_name'   	=> $comm_name,
			'no_male'     	=> $no_male,
			'no_female'   	=> $no_female,
			'date_org'      => date("Y-m-d", strtotime($date_org)),
			'freq_meetings' => $freq_meetings)
		);

		return $id;
	}
} //--end of SPI_OCComm