<?php

class Approval extends Eloquent {

    protected $table = 'rf_approved';

    protected $fillable = [
        'activity_id',
        'approved_level',
        'uploaded_by',
        'uploaded_at',
        'act_reviewed',
        'srpmo_reviewed',
        'rpmo_approved',
        'kc_mode',
        'module_type',
        'psgc_id',
        'cycle_id',
        'program_id'
    ];
    public static function isReviewed($id)
    {
       return Approval::where('activity_id',$id)->exists();
    }
    public function getApproved($user)
    {
        return 'for reviewed';
    }
    public function approved_bassembly()
    {
        return $this->hasOne('BarangayAssembly', 'activity_id', 'activity_id');
    }


    public function approved_mibf()
    {
        return $this->hasOne('MunicipalForum', 'activity_id', 'activity_id');
    }

    public function approved_psa()
    {
        return $this->hasOne('PSAnalysis', 'activity_id', 'activity_id');
    }

    public function approved_mlprap()
    {
        return $this->hasOne('MLPRAP','action_id','activity_id');
    }

    public function mlprap_detail()
    {
        return $this->hasOne('MLPRAPDetails','actionplan_id','activity_id');
    }

    public function approved_grsbarangay()
    {
        return $this->hasOne('GrsBarangayModel','reference_no','activity_id');
    }


    public function approved_grsmuni()
    {
        return $this->hasOne('GrsMuniModel','reference_no','activity_id');
    }

    public function approved_intake()
    {
        return $this->hasOne('Intake','reference_no','activity_id');
    }
    public function intake_updates()
    {
        return $this->hasMany('Grsupdates','reference_no','activity_id');

    }

    /*
    *	Trainings
    */
    public function approved_training()
    {
        return $this->hasOne('Trainings', 'reference_no', 'activity_id');
    }

    public function trnbrgy()
    {
        return $this->hasOne('Trnbrgy', 'reference_no', 'activity_id');
    }

    public function trnparticipants()
    {
        return $this->hasMany('Trnparticipants', 'reference_no', 'activity_id');
    }

    public function trainor()
    {
        return $this->hasMany('Trainor', 'reference_no', 'activity_id');
    }


    /*
    *	Volunteers
    */
    public function approved_volunteer()
    {
        return $this->hasOne('Volunteer', 'volunteer_id', 'activity_id');
    }

    public function volcommmem()
    {
        return $this->hasMany('VolCommMem', 'volunteer_id', 'activity_id');
    }

    /* Sub-tables */
    public function sitiorep()
    {
        return $this->hasMany('SitioRep', 'activity_id', 'activity_id');
    }

    public function sectorsrep()
    {
        return $this->hasMany('SectorsRep', 'activity_id', 'activity_id');
    }

    public function activityissues()
    {
        return $this->hasMany('ActivityIssues', 'activity_id', 'activity_id');
    }

    /* Sub (PSA) */
    public function psaproject()
    {
        return $this->hasMany('PSAProjects', 'activity_id', 'activity_id');
    }

    public function psadocuments()
    {
        return $this->hasMany('PSADocuments', 'activity_id', 'activity_id');
    }


    /* Sub (MIBF) */
    public function projproposal()
    {
        return $this->hasMany('ProjProposal', 'activity_id', 'activity_id');
    }

    /**
     * @return mixed
     */
    public function activityOne()
    {
        return $this->hasOne('ActivityIssues','activity_id','activity_id');
    }
}
