<?php

class MLCCMain extends Eloquent {

	protected $table = 'spi_mlccmain';
	protected $primaryKey = 'mlcc_id';

	protected $fillable = array(
		'municipal_psgc',
		'no_of_barangays',
		'as_of_date',
		'kc_grant',
		'is_draft',
		'planned_cash',
		'planned_inkind',
		'actual_cash',
		'actual_inkind',
		'mlcc_id',
		'kc_mode',
		'cycle_id',
		'program_id'
	);

    public function detail()
    {
        return $this->hasMany('MLCCDetail','mlcc_id','mlcc_id');
    }
	
}