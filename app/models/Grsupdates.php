<?php

class Grsupdates extends Eloquent
{
    protected $table = 'kc_grsupdates';

    protected $fillable = array(
        'reference_no',
        'date',
        'narrative',
        'action_no',
        'update_by'
    );
}