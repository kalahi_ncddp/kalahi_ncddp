<?php

class MLCCDetail extends Eloquent {

	protected $table = 'spi_mlccdetl';
	protected $primaryKey = 'id';
	
	protected $fillable = array(
		'category',
		'plgu_planned',
		'plgu_actual',
		
		'mlgu_planned',
		'mlgu_actual',
		'blgu_planned',
		'blgu_actual',
		'others_planned',
		'others_actual',
		'mlcc_id'
	);
}