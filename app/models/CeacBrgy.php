<?php
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class CeacBrgy extends Eloquent {
	
	protected $table = 'kc_ceacbgry';
    use SoftDeletingTrait;
	protected $fillable = [
		'psgc_id',
		'program_id',
		'cycle_id',
		'activity_code',
		'startdate',
		'enddate',
		'enddate',
		'remarks',
		'actual_start',
		'actual_end',
        'kc_mode',
        'reference_no'
	];
	public function barangay()
	{
		return $this->hasOne('Barangay','barangay_psgc','psgc_id');
	}

	public function municipality()
	{
		return $this->hasOne('Municipality','municipality_psgc','muni_psgc');
	}

	public function province()
	{
		return $this->hasOne('Provinces','province_psgc','prov_psgc');
	}

}
