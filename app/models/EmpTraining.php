<?php

class EmpTraining extends Eloquent {
	
	protected $table = 'kc_emptraining';

	protected $fillable = [
		'record_id',
		'employee_id',
		'training_name',
		'startdate',
		'enddate',
		'conducted_by',
		'is_ncddp_related'
	];

	public function ncddp_related()
	{
		if($this->is_ncddp_related == 1)
		{
			return 'YES';
		}else
		{
			return 'NO';
		}
	}
    

}
