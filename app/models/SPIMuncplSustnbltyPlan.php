<?php


class SPIMuncplSustnbltyPlan extends Eloquent
{
	protected $table    = 'spi_muncplsustnbltyplans';
	protected $fillable = array(
		'municipality_psgc', 'program_id', 'cycle_id', 'date_created', 'remarks');
} //--end of SPI_OCComm