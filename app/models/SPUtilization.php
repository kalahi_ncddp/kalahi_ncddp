<?php

class SPUtilization extends Eloquent {
	protected $table = 'spi_utilization';
	protected $primaryKey = 'project_id';

	protected $fillable = array(
		'project_id',
		'as_of_date',
		'grant_utilization',
		'lcc_utilization'
	);

	protected static $rules = [
		'as_of_date'=>'required'
	];

	public static function createUtilization($data)
	{

		$validation =  Validator::make($data,self::$rules);
		if($validation->fails()){
			Session::flash('error','Fill the required field');
			return false;
		}else{
				$success = SPUtilization::create($data);
				if($success)
				{
					return true;
				}
				else {
					return false;
				}
		}
	
	}

}