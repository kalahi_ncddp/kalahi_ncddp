<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
class Regions extends Eloquent {
	use SoftDeletingTrait;
	protected $table = 'kc_regions';

    public function province()
    {
        $this->hasMany('Provinces');
    }
}
