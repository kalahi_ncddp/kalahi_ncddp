<?php

class Trainings extends Eloquent {
	
	protected $table = 'kc_trainings';
	 protected $primaryKey = 'reference_no';
    public function trnbrgy()
    {
        return $this->hasOne('Trnbrgy', 'reference_no', 'reference_no');
    }

    public function trnparticipants()
    {
        return $this->hasMany('Trnparticipants', 'reference_no', 'reference_no');
    }

    public function trainor()
    {
        return $this->hasMany('Trainor', 'reference_no', 'reference_no');
    }
	public static function get_muni_trainings($username, $mode){
		$office = Report::get_office($username);
		$psgc_id = Report::get_muni_psgc($username);
		
		if($office === "ACT"){
			$data = Trainings::where('psgc_id', $psgc_id)
			->where('kc_mode', $mode)
			->get();		
		}
		else if($office === "RPMO" or $office === "SRPMO"){
			$psgc_id = Report::get_muni_psgc_id($psgc_id);
			$data = Trainings::whereIn('psgc_id', $psgc_id)->get();	
		} 
		else{

			$data = Trainings::get();
		
		}
		
		return $data;
	}
	
	public static function get_brgy_trainings($username,$mode){
		$office = Report::get_office($username);
		$psgc_id = Report::get_muni_psgc($username);
		
		if($office === "ACT"){
			$psgc_id = Report::get_psgc_id($psgc_id);
			try {

		$data = Trainings::whereIn('psgc_id', $psgc_id)->where('kc_mode',$mode)->get();
			}catch(Exception $e){
				$data = array();
			}
				
		}
		else if($office === "RPMO" or $office === "SRPMO"){
			$psgc_id = Report::get_prov_psgc_id($psgc_id);
			$data = Trainings::whereIn('psgc_id', $psgc_id)->get();	
		} 
		else{
			$data = Trainings::get();
		}
		
		return $data;
	}
	
	public static function get_training_existing($psgc_id, $cycle_id, $training_cat, $date_conducted, $program_id,$mode){
		$training = Trainings::where('psgc_id', $psgc_id)
						->where('cycle_id', $cycle_id)
						->where('training_cat', $training_cat)
						->where('date_conducted', $date_conducted)
						->where('program_id', $program_id)
                        ->where('kc_mode',$mode)
						->pluck('reference_no');
			
		return $training;
	}
	
	public static function get_participant_existing($reference_no,$sex, $lastname, $firstname, $age){
		$participant = Trnparticipants::where('reference_no', $reference_no)
						->where('lastname', $lastname)
						->where('firstname', $firstname)
						->where('age', $age)
                        ->where('sex',$sex)
                        ->where('reference_no',$reference_no)
						->pluck('reference_no');
			
		return $participant;
	}
	
	
	
	public static function get_training_by_psgc($psgc_id){
		$training = Trainings::where('psgc_id', $psgc_id)->orderBy('reference_no', 'desc')->pluck('reference_no');
		
		return $training;
	}
	
	public static function get_training_cat(){
		$training_cat = DB::table('rf_trainingcat')->lists('description');
		
		return $training_cat;
	}
	
	public static function get_trainors($id){
		$trainors = DB::table('kc_trainor')->where('reference_no', $id)->get();
		
		return $trainors;
	}	
	
	public static function get_trnbrgy($id){
		$brgy = DB::table('kc_trnbrgy')->where('reference_no', $id)->get();
		
		return $brgy;
	}	
	
	public static function get_participants($id){
		$participants = Trnparticipants::where('reference_no', $id)->get();

		return $participants;
	}
	public static function get_participants_by_sex($id, $sex){
		$participants = Trnparticipants::where('reference_no', $id)
		->where('sex', $sex)->count();

		return $participants;
	}
	
	public static function delete_training($table, $id){
		DB::table($table)->where('reference_no', $id)->delete();
	}	
	
	public static function delete_participant( $id){
		Trnparticipants::where('participant_id', $id)->delete();
	}	
	
	public static function update_training($data, $id){
		Trainings::where('reference_no', $id)
            ->update($data);
	}
	
	// relations
	
	public function getMunicipal()
	{
		return $this->hasOne('Municipality','municipality_psgc','psgc_id');
	}

	public function getbarangay()
	{
		return $this->hasOne('Barangay','barangay_psgc','psgc_id');
	}

    public function getRepresented()
    {
       return  Trnparticipants::where('reference_no', $this->reference_no)->groupBy('psgc_id')->get()->count();
    }


	public function total_participants()
	{
		return Trnparticipants::where('reference_no', $this->reference_no)->get()->count();
	}	

	public function total_participants_by_genders($gender = 'F')
	{
		return Trnparticipants::where('reference_no', $this->reference_no)
									->where('sex','like',$gender[0].'%')->get()->count() ;
	}

	public function total_ip($gender = 'F')
	{
		return Trnparticipants::where('reference_no',$this->reference_no)
								->where('is_ip',1)
								->where('sex','like',$gender[0].'%')
								->get()->count();
	}

	public function total_lguofficial($gender = 'F')
	{
		return Trnparticipants::where('reference_no',$this->reference_no)
								->where('is_lguofficial',1)
								->where('sex','like',$gender[0].'%')
								->get()->count();
	}

	public function total_slp($gender = 'F')
	{
		return Trnparticipants::where('reference_no',$this->reference_no)
				->where('is_slp',1)
				->where('sex','like',$gender[0].'%')
				->get()->count();
	}

	public function total_pantawid($gender = 'F')
	{
		return Trnparticipants::where('reference_no',$this->reference_no)
				->where('is_pantawid',1)
				->where('sex','like',$gender[0].'%')
				->get()->count();
	}
	public function total_ipleader($gender = 'F')
	{
		return Trnparticipants::where('reference_no',$this->reference_no)
								->where('is_ipleader',1)->where('sex','like',$gender[0].'%')
								->get()->count();
	}

	public function age_participants($gender = 'F',$age = 60)
	{
		return Trnparticipants::where('reference_no',$this->reference_no)
								->where('age','>',$age)
								->where('sex','like',$gender[0].'%')
								->get()->count();
	}

	public function total_male_vol($gender = 'M')
	{
		return Trnparticipants::where('reference_no',$this->reference_no)
				->where('is_volunteer',1)
				->where('sex','like',$gender[0].'%')
				->get()->count();
	}

	public function total_female_vol($gender = 'F')
	{
		return Trnparticipants::where('reference_no',$this->reference_no)
				->where('is_volunteer',1)
				->where('sex','like',$gender[0].'%')
				->get()->count();
	}

	public function total_non_male_vol($gender = 'M')
	{
		return Trnparticipants::where('reference_no',$this->reference_no)
				->where('is_volunteer',0)
				->where('sex','like',$gender[0].'%')
				->get()->count();
	}
	public function total_non_female_vol($gender = 'F')
	{
		return Trnparticipants::where('reference_no',$this->reference_no)
				->where('is_volunteer',0)
				->where('sex','like',$gender[0].'%')
				->get()->count();
	}

	public function reference()
	{
		return $this->hasOne('References','reference_no','reference_no');
	}

}
