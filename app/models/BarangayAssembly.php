<?php
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
class BarangayAssembly extends Eloquent {
    protected $table = 'kc_bassembly';
//    protected $dates = ['deleted_at'];

    protected $primaryKey = 'activity_id';
	protected $fillable = [
		'activity_id',
		'program_id',
		'cycle_id',
		'psgc_id',
		'purpose',
		'no_families',
		'no_household',
		'no_atnmale',
		'no_atnfemale',
		'no_ipmale',
		'no_ipfemale',
		'no_oldmale',
		'no_oldmale',
		'no_pphousehold',
		'no_ppfamilies',
		'no_slphousehold',
		'no_slpfamilies',
		'no_iphousehold',
		'no_ipfamilies',
		'highlights',
		'total_household',
		'total_families',
		'is_draft',
		'kc_mode'
	];

	public function lguactivity() 
	{
	  return $this->hasOne('LguActivity', 'activity_id','activity_id');
	}
    /* Sub-tables */
    public function sitiorep()
    {
        return $this->hasMany('SitioRep', 'activity_id', 'activity_id');
    }

    public function sectorsrep()
    {
        return $this->hasMany('SectorsRep', 'activity_id', 'activity_id');
    }

    public function activityissues()
    {
        return $this->hasMany('ActivityIssues', 'activity_id', 'activity_id');
    }
}
