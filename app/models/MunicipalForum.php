<?php

class MunicipalForum extends Eloquent {
	
	protected $table = 'kc_municipalforum';
	protected $primaryKey = 'activity_id';

    public function lguactivity()
    {
        return $this->hasOne('LguActivity', 'activity_id','activity_id');
    }

  public function proj_proposals() {
  	return $this->hasMany('ProjProposal', 'activity_id', 'activity_id');
  }

  public function subproject()
  {

    return SPIProfile::where('municipal_id',$this->psgc_id)->where('kc_mode',Session::get('accelerated'))->get();
  }
  
  public function getTraining()
  {
      $training = Trainings::where('psgc_id',$this->psgc_id)
                  ->where('cycle_id',$this->cycle_id)
                  ->where('program_id',$this->program_id)
                  ->first();

      return $training;
  }
    public function projproposal()
    {
        return $this->hasMany('ProjProposal', 'activity_id', 'activity_id');
    }
    public function sectorsrep()
    {
        return $this->hasMany('SectorsRep', 'activity_id', 'activity_id');
    }
}
