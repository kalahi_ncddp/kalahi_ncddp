<?php

class Comments extends Eloquent {

    protected $table = 'kc_comments';

     protected $fillable = [
         'activity_id',
         'commented_by',
         'comments',
         'kc_mode',
         'psgc',
         'office',
         'module_type'
     ];

    public function offices()
    {
        return User::where('id',$this->commented_by)->first()->office_level;
    }

    public function fullname()
    {
        return User::where('id',$this->commented_by)->first()->fullname();
    }
}
