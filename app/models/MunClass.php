<?php
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
class MunClass extends Eloquent {
	
	protected $table = 'rf_munclass';

	public function municipality()
	{
		return $this->hasOne('Municipality','municipality_psgc','muni_psgc');
	}

	public function province()
	{
		return $this->hasOne('Provinces','province_psgc','prov_psgc');
	}

}
