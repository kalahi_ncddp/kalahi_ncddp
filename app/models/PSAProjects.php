<?php
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
class PSAProjects extends Eloquent {

	protected $table = 'kc_psaproject';
	protected $fillable = [
		'project_id',
		'activity_id',
		'rank',
		'problem',
		'problem_cat',
		'solution',
		'solution_cat'	
	];

    public function psa(){
        return $this->belongsTo('PSAnalysis','activity_id','activity_id');
    }
	public function scopeAllFromMunicipality($query, $municipal_id)
	{
		return $query
			->select('kc_psaproject.project_id', 'kc_psaproject.problem','kc_psaproject.activity_id', 'kc_psaproject.problem_cat',
					 'kc_psaproject.solution', 'kc_psaproject.solution_cat', 'kc_psanalysis.psgc_id')
			->join('kc_psanalysis', 'kc_psanalysis.activity_id', '=', 'kc_psaproject.activity_id')
			->join('kc_barangay', 'kc_barangay.barangay_psgc', '=', 'kc_psanalysis.psgc_id')
			->where('kc_barangay.muni_psgc', '=', $municipal_id);
	}

    public function solutions()
    {
        return $this->hasMany('Psasolutions','project_id','project_id');
    }

    public function spi(){
        $psa = PSAnalysis::where('activity_id',$this->activity_id)->first();

        $spi = SPIProfile::where('cycle_id',$psa->cycle_id)
            ->where('program_id',$psa->program_id)
            ->where('barangay_id',$psa->psgc_id)
            ->where('proj_subcategory',$this->solution_cat)
            ->first();
        return $spi;
    }
}
