<?php

class SPISET extends Eloquent {
	protected $table = 'spi_set';
	protected $primaryKey = 'id';

	protected $fillable = array(
		'project_id',
		'implementation_mode',
		'actual_grant',
		'actual_lcc',
		'date_conducted',
		'date_evaluated',
		'om_group',
		'phys_descrip',
		'sp_utilization',
		'org_and_mgmt',
		'institutional_linkage',
		'financial_component',
		'physical_technical',
		'sp_utilization_wt',
		'org_and_mgmt_wt',
		'institutional_linkage_wt',
		'financial_component_wt',
		'physical_technical_wt',
		'approved_grant',
		'approved_lcc'
	);

	public function final_rating() {
		// return ($this->sp_utilization 			* ($this->sp_utilization_wt / 100)) +
		// 			 ($this->org_and_mgmt					* ($this->org_and_mgmt_wt / 100)) +
		// 			 ($this->instutional_linkage	* ($this->instutional_linkage_wt / 100)) +
		// 			 ($this->financial_component	* ($this->financial_component_wt / 100)) +
		// 			 ($this->physical_technical		* ($this->physical_technical_wt / 100));

		return ($this->sp_utilization + $this->org_and_mgmt + $this->institutional_linkage + $this->financial_component + $this->physical_technical)  ;
	}

	public function adjectival_rating() {
		$final_rating = $this->final_rating();
		$rating = SPISETRating::where('min_score', '<=', $final_rating)
								->where('max_score', '>=', $final_rating)->first();

		if(empty($rating)){
			return 'out of the context';
		}else{
			return $rating->adjectival_rating;
		}
			
	}
}