<?php
/*
 this model is for the accessing the grs barangay table in the
 the database all database relationa query are handle this using orm
*/
class Projproposalspecial extends Eloquent
{

    protected $table = 'kc_projproposalspecial';
    protected $primaryKey = 'project_id';

    protected $fillable = [
        'special_id',
        'project_id',
        'activity_id',
        'rank',
        'project_name',
        'priority',
        'kc_amount',
        'lcc_amount',
        'pamana_amount',
        'type',
        'lead_brgy',
        'added_to_spi'
    ];
}
