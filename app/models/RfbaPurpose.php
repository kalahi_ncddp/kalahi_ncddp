<?php
/*
	purpose model
*/
class RfbaPurpose extends Eloquent {
	
	protected $table = 'rf_bapurpose';

	protected $timestamp = false;
	protected $fillable = [
		'code',
		'description'
	];

}
