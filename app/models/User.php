<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    protected $fillable = ['password','position','username','first_name','middle_name','last_name','psgc_id','office_level','municipality'];
	public function rfuser() 
	{
	  return $this->hasOne('RFUsers','userid');
	}

    public function fullname()
    {
        return $this->first_name.' '.$this->last_name;
    }
}
