<?php

class Supplier extends Eloquent {
	
	protected $table = 'kc_suppliers';

	protected $fillable = [
		'region_psgc',
		'prov_psgc',
		'muni_psgc',
		'cycle',
		'is_draft',
		'name_of_service_provider',
		'category',
		'address',
		'provider_id',
		'status',
		'remarks',
		'contact_person',
		'contact_number',
		'email',
	];

 	 
}
