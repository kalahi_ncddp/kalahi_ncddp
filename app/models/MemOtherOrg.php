<?php

class MemOtherOrg extends Eloquent {
	
	protected $table = 'kc_memotherorg';
	// protected $primaryKey = 'reference_no';
	
	protected $fillable = [
	];


	public function beneficiary()
	{
		return $this->belongsTo('Beneficiary', 'beneficiary_id', 'beneficiary_id');
	}

}