<?php

class References extends Eloquent {
	
	protected $table = 'rf_references';
	protected $primaryKey = 'reference_no';
	
	protected $fillable = [
		'reference_no',
		'ac_approved',
		'reference_type'
	];


    /*
    *	Trainings
    */
    public function approved_training()
    {
        return $this->hasOne('Trainings', 'reference_no', 'reference_no');
    }

    public function trnbrgy()
    {
        return $this->hasOne('Trnbrgy', 'reference_no', 'reference_no');
    }

	public function trnparticipants()
	{
		return $this->hasMany('Trnparticipants', 'reference_no', 'reference_no');
	}

	public function trainor()
	{
		return $this->hasMany('Trainor', 'reference_no', 'reference_no');
	}


	/*
	*	Volunteers
	*/
	public function approved_volunteer()
	{
		return $this->hasOne('Volunteer', 'volunteer_id', 'reference_no');
	}

	public function volcommmem()
	{
		return $this->hasMany('VolCommMem', 'volunteer_id', 'reference_no');
	}

	// check if aproved in reverse
	public function isApproved()
    {
        return  $this->ac_approved == '0000-00-00 00:00:00'  ;
    }
}