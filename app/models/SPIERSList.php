<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SPIERSList extends Eloquent
{
	use SoftDeletingTrait;
	protected $table         = 'spi_erslist';
	protected $primaryKey    = 'record_id';
	public    $incrementing  = false;
	protected $fillable      = array('project_id','date_reporting','date_start', 'date_ending');

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model)
            {
                $model->{$model->getKeyName()} = (string)$model->generateNewId();
            });
    }

    public function generateNewId()
    {
		$key      = '';
		$count    = DB::table('spi_erslist')->where('project_id', '=', $this->project_id)->count();
		$key      = $this->project_id . '-' . (intval($count) + 1);
		return $key;
    }

	public function project()
	{
		return $this->hasOne('SPIProfile','project_id','project_id');
	}

	public function totalLabor()
	{
		$spir = SPIERSRecord::where('record_id',$this->record_id)->get();
		$totalLabor = 0;

		foreach ($spir as $worker) {
			// $totalLabor += ($key->rate_day * $key->work_days  ) + ($key->rate_hours  * $key->work_hours );
			$totalLabor += ($worker->rate_hour * $worker->work_hours) + ($worker->rate_day * $worker->work_days);

		}
		return number_format($totalLabor,2);

	}

    public function ers_records()
    {
		return $this->hasMany('SPIERSRecord','record_id','record_id');
    }
} //--end of SPI_OCComm