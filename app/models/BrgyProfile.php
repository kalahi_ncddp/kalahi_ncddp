<?php

class BrgyProfile extends Eloquent {
	//use SoftDeletingTrait;
	protected $table = 'kc_brgyprofile';
	protected $primaryKey = 'profile_id';
	//protected $dates = ['deleted_at'];
	protected $fillable = [
	
		'is_draft'
		
	];
	
	
	public static function get_brgy_profiles($username, $mode){
		$office =  \User::where('username',$username)->first()->office_level;
		$psgc_id = Report::get_muni_psgc($username);
		if($office === "ACT"){
		// dd($psgc_id);
			$psgc_id = Report::get_psgc_id($psgc_id);
			try {
			$data = BrgyProfile::whereIn('barangay_psgc', $psgc_id)
			->where('kc_mode',$mode)
			->get();		
			}catch(Exception $e){
				$data = array();
			}
			
		}
		else if($office === "RPMO" or $office === "SRPMO"){
			$psgc_id = Report::get_prov_psgc_id($psgc_id);
			// $data = []
			$data = BrgyProfile::whereIn('barangay_psgc', $psgc_id)->get();	
		} 
		else{


            $data = BrgyProfile::get();
		}
		
		return $data;
	}
	
	public static function get_brgy_profile_existing($psgc_id, $program_id, $cycle_id,$mode){
		$brgyprofile = BrgyProfile::where('barangay_psgc', $psgc_id)
						->where('cycle_id', $cycle_id)
						->where('program_id', $program_id)
                        ->where('kc_mode',$mode)
						->pluck('profile_id');
			
		return $brgyprofile;
	}
	
	public static function get_brgy_profile_by_psgc($psgc_id){
		$brgy_profile = BrgyProfile::where('barangay_psgc', $psgc_id)->orderBy('profile_id', 'desc')->pluck('profile_id');
		
		return $brgy_profile;
	}
	
	public static function get_brgy_profile_other_data($table, $profile_id){
		$data = DB::table($table)->where('profile_id', $profile_id)->get();
		
		return $data;
	}
	
	public static function get_rf_facilities(){
		$data = DB::table('rf_facilities')->get();
		
		return $data;
	}

	public static function get_rf_type($code){
		// $data = DB::table('rf_brgyorg_type')->where('code', $code)->pluck('org_type');
		
		return [];
	}

	public static function get_types(){
		// $data = DB::table('rf_brgyorg_type')->lists('org_type', 'code');
		
		return [];
	}
	
	public static function save_brgyprofile_other_data($table, $data){
		DB::table($table)->insert($data);
	}	
	
	public static function delete_brgyprofile_other_data($table, $id){
		DB::table($table)->where('profile_id', $id)->delete();
	}

	public static function get_bdcp_id($id){
		$persons = DB::table('kc_brgybdcp')->where('profile_id', $id)->orderBy('person_id', 'desc')->pluck('person_id');

		return $persons;
	}

	public static function get_blgu_id($id){
		$persons = DB::table('kc_blgu')->where('profile_id', $id)->orderBy('person_id', 'desc')->pluck('person_id');

		return $persons;
	}


	public static function get_org_id($id){
		$orgs = DB::table('kc_brgyorgs')->where('profile_id', $id)->orderBy('org_id', 'desc')->pluck('org_id');

		return $orgs;
	}

	public static function get_devproj_id($id){
		$devprojs = DB::table('kc_brgydevproj')->where('profile_id', $id)->orderBy('devproj_id', 'desc')->pluck('devproj_id');

		return $devprojs;
	}

	public static function get_bdcmeet_id($id){
		$meeting = DB::table('kc_brgybdcmeet')->where('profile_id', $id)->orderBy('meeting_id', 'desc')->pluck('meeting_id');

		return $meeting;
	}

	public static function get_person($id){
		$person = DB::table('kc_brgybdcp')->where('person_id', $id)->first();

		return $person;
	}

	public static function get_person_blgu($id){
		$person = DB::table('kc_blgu')->where('person_id', $id)->first();

		return $person;
	}

	public static function get_org($id){
		$org = DB::table('kc_brgyorgs')->where('org_id', $id)->first();

		return $org;
	}

	public static function get_devproj($id){
		$devproj = DB::table('kc_brgydevproj')->where('devproj_id', $id)->first();

		return $devproj;
	}

	public static function get_meeting($id){
		$meeting = DB::table('kc_brgybdcmeet')->where('meeting_id', $id)->first();

		return $meeting;
	}




    //    relationship bdcp table
    public function bdcp()
    {
        return $this->hasMany('Brgybdcp','profile_id','profile_id');
    }
//  relatioship brgy orgs
    public function orgs()
    {
        return $this->hasMany('Brgyorgs','profile_id','profile_id');
    }
// brgy devproj
    public function devproj()
    {
        return $this->hasMany('Brgydevproj','profile_id','profile_id');
    }
// brgy bdcmeet
    public function bdcmeet()
    {
        return $this->hasMany('Brgybdcmeet','profile_id','profile_id');
    }

    public function facilities()
    {
        return $this->hasMany('Brgyfacility','profile_id','profile_id');
    }

    public function income()
    {
        return $this->hasMany('Brgyincome','profile_id','profile_id');

    }

    public function ipprof()
    {
        return $this->hasMany('Brgyipprof','profile_id','profile_id');
    }

    public function fundsource()
    {
        return $this->hasMany('Brgyfundsource','profile_id','profile_id');
    }

    public function landstatus()
    {
        return $this->hasMany('Brgylandstatus','profile_id','profile_id');
    }

    public function techresource()
    {
        return $this->hasMany('Brgytechresource','profile_id','profile_id');
    }

    public function transpo()
    {
        return $this->hasMany('Brgytranspo','profile_id','profile_id');

    }

    public function crop()
    {
        return $this->hasMany('Brgycrop','profile_id','profile_id');
    }

    public function lgu()
    {
        return $this->hasMany('Brgylgu','profile_id','profile_id');
    }

    public function envcrit()
    {
        return $this->hasMany('Brgyencrit','profile_id','profile_id');
    }
}
