<?php

class Asset extends Eloquent {
	protected $table = 'kc_assets';
	protected $primaryKey = 'property_no';

	protected $fillable = array(
		'property_no',
		'serial_no',
		'description',
		'date_acquired',
		'quantity',
		'unit',
		'level_issuance',
		'issued_to',
		'position',
		'po_number',
		'reference_no',
		'acquisition_cost',
		'remarks',
		'status',
		'useful_life',
		'salvage_value',
		'fund_source',
		'par'
	);

	public function balance() {
		return ($this->acquisition_cost - $this->salvage_value) / $this->useful_life;
	}

	public function issued_to_staff() {
		return Staff::where(array("employee_id" => $this->issued_to))->first();
	}
}