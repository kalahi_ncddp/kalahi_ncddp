<?php

class MLPRAP extends Eloquent {
	
	protected $table = 'kc_mlprap';

	protected $fillable = [
		'action_id',
		'psgc_id',
		'for_year',
        'to_year',
		'is_draft',
        'kc_mode',
		'date_prepared'
	];


	public static $rules = array(
		'for_year' => 'required',
		'date_prepared' => 'required|date_format:m/d/Y'
	);

	public static function validate($data) {
		return Validator::make($data, static::$rules);
	}
    public function mlprap_detail()
    {
        return $this->hasOne('MLPRAPDetails','plan_id','action_id');
    }

    public function mlprapdetails()
    {
        return $this->hasMany('MLPRAPDetails','plan_id','action_id');
    }
}
