<?php


class SPIWorker extends Eloquent
{
	protected $table         = 'spi_workers';
	protected $primaryKey    = 'worker_id';
	public    $incrementing  = false;

	protected $fillable      = array(
		'lastname' , 'firstname'   , 'middlename'  , 'sex'        ,
		'age'      , 'birthday'    , 'nature_work' ,'type_of_work', 'rate_hour'  ,
		'rate_day' , 'is_pantawid' , 'is_seac'     , 'is_ip','is_slp'	  ,
		'created_by'
	);

	protected static function boot()
	{
		parent::boot();

		static::creating(function($model)
			{
				$model->{$model->getKeyName()} = (string)$model->generateNewId();
			});
	}

	public function generateNewId()
	{
		$key      = '';
		$user     = Session::get('username');
		$psgc_id  = Report::get_muni_psgc($user);
		$count    = DB::table('spi_projects_workers')
						->join('spi_profile', 'spi_profile.project_id', '=', 'spi_projects_workers.project_id')
						->where('municipal_id', '=', $psgc_id)->count();
		$key      = $psgc_id . '-' . (intval($count) + 1);
		return $key;
	}

	public function projects()
	{
		return $this->belongsToMany('SPIWorker', 'spi_projects_workers', 'worker_id', 'id');
	}

	public function scopeAllFromMunicipality($query, $municipal_id)
	{
		return $query
			->select('spi_workers.worker_id', 'spi_workers.firstname', 'spi_workers.middlename', 'spi_workers.lastname',
			  	'spi_workers.sex', 'spi_workers.age', 'spi_workers.birthday', 'spi_workers.rate_day', 'spi_workers.rate_hour',
				'spi_workers.nature_work', 'spi_workers.is_pantawid', 'spi_workers.is_seac', 'spi_workers.is_ip','spi_projects_workers.project_id'
			)
			->join('users', 'users.username', '=', 'spi_workers.created_by')
			->leftJoin('spi_projects_workers', 'spi_projects_workers.worker_id', '=', 'spi_projects_workers.worker_id')
            ->groupBy('worker_id')
			->where('users.psgc_id', '=', $municipal_id);
	}

	public function scopeAllFromERSList($query, $record_id)
	{
		return $query
			->select('spi_workers.worker_id', 'spi_workers.firstname', 'spi_workers.middlename', 'spi_workers.lastname',
					 'spi_workers.nature_work', 'spi_workers.sex', 'spi_workers.age', 'spi_workers.birthday', 
					 'spi_ersrecords.id', 'spi_ersrecords.rate_hour', 'spi_ersrecords.rate_day', 'spi_ersrecords.work_hours', 
					 'spi_ersrecords.work_days', 'spi_ersrecords.plan_cash', 'spi_ersrecords.actual_cash', 
					 'spi_ersrecords.plan_lcc', 'spi_ersrecords.actual_lcc')
			->join('spi_ersrecords', 'spi_ersrecords.worker_id', '=', 'spi_workers.worker_id');	
	}
} //--end of SPI_OCComm