<?php

class TransLog extends Eloquent {
	
	protected $table = 'kc_transaction_log';

	protected $fillable = [
		'trans_id',
		'module',
		'transaction',
		'user_id',
		'activity_id',
		'full_name',
	];

	// list of transaction

	/* 
		1. login
	    2. Logout
	    3. Export
	    4. Sync
	*/

	public static function log($module="",$transaction = "unidentified",$user_id = "anonymous",$fullname="",$activity_id ="not_module")
	{
		try{

			$trans_id = Str::slug(md5(TransLog::count()));
			TransLog::create([
				'trans_id'=>$trans_id,
				'module'=>$module,
				'transaction'=>$transaction,
				'user_id' => $user_id,
				'activity_id' => $activity_id,
				'full_name'=>$fullname
			]);
		}catch(Exception $e){
			Session::flash('error','There something happen in the system.. please report');
		}
	}
}
