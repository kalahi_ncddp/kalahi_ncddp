<?php

class ProjectTraining extends Eloquent {
	
	protected $table = 'kc_project_training';

	protected $fillable = [
		'training_id',
		'region_psgc',
		'prov_psgc',
		'muni_psgc',
		'cycle',
		'training_title',
		'date_conducted',
		'no_of_participants',
		'conducted_by',
		'venue',
		'level',
		'location_data',
		'regional',
		'province',
		'municipal',
		'training_cost',
		'fund_source',
		'created_at',
		'updated_at',
		'deleted_at',
		
	];

 	 
}
