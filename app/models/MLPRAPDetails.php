<?php

class MLPRAPDetails extends Eloquent {
	
	//primaryKey
	protected $table = 'kc_mlprapdetails';
	protected $fillable = [
		'actionplan_id',
		'plan_id',
		'coverage',
		'is_draft',
		'problem',
		'stakeholders',
		'project_name',
		'gaps',
		'actions',
		'timeframe_from',
		'timeframe_to',
		'fund_amount',
		'fund_source',
		'lead_group',
		'agency',
		'beneficiaries',
        'kc_mode'
	];

	public static $rules = array(
		'project_name'		=> 'required',
		'coverage' 			=> 'required',
		'stakeholders' 		=> 'required',
		'problem' 			=> 'required',
		'gaps' 				=> 'required',
		'actions' 			=> 'required',
		'timeframe_from' 	=> 'required',
		'timeframe_to' 		=> 'required',
		'fund_amount' 		=> 'required|numeric|min:0',
		'fund_source' 		=> 'required',
		'lead_group' 		=> 'required',
		'agency'			=> 'required',
		'beneficiaries'		=> 'required'
	);

	public static function validate($data) {
		return Validator::make($data, static::$rules);
	}
}