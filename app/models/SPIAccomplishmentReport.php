<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SPIAccomplishmentReport extends Eloquent
{
	use SoftDeletingTrait;
	protected $table         = 'spi_accomplishment_report';
	protected $primaryKey    = 'acc_report_id';
	public    $incrementing  = false;
	protected $fillable      = array('acc_report_id');

	public function items()
	{
		return $this->hasMany('SPIAccomplishmentItem','acc_report_id','acc_report_id');
	}

	public function plans()
	{
		return $this->hasMany('SPIPlannedPOWItem','acc_report_id','acc_report_id');	
	}

	public function report()
	{
		return $this->hasOne('SPISummaryPOWItem','acc_report_id','acc_report_id');
	}
} //--end of SPI_OCComm