<?php
/*
 this model is for the accessing the grs barangay table in the 
 the database all database relationa query are handle this using orm 
*/
class RFUsers extends Eloquent {
	
	protected $table = 'rf_users';

	public function user() 
	{
	      return $this->belongsTo('users','id','userid');
	}
}
