<?php

class Ceac extends Eloquent {

    protected $table = 'kc_ceaclist';

     protected $fillable = [
         'reference_no',
         'cycle_id',
         'program_id',
         'psgc_id',
         'kc_mode'
     ];

    public function municipality()
    {
        return $this->hasOne('Municipality','municipality_psgc','psgc_id');
    }
}
