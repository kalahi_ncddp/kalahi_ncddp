<?php

class SPICompletionReport extends Eloquent {
	protected $table = 'spi_spcrdata';
	protected $primaryKey = 'project_id';

	protected $fillable = array(
		'project_id',
		'date_csw',
		'date_prioritization',
		'date_completed',
		'date_inaugurate',
		'no_skilled_male',
		'no_skilled_female',
		'no_unskilled_male',
		'no_unskilled_female',
		'no_households',
		'no_families',
		'no_pantawid_hh',
		'no_pantawid_fm',
		'phys_actual',
		'total_cost',
		'total_direct',
		'total_indirect',
		'no_male_pop',
		'no_female_pop',
		'no_male_serve',
		'no_female_serve',
		'no_male_ip',
		'no_female_ip',
		'no_slp_hh',
		'no_slp_family',
		'no_ip_hh',
		'no_ip_hh_families',
		'proc_mode',
		'total_counterpart',
		'days_skilled_male',
		'days_skilled_female',
		'days_unskilled_male',
		'days_unskilled_female',
		'rate_skilled_male',
		'rate_skilled_female',
		'rate_unskilled_male',
		'rate_unskilled_female',
        'completed_status'
	);

	public function profile()
    {
		SPIProfile::where(array('project_id' => $this->project_id))->first();
	}
}