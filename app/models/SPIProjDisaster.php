<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SPIProjDisaster extends Eloquent
{
	use SoftDeletingTrait;
	protected $table         = 'spi_projdisaster';
	protected $primaryKey    = 'projdisaster_id';
	public    $incrementing  = false;
	protected $fillable      = array(
		'municipal_psgc', 'calamity_type', 'description', 'date_occured','kc_mode');

	public function municipality()
	{
		return $this->hasOne('Municipality','municipality_psgc','municipal_psgc');
	}

    public function items()
    {
		return $this->hasMany('SPIProjDisasterItems','projdisaster_id','projdisaster_id');
    }

    public function scopeAllFromMunicipality($query, $municipal_psgc,$mode)
    {
        return $query
            ->where('municipal_psgc', '=', $municipal_psgc)
			->where('kc_mode',$mode); 
    }
} //--end of SPI_OCComm