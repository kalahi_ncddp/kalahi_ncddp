<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SPIProjDisasterItems extends Eloquent
{
	use SoftDeletingTrait;
	protected $table         = 'spi_projdisaster_items';
	protected $primaryKey    = 'projdisaster_item_id';

	protected $fillable      = array(
		'projdisaster_id',
        'projdisaster_item_id',
        'barangay_psgc',
        'project_id',
        'est_damages',
        'est_cost',
        'project_name',
        'fund_source',
        'functionality_sp',
        'cycle_id',
        'total_sp','date_completed');

    public function project_disaster()
    {
        return $this->belongsTo('SPIProjDisaster', 'projdisaster_id', 'projdisaster_id');
	}

	public function barangay()
	{
		return $this->hasOne('Barangay','barangay_psgc','barangay_psgc');
	}

    public function project()
    {
        return $this->hasOne('SPIProfile', 'project_id', 'project_id');
    }
} //--end of SPI_OCComm