<?php
class SPIPlannedPOWItem extends Eloquent
{
	protected $table         = 'spi_planned_pow_items';
	protected $primaryKey    = 'id';
	public    $incrementing  = false;
	protected $fillable      = array('acc_report_id', 'item_no', 'month_num', 'startend', 'physical', 'financial');

	public function item()
	{
		return $this->hasOne('SPIWorkItem','item_no','item_no');
	}

	public function accomplishment_report()
    {
        return $this->belongsTo('SPIAccomplishmentReport', 'acc_report_id', 'acc_report_id');
	}

	public function scopeAllFromProject($query, $acc_report_id)
    {
        return $query
        	->orderBy('month_num', 'asc')
        	->orderBy('id', 'asc')
            ->where('acc_report_id', '=', $acc_report_id); 
    }

    public function actual()
	{
		return $this->hasOne('SPIActualPOWItem','id','id');
	}
} //--end of SPI_OCComm