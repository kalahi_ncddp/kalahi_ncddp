<?php

class SPIActualPOWItem extends Eloquent
{
	protected $table         = 'spi_actual_pow_items';
	protected $primaryKey    = 'id';
	public    $incrementing  = false;
	protected $fillable      = array('physical', 'financial','id');

	public function plan()
	{
		return $this->hasOne('SPIPlannedPOWItem','id','id');
	}
} //--end of SPI_OCComm