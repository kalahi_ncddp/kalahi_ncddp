<?php
/*
 this model is for the accessing the grs munisipality table in the 
 the database all database relationa query are handle this using orm 
*/
class GrsMuniModel extends Eloquent {
	protected $table = 'kc_grsmuninstall';

	protected $fillable = [
        'reference_no',
		'psgc_id',
		'program_id',
		'cycle_id',
		'is_draft',
		'date_orientation',
		'other_mat',
		'date_ffcomm',
		'date_training',
		'date_inspect',
		'no_brochures',
		'no_tarpauline',
		'no_posters',
		'date_meansrept',
		'is_boxinstalled',
		'phone_no',
		'address',
		'remarks',
        'kc_mode'
	];

	public function municipality()
	{
		return $this->hasOne('Municipality','municipality_psgc','psgc_id');
	}

	/**
	 * get the barangay complete detail 
	 *
	 * @param  int $cycle_id
	 * @return Array
	 */
	public static function lguCycle($cycle_id)
	{
		$cycle = DB::table('kc_lgucycles')->where('cycle_id',$cycle_id)
										->where('lgu_type',0)->first();
		return $cycle;
	}
	
}
