<?php

class Lgumdcequipments extends Eloquent {
	
	protected $table = 'kc_lgumdcequipments';
	public $timestamps = false;
	protected $fillable = [
		'equipment_type',
  		'quantity',
		'date',
		'is_equipment',
		'end_user',
		'functionality',
		'engagement_id'
	];

    public function ba()
    {
       return $this->hasOne('BarangayAssembly','activity_id','activity_id');
    }

}
