<?php

class CeacPlansMunController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['username'] = Session::get('username');
		$data['title']    = 'CEAC Plans';

		$data['municipality'] = CeacMuni::get();


		return View::make('ceac.plans.municipality.index')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user 			  = Session::get('username');
		$data['username'] = $user;
		$data['title']    = 'CEAC Plans';
		$kc_class = [];
		$cycles   = [];
		$purposes = [];
		// $kc_class[0] = 0;
		foreach (MunClass::get() as $kc ) {
			$kc_class[$kc->classification] = $kc->classification;
		}
		
		foreach (RfbaPurpose::get() as $purpose) {
			$purposes[$purpose->code] = $purpose->code.'-'.$purpose->description;
		}

		foreach (NCDDP::get_cycles() as $cycle) {
			$cycles[$cycle] = $cycle;
		}
		$office   = Report::get_office($user);
		$psgc_id  = Report::get_muni_psgc($user);
		if($office === 'ACT'){
			$barangay_id = Report::get_psgc_id($psgc_id);
		}
		else if($office === 'RPMO' or $office === 'SRPMO'){
			$barangay_id = Report::get_prov_psgc_id($psgc_id);
		}
		else{
			$barangay_id = Report::get_all_brgy_psgc_id();
		}

		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);
		$barangay_list = array('NULL' => '') + $barangay_list;




		$data['data'] = Municipality::where('municipality_psgc', $user)
														->with('province')
														->with('province.region')->first();

		$data['municipality'] = Municipality::with('province')
													->with('province.region')
													->first();

		$data['barangay_lists'] = $barangay_list;
		$data['cycles'] 		= array('NULL' => 'Choose cycle') + $cycles;
		$data['purposes']		= $purposes;

		return View::make('ceac.plans.municipality.create')->with($data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

			// 

		$rules = array(
			// 'program_id'    => 'required',
			'cycle_id' => 'required',
			'activity_code' => 'required',
			'enddate' => 'required|date',
		
		);

		$validator = Validator::make($input, $rules);

		if ( $validator->fails() ) {	

			return Redirect::to( 'ceac/plans/municipality/create' )
				->withErrors( $validator )
				->withInput( Input::all() );
		
		}
		else
		{	
				Session::flash('message', 'Plan Successfully Updated');
            $input['kc_mode'] = Session::get('accelerated');
            $input['startdate'] = date('Y-m-d',strtotime($input['startdate']));
				$input['enddate'] = date('Y-m-d',strtotime($input['enddate']));
				CeacMuni::create($input);
				return Redirect::to( 'ceac/plans/municipality/' );
			
		}	
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$user 			  = Session::get('username');
		$data['username'] = $user;
		$data['title']    = 'CEAC Plans';
		$kc_class = [];
		$cycles   = [];
		$purposes = [];
		// $kc_class[0] = 0;
		foreach (MunClass::get() as $kc ) {
			$kc_class[$kc->classification] = $kc->classification;
		}
		
		foreach (RfbaPurpose::get() as $purpose) {
			$purposes[$purpose->code] = $purpose->code.'-'.$purpose->description;
		}

		foreach (NCDDP::get_cycles() as $cycle) {
			$cycles[$cycle] = $cycle;
		}
		$office   = Report::get_office($user);
		$psgc_id  = Report::get_muni_psgc($user);
		if($office === 'ACT'){
			$barangay_id = Report::get_psgc_id($psgc_id);
		}
		else if($office === 'RPMO' or $office === 'SRPMO'){
			$barangay_id = Report::get_prov_psgc_id($psgc_id);
		}
		else{
			$barangay_id = Report::get_all_brgy_psgc_id();
		}
		
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);
		$barangay_list = array('NULL' => '') + $barangay_list;

		$data['data'] = Municipality::where('municipality_psgc', $user)
														->with('province')
														->with('province.region')->first();

		$data['municipality'] = Municipality::with('province')
													->with('province.region')
													->first();

		$data['barangay_lists'] = $barangay_list;
		$data['cycles'] 		= array('NULL' => 'Choose cycle') + $cycles;
		$data['purposes']		= $purposes;
	
		$data['ceac'] = CeacMuni::where('psgc_id',$id)->first();
		
		return View::make('ceac.plans.municipality.edit')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();

		$rules = array(
			// 'program_id'    => 'required',
			'cycle_id' => 'required',
			'program_id' => 'required',
			'activity_code' => 'required',
			'enddate' => 'required|date',
		
		);
		$validator = Validator::make($input, $rules);

		if ( $validator->fails() ) {	

			return Redirect::to( 'ceac/plans/municipality/create' )
				->withErrors( $validator )
				->withInput( Input::all() );
		
		}
		else
		{	
				Session::flash('message', 'Plan Successfully Updated');
				unset($input['_token']);
				unset($input['municipality']);
				unset($input['province']);
				unset($input['region']);
				$input['startdate'] = date('Y-m-d',strtotime($input['startdate']));
				$input['enddate'] = date('Y-m-d',strtotime($input['enddate']));
				CeacMuni::where('psgc_id',$id)->update($input);
				return Redirect::to( 'ceac/plans/municipality/' );
			
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		CeacMuni::where('psgc_id',$id)->delete();
		Session::flash('message', ' Successfully Deleted');
		return Redirect::to('ceac/plans/municipality');
	}

	/**  
	 *  function and method for extra functionality of the response 
	 *  
	 *	@return JSON Response
	 *	
	 */
	public function barangayData()
	{
		$input = Input::all();
  	   
		$barangay_id  = $input['barangay_id'];
	
		// get barangay details 
		$barangay_details = Barangay::where('barangay_psgc',$barangay_id)
										->with('municipality')
										->with('province')
										->with('province.region')
										->first();
		
		return Response::json($barangay_details);
	}


	public function lguCycles()
	{
		$input     = Input::all();
		
		$cycle_id  = $input['cycle_id'];

		$cycleData = DB::table('kc_lgucycles')->where('cycle_id',$cycle_id)
										  ->where('lgu_type',1)->first();
	
		return Response::json($cycleData);
	}
}
