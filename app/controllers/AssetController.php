<?php

class AssetController extends \BaseController {
	public function index() {
		$user = Session::get('username');
		$psgc_id = Report::get_muni_psgc($user);

		$assets = Asset::all();

		return $this->view('asset.index')
			->with('username', $user)
			->with('title', 'Assets Management')
			->with('assets', $assets);
	}

	public function show($property_no) {
		$user = Session::get('username');
		$asset = Asset::where(array('property_no' => $property_no))->first();
		$staff = $asset->issued_to_staff();

		return $this->view('asset.show')
			->with('username', $user)
			->with('title', 'Assets Management')
			->with('asset', $asset)
			->with('issued_to_name', $staff->lastname . ", " . $staff->firstname)
			->with('issued_to_position', $staff->curr_position);
	}

	public function create() {
		$user = Session::get('username');

		// get list of staff
		$staff_list = Staff::select(DB::raw('CONCAT(lastname, ", ", firstname) AS full_name'), "employee_id")
			->orderBy("full_name")->lists('full_name', 'employee_id');
		$level_list = ["NPMO" => "NPMO", "RPMO" => "RPMO", "SRPMT" => "SRPMT", "ACT" => "ACT"];
		$asset = new Asset();

		return $this->view('asset.create')
			->with('username', $user)
			->with('title', 'Assets Management')
			->with('asset', $asset)
			->with('staff_list', $staff_list)
			->with('level_list', $level_list);
	}

	public function store() {
		$user = Session::get('username');
		$data = Input::all();
		unset($data["_token"]);

		// store completion report 
		$data['date_acquired'] = $this->setDate($data["date_acquired"]);
		$asset = Asset::create($data);

		Session::flash('success', "Asset successfully created");
		return Redirect::route('asset.show', $data["property_no"]);
	}

	public function edit($property_no) {
		$user = Session::get('username');

		// get list of staff
		$staff_list = Staff::select(DB::raw('CONCAT(lastname, ", ", firstname) AS full_name'), "employee_id")
			->orderBy("full_name")->lists('full_name', 'employee_id');
		$level_list = ["NPMO" => "NPMO", "RPMO" => "RPMO", "SRPMT" => "SRPMT", "ACT" => "ACT"];
		$asset = Asset::where(array("property_no" => $property_no))->first();

		return $this->view('asset.edit')
			->with('username', $user)
			->with('title', "Assets Management")
			->with('asset', $asset)
			->with('staff_list', $staff_list)
			->with('level_list', $level_list);
	}

	public function update($property_no) {
		$user = Session::get('username');

		$asset = Asset::where(array("property_no" => $property_no))->first();

		$data = Input::all();
		unset($data["_token"]);

		// store completion report 
		$data['date_acquired'] = $this->setDate($data["date_acquired"]);
		$asset->fill($data);
		$asset->save();

		Session::flash('success', "Asset successfully updated");
		return Redirect::route('asset.show', $property_no);
	}

	public function destroy($property_no) {
		$asset = Asset::where(array("property_no" => $property_no))->first();
		$asset->delete();

		return Redirect::route('asset.index');
	}
}