<?php

class PSAProblemsAndSolutionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
        $data['username'] = Session::get('username');
        /*PSA Project is a model of psa problems and solution*/
        $data['title']    = 'PSA Priority Problems and Solution';
        $data['id']       = $id;
        $data['problems'] = PSAProjects::where('activity_id',$id)->get();
        $data['position'] = $this->user->getEncoderById($data['username']) == null ?  'ACT' :  $this->user->getEncoderById($data['username']);
        return $this->view('psa.psa_pproblems.index')
                    ->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{
		//
        $data['username'] = Session::get('username');
        /*PSA Project is a model of psa problems and solution*/
        $data['title']    = 'PSA Priority Problems and Solution';
        $data['id']       = $id;
        $data['problem']  = PSAProjects::where(['project_id'=>$id])->first();
        $data['problem_cat'] = NCDDP::get_problem_cat();
        $data['solution_cat'] = NCDDP::get_solution_cat();
        $data['psa_solutions'] = [];
        $data['position'] = $this->user->getEncoderById($data['username']) == null ?  'ACT' :  $this->user->getEncoderById($data['username']);
        return $this->view('psa.psa_pproblems.create')
            ->with($data);
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{
        $input = Input::all();

        $project = PSAProjects::where('rank',$input['rank'])->where('problem_cat',$input['problem_cat'])->where('activity_id',$id)->exists();

        if(PSAProjects::where('rank',$input['rank'])->where('activity_id',$id)->exists())
        {
            Session::flash('message', 'PSA Problem and Solution existing');
            return Redirect::to('psa/'.$id.'/priority-problems/create');

        }
        if(!$project){
            $count = PSAProjects::where('activity_id',$id)->count();
            $project_id = $id.''.($count + 1);
            PSAProjects::create(
                [
                    'project_id'=>$project_id,
                    'activity_id'=>$id,
                    'rank'=>$input['rank'],
                    'problem'=>$input['problem'],
                    'problem_cat'=>$input['problem_cat']
                ]
            );
            if(array_key_exists('solution',$input)){
                foreach($input['solution'] as $key => $solution){
                    Psasolutions::create(
                        [
                            'solution_id'=>$id.''.($count + 1).$key,
                            'project_id'=>$project_id,
                            'activity_id'=>$id,
                            'solution'=>$solution,
                            'solution_cat'=>$input['solution_cat'][$key]
                        ]
                    );
                }
            }
            Session::flash('message', 'PSA Problem and Solution created');
            return Redirect::to('psa/'.$id.'/priority-problems');
        }else{
            Session::flash('error', 'PSA Problem and Solution existing');
            return Redirect::to('psa/'.$id.'/priority-problems/create');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id,$ids)
	{
        $data['username'] = Session::get('username');

        $data['title']    = 'PSA Priority Problems and Solution';
        $data['problem']  = PSAProjects::where(['project_id'=>$ids])->first();
        $data['id'] = $id;
        $data['position'] = $this->user->getEncoderById($data['username']) == null ?  'ACT' :  $this->user->getEncoderById($data['username']);
        $data['psa_solutions'] = Psasolutions::where(['project_id'=>$ids])->get();

        return $this->view('psa.psa_pproblems.show')
            ->with($data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id,$ids)
	{
        $data['username'] = Session::get('username');

            if(Approval::isReviewed($id))
            {
                Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
                return Redirect::to('/psa/'.$id.'/priority-problems');
            }   
        /*PSA Project is a model of psa problems and solution*/
        $data['title']    = 'PSA Priority Problems and Solution';
        $data['id']       = $id;
        $data['ids']  = $ids;
        $data['problem']  = PSAProjects::where(['project_id'=>$ids])->first();
        $data['problem_cat'] = NCDDP::get_problem_cat();
        $data['solution_cat'] = NCDDP::get_solution_cat();
        $data['psa_solutions'] = Psasolutions::where(['project_id'=>$ids])->get();;
        $data['position'] = $this->user->getEncoderById($data['username']) == null ?  'ACT' :  $this->user->getEncoderById($data['username']);
        return $this->view('psa.psa_pproblems.edit')
            ->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,$ids)
	{
        $input = Input::all();

        PSAProjects::where('project_id',$ids)->update([
            'problem'=>$input['problem'],
            'problem_cat'=>$input['problem_cat'],
            'rank'=>$input['rank']
        ]);

        Psasolutions::where('project_id',$ids)->delete();
        if(array_key_exists('solution',$input)){
            foreach($input['solution'] as $key => $solution){
                Psasolutions::create(
                    [
                        'solution_id'=>$ids.''.$key,
                        'project_id'=>$ids,
                        'activity_id'=>$id,
                        'solution'=>$solution,
                        'solution_cat'=>$input['solution_cat'][$key]
                    ]
                );
            }
        }
        Session::flash('message', 'PSA Problem and Solution updated');
        return Redirect::to('psa/'.$id.'/priority-problems');
    }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id,$ids)
	{
        PSAProjects::where('project_id',$ids)->delete();
        Psasolutions::where('project_id',$ids)->delete();

            Session::flash('message', 'PSA Problem and Solution deleted');
        return Redirect::to('psa');
	}


}
