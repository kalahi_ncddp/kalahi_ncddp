<?php

class ServiceProviderController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//

		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['title']	  = 'Service Provider';
		$data['curr_position'] = $this->reference->getCurrentPosition();
		$data['program']  = $this->reference->getPrograms();
		$data['offices'] = $this->reference->getOffices();
		$data['statusofemployment'] = $this->reference->getStatusEmployment();

		$id = $this->user->getPsgcId($data['username']);
//        get the province id
        $province = Municipality::find($id)->isProvince;

        $region   = Municipality::find($id)->province->region->psgc_id;
        $current_cycle = $this->reference->getCycleById($id);


        $data['municipalities'] =  $this->grsRepository->getMunicipalityList($province->province_psgc);
        $data['province'] = $province;
        $data['region'] = $region;

		return $this->view('admindb.category.create')->with($data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
