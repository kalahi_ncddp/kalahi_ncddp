 <?php


class MLPRAPController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// $data['mlpraps'] = DB::table('kc_mlprap')
		// 							->leftJoin('kc_municipality', 'psgc_id', '=', 'municipality_psgc')
		// 							->leftJoin('kc_province', 'kc_municipality.province_psgc', '=', 'kc_province.province_psgc')
		// 							->get();


		$municipality_psgc = Session::get('username');
		$mode = Session::get('accelerated');

        $municipality_psgc = $this->user->getPsgcId($municipality_psgc, $mode);
		$data['mlpraps'] = MLPRAP::orderBy('for_year')
		->where('kc_mode',$mode)
		->get();

		// optional
		$data['municipality'] = Municipality::where('municipality_psgc', $municipality_psgc)->get();
		$data['province'] = Provinces::where('province_psgc', $data['municipality'][0]->province_psgc)->get();

		return $this->view('mlprap.index')->with($data)
										->with('title', 'Municipal Local Poverty Reduction Action Plan')
										->with('username', Session::get('username'));
	}







	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// n/a
	}





	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {

		// For Year picker
		$years = [];
		for ($i = 2020; $i > 2012 ; $i--) {
            $years[$i] =$i;
		}


		// Old - For Province values
		// $province_names = [];
		// $provinces = Provinces::orderBy('province')->get(array('province_psgc', 'province'));
		// foreach( $provinces as $province ) {
		// 	array_push($province_names, $province->province);
		// ->with('province_names', $province_names)
		// }

		return $this->view('mlprap.create')->with('username', Session::get('username'))
										  ->with('title', 'Municipal Local Poverty Reduction Action Plan')
										  ->with('years', $years);
	}




	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$municipality_psgc = Session::get('username');
        $municipality_psgc = $this->user->getPsgcId($municipality_psgc);
		$years = [];
		for ($i = 2014; $i > 1900 ; $i--) { 
			array_push($years, $years);
		}

		$year =  $input['for_year'];
        $to_year = $input['to_year'];
		$data['kc_mode']	    = Session::get('accelerated');


		$validation = MLPRAP::validate(Input::all());
		
		$mlprpexists = MLPRAP::where('for_year',$year)
							->where('to_year',$to_year)
							->where('psgc_id',$municipality_psgc)
							->where('date_prepared',date('Y-m-d',strtotime($input['date_prepared']) ))
							->exists();
		if($mlprpexists){
			return Redirect::route('mlprap.create')->withErrors('This mlprap is existing..')->withInput();

		}
		if( $validation->fails() ) {
			return Redirect::route('mlprap.create')->withErrors($validation)->withInput();
		}
		else {

			// For primary key increments | avoid duplication
			$id = 0;
			$newsquence = $this->getNewCurSequence();
			$newid = 'mlprap'.$municipality_psgc.$newsquence;
			$action_id = MLPRAP::where('action_id', $newid)->get();
			$right_id = $newid;

		
			
			MLPRAP::create(array(
				'action_id'			=> $right_id,
				'psgc_id'			=> $municipality_psgc,
				'for_year' 			=> $year,
                'to_year'           => $to_year,
				'date_prepared' 	=> date('Y-m-d',strtotime($input['date_prepared'])),
                'kc_mode'           => Session::get('accelerated')
			));
			
			return Redirect::route('mlprap.index')
				->with('message', 'MLPRAP has been added successfully .');
		}

	}








	public function edit($id) {


		
  		if(Approval::isReviewed($id))
        {
            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
            return Redirect::to('/mlprap/');
        }	
		$mlprap = MLPRAP::where('action_id', $id)->first();


		// for year dropdown
		$years = [];
		for ($i = 2014; $i > 1900 ; $i--) { 
			$years[$i] = $i;
		}

		// for primary key constraint
		$mlprapdetails_count = MLPRAPDetails::where('plan_id', $id)->count();

		return $this->view('mlprap.edit')->with('mlprap', $mlprap)
										->with('year_key', array_search($mlprap->for_year, $years))
                                        ->with('to_year',array_search($mlprap->to_year, $years))
										->with('years', $years)
										->with('mlprapdetails_count', $mlprapdetails_count)
										->with('username', Session::get('username'))
										->with('title', 'Municipal Local Poverty Reduction Action Plan');
	}

	public function update() {

		$input = Input::all();
		$input['is_draft'] = isset($input['is_draft']) ? 1 : 0; 

		$municipality_psgc = Session::get('username');
        $municipality_psgc = $this->user->getPsgcId($municipality_psgc);
		$years = [];
		for ($i = 2014; $i > 1900 ; $i--) { 
			array_push($years, $i);
		}


		$validation = MLPRAP::validate(Input::all());


		if( $validation->fails() ) {
			return Redirect::route('mlprap.edit', $mlprap_id)->withErrors($validation)->withInput();
		}
		else {

			
			if( Input::get('mlprapdetails_count') > 0 )
			{
				MLPRAP::where('action_id', Input::get('mlprap_id'))->update(array(
					'for_year' 			=> Input::get('for_year'),
                    'to_year' 			=> Input::get('to_year'),

                    'date_prepared' 	=> date('Y-m-d',strtotime( Input::get('date_prepared')))
				));
			}
			else
			{
				$year =  Input::get('for_year');
                $to_year = Input::get('to_year');
				// For primary key increments | avoid duplication
				$id = 0;
				$action_id = MLPRAP::where('action_id', 'mlprap-' . $municipality_psgc . '-' . $year . '-' . $id)->get();
				$right_id = 'mlprap-' . $municipality_psgc . '-' . $year . '-' . $id;

				while( !$action_id->isEmpty() ) {
					$id++;
					$action_id = MLPRAP::where('action_id', 'mlprap-' . $municipality_psgc . '-' . $year . '-' . $id)->get();
					$right_id = 'mlprap-' . $municipality_psgc . '-' . $year . '-' . $id;
				}


				MLPRAP::where('action_id', Input::get('mlprap_id'))->update(array(
					'action_id'			=> $right_id,
					'for_year' 			=> $year,
                    'to_year'           =>$to_year,
					'date_prepared' 	=> date('Y-m-d',strtotime( Input::get('date_prepared')))
				));
			}
				

			return Redirect::route('mlprap.index')->with('message', 'MLPRAP has been updated successfully.');
		}
	}









	public function delete($id) {
		if(Approval::isReviewed($id))
        {
            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
            return Redirect::to('/mlprap/');
        }	
		$MLPRAP = MLPRAP::where('action_id', $id);
		$mlprapdetails_count = MLPRAPDetails::where('plan_id', $id)->count();

		if( $MLPRAP != NULL && $mlprapdetails_count == 0) {
			$MLPRAP->delete();
			return Redirect::route('mlprap.index')->with('message', 'MLPRAP has been deleted successfully.');
		}
		else {
			return Redirect::route('mlprap.index')->with('message', 'Cannot delete MLPRAP. Please delete all action plans first.');	
		}
	}






	// On-change Dropdown
	public function dropdown()
	{
		$input = Input::get('option');

		$province = Provinces::where('province', $input)->get(array('province_psgc'));

		$municipalities = Municipality::where('province_psgc', $province[0]->province_psgc)
										->orderBy('municipality')
										->lists('municipality', 'municipality_psgc');

		return Response::json($municipalities);

	}


}
