<?php

use Repositories\Users\UsersInterface;
use Repositories\Reference\ReferenceInterface;
use Repositories\GRS\GRSInterface;

use Repositories\Reports\ReportsInterface;
use Repositories\SPI\SPIInterface;
use Repositories\Sync\SyncInterface;

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
    public $views;
    protected  $user;
    protected  $reference;
    protected $grsRepository;

    protected $reports;
    protected $syncrepository;

    protected $spi;
    /**
     * @param GrsInterface $grsRepository
     * @param UsersInterface $usersInterface
     * @param ReferenceInterface $reference
     */

    public function __construct(GrsInterface $grsRepository ,
                                UsersInterface $usersInterface,
                                ReferenceInterface $reference,
                                ReportsInterface $reports,
                                SPIInterface $spi,
                                SyncInterface $sync)
    {
        $this->user = $usersInterface;
        $this->reference = $reference;
        $this->grsRepository = $grsRepository;

        $this->reports = $reports;
        $this->syncrepository = $sync;
        $this->spi          = $spi;
    }
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
            $data['position'] = "";
			$this->layout = View::make($this->layout);
		}


	}

    /**
     * initial view function ..to prevent repetitive passing of data to views
     * use this so that you dont have to put the position var in views since it is the in the default layout
     * @param $view
     * @return mixed
     */


    public function view($view)
    {
        $user = Session::get('username');
        $data['comments'] = $this->reference->getComments();
//        echo '<pre>';
        $data['count'] = $this->reference->getCommentToday();
        $data['reference'] = $this->reference;
        $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);



        return View::make($view)->with($data);

    }

    // get Current Sequence of Municipality
    public function getCurSequence()
    {
        $user = Session::get('username');
        $psgc_id = $this->user->getPsgcId($user);
        return Municipality::where('municipality_psgc',$psgc_id)->first()->activity_seq;
    }

    /*
     *
     * Generate a sequence number based from the municipal activity_seq column;
     * Return String 6 sequence number
     *
     */
    public function getNewCurSequence()
    {
        $user = Session::get('username');
        $psgc_id = $this->user->getPsgcId($user);
        $num = Municipality::where('municipality_psgc',$psgc_id)->first()->activity_seq;

        $updated = Municipality::where('municipality_psgc',$psgc_id)->update(['activity_seq'=>$num+1]);
        return sprintf("%06d", $num+1);
    }

    public function barangaySequence($psgc_id)
    {
        $num = Barangay::where('barangay_psgc',$psgc_id)->first()->barangay_seq;

        $updated = Barangay::where('barangay_psgc',$psgc_id)->update(['barangay_seq'=>$num+1]);
        return sprintf("%06d", $num+1);

    }

	/**
	 * Return converted to specified date format .
	 * @param date object date
	 * @return string date
	 */
	public static  function setDate($date)
	{
        if($date!=null)
		    return date('Y-m-d',strtotime($date));
        else
            return null;
	}
}
