<?php

class VolunteerController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
			$user = Session::get('username');
			$mode = Session::get('accelerated');
			$input = Input::get('search');
			$data = Volunteer::get_volunteers($user, $mode,$input);

			return $this->view('volunteer.index')
				->with('title', 'Volunteers Profile')
				->with('username', $user)
				->with('data', $data['volunteers'])
				->with('total',$data['count']);			
	}

	public function getVolunteer()
	{

		$user = Session::get('username');
			$mode = Session::get('accelerated');
		$data = Volunteer::get_volunteers($user, $mode);
		//( 'Reviewed','Region','Province','Barangay','Program','Cycle','Last Name', 'First Name'

        return Datatable::collection($data)
		        ->showColumns('Reviewed','Region','Province','municipality','Barangay','program_id','cycle_id','lastname','Firstname')
		          ->addColumn('Reviewed',function($volunteer){
		          		if(Auth::user()->office_level=='ACT')
		          		{

                           if($volunteer->is_draft==0){

						   		return $this->reference->approval_statusv($volunteer->volunteer_id , 'ACT');	
                           }else{
                              return '<span class="label label-warning">draft</span>';
                           }
                           
		          		}
		          })
		         ->addColumn('Region', function($volunteer) {
		         	return Report::get_region_by_brgy_id($volunteer->psgc_id);
				 })
				 ->addColumn('Province', function($volunteer) {
		         	return Report::get_province_by_brgy_id($volunteer->psgc_id);
				 })
				 ->addColumn('municipality', function($volunteer) {
		         	return Report::get_municipality_by_brgy_id($volunteer->psgc_id);
				 })
				 ->addColumn('Barangay', function($volunteer) {
		         	return  Report::get_barangay($volunteer->psgc_id);
				 })
				 ->addColumn('lastname', function($volunteer) {
					return $volunteer->beneficiary->lastname;
				 })
				 ->addColumn('Firstname', function($volunteer) {
					return $volunteer->beneficiary->firstname;
		         })
				 
		         ->addColumn('Details', function($volunteer) {
		         	return  '<a class="btn btn-success btn" href="'.URL::to('volunteer/' .$volunteer->volunteer_id) .'">
				                         <i class="fa fa-eye"></i>      View Details '. hasComment($volunteer->volunteer_id) .'
				                            </a>';
				 })
		        ->searchColumns('firstname')
		        ->orderColumns('Reviewed','Region','Province','municipality','Barangay','Program','Cycle','Last Name', 'First Name','Details')
		        ->make();
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
			$user = Session::get('username');
			
			$educ_level_list = Volunteer::get_educ_level_list();
			$educ_level_list = array_combine($educ_level_list, $educ_level_list);
			$educ_level_list = array('NULL' => 'Select Education level') + $educ_level_list;
			
			$civil_status_list = Volunteer::get_civilstatus_list();
			$civil_status_list = array_combine($civil_status_list, $civil_status_list);
			$civil_status_list = array('NULL' => 'Select Civil Status') + $civil_status_list;

			$position_blgu = Volunteer::get_position_blgu();
			$position_blgu = array('NULL' => 'Select Current Position') + $position_blgu;

			$data = Volunteer::get_volunteers_list();
			
			return $this->view('volunteer.create')
			->with('title', 'Volunteer')
			->with('educ_level_list', $educ_level_list)
			->with('position_blgu', $position_blgu)
			->with('civil_status_list', $civil_status_list)
			->with('data', $data)
			->with('username', $user);			
	}
	
	public function create_volunteer($id){
			$user = Session::get('username');
			$office = Report::get_office($user);
			$psgc_id = Report::get_muni_psgc($user);

			if($office === 'ACT'){
				$barangay_id = Report::get_psgc_id($psgc_id);
			}
			else if($office === 'RPMO' or $office === 'SRPMO'){
				$barangay_id = Report::get_prov_psgc_id($psgc_id);
			}
			else{
				$barangay_id = Report::get_all_brgy_psgc_id();
			}
			
			$barangay_name = NCDDP::get_barangay_name($barangay_id);
			$barangay_list = array_combine($barangay_id, $barangay_name);
			$barangay_list = array(NULL => 'Select Barangay') + $barangay_list;

			$cycle = Report::get_current_cycle($psgc_id);
			$program = Report::get_current_program($psgc_id);
			$municipality = Report::get_municipality($psgc_id);
			$province = Report::get_province($psgc_id);
			
			$cycle_list = NCDDP::get_cycles();
			$cycle_list = array_combine($cycle_list, $cycle_list);
			$cycle_list = array('NULL' => 'Select Cycle') + $cycle_list;
        $program_list = NCDDP::get_programs();
        $program_list = array_combine($program_list, $program_list);
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
			
			$volcomm_list_id = Volunteer::get_volcomm_list_id();
			$volcomm_list_description = Volunteer::get_volcomm_list_description();
			$volcomm_list = array_combine($volcomm_list_id, $volcomm_list_description);
			$volcomm_list = array('NULL' => 'Select Committee') + $volcomm_list;
			
			$volpost_list = Volunteer::get_volpost_list();
			$volpost_list = array_combine($volpost_list, $volpost_list);
			$volpost_list = array('NULL' => 'Select Position') + $volpost_list;
			
			$training_data = Volunteer::get_memothertrn_by_id($id);
			$org_data = Volunteer::get_memotherorg_by_id($id);
			
			$beneficiary = Volunteer::get_beneficiary_by_id($id);
			
			$data['description'] = '';
			return$this->view('volunteer.create_volunteer')
			->with('title', 'Volunteer Profile Record')
			->with('brgy_list', $barangay_list)
			->with('municipality', $municipality)
			->with('province', $province)
			->with('cycle', $cycle)
			->with('cycle_list', $cycle_list)
			->with('sector_list', $sector_list)
			->with('volcomm_list', $volcomm_list)
			->with('volpost_list', $volpost_list)
                ->with('program_lists',$program_list)
			->with('program', $program)
			->with('training_data', $training_data)
			->with('org_data', $org_data)
			->with('beneficiary', $beneficiary)
			->with('username', $user)
			->with($data);

	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'lastname'   => 'required',
			'firstname'   => 'required',
			
			'birthdate' => 'required'
		);
		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('volunteer/create')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			$beneficiary_id = $data["beneficiary_id"];
            $data['kc_mode']	    = Session::get('accelerated');
			if($beneficiary_id == NULL){
				$beneficiary = Volunteer::get_beneficiary_existing($data["lastname"], $data["firstname"], $data["middlename"], $data["birthdate"]);
				if($beneficiary == NULL){
					unset($data["_token"]);
					unset($data["age"]);
					$data["is_ip"] = Input::has("is_ip")?1:0;
					$data["is_ipleader"] = Input::has("is_ipleader")?1:0;

					
					$psgc_id = Report::get_muni_psgc(Session::get('username'));


                    $data['birthdate']  = $this->setDate($data['birthdate']);

                   $beneficiary_id = Beneficiary::generateBeneficiaryId($data);
				}	
				else{
					$error = 'Volunteer Profile Duplicate. Please use existing Volunteer Profile';
					Session::flash('message', $error);
					return Redirect::to('volunteer/create')
					->withInput(Input::all());	
				}	
			}
			return Redirect::to('volunteer/create_volunteer/'.$beneficiary_id);		
		}
	}
	
	public function store_volunteer(){
			//print_r(Input::all());
        $data['kc_mode']	    = Session::get('accelerated');
        $volunteer = Volunteer::get_volunteer_existing(Input::get('beneficiary_id'), Input::get('psgc_id'), Input::get('program_id'), Input::get('cycle_id'),$data['kc_mode']);
        if($volunteer == NULL){
            $data = Input::all();

            $data['date_appointed'] = $this->setDate($data['date_appointed']);
            $data['aschair_start']  = isset($data['aschair_start']) ?  $this->setDate($data['aschair_start']) : '';
            $data['aschair_end']  = isset($data['aschair_end']) ? $this->setDate($data['aschair_end']) : '';
                unset($data["_token"]);
				unset($data["barangay"]);
				unset($data["kc_code"]);
				DB::table('KC_MemOtherOrg')->where('beneficiary_id',$data["beneficiary_id"])->delete();
				DB::table('KC_MemOtherTrn')->where('beneficiary_id',$data["beneficiary_id"])->delete();

				if(array_key_exists("org_name", $data)){
					$org_data = $data["org_name"];
					$position_org = $data["position_org"];

					unset($data["org_name"]);
					unset($data["position_org"]);

					$i = 0;
					foreach($org_data as $org_name){
						Report::save_ncddp('KC_MemOtherOrg', array('beneficiary_id'=>$data["beneficiary_id"], 'org_name'=>$org_name, 'position'=>$position_org[$i]));
						$i++;
					}
				}
					
				if(array_key_exists("training_title", $data)){
					$training_data = $data["training_title"];
					$training_provider = $data["training_prov"];
					$year_attended = $data["year_attended"];
					unset($data["training_title"]);
					unset($data["training_prov"]);
					unset($data["year_attended"]);
					$i = 0;
					foreach($training_data as $training_title){
						Report::save_ncddp('KC_MemOtherTrn', array('beneficiary_id'=>$data["beneficiary_id"], 'training_title'=>$training_title, 'training_prov'=>$training_provider[$i], 'year_attended'=>$year_attended[$i]));
						$i++;
					}
				}
				

				if(array_key_exists("sector", $data)){
                    $sector_represented = "";
                    foreach($data["sector"] as $sector){
                        $sector_represented = $sector_represented.','.$sector;
                    }
                    unset($data["sector"]);
                    $data["sector_represented"] = ltrim ($sector_represented, ',');
				}

		        $data['kc_mode']	    = Session::get('accelerated');

	            $vol_id = Volunteer::create($data);
				$newsquence = sprintf("%06d", $vol_id->id);
		        $volunteer_id =  'VL'.Input::get('psgc_id').$newsquence;
				
				// set new volunteer id from incremental id
				Volunteer::where('id',$vol_id->id)->update(['volunteer_id'=>$volunteer_id]);

				$data["volunteer_id"] = $volunteer_id;
				$data['kc_mode']      = Session::get('accelerated');
				
            $committee_data = array();
            if(array_key_exists("committee", $data)){
                $committee_data = $data["committee"];
                $position = $data["position"];
                $startdate = $data["startdate"];
                $enddate = $data["enddate"];
                unset($data["committee"]);
                unset($data["position"]);
                unset($data["startdate"]);
                unset($data["enddate"]);

                $i = 0;
                foreach($committee_data as $comm){
                    Report::save_ncddp('KC_VolCommMem', array('volunteer_id'=>$volunteer_id, 'committee'=>$comm, 'position'=>$position[$i], 'startdate'=> $this->setDate($startdate[$i]), 'enddate'=>$this->setDate($enddate[$i])));
                    $i++;
                }
	            }
	           
	            // Report::save_ncddp('kc_volunteers', $data);
				
				$i = 0;
				foreach($committee_data as $comm){
					try{
						Report::save_ncddp('kc_volcommem', array('volunteer_id'=>$volunteer_id, 'committee'=>$comm, 'position'=>$position[$i], 'startdate'=>$this->setDate($startdate[$i]), 'enddate'=>$this->setDate($enddate[$i])));
					}catch(Exception $e){
						
					}
					$i++;
				}
				
				return Redirect::to('volunteer/'.$data["volunteer_id"]);
			}	
			else{
				$error = 'Volunteer Record Existing. <a href = "'.URL::to('volunteer/'.$volunteer).'">'.$volunteer.'</a>';
				Session::flash('message', $error);
				return Redirect::to('volunteer/create_volunteer/'.Input::get('beneficiary_id'))
				->withInput(Input::all());	
			}			
			
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

        $user = Session::get('username');

			$volunteer = DB::table('kc_volunteers')->where('volunteer_id',$id)->first();
			// dd($volunteer);
				$beneficiary = Volunteer::get_beneficiary_by_id($volunteer->beneficiary_id);
        $committee_data = Volunteer::get_volcommmem_by_id($id);
			$training_data = Volunteer::get_memothertrn_by_id($volunteer->beneficiary_id);
			$org_data = Volunteer::get_memotherorg_by_id($volunteer->beneficiary_id);
			
			$data['description'] = $volunteer->volunteer_id;
			return $this->view('volunteer.show')
				->with('title', 'Volunteer Profile')
				->with('username', $user)
				->with('volunteer', $volunteer)
				->with('beneficiary', $beneficiary)
				->with('committee_data', $committee_data)
				->with('training_data', $training_data)
				
				->with('org_data', $org_data)
				->with($data);
	}		

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = Session::get('username');
			$office = Report::get_office($user);

		if(Approval::isReviewed($id))
        {
            Session::flash('message','This data is already reviewed by AC.. Untag this to data');
            return Redirect::to('/volunteer/'.$id);
        }	

        $volunteer = Volunteer::get_volunteer_by_id($id);

        $beneficiary = Volunteer::get_beneficiary_by_id($volunteer->beneficiary_id);
        $committee_data = Volunteer::get_volcommmem_by_id($id);
			$training_data = Volunteer::get_memothertrn_by_id($volunteer->beneficiary_id);
			$org_data = Volunteer::get_memotherorg_by_id($volunteer->beneficiary_id);
			
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
		
			$volcomm_list_id = Volunteer::get_volcomm_list_id();
			$volcomm_list_description = Volunteer::get_volcomm_list_description();
			$volcomm_list = array_combine($volcomm_list_id, $volcomm_list_description);
			$volcomm_list = array('NULL' => 'Select Committee') + $volcomm_list;
			
			$volpost_list = Volunteer::get_volpost_list();
			$volpost_list = array_combine($volpost_list, $volpost_list);
			$volpost_list = array('NULL' => 'Select Position') + $volpost_list;
			
			$civil_status_list = Volunteer::get_civilstatus_list();
			$civil_status_list = array_combine($civil_status_list, $civil_status_list);
			$civil_status_list = array('NULL' => '') + $civil_status_list;

			$educ_level_list = Volunteer::get_educ_level_list();
			$educ_level_list = array_combine($educ_level_list, $educ_level_list);
			$position_blgu = Volunteer::get_position_blgu();
			$position_blgu = array('NULL' => 'Select Curren Position') + $position_blgu;

			$sector_data = explode(",", $volunteer->sector_represented);
			
			return $this->view('volunteer.edit')
			->with('title', 'Volunteer')
			->with('sector_list', $sector_list)
			->with('volcomm_list', $volcomm_list)
			->with('volpost_list', $volpost_list)
			->with('sector_data', $sector_data)
			->with('position_blgu', $position_blgu)
			->with('civil_status_list', $civil_status_list)
			->with('educ_level_list', $educ_level_list)
			->with('training_data', $training_data)
			->with('org_data', $org_data)
			->with('committee_data', $committee_data)
			->with('beneficiary', $beneficiary)
			->with('volunteer', $volunteer)
			->with('username', $user);
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();
				unset($data["_token"]);
				unset($data["_method"]);
        $data['date_appointed'] = $this->setDate($data['date_appointed']);
        $data['birthdate']  = $this->setDate($data['birthdate']);

        $data['is_draft'] = isset($data['is_draft']) ? 1 : 0;
              

                DB::table('KC_MemOtherOrg')->where('beneficiary_id',$data["beneficiary_id"])->delete();
                DB::table('KC_MemOtherTrn')->where('beneficiary_id',$data["beneficiary_id"])->delete();
                DB::table('KC_VolCommMem')->where('volunteer_id',$id)->delete();

				if(array_key_exists("org_name", $data)){
					$org_data = $data["org_name"];
					$position_org = $data["position_org"];
					unset($data["org_name"]);
					unset($data["position_org"]);
					unset($data["startdate_org"]);
					unset($data["enddate_org"]);
					
					$i = 0;
					foreach($org_data as $org_name){
						Report::save_ncddp('KC_MemOtherOrg', array('beneficiary_id'=>$data["beneficiary_id"], 'org_name'=>$org_name, 'position'=>$position_org[$i]));
						$i++;
					}
				}
					
				if(array_key_exists("training_title", $data)){
					$training_data = $data["training_title"];
					$training_provider = $data["training_prov"];
					$year_attended = $data["year_attended"];
					unset($data["training_title"]);
					unset($data["training_prov"]);
					unset($data["year_attended"]);
					$i = 0;
					foreach($training_data as $training_title){
						Report::save_ncddp('KC_MemOtherTrn', array('beneficiary_id'=>$data["beneficiary_id"], 'training_title'=>$training_title, 'training_prov'=>$training_provider[$i], 'year_attended'=>$year_attended[$i]));
						$i++;
					}
				}
				
				$committee_data = array();
				if(array_key_exists("committee", $data)){
					$committee_data = $data["committee"];

					$position = $data["position"];
					$startdate = $data["startdate"];
					$enddate = $data["enddate"];
					unset($data["committee"]);
					unset($data["position"]);
					unset($data["startdate"]);
					unset($data["enddate"]);
					$i = 0;
					foreach($committee_data as $comm){
						Report::save_ncddp('KC_VolCommMem', array('volunteer_id'=>$id, 'committee'=>$comm, 'position'=>$position[$i], 'startdate'=> $this->setDate($startdate[$i]), 'enddate'=>$this->setDate($enddate[$i])));
						$i++;
					}
				}
				
			
				
				$beneficiary_data = array('beneficiary_id'=> $data["beneficiary_id"],
				'lastname'=> $data["lastname"], 
				'firstname'=> $data["firstname"], 
				'middlename'=> $data["middlename"],
				'birthdate'=> $data["birthdate"],
				'sex'=> $data["sex"],
				'civil_status'=> $data["civil_status"],
				'no_children'=> $data["no_children"],
                'contact_no'=>$data['contact_no'],
                'current_position_blgu'=>$data['current_position_blgu'],
				'address'=> $data["address"],
				'educ_attainment'=> $data["educ_attainment"],
				'occupation'=> $data["occupation"],
				'is_ip'=> array_key_exists("is_ip", $data)?1:0,
				'is_ipleader'=> array_key_exists("is_ipleader", $data)?1:0,
                'is_slp'=> array_key_exists("is_slpbene", $data)?1:0,
                'is_pantawid'=> array_key_exists("is_ppbene", $data)?1:0);



				$volunteer_data = array('volunteer_id'=> $id,
				'date_appointed'=> $data["date_appointed"], 
				'aschair_start'=> array_key_exists("aschair_start",$data)? $this->setDate($data["aschair_start"]):NULL,
				'aschair_end'=> array_key_exists("aschair_end",$data)? $this->setDate($data["aschair_end"]):NULL,
				'is_bspmc'=> array_key_exists("is_bspmc", $data)?'1':'0',
				'is_ppbene'=> array_key_exists("is_ppbene", $data)?'1':'0',
				'is_slpbene'=> array_key_exists("is_slpbene", $data)?'1':'0',
				'is_ppleader'=> array_key_exists("is_ppleader", $data)?'1':'0',
				'is_slpleader'=> array_key_exists("is_slpleader", $data)?'1':'0',
				'is_draft' => $data['is_draft']);

                if(array_key_exists("sector", $data)){

                    $sector_represented = "";
                    foreach($data["sector"] as $sector){
                        $sector_represented = $sector_represented.','.$sector;
                    }
                    unset($data["sector"]);
                    $volunteer_data["sector_represented"] = ltrim ($sector_represented, ',');

                }

				Volunteer::update_beneficiary('KC_Beneficiary', $beneficiary_data);
				Volunteer::update_volunteer('KC_Volunteers', $volunteer_data);
				
				Session::flash('message', 'Volunteer Record Successfully updated');
				
				
				return Redirect::to('volunteer/'.$id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

        if($id[0] == 'V'){
            $bene_id = Volunteer::where('volunteer_id',$id)->first()->beneficiary_id;
            Volunteer::delete_volunteer('KC_VolCommMem', $id);
            Volunteer::where('volunteer_id', $id);

            Trnparticipants::where('beneficiary_id',$bene_id)->update(['is_volunteer'=>0]);
			Session::flash('message', 'Volunteer Record Successfully deleted!');
		}else{
            DB::table('KC_MemOtherOrg')->where('beneficiary_id', $id)->delete();
            DB::table('KC_MemOtherTrn')->where('beneficiary_id', $id)->delete();

			$volunteers = Volunteer::get_volunteers_by_beneficiary_id($id);
            Trnparticipants::where('beneficiary_id',$id)->update(['is_volunteer'=>0]);

			foreach($volunteers as $volunteer){
				Volunteer::delete_volunteer('KC_VolCommMem', $volunteer);
				Volunteer::delete_volunteer('KC_Volunteers', $volunteer);
			}
			Beneficiary::where('beneficiary_id', $id)->delete();
			Session::flash('message', 'Volunteer Profile and Records Successfully deleted!');
		}
		
		return Redirect::to('volunteer');
	}
	
	
}
