<?php

class LGUEngagementController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Session::get('username');
		$mode = Session::get('accelerated');

		$data = LGUEngagement::get_lgu_engagements($user, $mode);

		return $this->view('lguengagement.index')
			->with('title', 'PTA Integration Plans Checklist')
			->with('username', $user)
			->with('data', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
			$user = Session::get('username');
			$office = Report::get_office($user);
			$psgc_id = Report::get_muni_psgc($user);
			
			$cycle = Report::get_current_cycle($psgc_id);
			$program = Report::get_current_program($psgc_id);
			$municipality = Report::get_municipality($psgc_id);
			$province = Report::get_province($psgc_id);
			
			$cycle_list = NCDDP::get_cycles();
			$cycle_list = array_combine($cycle_list, $cycle_list);
			
			$program_list = NCDDP::get_programs();
			$program_list = array_combine($program_list, $program_list);

			return $this->view('lguengagement.create')
			->with('title', 'PTA Integration Plans Checklist')
			->with('municipality', $municipality)
			->with('psgc_id', $psgc_id)
			->with('province', $province)
			->with('cycle', $cycle)
			->with('cycle_list', $cycle_list)
			->with('program', $program)
			->with('program_list', $program_list)
			->with('username', $user);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
			$lguengagement = LGUEngagement::get_lgu_engagement_existing(Input::get('psgc_id'), Input::get('program_id'), Input::get('cycle_id'), Session::get('accelerated') );
			if($lguengagement == NULL){
				$data = Input::all();
				unset($data["_token"]);
				unset($data["kc_code"]);
				unset($data["municipality"]);
				unset($data["province"]);
				
				$newsquence = $this->getNewCurSequence();
				$engagement_id = 'LE'.Input::get('psgc_id').$newsquence;
				
				$data['engagement_id'] = $engagement_id;
				$data['municipal_psgc'] = $data['psgc_id'];
				$data['kc_mode']	    = Session::get('accelerated');
				
				unset($data['psgc_id']);
				/*START Insert CEAC table for reference*/
				$this->reference->insertCeac([
					'psgc_id' => $data['municipal_psgc'],
					'program_id' => $data['program_id'],
					'cycle_id'	=> $data['cycle_id'],
            		'start_date' => $this->setDate($data['moa_signed_date']),
					'end_date' =>''
					],
					$engagement_id,
					'PTA',
					'SIGNING'
					);
				/*END*/

				LGUEngagement::insert($data);
				
				
				return Redirect::to('lguengagement/'.$engagement_id.'/edit');
			}	
			else{
				$error = 'PTA Integration Plans Checklist Existing. <a href = "'.URL::to('lguengagement/'.$lguengagement).'">'.$lguengagement.'</a>';
				Session::flash('message', $error);
				return Redirect::to('lguengagement/create')
				->withInput(Input::all());	
			}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = Session::get('username');
		$lguengagement = LGUEngagement::find($id);
		$lguengagementdates = LGUEngagement::get_lgu_engagement_other_data('kc_lguengagementdates', $id);
		$lgumdcequipments = Lgumdcequipments::where('engagement_id',$id)->get();
			
		$data['description'] = $id;
		
		return $this->view('lguengagement.show')
				->with('title', 'PTA Integration Plans Checklist')
				->with('username', $user)
				->with('lguengagement', $lguengagement)
				->with('lguengagementdates', $lguengagementdates)
				->with('lgumdcequipments',$lgumdcequipments)
				->with($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = Session::get('username');
		$lguengagement = LGUEngagement::find($id);
		$lguengagementdates = LGUEngagement::get_lgu_engagement_other_data('kc_lguengagementdates', $id);
		$lgumdcequipments = Lgumdcequipments::where('engagement_id',$id)->get();
		$data['description'] = $id;
		
		return $this->view('lguengagement.edit')
				->with('title', 'PTA Integration Plans Checklist')
				->with('username', $user)
				->with('lguengagement', $lguengagement)
				->with('lguengagementdates', $lguengagementdates)
				->with('lgumdcequipments',$lgumdcequipments)
				->with($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	 public function update($id)
	{
		$data = Input::all();
		$data['engagement_id'] = $id;
		$data['is_draft'] = isset($data['is_draft']) ? 1 : 0; 


		unset($data["_token"]);
		unset($data["_method"]);
		unset($data["d"]);
		LGUEngagement::delete_lgu_engagement_other_data('kc_lguengagementdates', $id);
		if(array_key_exists("date_conducted", $data)){
			$date_conducted = $data["date_conducted"];
			$activity_type = $data["activity_type"];
			unset($data["date_conducted"]);
			unset($data["activity_type"]);
			
			$i = 0;
			foreach($date_conducted as $date){
				LGUEngagement::save_lgu_engagement_other_data('kc_lguengagementdates', array('engagement_id'=>$id, 'date_conducted'=>$date, 'activity_type'=>$activity_type[$i] ));
				$i++;
			}
		}
		// dd();
		lgumdcequipments::where("engagement_id",$id)->delete();
		if(array_key_exists("equipment_type", $data)){
			foreach ($data["equipment_type"] as $key => $value) {
			// dd($data['end_user'][$key]);
				
				lgumdcequipments::create([
						"engagement_id" => $id,
						"equipment_type"=>$data['equipment_type'][$key],
						"quantity"=>$data['quantity'][$key],
						"date"=>isset($data['equipment_date'][$key]) ? $this->setDate($data['equipment_date'][$key]) : null,
						"end_user"=>$data["end_user"][$key],
						"functionality"=>$data["functionality"][$key]
				]);
			}
		}
		unset($data['equipment_type']);
		unset($data['quantity']);
		unset($data['equipment_date']);
		unset($data["end_user"]);
		unset($data["functionality"]);

		
		LGUEngagement::where('engagement_id', $data['engagement_id'])
		->update($data);
		
		
		Session::flash('message', 'PTA Integration Plans Checklist Successfully updated');
		return Redirect::to('lguengagement/'.$id);		
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        lgumdcequipments::where("engagement_id",$id)->delete();
        LGUEngagement::delete_lgu_engagement_other_data('kc_lguengagementdates', $id);
        LGUEngagement::where('engagement_id',$id)->delete();
        Session::flash('message','Successfuly Deleted');
		return Redirect::to('lguengagement/');
		//no delete
	}
}