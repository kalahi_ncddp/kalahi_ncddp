<?php

class SPISETController extends \BaseController {
	public function show($project_id, $id) {
		$user = Session::get('username');
		$profile = SPIProfile::where(array('project_id' => $project_id))->first();
		$proposal = ProjProposal::where(array('project_id' => $profile->mibf_refno))->first();
		$set = SPISET::where(array('id' => $id))->first();

		return $this->view('spi_set.show')
			->with('username', $user)
			->with('title', 'Sub Project Sustainability Evaluation Tool (SET)')
			->with('description', $project_id)
			->with('set', $set)
			->with('profile', $profile)
			->with('proposal', $proposal);
	}

	public function create($id) {
		$user = Session::get('username');

		$profile = SPIProfile::where(array('project_id' => $id))->first();
		$proposal = ProjProposal::where(array('project_id' => $profile->mibf_refno))->first();

		// get list of implementation modes
		$implem_modes = SPIImplementationMode::lists('mode', 'mode_id');

		// get default weights
		$weights = SPISETWeights::first();

		
			$set = new SPISET(array(
				'project_id'								=> $id,
				'sp_utilization_wt'					=> empty($weights) ?$weights :$weights->sp_utilization,
				'org_and_mgmt_wt'						=> empty($weights) ?$weights :$weights->org_and_mgmt,
				'institutional_linkage_wt'	=> empty($weights) ?$weights :$weights->institutional_linkage,
				'financial_component_wt'		=> empty($weights) ?$weights :$weights->financial_component,
				'physical_technical_wt'			=> empty($weights) ?$weights :$weights->physical_technical
			));
		 	

		return $this->view('spi_set.create')
			->with('username', $user)
			->with('title', 'Sub Project Sustainability Evaluation Tool (SET)')
			->with('set', $set)
			->with('profile', $profile)
			->with('proposal', $proposal)
			->with('description', $set->project_id)
			->with('implem_modes', !empty($implem_modes) ? $implem_modes: []) ;
	}

	public function store() {
		$user = Session::get('username');
		$data = Input::all();
		unset($data["_token"]);

		// update date completed
		$profile = SPIProfile::where(array('project_id' => $data['project_id']))->first();
		$profile->date_completed = $this->setDate($data["date_completed"]);
		$profile->save();

		// store completion report 
		unset($data['date_completed']);
		$data['date_evaluated'] = $this->setDate($data["date_evaluated"]);
		$set = SPISET::create($data);

		Session::flash('success', "SP Completion Report successfully created");
		return Redirect::route('spi_profile.set.show', array($data['project_id'], $set->id));
	}

	public function edit($project_id, $id) {
		$user = Session::get('username');
		$set = SPISET::where(array('id' => $id))->first();
		$profile = SPIProfile::where(array('project_id' => $project_id))->first();
		$proposal = ProjProposal::where(array('project_id' => $profile->mibf_refno))->first();
		$implem_modes = SPIImplementationMode::lists('mode', 'mode_id');

		return $this->view('spi_set.edit')
			->with('username', $user)
			->with('title', 'Sub Project Sustainability Evaluation Tool (SET)')
			->with('set', $set)
			->with('profile', $profile)
			->with('proposal', $proposal)
			->with('description', $set->project_id)
			->with('implem_modes', $implem_modes);
	}

	public function update($project_id, $id) {
		$user = Session::get('username');
		$set = SPISET::where(array('id' => $id))->first();

		$data = Input::all();
		unset($data["_token"]);

		// update date completed
		$profile = SPIProfile::where(array('project_id' => $data['project_id']))->first();
		$profile->date_completed = $this->setDate($data["date_completed"]);
		$profile->save();

		// store completion report 
		unset($data['date_completed']);
		$data['date_evaluated'] = $this->setDate($data["date_evaluated"]);
		$set->fill($data);
		$set->save();

		Session::flash('success', "Sub Project SET successfully updated");
		return Redirect::route('spi_profile.set.show', array($set->project_id, $id));
	}

	public function destroy($project_id, $id) {
		$set = SPISET::where(array('id' => $id))->first();
		$set->delete();

		return Redirect::route('spi_profile.show', $project_id);
	}
}