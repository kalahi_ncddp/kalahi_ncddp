<?php

class SubProjDisasterController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		//$monolog->addInfo('Log Message', array('Yo'));
		$mode = Session::get('accelerated');
		$search = Session::get('search');

		$user         = Session::get('username');
		$muni_psgc    = $this->user->getPsgcId($user);
		$province      = Report::get_province($muni_psgc);
		$municipality = Report::get_municipality($muni_psgc);
		$projects     = SPIProfile::get_municipality_projects($muni_psgc,$search);
		$program       = Report::get_current_program($muni_psgc);
		$cycle         = Report::get_current_cycle($muni_psgc);
		$disasters     = SPIProjDisaster::allFromMunicipality($muni_psgc,$mode)->get();
		// dd($disasters);	
		$store_status   = Session::pull('spi-disaster.store', null);
		$update_status  = Session::pull('spi-disaster.update', null);
		$destroy_status = Session::pull('spi-disaster.destroy', null);
		$functionality_sp = FunctionalitySP::all();

		// dd($functionality_sp);
		$monolog->addInfo('Functionality SP', array($functionality_sp));
		return $this->view('spidisaster.index')
			->with('title',             'Damaged Sub-Project by Disaster')
			->with('username',          $user)
			->with('province',          $province)
			->with('municipality',      $municipality)
			->with('municipality_psgc', $muni_psgc)
			->with('program',           $program)
			->with('cycle',             $cycle)
			->with('store_status',      $store_status)
			->with('update_status',     $update_status)
			->with('destroy_status',    $destroy_status)
			->with('disasters',         $disasters)
			->with('functionality_sp',  $functionality_sp);

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user          = Session::get('username');
		$muni_psgc     = $this->user->getPsgcId($user);
		$municipality  = Report::get_municipality($muni_psgc);
		$province      = Report::get_province($muni_psgc);
		$barangay_id   = Report::get_psgc_id($muni_psgc);
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);
		$program       = Report::get_current_program($muni_psgc);
		$cycle         = Report::get_current_cycle($muni_psgc);
		$all_projects  = SPIProfile::get_municipality_projects($muni_psgc);
		$functionality_sp = FunctionalitySP::all();

		return $this->view('spidisaster.create')
			->with('title', 'Damaged Sub-Project by Disaster')
			->with('username',          $user)
			->with('municipality',      $municipality)
			->with('municipality_psgc', $muni_psgc)
			->with('province',          $province)
			->with('barangays',         $barangay_list)
			->with('program',           $program)
			->with('cycle',             $cycle)
			->with('all_projects',      $all_projects)
			->with('functionality_sp',  $functionality_sp);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$is_successful       = FALSE;
		$in_calamity_type    = Input::get('in_calamity_type',    false);
		$in_calamity_desc    = Input::get('in_calamity_desc',    false);
		$in_dateoccured      = Input::get('in-dateoccured',      false);
	    $in_municipalitypsgc = Input::get('in-municipalitypsgc', false);
	    
	    if(!($in_calamity_type  === false || $in_calamity_desc === false ||
	    	 $in_dateoccured   === false || $in_municipalitypsgc  === false))
	    {
			$spidisaster = new SPIProjDisaster;
			$spidisaster->municipal_psgc = $in_municipalitypsgc;
			$spidisaster->calamity_type  = $in_calamity_type;
			$spidisaster->description    = $in_calamity_desc;
			$spidisaster->kc_mode = Session::get('accelerated');

			$spidisaster->date_occured   = date("Y-m-d", strtotime($in_dateoccured));
			$spidisaster->save();

			$is_successful = TRUE;
	    }

		Session::put("spi-disaster.store", $is_successful);		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user          = Session::get('username');
		$muni_psgc     = $this->user->getPsgcId($user);
		$municipality  = Report::get_municipality($muni_psgc);
		$province      = Report::get_province($muni_psgc);
		$barangay_id   = Report::get_psgc_id($muni_psgc);
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);
		$program       = Report::get_current_program($muni_psgc);
		$cycle         = Report::get_current_cycle($muni_psgc);
		$all_projects  = SPIProfile::get_municipality_projects($muni_psgc,Input::get('search'));
		$disaster      = SPIProjDisaster::find($id);

		$store_status   = Session::pull("spi-disaster.$id.store", null);
		$update_status  = Session::pull("spi-disaster.$id.update", null);
		$destroy_status = Session::pull("spi-disaster.$id.destroy", null);
		$functionality_sp = FunctionalitySP::all();
		return $this->view('spidisaster.edit')
			->with('title', 'Damaged Sub-Project by Disaster')
			->with('username',          $user)
			->with('municipality',      $municipality)
			->with('municipality_psgc', $muni_psgc)
			->with('province',          $province)
			->with('barangays',         $barangay_list)
			->with('program',           $program)
			->with('cycle',             $cycle)
			->with('disaster',          $disaster)
			->with('store_status',      $store_status)
			->with('update_status',     $update_status)
			->with('destroy_status',    $destroy_status)
			->with('all_projects',      $all_projects)
			->with('functionality_sp',  $functionality_sp);

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$is_successful       = FALSE;
		$method = Input::get('method', false);

		if($method == 'add')
		{
			$project_disaster_id = $id;
			$barangay_psgc       = Input::get('in-barangay',   false);
			$project_id          = Input::get('in-subproject', false);
			$est_damages         = Input::get('in-estdamages', false);
			$est_cost            = Input::get('in-estcost', false);
            $program             = Input::get('in-projprogram',false);
            $functionality       = Input::get('in-functionality_sp',false);
            $total_sp            = Input::get('in-total_sp', false);
	    $cycle = Input::get('in-cycle', false);

            $date_completed      = $this->setDate(Input::get('in-datecompltd', false));

		    if(!($project_disaster_id  === false || $barangay_psgc === false || 
		    	 $project_id           === false || $est_damages   === false || 
		    	 $est_cost             === false))
		    {
		    	
		    	$spidisaster = SPIProjDisaster::find($project_disaster_id);
		    	$count = SPIProjDisasterItems::where('projdisaster_id',$project_disaster_id)->count()  + 1;
                $newid = $project_disaster_id .'-'.$count;
		    	/*REMOVE THE CONNECTION TO SPI PROFILE */
                $spidisaster->items()->save(new SPIProjDisasterItems([
		    		'projdisaster_id' => $project_disaster_id,
		    	    'barangay_psgc'   => $barangay_psgc,
		    	    'project_id'      => $project_id,
		    	    'est_damages'     => $est_damages,
		    	    'est_cost'        => $est_cost,
                    'project_name'    => $project_id, // supposedly sub project name
                    'fund_source'     => $program,
                    'total_sp'        => $total_sp,
                    'cycle_id'			=>$cycle,
                    'functionality_sp'   => $functionality,
                    'date_completed'  => $date_completed
		    	]));

		    	$is_successful        = TRUE;
		    	Session::put("spi-disaster.$id.store", $is_successful);		
		    }
		}
		else if($method == 'delete')
		{
			$project_disaster_item_id = Input::get('del-id', false);
			$affectedRows = SPIProjDisasterItems::find($project_disaster_item_id)->delete();
			$is_successful = ($affectedRows > 0) ? TRUE : FALSE;
			Session::put("spi-disaster.$id.destroy", $is_successful);		
		}
		else if($method == 'update')
		{
			$project_disaster_id      = $id;
			$project_disaster_item_id = Input::get('edit-id',   false);
			$barangay_psgc            = Input::get('edit-barangay',   false);
			$project_id               = Input::get('in-subproject', false);
			$est_damages              = Input::get('edit-estdamages', false);
			$est_cost                 = Input::get('edit-estcost', false);
            $program             = Input::get('edit-projprogram',false);
            $functionality       = Input::get('edit-functionality_sp',false);
            $total_sp            = Input::get('edit-total_sp', false);
//dd(['barangay_psgc' =>$barangay_psgc,
//                     'est_damages'     => $est_damages,
//                     'est_cost'        => $est_cost,
//                     'project_name'    => $project_id, // supposedly sub project name
//                     'fund_source'     => $program,
//                     'total_sp'        => $total_sp,
//                     'functionality_sp'   => $functionality,
//                     ]);
		    if(!($project_disaster_id  === false || $barangay_psgc === false || 
		    	 $project_id           === false || $est_damages   === false || 
		    	 $est_cost             === false))
		    {
		    	$spidisaster_item = SPIProjDisasterItems::find($project_disaster_item_id);
		    	 $spidisaster_item->update([ 'barangay_psgc'   => $barangay_psgc,
//                     'project_id'      => $project_id,
                     'barangay_psgc' =>$barangay_psgc,
                     'est_damages'     => $est_damages,
                     'est_cost'        => $est_cost,
                     'project_name'    => $project_id, // supposedly sub project name
                     'fund_source'     => $program,
                     'total_sp'        => $total_sp,
                     'functionality_sp'   => $functionality,
                     ]);
		    }
		    $is_successful        = TRUE;
		    Session::put("spi-disaster.$id.update", $is_successful);		
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$affectedRows = SPIProjDisaster::find($id)->delete();
		$is_successful = ($affectedRows > 0) ? TRUE : FALSE;
		Session::put('spi-disaster.destroy', $is_successful);		
	}


    public function export($id)
    {

        Excel::create('disaster', function($excel) use ($id) {
            


            $disaster      = SPIProjDisaster::find($id)->items;


            $data = $disaster->toArray();//get the array from model
            /* CREATE FIRST SHEET*/
            $excel->sheet('Sheetname', function($sheet) use($data) {
                /*INSERT SHEET FROM ARRAY*/
                $sheet->fromArray($data);

            });
        })->export('xlsx');


    }
}
