<?php


class UserManagementController extends \BaseController {



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $id = Session::get('username');
        $data['username'] = $id;
        $id = $this->user->getPsgcId($id);
        $data['title'] = 'User Management';
        $data['users'] = $this->user->getAllEncoderByMuni($id);

        $data['position'] = $this->user->getEncoderById($id) == null ?  'ACT' :  $this->user->getEncoderById($id);

        return $this->view('User.index')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $id = Session::get('username');

        //   get the province id
        $id = $this->user->getPsgcId($id);
        $office_level = $this->user->getOfficelevelByPsgc($id);

        $data['position'] = $this->user->getEncoderById($id) == null ?  'ACT' :  $this->user->getEncoderById($id);


        $municipality =  Municipality::find($id);
        $province = $municipality->isProvince;
        $region   = Municipality::find($id)->province->region->psgc_id;
        $current_cycle = $this->reference->getCycleById($id);

        $data['username'] = $id;
        $data['title'] = 'Add New User';
        $data['barangay'] = $this->grsRepository->getBarangayList($id);
        $data['municipalities'] =  $this->grsRepository->getMunicipalityList($province->province_psgc);

        $data['positions']   = [ 'Encoder'=>'Encoder','ACT'=>'Area Coordinator',
                                'Technical Facilitator'=>'Technical Facilitator',
                                'Commmunity Empowerment Facilitator'=>'Commmunity Empowerment Facilitator',
                                'Commmunity Finance Facilitator'=>'Commmunity Finance Facilitator'];

        $data['municipality'] = $municipality->municipality_psgc;

        return $this->view('User.create')->with($data);

    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $input = Input::all();
        $rules = array(
            'username'   => 'required',
            'first_name'   => 'required',
            'last_name'    => 'required',
            'position'    => 'required',
            'password' => 'required',
        );
        $validator = Validator::make($input, $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('user/create')
                ->withErrors($validator)
                ->withInput($input);
        } else {

            $insert = $this->user->insertNewUser($input);
            if($insert){

               Session::flash('message','Successfully added '.$input['first_name']);
                return Redirect::to('user');
            }
            else
            {
                Session::flash('message','error please try again');
                return Redirect::to('user');
            }
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $ids = Session::get('username');
        $municipality =  Municipality::find($ids);
        $province = $municipality->isProvince;
        $region   = Municipality::find($ids)->province->region->psgc_id;
        $current_cycle = $this->reference->getCycleById($ids);

        $data['username'] = $ids;

        $data['user'] = $this->user->getEncoder($id);

        $data['barangay'] = $this->grsRepository->getBarangayList($ids);
        $data['municipalities'] =  $this->grsRepository->getMunicipalityList($province->province_psgc);

        // $data['positions']   = [ 'encoder'=>'encoder','ac'=>'AC'];
         $data['positions']   = [ 'Encoder'=>'Encoder','ACT'=>'Area Coordinator','Technical Facilitator'=>'Technical Facilitator','Commmunity Empowerment Facilitator'=>'Commmunity Empowerment Facilitator','Municipal Financial Analyst'=>'Municipal Financial Analyst (MFA)'];


        $data['municipality'] = $municipality->municipality_psgc;
        $data['title'] = 'Edit Information';

        return $this->view('User.edit')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $input = Input::all();
        $rules = array(
            'username'   => 'required',
            'first_name'   => 'required',
            'last_name'    => 'required',
            'position'    => 'required',
        );
        $validator = Validator::make($input, $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::previous()
                ->withErrors($validator)
                ->withInput($input);
        } else {

            $insert = $this->user->updateUser($id,$input);
            if($insert){

                Session::flash('message','Successfully updated '.$input['first_name']);
                return Redirect::to('user');
            }
            else
            {
                Session::flash('message','error please try again');
                return Redirect::to('user');
            }
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        User::find($id)->delete();
		Session::flash('message','Successfully deleted '.$id);
        return Redirect::to('user');
	}


}
