<?php

class SPITechAssistController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//$monolog - for debug only
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', array('Test'));

		$user           = Session::get('username');
		$psgc_id        = Report::get_muni_psgc($user);
		$municipality   = Report::get_municipality($psgc_id);
		$province       = Report::get_province($psgc_id);
		$barangay_id    = Report::get_psgc_id($psgc_id);
		// dd($barangay_id);
		$barangay_name  = NCDDP::get_barangay_name($barangay_id);
		$barangay_list  = array_combine($barangay_id, $barangay_name);
		$program        = Report::get_current_program($psgc_id);
		$cycle          = Report::get_current_cycle($psgc_id);
		$mode = Session::get('accelerated');
		$assistances    = SPITechAssist::where('kc_mode',$mode)
						->where('municipal_psgc', '=', $psgc_id )->get();
		$activities     = CeacActivties::all();
		$tech_category  = SPITechCategory::all();
		$tech_provider  = SPITechProvider::all();
		$tech_provider_to = SPITechProviderTo::all();
		
		$store_status   = Session::pull('spi-techassist.store', null);
		$update_status  = Session::pull('spi-techassist.update', null);
		$destroy_status = Session::pull('spi-techassist.destroy', null);

		$monolog->addInfo('Activities', array($activities));
		$monolog->addInfo('Tech Category', array($tech_category));
		$monolog->addInfo('Tech Provider', array($tech_provider));
		$monolog->addInfo('Tech Provider To', array($tech_provider_to));
		$monolog->addInfo('Barangay', array($barangay_list));
		$monolog->addInfo('Assistances', array($assistances));

		return $this->view('spitechassist.index')
			->with('title',             'Technical Assistance')
			->with('username',          $user)
			->with('municipality',      $municipality)
			->with('barangays',         $barangay_list)
			->with('municipality_psgc', $psgc_id)
			->with('province',          $province)
			->with('program',           $program)
			->with('cycle',             $cycle)
			->with('assistances',       $assistances)
			->with('activities',        $activities)
			->with('tech_category',     $tech_category)
			->with('tech_provider',     $tech_provider)
			->with('tech_provider_to',  $tech_provider_to)
			->with('store_status',      $store_status)
			->with('update_status',     $update_status)
			->with('destroy_status',    $destroy_status);

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//$monolog - for debug only
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', array('Test'));
        $newsequence = $this->getNewCurSequence();
        $is_successful = FALSE;
		$id            = Input::get('in-barangay').''.$newsequence;
		$program_id    = Input::get('in-programid',        false);
		$cycle_id      = Input::get('in-cycleid',          false);
	    $municipality  = Input::get('in-municipalitypsgc', false);
		$barangay      = Input::get('in-barangay',         false);
		$ceac_acticity = Input::get('in-ceacactivity',     false);
		$date_from     = Input::get('in-datefrom',         false);
		$date_to       = Input::get('in-dateto',           false);
		$category      = Input::get('in-category',         false);
		$provided_by   = Input::get('in-providedby',       false);
		$provided_to   = Input::get('in-providedto',       false);
		$details       = Input::get('in-details',          false);
		$purpose 	   = Input::get('in-purpose',			false);
		 $data['kc_mode'] = Session::get('accelerated');
		if(!($program_id  === false || $cycle_id      === false || $municipality  === false ||
		     $barangay    === false || $ceac_acticity === false || $date_from     === false ||
		     $date_to     === false || $category      === false || $provided_by   === false ||
		     $provided_to   === false ||$details     === false || $purpose       === false))
		{
			$monolog->addInfo('Log Message', array('All inputs are complete'));

			$spi_techassist = new SPITechAssist;
            $spi_techassist->id             = $id;
			$spi_techassist->municipal_psgc = $municipality;
			$spi_techassist->barangay_psgc  = $barangay;
			$spi_techassist->program_id     = $program_id;
			$spi_techassist->cycle_id       = $cycle_id;
			$spi_techassist->startdate      = date("Y-m-d", strtotime($date_from));
			$spi_techassist->enddate        = date("Y-m-d", strtotime($date_to));
			$spi_techassist->activity_code  = $ceac_acticity;
			$spi_techassist->provider       = $provided_by;
			$spi_techassist->provider_to       = $provided_to;
			$spi_techassist->tech_category  = $category;
			$spi_techassist->tech_details   = $details;
			$spi_techassist->purpose 		= $purpose;
			$spi_techassist->kc_mode		= $data['kc_mode'];
			$spi_techassist->save();
			$id = $spi_techassist->id;
			$is_successful = ($id !== NULL) ? TRUE : FALSE;
		}

		$monolog->addInfo('Log Message', array('id' => $id));
		Session::put('spi-techassist.store', $is_successful);		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', array('Testsadddddddddd'));
		$is_successful = FALSE;
		$barangay      = Input::get('edit-barangay',     false);
		$ceac_acticity = Input::get('edit-ceacactivity', false);
		$date_from     = Input::get('edit-datefrom',     false);
		$date_to       = Input::get('edit-dateto',    	 false);
		$provided_by   = Input::get('edit-providedby',   false);
		$provided_to   = Input::get('edit-providedto',   false);
		$category      = Input::get('edit-category',     false);
		$details       = Input::get('edit-details',      false);
		if(!($barangay === false || $provided_by    === false || $category === false || 
			 $details  === false))
		{
			$monolog->addInfo('Log Message', array('All inputs are complete'));
			$spi_techassist = SPITechAssist::find($id);
			$spi_techassist->activity_code  = $ceac_acticity;
			$spi_techassist->provider       = $provided_by;
			$spi_techassist->provider_to       = $provided_to;
			$spi_techassist->startdate      = date("Y-m-d", strtotime($date_from));
			$spi_techassist->enddate        = date("Y-m-d", strtotime($date_to));
			$spi_techassist->tech_category  = $category;
			$spi_techassist->tech_details   = $details;
			$spi_techassist->save();
			$is_successful = TRUE;
		}

		Session::put('spi-techassist.update', $is_successful);		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$affectedRows = SPITechAssist::find($id)->delete();
		$is_successful = ($affectedRows > 0) ? TRUE : FALSE;
		Session::put('spi-techassist.destroy', $is_successful);		
	}


}
