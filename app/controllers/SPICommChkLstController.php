<?php

class SPICommChkLstController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//$monolog - for debug only
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', array('Test'));
		$mode = Session::get('accelerated');
		$user          = Session::get('username');

		$psgc_id       = User::where('username',$user)->first()->psgc_id;

		$municipality   = Report::get_municipality($psgc_id);
		$province       = Report::get_province($psgc_id);
		$region 		= Report::get_region($psgc_id);

		$cycle         = Report::get_current_cycle($psgc_id);
		$program       = Report::get_current_program($psgc_id);
		$rfuser        = User::where('username',$user)->get();
		$rfuser        = (count($rfuser) > 0) ? $rfuser[0] : null;
	    $lgu_psgc      = $rfuser->psgc_id;
	    $committees    = SPIOCComm::where('lgu_psgc', '=', $lgu_psgc)
	    					->where('kc_mode',$mode)
	    					->where('cycle_id', $cycle)
	    					->where('program_id', $program)
	    					->get();
	    $lgu_covered   = '';
	    $assigned_view = '';

		$monolog->addInfo('Log Message', array('Committees', $committees));

	    if($rfuser->office_level == 'ACT')
	    {
	    	$lgu_covered   = 'M';	
	    	$assigned_view = 'spicommchklst.index';
	    }
		
		$store_status   = Session::pull('spi-comm.store', null);
		$update_status  = Session::pull('spi-comm.update', null);
		$destroy_status = Session::pull('spi-comm.destroy', null);
		return $this->view($assigned_view)
			->with('title', 'Oversight and Coordinating Committees Checklist')
			->with('username', $user)
			->with('program', $program)
			->with('cycle', $cycle)
			->with('lgu_psgc', $lgu_psgc)
			->with('lgu_covered', $lgu_covered)
			->with('committees', $committees)
			->with('store_status', $store_status)
			->with('update_status', $update_status)
			->with('destroy_status', $destroy_status)
			->with('province',          $province)
			->with('municipality_psgc', $psgc_id)
			->with('municipality',      $municipality)
			->with('region', $region);

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//$monolog - for debug only
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', array('Test'));

		$is_successful = FALSE;
		$id            = NULL;
		$program_id    = Input::get('in-programid',   false);
		$cycle_id      = Input::get('in-cycleid',     false);
	    $lgu_psgc      = Input::get('in-lgupsgc',     false);  // this is dependent on lgu_covered
		$lgu_covered   = Input::get('in-lgucovered',  false); // P - Provincial; M - Municipal 
		$comm_name     = Input::get('in-commname',    false);
		$no_male       = Input::get('in-malenum',     false);
		$no_female     = Input::get('in-femalenum',   false);
		$date_org      = Input::get('in-dateorg',     false);
		$freq_meetings = Input::get('in-regmeetings', false);
		$data['kc_mode']	    = Session::get('accelerated');

        $newsquence = $this->getNewCurSequence();
        $id = 'SPICOM'.Input::get('in-lgupsgc').$newsquence;

		if(!($program_id  === false || $cycle_id  === false || $lgu_psgc      === false ||
		     $lgu_covered === false || $comm_name === false || $no_male       === false ||
		     $no_female   === false || $date_org  === false || $freq_meetings === false))
		{
			$monolog->addInfo('Log Message', array('All inputs are complete'));
			$spi_occomm = new SPIOCComm;
            $spi_occomm->id   = $id;
			$spi_occomm->program_id    = $program_id;
			$spi_occomm->cycle_id      = $cycle_id;
			$spi_occomm->lgu_psgc      = $lgu_psgc;
			$spi_occomm->lgu_covered   = $lgu_covered;
			$spi_occomm->comm_name     = $comm_name;
			$spi_occomm->no_male       = $no_male;
			$spi_occomm->no_female     = $no_female;
			$spi_occomm->kc_mode = Session::get('accelerated');
			$spi_occomm->date_org      = date("Y-m-d", strtotime($date_org));
			$spi_occomm->freq_meetings = $freq_meetings;
			$spi_occomm->save();
			$id = $spi_occomm->id;
			$is_successful = ($id !== NULL) ? TRUE : FALSE;
		}

		$monolog->addInfo('Log Message', array('id' => $id));
		Session::put('spi-comm.store', $is_successful);		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', array('Test'));

		$is_successful = FALSE;
		$comm_name     = Input::get('edit-commname',    false);
		$no_male       = Input::get('edit-malenum',     false);
		$no_female     = Input::get('edit-femalenum',   false);
		$date_org      = Input::get('edit-dateorg',     false);
		$freq_meetings = Input::get('edit-regmeetings', false);
		if(!($comm_name === false || $no_male       === false || $no_female   === false || 
			 $date_org  === false || $freq_meetings === false))
		{
			$monolog->addInfo('Log Message', array('All inputs are complete'));
			$spi_occomm = SPIOCComm::find($id);
			$spi_occomm->comm_name     = $comm_name;
			$spi_occomm->no_male       = $no_male;
			$spi_occomm->no_female     = $no_female;
			$spi_occomm->date_org      = date("Y-m-d", strtotime($date_org));
			$spi_occomm->freq_meetings = $freq_meetings;
			$spi_occomm->save();
			$is_successful = TRUE;
		}

		Session::put('spi-comm.update', $is_successful);		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$affectedRows = SPIOCComm::find($id)->delete();
		$is_successful = ($affectedRows > 0) ? TRUE : FALSE;
		Session::put('spi-comm.destroy', $is_successful);		
	}


}
