<?php

class ProjectCoverageAndStaffingController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['staffs'] = Staff::get();
		$data['title']	  = 'Project Coverage and Staffing';
		
		return $this->view('admindb.projectcoverage.index')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['title']	  = 'Project Coverage and Staffing';
		$data['curr_position'] = $this->reference->getCurrentPosition();
		$data['program']  = $this->reference->getPrograms();
		$data['offices'] = $this->reference->getOffices();
		$data['statusofemployment'] = $this->reference->getStatusEmployment();


        
		return $this->view('admindb.projectcoverage.create')->with($data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$data = Input::all();

		$psgc = $this->user->getPsgcId(Session::get('username'));
		// Staff::where('employee_id','23123')->get()
		$rules = array(
			// 'employee_id' => 'required'
		);
		$validator = Validator::make($data, $rules);
		
		if ($validator->fails()) {	
			return Redirect::to('project-coverage/create')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			$employee_id = $this->user->generateEmployeeId($psgc);
			$data['employee_id'] = $employee_id;

			// add first to emphist

			$input = $data;
			$input['psgc_id'] = $psgc;
			$input['birthday'] = $this->setDate($input['birthday']);
			// unset($input['birthday']);

			$input['contract_start'] = $this->setDate($input['contract_start']);
			$input['contract_end'] = $this->setDate($input['contract_end']);
			Staff::create($input);

			if(array_key_exists('training_name', $data))
			{
				EmpTraining::where('employee_id',$employee_id)->delete();
				foreach ($data['training_name'] as $key => $value) {
					$training = [
						'employee_id' =>$employee_id,
						'training_name' => $value,
						'startdate' => isset($data['training_start'][$key]) ? $this->setDate($data['training_start'][$key]) :' ',
						'enddate'   => isset($data['training_end'][$key]) ? $this->setDate($data['training_end'][$key]) : '',
						'conducted_by' =>$data['conducted_by'][$key],
						'is_ncddp_related' => isset($data['is_ncddp_related'][$key]) ? 1 : 0
					];

					EmpTraining::create($training);
				}
			}
			if(array_key_exists('position_held', $data)){
				EmpHistory::where('employee_id',$employee_id)->delete();
				foreach ($data['position_held'] as $key =>  $value) {
					$history = [
						'employee_id' =>$employee_id,
						'position' => $value,
						'startdate' => $this->setDate($data['history_start'][$key]),
						'enddate'   => $this->setDate($data['history_end'][$key]),
					];
					
					EmpHistory::create($history);
				}
			}
			// create
		}
		Session::flash('message','Successfully added '.$employee_id );
		return Redirect::to('project-coverage');
	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['staff'] = Staff::where('employee_id',$id)->first();
		$data['title']	  = 'Project Coverage and Staffing';

		$data['emp_history'] = EmpHistory::where('employee_id',$id)->get();
		$data['emp_training'] = EmpTraining::where('employee_id',$id)->get();
		return $this->view('admindb.projectcoverage.show')->with($data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{


		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['title']	  = 'Project Coverage and Staffing';
		$data['curr_position'] = $this->reference->getCurrentPosition();
		$data['program']  = $this->reference->getPrograms();
		$data['offices'] = $this->reference->getOffices();
		$data['emp_history'] = EmpHistory::where('employee_id',$id)->get();
		$data['emp_training'] = EmpTraining::where('employee_id',$id)->get();
		
		$data['staff'] = Staff::where('employee_id',$id)->first();
		$data['statusofemployment'] = $this->reference->getStatusEmployment();

		
		return $this->view('admindb.projectcoverage.edit')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();

		$psgc = $this->user->getPsgcId(Session::get('username'));
		$rules = array(
			// 'employee_id' => 'required'
		);
		$validator = Validator::make($data, $rules);
		
		if ($validator->fails()) {	
			return Redirect::to('project-coverage/'.$id.'/edit')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			unset($data['_method']);
			unset($data['_token']);

			$input = $data;

			unset($input['training_name']);
			unset($input['conducted_by']);
			unset($input['training_start']);
			unset($input['training_end']);
			unset($input['is_ncddp_related']);
			unset($input['position']);
			unset($input['history_end']);
			unset($input['history_start']);
			unset($input['position_held']);

			$input['birthday'] = $this->setDate($input['birthday']);
			unset($input['birthday']);

			$input['contract_start'] = $this->setDate($input['contract_start']);
			$input['contract_end'] = $this->setDate($input['contract_end']);

			// echo "<pre>";
			// dd($input);
			Staff::where('employee_id',$id)->update($input);
			if(array_key_exists('training_name', $data))
			{
				EmpTraining::where('employee_id',$id)->delete();
				foreach ($data['training_name'] as $key => $value) {
					$training = [
						'employee_id' =>$id,
						'training_name' => $value,
						'startdate' => isset($data['training_start'][$key]) ? $this->setDate($data['training_start'][$key]) :' ',
						'enddate'   => isset($data['training_end'][$key]) ? $this->setDate($data['training_end'][$key]) : '',
						'conducted_by' =>$data['conducted_by'][$key],
						'is_ncddp_related' => isset($data['is_ncddp_related'][$key]) ? 1 : 0
					];

					EmpTraining::create($training);
				}
			}
			if(array_key_exists('position_held', $data)){
				EmpHistory::where('employee_id',$id)->delete();
				foreach ($data['position_held'] as $key =>  $value) {
					$history = [
						'employee_id' =>$id,
						'position' => $value,
						'startdate' => $this->setDate($data['history_start'][$key]),
						'enddate'   => $this->setDate($data['history_end'][$key]),
					];
					
					EmpHistory::create($history);
				}
			}
			// create
		}
		Session::flash('message','Successfully updated '.$id );
		return Redirect::to('project-coverage');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		Staff::where('employee_id',$id)->delete();
		EmpHistory::where('employee_id',$id)->delete();
		EmpTraining::where('employee_id',$id)->delete();
		return Redirect::to('project-coverage')->with('message','Successfully Deleted '.$id);
	}


}
