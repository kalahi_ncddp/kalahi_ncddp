<?php

class ApprovalController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function untag()
    {
        $activity_id = Input::get('activity_id','');

          Approval::where('activity_id',$activity_id)->delete();
          TransLog::log('review','Unreviewed',Auth::user()->username,Auth::user()->psgc_id,$activity_id);
          return Redirect::back()->with('message','ID '.$activity_id .' has been untagged');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $input = Input::all();
        $user = Session::get('username');
        $psgc_id = $this->user->getPsgcId($user);
        $office = $this->user->getOfficelevelByPsgc($psgc_id);
        $mode = Session::get('accelerated');
        $module_type = $input['module_type'];

//        dd($data);
//        try{

        foreach($input['check'] as $ids)
        {

            $exist = Approval::where('activity_id',$ids)->exists();

            if($office == 'ACT') {
                $data['act_reviewed'] = date('Y-m-d');
                $data['approved_level'] = 'ACT';
            }
            else if($office == 'SRPMO') {
                $data['srpmo_reviewed'] = date('Y-m-d');
                $data['approved_level'] = 'SRPMO';
            }
            else if($office == 'RPMO') {
                $data['rpmo_approved'] = date('Y-m-d');
                $data['approved_level'] = 'RPMO';


            }
//                $data['uploaded_by'] = $user;
//                dd($data);
            $data['activity_id'] = $ids;
            $data['kc_mode']  = $mode;
            $data['psgc_id']  = $psgc_id;
            $data['module_type'] = $module_type;
//                dd($data);
            if($exist){
                Approval::where('activity_id',$ids)->update($data);
            }
            else
            {
                Approval::create($data);
                TransLog::log('review','reviewed',Auth::user()->username,Auth::user()->psgc_id,$ids);
            }
        }
        return json_encode(['status'=>'ok']);
//        }catch (Exception $e){
//            dd($e);
//            return json_encode(['status'=>'error']);;
//        }
    }


    public function ceac()
    {
        $input = Input::all();
        $user = Session::get('username');
        $psgc_id = $this->user->getPsgcId($user);
        $office = $this->user->getOfficelevelByPsgc($psgc_id);
        $mode = Session::get('accelerated');
        $module_type = $input['module_type'];


//        try{


            $exist = Approval::where('cycle_id',$input['cycle_id'])
                ->where('program_id',$input['program_id'])
                ->where('module_type',$module_type)
                ->where('psgc_id',$psgc_id)->exists();
            if($office == 'ACT') {
                $data['act_reviewed'] = date('Y-m-d');
                $data['approved_level'] = 'ACT';
                $data['cycle_id'] = $input['cycle_id'];
                $data['program_id'] = $input['program_id'];
                $data['psgc_id'] = $psgc_id;
            }
            else if($office == 'SRPMO') {
                $data['srpmo_reviewed'] = date('Y-m-d');
                $data['approved_level'] = 'SRPMO';
                $data['cycle_id'] = $input['cycle_id'];
                $data['program_id'] = $input['program_id'];
                $data['psgc_id'] = $psgc_id;
            }
            else if($office == 'RPMO') {
                $data['rpmo_approved'] = date('Y-m-d');
                $data['approved_level'] = 'RPMO';
                $data['cycle_id'] = $input['cycle_id'];
                $data['program_id'] = $input['program_id'];
                $data['psgc_id'] = $psgc_id;

            }
//                $data['uploaded_by'] = $user;
//                dd($data);
            $data['activity_id'] = $input['reference_no'];
            $data['kc_mode']  = $mode;
            $data['module_type'] = $module_type;
            if($exist){
                Approval::where('cycle_id',$input['cycle_id'])
                            ->where('program_id',$input['program_id'])
                            ->where('module_type',$module_type)
                            ->where('psgc_id',$psgc_id)->update($data);
            }
            else
            {
                Approval::create($data);
            }

        return json_encode(['status'=>'ok']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
