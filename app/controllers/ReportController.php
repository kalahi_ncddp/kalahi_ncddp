<?php

class ReportController extends \BaseController {

	/**
	 * Display a listing of BA reports
	 *
	 * @return Response
	 */
	public function ba()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();
		// get
		$list_ref_array = $this->reference->getProvMuniBarangayByOfficeLevel($user);

		$data = array_merge($list_ref_array,$data);
		$data['username'] = $user;
		$data['title'] = 'Barangay Assembly Reports';
		return $this->view('reports.data_reports.ba')->with($data);
	}
	/**
	* CHECK THIS PDF GENERATOR
	* Displays the BATotalAttendies pdf forms
	*
	*/
	public function BApdf()
	{
		$user = Session::get('username');

		// GET THE PSGC ID OF THE USER
		$psgc_id = $this->user->getPsgcId($user);
		$cycle	 = Input::get('cycle_id');
		// GET ALL THE LIST OF THE PROGRAM/FUNDSOURCE
		$program = $this->reference->getPrograms();

		// GET THE CURRENT KC_MODE OF THE SYSTEM IF IT IS REGULAR OR ACCELERATED
		$mode	 = Session::get('accelerated');

		// GET THE OFFICE LEVEL OF THE USER BASED IN
		$office_level = $this->user->getOfficeLevelByUsername($user);


		$data['fundsources'] = [ Input::get('program_id') ];

		$data['cycle'] = $cycle;


		$data['region'] = $this->user->getRegionOfficeName($user);
 



		$data['office_level'] = $office_level;
		$data['reports_instance'] = $this->reports;
		$data['mode'] = $mode;
		$pdf = PDF::loadView('PDF.household_participation_rate_template',$data);
		return $pdf->stream();
	}

	public function batotalattendees()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();

		// get
		$list_ref_array = $this->reference->getProvMuniBarangayByOfficeLevel($user);

		$data = array_merge($list_ref_array,$data);
		// echo "<pre>";
		// dd($data);

		$data['username'] = $user;
		$data['title'] = 'Barangay Assembly Total Attendees Report';
		return $this->view('reports.data_reports.ba_total_attendies')->with($data);
	}
	/**
	* CHECK THIS PDF GENERATOR
	* Displays the BATotalAttendies pdf forms
	*
	*/
	public function BATotalAttendeespdf()
	{
		$user = Session::get('username');

		// GET THE PSGC ID OF THE USER
		$psgc_id = $this->user->getPsgcId($user);
		$cycle	 = Input::get('cycle_id');
		// GET ALL THE LIST OF THE PROGRAM/FUNDSOURCE
		$program = $this->reference->getPrograms();

		// GET THE CURRENT KC_MODE OF THE SYSTEM IF IT IS REGULAR OR ACCELERATED
		$mode	 = Session::get('accelerated');

		// GET THE OFFICE LEVEL OF THE USER BASED IN
		$office_level = $this->user->getOfficeLevelByUsername($user);


		// $data['fundsources'] = $program;
		$data['fundsources'] = [ Input::get('program_id') ];
		$data['cycle'] = $cycle;

		
		$data['region'] = $this->user->getRegionOfficeName($user);
 



		$data['office_level'] = $office_level;
		$data['reports_instance'] = $this->reports;
		$data['mode'] = $mode;
		// dd($program);
		$pdf = PDF::loadView('PDF.ba_report_total_attendees',$data)->setOrientation('landscape');
		return $pdf->stream();
	}


public function volunteercomitee()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();

		// get
		$list_ref_array = $this->reference->getProvMuniBarangayByOfficeLevel($user);

		$data = array_merge($list_ref_array,$data);
		// echo "<pre>";
		// dd($data);

		$data['username'] = $user;
		$data['title'] = 'Community Volunteers: Number of Volunteers';
		return $this->view('reports.data_reports.volunteer_communitee')->with($data);
	}
	/**
	* CHECK THIS PDF GENERATOR
	* Displays the BATotalAttendies pdf forms
	*
	*/
	public function volunteercomiteepdf()
	{
		$user = Session::get('username');

		// GET THE PSGC ID OF THE USER
		$psgc_id = $this->user->getPsgcId($user);
		$cycle	 = Input::get('cycle_id');
		// GET ALL THE LIST OF THE PROGRAM/FUNDSOURCE
		$program = $this->reference->getPrograms();

		// GET THE CURRENT KC_MODE OF THE SYSTEM IF IT IS REGULAR OR ACCELERATED
		$mode	 = Session::get('accelerated');

		// GET THE OFFICE LEVEL OF THE USER BASED IN
		$office_level = $this->user->getOfficeLevelByUsername($user);


		// $data['fundsources'] = $program;
		$data['fundsources'] = [ Input::get('program_id') ];
		$data['cycle'] = $cycle;


		$data['region'] = $this->user->getRegionOfficeName($user);
 


		$data['office_level'] = $office_level;
		$data['reports_instance'] = $this->reports;
		$data['mode'] = $mode;
		// dd($program);
		$pdf = PDF::loadView('PDF.volunteer_communitee',$data);
		return $pdf->stream();
	}


	//--

	public function trainingparticipants()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();

		// get
		$list_ref_array = $this->reference->getProvMuniBarangayByOfficeLevel($user);

		$data = array_merge($list_ref_array,$data);
		// echo "<pre>";
		// dd($data);

		$data['username'] = $user;
		$data['title'] = 'Community Volunteers: Total No. of Committee Heads and Members';
		return $this->view('reports.data_reports.training_participants')->with($data);
	}
	/**
	* CHECK THIS PDF GENERATOR
	* Displays the BATotalAttendies pdf forms
	*
	*/
	public function trainingparticipantspdf()
	{
		$user = Session::get('username');

		// GET THE PSGC ID OF THE USER
		$psgc_id = $this->user->getPsgcId($user);
		$cycle	 = Input::get('cycle_id');
		// GET ALL THE LIST OF THE PROGRAM/FUNDSOURCE
		$program = $this->reference->getPrograms();

		// GET THE CURRENT KC_MODE OF THE SYSTEM IF IT IS REGULAR OR ACCELERATED
		$mode	 = Session::get('accelerated');

		// GET THE OFFICE LEVEL OF THE USER BASED IN
		$office_level = $this->user->getOfficeLevelByUsername($user);


		$data['fundsources'] = $program;

		$data['cycle'] = $cycle;

		$data['region'] = $this->user->getRegionOfficeName($user);
 



		$data['office_level'] = $office_level;
		$data['reports_instance'] = $this->reports;
		$data['mode'] = $mode;
		// dd($program);
		$pdf = PDF::loadView('PDF.training_participants',$data);
		return $pdf->stream();
	}



	//-


	public function ceac()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();
		// $region = Report::get_region_by_brgy_id($psa->psgc_id);
		// get
		$list_ref_array = $this->reference->getProvMuniBarangayByOfficeLevel($user);
		
		$data = array_merge($list_ref_array,$data);
		// echo "<pre>";
		// dd($data);

		$data['username'] = $user;
		$data['title'] = ' Community Empowerment Activity Cycle: Regular CEAC Details';
		return $this->view('reports.data_reports.ceac')->with($data);
	}
	/**
	* CHECK THIS PDF GENERATOR
	* Displays the BATotalAttendies pdf forms
	*
	*/
	public function ceacpdf()
	{
		$user = Session::get('username');

		// GET THE PSGC ID OF THE USER 
		$psgc_id = $this->user->getPsgcId($user);
		$cycle	 = Input::get('cycle_id');
		// GET ALL THE LIST OF THE PROGRAM/FUNDSOURCE
		$program = $this->reference->getPrograms();

		// GET THE CURRENT KC_MODE OF THE SYSTEM IF IT IS REGULAR OR ACCELERATED 	
		$mode	 = Session::get('accelerated');

		// GET THE OFFICE LEVEL OF THE USER BASED IN 		
		$office_level = $this->user->getOfficeLevelByUsername($user);

		
		$data['fundsources'] = $program;
		
		$data['cycle'] = $cycle;

		
	

		$data['office_level'] = $office_level;	
		$data['reports_instance'] = $this->reports;
		$data['mode'] = $mode;
		// dd($program);
		$pdf = PDF::loadView('PDF.ceac',$data)->setOrientation('landscape');
		return $pdf->stream();
	}
	





	/**
	 * Display a listing of PSA reports
	 *
	 * @return Response
	 */
	public function psa()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();

		$data['username'] = $user;

		$data['title'] = 'Participatory Situation Analysis Reports';
		return $this->view('reports.data_reports.psa')->with($data);
	}
	/**
	*
	* Displays the PSApdf forms
	*
	*/
	public function PSApdf()
	{
		$psgc_id = Input::get('psgc_id');
		$cycle	 = Input::get('cycle_id');
		$program = Input::get('program_id');
		$mode	 = Session::get('accelerated');

		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);
		$office_level = $this->user->getOfficelevelByPsgc($user);

		$data['fundsource'] = $program;

		$data['cycle'] = $cycle;
		$psa = array();
		if($cycle != NULL && $program != NULL){
			$psa = $this->reports->getPSADataProfile($office_level,$user, $program	,$cycle);
		}
		$data['bareports']  = $psa;
		// dd($psa);

		$pdf = PDF::loadView('PDF.psa_data_profile_template',$data);
		return $pdf->stream();
	}

	/**
	 * Display a listing of PSA reports
	 *
	 * @return Response
	 */
	public function grievances()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();

		$data['username'] = $user;

		$data['title'] = 'Grievance Redress System (GRS):';
		return $this->view('reports.data_reports.grievances')->with($data);
	}
	/**
	*
	* Displays the PSApdf forms
	*
	*/
	public function grievancespdf()
	{
		$psgc_id = Input::get('psgc_id');
		$cycle	 = Input::get('cycle_id');
		$program = Input::get('program_id');
		$resolution_status = Input::get('resolution_status');
		$grievance_intake = Input::get('grievance_intake');
		$type = Input::get('type');
		$mode	 = Session::get('accelerated');
		$arr_type = explode(',', $type);

		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);
		$office_level = $this->user->getOfficelevelByPsgc($user);

		$data['fundsource'] = $program;


		$data['cycle'] = $cycle;
		if($resolution_status == 'p' or $resolution_status == 'Pending')
			$data['resolution_status'] = "Pending";
		else if($resolution_status == 'r' or $resolution_status == 'Resolved')
			$data['resolution_status'] = "Resolved";
		else if($resolution_status == 'o' or $resolution_status == 'On Going')
			$data['resolution_status'] = "On Going";
		else
			$data['resolution_status'] = "";

		if($grievance_intake === "intake")
			$data['grievance_intake'] = "intake";
		else if($grievance_intake === "pincos")
			$data['grievance_intake'] = "pincos";
		else
			$data['grievance_intake'] = "";

		$data['type'] = $type;
		$data['municipality'] = \Municipality::find($user)->municipality;

		$grievances = array();
		if($cycle != NULL && $program != NULL){
			$grievances = $this->reports->getGrievances($office_level,$user, $program, $cycle, $resolution_status, $grievance_intake, $arr_type);
		}
		$data['bareports']  = $grievances;

		$pdf = PDF::loadView('PDF.grievances_template',$data)->setOrientation('landscape');
		return $pdf->stream();
	}





	public function get_overall_total($data1, $data2)
	{
		$total = [0, 0, 0, 0];
		$counter = 0;

		echo '<pre>';
		foreach( $data1[ sizeof($data1) - 1] as $concern )
		{
			$total[$counter] += $concern;
			$counter++;
		}

		$counter = 0;
		foreach( $data2[ sizeof($data2) - 1] as $concern )
		{
			$total[$counter] += $concern;
			$counter++;
		}

		return $total;
	}


	public function grs_concerns()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();

		$data['username'] = $user;
		$data['title'] = 'GRS Concerns';

		return $this->view('reports.data_reports.grs_concerns')->with($data);
	}


	public function grs_concerns_pdf()
	{
		// From Boss Onar and Allen
		$user = Session::get('username');

		// Filters
		$office_level = $this->user->getOfficeLevelByUsername($user);
		$psgc_id = $this->user->getPsgcId($user);
		$program = Input::get('program_id');
		$cycle	 = Input::get('cycle_id');
		$mode	 = Session::get('accelerated');

		// Data
		$data['grs_category'] = $this->reference->getConcernCategory();
		$data['concerns_intake'] = $this->reference->getGRSConcernsIntake($office_level, $psgc_id, $program, $cycle);
		$data['concerns_pincos'] = $this->reference->getGRSConcernsPINCOS($office_level, $psgc_id, $program, $cycle);
		$data['overall_total'] = $this->get_overall_total( $data['concerns_intake'], $data['concerns_pincos']);

		$pdf = PDF::loadView('PDF.grs_concerns', $data);
		return $pdf->stream();
	}


	public function grs_gender()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();

		$data['username'] = $user;
		$data['title'] = 'GRS Gender';

		return $this->view('reports.data_reports.grs_gender')->with($data);
	}


	public function grs_gender_pdf()
	{
		// From Boss Onar and Allen
		$user = Session::get('username');

		// Filters
		$office_level = $this->user->getOfficeLevelByUsername($user);
		$psgc_id = $this->user->getPsgcId($user);
		$program = Input::get('program_id');
		$cycle	 = Input::get('cycle_id');
		$mode	 = Session::get('accelerated');

		// Data
		$data['grs_nature'] = $this->reference->getConcernNature();
		$data['gender_count'] = $this->reference->getGRSGender($office_level, $psgc_id, $program, $cycle);

		$pdf = PDF::loadView('PDF.grs_gender', $data);
		return $pdf->stream();
	}




	public function grs_mode_of_filing()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();

		$data['username'] = $user;
		$data['title'] = 'GRS Mode of Filing';

		return $this->view('reports.data_reports.grs_mode_of_filing')->with($data);
	}


	public function grs_mode_of_filing_pdf()
	{
		// From Boss Onar and Allen
		$user = Session::get('username');

		// Filters
		$office_level = $this->user->getOfficeLevelByUsername($user);
		$psgc_id = $this->user->getPsgcId($user);
		$program = Input::get('program_id');
		$cycle	 = Input::get('cycle_id');
		$mode	 = Session::get('accelerated');

		// Data
		$data['mode_of_filing'] = $this->reference->getModeOfFiling();
		$data['mode_of_filing_intake'] = $this->reference->getGRSModeOfFilingIntake($office_level, $psgc_id, $program, $cycle);
		$data['mode_of_filing_pincos'] = $this->reference->getGRSModeOfFilingPINCOS($office_level, $psgc_id, $program, $cycle);
		$data['overall_total'] = $this->get_overall_total( $data['mode_of_filing_intake'], $data['mode_of_filing_pincos']);

		$pdf = PDF::loadView('PDF.grs_modeoffiling', $data);
		return $pdf->stream();
	}



	public function grs_status()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();

		$data['username'] = $user;
		$data['title'] = 'GRS Status';

		return $this->view('reports.data_reports.grs_status')->with($data);
	}


	public function grs_status_pdf()
	{
		$user = Session::get('username');

		// Filters
		$office_level = $this->user->getOfficeLevelByUsername($user);
		$psgc_id = $this->user->getPsgcId($user);
		$program = Input::get('program_id');
		$cycle	 = Input::get('cycle_id');
		$mode	 = Session::get('accelerated');
		
		// Data
		$data['fund_source'] = $program;
		$data['grs_nature'] = $this->reference->getConcernNature();
		$data['municipalities'] = $this->reference->getMunicipalitiesByPSGC($psgc_id);
		$data['grs_status_intake'] = $this->reference->getGRSStatusIntake($office_level, $psgc_id, $program, $cycle, 'Intake');
		$data['grs_status_pincos'] = $this->reference->getGRSStatusIntake($office_level, $psgc_id, $program, $cycle, 'PINCOS');

		$pdf = PDF::loadView('PDF.grs_status', $data)->setOrientation('landscape');
		return $pdf->stream();
	}





	public function mibf()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();

		// get
		$list_ref_array = $this->reference->getProvMuniBarangayByOfficeLevel($user);

		$data = array_merge($list_ref_array,$data);
		// echo "<pre>";
		// dd($data);

		$data['username'] = $user;
		$data['title'] = 'Municipal Inter Baranggay Forum Reports';
		return $this->view('reports.data_reports.mibf')->with($data);
	}

	public function mibf_pdf()
	{
		$data 	= [];
		$cycle 	= Input::get('cycle');
		$user 	= Session::get('username');
		$municipal_psgc_id 		= Report::get_muni_psgc($user);
		$provincial_psgc_id 	= Report::get_prov_psgc($municipal_psgc_id);

		$data['cycle']				= $cycle;
		$data['office'] 			= Report::get_office($user);
		$data['municipality'] = Report::get_municipality($municipal_psgc_id);
		$data['province'] 		= Report::get_staff_province($municipal_psgc_id);
		$data['region'] 			= Report::get_region($municipal_psgc_id);
		$data['mibf_list'] 		= Report::get_all_mibf_priority_proposals_act($user, 1, $cycle);

		$pdf = PDF::loadView('PDF.mibf-act', $data);
		return $pdf->stream(date('YmdHis'). ' - MIBF Priority Proposal');
	}


	public function sp_by_mibf()
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		$data['program'] = $this->reference->getPrograms();
		$data['cycle']   = $this->reference->getCycles();

		$data['username'] = $user;
		$data['title'] = 'Prioritized SPs by MIBF';

		return $this->view('reports.data_reports.sp_by_mibf')->with($data);
	}


	public function sp_by_mibf_pdf()
	{
		$user = Session::get('username');

		// Filters
		$office_level = $this->user->getOfficeLevelByUsername($user);
		$psgc_id = $this->user->getPsgcId($user);
		$program = $this->reference->getPrograms();
		$cycle	 = Input::get('cycle_id');
		$mode	 = Session::get('accelerated');

		// Data
		$data['regions'] = $this->reference->getAllRegions();
		$data['cycle'] = $cycle;

		// ($office_level, $psgc_id, $program, $cycle);
		$data['sp_by_mibf'] = $this->reference->getSPByMIBF($cycle);

		$pdf = PDF::loadView('PDF.sp_by_mibf', $data);
		return $pdf->stream();
	}



















	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
