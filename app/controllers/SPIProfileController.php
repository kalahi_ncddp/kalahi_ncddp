<?php

class SPIProfileController extends \BaseController {

	/**
	 * Lists all the SPI Profiles.
	 */
	public function index() {
		$user = Session::get('username');
		$psgc_id = $this->user->getPsgcId($user);
		$search = Input::get('search');
		$projects = SPIProfile::get_municipality_projects($psgc_id,$search);

		$cycle_id = Report::get_current_cycle($psgc_id);
		$mode = Session::get('accelerated');
		$project_proposals = ProjProposal::get_current_proposals($psgc_id,$mode);
			return $this->view('spi_profile.index')
			->with('username', $user)
			->with('title', 'Sub Project Profile')
			->with('projects', $projects)
			->with('project_proposals', $project_proposals);
	}

	/**
	 * Shows the form for creating a new Sub Project from an MIBF Project Proposal.
	 */
	public function create() {
		$user = Session::get('username');
		$psgc_id = Report::get_muni_psgc($user);
		
		$rules = array(
			'project_id'   => 'required',
		);
		$validator = Validator::make(Input::all(), $rules);
		// added validator
		if ($validator->fails()) {	
			return Redirect::back()
				->withErrors($validator)
				->withInput(Input::all());
		} else {


			$proposal = ProjProposal::where(array('project_id' => Input::get('project_id')))->first();

            /*return cycle and program of the proposal*/
            $proposal_cycle_id = MunicipalForum::where('activity_id',$proposal->activity_id)->first()->cycle_id;
            $proposal_program_id = MunicipalForum::where('activity_id',$proposal->activity_id)->first()->program_id;


            /*declare a data variable as array storage*/
            $data = [
                'cycle_id'=>$proposal_cycle_id,
                'program_id'=>$proposal_program_id
            ];

			$subcategories = SPICategory::lists('sub_category', 'sub_category');

			$profile = new SPIProfile(array(
				'project_name'	=> $proposal->project_name,
				'municipal_id'	=> $psgc_id,
				'barangay_id'		=> $proposal->lead_brgy,
				'mibf_refno'		=> $proposal->project_id,
				'mibf_rank'			=> $proposal->rank
			));

            $cycle = array('NULL' => 'Select Cycle') + $this->reference->getCycles();


            $program = array('NULL' => 'Select Program') + $this->reference->getPrograms();
            $program = $program;
            /*check if proj exists*/
            if(!isset($proposal_program_id)){
                unset($program['KC-NCDDP']);
            }
//            dd($data);
			return $this->view('spi_profile.create')
				->with('username', $user)
				->with('title', 'Sub Project Profile')
				->with('profile', $profile)
				->with('subcategories', $subcategories)->with('programs',$program)
                ->with('cycles',$cycle)
                ->with($data);
		}
	}

	/**
	 * Shows the form for creating a new Sub Project from scratch
	 */
	public function build() {
		$user = Session::get('username');
		$psgc_id = Report::get_muni_psgc($user);
		
		$subcategories = SPICategory::lists('sub_category', 'sub_category');

		// get barangay list
         $barangay_id = Report::get_available_barangays($user);
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);

		$profile = new SPIProfile(array(
			'municipal_id'	=> $psgc_id
		));

        $cycle = array('NULL' => 'Select Cycle') + $this->reference->getCycles();
        $program = array('NULL' => 'Select Program') + $this->reference->getPrograms();
        $program = $program;
        /*check if proj exists*/

         unset($program['KC-NCDDP']);

		return $this->view('spi_profile.create')
			->with('username', $user)
			->with('title', 'Sub Project Profile')
			->with('profile', $profile)
			->with('subcategories', $subcategories)
			->with('barangay_list', $barangay_list)
            ->with('programs',$program)
            ->with('cycles',$cycle);
	}

	/**
	 * Stores the profile to the database.
	 */
	public function store() {
		$user = Session::get('username');
		$psgc_id = Report::get_muni_psgc($user);
		$data = Input::all();
		unset($data["_token"]);
		$data['project_id'] = $this->generate_project_id($data['municipal_id']);
		$data['program_id'] = isset($data['program_id']) ? $data['program_id'] : Report::get_current_program($psgc_id);
		$data['cycle_id'] = isset($data['cycle_id']) ? $data['cycle_id'] : Report::get_current_cycle($psgc_id);
		$data['date_started'] = $this->setDate($data["date_started"]);
		$data['planned_date_completed'] = $this->setDate($data['planned_date_completed']);
	    $data['kc_mode']	    = Session::get('accelerated');
		$profile = SPIProfile::create($data);
		if (isset($data['mibf_refno']) && $data['mibf_refno'] !== "") {
			// update project proposal
			$proposal = ProjProposal::where(array('project_id' => $data['mibf_refno']))->first();
			$proposal->added_to_spi = 'T';
			$proposal->save();
		}

		Session::flash('success', "Sub Project Profile successfully created");

		return Redirect::route('spi_profile.show', $data['project_id']);
	}

	/**
	 * Shows the details of the Sub Project.
	 */
	public function show($id) {
		$user = Session::get('username');
		$profile = SPIProfile::where(array('project_id' => $id))->first();
		$set_list = SPISET::where(array('project_id' => $id))->get();

		return $this->view('spi_profile.show')
			->with('username', $user)
			->with('title', 'Sub Project Profile')
			->with('description', $profile->project_id)
			->with('profile', $profile)
			->with('set_list', $set_list);
	}

	/**
	 * Shows the form for updating a new Sub Project.
	 */
	public function edit($id) {
		$user = Session::get('username');
		$profile = SPIProfile::where(array('project_id' => $id))->first();
		$subcategories = SPICategory::lists('sub_category', 'sub_category');
		$psgc_id = Report::get_muni_psgc($user);
        $cycle = array('NULL' => 'Select Cycle') + $this->reference->getCycles();
        $program = array('NULL' => 'Select Program') + $this->reference->getPrograms();
		// get barangay list
		$barangay_id = Report::get_available_barangays($user);
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);
		return $this->view('spi_profile.edit')
			->with('username', $user)
			->with('title', 'Sub Project Profile')
			->with('description', $profile->project_id)
			->with('profile', $profile)
			->with('subcategories', $subcategories)
			->with('barangay_list', $barangay_list)
            ->with('programs',$program)
            ->with('cycles',$cycle);

	}

	public function update($id) {
		$user = Session::get('username');
		$profile = SPIProfile::where(array('project_id' => $id))->first();

		$data = Input::all();
		$data['is_draft'] = isset($data['is_draft']) ? 1 : 0;
		
		unset($data["_token"]);
		$data['date_started'] = $this->setDate($data["date_started"]);
		$data['planned_date_completed'] = $this->setDate($data['planned_date_completed']);

		$profile->fill($data);
		$profile->save();

		Session::flash('success', "Sub Project Profile successfully updated.");

		return Redirect::route('spi_profile.show', $profile->project_id);
	}

	public function destroy($id) {
		$user = Session::get('username');
        try{
            $profile = SPIProfile::where('project_id' , $id)->first();
            SPIAccomplishmentItem::where(['acc_report_id'=>$id])->delete();
            SPICompletionReport::where(array('project_id' => $id))->delete();
            SPIBarangay::where(array('project_id' => $id))->delete();
            SPISET::where(array('project_id' => $id))->delete();
            if (isset($profile->mibf_refno) && $profile->mibf_refno !== "") {
//            dd($profile->mibf_refno);
                // update project proposal
                $proposal = ProjProposal::where(array('project_id' => $profile->mibf_refno))->first();
                $proposal->added_to_spi = 'F';
                $proposal->save();
            }

            $status = $profile->delete();
        }catch(\Exception $e){
            Session::flash('error','There is an error when deleting SPI profile due to data dependency in other module');
        }

//        dd($status);
		return Redirect::route('spi_profile.index');
	}

	public function certificate($id) {
		$user = Session::get('username');
		$profile = SPIProfile::where(array('project_id' => $id))->first();

		return $this->view('spi_profile.certificate')
			->with('username', $user)
			->with('title', 'Sub Project Certificate of Completion and Acceptance')
			->with('description', $profile->project_id)
			->with('profile', $profile);
	}

	public function edit_certificate($id) {
		$user = Session::get('username');
		$profile = SPIProfile::where(array('project_id' => $id))->first();

		return $this->view('spi_profile.edit_certificate')
			->with('username', $user)
			->with('title', 'Sub Project Certificate of Completion and Acceptance')
			->with('description', $profile->project_id)
			->with('profile', $profile);
	}

	public function update_certificate($id) {
		$user = Session::get('username');
		$profile = SPIProfile::where(array('project_id' => $id))->first();

		$data = Input::all();

		unset($data["_token"]);

		$profile->fill($data);
		$profile->save();

		Session::flash('success', "Sub Project Certificate successfully updated.");

		return Redirect::route('spi_profile.certificate', $profile->project_id);
	}

	/**
	 * Helper function which returns the unit of the given category.
	 */
	public function get_category_unit() {
		//check if its our form
    if (Session::token() !== Input::get( '_token' )) {
	    return Response::json( ['msg' => 'Unauthorized attempt to create setting'] );
    }

    $sub_category = Input::get('sub_category');
    $unit = SPICategory::find($sub_category)->unit;

    return Response::json( ['unit' => $unit, 'msg' => 'success'] );
	}

	/**
	 * Generates a project id.
	 */
	private function generate_project_id($psgc_id) {

		 $newsquence = $this->getNewCurSequence();
		

		return 'SPI'.$psgc_id.$newsquence;
	}
}