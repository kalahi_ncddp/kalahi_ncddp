<?php

class SPIHouseholdBeneficiariesController extends \BaseController {
	public function show($id) {
		$user = Session::get('username');
		$profile = SPIProfile::where(array('project_id' => $id))->first();

		return $this->view('spi_profile.household')
			->with('username', $user)
			->with('title', 'Sub Project Household Beneficiaries')
			->with('description', $profile->project_id)
			->with('profile', $profile);
	}

	public function edit($id) {
		$user = Session::get('username');
		$profile = SPIProfile::where(array('project_id' => $id))->first();

		return $this->view('spi_profile.edit_household')
			->with('username', $user)
			->with('title', 'Sub Project Household Beneficiaries')
			->with('description', $profile->project_id)
			->with('profile', $profile);
	}

	public function update($id) {
		$user = Session::get('username');
		$profile = SPIProfile::where(array('project_id' => $id))->first();

		$data = Input::all();
		unset($data["_token"]);

		$profile->fill($data);
		$profile->save();

		Session::flash('success', "SP Household Beneficiaries successfully updated");
		return Redirect::route('spi_profile.household', $id);
	}
}