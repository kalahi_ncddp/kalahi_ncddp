<?php

class SPIERSListController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', Session::all());

		$id             = Request::segment(2);;
		$update_status  = Session::pull("spi-erslist.$id.update", null);
		$destroy_status = Session::pull("spi-erslist.destroy", null);

		$spi_profile = SPIProfile::find($id);
		$monolog->addInfo('SPI PROFILE', array('yo' => $spi_profile));
		$erslist = $spi_profile->ers;
		$monolog->addInfo('ERS LIST', array('yo' => $erslist));
		$monolog->addInfo('ERS LIST', Session::all());
		$user = Session::get('username');
		return $this->view('spierslist.show')
			->with('title', 'CDD Sub-Project Workers & ERS')
			->with('username', $user)
			->with('update_status', $update_status)
			->with('destroy_status', $destroy_status)
			->with('project_id', $id)
			->with('spi_profile_name', $spi_profile->project_name)
			->with('ers_list', $erslist);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', Session::all());

		$is_successful = FALSE;
		$id            = NULL;
		$project_id    = Request::segment(2);
	    $date_report   = Input::get('in-datereport', false); 
		$date_start    = Input::get('in-datestart',  false); 
		$date_ending   = Input::get('in-dateending', false);
		
		if(!($project_id  === false || $date_report === false ||
		     $date_start === false || $date_ending === false))
		{
			$ers_list = new SPIERSList;
			$ers_list->project_id     = $project_id;
			$ers_list->date_reporting = date("Y-m-d", strtotime($date_report));
			$ers_list->date_start     = date("Y-m-d", strtotime($date_start));
			$ers_list->date_ending    = date("Y-m-d", strtotime($date_ending));
			$ers_list->save();
			$id = $ers_list->record_id;	

			$monolog->addInfo('ID : ', array($id));

			$is_successful = TRUE;
		}
		Session::put("spi-erslist.$project_id.update", $is_successful);

		echo $id;
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$affectedRows = SPIERSList::find($id)->delete();
		$is_successful = ($affectedRows > 0) ? TRUE : FALSE;
		Session::put("spi-erslist.destroy", $is_successful);
	}
}
