<?php

class MuniProfileController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Session::get('username');
		$mode = Session::get('accelerated');

		$data = MuniProfile::get_muni_profiles($user, $mode);

		return $this->view('muniprofile.index')
			->with('title', 'Municipal Profile')
			->with('username', $user)
			->with('data', $data);
	}

	/**
	 * Show the form ftor creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
			$user = Session::get('username');
			$office = Report::get_office($user);
			$psgc_id = Report::get_muni_psgc($user);
			
			$cycle = Report::get_current_cycle($psgc_id);
			$program = Report::get_current_program($psgc_id);
			$municipality = Report::get_municipality($psgc_id);
			$province = Report::get_province($psgc_id);
			
			$cycle_list = NCDDP::get_cycles();
			$cycle_list = array_combine($cycle_list, $cycle_list);
			
			$program_list = NCDDP::get_programs();
			$program_list = array_combine($program_list, $program_list);

            $sector_list = NCDDP::get_sectors();

			return $this->view('muniprofile.create')
			->with('title', 'Municipal Profile')
			->with('municipality', $municipality)
			->with('psgc_id', $psgc_id)
			->with('province', $province)
			->with('cycle', $cycle)
			->with('cycle_list', $cycle_list)
			->with('program', $program)
			->with('program_list', $program_list)
            ->with('sector_list', $sector_list)
            ->with('username', $user);
			
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
			$muni_profile = MuniProfile::get_muni_profile_existing(Input::get('psgc_id'), Input::get('program_id'), Input::get('cycle_id'),Session::get('accelerated'));
			if($muni_profile == NULL){
				$data = Input::all();
				unset($data["_token"]);
				unset($data["kc_code"]);
				unset($data["municipality"]);
				unset($data["province"]);
				
				$muni_profile = MuniProfile::get_muni_profile_by_psgc(Input::get('psgc_id'));

                $newsquence = $this->getNewCurSequence();
                $profile_id = 'MP'.Input::get('psgc_id').$newsquence;
				$data['profile_id'] = $profile_id;
				$data['municipal_psgc'] = $data['psgc_id'];
				$data['kc_mode']	    = Session::get('accelerated');

                unset($data['psgc_id']);

				MuniProfile::insert($data);
				
				
				return Redirect::to('muniprofile/'.$profile_id.'/edit');
			}	
			else{
				$error = 'Municipal Profile Existing. <a href = "'.URL::to('muniprofile/'.$muni_profile).'">'.$muni_profile.'</a>';
				Session::flash('message', $error);
				return Redirect::to('muniprofile/create')
				->withInput(Input::all());	
			}			
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
			$user = Session::get('username');
			$muni_profile = MuniProfile::find($id);
			$muni_techresource = MuniProfile::get_muni_profile_other_data('kc_munitechres', $id);
			$muni_income = MuniProfile::get_muni_profile_other_data('kc_munincome', $id);
			$muni_fundsource = MuniProfile::get_muni_profile_other_data('kc_munifund', $id);
			$muni_empowerment = MuniProfile::get_muni_profile_other_data('kc_empowerment', $id);
			$muni_gadfund = MuniProfile::get_muni_profile_other_data('kc_gadfund', $id);
			$muni_drrmfund = MuniProfile::get_muni_profile_other_data('kc_drrmfund', $id);
			$muni_mode_transpo = MuniProfile::get_muni_profile_other_data('kc_modetranspo', $id);
			$data['description'] = $id;
		
			return $this->view('muniprofile.show')
				->with('title', 'Municipal Profile')
				->with('username', $user)
				->with('muni_profile', $muni_profile)
				->with('muni_techresource', $muni_techresource)
				->with('muni_income', $muni_income)
				->with('muni_fundsource', $muni_fundsource)
				->with('muni_empowerment', $muni_empowerment)
				->with('muni_gadfund', $muni_gadfund)
				->with('muni_drrmfund', $muni_drrmfund)
				->with('muni_mode_transpo', $muni_mode_transpo)
				->with($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

			if(Approval::isReviewed($id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to data');
	            return Redirect::to('/muniprofile/'.$id);
	        }	

			$user = Session::get('username');
            $psgc_id = $this->user->getPsgcId($user);
			$muni_profile = MuniProfile::find($id);
			$muni_techresource = MuniProfile::get_muni_profile_other_data('kc_munitechres', $id);
			$muni_income = MuniProfile::get_muni_profile_other_data('kc_munincome', $id);
			$muni_fundsource = MuniProfile::get_muni_profile_other_data('kc_munifund', $id);
			$muni_empowerment = MuniProfile::get_muni_profile_other_data('kc_empowerment', $id);
			$muni_gadfund = MuniProfile::get_muni_profile_other_data('kc_gadfund', $id);
			$muni_drrmfund = MuniProfile::get_muni_profile_other_data('kc_drrmfund', $id);
			$muni_mode_transpo = MuniProfile::get_muni_profile_other_data('kc_modetranspo', $id);
			// dd($muni_profile);
			$data['description'] = $id;

            $muni_profile->barangay_number = \Municipality::where('municipality_psgc',$psgc_id)->first()->barangay()->count();
		
			return $this->view('muniprofile.edit')
				->with('title', 'Municipal Profile')
				->with('username', $user)
				->with('muni_profile', $muni_profile)
				->with('muni_techresource', $muni_techresource)
				->with('muni_income', $muni_income)
				->with('muni_fundsource', $muni_fundsource)
				->with('muni_empowerment', $muni_empowerment)
				->with('muni_gadfund', $muni_gadfund)
				->with('muni_drrmfund', $muni_drrmfund)
				->with('muni_mode_transpo', $muni_mode_transpo)
				->with($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$data = Input::all();
		$data['profile_id'] = $id;
		$data['is_draft'] = isset($data['is_draft']) ? 1 : 0; 

		unset($data["_token"]);
		unset($data["_method"]);
		MuniProfile::delete_muni_other_data('kc_munifund', $id);
		if(array_key_exists("fund_source", $data)){
			$fund_source_data = $data["fund_source"];
			$amount = $data["amount"];
			unset($data["fund_source"]);
			unset($data["amount"]);
			
			$i = 0;
			foreach($fund_source_data as $fund_source){
				$current_cost = str_replace( ',', '', $amount[$i] );
				MuniProfile::save_muni_other_data('kc_munifund', array('profile_id'=>$id, 'fund_source'=>$fund_source, 'amount'=>$current_cost ));
				$i++;
			}
		}

		MuniProfile::delete_muni_other_data('kc_gadfund', $id);
		if(array_key_exists("gad_activity", $data)){
			$gad_fund_data = $data["gad_activity"];
			$cost = $data["cost"];
			unset($data["gad_activity"]);
			unset($data["cost"]);
			
			$i = 0;
			foreach($gad_fund_data as $gad_fund){
				/* Strip commas */
				$current_cost = str_replace( ',', '', $cost[$i] );
				MuniProfile::save_muni_other_data('kc_gadfund', array('profile_id'=>$id, 'activity'=>$gad_fund, 'cost'=>$current_cost ));
				$i++;
			}
		}


		MuniProfile::delete_muni_other_data('kc_modetranspo', $id);
        if(array_key_exists("mode_transportation", $data)){
            $mode_transpo_data = $data["mode_transportation"];
			$cost = $data["mode_cost"];
			unset($data["mode_transportation"]);
			unset($data["mode_cost"]);
			
			$i = 0;
			foreach($mode_transpo_data as $mode_transpo){

				$current_cost = str_replace( ',', '', $cost[$i] );
				MuniProfile::save_muni_other_data('kc_modetranspo', array('profile_id'=>$id, 'mode_transportation'=>$mode_transpo, 'mode_cost'=>$current_cost ));
				$i++;
			}
		}

		MuniProfile::delete_muni_other_data('kc_drrmfund', $id);
		if(array_key_exists("drrm_activity", $data)){
			$drrm_fund_data = $data["drrm_activity"];
			$cost = $data["drrm_cost"];
			unset($data["drrm_activity"]);
			unset($data["drrm_cost"]);
			
			$i = 0;
			foreach($drrm_fund_data as $drrm_fund){
				$current_cost = str_replace( ',', '', $cost[$i] );
				MuniProfile::save_muni_other_data('kc_drrmfund', array('profile_id'=>$id, 'drrm_activity'=>$drrm_fund, 'drrm_cost'=>$current_cost ));
				$i++;
			}
		}

		MuniProfile::delete_muni_other_data('kc_munitechres', $id);
        if(array_key_exists("type_equiptment", $data)){
			$equipt_type_data = $data["type_equiptment"];
			$curr_condition = $data["curr_condition"];
			$curr_capability = $data["curr_capability"];
			$consumption = $data["consumption"];
			$rental_rate = $data["rental_rate"];
			unset($data["type_equiptment"]);
			unset($data["curr_condition"]);
			unset($data["curr_capability"]);
			unset($data["consumption"]);
			unset($data["rental_rate"]);
			$i = 0;
			foreach($equipt_type_data as $equipt_type){
				MuniProfile::save_muni_other_data('kc_munitechres', array('profile_id'=>$id, 'type_equiptment'=>$equipt_type, 'curr_condition'=>$curr_condition[$i], 'curr_capability'=>$curr_capability[$i], 'consumption'=>$consumption[$i], 'rental_rate'=>$rental_rate[$i]));
				$i++;
			}
		}
		// echo '<pre>';
		// dd($data);
		MuniProfile::delete_muni_other_data('kc_empowerment', $id);
		if(array_key_exists("org_name", $data)){
			$org_name_data = $data["org_name"];
			$org_type = (array)$data["org_type"];
			$org_formal = (array)$data["org_formal"];
			$org_accredited = (array)$data["org_accredited"];
			$org_advocacy = (array)$data["org_advocacy"];
			$org_area = isset($data["org_area"]) ? (array)$data["org_area"] : "";
			$org_years_operating = isset($data["org_years_operating"])? (array)$data["org_years_operating"]:"";
			$org_active_inactive = isset($data["org_active_inactive"]) ? (array)$data["org_active_inactive"] : "" ;
			$org_act_service = (array)$data["org_act_service"];
			$org_total_male = (array)$data["org_total_male"];
			$org_total_female = (array)$data["org_total_female"];
			$org_male_ip = (array)$data["org_male_ip"];
			$org_female_ip = (array)$data["org_female_ip"];
			$org_marginalized = (array)$data["org_marginalized"];
			unset($data["org_name"]);
			unset($data["org_type"]);
			unset($data["org_formal"]);
			unset($data["org_accredited"]);
			unset($data["org_advocacy"]);
			unset($data["org_area"]);
			unset($data["org_years_operating"]);
			unset($data["org_active_inactive"]);
			unset($data["org_act_service"]);
			unset($data["org_total_male"]);
			unset($data["org_total_female"]);
			unset($data["org_male_ip"]);
			unset($data["org_female_ip"]);
			unset($data["org_marginalized"]);
			$i = 0;
			foreach($org_name_data as $org_name){
				MuniProfile::save_muni_other_data('kc_empowerment', 
					array(
                        'profile_id'=>$id,
						'org_name'=>$org_name, 
						'org_type'=>$org_type[$i], 
						'org_formal'=>$org_formal[$i], 
						'org_accredited'=> isset($org_accredited[$i]) ?  $org_accredited[$i] : "",
						'org_advocacy'=>$org_advocacy[$i], 
						'org_area'=> isset($org_area[$i]) ? $org_area[$i] : "", 
						'org_years_operating'=>$org_years_operating[$i],
						'org_active_inactive'=> isset($org_active_inactive[$i]) ? $org_active_inactive[$i] :"", 
						'org_act_service'=>$org_act_service[$i], 
						'org_total_male'=>$org_total_male[$i],
						'org_total_male'=>$org_total_male[$i], 
						'org_total_female'=>$org_total_female[$i], 
						'org_male_ip'=>$org_male_ip[$i],
						'org_female_ip'=>$org_female_ip[$i], 
						'org_marginalized'=>$org_marginalized[$i]
						));
				$i++;
			}
		}

		MuniProfile::delete_muni_other_data('kc_munincome', $id);
		if(array_key_exists("activity", $data)){
			$activity_data = $data["activity"];
			$avg_income = $data["avg_income"];
			$act_hdinvolved = $data["act_hdinvolved"];
			$seasonality = $data["seasonality"];
			unset($data["activity"]);
			unset($data["avg_income"]);
			unset($data["act_hdinvolved"]);
			unset($data["seasonality"]);
			$i = 0;
			foreach($activity_data as $activity){
				MuniProfile::save_muni_other_data('kc_munincome', array('profile_id'=>$id, 'activity'=>$activity, 'avg_income'=>$avg_income[$i], 'act_hdinvolved'=>$act_hdinvolved[$i], 'seasonality'=>$seasonality[$i]));
				$i++;
			}
		}

        MuniProfile::where('profile_id', $data['profile_id'])
		->update($data);
		
		
		Session::flash('message', 'Municipal Profile Successfully updated');
		return Redirect::to('muniprofile/'.$id);		
	}

	public function show_mdcp($id)
	{
		if (Auth::check()){
		   	$user = Session::get('username');
			$data = MuniProfile::get_muni_profile_other_data('kc_munimdcp', $id);
//            dd($id);
//            mdcp()
//                			$data = MuniProfile::where('profile_id' ,$id)->first()->mdcp();

			//$data['id'] = $id;
		
			return $this->view('muniprofile.show_mdcp')
				->with('title', 'MDC Profile')
				->with('username', $user)
				->with('id', $id)
				->with('data', $data);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}
	
	public function delete_mdcp($id)
	{

		
		$reference = Munimcdp::where('person_id',$id)->first()->profile_id;
		/* DELETE */

			if(Approval::isReviewed($reference))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to delete data');
	            return Redirect::back();
	        }	
		Munimcdp::where('person_id',$id)->delete();
		Session::flash('message','You have Successfully MDC Profile  ');
		return Redirect::to('muniprofile/'.$reference.'/show_mdcp');
	}
	public function create_person($id){
		if (Auth::check()){

			if(Approval::isReviewed($id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to add data');
	            return Redirect::back();
	        }	
			$user = Session::get('username');
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
			$status_list = NCDDP::get_civilstatus();
			$group_list = NCDDP::get_ipgroup();
			$level_list = NCDDP::get_educlevel();
			$position_list = NCDDP::get_brgy_rep();
			
			return $this->view('muniprofile.create_person')
				->with('title', 'Add Person')
				->with('username', $user)
				->with('id', $id)
				->with('sector_list', $sector_list)
				->with('status_list', $status_list)
				->with('group_list', $group_list)
				->with('level_list', $level_list)
				->with('position_list', $position_list);
		}else{
			return View::make('reports.login')
				->with('title', 'Login');
		}
	}

	public function submit_person(){
		if (Auth::check()){
			$user = Session::get('username');
			$rules = array(
				// 'last_name'   => 'required',
				// 'first_name' => 'required',
				// 'middle_name' => 'required',
				// 'sex'   => 'required',
				// 'age' => 'required',
				// 'civil_status' => 'required',
				// 'ip_group' => 'required',
				// 'educ_attainment' => 'required',
				// 'position' => 'required',
				// 'startdate' => 'required',
				// 'enddate' => 'required',
				// 'sector_rep' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);
			$data = Input::all();
			$data['ip_group']	= isset($data['ip_group']) ? $data['ip_group'] : '';
			$data['is_ipgroup'] = isset($data['is_ipgroup']) ? 1 : 0; 

			if ($validator->fails()) {
				return Redirect::to('muniprofile/'.Input::get('id').'/create_person')
					->withErrors($validator)
					->withInput(Input::all());
			} else{
				$munimcdps = Munimlgu::where('last_name',$data['last_name'])
				                        ->where('first_name',$data['first_name'])
				                        ->where('middle_name',$data['middle_name'])
				                        ->where('sex',$data['sex'])
				                        ->exists();
				if($munimcdps){
					return Redirect::to('muniprofile/'.Input::get('id').'/create_person')
					->withErrors('This person is existing in MLGU profile')
					->withInput(Input::all());
				}
				$persons = MuniProfile::get_mdcp_id(Input::get('id'));
				if($persons != NULL){
					$person_id = substr($persons, 0, -4);
					$number = substr($persons, -4);
					$person_id = $person_id.sprintf("%04d", $number+1);
				}
				else{
					$person_id = 'PER-'.Input::get('id').'-0001';

				}
                /* set sectors rep*/
                if(array_key_exists("sector_rep", $data)){

                    $sector_represented = "";
                    foreach($data["sector_rep"] as $sector){
                        $sector_represented = $sector_represented.','.$sector;
                    }
                    unset($data["sector_rep"]);
                   $sector = ltrim ($sector_represented, ',');

                }
                /* end set*/
                $person = array('profile_id' => Input::get('id'),
					'person_id' => $person_id,
					'last_name' => Input::get('last_name' ,""),
					'first_name' => Input::get('first_name',""),
					'middle_name' => Input::get('middle_name'),
					'sex' => Input::get('sex'),
					'age' => Input::get('age'),
					'civil_status' => Input::get('civil_status',""),
					'is_ipgroup' => Input::get('is_ipgroup',0),
					'ip_group' => Input::get('ip_group',0),
					'educ_attainment' => Input::get('educ_attainment',""),
					'position' => Input::get('position',""),
					'startdate' =>  $this->setDate($data["startdate"]),
					'enddate' => $this->setDate($data["enddate"]),
                    'sector_rep' => isset($sector) ? $sector : ""
				);

				MuniProfile::save_muni_other_data('kc_munimdcp', $person);

				return Redirect::to('muniprofile/'.Input::get('id').'/show_mdcp')
					->with('message', 'Person profile added.');
					
			}
		}
	}

	public function show_person($id){
		if (Auth::check()){
			$user = Session::get('username');
			$person = MuniProfile::get_person($id);
//            $person->sector_rep =  explode(",", $person->sector_rep);

            return $this->view('muniprofile.show_person')
				->with('title', 'MDC Profile')
				->with('id', $id)
				->with('person', $person)
				->with('username', $user);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function edit_person($id)
	{
		if (Auth::check()){
			$user = Session::get('username');
			$person = MuniProfile::get_person($id);
			if(Approval::isReviewed($person->profile_id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to edit data');
	            return Redirect::back();
	        }	
			$status_list = NCDDP::get_civilstatus();
			$group_list = NCDDP::get_ipgroup();
			$level_list = NCDDP::get_educlevel();
			$position_list = NCDDP::get_brgy_rep();
            $person->sector_rep =  explode(",", $person->sector_rep);

            $sector_list = NCDDP::get_sectors();
            $sector_list = array_combine($sector_list, $sector_list);
			return $this->view('muniprofile.edit_person')
				->with('title', 'MDC Profile')
				->with('id', $id)
				->with('username', $user)
				->with('person', $person)
				->with('sector_list', $sector_list)
				->with('status_list', $status_list)
				->with('group_list', $group_list)
				->with('level_list', $level_list)
				->with('position_list', $position_list);
			
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function update_person()
	{
		$id = Input::get('person_id');
		$rules = array(
			// 'last_name'   => 'required',
			// 'first_name' => 'required',
			// 'middle_name' => 'required',
			// 'sex'   => 'required',
			// 'age' => 'required',
			// 'civil_status' => 'required',
			// 'ip_group' => 'required',
			// 'educ_attainment' => 'required',
			// 'position' => 'required',
			// 'startdate' =>  'required',
			// 'enddate' => 'required',
			// 'sector_rep' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('muniprofile/'.$id.'/edit_person')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			
				$data = Input::all();
				$data["startdate"] = $this->setDate($data["startdate"]);
				$data["enddate"] = $this->setDate($data["enddate"]);

				unset($data["_token"]);
				unset($data["_method"]);
				$data['ip_group']	= isset($data['is_ipgroup']) ? $data['ip_group'] : '';
				$data['is_ipgroup'] = isset($data['is_ipgroup']) ? 1 : 0; 
				$data['person_id'] = $id;


                /* set sectors rep*/
                if(array_key_exists("sector_rep", $data)){

                    $sector_represented = "";
                    foreach($data["sector_rep"] as $sector){
                        $sector_represented = $sector_represented.','.$sector;
                    }
                    $data["sector_rep"] = ltrim ($sector_represented, ',');

                }
                /* end set*/

				DB::table('kc_munimdcp')
           			->where('person_id', $id)
            		->update($data);
				
				Session::flash('message', 'Person profile successfully updated');
				return Redirect::to('muniprofile/'.$id.'/show_person');
				
		}
	}

	public function show_mlgu($id)
	{
		if (Auth::check()){
		   	$user = Session::get('username');
			$data = MuniProfile::get_muni_profile_other_data('kc_munimlgu', $id);
			
			//$data['id'] = $id;
		
			return $this->view('muniprofile.show_mlgu')
				->with('title', 'MLGU Officials Profile')
				->with('username', $user)
				->with('id', $id)
				->with('data', $data);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}
	
	public function create_person_mlgu($id){
		if (Auth::check()){
			if(Approval::isReviewed($id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to add data');
	            return Redirect::back();
	        }	
			$user = Session::get('username');
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
			$status_list = NCDDP::get_civilstatus();
			$group_list = NCDDP::get_ipgroup();
			$level_list = NCDDP::get_educlevel();
			$position_list = DB::table('RF_VolPost')->lists('position','position');
			return $this->view('muniprofile.create_person_mlgu')
				->with('title', 'Add MLGU')
				->with('username', $user)
				->with('id', $id)
				->with('sector_list', $sector_list)
				->with('status_list', $status_list)
				->with('group_list', $group_list)
				->with('level_list', $level_list)
				->with('position_list', $position_list);
		}else{
			return View::make('reports.login')
				->with('title', 'Login');
		}
	}

	public function submit_person_mlgu(){
		if (Auth::check()){
			$user = Session::get('username');
			$rules = array(
				// 'last_name'   => 'required',
				// 'first_name' => 'required',
				// 'middle_name' => 'required',
				// 'sex'   => 'required',
				// 'age' => 'required',
				// 'civil_status' => 'required',
				// 'ip_group' => 'required',
				// 'educ_attainment' => 'required',
				// 'position' => 'required',
				// 'startdate' => 'required',
				// 'enddate' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);
			$data = Input::all();
			$data['ip_group']	= isset($data['is_ipgroup']) ? $data['ip_group'] : '';
			$data['is_ipgroup'] = isset($data['is_ipgroup']) ? 1 : 0; 

			if ($validator->fails()) {
				return Redirect::to('muniprofile/'.Input::get('id').'/create_person_mlgu')
					->withErrors($validator)
					->withInput(Input::all());
			} else{
				$persons = MuniProfile::get_mlgu_id(Input::get('id'));
				if($persons != NULL){
					$person_id = substr($persons, 0, -4);
					$number = substr($persons, -4);
					$person_id = $person_id.sprintf("%04d", $number+1);
				}
				else{
					$person_id = 'PER-'.Input::get('id').'-0001';

				}
				$person = array('profile_id' => Input::get('id'),
					'person_id' => $person_id,
					'last_name' => Input::get('last_name'),
					'first_name' => Input::get('first_name'),
					'middle_name' => Input::get('middle_name'),
					'sex' => Input::get('sex'),
					'age' => Input::get('age'),
					'civil_status' => Input::get('civil_status'),
					'is_ipgroup' => Input::get('is_ipgroup'),
					'ip_group' => Input::get('ip_group'),
					'educ_attainment' => Input::get('educ_attainment'),
					'position' => Input::get('position'),
					'startdate' =>  $this->setDate($data["startdate"]),
					'enddate' => $this->setDate($data["enddate"])
				);

				MuniProfile::save_muni_other_data('kc_munimlgu', $person);

				return Redirect::to('muniprofile/'.Input::get('id').'/show_mlgu')
					->with('message', 'Person profile added.');
					
			}
		}
	}

	public function show_person_mlgu($id){
		if (Auth::check()){
			$user = Session::get('username');
			$person = MuniProfile::get_person_mlgu($id);

			return $this->view('muniprofile.show_person_mlgu')
				->with('title', 'MLGU Officials Profile')
				->with('id', $id)
				->with('person', $person)
				->with('username', $user);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function delete_mlgu($id)
	{

		
		$reference = Munimlgu::where('person_id',$id)->first()->profile_id;
		/* DELETE */
		if(Approval::isReviewed($reference))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to delete data');
	            return Redirect::back();
	        }	
		Munimlgu::where('person_id',$id)->delete();
		Session::flash('message','You have Successfully Deleted MLGU Officials Profile ');
		return Redirect::to('muniprofile/'.$reference.'/show_mlgu');
	}

	public function edit_person_mlgu($id)
	{
		if (Auth::check()){
			$user = Session::get('username');
			$person = MuniProfile::get_person_mlgu($id);
			if(Approval::isReviewed($person->profile_id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to edit data');
	            return Redirect::back();
	        }	
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
			$status_list = NCDDP::get_civilstatus();
			
			$group_list = NCDDP::get_ipgroup();
			$level_list = NCDDP::get_educlevel();
			$position_list = DB::table('RF_VolPost')->lists('position','position');


			return $this->view('muniprofile.edit_person_mlgu')
				->with('title', 'MLGU Officials Profile')
				->with('id', $id)
				->with('username', $user)
				->with('person', $person)
				->with('sector_list', $sector_list)
				->with('status_list', $status_list)
				// ->with('ip_group', $group_list),
				
				->with('group_list', $group_list)
				->with('level_list', $level_list)
				->with('position_list', $position_list);
			
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function update_person_mlgu()
	{
		$id = Input::get('person_id');
		$rules = array(
			// 'last_name'   => 'required',
			// 'first_name' => 'required',
			// 'middle_name' => 'required',
			// 'sex'   => 'required',
			// 'age' => 'required',
			// 'civil_status' => 'required',
			// 'ip_group' => 'required',
			// 'educ_attainment' => 'required',
			// 'position' => 'required',
			// 'startdate' =>  'required',
			// 'enddate' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('muniprofile/'.$id.'/edit_person_mlgu')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			
				$data = Input::all();
				$data["startdate"] = $this->setDate($data["startdate"]);
				$data["enddate"] = $this->setDate($data["enddate"]);
				$data['ip_group']	= isset($data['is_ipgroup']) ? $data['ip_group'] : '';
				$data['is_ipgroup'] = isset($data['is_ipgroup']) ? 1 : 0; 
				unset($data["_token"]);
				unset($data["_method"]);
				
				$data['person_id'] = $id;
				
				DB::table('kc_munimlgu')
           			->where('person_id', $id)
            		->update($data);
				
				Session::flash('message', 'Person profile successfully updated');
				return Redirect::to('muniprofile/'.$id.'/show_person_mlgu');
				
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		MuniProfile::delete_muni_other_data('kc_munitechres', $id);
		MuniProfile::delete_muni_other_data('kc_munincome', $id);
		MuniProfile::delete_muni_other_data('kc_munifund', $id);
		
		MuniProfile::where('profile_id',$id)->delete();
		
		// redirect
		Session::flash('message', 'Municipal Profile Successfully deleted!');
		return Redirect::to('muniprofile');
	}
}