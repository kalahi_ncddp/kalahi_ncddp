<?php

class ProjectTrainingController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['project'] = ProjectTraining::get();
		$data['title']	  = 'Project Training';

		return $this->view('admindb.project_training.index')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		//
		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['title']	  = 'Project Training';
		$data['curr_position'] = $this->reference->getCurrentPosition();
		$data['program']  = $this->reference->getPrograms();
		$data['offices'] = $this->reference->getOffices();
		$data['statusofemployment'] = $this->reference->getStatusEmployment();
		$data['region_name'] = $this->grsRepository->getRegionList();

		$data['category'] = DB::table('rf_serviceprovider_category')->lists('category','category');
		$data['status'] = DB::table('rf_serviceprovider_status')->lists('status','status');
		$id = $this->user->getPsgcId($data['username']);
//        get the province id
        $province = Municipality::find($id)->isProvince;

        $region   = Municipality::find($id)->province->region->psgc_id;
        $current_cycle = $this->reference->getCycleById($id);


        $data['municipalities'] =  $this->grsRepository->getMunicipalityList($province->province_psgc);
        $data['province'] = $province;
        
        $data['region'] = $region;


		return $this->view('admindb.project_training.create')->with($data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//

		$data = Input::all();

		// $psgc = $this->user->getPsgcId(Session::get('username'));
		// Staff::where('name_of_service_provider','23123')->get()
		$rules = array(
			// 'name_of_service_provider' => 'required'
		);
		$validator = Validator::make($data, $rules);
		
		if ($validator->fails()) {	
			return Redirect::to('project_training/create')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			// $name_of_service_provider = $this->user->generateEmployeeId($psgc);
			// // $data['name_of_service_provider'] = $name_of_service_provider;

			// // add first to emphist
			$newsquence = $this->getNewCurSequence();
					
			$training_id = 'T'.Input::get('muni_psgc').$newsquence;
			// dd($supplier_id);

			$input = $data;
			
			$input['training_id'] = $training_id;
			// $input['name_of_service_provider'] = $psgc;
			ProjectTraining::create($input);

			// create
		}
		Session::flash('message','Successfully added ' );
		return Redirect::to('project-training');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['training'] = ProjectTraining::where('training_id',$id)->first();
		$data['title']	  = 'Project Training';

		return $this->view('admindb.project_training.show')->with($data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['title']	  = 'Edit Project Training';
		$data['curr_position'] = $this->reference->getCurrentPosition();
		$data['program']  = $this->reference->getPrograms();
		$data['offices'] = $this->reference->getOffices();

		$data['training'] = ProjectTraining::where('training_id',$id)->first();
		$data['statusofemployment'] = $this->reference->getStatusEmployment();
		
		$id = $this->user->getPsgcId($data['username']);

		$province = Municipality::find($id)->isProvince;

        $region   = Municipality::find($id)->province->region->psgc_id;
        $current_cycle = $this->reference->getCycleById($id);


        $data['municipalities'] =  $this->grsRepository->getMunicipalityList($province->province_psgc);
        $data['province'] = $province;
        $data['region'] = $region;
		return $this->view('admindb.project_training.edit')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//

		$data = Input::all();

		$psgc = $this->user->getPsgcId(Session::get('username'));
		$rules = array(
			// 'employee_id' => 'required'
		);
		$validator = Validator::make($data, $rules);
		
		if ($validator->fails()) {	
			return Redirect::to('project-training/'.$id.'/edit')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			unset($data['_method']);
			unset($data['_token']);

			$input = $data;

			// unset($input['muni_psgc']);
			// unset($input['name_of_service_provider']);
			// unset($input['category']);
			// unset($input['address']);
			// unset($input['status']);
			// unset($input['remarks']);
			// unset($input['contact_person']);
			// unset($input['email']);
		

			
			// echo "<pre>";
			// dd($input);
			ProjectTraining::where('training_id',$id)->update($input);
			
			
			// create
		}
		Session::flash('message','Successfully updated '.$id );
		return Redirect::to('project-training');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//

		ProjectTraining::where('training_id',$id)->delete();
		
		return Redirect::to('project-training')->with('message','Successfully Deleted '.$id);
	}


}
