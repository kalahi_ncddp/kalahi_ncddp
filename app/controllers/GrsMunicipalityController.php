<?php

class GrsMunicipalityController extends \BaseController {

	/**
	 * Display a listing of the of GRS Municipality.
	 *
	 * @return GET Response
	 */
	public function index()
	{
		$mode = Session::get('accelerated');
		$data['username'] = Session::get('username');
        $data['username'] = $this->user->getPsgcId($data['username'], $mode);
		$data['title'] = 'GRS Municipality';
		$data['data'] = Report::get_municipality($data['username']);
		$data['grsmunicipalities'] = GrsMuniModel::where('psgc_id', $data['username'])->where('kc_mode',$mode)->with('municipality.province')
												   ->with('municipality.province.region')->get();
 
		return $this->view('grsmunicipality.index')->with($data);
	}


	/**
	 * create forms for GRS 
	 *
	 * @return GET Response
	 */
	public function create()
	{
		 $user = Session::get('username');
        $user = $this->user->getPsgcId($user);
		$data['title'] = 'GRS Municipality';
		$office   = Report::get_office($user);
		$psgc_id  = Report::get_muni_psgc($user);
		
		$kc_class = [];
		$cycles   = [];

		// $kc_class[0] = 0;
		foreach (MunClass::get() as $kc ) {
			$kc_class[$kc->classification] = $kc->classification;
		}
		
		foreach (NCDDP::get_cycles() as $cycle) {
			$cycles[$cycle] = $cycle;
		}


		if($office === 'ACT'){
			$barangay_id = Report::get_psgc_id($psgc_id);
		}
		else if($office === 'RPMO' or $office === 'SRPMO'){
			$barangay_id = Report::get_prov_psgc_id($psgc_id);
		}
		else{
			$barangay_id = Report::get_all_brgy_psgc_id();
		}
		
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);
		$barangay_list = array('NULL' => '') + $barangay_list;


		$data['data'] = Municipality::where('municipality_psgc', $user)
														->with('province')
														->with('province.region')->first();

		$data['municipality'] = Municipality::with('province')
													->with('province.region')
													->first();
		$data['username'] = $user;
		$data['barangay_lists'] = $barangay_list;
 		$data['kc_class'] 		= $kc_class;
 		$data['cycles']   		= $cycles;

		$municipality = Report::get_municipality($user);
		$province = Report::get_province($user);
 		$data['province']  = $province;
 		$data['municipality'] = $municipality;
 		$data['region']  = Report::get_region($user);
 		$data['programs'] = $this->reference->getPrograms();
 		$data['cycle'] = $cycle = LguCycles::where('psgc_id',$user)->first();
		return $this->view('grsmunicipality.create')->with($data);
	}	


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return POST Response
	 *
	 *
	 *
	 *
	 */

	public function store()
	{
		$input = Input::all();
		
		$rules = array(
			// 'program_id'    => 'required',
			'cycle_id' => 'required',
			
		);

		$data['psgc_id'] 		= $input['municipality'];
		$data['program_id']     = $input['program_id']; 
		$data['cycle_id']		= $input['cycle_id']; 
		$data['date_orientation']	= date('Y-m-d',strtotime($input['date_orientation']));
		$data['date_orientation']	= date('Y-m-d',strtotime($input['date_orientation']));;
		$data['date_ffcomm']	= $this->setDate($input['date_ffcomm']);
		$data['date_training']	= $this->setDate($input['date_training']);
		$data['date_inspect']	= $this->setDate($input['date_inspect']);
		$data['no_brochures']	= $input['no_brochures'];
		$data['no_tarpauline']	= $input['no_tarpauline'];
		$data['no_posters']		= $input['no_posters'];
		$data['other_mat']      = $input['other_mat'];
		$data['date_meansrept']	= date('Y-m-d',strtotime($input['date_meansrept']));;
		$data['is_boxinstalled']= isset($input['is_boxinstalled']) ? 1 : 0;
		$data['phone_no']		= $input['phone_no'];
		$data['address']		= $input['address'];
		$data['remarks']		= $input['remarks'];
        $data['kc_mode']        = Session::get('accelerated');
		$validator = Validator::make($input, $rules);
		
		if ( $validator->fails() ) {	

			return Redirect::back()
				->withErrors( $validator )
				->withInput( Input::all() );
		
		}
		else
		{	
			if( GrsMuniModel::where('psgc_id',$data['psgc_id'])->where('cycle_id',$data['cycle_id'])->where('kc_mode',$data['kc_mode']  )->count() > 0 ){
				return Redirect::to( 'grs-municipality/create' )
				->withErrors( 'Duplicate Entry' );
			}else{
				Session::flash('message', 'Installation Successfully Added');
                $newsquence = $this->getNewCurSequence();
                $activity_id = 'GRSM'.$data['psgc_id'].$newsquence;
                $data['reference_no'] = $activity_id;
				GrsMuniModel::create($data);
				return Redirect::to( 'grs-municipality' );
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return GET Responseph
	 */
	public function show($id)
	{
        $data['username'] = Session::get('username');
        $data['title'] = 'Grs Municipality';
        $cycle = Input::get('cycle');


        $data['grs']  = GrsMuniModel::where('reference_no',$id)->first();
        return $this->view('grsmunicipality.show')->with($data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return GET Response
	 */
	public function edit($id )
	{



		$user = Session::get('username');
        $user = $this->user->getPsgcId($user);
		$data['title'] = 'GRS Municipality';
		$office   = Report::get_office($user);
		$psgc_id  = Report::get_muni_psgc($user);

  		if(Approval::isReviewed($id))
        {
            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
            return Redirect::to('/grs-municipality/'.$id);
        }	
		
		$kc_class = [];
		$cycles   = [];

		// $kc_class[0] = 0;
		foreach (MunClass::get() as $kc ) {
			$kc_class[$kc->classification] = $kc->classification;
		}
		
		foreach (NCDDP::get_cycles() as $cycle) {
			$cycles[$cycle] = $cycle;
		}


		if($office === 'ACT'){
			$barangay_id = Report::get_psgc_id($psgc_id);
		}
		else if($office === 'RPMO' or $office === 'SRPMO'){
			$barangay_id = Report::get_prov_psgc_id($psgc_id);
		}
		else{
			$barangay_id = Report::get_all_brgy_psgc_id();
		}
		
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);
		$barangay_list = array('NULL' => '') + $barangay_list;


		$data['data'] = Municipality::where('municipality_psgc', $user)
														->with('province')
														->with('province.region')->first();

		$data['municipality'] = Municipality::with('province')
													->with('province.region')
													->first();
		$data['username'] = $user;
		$data['barangay_lists'] = $barangay_list;
 		$data['kc_class'] 		= $kc_class;
 		$data['cycles']   		= $cycles;
        $data['program']        =
 		$GrsMuniModel =  GrsMuniModel::where('reference_no',$id)
												->with('municipality.province')
												   ->with('municipality.province.region')->first();
		$data['programs'] = $this->reference->getPrograms();
		$data['grsmunicipality'] = $GrsMuniModel;

		return $this->view('grsmunicipality.edit')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return  PATCH Response
	 */
	public function update($id)
	{
		$input = Input::all();
		
		$rules = array(
			// 'program_id'    => 'required',
			'cycle_id' => 'required',
			'date_ffcomm' => 'required|date',

		);

		$data['psgc_id'] 		= $input['municipality'];
		$data['program_id']     = $input['program_id']; 
		$data['cycle_id']		= $input['cycle_id']; 
		$data['is_draft'] = isset($input['is_draft']) ? 1 : 0; 
		$data['date_orientation']	= date('Y-m-d',strtotime($input['date_orientation']));
		$data['date_orientation']	= date('Y-m-d',strtotime($input['date_orientation']));;
		$data['date_ffcomm']	= date('Y-m-d',strtotime($input['date_ffcomm']));;
		$data['date_training']	= date('Y-m-d',strtotime($input['date_training']));;
		$data['date_inspect']	= date('Y-m-d',strtotime($input['date_inspect']));;
		$data['no_brochures']	= $input['no_brochures'];
		$data['no_tarpauline']	= $input['no_tarpauline'];
		$data['other_mat']      = $input['other_mat'];
		$data['no_posters']		= $input['no_posters'];
		$data['date_meansrept']	= date('Y-m-d',strtotime($input['date_meansrept']));;
		$data['is_boxinstalled']= isset($input['is_boxinstalled']) ? 1 : 0;
		$data['phone_no']		= $input['phone_no'];
		$data['address']		= $input['address'];
		$data['remarks']		= $input['remarks'];

		$validator = Validator::make($input, $rules);
		
		if ( $validator->fails() ) {	

			return Redirect::back()
				->withErrors( $validator )
				->withInput( Input::all() );
		
		}
		else
		{	
			
				unset($data['psgc_id']);
				unset($data['program_id']);
				unset($data['cycle_id']);
				Session::flash('message', 'Installation Successfully Updated');
				GrsMuniModel::where('reference_no',$id)->update($data);
				return Redirect::to( 'grs-municipality' );
		
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return GET Response
	 */
	public function destroy($id)
	{
		GrsMuniModel::where('reference_no',$id)->delete();
		Session::flash('message', 'Installation Successfully Deleted');
		return Redirect::to( 'grs-municipality' );
	}

	public function lguCycles()
	{
		$input     = Input::all();
		
		$cycle_id  = $input['cycle_id'];

		$cycleData = GrsMuniModel::lguCycle($cycle_id);
	
		return Response::json($cycleData);
	}

}
