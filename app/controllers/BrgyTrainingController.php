<?php

class BrgyTrainingController extends \BaseController {



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
			$user = Session::get('username');
            $mode = Session::get('accelerated');
			$data = Trainings::get_brgy_trainings($user,$mode);

			return $this->view('brgy_trainings.index')

				->with('title', 'Community Training')

				->with('username', $user)
				->with('data', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $user = Session::get('username');
        $office = Report::get_office($user);
        $psgc_id = Report::get_muni_psgc($user);
        $municipality = Report::get_municipality($psgc_id);
        $province = Report::get_province($psgc_id);
        $region = Report::get_region($psgc_id);

        if($office === 'ACT'){
            $barangay_id = Report::get_psgc_id($psgc_id);
        }
        else if($office === 'RPMO' or $office === 'SRPMO'){
            $barangay_id = Report::get_prov_psgc_id($psgc_id);
        }
        else{
            $barangay_id = Report::get_all_brgy_psgc_id();
        }

        $barangay_name = NCDDP::get_barangay_name($barangay_id);
        $barangay_list = array_combine($barangay_id, $barangay_name);
        $barangay_list = array(NULL => '') + $barangay_list;


        $cycle = Report::get_current_cycle($psgc_id);
        $program = Report::get_current_program($psgc_id);

        $cycle_list = NCDDP::get_cycles();
        $cycle_list = array_combine($cycle_list, $cycle_list);


        $program_list = NCDDP::get_programs();
        $program_list = array_combine($program_list, $program_list);


        $lgutype = 'brgy';
        $cat_list = $this->reference->getTrainingCat($lgutype);
        $data['kc_code']     = $this->reference->getKCcode($user);
        return $this->view('brgy_trainings.create')
            ->with('title', 'Barangay Training')
            ->with('brgy_list', $barangay_list)
            ->with('cat_list', $cat_list)
            ->with('municipality', $municipality)
            ->with('province', $province)
            ->with('cycle', $cycle)
            ->with('program', $program)
            ->with('region', $region)
            ->with('cycle_list', $cycle_list)
            ->with('program_list', $program_list)
            ->with('username', $user)
            ->with($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $mode = Session::get('accelerated');
        $brgy_training = Trainings::get_training_existing(Input::get('psgc_id'), Input::get('training_title'), Input::get('cycle_id'), Input::get('training_cat'), Input::get('date_conducted'), Input::get('program_id'),$mode);
        if($brgy_training == NULL){
            $data = Input::all();
            unset($data["_token"]);
            unset($data["kc_code"]);
            unset($data["barangay"]);

            $training_cat = Input::get('training_cat');
            $brgy_training = Trainings::get_training_by_psgc(Input::get('psgc_id'));
 
            $newsquence = $this->getNewCurSequence();

            $reference_no = 'BT'.Input::get('psgc_id').$newsquence;
           

            $data['reference_no'] = $reference_no;
            $data['lgu_type'] = 1;
            $data['start_date'] = $this->setDate($data['start_date']);
        	$data['end_date'] = $this->setDate($data['end_date']);
            $data['kc_mode'] = $mode;

            /*insert to LGU*/
            $lgu_activity = array(
                'activity_id' => $reference_no,
                'psgc_id' => $data["psgc_id"],
                'activity_type' => 'BRGYTRAINING',
                'startdate' => $data['start_date'],
                'enddate' => $data['end_date'],
                'activity_name' => $training_cat,
                'kc_mode'=>Session::get('accelerated'),
                'cycle_id'=>$data['cycle_id'],
                'program_id'=>$data['program_id']
                );

            $lgu = LguActivity::where( ['activity_id'=>$reference_no,'psgc_id'=>$data["psgc_id"]])->exists();
            if($lgu){
                $lgu_activity = array('activity_name'=>$training_cat);
                LguActivity::where( ['activity_id'=>$reference_no,'psgc_id'=>$data["psgc_id"],'activity_name'=>$lgu_activity['activity_name']])->update($lgu_activity);
            }else{

                LguActivity::create($lgu_activity);
            }
            /* end to lgu*/
            Trainings::insert($data);
            Session::flash('message', 'Community Training Record Successfully Inserted!');
            return Redirect::to('brgy_trainings/'.$reference_no.'/edit');
        }
        else{
            $error = 'Barangay Training Existing. <a href = "'.URL::to('mun_trainings/'.$mun_training).'">'.$mun_training.'</a>';
            Session::flash('message', $error);
            return Redirect::to('brgy_trainings/create')
                ->withInput(Input::all());
        }
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
			$user = Session::get('username');
			
			$brgy_training = Trainings::find($id);
			
			
			$brgy_training['training_cat'] =\DB::table('rf_trainingcat')->where('catagory_id',$brgy_training['training_cat'] )
																		->orWhere('description',$brgy_training['training_cat'])->where('is_brgy',1)->first()->description;
	
			$trainor_data = Trainings::get_trainors($id);
			 $data['issues']= NCDDP::get_issues_by_id($id);

            $data['description'] = $brgy_training->reference_no;
			return $this->view('brgy_trainings.show')
				->with('title', 'Community Training')
				->with('username', $user)
				->with('training', $brgy_training)
				->with('trainor_data', $trainor_data)
                ->with($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
			$user = Session::get('username');
			
			$brgy_training = Trainings::find($id);

			if(Approval::isReviewed($id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
	            return Redirect::to('/brgy_trainings/'.$id);
	        }	

			
			$trainor_data = Trainings::get_trainors($id);
			$lgutype = 'brgy';
        	$cat_list = $this->reference->getTrainingCat($lgutype);

			$data['cat_list'] = $cat_list;
			$data['description'] = $brgy_training->reference_no;
			return $this->view('brgy_trainings.edit')
				->with('title', 'Community Training')
				->with('username', $user)
				->with('trainor_data', $trainor_data)
				->with('training', $brgy_training)
				->with($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$data = Input::all();
				unset($data["_token"]);
				unset($data["_method"]);
				
				Trainings::delete_training('kc_trainor', $id);
				
				
				if(array_key_exists("trainor_name", $data)){
					$trainor_data = $data["trainor_name"];
					$designation_data = $data["designation"];
					$topics_data = $data["topics_discussed"];
					
					unset($data["trainor_name"]);
					unset($data["designation"]);
					unset($data["topics_discussed"]);
					
					$i = 0;
					foreach($trainor_data as $trainor){
						Report::save_ncddp('kc_trainor', array('reference_no'=>$id, 'trainor_name'=>$trainor, 'designation'=>$designation_data[$i], 'topics_discussed'=>$topics_data[$i]));
						$i++;
					}
				}
		$data['is_draft'] = isset($data['is_draft']) ? 1 : 0; 
        $data['start_date'] = $this->setDate($data['start_date']);
        $data['end_date'] = $this->setDate($data['end_date']);

            $training = Trainings::where('reference_no',$id)->first();
            $training_cat = $training->training_cat;

            $lgu_activity = array(
                'activity_id' => $id,
                'psgc_id' => $training->psgc_id,
                'activity_type' => 'BRGYTRAINING',
                'startdate' => $data['start_date'],
                'enddate' => $data['end_date'],
                'activity_name' => $training_cat,
                'kc_mode'=>Session::get('accelerated'));

            $lgu = LguActivity::where( ['activity_id'=>$id,'psgc_id'=>$training->psgc_id])->exists();
            if($lgu){
                $lgu_activity = array('activity_name'=>$training_cat);
                LguActivity::where( ['activity_id'=>$id,'psgc_id'=>$training->psgc_id,'activity_name'=>$lgu_activity['activity_name']])->update($lgu_activity);
            }else{

                LguActivity::create($lgu_activity);
            }

				Trainings::update_training($data, $id);
				
				Session::flash('message', 'Community Training Record Successfully updated');
			
				return Redirect::to('brgy_trainings/'.$id);
	}

	public function enlist_participants($id){
			$user = Session::get('username');
			

			$participants_male = Trainings::get_participants_by_sex($id, 'M');
			$participants_female = Trainings::get_participants_by_sex($id, 'F');
			$data = array('no_atnmale' => $participants_male, 'no_atnfemale' => $participants_female);
			Trainings::update_training($data, $id);
			
			$data = Trainings::get_participants($id);
			
        $datas['description'] = ' (<a href="'. URL::to('brgy_trainings/'.$id).'">'.$id.'</a>) Participants';
			return $this->view('brgy_trainings.enlist_participants')
				->with('title', 'Community Training')
				->with('id', $id)
				->with('username', $user)
				->with('data', $data)
                ->with($datas);
	}

	public function add_participant($id){
			$user = Session::get('username');
			$user = $this->user->getPsgcId($user);
			$data = Volunteer::get_volunteers_list();
			$brgy_list = $this->grsRepository->getBarangayList($user);
			if(Approval::isReviewed($id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to add Participants');
	            return Redirect::to('/brgy_trainings/'.$id.'/enlist_participants');
	        }	
            $data['cycle_id']  = Trainings::where('reference_no',$id)->first()->cycle_id;
            $data['program_id']= Trainings::where('reference_no',$id)->first()->program_id;
            $data['psgc_id']   = Trainings::where('reference_no',$id)->first()->psgc_id;
            $data['sector_list'] = NCDDP::get_sectors();
			return $this->view('brgy_trainings.add_participant')
			->with('title', 'Community Training')
			->with('username', $user)
			->with('data', $data)
			->with('id', $id)
			->with('brgy_list',$brgy_list)
                ->with($data);
			
	}

	public function submit_participant(){
		$participant = Trainings::get_participant_existing(Input::get('reference_no'),Input::get('sex'),Input::get('lastname'), Input::get('firstname'), Input::get('age'));
			// dd($participant);
		$bene_id = "";
			if($participant == NULL){
				$data = Input::all();
				unset($data["_token"]);
				unset($data["_method"]);
                /* set sectors rep*/
                if(array_key_exists("sector", $data)){

                    $sector_represented = "";
                    foreach($data["sector"] as $sector){
                        $sector_represented = $sector_represented.','.$sector;
                    }
                    $data["sector"] = ltrim ($sector_represented, ',');
                    unset( $data['sector_text']);
                }else{
                    $data['sector'] =  $data['sector_text'];
                    unset( $data['sector_text']);
                }
				if($data["beneficiary_id"] == NULL){
					unset($data["beneficiary_id"]);

					$input = $data;

					unset($input['sector']);
					unset($input['is_volunteer']);
					unset($input['psgc_id']);
					unset($input['age']);
					unset($input['is_pantawid']);
					unset($input['is_slp']);
					unset($input['is_lguofficial']);
					unset($input['reference_no']);

					$bene_id = Beneficiary::generateBeneficiaryId($input);
					$data['beneficiary_id'] = $bene_id;

				}
				unset($data["birthdate"]);
				Trnparticipants::insertParticipant($data,$data['psgc_id'],'BTP');

				return Redirect::to('brgy_trainings/'.$data["reference_no"].'/enlist_participants')
					->with('message', 'Participant added.');
		
			}	
			else{
				$error = 'Participant Existing.';
				Session::flash('message', $error);
				return Redirect::to('brgy_trainings/'.Input::get('reference_no').'/add_participant')
				->withInput(Input::all());	
			}		
	}
	
	
	public function edit_participant($id) 
	{
		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);
		$volunteer = Volunteer::get_volunteers_list();
		
		$participant_id = Input::get('participant_id',null);


			$participant = Trnparticipants::where('beneficiary_id',$id)
										->first();
		if(Approval::isReviewed($participant->reference_no))
        {
            Session::flash('message','This data is already reviewed by AC.. Untag this to edit Participant');
            return Redirect::to('/brgy_trainings/'.$participant->reference_no.'/enlist_participants');
        }	
        $participant->beneficiary->sector =  explode(",", !empty($participant->beneficiary->sector)?$participant->beneficiary->sector : $participant->sector );
        $data['sector_list'] = NCDDP::get_sectors();
        $data['sector_list'] = array_combine($data['sector_list'] , $data['sector_list']);

		$brgy_list = $this->grsRepository->getBarangayList($user);

			return $this->view('brgy_trainings.edit_participant')
			->with('title', 'Community Training')
			->with('username', $user)
			->with('data', $volunteer)
			->with('participant',$participant)
			->with('id', $id)
			->with('brgy_list',$brgy_list)
			->with('description',$participant->beneficiary_id)
                ->with($data);
	}

	public function updateParticipants($id)
	{
		$input = Input::all();
		unset($input["_token"]);
		unset($input["_method"]);
        if(array_key_exists("sector", $input)){

            $sector_represented = "";
            foreach($input["sector"] as $sector){
                $sector_represented = $sector_represented.','.$sector;
            }
            $input["sector"] = ltrim ($sector_represented, ',');
            unset( $input['sector_text']);
        }else{
            $input['sector'] =  $input['sector_text'];
            unset( $input['sector_text']);
        }
		$benefiaciary_data = $input;

		unset($input["birthdate"]);
		unset($benefiaciary_data['psgc_id']);
		$reference = Trnparticipants::where('beneficiary_id',$id);
		$reference->update($input);
					unset($benefiaciary_data['is_volunteer']);
					unset($benefiaciary_data['age']);
                    unset($benefiaciary_data['sector']);
					unset($benefiaciary_data['reference_no']);
		Beneficiary::where('beneficiary_id',$id)->update($benefiaciary_data);
		return Redirect::to('brgy_trainings/'.$reference->first()->reference_no.'/enlist_participants')->with('message','Update Success');
	}
	public function delete_participant(){
		$data = Input::all();
		if(Approval::isReviewed($data['reference_no']))
        {
            Session::flash('message','This data is already reviewed by AC.. Untag this to delete Participant');
            return Redirect::to('/brgy_trainings/'.$data['reference_no'].'/enlist_participants');
        }	
		if(array_key_exists("participant", $data)){
			foreach($data["participant"] as $p){
				Trainings::delete_participant($p);
			}
			return Redirect::to('brgy_trainings/'.$data["reference_no"].'/enlist_participants')
					->with('message', 'Participant Deleted.');
		}
		else{
			return Redirect::to('brgy_trainings/'.$data["reference_no"].'/enlist_participants')
					->with('message', 'No Participant Deleted.');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Trainings::delete_training('kc_trnBrgy', $id);
		Trainings::delete_training('kc_trainor', $id);
		Trainings::delete_training('kc_trnParticipants', $id);
		Trainings::delete_training('kc_trainings', $id);
		
		return Redirect::to('brgy_trainings/')
					->with('message', 'Community Training Record Deleted.');
		
	}

}