<?php

class SupplierController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		//

		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['provider'] = Supplier::get();
		$data['title']	  = 'List of Service Provider';

		return $this->view('admindb.supplier.index')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['title']	  = 'Service Provider';
		$data['curr_position'] = $this->reference->getCurrentPosition();
		$data['program']  = $this->reference->getPrograms();
		$data['offices'] = $this->reference->getOffices();
		$data['statusofemployment'] = $this->reference->getStatusEmployment();

		$data['category'] = DB::table('rf_serviceprovider_category')->lists('category','category');
		$data['status'] = DB::table('rf_serviceprovider_status')->lists('status','status');
		$id = $this->user->getPsgcId($data['username']);
//        get the province id
        $province = Municipality::find($id)->isProvince;

        $region   = Municipality::find($id)->province->region->psgc_id;
        $current_cycle = $this->reference->getCycleById($id);


        $data['municipalities'] =  $this->grsRepository->getMunicipalityList($province->province_psgc);
        $data['province'] = $province;
        $data['region'] = $region;

		return $this->view('admindb.supplier.create')->with($data);

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data = Input::all();

		// $psgc = $this->user->getPsgcId(Session::get('username'));
		// Staff::where('name_of_service_provider','23123')->get()
		$rules = array(
			// 'name_of_service_provider' => 'required'
		);
		$validator = Validator::make($data, $rules);
		
		if ($validator->fails()) {	
			return Redirect::to('supplier/create')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			// $name_of_service_provider = $this->user->generateEmployeeId($psgc);
			// // $data['name_of_service_provider'] = $name_of_service_provider;

			// // add first to emphist
			$newsquence = $this->getNewCurSequence();
					
			$provider_id = 'P'.Input::get('muni_psgc').$newsquence;
			// dd($supplier_id);

			$input = $data;
			
			$input['provider_id'] = $provider_id;
			// $input['name_of_service_provider'] = $psgc;
			Supplier::create($input);

			// create
		}
		Session::flash('message','Successfully added ' );
		return Redirect::to('supplier');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['supplier'] = Supplier::where('provider_id',$id)->first();
		$data['title']	  = 'List of Service Providers';

		return $this->view('admindb.supplier.show')->with($data);
	
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$data['username'] = Session::get('username');
		$data['username'] = $this->user->getPsgcId($data['username']);
		$data['title']	  = 'Edit List of Providers';
		$data['curr_position'] = $this->reference->getCurrentPosition();
		$data['program']  = $this->reference->getPrograms();
		$data['offices'] = $this->reference->getOffices();

		$data['supplier'] = Supplier::where('provider_id',$id)->first();
		$data['statusofemployment'] = $this->reference->getStatusEmployment();
		
		$id = $this->user->getPsgcId($data['username']);

		$province = Municipality::find($id)->isProvince;

        $region   = Municipality::find($id)->province->region->psgc_id;
        $current_cycle = $this->reference->getCycleById($id);


        $data['municipalities'] =  $this->grsRepository->getMunicipalityList($province->province_psgc);
        $data['province'] = $province;
        $data['region'] = $region;
		return $this->view('admindb.supplier.edit')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();

		$psgc = $this->user->getPsgcId(Session::get('username'));
		$rules = array(
			// 'employee_id' => 'required'
		);
		$validator = Validator::make($data, $rules);
		
		if ($validator->fails()) {	
			return Redirect::to('supplier/'.$id.'/edit')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			unset($data['_method']);
			unset($data['_token']);

			$input = $data;

			// unset($input['muni_psgc']);
			// unset($input['name_of_service_provider']);
			// unset($input['category']);
			// unset($input['address']);
			// unset($input['status']);
			// unset($input['remarks']);
			// unset($input['contact_person']);
			// unset($input['email']);
		

			
			// echo "<pre>";
			// dd($input);
			Supplier::where('provider_id',$id)->update($input);
			
			
			// create
		}
		Session::flash('message','Successfully updated '.$id );
		return Redirect::to('supplier');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//

		Supplier::where('provider_id',$id)->delete();
		
		return Redirect::to('supplier')->with('message','Successfully Deleted '.$id);
	}


}
