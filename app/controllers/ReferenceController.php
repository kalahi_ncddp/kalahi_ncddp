<?php

class ReferenceController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function ba()
	{
		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function approved()
    {
        $input = Input::all();
        $reference_type = $input['reference_type'];
        // dd($reference_type);
        try{

            foreach($input['check'] as $ids)
            {
            
            	$exist = References::where('reference_no',$ids)->exists();
            	$data = [
            		'reference_no'=>$ids,
            		'reference_type'=>$reference_type,
            		'ac_approved'=>date('Y-m-d')
            	];
            	if($exist){
            		References::where('reference_no',$ids)->update($data);
            	}
            	else
            	{
             	   References::create($data);
            	}
            }
            return json_encode(['status'=>'ok']);
        }catch (Exception $e){
            return json_encode(['status'=>'error']);;
        }

    }

    public function approvedGRS()
    {
    	$input = Input::all();
        $reference_type = $input['reference_type'];
        try{

            foreach($input['check'] as $key => $ids)
            {
            
            	$exist = References::where('reference_no',$ids)->exists();
            	$data = [
            		'reference_no'=>$input['check'][$key].'-'.$input['program'][$key].'-'.$input['cycle'][$key],
            		'reference_type'=>$reference_type,
            		'ac_approved'=>date('Y-m-d')
            	];
            	if($exist){
            		References::where('reference_no',$ids)->update($data);
            		
            	}
            	else
            	{
             	   References::create($data);
            	}
            }
            return json_encode(['status'=>'ok']);
        }catch (Exception $e){
          	return $e;
            return json_encode(['status'=>'error']);;
        }

    }

}
