 <?php

class GrsIntakeController extends \BaseController {


    /**
	 * Display a listing of the intake.
	 *
	 * @return Responses
	 */
	public function index()
	{
      	$data['username'] = Session::get('username');
        $user = $this->user->getPsgcId($data['username']);

		$data['title'] = 'GRS Intake';
		$mode = Session::get('accelerated');
        $data['data'] = Report::get_brgy_assembly($data['username'],$mode);
        $data['intakes'] = $this->grsRepository->getAllIntakes($user);


		return $this->view('grsintake.index')->with($data);


	}
	/**
	 * Show the form for creating new intake.
	 *
	 * @return Response
	 */
	public function create()
	{
        $user = Session::get('username');

//        get the province id
        
       $data['username'] = $user;
        $data['title'] = 'Add Intake Record';
        $data['barangay'] = $this->grsRepository->getBarangayList($user);
        $data['municipalities'] =  $this->grsRepository->getMunicipalityListByPsgc($user);
        $data['municipality'] = $this->grsRepository->getMunicipality($user);
        $data['provinces'] = $this->grsRepository->getProvinceListByPsgc($user);
        $data['province'] = $this->grsRepository->getProvince($user);
        $data['regions'] = $this->grsRepository->getRegionListByPsgc($user);
        $data['region'] = "";

//        dd( $data['municipalities'] );
//        $data['municipality'] = $id;
        $data['cycles'] = $this->reference->getCycles();
        $data['cycle_id'] =  '';
        $data['programs'] = $this->reference->getPrograms();
        $data['program'] =  '';
        $data['grievance_form'] = $this->reference->getGrievanceForm();
        $data['ip_groupname'] = $this->reference->getIPGroupName();
        $data['designations']  = $this->reference->getDesignations();
        $data['mode_of_filling'] = $this->reference->getModeFilling();
        $data['genders'] =  ['M'=>'Male','F'=>'Female','U'=>'unknown','O'=>'Organization/Group/Institution'];
        $data['intake_level'] = ['municipal'=>'Municipal'];
        $data['status_resolution'] = [''=>'Select Status','On Going'=>'On Going','Pending'=>'Pending','Resolved'=>'Resolved'];
        $data['officers'] = [''=>'Select Officers','ACT'=>'ACT/MCT','RPMO'=>'RPMO','NPMO'=>'NPMO'];
        $data['activity_issues'] = $this->reference->getActivityIssuesByMuni($user);
        $data['concern_nature'] = $this->reference->getConcernNature();
        $data['concern_category'] = $this->reference->getConcernCategory();
        $data['subject_complaint'] = $this->reference->getSubjectComplaint();


        return $this->view('grsintake.create')->with($data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $input = Input::all();
        $rules = array(
            'cycle_id'   => 'required',
            'program_id'    => 'required',
        );
        $validator = \Validator::make($input, $rules);

        $exists = Intake::where(['cycle_id'=>$input['cycle_id'],'program_id'=>$input['program_id'],'intake_date'=>$this->setDate($input['intake_date']),'sender'=>$input['sender']])->exists();

        // process the login
        if($exists)
        {
//            Session::flash('errors','Duplicate entry check the date of intake and sender');
            return Redirect::to('grs-intake/create')
                ->withErrors('Duplicate entry check the date of intake and sender')
                ->withInput(Input::all());
        }
        if ($validator->fails() )
        {

            return Redirect::to('grs-intake/create')
                ->withErrors($validator)
                ->withInput(Input::all());
        }
        else {

            if( !empty($input['region_id']) ){
                $input['psgc_id'] = $input['region_id'];
            }
            else{
                $input['psgc_id'] = "00000000";
            }
            if( !empty($input['province_id']) ){
                $input['psgc_id'] = $input['province_id'];
            }
            if( !empty($input['municipality_id']) ){
                $input['psgc_id'] = $input['municipality_id'];
            }

            if( !empty($input['barangay_id']) ){
                $input['psgc_id'] = $input['barangay_id'];
            }
            $input['intake_date'] = $this->setDate($input['intake_date']) ;
//            $input['resolution_date'] = $this->setDate($input['resolution_date']);
            $sequence = $this->getNewCurSequence();
            $intake = $this->grsRepository->insertIntake($input,$sequence);

            Session::flash('message','Successfully created a Intake');
            return Redirect::to('grs-intake/'.$intake.'/edit');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($ids)
	{
        $id = Session::get('username');

        $data['username'] = $id;
        $data['title'] = 'Intake';
        $data['description'] = $ids;
        $data['intake'] = Intake::where('reference_no',$ids)->first();

        return $this->view('grsintake.show')->with($data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($ids)
	{
        $id = Session::get('username');
        $id = $this->user->getPsgcId($id);

        if(Approval::isReviewed($ids))
        {
            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
            return Redirect::to('/grs-intake/'.$ids);
        }


        $province = Municipality::find($id)->isProvince;

        $region   = Municipality::find($id)->province->region->psgc_id;
        $current_cycle = $this->reference->getCycleById($id);
        $data['username'] = $id;
        $data['title'] = 'Intake';
        $data['description'] = $ids;
        $data['intake'] = Intake::where('reference_no',$ids)->first();
        $data['barangay'] = $this->grsRepository->getBarangayList($id);
        $data['municipalities'] =  $this->grsRepository->getMunicipalityList($province->province_psgc);
        $data['regions'] = $this->grsRepository->getRegionList();
        $data['region'] = $region;
        $data['municipality'] = $id;
        $data['cycles'] = $this->reference->getCycles();
        $data['cycle_id'] =  $current_cycle->cycle_id;
        $data['programs'] = $this->reference->getPrograms();
        $data['program'] =  $current_cycle->program;
        $data['grievance_form'] = $this->reference->getGrievanceForm();
        $data['ip_groupname'] = $this->reference->getIPGroupName();
        $data['designations']  = $this->reference->getDesignations();
        $data['mode_of_filling'] = $this->reference->getModeFilling();
        $data['genders'] =  ['M'=>'Male','F'=>'Female','U'=>'unknown','O'=>'Organization/Group/Institution'];
        $data['intake_level'] = ['municipal'=>'Municipal'];
        $data['status_resolution'] = [''=>'Select Status','On Going'=>'On Going','Pending'=>'Pending','Resolved'=>'Resolved'];
        $data['status_level'] = [''=>'Select Feedback',' No comment'=>'No comment','Not satisfied'=>'Not satisfied','Satisfied'=>'Satisfied','Very Satisfied'=>'Very Satisfied'];
        $data['officers'] = [''=>'Select Officers','ACT'=>'ACT/MCT','SRPMO'=>'SRPMO','RPMO'=>'RPMO','NPMO'=>'NPMO'];
        $data['activity_issues'] = $this->reference->getActivityIssuesByMuni($id);
        $data['concern_nature'] = $this->reference->getConcernNature();
        $data['concern_category'] = $this->reference->getConcernCategory();
        $data['subject_complaint'] = $this->reference->getSubjectComplaint();

        return $this->view('grsintake.edit')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $input = Input::all();

        $rules = array(

        );

        $validator = \Validator::make($input, $rules);

        // process the login
        if ($validator->fails())
        {
            dd('s');
            return Redirect::back()
                ->withErrors($validator)
                ->withInput(Input::all());
        }
        else {
            $input['is_draft'] = isset($input['is_draft']) ? 1 : 0;
//            $input['intake_date'] = $this->setDate($input['intake_date']) ;
            $input['resolution_date'] = $this->setDate($input['resolution_date']);

            $this->grsRepository->updateIntake($input,$id);

            Session::flash('message','Successfully updated '. $id);
            return Redirect::to('grs-intake');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
         if(Approval::isReviewed($id))
        {
            Session::flash('message','This data is already reviewed by AC.. Deleting is not allowed');
            return Redirect::to('/grs-intake/'.$id);
        }

        Intake::where('reference_no',$id)->delete();
        Grsupdates::where('reference_no',$id)->delete();
        Session::flash('message','Successfully deleted a Intake');
        return Redirect::to('grs-intake');

	}

    public function pincos()
    {
        $activity_id = Input::get('activity_id');
        $issue = Input::get('issue_no');
      $activity = ActivityIssues::where('activity_id',$activity_id)->where('issue_no',$issue)->first();
      return Response::json($activity);
    }


}
