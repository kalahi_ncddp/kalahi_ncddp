<?php

use Repositories\Users\UsersInterface;

class MIBFController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

			$user = Session::get('username');
            $user = $this->user->getPsgcId($user);
            $mode = Session::get('accelerated');
			$data = Report::get_mibf($user,$mode);

            $datas['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);


			return $this->view('mibf.index')
				->with('title',  Session::get('accelerated') == 1? 'Municipal Forum' : 'Municipal Inter Barangay Forum (MIBF)')
				->with('username', $user)
				->with('data', $data)
                ->with($datas);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if (Auth::check()){
			$user = Session::get('username');
            $user = $this->user->getPsgcId($user);
			$office = Report::get_office($user);
			$psgc_id = Report::get_muni_psgc($user);
			$province = Report::get_province($psgc_id);

			if($office == "ACT"){
				$muni_id = Report::get_muni_psgc_id($psgc_id);
				$muni_name = Report::get_municipality($psgc_id);
				$muni_list = array($psgc_id => $muni_name);
				$muni_list = array('NULL' => '') + $muni_list;
			}else{
				$muni_id = Report::get_muni_psgc_id($psgc_id);
				$muni_name = NCDDP::get_municipality_name($muni_id);
				$muni_list = array_combine($muni_id, $muni_name);
				$muni_list = array('NULL' => '') + $muni_list;
			}

			$cycle = Report::get_current_cycle($psgc_id);
			$program = Report::get_current_program($psgc_id);
			//var_dump($cycle);
			$cycle_list = NCDDP::get_cycles();
			$program_list = NCDDP::get_programs();
			$mode = Session::get('accelerated');	
			$purpose_list = $this->reference->getMIBFpurpose($mode);

			//$cycle_list_rf = array_combine($cycle_list_rf, $cycle_list_rf);
			//$cycle_list_rf = array('NULL' => '') + $cycle_list_rf;
            $datas['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);
			return $this->view('mibf.create')
			->with('title',  Session::get('accelerated') == 1? 'Municipal Forum' : 'Municipal Inter Barangay Forum (MIBF)')
			->with('muni_list', $muni_list)
			->with('psgc_id', $psgc_id)
			->with('municipality', $muni_name)
			->with('province', $province)
			->with('cycle', $cycle)
			->with('program', $program)
			->with('cycle_list', $cycle_list)
			->with('program_list', $program_list)
			->with('purpose_list', $purpose_list)
			->with('username', $user)
                ->with($datas);
		}else{
			return $this->view('reports.login')
			->with('title', 'Login');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'psgc_id'   => 'required',
			'purpose'    => 'required',
			'program_id'    => 'required',
			'cycle_id' => 'required'
		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('mibf/create')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			$mode = Session::get('accelerated');
			$mibf = Report::get_mibf_existing(Input::get('psgc_id'), Input::get('program_id'), Input::get('cycle_id'), Input::get('purpose'),$mode);
			if($mibf == NULL){
				$data = Input::all();
				unset($data["_token"]);
				unset($data["municipality"]);
				unset($data["kc_code"]);
				unset($data["province"]);


                /* Populate MIBF analysis table */
                $training = Trainings::where('psgc_id',$data['psgc_id'])
                    ->where('cycle_id',$data['cycle_id'])
                    ->where('program_id',$data['program_id'])
              	    ->where('kc_mode',Session::get('accelerated'))
                    ->first();
                $data['no_atnmale'] = $training->total_participants_by_genders($gender = 'M');
                $data['no_atnfemale'] = $training->total_participants_by_genders($gender = 'F');
                  $data['no_ipmale'] =  $training->total_ip('M');
                $data['no_ipfemale'] = $training->total_ip('F');
                $data['no_oldmale'] = $training->age_participants( 'M', 60);
                $data['no_oldfemale'] = $training->age_participants( 'F', 60);
                $data['no_lgumale'] = $training->total_lguofficial('M');
                $data['no_lgufemale'] = $training->total_lguofficial('F');

                /* end */

				$newsquence = $this->getNewCurSequence();
				$activity_id = 'MIBF'.Input::get('psgc_id').$newsquence;


				$data['activity_id'] = $activity_id;

				$data['kc_mode'] = Session::get('accelerated');
//
                $activity_type = $this->getActivityType($data['purpose']);

				$lgu_activity = array(
                		'activity_id' => $activity_id,
                		'psgc_id' => $data["psgc_id"],
                		'startdate' => isset($data['start_date']) ?$this->setDate($data['start_date']) : null,
                		'enddate' =>isset($data['start_date']) ? $this->setDate($data['end_date']) : null,
                		'activity_name' => $activity_type,
                		'activity_type' => 'MIBF',

                		);

                $lgu = LguActivity::where( ['activity_id'=>$activity_id,'psgc_id'=>$data["psgc_id"]])->exists();

                if($lgu){
                    $lgu_activity = array('activity_name'=> $activity_type);
                    LguActivity::where( ['activity_id'=>$activity_id,'psgc_id'=>$data["psgc_id"],'activity_name'=>$lgu_activity['activity_name']])->update($lgu_activity);
                }else{

                    LguActivity::create($lgu_activity);
                }




                $data['is_draft'] = isset($data['is_draft']) ? 1 : 0;
                unset($data['start_date']);
                unset($data['end_date']);
                Report::save_ncddp('kc_municipalforum', $data);

				Session::flash('message','Successfully Added Mibf data');
				// return Redirect::to('mibf/edit')
				return Response::json(['id'=>$activity_id]);
			}

			else{
				$error = 'Municipal Forum Existing. <a href = "'.URL::to('mibf/'.$mibf).'">'.$mibf.'</a>';
				Session::flash('message', $error);
				return Response::json(['error'=>true]);
				// return Redirect::to('mibf/create')
				// ->withInput(Input::all());
			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

			$user = Session::get('username');
            $user = $this->user->getPsgcId($user);
			$psgc_id = Report::get_muni_psgc($user);

			$mibf = NCDDP::get_mibf_by_id($id);
			$criteria = NCDDP::get_criteria($id);

			$issues = NCDDP::get_issues_by_id($id);
			$sector_data = NCDDP::get_sectors_by_id($id);
			$brgy_rep_data = NCDDP::get_brgy_rep_by_id($id);
			$total_barangays = NCDDP::count_barangays($psgc_id);
			$total_brgy_rep = NCDDP::count_barangays_rep($id);

			$data['description'] = $mibf->activity_id;
			$purpose =  DB::table('rf_mibfpurpose')->where('code',$mibf['purpose'])->first();
			$mibf['purpose'] =isset($purpose)? $purpose->purpose : $mibf['purpose'];
			// dd($criteria);
           $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);
			return $this->view('mibf.show')
				->with('title',  Session::get('accelerated') == 1? 'Municipal Forum' : 'Municipal Inter Barangay Forum (MIBF)')
				->with('username', $user)
				->with('total_barangays', $total_barangays)
				->with('total_brgy_rep', $total_brgy_rep)
				->with('mibf', $mibf)
				->with('criteria', $criteria)
				->with('issues', $issues)
				->with('sector_data', $sector_data)
                ->with($data)
				->with('brgy_rep_data', $brgy_rep_data);


	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
			if(Approval::isReviewed($id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
	            return Redirect::to('/mibf/'.$id);
	        }	
			$user = Session::get('username');
            $user = $this->user->getPsgcId($user);
			$mibf = NCDDP::get_mibf_by_id($id);
			$criteria = NCDDP::get_criteria($id);
			$sector_data = NCDDP::get_sectors_by_id($id);
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
			$brgy_rep_data = NCDDP::get_brgy_rep_by_id($id);
			$brgy_rep_list = NCDDP::get_brgy_rep();
			$brgy_rep_list = array('NULL' => '') + $brgy_rep_list;

			$barangay_id = Report::get_psgc_id($mibf->psgc_id);
			$barangay_name = NCDDP::get_barangay_name($barangay_id);
			$barangay_list = array_combine($barangay_id, $barangay_name);
			$barangay_list = array('NULL' => '') + $barangay_list;
            $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);
			$data['description'] = $mibf->activity_id;
			$purpose =  DB::table('rf_mibfpurpose')->where('code',$mibf['purpose'])->first();
			$mibf['purpose'] =isset($purpose)? $purpose->purpose : $mibf['purpose'];

			return $this->view('mibf.edit')
				->with('title',  Session::get('accelerated') == 1? 'Municipal Forum' : 'Municipal Inter Barangay Forum (MIBF)')
				->with('username', $user)
				->with('mibf', $mibf)
				->with('criteria', $criteria)
				->with('sector_list', $sector_list)
				->with('sector_data', $sector_data)
				->with('brgy_rep_data', $brgy_rep_data)
				->with('brgy_rep_list', $brgy_rep_list)
				->with('barangay_list', $barangay_list)
				->with($data);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$purpose = Input::get('purpose');
		if(Input::get('action') == 'proposal'){
			if(Input::get('old_rank') != Input::get('rank')){
				if($purpose == "Prioritization"){
					$rules = array(
						'rank'   => 'required',
						'project_name'   => 'required',
						'lead_brgy' => 'required',
						'priority' => 'required',
						'rank' => 'unique:kc_projproposal,rank,NULL,id,type,1'
					);
				}else if($purpose == "Special"){
					$rules = array(
						'rank'   => 'required',
						'project_name'   => 'required',
						'lead_brgy' => 'required',
						'priority' => 'required',
						'rank' => 'unique:kc_projproposal,rank,NULL,id,type,2'
					);
				}
            else if($purpose == "Municipal Forum"){
                $rules = array(
                    'rank'   => 'required',
                    'project_name'   => 'required',
                    'lead_brgy' => 'required',
                    'priority' => 'required',
                    'rank' => 'unique:kc_projproposal,rank,NULL,id,type,2'
                );
            }
			}else{
				$rules = array(
					'rank'   => 'required',
					'project_name'   => 'required',
					'lead_brgy' => 'required',
					'priority' => 'required'
				);
			}
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('mibf/'.$id.'/edit_proposal/'.$purpose)
				->withErrors($validator)
				->withInput(Input::all());
		} else {

				$data = Input::all();
				unset($data["old_rank"]);
				unset($data["_token"]);
				unset($data["_method"]);
				unset($data["id"]);
				unset($data["action"]);
				unset($data["purpose"]);
				$data['project_id'] = $id;

				if(array_key_exists("barangay", $data)){
					$barangay = $data["barangay"];
					unset($data["barangay"]);
					Report::delete_project_coverage($id);
					$i = 0;
					foreach($barangay as $brgy){
						if(($brgy != 'default') && ($brgy != Input::get('lead_brgy'))){
							DB::table('kc_projcoverage')->insert(array(
								'project_id' => $id,
								'name' => $brgy)
							);
						}
						$i++;
					}
					DB::table('kc_projcoverage')->insert(array(
						'project_id' => $id,
						'name' => Input::get('lead_brgy'))
					);
				}
				else{
					Report::delete_project_coverage($id);
					DB::table('kc_projcoverage')->insert(array(
						'project_id' => $id,
						'name' => Input::get('lead_brgy'))
					);
				}

            $input = Input::all();

                if($purpose == "Special"){
                    $special = Projproposalspecial::where('project_id',$input['id'])->exists();
                    $count   = Projproposalspecial::where('project_id',$input['id'])->count();
                    if($special){
                        Projproposalspecial::where('project_id',$input['id'])->update([
                            'project_id' => $input['id'],
                            'activity_id' => $input['activity_id'],
                            'rank' => $input['rank'],
                            'project_name' => $input['project_name'],
                            'priority' => $input['priority'],
                            'kc_amount' => $input['kc_amount'],
                            'lcc_amount' => $input['lcc_amount'],
                            'type' => 4,
                            'lead_brgy' => $input['lead_brgy']
                        ]);
                    }else{
                        Projproposalspecial::create([
                            'special_id' => $input['id'] .''. $count + 1,
                            'project_id' => $input['id'],
                            'activity_id' => $input['activity_id'],
                            'rank' => $input['rank'],
                            'project_name' => $input['project_name'],
                            'priority' => $input['priority'],
                            'kc_amount' => $input['kc_amount'],
                            'lcc_amount' => $input['lcc_amount'],
                            'type' => 4,
                            'lead_brgy' => $input['lead_brgy'],
                        ]);
                    }
                }else{
                    Report::update_proposal($data);
                }

				if($purpose == "Prioritization")
					Session::flash('message', 'Prioritization successfully updated');


				return Redirect::to('mibf/'.$data['activity_id'].'/show_proposals/'.$purpose);
			}
		}else{
			$rules = array(
				'startdate'   => 'required',
				'enddate'   => 'required',
			);
			$validator = Validator::make(Input::all(), $rules);

			// process the login
			if ($validator->fails()) {
				return Redirect::to('mibf/'.$id.'/edit')
					->withErrors($validator)
					->withInput(Input::all());
			} else {
				$data = Input::all();
				unset($data["_token"]);
				unset($data["_method"]);
				$data['activity_id'] = $id;


				$lgu_activity = array('activity_id' => $id, 'startdate' => $this->setDate($data["startdate"]) , 'enddate' => $this->setDate($data["enddate"]) );
                Report::update_ncddp('kc_lguactivity', $lgu_activity);

				unset($data["startdate"]);
				unset($data["enddate"]);
				unset($data["action"]);

				if(array_key_exists("sector", $data)){
					$sector_data = $data["sector"];
					unset($data["sector"]);
					Report::delete_ncddp('kc_sectorsrep', $id);
					foreach($sector_data as $sector){
						Report::save_ncddp('kc_sectorsrep', array('activity_id'=>$id, 'sector'=>$sector));
					}
				}
				else{
					Report::delete_ncddp('kc_sectorsrep', $id);
				}
				if(array_key_exists("barangay_psgc", $data)){
					$barangay_psgc = $data["barangay_psgc"];
					$brgy_rep1 = $data["brgy_rep1"];
					$brgy_rep2 = $data["brgy_rep2"];
					$brgy_rep3 = $data["brgy_rep3"];
					unset($data["barangay_psgc"]);
					unset($data["brgy_rep1"]);
					unset($data["brgy_rep2"]);
					unset($data["brgy_rep3"]);
					Report::delete_ncddp('kc_brgyrep', $id);
					$i = 0;
					foreach($barangay_psgc as $psgc){
						if($psgc != 0){
							DB::table('kc_brgyrep')->insert(array(
								'activity_id' => $id,
								'barangay_psgc' => $psgc,
								'brgy_rep1' => $brgy_rep1[$i],
								'brgy_rep2' => $brgy_rep2[$i],
								'brgy_rep3' => $brgy_rep3[$i])
							);
							$i++;
						}
					}
				}
				else{
					Report::delete_ncddp('kc_brgyrep', $id);
				}

				if(Input::get('purpose') == "Criteria Setting Workshop" || Input::get('purpose') == "CSW"){
					if(array_key_exists("criteria", $data)){
						$criteria = $data["criteria"];
						$rate = $data["rate"];
						unset($data["criteria"]);
						unset($data["rate"]);
						Report::delete_ncddp('kc_criteria', $id);
						$i = 0;
						foreach($criteria as $criterion){
							if($criterion != NULL){
								DB::table('kc_criteria')->insert(array(
									'activity_id' => $id,
									'criteria' => $criterion,
									'rate' => $rate[$i])
								);
								$i++;
							}
						}
					}
					else{
						Report::delete_ncddp('kc_criteria', $id);
					}
				}

				unset($data["criteria"]);
				unset($data["rate"]);
                $data['is_draft'] = isset($data['is_draft']) ? 1 : 0;

                /* Populate MIBF analysis table */
                $mibf = MunicipalForum::where('activity_id',$id)->first();
                $training = Trainings::where('psgc_id',$mibf->psgc_id)
                    ->where('cycle_id',$mibf->cycle_id)
                    ->where('program_id',$mibf->program_id)
                    ->where('kc_mode',Session::get('accelerated'))
                    ->first();
                $data['no_atnmale'] = $training->total_participants_by_genders($gender = 'M');
                $data['no_atnfemale'] = $training->total_participants_by_genders($gender = 'F');
                $data['no_ipmale'] =  $training->total_ip('M');
                $data['no_ipfemale'] = $training->total_ip('F');
                $data['no_oldmale'] = $training->age_participants( 'M', 60);
                $data['no_oldfemale'] = $training->age_participants( 'F', 60);
                $data['no_lgumale'] = $training->total_lguofficial('M');
                $data['no_lgufemale'] = $training->total_lguofficial('F');

                /* end */

                Report::update_ncddp('kc_municipalforum', $data);
				Session::flash('message',  Session::get('accelerated') == 1? 'Municipal Forum' : 'Municipal Inter Barangay Forum (MIBF)' .'Successfully updated');

				return Redirect::to('mibf/'.$id);
			}
		}
	}

	public function show_proposals($id, $purpose){
			$user = Session::get('username');
        $user = $this->user->getPsgcId($user);

        if($purpose == "Prioritization") {
                $datas = NCDDP::get_proposals($id, 1);
            }
			else if($purpose == "Special"){
                $mf =       \MunicipalForum::where('activity_id',$id)->first();
                $pr =     \MunicipalForum::where('program_id', $mf->program_id)->where('cycle_id',$mf->cycle_id)->first();
                $datas =  ProjProposal::where('activity_id', $pr->activity_id)
                    ->where('type', 1)
                    ->get();

//                ProjProposal::
            }
            else if($purpose == 'Municipal Forum'){
                $datas = NCDDP::get_proposals($id, 3);
            }

			$coverage = Report::get_proj_coverage();
            $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);
            if($purpose == "Prioritization"){
		    		$title = 'Prioritizations';
		    		$data['description'] = $id;
            }
		    else if($purpose == "Special"){
		    		$title = 'Special MIBF';
		    		$data['description'] = $id;
		    }
            else if($purpose == "Municipal Forum"){
                $title = 'Municipal Forum';
                $data['description'] = $id;
            }
			return $this->view('mibf.show_proposals')
				->with('title', $title)
				->with('id', $id)
				->with('purpose', $purpose)
				->with('username', $user)
				->with('data', $datas)
				->with('coverage', $coverage)
                ->with($data);
	}

	public function add_proposal($id, $purpose){
		if (Auth::check()){
			$user = Session::get('username');
            $user = $this->user->getPsgcId($user);
			$office = Report::get_office($user);
			$psgc_id = Report::get_muni_psgc($user);

			if($office === 'ACT'){
				$barangay_id = Report::get_psgc_id($psgc_id);
			}
			else if($office === 'RPMO' or $office === 'SRPMO'){
				$barangay_id = Report::get_prov_psgc_id($psgc_id);
			}
			else{
				$barangay_id = Report::get_all_brgy_psgc_id();
			}




        	if($purpose == "Prioritization")
	    		$title = 'Add Prioritization';
	    	else if($purpose == "Special")
	    		$title = 'Add Special MIBF';
            else if($purpose == "Municipal Forum")
                $title = 'Municipal Forum';


			$barangay_name = NCDDP::get_barangay_name($barangay_id);
			$barangay_list = array_combine($barangay_id, $barangay_name);
			$barangay_list = array_combine($barangay_id, $barangay_name);
			$barangay_list1 = array('default' => 'Please select barangay') + $barangay_list;
            $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);
			return $this->view('mibf.add_proposal')
			->with('title', $title)
			->with('brgy_list', $barangay_list)
			->with('brgy_list1', $barangay_list1)
			->with('username', $user)
			->with('id', $id)
			->with('purpose', $purpose)
            ->with($data);

		}else{
			return $this->view('reports.login')
			->with('title', 'Login');
		}
	}

	public function submit_proposal(){
		if (Auth::check()){
			$user = Session::get('username');
            $user = $this->user->getPsgcId($user);
			$psgc_id = Report::get_muni_psgc($user);
			$purpose = Input::get('purpose');
			if($purpose == "Prioritization"){
				$rules = array(
					'rank'   => 'required',
					'project_name'   => 'required',
					'lead_brgy' => 'required',
					'priority' => 'required'
//					'rank' => 'unique:kc_projproposal,rank,project_name,NULL,id,type,1'
				);
			}else if($purpose == "Special"){
				$rules = array(
					'rank'   => 'required',
					'project_name'   => 'required',
					'lead_brgy' => 'required',
					'priority' => 'required'
//					'rank' => 'unique:kc_projproposal,rank,project_name,NULL,id,type,2'
				);
			}
            else if($purpose == "Municipal Forum"){
                $rules = array(
                    'rank'   => 'required',
                    'project_name'   => 'required',
                    'lead_brgy' => 'required',
                    'priority' => 'required'
//                    'rank' => 'unique:kc_projproposal,project_name,rank,NULL,id,type,2'
                );
            }
            $id = Input::get('id');
			$validator = Validator::make(Input::all(), $rules);
			$data = Input::all();

			if($purpose == "Prioritization"){

				$type = 1;
            	$proposalexist = ProjProposal::where('activity_id',$id)->where('rank',Input::get('rank'))->exists();

			}
			else if($purpose == "Special"){

				$type = 2;
            	$proposalexist = ProjProposal::where('activity_id',$id)->where('rank',Input::get('rank'))->exists();

			}
            else if($purpose == "Municipal Forum"){

                $type = 3;
            	$proposalexist = ProjProposal::where('activity_id',$id)->where('rank',Input::get('rank'))->exists();

            }

            if($proposalexist){
         		return Redirect::to('mibf/'.Input::get('id').'/add_proposal/'.$purpose)
					->withErrors('Rank has been taken')
					->withInput(Input::all());
            }
            if ($validator->fails()) {
				return Redirect::to('mibf/'.Input::get('id').'/add_proposal/'.$purpose)
					->withErrors($validator)
					->withInput(Input::all());
			} else{
				$proposals = NCDDP::get_proposals_id(Input::get('id'));
//                    dd(Input::get('id'));
				if($proposals != NULL){
					$project_id = substr($proposals, 0, -4);
					$number = substr($proposals, -4);
					$project_id = $project_id.sprintf("%04d", $number+1);
				}
				else{
					$project_id = 'PROJ-'.Input::get('id').'-0001';

				}
				$proposal = array('project_id' => $project_id,
					'activity_id' => Input::get('id'),
					'rank' => Input::get('rank'),
					'project_name' => Input::get('project_name'),
					'priority' => Input::get('priority'),
					'kc_amount' => Input::get('kc_amount'),
                    'lcc_amount' => Input::get('lcc_amount'),
					'lead_brgy' => Input::get('lead_brgy'),
					'type' => $type
				);
				if(array_key_exists("barangay", $data)){
					$barangay = $data["barangay"];
					unset($data["barangay"]);
					Report::delete_project_coverage($project_id);


					$i = 0;
					foreach($barangay as $brgy){
						if(($brgy != 'default') && ($brgy != Input::get('lead_brgy'))){

							DB::table('kc_projcoverage')->insert(array(
								'project_id' => $project_id,
								'name' => $brgy)
							);

						}
						$i++;
					}

					DB::table('kc_projcoverage')->insert(array(
						'project_id' => $project_id,
						'name' => Input::get('lead_brgy'))
					);
				}
				else{

					Report::delete_project_coverage($id);
					DB::table('kc_projcoverage')->insert(array(
						'project_id' => $id,
						'name' => Input::get('lead_brgy'))
					);
				}

				Report::save_ncddp('kc_projproposal', $proposal);

				return Redirect::to('mibf/'.Input::get('id').'/show_proposals/'.$purpose)
					->with('message', 'Project proposal added.');

			}
		}
	}

	public function edit_proposal($id, $purpose)
	{


		if (Auth::check()){
			$user = Session::get('username');
            $user = $this->user->getPsgcId($user);
			$office = Report::get_office($user);
			$psgc_id = Report::get_muni_psgc($user);

			$proposal = NCDDP::get_proposal_by_id($id);
			$coverage = Report::get_proj_coverage_by_id($id);

			if($office === 'ACT'){
				$barangay_id = Report::get_psgc_id($psgc_id);
			}
			else if($office === 'RPMO' or $office === 'SRPMO'){
				$barangay_id = Report::get_prov_psgc_id($psgc_id);
			}
			else{
				$barangay_id = Report::get_all_brgy_psgc_id();
			}

			if($purpose == "Prioritization"){

	    		$title = 'Edit Prioritization';
	    		$url = 'Prioritization';
			}
	    	else if($purpose == "Special"){

	    		$title = 'Edit Special MIBF';
	    		$url = 'Special';
	    		
	    	}
            else if($purpose == "Municipal Forum"){

                $title = 'Edit Municipal Forum';
	    		$url = 'Municipal Forum';
            }



            if(Approval::isReviewed($proposal->activity_id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
	            return Redirect::to('/mibf/'.$proposal->activity_id.'/show_proposals/'.$url);
	        }
                $barangay_id = Report::get_psgc_id($psgc_id);
			$barangay_name = NCDDP::get_barangay_name($barangay_id);
			$barangay_list = array_combine($barangay_id, $barangay_name);
			$barangay_list1 = array('default' => 'Please select barangay') + $barangay_list;
            $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);
			return $this->view('mibf.edit_proposal')
				->with('title', $title)
				->with('username', $user)
				->with('proposal', $proposal)
				->with('brgy_list', $barangay_list)
				->with('brgy_list1', $barangay_list1)
				->with('coverage', $coverage)
				->with('purpose', $purpose)
                ->with($data);

		}else{
			return $this->view('reports.login')
			->with('title', 'Login');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{


		$action = Input::get('action');
		$purpose = Input::get('purpose');

	

        if($action == 'proposal'){
			$id = Input::get('id');
	        $mibf_id = DB::table('kc_projproposal')->where('project_id',$id)->first()->activity_id;

			if(Approval::isReviewed($mibf_id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
	            return Redirect::to('/mibf/'.$mibf_id.'/show_proposals/'.$purpose);
	        }
			$activity_id = Input::get('activity_id');

			Report::delete_project_coverage($id);
			Report::delete_proposals($id);
			Session::flash('message', 'Proposal successfully deleted');
			return Redirect::to('mibf/'.$activity_id.'/show_proposals/'.$purpose);
		}else{

			Report::delete_ncddp('kc_sectorsrep', $id);
			//Report::delete_ncddp('kc_brgyrep', $id);

	        Report::delete_ncddp('kc_projproposal',$id);
	        Report::delete_ncddp('kc_municipalforum', $id);
		}
		// redirect
		Session::flash('message', 'Successfully deleted!');
		return Redirect::to('mibf');
	}

	public function pincos()
	{
		session_start(); //hello
		//return Redirect::intended('reports');
		if( isset($_REQUEST['logout'])) {
			Auth::logout();
			Session::flush();
		}

		if (Auth::check()){
			$user = Session::get('username');
            $user = $this->user->getPsgcId($user);
			$data = Input::all();
			$activity_id = $data['activity_id'];
			if(array_key_exists("raise", $data)){
				$raise_by = $data['raise'];
				$issue = $data['issue'];
				$action = $data['action'];
				Report::delete_ncddp('kc_activityissues', $activity_id);
				$i = 0;
				foreach($raise_by as $r){
					DB::table('kc_activityissues')->insert(array(
						'activity_id' => $activity_id,
						'issue_no' => $i,
						'raise_by' => $r,
						'issue' => $issue[$i],
						'action' => $action[$i])
					);
					$i++;
				}
			}else{
					Report::delete_ncddp('kc_activityissues', $activity_id);
			}
			Session::flash('message', 'PINCOS Successfully updated.');
				return Redirect::to('mibf/'.$activity_id);
		}else{
			$username = Input::get('username');
			$password =Input::get('password');
			if (Auth::attempt(array('username' => $username, 'password' => $password))){
				Session::put('username', $username);
				return Redirect::to('');
			}else{
				if($username != NULL){
					return Redirect::to('');
				}else{
					return $this->view('reports.login')
					->with('title', 'Login');
				}
			}
		}
	}

    private function getActivityType($purpose)
    {
        return DB::table('rf_mibfpurpose')->where('purpose',$purpose)->pluck('code');
    }

    public function approved()
    {
        $input = Input::all();

        try{
            foreach($input['check'] as $ids)
            {
               $lgu =  MunicipalForum::where('activity_id',$ids[0])->where('purpose',$ids[1])->first();
                    LguActivity::where('activity_id',$lgu->activity_id)->update(['ac_approved'=>date('Y-m-d')]);
            }
            return json_encode(['status'=>'ok']);
        }catch (Exception $e){
            return json_encode(['status'=>'error']);
        }

    }

    public function getMunicipalTraining()
	{

		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		  $mibf = \MunicipalForum::where('psgc_id',Auth::user()->psgc_id)->get();
        $psgc_id = [];
        $cycles = [];
        $programs = [];
        foreach ($mibf as $psas) {
            $psgc_id[] = $psas->psgc_id;
            $cycles[] = $psas->cycle_id;
            $programs[] = $psas->program_id;
        }
        if(Session::get('accelerated') == 1){
            $train = \Trainings::where('psgc_id',Auth::user()->psgc_id)->whereIn('training_cat',['MF','CSW','MIBF-PRA','Prioritization','Special'])
            					->where('kc_mode',Session::get('accelerated'))
            					->get();
        }else{
            $train = \Trainings::where('psgc_id',Auth::user()->psgc_id)
            		->whereIn('training_cat',['CSW','MIBF-PRA','Prioritization','Special'])
            		->where('kc_mode',Session::get('accelerated'))->get();

        }

		// $train = $this->reference->getCommunityTrainingForPSA();
		// dd($train);
		return Datatable::collection($train)
		        ->showColumns('training_title','cycle_id','program_id')
		         ->addColumn('purpose',function($training){
		         	return $training->training_cat;
		         })
		         ->addColumn('psgc_id',function($training){

		         	return $training->getMunicipal->municipality;
		         })
		         ->addColumn('Date Conducted',function($training){
		         	return toDate($training->start_date);
		         })
		         ->addColumn('option', function($trainings) 
		         {
		         	  // $vol = $volunteer->volunteer!=null ? $volunteer->volunteer->psgc_id : '';
                     $cat = DB::table('rf_mibfpurpose')->where('code',$trainings->training_cat)->pluck('purpose');
		         	  $ifPSA = \MunicipalForum::where(['purpose'=>$cat,
		         	  									'program_id'=>$trainings->program_id,
		         	  									'cycle_id'=>$trainings->cycle_id,
		         	  									'psgc_id'=>$trainings->psgc_id])
		         	  							->where('kc_mode',Session::get('accelerated'))
		         	  							->exists();
                     if($trainings->training_cat=='Special'){
                            $noprio = \MunicipalForum::whereIn('purpose',['MIBF-PRA','Prioritization'])
                            						->where(['program_id'=>$trainings->program_id,'cycle_id'=>$trainings->cycle_id,'psgc_id'=>$trainings->psgc_id])
                            						->where('kc_mode',Session::get('accelerated'))->exists();

                            if(!$noprio){
                                return 'Add MIBF-PRA first';
                            }
                     }
		         	  if($ifPSA){
		         	  	return 'added';
		         	  }
				      return  '<a class="btn btn-success btn" onclick="$(this).use_mibf($(this));">Use
				                       <div class="hidden">
												<div class="program_id">'.$trainings->program_id .'</div>
												<div class="cycle_id">'.$trainings->cycle_id .'</div>
												<div class="psgc_id">'. $trainings->psgc_id .'</div>
												<div class="start_date">'. $trainings->start_date .'</div>
												<div class="end_date">'. $trainings->end_date .'</div>
												<div class="purpose">'.$trainings->training_cat.'</div>
											 </div>
				                            </a>';

				    })
		        ->searchColumns('firstname')
		        ->orderColumns('lastname', 'firstname','birthdate','middlename')
		        ->make();
	}

	public function generate_report()
	{
		$data = [];
		$user = Session::get('username');
		$municipal_psgc_id 		= Report::get_muni_psgc($user);
		$provincial_psgc_id 	= Report::get_prov_psgc($municipal_psgc_id);

		$data['office'] 			= Report::get_office($user);
		$data['municipality'] = Report::get_municipality($municipal_psgc_id);
		$data['province'] 		= Report::get_staff_province($municipal_psgc_id);
		$data['region'] 			= Report::get_region($municipal_psgc_id);
		$data['mibf_list'] 		= Report::get_all_mibf_priority_proposals_act($user);
		$pdf = PDF::loadView('PDF.mibf-act', $data);
		return $pdf->download(date('YmdHis'). ' - MIBF Priority Proposal');

	}
}