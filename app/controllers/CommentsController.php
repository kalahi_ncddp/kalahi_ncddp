<?php

class CommentsController extends \BaseController {
    public function getComments()
    {

        $office = Input::get('office','SRPMO');
        try {
            // RPMO 
            $this->retrieveComment('RPMO');
            // SRPMO
            $this->retrieveComment('SRPMO');
            // NPMO
            $this->retrieveComment('NPMO');
        }catch (Exception $e){
           Session::flash('error','failed to retrieve comments from the server');
        }
        return Redirect::to('reports');
    }
    public function retrieveComment($office = 'SRPMO')
    {
        $mode = Session::get('accelerated');
        $psgc_id = Auth::user()->psgc_id;

//        return $psgc_id;?
        $header = array( "Content-type: application/json" );
       $urls = 'http://ncddpdb.dswd.gov.ph/';
        // $urls = 'http://localhost:8000/';
        $url = $urls.'/comment?psgc='.$psgc_id.'&office='.$office.'&mode='.$mode;
        // Start cURL
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $results = curl_exec($ch);
        curl_close($ch);

        $results = (array) json_decode($results);
//        dd($url);
        foreach($results as $result) {
            $exists = Comments::where('activity_id',$result->activity_id)
                ->where('office',$office)->where('kc_mode',$mode)->exists();
            if($exists) {
                Comments::where('activity_id',$result->activity_id)
                    ->where('office',$office)->where('kc_mode',$mode)->update(['comments'=>$result->comments]);
            } else {
                Comments::create([
                    'activity_id'=>$result->activity_id,
                    'commented_by'=>$result->commented_by,
                    'comments'=>$result->comments,
                    'kc_mode'=>$result->kc_mode,
                    'psgc'=>$result->psgc,
                    'office'=>$result->office,
                    'module_type'=>$result->module_type
                ]);
            }

        }


    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
