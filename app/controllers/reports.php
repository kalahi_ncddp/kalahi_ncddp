<?php



class Reports extends BaseController {

	public $restful = true;

	public function index() {

		$user = Session::get('username');
		$office = Report::get_office($user);
		$psgc_id = Report::get_muni_psgc($user);

		$muni = Report::get_staff_municipality($psgc_id);
		$province = Report::get_staff_province($psgc_id);
		$region = Report::get_staff_region($psgc_id);
        $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);

        $cycle = Report::get_current_cycle($psgc_id);

        return $this->view('reports.index')
			->with('title', '')
			->with('username', $user)
			->with('office', $office)
			->with('muni', $muni)
			->with('province', $province)
			->with('region', $region)
            ->with($data);
	}
    public function getComments()
    {

        $office = Input::get('office','SRPMO');
        try {
            $this->retrieveComment($office);

        }catch (Exception $e){
            Session::flash('error','failed to retrieve comments from the server');
        }
        return Redirect::to('reports');
    }
    public function retrieveComment($office = 'SRPMO')
    {
        $mode = Session::get('accelerated');
        $psgc_id = Session::get('username');
        $psgc_id = \Municipality::where('municipality_psgc',$psgc_id)->first()->province->province_psgc;
//        return $psgc_id;?
        $header = array( "Content-type: application/json" );
        $urls = 'http://ncddpdb.dswd.gov.ph';
        $url = $urls.'/comment?psgc='.$psgc_id.'&ofice='.$office.'&mode='.$mode;

        // Start cURL
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $results = curl_exec($ch);
        curl_close($ch);

        $results = (array) json_decode($results);

//        sync to comments

        foreach($results as $result) {
            $exists = Comments::where('activity_id',$result->activity_id)
                                ->where('office',$office)->where('kc_mode',$mode)->exists();

            if($exists) {
                Comments::where('activity_id',$result->activity_id)
                    ->where('office',$office)->where('kc_mode',$mode)->update(['comments'=>$result->comments]);
            } else {
                Comments::create([
                    'activity_id'=>$result->activity_id,
                    'commented_by'=>$result->commented_by,
                    'comments'=>$result->comments,
                    'kc_mode'=>$result->kc_mode,
                    'psgc'=>$result->psgc,
                    'office'=>$result->office,
                    'module_type'=>$result->module_type
                ]);
            }

        }


        return $result;
    }
}