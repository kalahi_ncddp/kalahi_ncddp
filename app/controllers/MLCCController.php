    <?php

class MLCCController extends \BaseController {

	public function index() {
		$user = Session::get('username');
		$psgc_id = Report::get_muni_psgc($user);
		$mode = Session::get('accelerated');
		$mlcc_list = MLCCMain::where('kc_mode',$mode)
			->where(array('municipal_psgc' => $psgc_id))
			->where('kc_mode', $mode)
			->get();

		
			return $this->view('mlcc.index')
			->with('username', $user)
			->with('title', 'Municipal Consolidated Status of LCC')
			->with('mlcc_list', $mlcc_list);
	}

	public function show($id) {
		$user = Session::get('username');
		$mlcc = MLCCMain::where(array('mlcc_id' => $id))->first();
		$mlcc_det_list = MLCCDetail::where(array('mlcc_id' => $id))->get();

		return $this->view('mlcc.show')
			->with('username', $user)
			->with('title', 'Municipal Consolidated Status of LCC')
			->with('description', $id)
			->with('mlcc', $mlcc)
			->with('mlcc_det_list', $mlcc_det_list);
	}

	public function create() {
		$user = Session::get('username');
		$psgc_id = Report::get_muni_psgc($user);
		$mlcc_categs = MLCCCategory::get();

		$mlcc = new MLCCMain(array(
			'municipal_psgc'	=> $psgc_id
		));
		
		$mlcc['no_of_barangays'] = \Municipality::where('municipality_psgc',$psgc_id)->first()->barangay()->count();
		$mlcc_det_list = [];
		foreach($mlcc_categs as $categ) {
			array_push($mlcc_det_list, new MLCCDetail(array(
				'category' => $categ->category
			)));
		}
		$data['cycles'] = $this->reference->getCycles();
		$data['programs'] = $this->reference->getPrograms();	
		return $this->view('mlcc.create')
			->with('username', $user)
			->with('title', 'Municipal Consolidated Status of LCC')
			->with('mlcc', $mlcc)
			->with('mlcc_det_list', $mlcc_det_list)
			->with($data);
	}

	public function store() {
		$user = Session::get('username');
		$data = Input::all();
		unset($data["_token"]);

		// store main
		$psgc_id = Auth::user()->psgc_id;
	$newsquence = $this->getNewCurSequence();

		$data["mlcc_id"] = 'MLCC'.$psgc_id.$newsquence;
		$data['municipal_psgc'] = $psgc_id;
		$data["as_of_date"] = $this->setDate($data["mlccmain"]["as_of_date"]);
		$data['kc_mode']	    = Session::get('accelerated');
		$mlcc = MLCCMain::create($data);
		// store details
		$categories = isset($data["category"]) ? $data["category"] : ""  ;
		if(!empty($data['category'])){

			foreach($categories as $category) {
				$detail = new MLCCDetail(array(
					'mlcc_id' => $data["mlcc_id"],
					'category' => $category,
					'plgu_planned' => $data["plgu_planned"][$category],
					'plgu_actual' => $data["plgu_actual"][$category],
					'mlgu_planned' => $data["mlgu_planned"][$category],
					'mlgu_actual' => $data["mlgu_actual"][$category],
					'blgu_planned' => $data["blgu_planned"][$category],
					'blgu_actual' => $data["blgu_actual"][$category],
					'others_planned' => $data["others_planned"][$category],
					'others_actual' => $data["others_actual"][$category],
				));
				$detail->save();
			}
		}

		Session::flash('success', "MLCC successfully created");
		return Redirect::route('mlcc.show', $data["mlcc_id"]);
	}

	public function edit($id) {
		$user = Session::get('username');
		$mlcc = MLCCMain::where(array('mlcc_id' => $id))->first();
		$mlcc_det_list = MLCCDetail::where(array('mlcc_id' => $id))->get();
		$mlcc['no_of_barangays'] = \Municipality::where('municipality_psgc',Auth::user()->psgc_id)->first()->barangay()->count();
	$data['cycles'] = $this->reference->getCycles();
		$data['programs'] = $this->reference->getPrograms();	
		return $this->view('mlcc.edit')
			->with('username', $user)
			->with('title', 'Municipal Consolidated Status of LCC')
			->with('description', $id)
			->with('mlcc', $mlcc)
			->with('mlcc_det_list', $mlcc_det_list)
			->with($data);
	}

	public function update($id) {
		$user = Session::get('username');
		$mlcc = MLCCMain::where(array('mlcc_id' => $id))->first();

		$data = Input::all();
		$data['is_draft'] = isset($data['is_draft']) ? 1 : 0; 
		$data['kc_mode'] = Session::get('accelerated');
		unset($data["_token"]);

		// store main
		$data["as_of_date"] = $this->setDate($data["mlccmain"]["as_of_date"]);
		$mlcc->fill($data);
		$mlcc->save();

		// store details
		$categories = $data["category"];
		foreach($categories as $category) {
			$detail = MLCCDetail::where(array('mlcc_id' => $mlcc->mlcc_id, 'category' => $category))->first();
			$detail->fill(array(
				'plgu_planned' => $data["plgu_planned"][$category],
				'plgu_actual' => $data["plgu_actual"][$category],
				'mlgu_planned' => $data["mlgu_planned"][$category],
				'mlgu_actual' => $data["mlgu_actual"][$category],
				'blgu_planned' => $data["blgu_planned"][$category],
				'blgu_actual' => $data["blgu_actual"][$category],
				'others_planned' => $data["others_planned"][$category],
				'others_actual' => $data["others_actual"][$category]
			));
			$detail->save();
		}

		Session::flash('success', "MLCC successfully updated.");
		return Redirect::route('mlcc.show', $id);
	}

	public function destroy($id) {
		$mlcc = MLCCMain::where(array('mlcc_id' => $id))->first();
		$mlcc->delete();

		MLCCDetail::where(array('mlcc_id' => $id))->delete();

		return Redirect::route('mlcc.index');
	}

	/**
	 * Generates an mlcc id.
	 */
	private function generate_mlcc_id($psgc_id) {
		$last = MLCCMain::where(array('municipal_psgc' => $psgc_id))->orderBy('created_at', 'desc')->first();

		$rand = '0001';
		if($last != NULL) {
			$rand_part = substr($last->project_id, -4);
			$rand = sprintf("%04d", $rand+1);
		}

		return 'MLCC-'.$psgc_id.'-'.$rand;
	}
}