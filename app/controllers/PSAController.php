<?php

use Repositories\Users\UsersInterface;

class PSAController extends \BaseController {

    protected $user;

    /**
     * @param UsersInterface $usersInterface
     */

        /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		session_start(); //hello
		//return Redirect::intended('reports');
		if( isset($_REQUEST['logout'])) {
			Auth::logout();
			Session::flush();
		}

		if (Auth::check()){
			$user = Session::get('username');
			try {
            $mode = Session::get('accelerated');
			$data = Report::get_psa($user,$mode);
			}catch(Exception $e){
				$data = array();
			}
			
            $datas['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);
			return $this->view('psa.index')
				->with('title', 'Barangay Participatory Situation Analysis')
				->with('username', $user)
				->with('data', $data)
                ->with($datas);
				
		}else{
			$username = Input::get('username');
			$password =Input::get('password');
			if (Auth::attempt(array('username' => $username, 'password' => $password))){
				Session::put('username', $username);
				return Redirect::to('');
			}else{
				if($username != NULL){
					return Redirect::to('');
				}else{
					return $this->view('reports.login')
					->with('title', 'Login');
				}
			}
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if (Auth::check()){
			$user = Session::get('username');
			$office = Report::get_office($user);
			$psgc_id = Report::get_muni_psgc($user);
			
			if($office === 'ACT'){
				$barangay_id = Report::get_psgc_id($psgc_id);
			}
			else if($office === 'RPMO' or $office === 'SRPMO'){
				$barangay_id = Report::get_prov_psgc_id($psgc_id);
			}
			else{
				$barangay_id = Report::get_all_brgy_psgc_id();
			}
			
			$barangay_name = NCDDP::get_barangay_name($barangay_id);
			$barangay_list = array_combine($barangay_id, $barangay_name);
			$barangay_list = array('NULL' => '') + $barangay_list;
			
			$cycle = Report::get_current_cycle($psgc_id);
			$program = Report::get_current_program($psgc_id);
			$municipality = Report::get_municipality($psgc_id);
			$province = Report::get_province($psgc_id);
			
			$cycle_list = NCDDP::get_cycles();
			$cycle_list = array_combine($cycle_list, $cycle_list);
			$cycle_list = array('NULL' => '') + $cycle_list;
			$mode = Session::get('accelerated');	
			$purpose_list = $this->reference->getMIBFpurpose($mode);



            $datas['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);
			return $this->view('psa.create')
			->with('title', 'Barangay Participatory Situation Analysis')
			->with('brgy_list', $barangay_list)
			->with('municipality', $municipality)
			->with('province', $province)
			->with('cycle', $cycle)
			->with('cycle_list', $cycle_list)
			->with('program', $program)
			->with('username', $user)
                ->with($datas);
			
		}else{
			return $this->view('reports.login')
			->with('title', 'Login');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'psgc_id'   => 'required',
			'program_id'    => 'required',
			'cycle_id' => 'required'
		);
		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('psa/create')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			$mode = Session::get('accelerated');
			$psa = Report::get_psa_existing(Input::get('psgc_id'), Input::get('program_id'), Input::get('cycle_id'), Input::get('purpose'),$mode);
			if($psa == NULL){
				$data = Input::all();
				unset($data["_token"]);
				unset($data["barangay"]);
				unset($data["kc_code"]);
				unset($data["municipality"]);
				unset($data["province"]);
				
				$psa = Report::get_psa_by_psgc(Input::get('psgc_id'));



                /* Populate PSA analysis table */
                $training = Trainings::where('psgc_id',$data['psgc_id'])
                                    ->where('cycle_id',$data['cycle_id'])
                                    ->where('program_id',$data['program_id'])
                                    ->where('kc_mode',Session::get('accelerated'))
                                    ->first();

            $data['no_atnmale'] = $training->total_participants_by_genders($gender = 'M');
            $data['no_atnfemale'] = $training->total_participants_by_genders($gender = 'F');
	           
	            $data['no_ipmale'] =  isset($training) ? $training->total_ip('M') : 0;
	            $data['no_ipfemale'] = isset($training) ? $training->total_ip('F') : 0;
	            $data['no_oldmale'] = isset($training) ? $training->age_participants( 'M', 60) : 0;
	            $data['no_oldfemale'] = isset($training) ? $training->age_participants( 'F', 60) : 0;
                /* end */
				$newsquence = $this->getNewCurSequence();
				$activity_id = 'PSA'.Input::get('psgc_id').$newsquence;
				
				
				$data['activity_id'] = $activity_id;
				$data['kc_mode'] = Session::get('accelerated');



                $lgu_activity = array(
                		'activity_id' => $activity_id, 
                		'psgc_id' => $data["psgc_id"], 
                		'startdate' => $this->setDate($data['start_date']),
                		'enddate' => $this->setDate($data['end_date']),

                		'activity_type' => 'PSA',

                		);
                $lgu = LguActivity::where( ['activity_id'=>$activity_id,'psgc_id'=>$data["psgc_id"]])->exists();
                if($lgu){

                    LguActivity::where( ['activity_id'=>$activity_id,'psgc_id'=>$data["psgc_id"]])->update($lgu_activity);
                }else{

                    LguActivity::create($lgu_activity);
                }
                $data['is_draft'] = isset($data['is_draft']) ? 1 : 0;
                $data['kc_mode'] =  Session::get('accelerated');
                unset($data['start_date']);
                unset($data['end_date']);
                Report::save_ncddp('kc_psanalysis', $data);
				Session::flash('message','Successfully Added PSA data');
				
				return Response::json(['id'=>$activity_id]);

			}	
			else{
				$error = 'PSA Existing. <a href = "'.URL::to('psa/'.$psa).'">'.$psa.'</a>';
				Session::flash('message', $error);
				return Response::json(['error'=>true]);

				// return Redirect::to('psa/create')
				// ->withInput(Input::all());	
			}			
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if (Auth::check()){
			$user = Session::get('username');
			


			$psa = NCDDP::get_psa_by_id($id);
			$sector_data = NCDDP::get_sectors_by_id($id);
			$sitio_data = NCDDP::get_sitios_by_id($id);
			$psa_document = NCDDP::get_psa_documents_by_id($id);
			$psa_problem = NCDDP::get_psa_problems_by_id($id);
			$issues = NCDDP::get_issues_by_id($id);
			// $issues = PSAProjects::where('activity_id',$id)->get();
			// dd($issues->toArray());



            $data['position'] = User::where('username',$user)->first()->offive_level;
            $data['description'] = $psa->activity_id;
            $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);
//            dd(Report::get_region_by_brgy_id($psa->psgc_id));
//            dd($psa_document);
            return $this->view('psa.show')
				->with('title', 'Barangay Participatory Situation Analysis')
				->with('username', $user)
				->with('psa', $psa)
				->with('issues', $issues)
				->with('sector_data', $sector_data)
				->with('psa_document', $psa_document)
				->with('psa_problem', $psa_problem)
				->with('sitio_data', $sitio_data)
				->with($data);
		}else{
			return $this->view('reports.login')
			->with('title', 'Login');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::check()){

			if(Approval::isReviewed($id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
	            return Redirect::to('/psa/'.$id);
	        }	
			$user = Session::get('username');
			
			$psa = NCDDP::get_psa_by_id($id);
			$sector_data = NCDDP::get_sectors_by_id($id);
			$sitio_data = NCDDP::get_sitios_by_id($id);
			$psa_document = NCDDP::get_psa_documents_by_id($id);
			$psa_problem = NCDDP::get_psa_problems_by_id($id);
			$problem_cat = NCDDP::get_problem_cat();
			$solution_cat = NCDDP::get_solution_cat();
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
            $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);
			$data['description'] = $psa->activity_id;


			// dd($psa->getTraining()->total_participants_by_genders('M')|'0');
			return $this->view('psa.edit')
				->with('title', 'Barangay Participatory Situation Analysis')
				->with('username', $user)
				->with('psa', $psa)
				->with('psa_problem', $psa_problem)
				->with('problem_cat', $problem_cat)
				->with('solution_cat', $solution_cat)
				->with('sector_data', $sector_data)
				->with('sector_list', $sector_list)
				->with('psa_document', $psa_document)
				->with('sitio_data', $sitio_data)
				->with($data);;
			
		}else{
			return $this->view('reports.login')
			->with('title', 'Login');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'startdate'   => 'required',
			'enddate'   => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('psa/'.$id.'/edit')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			
				$data = Input::all();
				
				unset($data["_token"]);
				unset($data["_method"]);
				$data['is_draft'] = isset($data['is_draft']) ? 1 : 0;



				$lgu_activity = array('activity_id' => $id, 'startdate' => $data["startdate"], 'enddate' => $data["enddate"]);
				// Report::update_ncddp('KC_LGUActivity', $lgu_activity);
				LguActivity::where('activity_id',$id)->update([ 'startdate' => $this->setDate($data["startdate"]), 'enddate' => $this->setDate($data["enddate"]) ]);
				
				unset($data["startdate"]);
				unset($data["enddate"]);
				
				$data['activity_id'] = $id;
				if(array_key_exists("sector", $data)){
					$sector_data = $data["sector"];
					unset($data["sector"]);
					Report::delete_ncddp('KC_SectorsRep', $id);
					foreach($sector_data as $sector){
						Report::save_ncddp('KC_SectorsRep', array('activity_id'=>$id, 'sector'=>$sector));
					}
				}
				else{
					Report::delete_ncddp('KC_SectorsRep', $id);
				}
				
				if(array_key_exists("document_name", $data)){
					$psa_document = $data["document_name"];
					
					$psa_document_date = $data["date"];
					unset($data["document_name"]);
					unset($data["date"]);
					Report::delete_ncddp('KC_PSADocuments', $id);
					$i = 0;
					foreach($psa_document as $document){
						Report::save_ncddp('KC_PSADocuments', array( 'activity_id'=>$id, 'document_name'=>$document, 'date'=>$this->setDate($psa_document_date[$i])));
						$i++;
					}
				}
				else{
					Report::delete_ncddp('KC_PSADocuments', $id);
				}
				
				if(array_key_exists("rank", $data)){
					$rank = $data["rank"];
					$problem = $data["problem"];
					$problem_cat = $data["problem_cat"];
					$solution = $data["solution"];
					$solution_cat = $data["solution_cat"];
					unset($data["rank"]);
					unset($data["problem"]);
					unset($data["problem_cat"]);
					unset($data["solution"]);
					unset($data["solution_cat"]);
					Report::delete_ncddp('KC_PSAProject', $id);
					$i = 0;
					foreach($rank as $r){
						$project_id = $id+$i; 
							$if_exist = PSAProjects::where('project_id',$id)->first();
							if($if_exist){
								// Report::save_ncddp('KC_PSAProject', array('project_id' => $project_id,'activity_id'=>$id, 'rank'=>$r, 'problem'=>$problem[$i], 'problem_cat'=>$problem_cat[$i], 'solution'=>$solution[$i], 'solution_cat'=>$solution_cat[$i]));
								PSAProjects::create( array('project_id' => $project_id,'activity_id'=>$id, 'rank'=>$r, 'problem'=>$problem[$i], 'problem_cat'=>$problem_cat[$i], 'solution'=>$solution[$i], 'solution_cat'=>$solution_cat[$i]));
								
							}else{

								$project_id = $id.'-'. $project_id;
								PSAProjects::create( array('project_id' => $project_id,'activity_id'=>$id, 'rank'=>$r, 'problem'=>$problem[$i], 'problem_cat'=>$problem_cat[$i], 'solution'=>$solution[$i], 'solution_cat'=>$solution_cat[$i]));
							}
							
						$i++;
					}
				}
				else{
					Report::delete_ncddp('KC_PSAProject', $id);
				}
				
				
				if($data["sitio"] != NULL){
					$sitio_data = $data["sitio"];
					unset($data["sitio"]);
					Report::delete_ncddp('KC_SitioRep', $id);
					foreach($sitio_data as $sitio){
						if($sitio != NULL)
							Report::save_ncddp('KC_SitioRep', array('activity_id'=>$id, 'sitio'=>$sitio));
					}
				}

            /* Populate PSA analysis table */
            $psa = PSAnalysis::where('activity_id',$id)->first();
            
            $training = Trainings::where('psgc_id',$psa->psgc_id)
                ->where('cycle_id',$psa->cycle_id)
                ->where('program_id',$psa->program_id)
                ->where('kc_mode',Session::get('accelerated'))
                ->first();
            $data['no_atnmale'] = $training->total_participants_by_genders($gender = 'M');
            $data['no_atnfemale'] = $training->total_participants_by_genders($gender = 'F');
            $data['no_ipmale'] =  isset($training) ? $training->total_ip('M') : 0;
            $data['no_ipfemale'] = isset($training) ? $training->total_ip('F') : 0;
            $data['no_oldmale'] = isset($training) ? $training->age_participants( 'M', 60) : 0;
            $data['no_oldfemale'] = isset($training) ? $training->age_participants( 'F', 60) : 0;
            /* end */

            Report::update_ncddp('KC_PSAnalysis', $data);
				
				
				Session::flash('message', 'PSA Successfully updated');
				return Redirect::to('psa/'.$id);
				
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Report::delete_ncddp('KC_SectorsRep', $id);
		Report::delete_ncddp('KC_SitioRep', $id);
		Report::delete_ncddp('KC_PSAProject', $id);
		Report::delete_ncddp('KC_PSADocuments', $id);
		Report::delete_ncddp('KC_PSAnalysis', $id);
		Report::delete_ncddp('KC_LGUActivity', $id);
		Report::delete_ncddp('KC_ActivityIssues', $id);

		// redirect
		Session::flash('message', 'PSA Successfully deleted!');
		return Redirect::to('psa');
	}


	/**
	 * SET the is_draft to given resource
	 *
	 * @param  string  $id
	 * @return Response
	 */
	public function setFinal($id)
	{
		DB::table('kc_psanalysis')->where('activity_id', $id)->update(array('is_draft' => 0));
		
        return Response::json(['status'=>'ok']);
	}


	public function pincos()
	{
		session_start(); //hello
		//return Redirect::intended('reports');
		if( isset($_REQUEST['logout'])) {
			Auth::logout();
			Session::flush();
		}

		if (Auth::check()){
			$user = Session::get('username');
			$data = Input::all();
			$activity_id = $data['activity_id'];
			if(array_key_exists("raise", $data)){
				$raise_by = $data['raise'];
				$issue = $data['issue'];
				$action = $data['action'];
				Report::delete_ncddp('KC_ActivityIssues', $activity_id);
				$i = 0;
				foreach($raise_by as $r){
					DB::table('KC_ActivityIssues')->insert(array(
						'activity_id' => $activity_id,
						'issue_no' => $i,
						'raise_by' => $r,
						'issue' => $issue[$i],
						'action' => $action[$i])
					);
					$i++;
				}
			}else{
					Report::delete_ncddp('KC_ActivityIssues', $activity_id);
			}
			Session::flash('message', 'Successfully updated.');
				return Redirect::back();
		}else{
			$username = Input::get('username');
			$password =Input::get('password');
			if (Auth::attempt(array('username' => $username, 'password' => $password))){
				Session::put('username', $username);
				return Redirect::to('');
			}else{
				if($username != NULL){
					return Redirect::to('');
				}else{
					return $this->view('reports.login')
					->with('title', 'Login');
				}
			}
		}
	}
    public function approved()
    {
        $input = Input::all();
        try{
            foreach($input['check'] as $ids)
            {
                LguActivity::where('activity_id',$ids)->update(['ac_approved'=>date('Y-m-d')]);
            }
            return json_encode(['status'=>'ok']);
        }catch (Exception $e){
            return json_encode(['status'=>'error']);
        }

    }


	public function getCommunityTraining()
	{

		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);
        $mode = Session::get('accelerated');
		  $psa = \PSAnalysis::all();
        $psgc_id = [];
        $cycles = [];
        $programs = [];
        foreach ($psa as $psas) {
            $psgc_id[] = $psas->psgc_id;
            $cycles[] = $psas->cycle_id;
            $programs[] = $psas->program_id;
        }
        	

        $train = Trainings::leftJoin('kc_barangay','kc_barangay.barangay_psgc','=','kc_trainings.psgc_id')
        		->where('kc_barangay.muni_psgc',Auth::user()->psgc_id)
        		->whereIn('kc_trainings.training_cat',['BPSA'])->where('kc_trainings.kc_mode',$mode)->get();
        // $train = \Trainings::where('training_cat','BPSA')->where('kc_mode',$mode)->get();
      
		// $train = $this->reference->getCommunityTrainingForPSA();
		// dd($train);
		return Datatable::collection($train)
		        ->showColumns('training_title','cycle_id','program_id')
		         ->addColumn('psgc_id',function($training){
		         
		         	return $training->getbarangay->barangay;
		         })
		         ->addColumn('Date Conducted',function($training){
		         	return toDate($training->start_date);
		         })
		         ->addColumn('option', function($trainings) {
		         	  // $vol = $volunteer->volunteer!=null ? $volunteer->volunteer->psgc_id : '';
		         	  $ifPSA = \PSAnalysis::where(['program_id'=>$trainings->program_id,'cycle_id'=>$trainings->cycle_id,'psgc_id'=>$trainings->psgc_id])->exists();
				      
		         	  if($ifPSA){
		         	  	return 'added';
		         	  }
				      return  '<a class="btn btn-success btn" onclick="$(this).use_psa($(this));">Use
				                       <div class="hidden">
												<div class="program_id">'.$trainings->program_id .'</div>
												<div class="cycle_id">'.$trainings->cycle_id .'</div>
												<div class="psgc_id">'. $trainings->psgc_id .'</div>
												<div class="start_date">'. $trainings->start_date .'</div>
												<div class="end_date">'. $trainings->end_date .'</div>

											 </div>
				                            </a>';

				    })
		        ->searchColumns('firstname')
		        ->orderColumns('lastname', 'firstname','birthdate','middlename')
		        ->make();
	}
	
}