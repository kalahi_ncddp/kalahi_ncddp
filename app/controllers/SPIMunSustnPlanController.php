<?php

class SPIMunSustnPlanController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user           = Session::get('username');
		$psgc_id        = Report::get_muni_psgc($user);
		$municipality   = Report::get_municipality($psgc_id);
		$province       = Report::get_province($psgc_id);
		$program        = Report::get_current_program($psgc_id);
		$cycle          = Report::get_current_cycle($psgc_id);
		$plans          = SPIMuncplSustnbltyPlan::all();
		$mode 			= Session::get('accelerated');
		$store_status   = Session::pull('spi-munsusplan.store', null);
		$update_status  = Session::pull('spi-munsusplan.update', null);
		$destroy_status = Session::pull('spi-munsusplan.destroy', null);

		return $this->view('spimunsusplan.index')
			->with('title', 'Municipal Sustainability Plan')
			->with('username', $user)
			->with('municipality', $municipality)
			->with('municipality_psgc', $psgc_id)
			->with('province', $province)
			->with('program', $program)
			->with('cycle', $cycle)
			->with('plans', $plans)
			->with('store_status', $store_status)
			->with('update_status', $update_status)
			->with('destroy_status', $destroy_status);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$is_successful     = FALSE;
		$id                = NULL;
		$program_id        = Input::get('in-programid',   false);
		$cycle_id          = Input::get('in-cycleid',     false);
	    $municipality_psgc = Input::get('in-municipalitypsgc',     false);  // this is dependent on lgu_covered
		$date_created   = Input::get('in-datecreated',  false); // P - Provincial; M - Municipal 
		$remarks        = Input::get('in-remarks',    false);

		if(!($program_id  === false || $cycle_id  === false || $municipality_psgc === false ||
		     $date_created === false || $remarks === false))
		{
			$MunSusPlan = new SPIMuncplSustnbltyPlan;
			$MunSusPlan->program_id        = $program_id;
			$MunSusPlan->cycle_id          = $cycle_id;
			$MunSusPlan->municipality_psgc = $municipality_psgc;
			$MunSusPlan->date_created      = date("Y-m-d", strtotime($date_created));
			$MunSusPlan->remarks           = $remarks;
			$MunSusPlan->save();
			$id = $MunSusPlan->id;
			$is_successful = ($id !== NULL) ? TRUE : FALSE;
		}
			
		Session::put('spi-munsusplan.store', $is_successful);		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$is_successful = FALSE;
		$date_created  = Input::get('edit-datecreated', false);
		$remarks       = Input::get('edit-remarks',     false);

		if(!($date_created === false || $remarks === false ))
		{
			$MunSusPlan = SPIMuncplSustnbltyPlan::find($id);
			$MunSusPlan->date_created      = date("Y-m-d", strtotime($date_created));
			$MunSusPlan->remarks           = $remarks;
			$MunSusPlan->save();
			$is_successful = TRUE;
		}

		Session::put('spi-munsusplan.update', $is_successful);		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$affectedRows = SPIMuncplSustnbltyPlan::find($id)->delete();
		$is_successful = ($affectedRows > 0) ? TRUE : FALSE;
		Session::put('spi-munsusplan.destroy', $is_successful);
	}
}
