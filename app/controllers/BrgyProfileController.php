<?php

class BrgyProfileController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Session::get('username');
		$mode = Session::get('accelerated');

		$data = BrgyProfile::get_brgy_profiles($user, $mode);
		// dd($data);
		return $this->view('brgyprofile.index')
			->with('title', 'Barangay Profile')
			->with('username', $user)
			->with('data', $data);


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
			$user = Session::get('username');
			$office = Report::get_office($user);
			$psgc_id = Report::get_muni_psgc($user);
			
			if($office === 'ACT'){
				$barangay_id = Report::get_psgc_id($psgc_id);

			}
			else if($office === 'RPMO' or $office === 'SRPMO'){
				$barangay_id = Report::get_prov_psgc_id($pscg_id);
			}
			else{
				$barangay_id = Report::get_all_brgy_psgc_id();
			}
			$barangay_name = NCDDP::get_barangay_name($barangay_id);
			$barangay_list = array_combine($barangay_id, $barangay_name);
			$barangay_list = array(NULL => '') + $barangay_list;
		
				$cycle = Report::get_current_cycle($psgc_id);
				// dd($cycle);
			$program = Report::get_current_program($psgc_id);
			$municipality = Report::get_municipality($psgc_id);
			$province = Report::get_province($psgc_id);
			
			$cycle_list = NCDDP::get_cycles();
			$cycle_list = array_combine($cycle_list, $cycle_list);
			
			$program_list = NCDDP::get_programs();
			$program_list = array_combine($program_list, $program_list);

			return $this->view('brgyprofile.create')
			->with('title', 'Barangay Profile')
			->with('brgy_list', $barangay_list)
			->with('municipality', $municipality)
			->with('province', $province)
			->with('cycle', $cycle)
			->with('cycle_list', $cycle_list)
			->with('program', $program)
			->with('program_list', $program_list)
			->with('username', $user);
			
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

			$brgy_profile = BrgyProfile::get_brgy_profile_existing(Input::get('psgc_id'), Input::get('program_id'), Input::get('cycle_id'),Session::get('accelerated'));
			if($brgy_profile == NULL){
				$data = Input::all();

				unset($data["_token"]);
				unset($data["barangay"]);
				unset($data["kc_code"]);
				unset($data["municipality"]);
				unset($data["province"]);
				
				$brgy_profile = BrgyProfile::get_brgy_profile_by_psgc(Input::get('psgc_id'));


				$newsquence = $this->getNewCurSequence();
				$profile_id = 'BP'.Input::get('psgc_id').$newsquence;
				
				$data['profile_id'] = $profile_id;
				$data['barangay_psgc'] = $data['psgc_id'];
				$data['kc_mode']	    = Session::get('accelerated');

				unset($data['psgc_id']);
			
				BrgyProfile::insert($data);
				
				$rf_facilities = BrgyProfile::get_rf_facilities();
				
				foreach($rf_facilities as $facility){
					BrgyProfile::save_brgyprofile_other_data('kc_brgyfacility', array('profile_id'=>$profile_id, 'facility_name'=>$facility->name));
				}
				return Redirect::to('brgyprofile/'.$profile_id.'/edit');

            }
			else{
				$error = 'Barangay Profile Existing. <a href = "'.URL::to('brgyprofile/'.$brgy_profile).'">'.$brgy_profile.'</a>';
				Session::flash('message', $error);
				return Redirect::to('brgyprofile/create')
				->withInput(Input::all());	
			}			
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		   $user = Session::get('username');
			$brgy_profile = BrgyProfile::find($id);
			$brgy_ipprof = BrgyProfile::get_brgy_profile_other_data('kc_brgyipprof', $id);
			$brgy_techresource = BrgyProfile::get_brgy_profile_other_data('kc_brgytechresource', $id);
			$brgy_envcrit = BrgyProfile::get_brgy_profile_other_data('kc_brgyenvcrit', $id);
			$brgy_orgs = BrgyProfile::get_brgy_profile_other_data('kc_brgyorgs', $id);
			$brgy_transpo = BrgyProfile::get_brgy_profile_other_data('kc_brgytranspo', $id);
			$brgy_income = BrgyProfile::get_brgy_profile_other_data('kc_brgyincome', $id);
			$brgy_crop = BrgyProfile::get_brgy_profile_other_data('kc_brgycrop', $id);
			$brgy_facility = DB::table("kc_brgyfacility")->where('profile_id', $id)->groupBy('facility_name')->get();
			
			$brgy_fundsource = BrgyProfile::get_brgy_profile_other_data('kc_brgyfundsource', $id);
			$brgy_landtenure = BrgyProfile::get_brgy_profile_other_data('kc_brgylandstatus', $id);
			$brgy_dev_projs = BrgyProfile::get_brgy_profile_other_data('kc_brgydevproj', $id);
			$brgy_bdcp = BrgyProfile::get_brgy_profile_other_data('kc_brgybdcp', $id);
			$orgtype_list = BrgyProfile::get_types();
			$brgy_gadfund = BrgyProfile::get_brgy_profile_other_data('kc_brgygadfund', $id);
			
			$data['description'] = $id;
		
			return $this->view('brgyprofile.show')
				->with('title', 'Barangay Profile')
				->with('username', $user)
				->with('brgy_profile', $brgy_profile)
				->with('brgy_envcrit', $brgy_envcrit)
				->with('brgy_income', $brgy_income)
				->with('brgy_crop', $brgy_crop)
				->with('brgy_transpo', $brgy_transpo)
				->with('brgy_fundsource', $brgy_fundsource)
				->with('brgy_techresource', $brgy_techresource)
				->with('brgy_facility', $brgy_facility)
				->with('brgy_landtenure', $brgy_landtenure)
				->with('brgy_ipprof', $brgy_ipprof)
				->with('brgy_orgs', $brgy_orgs)
				->with('brgy_dev_projs', $brgy_dev_projs)
				->with('orgtype_list', $orgtype_list)
				->with('brgy_gadfund', $brgy_gadfund)
				->with($data);
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
			$user = Session::get('username');
			$brgy_profile = BrgyProfile::find($id);
			$brgy_ipprof = BrgyProfile::get_brgy_profile_other_data('kc_brgyipprof', $id);
			$brgy_techresource = BrgyProfile::get_brgy_profile_other_data('kc_brgytechresource', $id);
			$brgy_envcrit = BrgyProfile::get_brgy_profile_other_data('kc_brgyenvcrit', $id);
			$brgy_orgs = BrgyProfile::get_brgy_profile_other_data('kc_brgyorgs', $id);
			$brgy_transpo = BrgyProfile::get_brgy_profile_other_data('kc_brgytranspo', $id);
			$brgy_income = BrgyProfile::get_brgy_profile_other_data('kc_brgyincome', $id);
			$brgy_crop = BrgyProfile::get_brgy_profile_other_data('kc_brgycrop', $id);
			$brgy_facility = DB::table("kc_brgyfacility")->where('profile_id', $id)->groupBy('facility_name')->get();
			$brgy_fundsource = BrgyProfile::get_brgy_profile_other_data('kc_brgyfundsource', $id);
			$brgy_landtenure = BrgyProfile::get_brgy_profile_other_data('kc_brgylandstatus', $id);
			$brgy_gadfund = BrgyProfile::get_brgy_profile_other_data('kc_brgygadfund', $id);
			
			$data['description'] = $id;
		
			return $this->view('brgyprofile.edit')
				->with('title', 'Barangay Profile')
				->with('username', $user)
				->with('brgy_profile', $brgy_profile)
				->with('brgy_envcrit', $brgy_envcrit)
				->with('brgy_income', $brgy_income)
				->with('brgy_crop', $brgy_crop)
				->with('brgy_transpo', $brgy_transpo)
				->with('brgy_fundsource', $brgy_fundsource)
				->with('brgy_techresource', $brgy_techresource)
				->with('brgy_facility', $brgy_facility)
				->with('brgy_landtenure', $brgy_landtenure)
				->with('brgy_ipprof', $brgy_ipprof)
				->with('brgy_orgs', $brgy_orgs)
				->with('brgy_gadfund', $brgy_gadfund)
				->with($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$data = Input::all();
		
		$data['is_draft'] = isset($data['is_draft']) ? 1 : 0; 
		$data['profile_id'] = $id;
		$data['baragay_additiondetails'] = isset($data['is_brgyaffected']) ? $data['baragay_additiondetails'] : '';
		$data['is_brgyaffected'] = isset($data['is_brgyaffected']) ? 1 : 0;
		
		$data['is_poblacion'] = $data['is_poblacion']=='1' ? 1 : 0;

        unset($data["_token"]);
		unset($data["_method"]);
		BrgyProfile::delete_brgyprofile_other_data('kc_brgyenvcrit', $id);
		if(array_key_exists("location", $data)){
			$location_data = $data["location"];
			$description = $data["description"];
			// $effects = $data["effects"];
			unset($data["location"]);
			unset($data["description"]);
			// unset($data["effects"]);
			$i = 0;
			foreach($location_data as $location){
				BrgyProfile::save_brgyprofile_other_data('kc_brgyenvcrit', array('profile_id'=>$id, 'location'=>$location, 'description'=>$description[$i]));
				$i++;
			}
		}
		
		BrgyProfile::delete_brgyprofile_other_data('kc_brgyipprof', $id);
		if(array_key_exists("ip_group", $data)){
			$ip_group_data = $data["ip_group"];
			$ip_location = $data["ip_location"];
			$ip_no_malehdhh = $data["ip_no_malehdhh"];
			$ip_no_femalehdhh = $data["ip_no_femalehdhh"];
			$ip_no_male = $data["ip_no_male"];
			$ip_no_female = $data["ip_no_female"];
			unset($data["ip_group"]);
			unset($data["ip_location"]);
			unset($data["ip_no_malehdhh"]);
			unset($data["ip_no_femalehdhh"]);
			unset($data["ip_no_male"]);
			unset($data["ip_no_female"]);
			$i = 0;
			foreach($ip_group_data as $ip_group){
				BrgyProfile::save_brgyprofile_other_data('kc_brgyipprof', array('profile_id'=>$id, 'ip_group'=>$ip_group, 'ip_location'=>$ip_location[$i], 'ip_no_malehdhh'=>$ip_no_malehdhh[$i], 'ip_no_femalehdhh'=>$ip_no_femalehdhh[$i], 'ip_no_male'=>$ip_no_male[$i], 'ip_no_female'=>$ip_no_female[$i] ));
				$i++;
			}
		}
		
		BrgyProfile::delete_brgyprofile_other_data('kc_brgyfundsource', $id);
		if(array_key_exists("fund_source", $data)){
			$fund_source_data = $data["fund_source"];
			$amount = $data["amount"];
			unset($data["fund_source"]);
			unset($data["amount"]);
			
			$i = 0;
			foreach($fund_source_data as $fund_source){
				$current_cost = str_replace( ',', '', $amount[$i] );
				BrgyProfile::save_brgyprofile_other_data('kc_brgyfundsource', array('profile_id'=>$id, 'fund_source'=>$fund_source, 'amount'=>$current_cost ));
				$i++;
			}
		}

		BrgyProfile::delete_brgyprofile_other_data('kc_brgygadfund', $id);
		if(array_key_exists("activity", $data)){
			$activity_data = $data["activity"];
			$cost = $data["cost"];
			unset($data["activity"]);
			unset($data["cost"]);
			
			$i = 0;
			foreach($activity_data as $gadfund){
				$current_cost = str_replace( ',', '', $cost[$i] );
				BrgyProfile::save_brgyprofile_other_data('kc_brgygadfund', array('profile_id'=>$id, 'activity'=>$gadfund, 'cost'=>$current_cost ));
				$i++;
			}
		}

		BrgyProfile::delete_brgyprofile_other_data('kc_brgytechresource', $id);
		if(array_key_exists("equipt_type", $data)){
			$equipt_type_data = $data["equipt_type"];
			$curr_condition = $data["curr_condition"];
			$curr_capability = $data["curr_capability"];
			$consumption = $data["consumption"];
			$rental_rate = $data["rental_rate"];
			unset($data["equipt_type"]);
			unset($data["curr_condition"]);
			unset($data["curr_capability"]);
			unset($data["consumption"]);
			unset($data["rental_rate"]);
			$i = 0;
			foreach($equipt_type_data as $equipt_type){
				BrgyProfile::save_brgyprofile_other_data('kc_brgytechresource', array('profile_id'=>$id, 'equipt_type'=>$equipt_type, 'curr_condition'=>$curr_condition[$i], 'curr_capability'=>$curr_capability[$i], 'consumption'=>$consumption[$i], 'rental_rate'=>$rental_rate[$i]));
				$i++;
			}
		}
		
		BrgyProfile::delete_brgyprofile_other_data('kc_brgyincome', $id);
		if(array_key_exists("activity", $data)){
			$activity_data = $data["activity"];
			$avg_income = $data["avg_income"];
			$act_hdinvolved = $data["act_hdinvolved"];
			$seasonality = $data["seasonality"];
			unset($data["activity"]);
			unset($data["avg_income"]);
			unset($data["act_hdinvolved"]);
			unset($data["seasonality"]);
			$i = 0;
			foreach($activity_data as $activity){
				BrgyProfile::save_brgyprofile_other_data('kc_brgyincome', array('profile_id'=>$id, 'activity'=>$activity, 'avg_income'=>$avg_income[$i], 'act_hdinvolved'=>$act_hdinvolved[$i], 'seasonality'=>$seasonality[$i]));
				$i++;
			}
		}
		
		BrgyProfile::delete_brgyprofile_other_data('kc_brgycrop', $id);
		if(array_key_exists("majorcrop", $data)){
			$majorcrop_data = $data["majorcrop"];
			$production = $data["production"];
			$crop_hdinvolved = $data["crop_hdinvolved"];
			unset($data["majorcrop"]);
			unset($data["production"]);
			unset($data["crop_hdinvolved"]);
			$i = 0;
			foreach($majorcrop_data as $majorcrop){
				BrgyProfile::save_brgyprofile_other_data('kc_brgycrop', array('profile_id'=>$id, 'majorcrop'=>$majorcrop, 'production'=>$production[$i], 'crop_hdinvolved'=>$crop_hdinvolved[$i]));
				$i++;
			}
		}
		BrgyProfile::delete_brgyprofile_other_data('kc_brgytranspo', $id);
		if(array_key_exists("mode", $data)){
			$mode_data = $data["mode"];
			$cost = $data["cost"];
			$schedule = $data["schedule"];
			unset($data["mode"]);
			unset($data["cost"]);
			unset($data["schedule"]);
			$i = 0;
			foreach($mode_data as $mode){
				BrgyProfile::save_brgyprofile_other_data('kc_brgytranspo', array('profile_id'=>$id, 'mode'=>$mode, 'cost'=>$cost[$i], 'schedule'=>$schedule[$i]));
				$i++;
			}
		}
		
		BrgyProfile::delete_brgyprofile_other_data('kc_brgylandstatus', $id);
		if(array_key_exists("tenure_status", $data)){
			$tenure_status_data = $data["tenure_status"];
			$tenure_no_households = $data["tenure_no_households"];
			$tenure_no_hhfemalehead = $data["tenure_no_hhfemalehead"];
			$tenure_no_hhiphead = $data["tenure_no_hhiphead"];
			unset($data["tenure_status"]);
			unset($data["tenure_no_households"]);
			unset($data["tenure_no_hhfemalehead"]);
			unset($data["tenure_no_hhiphead"]);
			$i = 0;
			foreach($tenure_status_data as $tenure_status){
				BrgyProfile::save_brgyprofile_other_data('kc_brgylandstatus', array('profile_id'=>$id, 'tenure_status'=>$tenure_status, 'tenure_no_households'=>$tenure_no_households[$i], 'tenure_no_hhfemalehead'=>$tenure_no_hhfemalehead[$i], 'tenure_no_hhiphead'=>$tenure_no_hhiphead[$i]));
				$i++;
			}
		}
		BrgyProfile::delete_brgyprofile_other_data('kc_brgyfacility', $id);
		if(array_key_exists("facility_name", $data)){
			$facility_name_data = $data["facility_name"];
			$facility_distance = $data["facility_distance"];
			$facility_transpo = $data["facility_transpo"];
			// $facility_fare = $data["facility_fare"];
			$facility_avalailable = array();
			if(array_key_exists("facility_available", $data)){
				$facility_avalailable = $data["facility_available"];
				unset($data["facility_available"]);
			}
			unset($data["facility_name"]);
			unset($data["facility_distance"]);
			unset($data["facility_transpo"]);
			// unset($data["facility_fare"]);
			$i = 0;
			foreach($facility_name_data as $facility_name){
					$facility_name_lowered = strtolower($facility_name);

				if(in_array($facility_name, $facility_avalailable)){

					BrgyProfile::save_brgyprofile_other_data('kc_brgyfacility', array('profile_id'=>$id, 'facility_name'=>$facility_name, 'facility_available'=>1));
				}
				else{
					BrgyProfile::save_brgyprofile_other_data('kc_brgyfacility', array('profile_id'=>$id, 'facility_name'=>$facility_name, 'facility_available'=>0, 'facility_distance'=>$facility_distance[$facility_name_lowered], 'facility_transpo'=>$facility_transpo[$facility_name_lowered]));
				}
				$i++;
			}
		}

		// BrgyProfile::delete_brgyprofile_other_data('kc_brgyorgs', $id);
		// if(array_key_exists("org_id", $data)){
		// 	$org_data = $data["org_id"];
		// 	$org_name = $data["org_name"];
		// 	$org_type = $data["org_type"];
		// 	$is_registered = $data["is_registered"];
		// 	$is_lguaccredited = $data["is_lguaccredited"];
		// 	$advocacy = $data["advocacy"];
		// 	$area_operation = $data["area_operation"];
		// 	$years_operation = $data["years_operation"];
		// 	$active_inactive_org = $data["years_operation"];
		// 	$activities = $data["activities"];
		// 	$total_male_members_brgy = $data["total_male_members_brgy"];
		// 	$total_female_members_brgy = $data["total_female_members_brgy"];
		// 	$male_ip_members = $data["male_ip_members"];
		// 	$female_ip_members = $data["female_ip_members"];
		// 	$sector_rep = $data["sector_rep"];
		// 	// $activities = $data["activities"];

		// 	// $ip_no_malehdhh = $data["ip_no_malehdhh"];
		// 	// $ip_no_femalehdhh = $data["ip_no_femalehdhh"];
		// 	// $ip_no_male = $data["ip_no_male"];
		// 	// $ip_no_female = $data["ip_no_female"];
		// 	unset($data["org_id"]);
		// 	unset($data["org_name"]);
		// 	unset($data["org_type"]);
		// 	unset($data["is_registered"]);
		// 	unset($data["is_lguaccredited"]);
		// 	unset($data["advocacy"]);
		// 	unset($data["area_operation"]);
		// 	unset($data["years_operation"]);
		// 	unset($data["activities"]);
		// 	unset($data["total_male_members_brgy"]);
		// 	unset($data["total_female_members_brgy"]);
		// 	unset($data["male_ip_members"]);
		// 	unset($data["female_ip_members"]);
		// 	unset($data["sector_rep"]);
		// 	$i = 0;
		// 	foreach($org_data as $org_org){
		// 		BrgyProfile::save_brgyprofile_other_data('kc_brgyorgs', 
		// 			array('profile_id'=>$id, 
		// 				'org_id'=>$org_org, 
		// 				'org_name'=>$ip_location[$i], 
		// 				'org_type'=>$org_type[$i], 
		// 				'is_registered'=>$is_registered[$i], 
		// 				'is_lguaccredited'=>$is_lguaccredited[$i], 
		// 				'advocacy'=>$advocacy[$i],
		// 				'area_operation'=>$area_operation[$i],
		// 				'years_operation'=>$years_operation[$i], 
		// 				'activities'=>$activities[$i],
		// 				'total_male_members_brgy'=>$total_male_members_brgy[$i],
		// 				'total_female_members_brgy'=>$total_female_members_brgy[$i],
		// 				'male_ip_members'=>$male_ip_members[$i],
		// 				'female_ip_members'=>$female_ip_members[$i],
		// 				'sector_rep'=>$sector_rep[$i]




		// 				));
		// 		$i++;
		// 	}
		// }
		
		//var_dump($data);
		
		//BrgyProfile::save_brgyprofile_other_data('kc_brgyprofile', $data);
		// unset($data['facility_ava'])
		
		BrgyProfile::where('profile_id', $data['profile_id'])
		->update($data);
		
		
		Session::flash('message', 'Baragay Profile Successfully updated');
		return Redirect::to('brgyprofile/'.$id);		
	}

	public function show_bdcmeetings($id)
	{
		if (Auth::check()){
		   	$user = Session::get('username');
			$data = BrgyProfile::get_brgy_profile_other_data('kc_brgybdcmeet', $id);
			
			//$data['id'] = $id;
		
			return $this->view('brgyprofile.show_bdcmeetings')
				->with('title', 'BA/BDC Meetings')
				->with('username', $user)
				->with('id', $id)
				->with('data', $data);
		}else{
			return $this->view('reports.login')
			->with('title', 'Login');
		}
	}

	public function create_meeting($id){
		if (Auth::check()){
			$user = Session::get('username');

			return $this->view('brgyprofile.create_meeting')
			->with('title', 'BA/BDC Meetings')
			->with('username', $user)
			->with('id', $id);
			
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function submit_meeting(){
		if (Auth::check()){
			$user = Session::get('username');
			$rules = array(
				// 'year_source'   => 'required|unique:kc_brgybdcmeet,year_source',
				// 'no_baconducted'   => 'required',
				// 'no_avghousehold' => 'required',
				// 'no_avgmale' => 'required',
				// 'no_avgfemale' => 'required',
				// 'no_iphouseholds' => 'required',
				// 'sectors_rep' => 'required',
				// 'no_bdcmeet' => 'required',
				// 'no_bdcmale' => 'required',
				// 'no_bdcfemale' => 'required',
				// 'no_bdcmalerep' => 'required',
				// 'no_bdcfemalerep' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);
			$data = Input::all();

			if ($validator->fails()) {
				return Redirect::to('brgyprofile/'.Input::get('id').'/create_meeting')
					->withErrors($validator)
					->withInput(Input::all());
			} else{
				$meetings = BrgyProfile::get_bdcmeet_id(Input::get('id'));
				if($meetings != NULL){
					$meeting_id = substr($meetings, 0, -4);
					$number = substr($meetings, -4);
					$meeting_id = $meeting_id.sprintf("%04d", $number+1);
				}
				else{
					$meeting_id = 'MEET-'.Input::get('id').'-0001';

				}
				$meeting = array('profile_id' => Input::get('id'),
					'meeting_id' => $meeting_id,
					'year_source' => Input::get('year_source'),
					'no_baconducted' => Input::get('no_baconducted'),
					'no_avghousehold' => Input::get('no_avghousehold'),
					'no_avgmale' => Input::get('no_avgmale'),
					'no_avgfemale' => Input::get('no_avgfemale'),
					'no_iphouseholds' => Input::get('no_iphouseholds'),
					'sectors_rep' => Input::get('sectors_rep'),
					'no_bdcmeet' => Input::get('no_bdcmeet'),
					'no_bdcmale' => Input::get('no_bdcmale'),
					'no_bdcfemale' => Input::get('no_bdcfemale'),
					'no_bdcmalerep' => Input::get('no_bdcmalerep'),
					'no_bdcfemalerep' => Input::get('no_bdcfemalerep'),
					'bas_conducted' => Input::get('bas_conducted'),
				);

				BrgyProfile::save_brgyprofile_other_data('kc_brgybdcmeet', $meeting);

				return Redirect::to('brgyprofile/'.Input::get('id').'/show_bdcmeetings')
					->with('message', 'BA/BDC meeting record added.');
					
			}
		}
	}

	public function show_meeting($id){
		if (Auth::check()){
			$user = Session::get('username');
			$meeting = BrgyProfile::get_meeting($id);

			return $this->view('brgyprofile.show_meeting')
				->with('title', 'BA/BDC Meetings')
				->with('id', $id)
				->with('meeting', $meeting)
				->with('username', $user);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function edit_meeting($id)
	{
		if (Auth::check()){
			$user = Session::get('username');
			$meeting = BrgyProfile::get_meeting($id);

			return $this->view('brgyprofile.edit_meeting')
				->with('title', 'BA/BDC Meetings')
				->with('id', $id)
				->with('username', $user)
				->with('meeting', $meeting);
			
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function update_meeting()
	{
		$id = Input::get('meeting_id');
		if(Input::get('old_year') != Input::get('year_source')){
			$rules = array(
				// 'year_source'   => 'required|unique:kc_brgybdcmeet,year_source',
				// 'no_baconducted'   => 'required',
				// 'no_avghousehold' => 'required',
				// 'no_avgmale' => 'required',
				// 'no_avgfemale' => 'required',
				// 'no_iphouseholds' => 'required',
				// 'sectors_rep' => 'required',
				// 'no_bdcmeet' => 'required',
				// 'no_bdcmale' => 'required',
				// 'no_bdcfemale' => 'required',
				// 'no_bdcmalerep' => 'required',
				// 'no_bdcfemalerep' => 'required'
			);
		}else{
			$rules = array(
				// 'year_source'   => 'required',
				// 'no_baconducted'   => 'required',
				// 'no_avghousehold' => 'required',
				// 'no_avgmale' => 'required',
				// 'no_avgfemale' => 'required',
				// 'no_iphouseholds' => 'required',
				// 'sectors_rep' => 'required',
				// 'no_bdcmeet' => 'required',
				// 'no_bdcmale' => 'required',
				// 'no_bdcfemale' => 'required',
				// 'no_bdcmalerep' => 'required',
				// 'no_bdcfemalerep' => 'required'
			);
		}

		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('brgyprofile/'.$id.'/edit_meeting')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			
				$data = Input::all();

				unset($data["old_year"]);
				unset($data["_token"]);
				unset($data["_method"]);
				
				DB::table('kc_brgybdcmeet')
           			->where('meeting_id', $id)
            		->update($data);
				
				Session::flash('message', 'Meeting record successfully updated');
				return Redirect::to('brgyprofile/'.$id.'/show_meeting');
				
		}
	}


	public function delete_meeting($id)
	{

		// BrgyProfile::delete_brgyprofile_other_data('kc_brgyorgs', $id);
		$reference = Brgybdcmeet::where('meeting_id',$id)->first()->profile_id;
		/* DELETE */
		Brgybdcmeet::where('meeting_id',$id)->delete();
		Session::flash('message','You have Successfully Deleted BA/BDC Meeting');
		return Redirect::to('brgyprofile/'.$reference.'/show_bdcmeetings');
	}


	public function show_devprojs($id)
	{
		if (Auth::check()){
		   	$user = Session::get('username');
			$data = BrgyProfile::get_brgy_profile_other_data('kc_brgydevproj', $id);
			
			//$data['id'] = $id;
		
			return $this->view('brgyprofile.show_devprojs')
				->with('title', 'Barangay Development Projects')
				->with('username', $user)
				->with('id', $id)
				->with('data', $data);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}


	public function delete_devproj($id)
	{

		// BrgyProfile::delete_brgyprofile_other_data('kc_brgyorgs', $id);
		$reference = Brgydevproj::where('devproj_id',$id)->first()->profile_id;
		/* DELETE */
		Brgydevproj::where('devproj_id',$id)->delete();
		Session::flash('message','You have Successfully Deleted Brgy Development Projects ');
		return Redirect::to('brgyprofile/'.$reference.'/show_devprojs');
	}

	public function create_devproj($id){
		if (Auth::check()){
			$user = Session::get('username');

			return $this->view('brgyprofile.create_devproj')
				->with('title', 'Add Brgy. Dev. Project')
				->with('username', $user)
				->with('id', $id);
		}else{
			return View::make('reports.login')
				->with('title', 'Login');
		}
	}

	public function submit_devproj(){
		if (Auth::check()){
			$user = Session::get('username');
			$rules = array(
				// 'proj_name'   => 'required',
				// 'proj_nature'   => 'required',
				// 'proj_duration' => 'required',
				// 'location' => 'required',
				// 'scope_target' => 'required',
				// 'proj_cost' => 'required',
				// 'fund_source' => 'required',
				// 'impl_agency' => 'required',
				// 'cost_sharing' => 'required',
				// 'no_benemale' => 'required',
				// 'no_benefemale' => 'required',
				// 'no_pphousehold' => 'required',
				// 'no_slphousehold' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);
			$data = Input::all();

			if ($validator->fails()) {
				return Redirect::to('brgyprofile/'.Input::get('id').'/create_devproj')
					->withErrors($validator)
					->withInput(Input::all());
			} else{
				$devprojs = BrgyProfile::get_devproj_id(Input::get('id'));
				if($devprojs != NULL){
					$devproj_id = substr($devprojs, 0, -4);
					$number = substr($devprojs, -4);
					$devproj_id = $devproj_id.sprintf("%04d", $number+1);
				}
				else{
					$devproj_id = 'DP-'.Input::get('id').'-0001';

				}
				$devproj = array('profile_id' => Input::get('id'),
					'devproj_id' => $devproj_id,
					'proj_name' => Input::get('proj_name'),
					'proj_nature' => Input::get('proj_nature'),
					'proj_duration' => Input::get('proj_duration'),
					'location' => Input::get('location'),
					'scope_target' => Input::get('scope_target'),
					'proj_cost' => Input::get('proj_cost'),
					'fund_source' => Input::get('fund_source'),
					'impl_agency' => Input::get('impl_agency'),
					'cost_sharing' => Input::get('cost_sharing'),
					'no_benemale' => Input::get('no_benemale'),
					'no_benefemale' => Input::get('no_benefemale'),
					'no_pphousehold' => Input::get('no_pphousehold'),
					'no_slphousehold' => Input::get('no_slphousehold'),
					'poverty_one' => Input::get('poverty_one'),
					'poverty_two' => Input::get('poverty_two'),
					'poverty_three' => Input::get('poverty_three'),
					'crops_one' => Input::get('crops_one'),
					'crops_two' => Input::get('crops_two'),
					'crops_three' => Input::get('crops_three'),
					'avg_annualhh' => Input::get('avg_annualhh'),
					'avgannualincome_males' => Input::get('avgannualincome_males'),
					'avgannualincome_females' => Input::get('avgannualincome_females'),
					'avgannualincome_ips' => Input::get('avgannualincome_ips'),
					'dirt' => Input::get('dirt'),
					'gravel' => Input::get('gravel'),
					'asphalted' => Input::get('asphalted'),
					'cemented' => Input::get('cemented'),
					'transpo_one' => Input::get('transpo_one'),
					'transpo_two' => Input::get('transpo_two'),
					'transpo_three' => Input::get('transpo_three')
				);

				BrgyProfile::save_brgyprofile_other_data('kc_brgydevproj', $devproj);

				return Redirect::to('brgyprofile/'.Input::get('id').'/show_devprojs')
					->with('message', 'Brgy. development project successfully added.');
					
			}
		}
	}

	public function show_devproj($id){
		if (Auth::check()){
			$user = Session::get('username');
			$devproj = BrgyProfile::get_devproj($id);

			return $this->view('brgyprofile.show_devproj')
				->with('title', 'Barangay Development Projects')
				->with('id', $id)
				->with('devproj', $devproj)
				->with('username', $user);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function edit_devproj($id)
	{
		if (Auth::check()){
			$user = Session::get('username');
			$devproj = BrgyProfile::get_devproj($id);

			return $this->view('brgyprofile.edit_devproj')
				->with('title', 'Barangay Development Projects')
				->with('id', $id)
				->with('username', $user)
				->with('devproj', $devproj);
			
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function update_devproj()
	{
		$id = Input::get('devproj_id');
		$rules = array(
			// 'proj_name'   => 'required',
			// 'proj_nature'   => 'required',
			// 'proj_duration' => 'required',
			// 'location' => 'required',
			// 'scope_target' => 'required',
			// 'proj_cost' => 'required',
			// 'fund_source' => 'required',
			// 'impl_agency' => 'required',
			// 'cost_sharing' => 'required',
			// 'no_benemale' => 'required',
			// 'no_benefemale' => 'required',
			// 'no_pphousehold' => 'required',
			// 'no_slphousehold' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('brgyprofile/'.$id.'/edit_devproj')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			
				$data = Input::all();

				unset($data["_token"]);
				unset($data["_method"]);
				
				//$data['person_id'] = $id;
				
				DB::table('kc_brgydevproj')
           			->where('devproj_id', $id)
            		->update($data);
				
				Session::flash('message', 'Brgy. development project successfully updated');
				return Redirect::to('brgyprofile/'.$id.'/show_devproj');
				
		}
	}

	public function show_orgs($id)
	{
		if (Auth::check()){
		   	$user = Session::get('username');
			$data = BrgyProfile::get_brgy_profile_other_data('kc_brgyorgs', $id);
			
			//$data['id'] = $id;
			return $this->view('brgyprofile.show_orgs')
				->with('title', 'Barangay Organization')
				->with('username', $user)
				->with('id', $id)
				->with('data', $data);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function create_org($id){
		if (Auth::check()){
			$user = Session::get('username');
			$orgtype_list = BrgyProfile::get_types();

			return $this->view('brgyprofile.create_org')
				->with('title', 'Barangay Organization ')
				->with('username', $user)
				->with('id', $id)
				->with('orgtype_list', $orgtype_list);
		}else{
			return View::make('reports.login')
				->with('title', 'Login');
		}
	}

	public function submit_org(){
		if (Auth::check()){
			$user = Session::get('username');
			$rules = array(
				// 'org_name'   => 'required',
				// // 'org_type'   => 'required',
				// 'is_registered' => 'required',
				// 'is_lguaccredited' => 'required',
				// 'advocacy' => 'required',
				// 'area_operation' => 'required',
				// 'no_memmale' => 'required',
				// 'no_memfemale' => 'required',
				// 'no_ipmale' => 'required',
				// 'no_ipfemale' => 'required',
				// 'no_ofcmale' => 'required',
				// 'no_ofcfemale' => 'required',
				// 'sector_rep' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);
			$data = Input::all();

			if ($validator->fails()) {
				return Redirect::to('brgyprofile/'.Input::get('id').'/create_org')
					->withErrors($validator)
					->withInput(Input::all());
			} else{
				$orgs = BrgyProfile::get_org_id(Input::get('id'));
				if($orgs != NULL){
					$org_id = substr($orgs, 0, -4);
					$number = substr($orgs, -4);
					$org_id = $org_id.sprintf("%04d", $number+1);
				}
				else{
					$org_id = 'ORG-'.Input::get('id').'-0001';

				}
				$org = array('profile_id' => Input::get('id'),
					'org_id' => $org_id,
					'org_name' => Input::get('org_name'),
					'org_type' => Input::get('org_type'),
					'is_registered' => Input::get('is_registered'),
					'is_lguaccredited' => Input::get('is_lguaccredited'),
					'advocacy' => Input::get('advocacy'),
					'area_operation' => Input::get('area_operation'),
					'years_operation' => Input::get('years_operation'),
					'active_inactive_org' => Input::get('active_inactive_org'),
					'activities' => Input::get('activities'),
					'total_male_members_brgy' => Input::get('total_male_members_brgy'),
					'total_female_members_brgy' => Input::get('total_female_members_brgy'),
					'male_ip_members' => Input::get('male_ip_members'),
					'female_ip_members' => Input::get('female_ip_members'),
					// 'no_memmale' => Input::get('no_memmale'),
					// 'no_memfemale' => Input::get('no_memfemale'),
					// 'no_ipmale' => Input::get('no_ipmale'),
					// 'no_ipfemale' => Input::get('no_ipfemale'),
					// 'no_ofcmale' => Input::get('no_ofcmale'),
					// 'no_ofcfemale' => Input::get('no_ofcfemale'),
					// 'sector_rep' => Input::get('sector_rep'),
				);

				BrgyProfile::save_brgyprofile_other_data('kc_brgyorgs', $org);

				return Redirect::to('brgyprofile/'.Input::get('id').'/show_orgs')
					->with('message', 'Organization successfully added.');
					
			}
		}
	}

	public function show_org($id){
		if (Auth::check()){
			$user = Session::get('username');
			$brgy_org = BrgyProfile::get_org($id);

			return $this->view('brgyprofile.show_org')
				->with('title', 'Brgy. Organization')
				->with('id', $id)
				->with('brgy_org', $brgy_org)
				->with('username', $user);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function edit_org($id)
	{
		if (Auth::check()){
			$user = Session::get('username');
			$brgy_org = BrgyProfile::get_org($id);
			$orgtype_list = BrgyProfile::get_types();

			return $this->view('brgyprofile.edit_org')
				->with('title', 'Organization')
				->with('id', $id)
				->with('username', $user)
				->with('brgy_org', $brgy_org)
				->with('orgtype_list', $orgtype_list);
			
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function update_org()
	{
		$id = Input::get('org_id');
		$rules = array(
			// 'org_name'   => 'required',
			// 'org_type'   => 'required',
			// 'is_registered' => 'required',
			// 'is_lguaccredited' => 'required',
			// 'advocacy' => 'required',
			// 'area_operation' => 'required',
			// 'sector_rep' => 'required',
			// 'years_operation' => 'required',
			// 'activities' => 'required',
			// 'total_male_members_brgy' => 'required',
			// 'total_female_members_brgy' => 'required',
			// 'male_ip_members' => 'required',
			// 'female_ip_members' => 'required'
			// 'no_memmale' => 'required',
			// 'no_memfemale' => 'required',
			// 'no_ipmale' => 'required',
			// 'no_ipfemale' => 'required',
			// 'no_ofcmale' => 'required',
			// 'no_ofcfemale' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('brgyprofile/'.$id.'/edit_org')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			
				$data = Input::all();

				unset($data["_token"]);
				unset($data["_method"]);
				
				// Trainings::delete_training('kc_trainor', $id);
					// BrgyProfile::delete_brgyprofile_other_data('kc_brgyorgs', $id);
				//$data['person_id'] = $id;
				
				DB::table('kc_brgyorgs')
           			->where('org_id', $id)
            		->update($data);
				
				Session::flash('message', 'Organization successfully updated');
				return Redirect::to('brgyprofile/'.$id.'/show_org');
				
		}
	}

	public function delete_org($id)
	{

		// BrgyProfile::delete_brgyprofile_other_data('kc_brgyorgs', $id);
		$reference = Brgyorgs::where('org_id',$id)->first()->profile_id;
		/* DELETE */
		Brgyorgs::where('org_id',$id)->delete();
		Session::flash('message','You have Successfully Deleted Brgy Organization Profile');
		return Redirect::to('brgyprofile/'.$reference.'/show_orgs');
	}

	
	public function show_bdcp($id)
	{
		if (Auth::check()){
		   	$user = Session::get('username');
			$data = BrgyProfile::get_brgy_profile_other_data('kc_brgybdcp', $id);
			
			//$data['id'] = $id;
		
			return $this->view('brgyprofile.show_bdcp')
				->with('title', 'Barangay Profile')
				->with('username', $user)
				->with('id', $id)
				->with('data', $data);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function create_person($id){
		if (Auth::check()){
			$user = Session::get('username');
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
			$status_list = NCDDP::get_civilstatus();
			$group_list = NCDDP::get_ipgroup();
			$level_list = NCDDP::get_educlevel();
			$position_list = NCDDP::get_brgy_rep();
			
			return $this->view('brgyprofile.create_person')
				->with('title', 'Add BDCP')
				->with('username', $user)
				->with('id', $id)
				->with('sector_list', $sector_list)
				->with('status_list', $status_list)
				->with('group_list', $group_list)
				->with('level_list', $level_list)
				->with('position_list', $position_list);
		}else{
			return View::make('reports.login')
				->with('title', 'Login');
		}
	}

	public function submit_person(){
		if (Auth::check()){
			$user = Session::get('username');
			$rules = array(
			);

			$validator = Validator::make(Input::all(), $rules);
			$data = Input::all();
			$data['ip_group']	= isset( $data['ip_group'] ) ? 1 : 0;
			$data['is_ipgroup'] = isset( $data['is_ipgroup'] ) ? 1 : 0; 

			if ($validator->fails()) {
				return Redirect::to('brgyprofile/'.Input::get('id').'/create_person')
					->withErrors($validator)
					->withInput(Input::all());
			} else{
				  $blgu = Brgylgu::where('last_name',$data['last_name'])
				                        ->where('first_name',$data['first_name'])
				                        ->where('middle_initial',$data['middle_name'])
				                        ->where('sex',$data['sex'])
				                        ->exists();
		            if(!$blgu){

						$persons = BrgyProfile::get_bdcp_id(Input::get('id'));
						if($persons != NULL){
							$person_id = substr($persons, 0, -4);
							$number = substr($persons, -4);
							$person_id = $person_id.sprintf("%04d", $number+1);
						}
						else{
							$person_id = 'PER-'.Input::get('id').'-0001';

						}

		                /* set sectors rep*/
		                if(array_key_exists("sector_rep", $data)){

		                    $sector_represented = "";
		                    foreach($data["sector_rep"] as $sector){
		                        $sector_represented = $sector_represented.','.$sector;
		                    }
		                    $data["sector_rep"] = ltrim ($sector_represented, ',');

		                }
		                /* end set*/
						$person = array('profile_id' => Input::get('id'),
							'person_id' => $person_id,
							'last_name' => Input::get('last_name'),
							'first_name' => Input::get('first_name'),
							'middle_name' => Input::get('middle_name'),
							'sex' => Input::get('sex'),
							'age' => Input::get('age'),
							'civil_status' => Input::get('civil_status'),
							'ip_group' => Input::get('ip_group'),
							'educ_attainment' => Input::get('educ_attainment'),
							'position' => Input::get('position'),
							'startdate' =>  $this->setDate($data["startdate"]),
							'enddate' => $this->setDate($data["enddate"]),
							'org_nonkc' => Input::get('org_nonkc'),
							'kc_comm' => Input::get('kc_comm'),
							'sector_rep' => !empty($data["sector_rep"]) ?$data["sector_rep"] : "" ,
							'is_ipgroup' => Input::get('is_ipgroup',''),
							'kc_ncddp_com' => Input::get('kc_ncddp_com')
						);

						BrgyProfile::save_brgyprofile_other_data('kc_brgybdcp', $person);

						return Redirect::to('brgyprofile/'.Input::get('id').'/show_bdcp')
							->with('message', 'Person profile added.');
							
					} else {
						return Redirect::to('brgyprofile/'.Input::get('id').'/create_person')
											->withErrors('There is an existing data named '.Input::get('last_name').' '.Input::get('first_name').' '.Input::get('middle_name'). ' in BLGU')
											->withInput(Input::all());
					}
				}
		}
	}

	public function show_person($id){
		if (Auth::check()){
			$user = Session::get('username');
			$person = BrgyProfile::get_person($id);

			return $this->view('brgyprofile.show_person')
				->with('title', 'BDC Profile')
				->with('id', $id)
				->with('person', $person)
				->with('username', $user);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function delete_bdcp($id)
	{
		$input = Input::all();
		// dd($input);

		// BrgyProfile::delete_brgyprofile_other_data('kc_brgyorgs', $id);
		$reference = Brgybdcp::where('last_name',$input['last_name'])->where('first_name', $input['first_name'])->where('middle_name', $input['middle_name'])->first()->profile_id;
		/* DELETE */
		Brgybdcp::where('last_name',$input['last_name'])->delete();
		Brgybdcp::where('first_name',$input['first_name'])->delete();
		Brgybdcp::where('middle_name',$input['middle_name'])->delete();
		Session::flash('message','You have Successfully Deleted BDC Profile');
		return Redirect::to('brgyprofile/'.$reference.'/show_bdcp');
	}

	public function edit_person($id)
	{
		if (Auth::check()){
			$user = Session::get('username');
			$person = BrgyProfile::get_person($id);
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
			$status_list = NCDDP::get_civilstatus();
			$group_list = NCDDP::get_ipgroup();
			$level_list = NCDDP::get_educlevel();
			$position_list = NCDDP::get_brgy_rep();

            $person->sector_rep =  explode(",", $person->sector_rep);


			return $this->view('brgyprofile.edit_person')
				->with('title', 'BDC Profile')
				->with('id', $id)
				->with('username', $user)
				->with('person', $person)
				->with('sector_list', $sector_list)
				->with('status_list', $status_list)
				->with('group_list', $group_list)
				->with('level_list', $level_list)
				->with('position_list', $position_list);
			
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function update_person()
	{
		$id = Input::get('person_id');
		$rules = array(
			// 'last_name'   => 'required',
			// 'first_name' => 'required',
			// 'middle_name' => 'required',
			// 'sex'   => 'required',
			// 'age' => 'required',
			// 'civil_status' => 'required',
			// 'ip_group' => 'required',
			// 'educ_attainment' => 'required',
			// 'position' => 'required',
			// 'startdate' => 'required',
			// 'enddate' => 'required',
			// 'org_nonkc' => 'required',
			// 'kc_comm' => 'required',
			// 'sector_rep' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('brgyprofile/'.$id.'/edit_person')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			
				$data = Input::all();
				$data["startdate"] = $this->setDate($data["startdate"]);
				$data["enddate"] = $this->setDate($data["enddate"]);
				$data['ip_group']	= isset($data['ip_group']) ? $data['ip_group'] : '';
				$data['is_ipgroup'] = isset($data['is_ipgroup']) ? 1 : 0; 
				unset($data["_token"]);
				unset($data["_method"]);
                /* set sectors rep*/
                if(array_key_exists("sector_rep", $data)){

                    $sector_represented = "";
                    foreach($data["sector_rep"] as $sector){
                        $sector_represented = $sector_represented.','.$sector;
                    }
                    $data["sector_rep"] = ltrim ($sector_represented, ',');

                }
                /* end set*/
				$data['person_id'] = $id;
				
				DB::table('kc_brgybdcp')
           			->where('person_id', $id)
            		->update($data);
				
				Session::flash('message', 'Person profile successfully updated');
				return Redirect::to('brgyprofile/'.$id.'/show_person');
				
		}
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		BrgyProfile::delete_brgyprofile_other_data('kc_brgyipprof', $id);
		BrgyProfile::delete_brgyprofile_other_data('kc_brgytechresource', $id);
		BrgyProfile::delete_brgyprofile_other_data('kc_brgyenvcrit', $id);
		BrgyProfile::delete_brgyprofile_other_data('kc_brgyorgs', $id);
		BrgyProfile::delete_brgyprofile_other_data('kc_brgytranspo', $id);
		BrgyProfile::delete_brgyprofile_other_data('kc_brgyincome', $id);
		BrgyProfile::delete_brgyprofile_other_data('kc_brgycrop', $id);
		BrgyProfile::delete_brgyprofile_other_data('kc_brgyfacility', $id);
		BrgyProfile::delete_brgyprofile_other_data('kc_brgyfundsource', $id);
		BrgyProfile::delete_brgyprofile_other_data('kc_brgylandstatus', $id);
		
		BrgyProfile::where('profile_id',$id)->delete();
		
		// redirect
		Session::flash('message', 'Barangay Profile Successfully deleted!');
		return Redirect::to('brgyprofile');
	}








// BLGU


public function show_blgu($id)
	{
		if (Auth::check()){
		   	$user = Session::get('username');
			$data = BrgyProfile::get_brgy_profile_other_data('kc_blgu', $id);
			
			//$data['id'] = $id;
		
			return $this->view('brgyprofile.show_blgu')
				->with('title', 'BLGU Officials Profile')
				->with('username', $user)
				->with('id', $id)
				->with('data', $data);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function create_person_blgu($id){
		if (Auth::check()){
			$user = Session::get('username');
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
			$status_list = NCDDP::get_civilstatus();
			$group_list = NCDDP::get_ipgroup();
			$level_list = NCDDP::get_educlevel();
			$position_list = NCDDP::get_brgy_rep();
			
			return $this->view('brgyprofile.create_person_blgu')
				->with('title', 'Add BLGU Official')
				->with('username', $user)
				->with('id', $id)
				->with('sector_list', $sector_list)
				->with('status_list', $status_list)
				->with('group_list', $group_list)
				->with('level_list', $level_list)
				->with('position_list', $position_list);
		}else{
			return View::make('reports.login')
				->with('title', 'Login');
		}
	}


	public function submit_person_blgu(){
		if (Auth::check()){
			$user = Session::get('username');
			$rules = array(
				// 'last_name'   => 'required',
				// 'first_name' => 'required',
				// 'middle_name' => 'required',
				// 'sex'   => 'required',
				// 'age' => 'required',
				// 'civil_status' => 'required',
				// 'ip_group' => 'required',
				// 'educ_attainment' => 'required',
				// 'position' => 'required',
				// 'startdate' => 'required',
				// 'enddate' => 'required',
				// 'org_nonkc' => 'required',
				// 'kc_comm' => 'required',
				// 'sector_rep' => 'required'
			);

			$validator = Validator::make(Input::all(), $rules);
			$data = Input::all();
			$data['ipgroup']	= isset($data['isipgroup']) ? $data['ipgroup'] : '';
			$data['is_ipgroup'] = isset($data['is_ipgroup']) ? 1 : 0; 

			if ($validator->fails()) {
				return Redirect::to('brgyprofile/'.Input::get('id').'/create_person_blgu')
					->withErrors($validator)
					->withInput(Input::all());
			} else{
                $blguexists = Brgylgu::where('last_name',$data['last_name'])
                                        ->where('first_name',$data['first_name'])
                                        ->where('middle_initial',$data['middle_initial'])
                                         ->where('sex',$data['sex'])
                                        ->exists();
                if(!$blguexists)
                {
                    $bdcpexists = Brgybdcp::where('last_name',$data['last_name'])
                        ->where('first_name',$data['first_name'])
                        ->where('middle_name',$data['middle_initial'])
                        ->where('sex',$data['sex'])
                        ->exists();
                    if(!$bdcpexists){

                        $persons = BrgyProfile::get_blgu_id(Input::get('id'));
                        if($persons != NULL){
                            $person_id = substr($persons, 0, -4);
                            $number = substr($persons, -4);
                            $person_id = $person_id.sprintf("%04d", $number+1);
                        }
                        else{
                            $person_id = 'PER-'.Input::get('id').'-0001';

                        }
                        $person = array('profile_id' => Input::get('id'),
                            'person_id' => $person_id,
                            'last_name' => Input::get('last_name'),
                            'first_name' => Input::get('first_name'),
                            'middle_initial' => Input::get('middle_initial'),
                            'sex' => Input::get('sex'),
                            'age' => Input::get('age'),
                            'civilstatus' => Input::get('civilstatus'),
                            'is_ipgroup' => Input::get('is_ipgroup'),
                            'educ_attainment' => Input::get('educ_attainment'),
                            'position' => Input::get('position'),
                            'startdate' =>  $this->setDate($data["startdate"]),
                            'enddate' => $this->setDate($data["enddate"]),
                            'non_kc_org' => Input::get('non_kc_org'),
                            'kc_ncddp_com' => Input::get('kc_ncddp_com'),
                            'ipgroup' => Input::get('ipgroup')
                            // 'sector_rep' => Input::get('sector_rep'),
                            // 'is_ipgroup' => Input::get('is_ipgroup'),
                            // 'kc_ncddp_com' => Input::get('kc_ncddp_com')
                        );

                        BrgyProfile::save_brgyprofile_other_data('kc_blgu', $person);

                        return Redirect::to('brgyprofile/'.Input::get('id').'/show_blgu')
                            ->with('message', 'Person profile added.');
                    }
                    else
                    {
                        return Redirect::to('brgyprofile/'.Input::get('id').'/create_person_blgu')
                            ->withErrors('There is an existing data in bdc.')->withInput(Input::all());;
                    }


                }
                else
                {
                    return Redirect::to('brgyprofile/'.Input::get('id').'/create_person_blgu')
                        ->withErrors('There is an existing data.')->withInput(Input::all());;
                }

			}
		}
	}

	public function show_person_blgu($id){
		if (Auth::check()){
			$user = Session::get('username');
			$person = BrgyProfile::get_person_blgu($id);
			// dd($person);
			return $this->view('brgyprofile.show_person_blgu')
				->with('title', 'BLGU Officials Profile')
				->with('id', $id)
				->with('person', $person)
				->with('username', $user);
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function edit_person_blgu($id)
	{
		if (Auth::check()){
			$user = Session::get('username');
			$person = BrgyProfile::get_person_blgu($id);
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
			$status_list = NCDDP::get_civilstatus();
			$group_list = NCDDP::get_ipgroup();
			$level_list = NCDDP::get_educlevel();
			$position_list = NCDDP::get_brgy_rep();


			return $this->view('brgyprofile.edit_person_blgu')
				->with('title', 'BLGU Officials Profile')
				->with('id', $id)
				->with('username', $user)
				->with('person', $person)
				->with('sector_list', $sector_list)
				->with('status_list', $status_list)
				->with('group_list', $group_list)
				->with('level_list', $level_list)
				->with('position_list', $position_list);
			
		}else{
			return View::make('reports.login')
			->with('title', 'Login');
		}
	}

	public function update_person_blgu()
	{
		$id = Input::get('person_id');
		$data = Input::all();
		$rules = array(
			// 'last_name'   => 'required',
			// 'first_name' => 'required',
			// 'middle_name' => 'required',
			// 'sex'   => 'required',
			// 'age' => 'required',
			// 'civil_status' => 'required',
			// 'ip_group' => 'required',
			// 'educ_attainment' => 'required',
			// 'position' => 'required',
			// 'startdate' => 'required',
			// 'enddate' => 'required',
			// 'org_nonkc' => 'required',
			// 'kc_comm' => 'required',
			// 'sector_rep' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('brgyprofile/'.$id.'/edit_person_blgu')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			
				$data = Input::all();
				$data["startdate"] = $this->setDate($data["startdate"]);
				$data["enddate"] = $this->setDate($data["enddate"]);
				$data['ipgroup']	= isset($data['is_ipgroup']) ? $data['ipgroup'] : '';
				$data['is_ipgroup'] = isset($data['is_ipgroup']) ? 1 : 0; 
				unset($data["_token"]);
				unset($data["_method"]);
				
				$data['person_id'] = $id;
				
				DB::table('kc_blgu')
           			->where('person_id', $id)
            		->update($data);
				
				Session::flash('message', 'Person profile successfully updated');
				return Redirect::to('brgyprofile/'.$id.'/show_person_blgu');
				
		}
	}

	public function delete_blgu($id)
	{
		$input = Input::all();
		// dd($input);

		// BrgyProfile::delete_brgyprofile_other_data('kc_brgyorgs', $id);
		$reference = Brgylgu::where('last_name',$input['last_name'])->where('first_name', $input['first_name'])->where('middle_initial', $input['middle_initial'])->first()->profile_id;
		/* DELETE */
		Brgylgu::where('last_name',$input['last_name'])->delete();
		Brgylgu::where('first_name',$input['first_name'])->delete();
		Brgylgu::where('middle_initial',$input['middle_initial'])->delete();
		Session::flash('message','You have Successfully Deleted Brgy Development Council Profile');
		return Redirect::to('brgyprofile/'.$reference.'/show_blgu');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function destroy_blgu($id)
	// {
		
	// 	BrgyProfile::delete_brgyprofile_other_data('kc_brgyipprof', $id);
	// 	BrgyProfile::delete_brgyprofile_other_data('kc_brgytechresource', $id);
	// 	BrgyProfile::delete_brgyprofile_other_data('kc_brgyenvcrit', $id);
	// 	BrgyProfile::delete_brgyprofile_other_data('kc_brgyorgs', $id);
	// 	BrgyProfile::delete_brgyprofile_other_data('kc_brgytranspo', $id);
	// 	BrgyProfile::delete_brgyprofile_other_data('kc_brgyincome', $id);
	// 	BrgyProfile::delete_brgyprofile_other_data('kc_brgycrop', $id);
	// 	BrgyProfile::delete_brgyprofile_other_data('kc_brgyfacility', $id);
	// 	BrgyProfile::delete_brgyprofile_other_data('kc_brgyfundsource', $id);
	// 	BrgyProfile::delete_brgyprofile_other_data('kc_brgylandstatus', $id);
		
	// 	BrgyProfile::where('profile_id',$id)->delete();
		
	// 	// redirect
	// 	Session::flash('message', 'Barangay Profile Successfully deleted!');
	// 	return Redirect::to('brgyprofile');
	// }
}




