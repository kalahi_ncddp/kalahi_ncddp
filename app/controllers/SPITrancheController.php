<?php

class SPITrancheController extends \BaseController {
	public function show($id) {
		$user = Session::get('username');
		$tranche = SPITranche::where(array('project_id' => $id))->first();

		if (isset($tranche)) {
			$profile = SPIProfile::where(array('project_id' => $id))->first();
			$proposal = ProjProposal::where(array('project_id' => $profile->mibf_refno))->first();
			$utilization = SPUtilization::where(array('project_id' => $id))->get();
			return $this->view('spi_tranche.show')
				->with('username', $user)
				->with('title', 'Sub Project Tranche')
				->with('description', $tranche->project_id)
				->with('tranche', $tranche)
				->with('profile', $profile)
				->with('utilization', $utilization)
				->with('proposal', $proposal);
		}

		return Redirect::route('spi_profile.tranche.create', $id);
	}

	public function create($id) {
		$user = Session::get('username');

		$profile = SPIProfile::where(array('project_id' => $id))->first();

		if(isset($profile->mibf_refno)){

			$proposal = ProjProposal::where(array('project_id' => $profile->mibf_refno))->first();
		}else{
			$proposal = [];
		}

		$tranche = new SPITranche(array(
			'project_id'						=> $id
		));

		return $this->view('spi_tranche.create')
			->with('username', $user)
			->with('title', 'Sub Project Tranche')
			->with('tranche', $tranche)
			->with('profile', $profile)
			->with('proposal', $proposal)
			->with('description', $tranche->project_id);
	}

	public function store() {
		$user = Session::get('username');
		$data = Input::all();
		unset($data["_token"]);

		// store completion report
		$data['release_date_1'] = $this->setDate($data["release_date_1"]);
		$data['release_date_2'] = $this->setDate($data["release_date_2"]);
		$data['release_date_3'] = $this->setDate($data["release_date_3"]);
		SPITranche::create($data);

		Session::flash('success', "SP Tranche successfully created");
		return Redirect::route('spi_profile.tranche.show', $data['project_id']);
	}

	public function edit($id) {
		$user = Session::get('username');
		$tranche = SPITranche::where(array('project_id' => $id))->first();
		$profile = SPIProfile::where(array('project_id' => $id))->first();
		$proposal = ProjProposal::where(array('project_id' => $profile->mibf_refno))->first();

		return $this->view('spi_tranche.edit')
			->with('username', $user)
			->with('title', 'Sub Project Completion Report')
			->with('description', $tranche->project_id)
			->with('tranche', $tranche)
			->with('profile', $profile)
			->with('proposal', $proposal);
	}

	public function update($id) {
		$user = Session::get('username');
		$tranche = SPITranche::where(array('project_id' => $id))->first();

		$data = Input::all();
		unset($data["_token"]);

		// store completion report 
		$data['release_date_1'] = $this->setDate($data["release_date_1"]);
		$data['release_date_2'] = $this->setDate($data["release_date_2"]);
		$data['release_date_3'] = $this->setDate($data["release_date_3"]);

		$tranche->fill($data);
		$tranche->save();

		Session::flash('success', "SP Tranche successfully updated");
		return Redirect::route('spi_profile.tranche.show', $id);
	}
}