<?php

class SPISbPrjWrkrsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);

		// reference to  Repositories/SPI/SPIInterface
		// mode is particular if CEAC is Regular or Accelerated
		$data['sprojectprofiles'] = $this->spi->getSubProjectProfile($user);
		
		return $this->view('spisubworkers.index')
			->with('title', 'CDD Sub-Project ERS')
			->with('username', $user)
			->with($data);
	}


	public function genid()
	{
		$spi_worker = new SPIWorker;
		return $spi_worker->generateNewId();
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());

		$id             = Request::segment(2);;
		$create_status  = Session::pull("spi-subworkers.$id.create", null);
		$monolog->addInfo('Log Message', array("spi-subworkers.$id.update"));
		$destroy_status = Session::pull("spi-subworkers.$id.destroy", null);
		$spi_profile    = SPIProfile::find($id);
		$monolog->addInfo('SPI PROFILE', array('yo' => $spi_profile));
		$workers        = $spi_profile->workers;
		$monolog->addInfo('SPI PROFILE : PROJECT WORKERS', array($workers));
		$user           = Session::get('username');
		$muni_psgc_id   = Report::get_muni_psgc($user);
		$all_workers    = SPIWorker::allFromMunicipality($muni_psgc_id)->get();
		$monolog->addInfo('SPI PROFILE : ALL WORKERS', array($all_workers));
		$user        = Session::get('username');
		return $this->view('spisubworkers.show')
			->with('title', 'CDD Sub-Project Workers & ERS')
			->with('username', $user)
			->with('create_status', $create_status)
			->with('destroy_status', $destroy_status)
			->with('project_id', $id)
			->with('spi_profile_name', $spi_profile->project_name)
			->with('all_workers', $all_workers)
			->with('workers', $workers);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		/*
		$is_successful = FALSE;
		$id            = NULL;
		$project_id    = Request::segment(2);
	    $date_report   = Input::get('in-datereport', false); 
		$date_start    = Input::get('in-datestart',  false); 
		$date_ending   = Input::get('in-dateending', false);
		
		if(!($project_id  === false || $date_report === false ||
		     $date_start === false || $date_ending === false))
		{
			$ers_list = new SPIERSList;
			$ers_list->project_id     = $project_id;
			$ers_list->date_reporting = date("Y-m-d", strtotime($date_report));
			$ers_list->date_start     = date("Y-m-d", strtotime($date_start));
			$ers_list->date_ending    = date("Y-m-d", strtotime($date_ending));
			$ers_list->save();
			$id = $ers_list->record_id;	
			$is_successful = ($id !== NULL) ? TRUE : FALSE;
		}
		Session::put("spi-subworkers.$project_id.update", $is_successful);
//		*/
       // echo '<pre>';
       // dd(Input::all());
        $monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', array('Test sadasdasd'));

		$is_successful = FALSE;
		$project_id    = Input::get('in-projectid',    false);
		$existing_user = Input::get('in-dateoccured',  false);
	    $lastname      = Input::get('in-lastname',     false);
		$firstname     = Input::get('in-firstname',    false); 
		$middlename    = Input::get('in-middlename',   false);
		$sex           = Input::get('in-sex',          false);
		$age           = Input::get('in-age',          false);
		$birthday      = Input::get('in-birthday',     false);
		$naturework    = Input::get('in-natureofwork', false);
        $typeofwork    = Input::get('in-typeofwork', false);
        $ratehour      = Input::get('in-hourrate',     false);
		$rateday       = Input::get('in-dailyrate',    false);
		$is_pantawid   = Input::get('in-pantawid',   false);
		$is_seac       = Input::get('in-seac',   false);
        $is_slp       	= Input::get('in-slp',   false);
		$is_ip       	= Input::get('in-ip',   false);
        $skilled       = DB::table('rf_natureofwork')->where('nature_of_work',$naturework)->first()->type;
		if($existing_user !== false)
		{
			$worker_id     = Input::get('in-workerid',     false);
			$monolog->addInfo('Log Message', array('Some inputs are complete'));
			$spi_profile = SPIProfile::find($project_id);
			$spi_profile->workers()->attach($worker_id);
			
			$is_successful = TRUE;
		}
		else if(!($project_id  === false || $lastname === false || $firstname   === false || 
				  $middlename  === false || $sex      === false || $age         === false)
		)
		{
			$monolog->addInfo('Log Message', array('All inputs are complete'));
			
			$spi_profile = SPIProfile::find($project_id);
			$monolog->addInfo('Log Message', array($spi_profile));
			
			$spi_worker = new SPIWorker;
			$spi_worker->lastname    = $lastname;
			$spi_worker->firstname   = $firstname;
			$spi_worker->middlename  = $middlename;
			$spi_worker->sex         = $sex;
			$spi_worker->age         = $age;
			if( $birthday ){
				$spi_worker->birthday    =   date("Y-m-d", strtotime($birthday));
			}
			$spi_worker->nature_work = $naturework;
            $spi_worker->type_of_work  = $typeofwork;
			$spi_worker->rate_hour   = $ratehour;
			$spi_worker->rate_day    = $rateday;
			$spi_worker->is_pantawid = ($is_pantawid == 'on') ? 1 : 0;
			$spi_worker->is_seac     = ($is_seac == 'on')     ? 1 : 0;
			$spi_worker->is_ip     = ($is_ip == 'on')     ? 1 : 0;
            $spi_worker->is_slp     = ($is_slp == 'on')     ? 1 : 0;
			$spi_worker->created_by  = Session::get('username');
            $spi_worker->skilled = $skilled;
			//$spi_worker->save();
			//$spi_worker->push();
			SPIProfile::find($project_id)->workers()->save($spi_worker);

			/*
			$spi_ersrecord = new SPIERSRecord(array(
				'rate_hour'  => $ratehour,
				'rate_day'   => $rateday,
				'work_hours' => $workhour,
				'work_days'  => $workday
			));

			$spi_erslist = SPIERSList::find($record_id);
			$spi_erslist->records()->save($spi_ersrecord);
			$spi_ersrecord->worker()->save($spi_worker);			
			*/
			$is_successful = TRUE;
		}

		$monolog->addInfo('Log Message', array("spi-subworkers.$project_id.update"));
		Session::put("spi-subworkers.$project_id.create", $is_successful);		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', array("Soft Delete for $id"));	

		$worker_id     = Input::get('in-workerid', FALSE);
		$monolog->addInfo('Log Message', array("Soft Delete for $id " . $worker_id));	

		SPIProfile::find($id)->workers()->detach($worker_id);
		$is_successful = TRUE;

		Session::put("spi-subworkers.$id.destroy", $is_successful);
	}
    /**
     *  Contains the api for retriveing the type of work
     *  @return array
     * */
    public function apiTypeofwork()
    {
        $natureofwork = e(Input::get('type'));
        $data['work'] = DB::table('rf_natureofwork')->where('nature_of_work',$natureofwork)->first()->type;
        return Response::json($data);
    }

}
