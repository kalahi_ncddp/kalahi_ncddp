<?php

class CeacMonitoringController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['username'] = Session::get('username');
		$data['title']    = 'CEAC';


        $psgc = $this->user->getPsgcId($data['username']);
		$ceac = $this->reference->getCeac($psgc);
		$data['ceac'] = $ceac;
		$data['all_cycles'] = $this->reference->getCycles();
		$data['program'] = $this->reference->getPrograms();

		
		return $this->view('ceac.monitoring.index')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
        $mode = Session::get('accelerated');
        $user = User::where('username',Session::get('username'))->first();
        $exist = Ceac::where('psgc_id',$user->psgc_id)->where('cycle_id',$input['cycle_id'])->where('program_id',$input['program_id'])->where('kc_mode',$mode)->exists();
        if(!$exist)
        {
            $newsquence = $this->getNewCurSequence();
            $activity_id = 'CEAC'.$user->psgc_id.$newsquence;
            $input['psgc_id'] = $user->psgc_id;
            $input['kc_mode'] = $mode;
            $input['reference_no'] =$activity_id;
            Ceac::create($input);
            return Response::json(['result' => true ]);
        }
        else
        {
            return Response::json(['result' => false]);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @param  int year 
	 * @param  int cycle  
	 * @return Response
	 */
	public function show($cycle_id,$program,$id)
	{
        $data['username'] = Session::get('username');
        $data['title']	  = 'CEAC Monitoring';
        $mode = Session::get('accelerated');

        $data['reference_no'] = Ceac::where('program_id',$program)->where('cycle_id',$cycle_id)->first()->reference_no;


        $cycle = LguCycles::where('psgc_id',$id)->first();


        $barangay = Municipality::find($id);
        $data['barangays'] = $barangay->barangay;

        $data['activities'] = CeacActivties::where('is_muni_activity',1)
                                            ->where('is_regular','!=',$mode)
											->orderBy('activity_no')
                                            ->groupBy('activity_code')
											->get();


		$data['description'] = Municipality::find($id)->municipalities .' program year '.$cycle->year;
		$data['cycle']   = $cycle;
		$data['cycle_id'] = $cycle_id;
		$data['year'] = $cycle->year;
		$data['psgc_id'] = $id;
		$data['program'] = $program;
		return $this->view('ceac.monitoring.show')->with($data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$input = Input::all();


		// build input for planned;
		$planned['psgc_id'] = $input['psgc_id'];
		$planned['program_id'] = $input['program'];
		$planned['cycle_id'] = $input['cycle_id'];
        $planned['kc_mode'] = Session::get('accelerated');
        $planned['reference_no'] = $input['reference_no'];
       $planned['activity_code'] = $input['activity_code'];
		$planned['startdate'] = $this->setDate($input['plan_start']);
		$planned['enddate'] = $this->setDate($input['plan_end']);
		$planned['actual_start'] =  $this->setDate(Input::get('actual_start',''));
		$planned['actual_end']  =   $this->setDate(Input::get('actual_end',''));
			 // CeacMuni::firstOrNew($planned)->save();

        // $planned_to_compare = CeacMuni::where(['psgc_id'=>$input['psgc_id'],
        //                                 'cycle_id'=>$input['cycle_id'],
        //                                 'program_id'=>$input['program'],
        //                                 'kc_mode'=> $planned['kc_mode']])->orderBy('enddate', 'desc')->first();
        // if($planned['actual_start']!=null){
        //     $actual_to_compare = CeacMuni::where(['psgc_id'=>$input['psgc_id'],
        //         'cycle_id'=>$input['cycle_id'],
        //         'program_id'=>$input['program'],
        //         'kc_mode'=> $planned['kc_mode']])->orderBy('actual_end', 'desc')->first();

        //     if(!empty($actual_to_compare))
        //     {
        //         if( strtotime($actual_to_compare->actual_end) > strtotime($planned['actual_end']) )
        //         {
        //             $actual_plan = CeacMuni::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->exists();
        //             if(!$actual_plan){
        //                 return Response::json(['result' => 'error','message' => 'Actual Date must be higher to '. toDate($actual_to_compare->actual_end)]);
        //             }
        //         }
        //     }
        // } else {
        //     unset($planned['actual_start']);
        //     unset($planned['actual_end']);

        // }
//        dd($planned_to_compare->enddate);
        if(empty($planned_to_compare)){
            $plan = CeacMuni::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->exists();
            if($plan){
                CeacMuni::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])
                    ->update($planned);
            }else{
                CeacMuni::create($planned);
            }
            return Response::json(['result'=>'success']);
        }else {

            if(   strtotime($planned_to_compare->enddate)>strtotime($planned['startdate'])) {
                $plan = CeacMuni::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->exists();
                if(!$plan){
                    return Response::json(['result' => 'error', 'message' => 'Planned must be higher to ' . toDate($planned_to_compare->enddate)]);
                }else{
                    CeacMuni::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])
                        ->update($planned);
                    return Response::json(['result' => 'success', 'message' => 'Updated' . toDate($planned_to_compare->enddate)]);

                }
            }
            else
            {
                $plan = CeacMuni::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->exists();
                if($plan){
                    CeacMuni::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])
                        ->update($planned);
                }else{
                    CeacMuni::create($planned);
                }
                return Response::json(['result'=>'success']);
            }
        }
//        $activity = CeacMuni::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'])



		// $actual = lguActivity::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->exists();
		// if($plan){
		// 	CeacMuni::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->update($planned);
		// }else{
		// 	CeacMuni::create($planned);
		// }
		// update lguactivity


	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
//        dd($id);
        CeacMuni::where('reference_no',$id)->delete();
        CeacBrgy::where('reference_no',$id)->delete();

        Ceac::where('reference_no',$id)->delete();

        Session::flash('message','Successfully deleted ceac');
        return Redirect::to('ceac/monitoring');

	}


}
