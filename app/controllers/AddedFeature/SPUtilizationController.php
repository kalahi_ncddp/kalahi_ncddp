<?php namespace AddedFeature;
use Session;
use Input;
use Auth;
use Redirect;


class SPUtilizationController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{
		$user = Session::get('username');
		return $this->view('spi_tranche.spi_tranche_utilization.create')
			->with('username', $user)
			->with('subproject',$id)
			->with('title', 'Add Monthly Utilization Data');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{
		$input['project_id'] = $id;
		$input['as_of_date'] = $this->setDate(Input::get('as_of_date',''));
		$input['grant_utilization'] = Input::get('grant_utilization',0);
		$input['lcc_utilization'] =  Input::get('lcc_utilization',0);
		$result = \SPUtilization::createUtilization($input);
		if($result){
			Session::flash('message','Successfully added Utilization');
			return Redirect::to('spi_profile/'.$id.'/tranche');
		}else{
			return Redirect::back();
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($spi_id)
	{
		//
		$id = Input::get('id');

		$utilization = \SPUtilization::where('id',$id)->first();
		$user = Session::get('username');
		return $this->view('spi_tranche.spi_tranche_utilization.edit')
			->with('username', $user)
			->with('subproject',$spi_id)
			->with('utilization',$utilization)
			->with('title', 'Edit Monthly Utilization Data');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($project_id)
	{
		$id = Input::get('id');

		$input['as_of_date'] = $this->setDate(Input::get('as_of_date',''));
		$input['grant_utilization'] = Input::get('grant_utilization',0);
		$input['lcc_utilization'] =  Input::get('lcc_utilization',0);
		$result = \SPUtilization::where('id',$id)->update($input);
		if($result){
			Session::flash('message','Successfully Updated Utilization');
			return Redirect::to('spi_profile/'.$project_id.'/tranche');
		}else{
			return Redirect::back();
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($project_id)
	{
		//
		$id = Input::get('id');
		$result = \SPUtilization::where('id',$id)->delete();
		if($result){
			Session::flash('message','Successfully deleted a Utilization');
			return Redirect::to('spi_profile/'.$project_id.'/tranche');
		}else{
			return Redirect::back();
		}		
	}


}
