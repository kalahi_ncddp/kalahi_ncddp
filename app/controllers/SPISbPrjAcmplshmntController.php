<?php

class SPISbPrjAcmplshmntController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index()
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', array('Yo'));
		$search = Input::get('search','');
		$user         = Session::get('username');
		$muni_psgc    = $this->user->getPsgcId($user);
		$municipality = Report::get_municipality($muni_psgc);
		$projects     = SPIProfile::get_municipality_projects($muni_psgc,$search);

		$monolog->addInfo('Log Message', array($projects));

		return $this->view('spiaccomplishment.index')
			->with('title', 'Sub-Project Accomplishment Report')
			->with('username', $user)
			->with('municipality', $municipality)
			->with('projects', $projects);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Respons
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());

		$in_projectid    = Input::get('in-projectid',    false);
		$in_itemnum      = Input::get('in-itemnum',      false);
		$in_itemid       = Input::get('in-itemid',      false);
		$in_itemscope    = Input::get('in-itemscope',    false);
		$in_itemunit     = Input::get('in-itemunit',     false);
		$in_itemunitcost = Input::get('in-itemunitcost', false);
		$in_itemamount   = Input::get('in-itemamount',   false);
		$in_accmplsh     = Input::get('in-accmplsh',   false);
		
		$work_item       = '';
		$is_successful   = FALSE;


		$acc_report = SPIAccomplishmentReport::find($in_projectid);
		if ($acc_report == NULL) 
		{
			$acc_report = new SPIAccomplishmentReport;
			$acc_report->acc_report_id = $in_projectid;
			$acc_report->save();
		}

		if($in_itemnum == '*')
		{
			$monolog->addInfo('NEW Work Item', array('OKS'));
			$work_item = new SPIWorkItem;
			$work_item->scope_of_work = $in_itemscope;
			$work_item->unit          = $in_itemunit;
			$work_item->save();
			$monolog->addInfo('Log Message Work Item', array($work_item->item_no));

			$acc_item = new SPIAccomplishmentItem;
			$acc_item->acc_report_id = $in_projectid;
			$acc_item->item_no       = $work_item->item_no;
			$acc_item->item_id       = $in_itemid;
			$acc_item->unit_cost     = $in_itemunitcost;
			$acc_item->quantity      = $in_itemamount;
			$acc_item->save();
			$is_successful           = TRUE;
			$tasks = DB::select('call recalc_accomplishment_items("' . $in_projectid . '")');
			Session::put("spi-accomplishment.$in_projectid.store", $is_successful);
		}
		else
		{
			$work_item = SPIWorkItem::find($in_itemnum);

			if($work_item)
			{
				$monolog->addInfo('Work Item existing', array('OK WORK ITEM'));
				$acc_item = new SPIAccomplishmentItem;
				$acc_item->acc_report_id = $in_projectid;
				$acc_item->item_id       = $in_itemid;
				$acc_item->item_no       = $in_itemnum;
				$acc_item->unit_cost     = $in_itemunitcost;
				$acc_item->quantity      = $in_itemamount;
				$acc_item->save();
				$is_successful           = TRUE;
				$tasks = DB::select('call recalc_accomplishment_items("' . $in_projectid . '")');
			}

			Session::put("spi-accomplishment.$in_projectid.store", $is_successful);
		}
		$monolog->addInfo('Accomplishment Report', array(Session::all()));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());

		$user       = Session::get('username');
		$muni_psgc  = $this->user->getPsgcId($user);
		$project    = SPIProfile::find($id);
		$work_items = SPIWorkItem::allFromMunicipality($muni_psgc)->get();
		$acc_report = SPIAccomplishmentReport::find($project->project_id);


		$monolog->addInfo('Accomplishment Item', array(Session::all()));

		$monolog->addInfo('Accomplishment Report', array($acc_report));

		$monolog->addInfo('Accomplishment Report', array(SPIPlannedPOWItem::allFromProject($project->project_id)->get()));
		$monolog->addInfo('SUMMARY', array(SPISummaryPOWItem::allFromProject($project->project_id)->get()));

		$store_status   = Session::pull("spi-accomplishment.$id.store", null);
		$update_status  = Session::pull("spi-accomplishment.$id.update", null);
		$destroy_status = Session::pull("spi-accomplishment.$id.destroy", null);

		$monolog->addInfo('Accomplishment Item', array(Session::all()));

		return $this->view('spiaccomplishment.show')
			->with('title', 'Sub-Project Accomplishment Report')
			->with('username', $user)
			->with('id', $id)
			->with('project', $project)
			->with('work_items', $work_items)
			->with('summary', SPISummaryPOWItem::allFromProject($project->project_id)->get())
			->with('plans', SPIPlannedPOWItem::allFromProject($project->project_id)->get())
			->with('acc_report', $acc_report)
			->with('acc_report_items', $acc_report == '' ? array(): $acc_report->items)
			->with('create_status', $store_status)
			->with('update_status', $update_status)
			->with('destroy_status', $destroy_status);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$in_mode         = Input::get('in-mode',   false);

		$work_item       = '';
		$is_successful   = FALSE;
		if($in_mode == 'add_item')
		{
			$in_accitemid    = Input::get('in-accitemid',    false);
			$in_projectid    = Input::get('in-projectid',    false);
			$in_itemunitcost = Input::get('in-itemunitcost', false);
			$in_itemamount   = Input::get('in-itemamount',   false);

			$acc_item = SPIAccomplishmentItem::find($in_accitemid);
			$acc_item->unit_cost     = $in_itemunitcost;
			$acc_item->quantity      = $in_itemamount;
			$acc_item->save();
			$tasks = DB::select('call recalc_accomplishment_items("' . $in_projectid . '")');
			$is_successful           = TRUE;			
			Session::put("spi-accomplishment.$in_projectid.update", $is_successful);
		}
		else if($in_mode == 'add_plan')
		{
			$in_projectid = Input::get('in-projectid', false);
			$in_monthnum  = Input::get('in-monthnum',     false);
			$in_item      = Input::get('in-item',      false);
			$in_startend  = Input::get('in-startend',  false);
			$in_physical  = Input::get('in-physical',  false);
			$in_financial = Input::get('in-financial', false);

			$plan_item = new SPIPlannedPOWItem;
			$plan_item->id = $in_projectid .'-'.$in_item;
			$plan_item->acc_report_id = $in_projectid;
			$plan_item->item_no       = $in_item;
			$plan_item->month_num     = $in_monthnum;
			$plan_item->startend      = $in_startend;
			$plan_item->physical      = $in_physical;
			$plan_item->financial     = $in_financial;
			$s = $plan_item->save();
		}
		else if($in_mode == 'add_actual')
		{
			$id           = Input::get('id', false);
			$in_physical  = Input::get('in-physical',  false);
			$in_financial = Input::get('in-financial', false);
			$actual_item = SPIActualPOWItem::find($id);
			if($actual_item)
			{
				$monolog->addInfo('Actual POW Item', array('existing'));
				$actual_item->physical  = $in_physical;
				$actual_item->financial = $in_financial;
				$actual_item->save();
			}
			else
			{

				$monolog->addInfo('Actual POW Item', array('new'));
				$actual_item = new SPIActualPOWItem;
				$actual_item->id        = $id;
				$actual_item->physical  = $in_physical;
				$actual_item->financial = $in_financial;
				$actual_item->save();

			
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$mode   = Input::get('mode',    false);

			$monolog->addInfo('MODE', array($mode));
		if($mode == 'del_items')
		{
			$del_id = Input::get('del-id',    false);
			$monolog->addInfo('REMOVE', array($del_id));
			$is_successful = FALSE;
			if($del_id)
			{
				$affectedRows = SPIAccomplishmentItem::find($del_id)->delete();
				$is_successful = ($affectedRows > 0) ? TRUE : FALSE;
				$tasks = DB::select('call recalc_accomplishment_items("' . $id . '")');

			}
			Session::put("spi-accomplishment.$id.destroy", $is_successful);	
		}
		else
		{
			$del_id = Input::get('del-id',    false);
			$monolog->addInfo('REMOVE', array($del_id));
			$is_successful = FALSE;
			if($del_id)
				$hasActualPOW = SPIActualPOWItem::find($del_id);

				if(isset($hasActualPOW)){
					if($hasActualPOW->physical>0 and $hasActualPOW->financial>0 )
					{
						return Response::json(['message'=>'You must clear the actual plan first'],406 );
					}else{
						 SPIActualPOWItem::find($del_id)->delete();
						$affectedRows = SPIPlannedPOWItem::find($del_id)->delete();
						$is_successful = ($affectedRows > 0) ? TRUE : FALSE;

					}
				}else{

					$affectedRows = SPIPlannedPOWItem::find($del_id)->delete();
						$is_successful = ($affectedRows > 0) ? TRUE : FALSE;

				}
				
				// if(!$hasActualPOW)
				// {
				// 	$affectedRows = SPIPlannedPOWItem::find($del_id)->delete();
				// 	$is_successful = ($affectedRows > 0) ? TRUE : FALSE;
					
				// }
				// else
				// {
				// 	$is_successful = FALSE;
				// 	return Response::json(['message'=>'You must clear the actual plan first'],406 );
				// }
		}


		
	}
}
