<?php

class MLPRAPDetailsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
        return $this->view('mlprapdetails.index')->with('mlprap_id', $id)
            ->with('mlprap', MLPRAP::where('action_id', $id)->get())
            ->with('mlprap_details', MLPRAPDetails::where('plan_id', $id)->get())
            ->with('title', 'Municipal Local Poverty Reduction Action Plan')
            ->with('username', Session::get('username'));
	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return $this->view('mlprapdetails.show')->with('mlprap_id', $id)
												->with('mlprap', MLPRAP::where('action_id', $id)->get())
												->with('mlprap_details', MLPRAPDetails::where('plan_id', $id)->get())
												->with('title', 'Municipal Local Poverty Reduction Action Plan')
												->with('username', Session::get('username'));
	}

	







	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($mlprap_id)
	{
		return $this->view('mlprapdetails.create')->with('mlprap', MLPRAP::where('action_id', $mlprap_id)->first())
													->with('fundsources', RFFundSource::all()->lists('name'))
													->with('leadgroups', RFLeadGroup::all()->lists('name'))
                                                    ->with('agency', DB::table('rf_agencies')->lists('name','name'))
													->with('username', Session::get('username'))
													->with('title', 'Municipal Local Poverty Reduction Action Plan');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$mlprap_id = Input::get('mlprap_id');
		$validation = MLPRAPDetails::validate(Input::all());
		$data = Input::all();
				unset($data["_token"]);
				unset($data["_method"]);
				// dd($data);
		        if(array_key_exists("agency", $data)){

                    $agency_represented = "";
                    foreach($data["agency"] as $agency){
                        $agency_represented = $agency_represented.','.$agency;
                    }
                    $data["agency"] = ltrim ($agency_represented, ',');
                    unset( $data['agency_text']);
                }else{
                	if(isset($data['agency_text'])){
	                    $data['agency'] =  $data['agency_text'];
	                    unset( $data['agency_text']);
                	}
                }

	            if(array_key_exists("lead_group", $data)){

                    $leadgroup_represented = "";
                    foreach($data["lead_group"] as $lead_group){
                        $leadgroup_represented = $leadgroup_represented.','.$lead_group;
                    }
                    $data["lead_group"] = ltrim ($leadgroup_represented, ',');
                    unset( $data['leadgroup_text']);
                }else{
                	if(isset($data['leadgroup_text'])){

	                    $data['lead_group'] =  $data['leadgroup_text'];
	                    unset( $data['leadgroup_text']);
                	}
                }



		if( $validation->fails() ) {
			return Redirect::route('mlprapdetails.create', $mlprap_id)->withErrors($validation)->withInput();
		}
		else {

			// For primary key increments | avoid duplication
			$id = 0;
			$check_id = MLPRAPDetails::where('actionplan_id', $mlprap_id. '-' . $id)->get();
			$actionplan_id = $mlprap_id. '-' . $id;

			while( !$check_id->isEmpty() ) {
				$id++;
				$check_id = MLPRAPDetails::where('actionplan_id', $mlprap_id. '-' . $id)->get();
				$actionplan_id = $mlprap_id. '-' . $id;
			}


			// For fund source and lead group
			$fund_sources = RFFundSource::all()->lists('name');
			$lead_groups = RFLeadGroup::all()->lists('name');

			$fund_source = '';
			// $lead_group = '';

			if( Input::get('fund_source') == 'others' ) {
				$fund_source = Input::get('other_fund_source');
			}
			elseif( Input::get('fund_source') != 'default') {
				$fund_source = $fund_sources[ Input::get('fund_source')];
			}


			// if( Input::get('lead_group') == 'others' ) {
			// 	$lead_group = Input::get('other_lead_group');
			// }
			// elseif( Input::get('lead_group') != 'default') {
			// 	$lead_group = $lead_groups[ Input::get('lead_group')];
			// }

			
            // if( Input::get('agency') == 'others' ) {
            //     $agency = Input::get('other_agency');
            // }
            // elseif( Input::get('agency') != 'default') {
            //     $agency = Input::get('agency');
            // }
				$agency = $agency_represented;
				$lead_group = $leadgroup_represented;
				// dd($agency);
			
			MLPRAPDetails::create(array(
				'actionplan_id'		=> $actionplan_id,
				'plan_id'			=> $mlprap_id,
				'project_name'		=> Input::get('project_name'),
				'coverage'			=> Input::get('coverage'),
				'stakeholders'		=> Input::get('stakeholders'),
				'problem'			=> Input::get('problem'),
				'gaps'				=> Input::get('gaps'),
				'actions'			=> Input::get('actions'),
				'timeframe_from'	=> $this->setDate(Input::get('timeframe_from')),
				'timeframe_to'		=> $this->setDate(Input::get('timeframe_to')),
				'fund_amount'		=> Input::get('fund_amount'),
				'fund_source'		=> $fund_source,
				'lead_group'		=> $lead_group,
				'agency'			=> $agency,
				'beneficiaries'		=> Input::get('beneficiaries')
			));
			
			return Redirect::route('mlprapdetails.show', $mlprap_id)
								->with('message', 'Action Plan has been added successfully.');
		}
	}










	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		
		$mlprapdetails = MLPRAPDetails::where('actionplan_id', $id)->first();
		$mlprap = MLPRAP::where('action_id', $mlprapdetails->plan_id)->first();		// for reference/redirect

		if(Approval::isReviewed($mlprapdetails->plan_id))
        {
            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
            return Redirect::to('/mlprapdetails/'.$mlprapdetails->plan_id);
        }	
		$fund_sources = RFFundSource::all()->lists('name');
		$lead_groups = RFLeadGroup::all()->lists('name');
        $agency  =  DB::table('rf_agencies')->lists('name','name');
		$fund_source = 'others';
		$lead_group = 'others';
        $agency = 'others';
		if( in_array($mlprapdetails->fund_source, $fund_sources) ) {
			$fund_source = array_search($mlprapdetails->fund_source, $fund_sources);
		}
		elseif( $mlprapdetails->fund_source == '' ){
			$fund_source = 'default';
		}

        if( in_array($mlprapdetails->agency, DB::table('rf_agencies')->lists('name','name')) ) {
            $agency = array_search($mlprapdetails->agency, DB::table('rf_agencies')->lists('name','name'));
        }
        elseif( $mlprapdetails->agency == '' ){
            $agency = 'default';
        }



        $mlprapdetails->lead_group =  explode(",",  $mlprapdetails->lead_group);
        $mlprapdetails->agency =  explode(",",  $mlprapdetails->agency);
		return $this->view('mlprapdetails.edit')->with('mlprapdetails', $mlprapdetails)
												->with('mlprap', $mlprap)
												->with('fund_sources', $fund_sources)
												->with('leadgroups', $lead_groups)
												->with('fund_source', $fund_source)
												->with('lead_group', $lead_group)
                                                ->with('agency', DB::table('rf_agencies')->lists('name','name'))
                                                ->with('agencies', $agency)
												->with('username', Session::get('username'))
												->with('title', 'Municipal Local Poverty Reduction Action Plan');

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$validation = MLPRAPDetails::validate(Input::all());
		$data = Input::all();

        unset($data["_token"]);
        unset($data["_method"]);
        // dd($data);
        if(array_key_exists("agency", $data)){

            $agency_represented = "";
            foreach($data["agency"] as $agency){
                $agency_represented = $agency_represented.','.$agency;
            }
            $data["agency"] = ltrim ($agency_represented, ',');
            unset( $data['agency_text']);
        }else{
            $data['agency'] =  $data['agency_text'];
            unset( $data['agency_text']);
        }

        if(array_key_exists("lead_group", $data)){

            $leadgroup_represented = "";
            foreach($data["lead_group"] as $lead_group){
                $leadgroup_represented = $leadgroup_represented.','.$lead_group;
            }
            $data["lead_group"] = ltrim ($leadgroup_represented, ',');
            unset( $data['leadgroup_text']);
        }else{
            $data['lead_group'] =  $data['leadgroup_text'];
            unset( $data['leadgroup_text']);
        }
		$data['is_draft'] = isset($data['is_draft']) ? 1 : 0; 



		if( $validation->fails() ) {
			return Redirect::route('mlprapdetails.edit', $id)->withErrors($validation)->withInput();
		}
		else {

			// For fund source and lead group
			$fund_sources = RFFundSource::all()->lists('name');
			$lead_groups = RFLeadGroup::all()->lists('name');

			$fund_source = '';
			$lead_group = '';

			if( Input::get('fund_source') == 'others' ) {
				$fund_source = Input::get('other_fund_source');
			}
			elseif( Input::get('fund_source') != 'default') {
				$fund_source = $fund_sources[ Input::get('fund_source')];
			}


//			if( Input::get('lead_group') == 'others' ) {
//				$lead_group = Input::get('other_lead_group');
//			}
//			elseif( Input::get('lead_group') != 'default') {
//				$lead_group = $lead_groups[ Input::get('lead_group')];
//			}
//            if( Input::get('agency') == 'others' ) {
//                $agency = Input::get('other_agency');
//            }
//            elseif( Input::get('agency') != 'default') {
//                $agency = Input::get('agency');
//            }

            $agency = $agency_represented;
            $lead_group = $leadgroup_represented;

			MLPRAPDetails::where('actionplan_id', $id)->update(array(
				'project_name'		=> Input::get('project_name'),
				'coverage'			=> Input::get('coverage'),
				'stakeholders'		=> Input::get('stakeholders'),
				'problem'			=> Input::get('problem'),
				'gaps'				=> Input::get('gaps'),
				'actions'			=> Input::get('actions'),
                'timeframe_from'	=> $this->setDate(Input::get('timeframe_from')),
                'timeframe_to'		=> $this->setDate(Input::get('timeframe_to')),
				'fund_amount'		=> Input::get('fund_amount'),
				'fund_source'		=> $fund_source,
				'lead_group'		=> $lead_group,
				'agency'			=> $agency,
				'beneficiaries'		=> Input::get('beneficiaries')
			));

			return Redirect::route('mlprapdetails.show', Input::get('mlprap_id'))->with('message', 'Action Plan has been updated successfully.');
		}
	}




	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
		$MLPRAPDetails = MLPRAPDetails::where('actionplan_id', $id);
		if(Approval::isReviewed($MLPRAPDetails->first()->plan_id))
        {
            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
            return Redirect::to('/mlprapdetails/'.$MLPRAPDetails->first()->plan_id);
        }	
		if( $MLPRAPDetails != NULL ) {
			$mlprap_id = $MLPRAPDetails->first()->plan_id; // for redirect
			$MLPRAPDetails->delete();
			return Redirect::route('mlprapdetails.show', $mlprap_id)->with('message', 'Action Plan has been deleted successfully.');
		}
	}


}

















	// public function upload() {
		
	// 	$data = MLPRAPDetails::all()->toJSON();

	// 	echo 'Saving JSON File..';

	// 	// timestamp file
	// 	$filename = public_path(). "/updates/udpates.json";
		
	// 	// Save File
 //        $bytes_written = File::put($filename, $data);
	// 	if ($bytes_written === false) {
	// 	    die("Error writing to file");
	// 	}

	// 	$contents = json_decode(File::get($filename));
	//     echo '<pre>';
	//     dd($contents);
	//     echo '</pre>';

	// }


	// public function download() {

	// 	$data = MLPRAPDetails::all()->toJSON();

	// 	$filename = public_path(). "/updates/udpates.json";
		
	// 	// Save File
 //        $bytes_written = File::put($filename, $data);
	// 	if ($bytes_written === false) {
	// 	    die("Error writing to file");
	// 	}

	// 	// Download
 //        $headers = array( 'Content-Type' => 'application/json' );
 //        return Response::download($filename, 'updates.json', $headers);

	// }


	// public function read() {

	// 	$filename = public_path(). "/updates/udpates.json";

	// 	$contents = json_decode(File::get($filename));
	// 	echo 'hey';
	//     echo '<pre>';
	//     var_dump($contents);
	//     echo '</pre>';

	//     MLPRAPDetails::truncate();

	//     foreach( $contents as $content ) {
	//   		MLPRAPDetails::create(array(
	// 			'actionplan_id'		=> $content->actionplan_id,
	// 			'plan_id'			=> $content->plan_id,
	// 			'project_name'		=> $content->project_name,
	// 			'coverage'			=> $content->coverage,
	// 			'stakeholders'		=> $content->stakeholders,
	// 			'problem'			=> $content->problem,
	// 			'gaps'				=> $content->gaps,
	// 			'actions'			=> $content->actions,
	// 			'timeframe'			=> $content->timeframe,
	// 			'fund_amount'		=> $content->fund_amount,
	// 			'fund_source'		=> $content->fund_source,
	// 			'lead_group'		=> $content->lead_group,
	// 			'agency'			=> $content->agency,
	// 			'beneficiaries'		=> $content->beneficiaries,
	// 			'created_at'		=> $content->created_at,
	// 			'updated_at'		=> $content->updated_at
	// 		));
	//     }


	//     // DB::statement("SET foreign_key_checks=0");
	// 	// Model::truncate();
	// 	// DB::statement("SET foreign_key_checks=1");

	// }

	


	/* 

		// Reading files - PHP
		// header('Content-Type: application/json;charset=utf-8');
		// $s = file_get_contents('config.txt');
		// echo json_encode($s);
		// read
		// $p = json_decode(file_get_contents('filename.json'));


		// Process in server - // add tables for last_upload_date
		// Insert to server

		
		// $header = array(
		// 	"Content-type: application/json",
		// 	"Content-length: ".strlen($data_string),
		// );

		// $ch = curl_init();

		// $url = 'http://localhost/kalahiserver/test/upload';

		// curl_setopt($ch, CURLOPT_URL, $url);
		// curl_setopt($ch, CURLOPT_POST, true );
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

		// $result = curl_exec($ch);

		// curl_close($ch);


	*/
