<?php
use Repositories\SystemUpdate\SystemUpdateInterface;
class AuthenticationController extends \BaseController {

	/**
	 * Make an auth check.
	 *
	 * @return Response
	 */
    protected  $systemUpdate;
    protected $grammars;

    public function __construct(SystemUpdateInterface $systemUpdate)
    {
        $this->systemUpdate = $systemUpdate;
    }
	public function index()
	{		
		
		if(Auth::check())
		{
			return Redirect::to('/reports');
		}
		// echo "<pre>";
		
       	$filedir = app_path().'\storage\update.json';
		if(file_exists($filedir))
		{
			$file = json_decode(file_get_contents($filedir));
			if(isset($file)){
				if($file->version != version())
				{
					$this->systemUpdate->updatemlcc();
					$this->systemUpdate->updateIDinVol();
					$this->systemUpdate->updateSpi();
					$this->systemUpdate->updateDuplicates();
					$this->systemUpdate->updateLog();
					$this->systemUpdate->updateDB();
					$this->systemUpdate->add_spi_utlization();

					//update to 1.7.5 to 1.7.6
					$this->systemUpdate->updateMIBFproject();
					$this->systemUpdate->updateGrsDB();
					$this->systemUpdate->updateProcurement();
					$this->systemUpdate->updateERS();
						
					$file->version = version();
					$file->last_updated = date('Y-m-d h:s A');
		
					file_put_contents($filedir, json_encode($file));
					Session::flash('updated',true);
				}
		
			}else{
				Session::flash('updated_not','Update not successful');
			}
			$file = json_decode(file_get_contents($filedir));
			// dd($file);
		}else{

		}
		
		$data = [];
		// $update_status = false;
		// if($update_status){
		// 	Session::flash('must_update',true);
		// }

		/* EMPTy DATA MEANS NOT YET Updated*/
		return View::make('login')->with('title', 'Login')->with($data);
	}




	/**
	 * Authentication if the user has already login
	 *
	 * @return Response
	 */
	public function login()
	{
		$username = Input::get('username');
		
		$password =Input::get('password');
		
		$mode = Input::get('mode');

		if (Auth::attempt(array('username' => $username, 'password' => $password))){
			
			Session::put('username', $username);
			
			Session::put('accelerated',$mode);	

            Session::flash('modals',true);

            session_start();
            $_SESSION['user'] = $username;

            
	        if( Volunteer::where('cycle_id','NCDDP')->exists() ){
	            Session::flash('update_volunteer',true);
	        }
	        TransLog::log('Login','login',$username,Auth::user()->psgc_id);
			return Response::json(['status'=>'ok']);
		}else{
			return Response::json(['error'=>'error']);
		}
	}

	/**
	 *  Delete session and the Auth instance variable to clear
	 *
	 * @return Response
	 */
	public function logout()
	{
		try{

	    	TransLog::log('Logout','Logout',Auth::user()->username,Auth::user()->psgc_id);
		}catch(Exception $e){
			
		}
		Session::flush();
		Auth::logout();
		return Redirect::to('');
	}

}
