<?php

class GrsBarangayController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['username'] = Session::get('username');
		
		$data['title'] = 'GRS Barangay';
		$mode = Session::get('accelerated');//kc mode
		$data['grsbarangays'] = GrsBarangayModel::where('kc_mode',$mode)
												->with('barangay.municipality')
												->with('barangay.province')
												->with('barangay.province.region')
												->get();
			
		return $this->view('/grsbarangay.index')->with($data);
	}


	/**rf_munclass
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user     = Session::get('username');
        $user = $this->user->getPsgcId($user);

		$office   = Report::get_office($user);
		$psgc_id  = Report::get_muni_psgc($user);
		
		$kc_class = [];
		$cycles   = [];

		// $kc_class[0] = 0;
		foreach (MunClass::get() as $kc ) {
			$kc_class[$kc->classification] = $kc->classification;
		}
		
		foreach (NCDDP::get_cycles() as $cycle) {
			$cycles[$cycle] = $cycle;
		}


		if($office === 'ACT'){
			$barangay_id = Report::get_psgc_id($psgc_id);
		}
		else if($office === 'RPMO' or $office === 'SRPMO'){
			$barangay_id = Report::get_prov_psgc_id($psgc_id);
		}
		else{
			$barangay_id = Report::get_all_brgy_psgc_id();
		}
		
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);
		$barangay_list = array('NULL' => '') + $barangay_list;
		// data to pass to the parameter response 	
		$data['username'] 		= $user;
		$data['title'] 			= 'GRS Barangay';
		$mode = Session::get('accelerated'); //kc_mode
		$data['data'] = Report::get_brgy_assembly($data['username'],$mode);
 		$data['barangay_lists'] = $barangay_list;
 		$data['kc_class'] 		= $kc_class;
 		$data['cycles']   		= $cycles;
 		
		$municipality = Report::get_municipality($user);
		$province = Report::get_province($user);
 		$data['province']  = $province;
 		$data['municipality'] = $municipality;
 		$data['region']  = Report::get_region($user);
 		$data['cycle'] = $cycle = LguCycles::where('psgc_id',$user)->first();
 		$data['programs'] = $this->reference->getPrograms();

		return $this->view('grsbarangay.create')->with($data);
	}

	

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$rules = array(
			'psgc_id' =>'required',
			'program_id'    => 'required',
			'cycle_id' => 'required'
		
		);
		$data['psgc_id'] 		= $input['psgc_id'];
		$data['program_id']     = $input['program_id']; 
		$data['cycle_id']		= $input['cycle_id']; 
		$data['date_infodess']	= date('Y-m-d',strtotime($input['date_infodess']));
		$data['date_voliden']	= date('Y-m-d',strtotime($input['date_voliden']));;
		$data['date_ffcomm']	= date('Y-m-d',strtotime($input['date_ffcomm']));;
		$data['date_training']	= date('Y-m-d',strtotime($input['date_training']));;
		$data['date_inspect']	= date('Y-m-d',strtotime($input['date_inspect']));;
		$data['no_manuals']		= $input['no_manuals'];
		$data['no_brochures']	= $input['no_brochures'];
		$data['no_tarpauline']	= $input['no_tarpauline'];
		$data['no_posters']		= $input['no_posters'];
		$data['date_meansrept']	= date('Y-m-d',strtotime($input['date_meansrept']));;
		$data['is_boxinstalled']= isset($input['is_boxinstalled'])? 1 : 0;
		$data['phone_no']		= $input['phone_no'];
		$data['address']		= $input['address'];
		$data['remarks']		= $input['remarks'];
		$data['kc_mode']	    = Session::get('accelerated'); //kc mode
		$validator = Validator::make($input, $rules);
		
		if ( $validator->fails() ) {	

			return Redirect::back()
				->withErrors( $validator )
				->withInput( Input::all() );
		
		}
		else
		{	
			if( GrsBarangayModel::where('psgc_id',$data['psgc_id'])->where('cycle_id',$data['cycle_id'])->where('kc_mode',$data['kc_mode']  )->count() > 0 ){
				return Redirect::to( 'grs-barangay/create' )
				->withErrors( 'Duplicate Entry' );
			}else{
				Session::flash('message', 'Installation Successfully Added');

                $newsquence = $this->getNewCurSequence();
                $activity_id = 'GRSB'.Input::get('psgc_id').$newsquence;
                $data['reference_no'] = $activity_id;

				GrsBarangayModel::create($data);
				return Redirect::to( 'grs-barangay/' );
			}
		}

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        $data['username'] = Session::get('username');
        $data['title']    = 'Grs Barangay';

        $data['grs']  = GrsBarangayModel::where('reference_no',$id)
            ->first();
        return $this->view('grsbarangay.show')->with($data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user     = Session::get('username');
        $user = $this->user->getPsgcId($user);
		$office   = Report::get_office($user);
		$psgc_id  = Report::get_muni_psgc($user);
		

		
  		if(Approval::isReviewed($id))
        {
            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
            return Redirect::to('/grs-barangay/'.$id);
        }	
		
		$kc_class = [];
		$cycles   = [];

		// $kc_class[0] = 0;
		foreach (MunClass::get() as $kc ) {
			$kc_class[$kc->classification] = $kc->classification;
		}
		
		foreach (NCDDP::get_cycles() as $cycle) {
			$cycles[$cycle] = $cycle;
		}


		if($office === 'ACT'){
			$barangay_id = Report::get_psgc_id($psgc_id);
		}
		else if($office === 'RPMO' or $office === 'SRPMO'){
			$barangay_id = Report::get_prov_psgc_id($psgc_id);
		}
		else{
			$barangay_id = Report::get_all_brgy_psgc_id();
		}
		
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);
		$barangay_list = array('NULL' => '') + $barangay_list;

		// data to pass to the parameter response 	
		$data['username'] 		= $user;
		$data['title'] 			= 'GRS Barangay';
		// $data['data']			= Report::get_brgy_assembly($data['username']);
 		$data['barangay_lists'] = $barangay_list;
 		$data['kc_class'] 		= $kc_class;
 		$data['cycles']   		= array('NULL' => 'Choose cycle') + $cycles;
 		$data['programs'] = $this->reference->getPrograms();
		$grsBarangayIns =  GrsBarangayModel::where('reference_no',$id)
											->first();
		$data['barangay'] = $grsBarangayIns;
	
		return $this->view('grsbarangay.edit')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
	
		$rules = array(
			// 'program_id'    => 'required',
			'cycle_id' => 'required',

		);
		$data['psgc_id'] 		= $input['psgc_id'];
		$data['program_id']     = $input['program_id']; 
		$data['cycle_id']		= $input['cycle_id']; 
		$data['is_draft'] = isset($input['is_draft']) ? 1 : 0; 

		$data['date_infodess']	= date('Y-m-d',strtotime($input['date_infodess']));
		$data['date_voliden']	= date('Y-m-d',strtotime($input['date_voliden']));;
		$data['date_ffcomm']	= date('Y-m-d',strtotime($input['date_ffcomm']));;
		$data['date_training']	= date('Y-m-d',strtotime($input['date_training']));;
		$data['date_inspect']	= date('Y-m-d',strtotime($input['date_inspect']));;
		$data['no_manuals']		= $input['no_manuals'];
		$data['no_brochures']	= $input['no_brochures'];
		$data['no_tarpauline']	= $input['no_tarpauline'];
		$data['no_posters']		= $input['no_posters'];
		$data['date_meansrept']	= date('Y-m-d',strtotime($input['date_meansrept']));;
		$data['is_boxinstalled']= isset($input['is_boxinstalled'])? 1: 0;
		$data['phone_no']		= $input['phone_no'];
		$data['address']		= $input['address'];
		$data['remarks']		= $input['remarks'];

		$validator = Validator::make($input, $rules);
			

		if ( $validator->fails() ) {	

			return Redirect::back()
				->withErrors( $validator )
				->withInput( Input::all() );
		
		}
		else
		{	

				Session::flash('message', 'Installation Successfully Updated');
				unset($data['psgc_id']);
				unset($data['program_id']);
				unset($data['cycle_id']);
				GrsBarangayModel::where('reference_no',$id)->update($data);
				return Redirect::to( 'grs-barangay' );
			
		}

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		GrsBarangayModel::where('psgc_id',$id)->delete();
		Session::flash('message', 'Installation Successfully Deleted');
		return Redirect::to( 'grs-barangay' );
	}

	/**  
	 *  function and method for extra functionality of the response 
	 *  
	 *	@return JSON Response
	 *	
	 */
	public function barangayData()
	{
		$input = Input::all();
  	   
		$barangay_id  = $input['barangay_id'];
	
		// get barangay details 
		$barangay_details = Barangay::where('barangay_psgc',$barangay_id)
										->with('municipality')
										->with('province')
										->with('province.region')
										->first();
		
		return Response::json($barangay_details);
	}


	public function lguCycles()
	{
		$input     = Input::all();
		
		$cycle_id  = $input['cycle_id'];

		$cycleData = GrsBarangayModel::lguCycle($cycle_id);
	
		return Response::json($cycleData);
	}

}
