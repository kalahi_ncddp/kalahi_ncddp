<?php




class MunTrainingController extends \BaseController {


    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
			$user = Session::get('username');
			$mode = Session::get('accelerated');

			$data = Trainings::get_muni_trainings($user, $mode);

			return $this->view('mun_trainings.index')
				->with('title', 'Municipal Trainings')
				->with('username', $user)
				->with('data', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
			$user = Session::get('username');
			$user = $this->user->getPsgcId($user);
			$office = Report::get_office($user);
			$psgc_id = Report::get_muni_psgc($user);
			$province = Report::get_province($psgc_id);
			$region = Report::get_region($psgc_id);

			if($office == "ACT"){
				$muni_id = Report::get_muni_psgc_id($psgc_id);
				$muni_name = Report::get_municipality($psgc_id);
				$muni_list = array($psgc_id => $muni_name);
			}else{
				$muni_id = Report::get_muni_psgc_id($psgc_id);
				$muni_name = NCDDP::get_municipality_name($muni_id);
				$muni_list = array_combine($muni_id, $muni_name);
			}

			$cycle = Report::get_current_cycle($psgc_id);
			$program = Report::get_current_program($psgc_id);
			
			$cycle_list = NCDDP::get_cycles();
			$cycle_list = array_combine($cycle_list, $cycle_list);
			
			
			$program_list = NCDDP::get_programs();
			$program_list = array_combine($program_list, $program_list);

            $lgutype = 'muni';
            $cat_list = $this->reference->getTrainingCat($lgutype);
		
			//added barangay list
			$data['brgy_list'] = $this->grsRepository->getBarangayList($user);
            $data['kc_code']     = $this->reference->getKCcode($user);
			return $this->view('mun_trainings.create')
			->with('title', 'Municipal/Inter-Barangay Training')
			->with('muni_list', $muni_list)
			->with('cat_list', $cat_list)
			->with('psgc_id', $psgc_id)
			->with('municipality', $muni_name)
			->with('province', $province)
			->with('cycle', $cycle)
			->with('program', $program)
			->with('region', $region)
			->with('cycle_list', $cycle_list)
			->with('program_list', $program_list)
			->with('username', $user)
			->with($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $mode 	    = Session::get('accelerated');

        $mun_training = Trainings::get_training_existing(Input::get('psgc_id'), Input::get('training_title'), Input::get('cycle_id'), Input::get('training_cat'), Input::get('date_conducted'), Input::get('program_id'),$mode);
			if($mun_training == NULL){
				$data = Input::all();
				unset($data["_token"]);
				unset($data["kc_code"]);
				unset($data["municipality"]);
				
				$mun_training = Trainings::get_training_by_psgc(Input::get('psgc_id'));
			
				$newsquence = $this->getNewCurSequence();

        		$reference_no = 'MTS'.Input::get('psgc_id').$newsquence;
			
				
				$data['reference_no'] = $reference_no;
				$data['lgu_type'] = 1;
	
				$data['start_date'] = $this->setDate($data['start_date']);
                $data['end_date'] = $this->setDate($data['end_date']);
				$data['kc_mode']	    = $mode;
                $lgu_activity = array(
                    'activity_id' => $reference_no,
                    'psgc_id' => $data["psgc_id"],
                    'activity_type' => 'munitraining',
                    'startdate' => $data['start_date'],
                    'enddate' => $data['end_date'],
                    'activity_name' => Input::get('training_cat'),
                    'kc_mode'=>Session::get('accelerated'),
                    'cycle_id'=>$data['cycle_id'],
                    'program_id'=>$data['program_id']
                );

                $lgu = LguActivity::where( ['activity_id'=>$reference_no,'psgc_id'=>$data["psgc_id"]])->exists();
                if($lgu){
                    $lgu_activity = array('activity_name'=>Input::get('training_cat'));
                    LguActivity::where( ['activity_id'=>$reference_no,'psgc_id'=>$data["psgc_id"],'activity_name'=>$lgu_activity['activity_name']])->update($lgu_activity);
                }else{

                    LguActivity::create($lgu_activity);
                }
                Trainings::insert($data);
				 Session::flash('message', 'Municipal/Inter-Barangay Training Record Successfully created!');
				return Redirect::to('mun_trainings/'.$reference_no.'/edit');
			}	
			else{
				$error = 'Municipal Training Existing. <a href = "'.URL::to('mun_trainings/'.$mun_training).'">'.$mun_training.'</a>';
				Session::flash('message', $error);
				return Redirect::to('mun_trainings/create')
				->withInput(Input::all());	
			}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
			$user = Session::get('username');
			$user = $this->user->getPsgcId($user);
			$mun_training = Trainings::find($id);
			
			$barangay_count = count(Report::get_psgc_id($mun_training->psgc_id));
			$mun_training['training_cat'] = DB::table('rf_trainingcat')->where('catagory_id',$mun_training['training_cat'] )
																		->orWhere('description',$mun_training['training_cat'])->where('is_muni',1)->first()->description;
		
	
			$barangay_data = $this->reference->getBarangayByMuni($user);
				$issues = NCDDP::get_issues_by_id($id);
			$trainor_data = Trainings::get_trainors($id);
			
			return $this->view('mun_trainings.show')
				->with('title', 'Municipal Training')
				->with('username', $user)
				->with('training', $mun_training)
				->with('barangay_count', $barangay_count)
				->with('barangay_data', $barangay_data)
				->with('trainor_data', $trainor_data)
				->with('issues',$issues);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
			if(Approval::isReviewed($id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
	            return Redirect::to('/mun_trainings/'.$id);
	        }	
			$user = Session::get('username');
			
			$mun_training = Trainings::find($id);

			$barangay_id = Report::get_psgc_id($mun_training->psgc_id);
			$barangay_name = NCDDP::get_barangay_name($barangay_id);
			$barangay_list = array_combine($barangay_id, $barangay_name);
			
			$barangay_data = Trainings::get_trnbrgy($id);
			
			$barangay_psgc = array();
			$barangay_pax = array();
			foreach($barangay_data as $barangay){
				array_push($barangay_psgc, $barangay->psgc_id);
				array_push($barangay_pax, $barangay->no_participants);
			}

			
			$trainor_data = Trainings::get_trainors($id);
			
			$lgutype = 'muni';
            $cat_list = $this->reference->getTrainingCat($lgutype);
			$data['cat_list'] = $cat_list;
			$data['description'] = $mun_training->reference_no;
			return $this->view('mun_trainings.edit')
				->with('title', 'Municipal/Inter-Barangay Training')
				->with('username', $user)
				->with('trainor_data', $trainor_data)
				->with('training', $mun_training)
				->with('barangay_list', $barangay_list)
				->with('barangay_psgc', $barangay_psgc)

				->with('barangay_pax', $barangay_pax)
				->with($data);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$data = Input::all();
				unset($data["_token"]);
				unset($data["_method"]);
				
				Trainings::delete_training('kc_trnbrgy', $id);
				Trainings::delete_training('kc_trainor', $id);
				
				
				if(array_key_exists("trainor_name", $data)){
					$trainor_data = $data["trainor_name"];
					$designation_data = $data["designation"];
					$topics_data = $data["topics_discussed"];
					
					unset($data["trainor_name"]);
					unset($data["designation"]);
					unset($data["topics_discussed"]);
					
					$i = 0;
					foreach($trainor_data as $trainor){
						Report::save_ncddp('kc_trainor', array('reference_no'=>$id, 'trainor_name'=>$trainor, 'designation'=>$designation_data[$i], 'topics_discussed'=>$topics_data[$i]));
						$i++;
					}
				}

			if(array_key_exists("pax", $data) ){
				if(  array_key_exists("brgy_checkbox", $data)){

					$brgy_data = $data["brgy_checkbox"];
					$pax_data = $data["pax"];
					unset($data["brgy_checkbox"]);
					unset($data["pax"]);
					$i = 0;
					foreach($brgy_data as $brgy){
//                      /*INSERT IF NOT EXISTS*/
                        $count = DB::table('kc_trnbrgy')->where(array('reference_no'=>$id))->count();
						if($count>0){
                            DB::table('kc_trnbrgy')->where(array('reference_no'=>$id))->update(array( 'psgc_id'=>$brgy, 'no_participants'=>$pax_data[$i]));
                        }else{
                            Report::save_ncddp('kc_trnbrgy', array('reference_no'=>$id, 'psgc_id'=>$brgy, 'no_participants'=>$pax_data[$i]));
                        }
						$i++;
					}
				}

			}else{
				unset($data["brgy_checkbox"]);
					unset($data["pax"]);
			}
				$data['is_draft'] = isset($data['is_draft']) ? 1 : 0; 

                $data['start_date'] = $this->setDate($data['start_date']);
                 $data['end_date'] = $this->setDate($data['end_date']);
                $training = Trainings::where('reference_no',$id)->first();
                $training_cat = $training->training_cat;
                $lgu_activity = array(
                    'activity_id' => $id,
                    'psgc_id' => $training->psgc_id,
                    'activity_type' => 'BRGYTRAINING',
                    'startdate' => $data['start_date'],
                    'enddate' => $data['end_date'],
                    'activity_name' => $training_cat,
                    'kc_mode'=>Session::get('accelerated'));

                $lgu = LguActivity::where( ['activity_id'=>$id,'psgc_id'=>$training->psgc_id])->exists();
                if($lgu){
                    $lgu_activity = array('activity_name'=>$training_cat);
                    LguActivity::where( ['activity_id'=>$id,'psgc_id'=>$training->psgc_id])->update($lgu_activity);
                }else{

                    LguActivity::create($lgu_activity);
                }
                 Trainings::update_training($data, $id);
				
				Session::flash('message', 'Municipal Training Record Successfully updated');
			
				return Redirect::to('mun_trainings/'.$id);
	}

	public function enlist_participants($id){
			$user = Session::get('username');
			
		
			$participants_male = Trainings::get_participants_by_sex($id, 'M');
			$participants_female = Trainings::get_participants_by_sex($id, 'F');
			$data = array('no_atnmale' => $participants_male, 'no_atnfemale' => $participants_female);
			Trainings::update_training($data, $id);
			
			$data = Trainings::get_participants($id);
            $datas['description'] = ' (<a href="'. URL::to('mun_trainings/'.$id).'">'.$id.'</a>) Participants';
			return $this->view('mun_trainings.enlist_participants')
				->with('title', 'Municipal Training')
				->with('id', $id)
				->with('username', $user)
				->with('data', $data)
                ->with($datas);

	}

	public function add_participant($id){
			if(Approval::isReviewed($id))
	        {
	            Session::flash('message','This training is already reviewed by AC.. Untag this to edit');
	            return Redirect::to('/mun_trainings/'.$id.'/enlist_participants');
	        }	
			$user = Session::get('username');
			$user = $this->user->getPsgcId($user);
			$psgc_id = Report::get_muni_psgc($user);
			$data = Volunteer::get_volunteers_list();
			$brgy_list = $this->grsRepository->getBarangayList($user);
            $data['sector_list'] = NCDDP::get_sectors();
            $data['cycle_id']  = Trainings::where('reference_no',$id)->first()->cycle_id;
            $data['program_id']= Trainings::where('reference_no',$id)->first()->program_id;
            $data['psgc_id']   = Trainings::where('reference_no',$id)->first()->psgc_id;

			return $this->view('mun_trainings.add_participant')
			->with('title', 'Municipal Training')
			->with('username', $user)
			->with('data', $data)
			->with('id', $id)
			->with('brgy_list',$brgy_list)->with($data);
			
	}

	public function submit_participant(){
        $participant = Trainings::get_participant_existing(Input::get('reference_no'),Input::get('sex'),Input::get('lastname'), Input::get('firstname'), Input::get('age'));
			if($participant == NULL){
				$data = Input::all();
				unset($data["_token"]);
				unset($data["_method"]);
				unset($data["birthdate"]);

                /* set sectors rep*/
                if(array_key_exists("sector", $data)){

                    $sector_represented = "";
                    foreach($data["sector"] as $sector){
                        $sector_represented = $sector_represented.','.$sector;
                    }
                    $data["sector"] = ltrim ($sector_represented, ',');
                    unset( $data['sector_text']);
                }else{
                  $data['sector'] =  $data['sector_text'];
                    unset( $data['sector_text']);
                }
                $data['is_volunteer'] = $data['is_volunteer']!="" ? $data['is_volunteer'] : 0;
                /* end set*/
				if($data["beneficiary_id"] == NULL){
					unset($data["beneficiary_id"]);
					$psgc_id = Report::get_muni_psgc(Session::get('username'));

					// $newsquence = $this->getNewCurSequence(); //generate new sequence number
					// // generate new benefiaciary ID based on Current Sequence of the Municipal kc table 
					// $beneficiary_id = 'B'.$psgc_id.''.$newsquence;
					// $data["beneficiary_id"] = $beneficiary_id;
					$input = $data;
					unset($input['is_volunteer']);
					unset($input['psgc_id']);
					unset($input['age']);
					
					unset($input['is_pantawid']);
					unset($input['is_slp']);
					unset($input['is_lguofficial']);
					unset($input['reference_no']);
					$bene_id = Beneficiary::generateBeneficiaryId($input);
					$data['beneficiary_id'] = $bene_id;
					
				}
				Trnparticipants::insertParticipant($data,Auth::user()->psgc_id,'MTP');


				return Redirect::to('mun_trainings/'.$data["reference_no"].'/enlist_participants')
					->with('message', 'Participant added.');
		
			}	
			else{
				$error = 'Participant Existing. Participant must have a different lastname, firstname ,sex and age';
				Session::flash('message', $error);
				return Redirect::to('mun_trainings/'.Input::get('reference_no').'/add_participant')
				->withInput(Input::all());	
			}		
	}
	public function edit_participant($id) 
	{

		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);
		$volunteer_list = Volunteer::get_volunteers_list();
		$participant = Trnparticipants::where('beneficiary_id',$id)
										->first();

		if(Approval::isReviewed($participant->reference_no))
        {
            Session::flash('message','This training is already reviewed by AC.. Untag this to edit');
            return Redirect::to('/mun_trainings/'.$participant->reference_no.'/enlist_participants');
        }
        $participant->beneficiary->sector =  explode(",", !empty($participant->beneficiary->sector)?$participant->beneficiary->sector : $participant->sector );
        $brgy_list = $this->grsRepository->getBarangayList($user);
        $data['sector_list'] = NCDDP::get_sectors();
        $data['sector_list'] = array_combine($data['sector_list'] , $data['sector_list']);
			return $this->view('mun_trainings.edit_participant')
			->with('title', 'Municipal Training')
			->with('username', $user)
			->with('data', $volunteer_list)
			->with('participant',$participant)
			->with('id', $id)
			->with('brgy_list',$brgy_list)
			->with('description',$participant->beneficiary_id)
                ->with($data);
	}

	public function updateParticipants($id)
	{
		$input = Input::all();
		unset($input["_token"]);
		unset($input["_method"]);
        /* set sectors rep*/
        if(array_key_exists("sector", $input)){

            $sector_represented = "";
            foreach($input["sector"] as $sector){
                $sector_represented = $sector_represented.','.$sector;
            }
            $input["sector"] = ltrim ($sector_represented, ',');

        }

        /* end set*/
		$benefiaciary_data = $input;

		unset($input["birthdate"]);
		unset($benefiaciary_data['psgc_id']);



		$reference = Trnparticipants::where('beneficiary_id',$id);
		$reference->update($input);
					unset($benefiaciary_data['is_volunteer']);
					unset($benefiaciary_data['age']);
					unset($benefiaciary_data['reference_no']);
		Beneficiary::where('beneficiary_id',$id)->update($benefiaciary_data);
		return Redirect::to('mun_trainings/'.$reference->first()->reference_no.'/enlist_participants')->with('message','Update Success');
	}
	public function delete_participant(){
		$data = Input::all();
		if(Approval::isReviewed($data['reference_no']))
        {
            Session::flash('message','This training is already reviewed by AC.. Untag this to delete participants');
            return Redirect::to('/mun_trainings/'.$data['reference_no'].'/enlist_participants');
        }	
		if(array_key_exists("participant", $data)){
			foreach($data["participant"] as $p){
				Trainings::delete_participant($p);
			}
			return Redirect::to('mun_trainings/'.$data["reference_no"].'/enlist_participants')
					->with('message', 'Participant Deleted.');
		}
		else{
			return Redirect::to('mun_trainings/'.$data["reference_no"].'/enlist_participants')
					->with('message', 'No Participant Deleted.');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Approval::isReviewed($id))
        {
            Session::flash('message','This data is already reviewed by AC.. Untag this to edit');
            return Redirect::to('/mun_trainings/'.$id);
        }	
        Trainings::delete_training('kc_trnbrgy', $id);
        Trainings::delete_training('kc_trainor', $id);
        Trainings::delete_training('kc_trnparticipants', $id);
        Trainings::delete_training('kc_trainor', $id);
        Trainings::where('reference_no',$id)->delete();

		return Redirect::to('mun_trainings/')
					->with('message', 'Municipal Training Record Deleted.');
		
	}

	public function getVolunteer()
	{

		$user = Session::get('username');
		$user = $this->user->getPsgcId($user);
		// $psgc_id = $this->reference->getMunicipalityByBarangayId($user);
        $psgc_id = Input::get('psgc_id',false);
        $cycle   = Input::get('cycle_id','NCDDP-1');
        $program_id = Input::get('program_id','KC-NCDDP');
        $session_mode = Session::get('accelerated');
        if($psgc_id){
           $volunteers = Volunteer::leftJoin('kc_beneficiary','kc_beneficiary.beneficiary_id','=','kc_volunteers.beneficiary_id')
            						->where('psgc_id','like', substr($psgc_id, 0,9).'%')
                                    ->where('program_id',$program_id)
                                    ->where('cycle_id',$cycle)
                                    ->where('kc_volunteers.kc_mode',$session_mode)
                                    ->select('kc_beneficiary.*','kc_volunteers.*')->get();
        } else {

        $volunteers = Volunteer::leftJoin('kc_beneficiary','kc_beneficiary.beneficiary_id','=','kc_volunteers.beneficiary_id')
									->where('psgc_id','like', substr(Auth::user()->psgc_id, 0,6).'%')
							        ->where('program_id',$program_id)
							        ->where('cycle_id',$cycle)
							        ->where('kc_volunteers.kc_mode',$session_mode)
							        ->select('kc_beneficiary.*','kc_volunteers.*')
							        ->get();

        }
        return Datatable::collection($volunteers)
		        ->showColumns('lastname', 'firstname','middlename','birthdate','option')
		          ->addColumn('birthdate',function($volunteer){
	         	    $volunteer = $volunteer;
		          	return toDate($volunteer->birthdate);
		          })
		         ->addColumn('option', function($volunteer) {
		         	  $vol = $volunteer->psgc_id!=null ? $volunteer->psgc_id : '';
		         	  $volunteer = $volunteer;
				      return  '<a class="btn btn-success btn" onclick="$(this).use_volunteer($(this));">Use
				                       <div class="hidden">
												<div class="lastname">'.$volunteer->lastname .'</div>
												<div class="firstname">'.$volunteer->firstname .'</div>
												<div class="middlename">'. $volunteer->middlename .'</div>
												<div class="birthdate">'. $volunteer->birthdate.'</div>
											 	<div class="sex">'. $volunteer->sex.'</div>
											 	<div class="no_children">'.$volunteer->no_children.'</div>
												<div class="civil_status">'.$volunteer->civil_status .'</div>
												<div class="address">'.$volunteer->address .'</div>
												<div class="contact_no">'.$volunteer->contact_no .'</div>
												<div class="educ_attainment">'.$volunteer->educ_attainment .'</div>
												<div class="occupation">'.$volunteer->occupation .'</div>
												<div class="beneficiary_id">'.$volunteer->beneficiary_id .'</div>
												<div class="sectors">'.$volunteer->sector_represented.'</div>
												<div class="psgc_id">'. $vol.'</div>
												<div class="is_ip">'.$volunteer->is_ip.'</div>
												<div class="is_ipleader">'.$volunteer->is_ipleader.'</div>
												<div class="is_pantawid">'.$volunteer->is_ppbene.'</div>
                                                <div class="is_slp">'.$volunteer->is_slpbene.'</div>
                                                <div class="is_lguofficial">'.$volunteer->is_lguofficial.'</div>

											 </div>
				                            </a>';

				    })
		        ->searchColumns('firstname')
		        ->orderColumns('lastname', 'firstname','middlename','birthdate','option')
		        ->make();
	}

    public function getParticipant()
    {
        $input = Input::all();

        $firstname = $input['firstname'];
        $middlename = $input['middlename'];
        $lastname = $input['lastname'];

        //check if it is exists in beneficiary
        $bene = Beneficiary::where(['firstname'=>$firstname,'lastname'=>$lastname,'middlename'=>$middlename])->first();
        $is_volunteer = Volunteer::where('beneficiary_id',$bene->beneficiary_id)->first();

        if(!empty($is_volunteer)){
            return Response::json(['status'=>'ok','volunteer'=>$bene]);
        }else{
            return Response::json(['status'=>'error']);
        }
    }
}