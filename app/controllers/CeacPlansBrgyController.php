<?php

class CeacPlansBrgyController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	
		$data['username'] = Session::get('username');
		$data['title']    = 'CEAC Plans';
		$data['barangay'] = CeacBrgy::get();

		return $this->view('ceac.plans.barangay.index')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	
		$user 			  = Session::get('username');
		$data['username'] = $user;
		$data['title']    = 'CEAC Plans';
		$kc_class = [];
		$cycles   = [];
		$purposes = [];
		// $kc_class[0] = 0;
		foreach (MunClass::get() as $kc ) {
			$kc_class[$kc->classification] = $kc->classification;
		}
		
		foreach (RfbaPurpose::get() as $purpose) {
			$purposes[$purpose->code] = $purpose->code.'-'.$purpose->description;
		}

		foreach (NCDDP::get_cycles() as $cycle) {
			$cycles[$cycle] = $cycle;
		}
		$office   = Report::get_office($user);
		$psgc_id  = Report::get_muni_psgc($user);
		if($office === 'ACT'){
			$barangay_id = Report::get_psgc_id($psgc_id);
		}
		else if($office === 'RPMO' or $office === 'SRPMO'){
			$barangay_id = Report::get_prov_psgc_id($psgc_id);
		}
		else{
			$barangay_id = Report::get_all_brgy_psgc_id();
		}
		
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);
		$barangay_list = array('NULL' => '') + $barangay_list;

		$data['barangay_lists'] = $barangay_list;
		$data['cycles'] 		= array('NULL' => 'Choose cycle') + $cycles;
		$data['purposes']		= $purposes;

		return $this->view('ceac.plans.barangay.create')->with($data);

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

			// 

		$rules = array(
			// 'program_id'    => 'required',
			'cycle_id' => 'required',
			'activity_code' => 'required',
			
		
		);

		$validator = Validator::make($input, $rules);

		if ( $validator->fails() ) {	

			return Redirect::to( 'ceac/plans/barangay/create' )
				->withErrors( $validator )
				->withInput( Input::all() );
		
		}
		else
		{	
				Session::flash('message', 'Plan Successfully Updated');
                $input['kc_mode'] = Session::get('accelerated');

				$input['startdate'] = date('Y-m-d',strtotime($input['startdate']));
				$input['enddate'] = date('Y-m-d',strtotime($input['enddate']));
				CeacBrgy::create($input);
				return Redirect::to( 'ceac/plans/barangay/' );
			
		}
	}


    /**
     * Display the specified resource.
     *
     * @param $cycle
     * @param $year
     * @param $muni_id
     * @param $brgy_id
     * @internal param int $id
     * @return Response
     */
	public function show($cycle_id,$program,$muni_id,$brgy_id)
	{
		$data['username'] = Session::get('username');
		$data['title']	  = 'CEAC Monitoring';
        $mode = Session::get('accelerated');
		$data['activities'] = CeacActivties::where('is_brgy_activity',1)
                                            ->where('is_regular','!=',$mode)
                                            ->orderBy('activity_no')
                                            ->groupBy('activity_code')
											->get();
        $data['reference_no'] = Ceac::where('program_id',$program)->where('cycle_id',$cycle_id)->first()->reference_no;

        $data['cycle']  = LguCycles::where('psgc_id',$muni_id)->first();
		$data['psgc_id'] = $brgy_id;
		$data['program'] = $program;
		$data['cycle_id']  = $cycle_id;
		$data['description'] = Barangay::find($brgy_id)->barangay;
		return $this->view('ceac.plans.barangay.show')->with($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user 			  = Session::get('username');
		$data['username'] = $user;
		$data['title']    = 'Ceac Plans';
		$kc_class = [];
		$cycles   = [];
		$purposes = [];
		// $kc_class[0] = 0;
		foreach (MunClass::get() as $kc ) {
			$kc_class[$kc->classification] = $kc->classification;
		}
		
		foreach (RfbaPurpose::get() as $purpose) {
			$purposes[$purpose->code] = $purpose->code.'-'.$purpose->description;
		}

		foreach (NCDDP::get_cycles() as $cycle) {
			$cycles[$cycle] = $cycle;
		}
		$office   = Report::get_office($user);
		$psgc_id  = Report::get_muni_psgc($user);
		if($office === 'ACT'){
			$barangay_id = Report::get_psgc_id($psgc_id);
		}
		else if($office === 'RPMO' or $office === 'SRPMO'){
			$barangay_id = Report::get_prov_psgc_id($psgc_id);
		}
		else{
			$barangay_id = Report::get_all_brgy_psgc_id();
		}
		
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);
		$barangay_list = array('NULL' => '') + $barangay_list;

		$data['barangay_lists'] = $barangay_list;
		$data['cycles'] 		= array('NULL' => 'Choose cycle') + $cycles;
		$data['purposes']		= $purposes;

		$data['ceac'] = CeacBrgy::where('psgc_id',$id)->first();
		
		return $this->view('ceac.plans.barangay.edit')->with($data);
	}


    /**
     * Update the specified resource in storage.
     *
     * @param $cycle
     * @param $year
     * @param $muni_id
     * @param $brgy_id
     * @internal param int $id
     * @return Response
     */
	public function update($cycle,$year,$muni_id,$brgy_id)
	{
		$input = Input::all();


		// build input for planned;
		$planned['psgc_id'] = $input['psgc_id'];
		$planned['program_id'] = $input['program'];
		$planned['cycle_id'] = $input['cycle_id'];
		$planned['activity_code'] = $input['activity_code'];
        $planned['kc_mode'] = Session::get('accelerated');
		$planned['startdate'] = $this->setDate($input['plan_start']);
        $planned['reference_no'] = $input['reference_no'];

        $planned['enddate'] = $this->setDate($input['plan_end']);

       	 $input['actual_start'] = Input::get("actual_start","");
       	 $input['actual_end'] = Input::get("actual_end","");

		$planned['actual_start'] = $input['actual_start']=="" ? "" : $this->setDate($input['actual_start']);
		$planned['actual_end']  = $input['actual_end']==""? "" : $this->setDate($input['actual_end']);
			 // CeacMuni::firstOrNew($planned)->save();
		$plan = CeacBrgy::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->exists();

        $planned_to_compare = CeacBrgy::where(['psgc_id'=>$input['psgc_id'],
                                'cycle_id'=>$input['cycle_id'],
                                'program_id'=>$input['program'],
                                'kc_mode'=> $planned['kc_mode']])->orderBy('enddate', 'desc')->first();

        $actual_to_compare = CeacBrgy::where(['psgc_id'=>$input['psgc_id'],
            'cycle_id'=>$input['cycle_id'],
            'program_id'=>$input['program'],
            'kc_mode'=> $planned['kc_mode']])->orderBy('actual_end', 'desc')->first();

        // if(!empty($actual_to_compare))
        // {
        //     if( strtotime($actual_to_compare->actual_end) > strtotime($planned['actual_end']) )
        //     {
        //         $actual_plan = CeacBrgy::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->exists();
        //         if(!$actual_plan){
        //             return Response::json(['result' => 'error','message' => 'Actual Date must be higher to '. toDate($actual_to_compare->actual_end)]);
        //         }
        //     }
        // }
        // if(empty($planned_to_compare))
        // {
        //     $plan = CeacBrgy::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->exists();
        //     if($plan){
        //         CeacBrgy::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])
        //             ->update($planned);
        //     }else{
        //         CeacBrgy::create($planned);
        //     }
        //     return Response::json(['result'=>'success']);
        // }
        // else
        // {
            // if(   strtotime($planned_to_compare->enddate)>strtotime($planned['startdate'])) {
            //     $plan = CeacBrgy::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->exists();
            //     if(!$plan){
            //         return Response::json(['result' => 'error', 'message' => 'Planned must be higher to ' . toDate($planned_to_compare->enddate)]);
            //     }else{
            //         CeacBrgy::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])
            //             ->update($planned);
            //         return Response::json(['result' => 'success', 'message' => 'Updated' . toDate($planned_to_compare->enddate)]);

            //     }
            // }
            // else
            // {
                $plan = CeacBrgy::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->exists();
                if($plan){
                    CeacBrgy::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])
                        ->update($planned);
                }else{
                    CeacBrgy::create($planned);
                }
                return Response::json(['result'=>'success']);
            // }
        // }


		// $actual = lguActivity::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->exists();
		// if($plan){
		// 	CeacMuni::where(['psgc_id'=>$planned['psgc_id'],'cycle_id'=>$planned['cycle_id'],'program_id'=>$planned['program_id'],'activity_code'=>$planned['activity_code']])->update($planned);
		// }else{
		// 	CeacMuni::create($planned);
		// }

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{ 
		CeacBrgy::where('psgc_id',$id)->delete();
		Session::flash('message', ' Successfully Deleted');
		return Redirect::to('ceac/plans/barangay');
	}

	/**  
	 *  function and method for extra functionality of the response 
	 *  
	 *	@return JSON Response
	 *	
	 */
	public function barangayData()
	{
		$input = Input::all();
  	   
		$barangay_id  = $input['barangay_id'];
	
		// get barangay details 
		$barangay_details = Barangay::where('barangay_psgc',$barangay_id)
										->with('municipality')
										->with('province')
										->with('province.region')
										->first();
		
		return Response::json($barangay_details);
	}

	public function lguCycles()
	{
		$input     = Input::all();
		
		$cycle_id  = $input['cycle_id'];

		$cycleData = DB::table('kc_lgucycles')->where('cycle_id',$cycle_id)
										  ->where('lgu_type',1)->first();
	
		return Response::json($cycleData);
	}

}
