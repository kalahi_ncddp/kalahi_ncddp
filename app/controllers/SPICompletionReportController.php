<?php

class SPICompletionReportController extends \BaseController {
	public function show($id) {
		$user = Session::get('username');
		$spcr = SPICompletionReport::where(array('project_id' => $id))->first();

		if (isset($spcr)) {
			$profile = SPIProfile::where(array('project_id' => $id))->first();
			$proposal = ProjProposal::where(array('project_id' => $profile->mibf_refno))->first();
			return $this->view('spi_completion_report.show')
				->with('username', $user)
				->with('title', 'Sub Project Completion Report')
				->with('description', $spcr->project_id)
				->with('spcr', $spcr)
				->with('profile', $profile)
				->with('proposal', $proposal);
		}

		return Redirect::route('spi_profile.spcr.create', $id);
	}

	public function create($id) {
		$user = Session::get('username');

		$profile = SPIProfile::where(array('project_id' => $id))->first();
		$proposal = ProjProposal::where(array('project_id' => $profile->mibf_refno))->first();
		// get csw and prioritization dates
		$csw_date = $prio_date = null;
		if ($proposal != null) {
			$activity_id = $proposal->activity_id;
			$csw = LguActivity::where(array('activity_id' => $activity_id, 'activity_type' => 'CSW'))->first();
			$prio = LguActivity::where(array('activity_id' => $activity_id, 'activity_type' => 'MIBF-PRA'))->first();
			$csw_date = isset($csw) ? $csw->startdate : null;
			$prio_date = isset($prio) ? $prio->startdate : null;
		}

		// get barangay list
		$barangay_id = Report::get_available_barangays($user);
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);

		// get list of procurement modes
		$proc_modes = SPIProcurementMode::lists('mode', 'mode_id');

		$spcr = new SPICompletionReport(array(
			'project_id'						=> $id,
			'date_csw'							=> $csw_date,
			'date_prioritization'		=> $prio_date,
			'no_skilled_male'				=> $profile->skilled_male,
			'no_skilled_female'			=> $profile->skilled_female,
			'no_unskilled_male'			=> $profile->nonskilled_male,
			'no_unskilled_female'		=> $profile->nonskilled_female
		));

		return $this->view('spi_completion_report.create')
			->with('username', $user)
			->with('title', 'Sub Project Completion Report')
			->with('spcr', $spcr)
			->with('profile', $profile)
			->with('proposal', $proposal)
			->with('description', $spcr->project_id)
			->with('proc_modes', $proc_modes)
			->with('barangay_list', $barangay_list);
	}

	public function store() {
		$user = Session::get('username');
		$data = Input::all();
		unset($data["_token"]);

		// update date completed

		$profile = SPIProfile::where(array('project_id' => $data['project_id']))->first();
		$profile->male_members = $data["no_male_serve"];
		$profile->female_members = $data["no_female_serve"];
		$profile->total_ip = $data["no_ip_hh"];
		$profile->date_completed = $this->setDate($data["date_completed"]);
		$profile->total_families = $data["no_families"];
		$profile->total_4p = $data["no_pantawid_fm"];
		$profile->save();

		// store completion report 
		unset($data['date_completed']);
		$data['date_inaugurate'] = $this->setDate($data["date_inaugurate"]);
		SPICompletionReport::create($data);

		// store barangays covered
		
		if(array_key_exists("barangay", $data)){
			// store barangays covered
				
			foreach ($data["barangay"] as $brgy) {
				SPIBarangay::create(array(
					'project_id'	=> $id,
					'barangay_id'	=> $brgy
				));
			}
		}
		Session::flash('success', "SP Completion Report successfully created");
		return Redirect::route('spi_profile.spcr.show', $data['project_id']);
	}

	public function edit($id) {
		$user = Session::get('username');
		$spcr = SPICompletionReport::where(array('project_id' => $id))->first();
		$profile = SPIProfile::where(array('project_id' => $id))->first();
		$proposal = ProjProposal::where(array('project_id' => $profile->mibf_refno))->first();
		$proc_modes = SPIProcurementMode::lists('mode', 'mode_id');

		// get barangay list
		$barangay_id = Report::get_available_barangays($user);
		$barangay_name = NCDDP::get_barangay_name($barangay_id);
		$barangay_list = array_combine($barangay_id, $barangay_name);

		return $this->view('spi_completion_report.edit')
			->with('username', $user)
			->with('title', 'Sub Project Completion Report')
			->with('description', $spcr->project_id)
			->with('spcr', $spcr)
			->with('profile', $profile)
			->with('proposal', $proposal)
			->with('proc_modes', $proc_modes)
			->with('barangay_list', $barangay_list);
	}

	public function update($id) {
		$user = Session::get('username');
		$spcr = SPICompletionReport::where(array('project_id' => $id))->first();

		$data = Input::all();
		unset($data["_token"]);

		// update date completed
		$profile = SPIProfile::where(array('project_id' => $data['project_id']))->first();
		$profile->date_completed = $this->setDate($data["date_completed"]);
		$profile->male_members = $data["no_male_serve"];
		$profile->female_members = $data["no_female_serve"];
		$profile->total_families = $data["no_families"];
		$profile->total_ip = $data["no_ip_hh"];
		$profile->total_4p = $data["no_pantawid_fm"];
		$profile->save();

		// store completion report
		unset($data['date_completed']);
		$data['date_inaugurate'] = $this->setDate($data["date_inaugurate"]);
		$spcr->fill($data);
		$spcr->save();

		if(array_key_exists("barangay", $data)){
			// store barangays covered
			SPIBarangay::where(array('project_id' => $id))->delete();

				
			foreach ($data["barangay"] as $brgy) {
				SPIBarangay::create(array(
					'project_id'	=> $id,
					'barangay_id'	=> $brgy
				));
			}
		}
		

		Session::flash('success', "SP Completion Report successfully updated");
		return Redirect::route('spi_profile.spcr.show', $id);
	}
}