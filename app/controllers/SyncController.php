<?php

class SyncController extends \BaseController {

	public function download($direct = false) {


//        Lguactivity
            $app = Approval::where('act_reviewed', '!=', '0000-00-00 00:00:00')->where('kc_mode',Session::get('accelerated'))->get()->lists('activity_id');
            /* check if there is no reviewed data*/
            if(empty($app)){
                Session::flash('exportError','No Reviewed data to be export');
                return Redirect::to('reports');
            }
            /* store lgu activity */
            $data['version'] = version();
            $data['lguactivity']	= LguActivity::whereIn('activity_id',$app)->get();

//iterate approved
            $user = Session::get('username');
            $psgc_id = $this->user->getPsgcId($user);
            $approved =  Approval::where('act_reviewed', '!=', '0000-00-00 00:00:00')
                                    ->whereNull('uploaded_at')
                                    ->where('psgc_id',$psgc_id)
                                    ->where('kc_mode',Session::get('accelerated'));

//            upload ba
            /* DONE */
            $data['bassembly'] 		= BarangayAssembly::whereIn('activity_id',$approved->lists('activity_id'))
                                                                            ->with('sitiorep')
                                                                            ->with('sectorsrep')
                                                                            ->with('activityissues')
                                                                            ->get();
//         upload volunteers
          /* DONE*/
           $data['volunteers']      =   Volunteer::leftJoin('rf_approved','rf_approved.activity_id','=','kc_volunteers.volunteer_id')
                                                                                ->where('rf_approved.act_reviewed', '!=', '0000-00-00 00:00:00')
                                                                                ->where('rf_approved.kc_mode',Session::get('accelerated'))
                                                                                ->select('kc_volunteers.*')
                                                                                ->with('beneficiary')
                                                                                ->with('beneficiary.memotherorg')
                                                                                ->with('beneficiary.memothertrn')
                                                                                ->with('volcommmem')
                                                                                ->get();
                                                                                // echo "<pre>";
                                                                                // dd(json_decode(json_encode($data['volunteers'])));
//           upload trainings
            /* DONE */
            $data['trainings']       = Trainings::whereIn('reference_no',$approved->lists('activity_id'))
                                        ->with('trnbrgy')
										->with('trnparticipants')
										->with('trnparticipants.beneficiary')
										->with('trainor')
                                        ->get();
//           up barangay psa

            $data['psa']              = PSAnalysis::whereIn('activity_id',$approved->lists('activity_id'))
                                            ->with('sitiorep')
											->with('sectorsrep')
											->with('activityissues')
											->with('psaproject')
                                            ->with('psaproject.solutions')
											->with('psadocuments')
                                            ->get();
//          upload mibf
            $data['mibf']             = MunicipalForum::whereIn('activity_id',$approved->lists('activity_id'))
                                            ->with('sectorsrep')
											->with('projproposal')
											->with('projproposal.projcoverage')
                                            ->get();
//          upload pta integration list
            $data['lguengagement']    = LGUEngagement::whereIn('engagement_id',$approved->lists('activity_id'))
                                            ->with('lgudates')
                                            ->with('lguequipments')
                                            ->get();
            $data['grs-barangay']     = GrsBarangayModel::whereIn('reference_no',$approved->lists('activity_id'))->get();
            $data['grs-municipality'] = GrsMuniModel::whereIn('reference_no',$approved->lists('activity_id'))->get();

            $data['grs-intake']       = Intake::whereIn('reference_no',$approved->lists('activity_id'))->with('grsupdates')->get();

            $data['brgyprofile']      = BrgyProfile::whereIn('profile_id',$approved->lists('activity_id'))
                                            ->with('bdcp')
                                            ->with('orgs')
                                            ->with('devproj')
                                            ->with('bdcmeet')
                                            ->with('facilities')
                                            ->with('income')
                                            ->with('ipprof')
                                            ->with('fundsource')
                                            ->with('landstatus')
                                            ->with('techresource')
                                            ->with('transpo')
                                            ->with('crop')
                                            ->with('envcrit')
                                            ->with('lgu')
                                            ->get();

            $data['muniprofile']      = MuniProfile::whereIn('profile_id',$approved->lists('activity_id'))
                                            ->with('techres')
                                            ->with('fund')
                                            ->with('mlgu')
                                            ->with('mdcp')
                                            ->with('income')
                                            ->with('empowerment')
                                            ->with('drmfund')
                                            ->with('gadfund')
                                            ->with('munitranspo')
                                            ->get();



            $data['spi_profile']      = SPIProfile::whereIn('project_id',$approved->lists('activity_id'))
                                            ->with('tranche')
                                            ->with('spcr')
                                            ->with('procurement')
                                            ->with('set')
                                            ->with('utilization')
                                            ->get();
            $data['mlcc']             = MLCCMain::whereIn('mlcc_id',$approved->lists('activity_id'))
                                            ->with('detail')
                                            ->get();


            $data['mlprap']           = MLPRAP::whereIn('action_id',$approved->lists('activity_id'))
                                            ->with('mlprapdetails')
                                            ->get();
            $data['fundedpsa']        = SPIFundedPSAPrio::whereIn('id',$approved->lists('activity_id'))->get();
            $data['occ']              = SPIOCComm::whereIn('id',$approved->lists('activity_id'))->get();
           
            $data['spi-erslist']      = SPIERSList::whereIn('record_id', $approved->lists('activity_id') )->with('ers_records')->with('ers_records.worker')->get();

            $data['spi-accomplishment'] = SPIAccomplishmentReport::whereIn('acc_report_id', $approved->lists('activity_id'))
                                            ->with('items')
                                            ->with('items.item')
                                            ->with('plans')
                                            ->with('plans.actual')
                                            ->with('report')
                                            ->get();
            $data['spita']            = SPITechAssist::whereIn('id',$approved->lists('activity_id'))->get();
            $data['disasterdb']       = SPIProjDisaster::whereIn('projdisaster_id',$approved->lists('activity_id'))->with('items')->get();
            $data['sustainability']   = SPIMuncplSustnbltyPlan::leftJoin('rf_approved','rf_approved.activity_id','=','spi_muncplsustnbltyplans.id')
                                                                                ->where('rf_approved.act_reviewed', '!=', '0000-00-00 00:00:00')
                                                                                ->where('rf_approved.kc_mode',Session::get('accelerated'))->get();
//            echo '<pre>';
//            dd( (array) $data['spi-accomplishment']);
        //for ceac brgy
        /*
         * this part is for uploading ceac
         *
         * */
        $ceacapproved =  Approval::where('act_reviewed', '!=', '0000-00-00 00:00:00')
            ->whereNull('uploaded_at')

            ->where('module_type','ceac')
            ->where('kc_mode',Session::get('accelerated'))->get();


            foreach($ceacapproved as $approve)
            {
                $brgy = \Barangay::where('muni_psgc',$approve->psgc_id)->lists('barangay_psgc');

                if(isset($brgy) and !empty($brgy))
                {
                    $datum = DB::table('kc_ceacbgry')->whereIn('psgc_id',$brgy)
                        ->where(function($query) use ($approve){
                            // if($approve->cycle_id != null){
                                $query->where('cycle_id',$approve->cycle_id);
                            // }
                        })
                        ->where(function($query) use ($approve){
                            // if($approve->program_id != null){
                                $query->where('program_id',$approve->cycle_id);
                            // }
                        })
                        // ->where('program_id',$approve->program_id)
                        ->get();
                    if(!empty($datum)) {
                        $data['ceac-brgy'][] = $datum;
                    }
                }
            }
            foreach($ceacapproved as $approve)
            {
                $datum = CeacMuni::where('psgc_id',$approve->psgc_id)
                    ->where('cycle_id',$approve->cycle_id)
                    ->where('program_id',$approve->program_id)
                    ->get();
                    if(!empty($datum)) {
                        $data['ceac-muni'][] = $datum;
                    }
            }

            foreach($ceacapproved as $approve)
            {
                $datum = Ceac::where('psgc_id',$approve->psgc_id)
                    ->where('cycle_id',$approve->cycle_id)
                    ->where('program_id',$approve->program_id)
                    ->get();
                    if(!empty($datum)) {
                        $data['ceaclist'][] = $datum;
                    }
            }
            

		// Volunteers
		// Done: KC_Volunteers | KC_VolCommMem | KC_MemOtherOrg | KC_MemOtherTrn | KC_Beneficiary

		// Training
		// Done: KC_Trainings | KC_trnbrgy | KC_trnparticipants | KC_trainor | KC_Beneficiary

		// BA Subtables
		// Done: SitioRep | SectorsRep | ActivityIssues

		// MIBF
		// Done: KC_MunicipalForum | KC_SectorsRep | KC_ProjProposal | KC_ProjCoverage

		// PSA Subtables
		// Done: KC_LGUActivity KC_PSAnalysis | KC_SitioRep | KC_SectorsRep | KC_ActivityIssues | KC_PSAProject | KC_PSADocuments


		$path		= public_path() . "/app/storage/jsons/";
		$filename	= Session::get('username'). '-' . date('Y-m-d_H-i-s') . '.json';

		
		// Save File to storage/jsons
		$bytes_written = File::put($path . $filename, json_encode($data));
		if ($bytes_written === false) {
			die("Error writing to file");
		}

		// Download
		$headers = array( 'Content-Type' => 'application/json' );
        if(!$direct){
            return Response::download($path . $filename, $filename, $headers);
        }else{
			return  [
				'response' => Response::download($path . $filename, $filename, $headers) ,
				'name' => $filename
			];
		}


	}


	public function upload() {

//        if(!$this->is_connected()){
//            Session::flash('message','no internet connection use export instead');
//            return Redirect::to('reports');
//        }
		// concerns
		// DONE 1. Filename
		// 2. Acitivity_id error
		// DONE 3. Auth before

		// download latest state | get filenamex
		$downloadedFile = $this->download(true);
		$filename = $downloadedFile['name'];

		// File Details
		$file = file_get_contents(public_path() . '/app/storage/jsons/' . $filename);
		$header = array( "Content-type: application/json" );
		$url = 'http://ncddpdb.dswd.gov.ph/upload';
        // $url = 'http://172.20.2.88:8000/upload';

		// Start cURL
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POST, true );
		curl_setopt($ch, CURLOPT_POSTFIELDS, $file);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


		$result = curl_exec($ch);
		curl_close($ch);
        if($result){
            $array = json_decode($result);
		      Session::flash('message', isset($array->status) ? $array->status : '...' );
        } else {
        Session::flash('error', 'Network error.');

        }    
		return Redirect::to('reports');

	}
    function is_connected()
    {
        $connected = @fsockopen("www.google.com", 80);
        //website, port  (try 80 or 443)
        if ($connected){
            $is_conn = true; //action when connected
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;
    }
}

	


