<?php

class SPIFundedPSAController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$store_status   = Session::pull("spi-fnddpsa.store",  null);
		$update_status  = Session::pull("spi-fnddpsa.update",  null);
		$destroy_status = Session::pull("spi-fnddpsa.destroy", null);

		$user           = Session::get('username');
		$psgc_id        = Report::get_muni_psgc($user);
		$municipality   = Report::get_municipality($psgc_id);
		
		$problems        = PSAProjects::AllFromMunicipality($psgc_id)->get();
		$search = Input::get('search');

		$kc_projects     = SPIProfile::get_municipality_projects($psgc_id,$search);
		$psaprios        = SPIFundedPSAPrio::where('kc_mode',Session::get('accelerated'))->where('psgc_id',Auth::user()->psgc_id)->get();
		return $this->view('spifnddpsa.index')
			->with('title', 'Funded PSA Priorities')
			->with('username', $user)
			->with('municipality', $municipality)
			->with('municipality_psgc', $psgc_id)
			->with('problems', $problems)
			->with('kc_projects', $kc_projects)
			->with('psaprios', $psaprios)
			->with('store_status', $store_status)
			->with('update_status', $update_status)
			->with('destroy_status', $destroy_status);
	}


    public function getSolutions()
    {
        $fund = SPIFundedPSAPrio::where('kc_mode',Session::get('accelerated'))->where('psgc_id',Auth::user()->psgc_id)->get()->lists('kcsoltn_project_id');
        if(empty($fund)){
            $solutions = Psasolutions::where('project_id',Input::get('id'))->get();
        }else{
            $solutions = Psasolutions::where('project_id',Input::get('id'))->whereNotIn('solution_id',$fund)->get();
        }

        return Response::json($solutions);
    }

    public function getSpi()
    {
        $solutions = Psasolutions::where('solution_id',Input::get('id'))->first();

        $solutions = $solutions->spi();

        return Response::json($solutions);
    }



	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$is_successful = false;
		$problem_project_id  = Input::get('in-problem',   false);
		$kcsol_project_id    = Input::get('in-solution',   false);
		$kc_ncddp  = Input::get('kc_ncddp_sub_project_id',   '');
        $kc_fund   = Input::get('kc_fund',0);
        $funder    = Input::get('funder','');
        $fund      = Input::get('fund',0);
        $sp        = Input::get('sub_project','');


		if(!($problem_project_id === false || $kcsol_project_id === false ))
		{
			$spi_fundedpsaprio = new SPIFundedPSAPrio;
            $spi_fundedpsaprio->psgc_id = Auth::user()->psgc_id;
			$spi_fundedpsaprio->prob_project_id = $problem_project_id;
			$spi_fundedpsaprio->kcsoltn_project_id = $kcsol_project_id;
            $spi_fundedpsaprio->kc_ncddp_sub_project_id = $kc_ncddp;
            $spi_fundedpsaprio->kc_fund = $kc_fund;
            $spi_fundedpsaprio->funder = $funder;
            $spi_fundedpsaprio->fund = $fund;
            $spi_fundedpsaprio->sub_project = $sp;
            $spi_fundedpsaprio->kc_mode = Session::get('accelerated');
            $spi_fundedpsaprio->save();

			$is_successful = true;
		}

		Session::put("spi-fnddpsa.store", $is_successful);
        return Redirect::to('spi-fnddpsa');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        SPIFundedPSAPrio::where('id',Input::get('id'))->update([
            'kc_fund'=> Input::get('edit_kc_fund',0),
            'funder'=>  Input::get('funder',''),
            'fund'  =>  Input::get('edit_fund',0),
            'sub_project'=> Input::get('sub_project','')
        ]);

        Session::flash('update_status',true);
        return Redirect::to('spi-fnddpsa');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$affectedRows = SPIFundedPSAPrio::find($id)->delete();
		$is_successful = ($affectedRows > 0) ? TRUE : FALSE;
		Session::put("spi-fnddpsa.destroy", $is_successful);
        return Redirect::to('spi-fnddpsa')->with('destroy_status',true);
	}


}
