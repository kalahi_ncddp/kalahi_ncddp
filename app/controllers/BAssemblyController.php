<?php

use Repositories\Users\UsersInterface;

class BAssemblyController extends \BaseController {

 
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $user = Session::get('username');

    	$user = $this->user->getPsgcId($user);

    	$mode = Session::get('accelerated');
        $data =  BarangayAssembly::where('psgc_id','like',substr($user, 0,6).'%')->where('kc_mode',$mode)->get(); 
        
        return $this->view('bassembly.index')
		            	->with('data', $data)
		            	->with('title', 'Barangay Assembly')
		            	->with('username', $user);



	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
			$user = Session::get('username');
			$office = Report::get_office($user);
			$psgc_id = Report::get_muni_psgc($user);
            $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);

            if($office === 'ACT'){
				$barangay_id = Report::get_psgc_id($psgc_id);
			}
			else if($office === 'RPMO' or $office === 'SRPMO'){
				$barangay_id = Report::get_prov_psgc_id($psgc_id);
			}
			else{
				$barangay_id = Report::get_all_brgy_psgc_id();
			}
			$barangay_name = NCDDP::get_barangay_name($barangay_id);
			$barangay_list = array_combine($barangay_id, $barangay_name);
			$barangay_list = array('NULL' => 'Select Barangay') + $barangay_list;
			
		
			$cycle = Report::get_current_cycle($psgc_id);
			$program = Report::get_current_program($psgc_id);
			$municipality = Report::get_municipality($psgc_id);
			$province = Report::get_province($psgc_id);
			
			$program_list = NCDDP::get_programs();
        	$program_list = array_combine($program_list, $program_list);

			$cycle_list = NCDDP::get_cycles();
			$cycle_list = array_combine($cycle_list, $cycle_list);
			$cycle_list = array('NULL' => 'Select Cycle') + $cycle_list;
				
			$mode = Session::get('accelerated');

			$purpose = $this->reference->getBApurpose($mode);
			// added data
            $datas['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);
			return $this->view('bassembly.create')
			->with('title', 'Barangay Assembly')
			->with('brgy_list', $barangay_list)
			->with('municipality', $municipality)
			->with('province', $province)
			->with('cycle', $cycle)
			->with('cycle_list', $cycle_list)
			->with('program', $program)
			->with('program_list', $program_list)
			->with('purpose_list', $purpose)
			->with('username', $user)
            ->with($datas);
			
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'barangay'   => 'required',
			'psgc_id'   => 'required',
			'purpose'    => 'required',
			'program_id'    => 'required',
			'cycle_id' => 'required',
			'purpose' =>'required'
		);
		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('bassembly/create')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			$brgy_assembly = Report::get_brgy_assembly_existing(Input::get('psgc_id'), Input::get('program_id'), Input::get('cycle_id'), Input::get('purpose'),Session::get('accelerated'));
			if($brgy_assembly == NULL){
				$data = Input::all();
				unset($data["_token"]);
				unset($data["barangay"]);
				unset($data["kc_code"]);
				unset($data["municipality"]);
				unset($data["province"]);
				
				$brgy_assembly = Report::get_brgy_assembly_by_psgc(Input::get('psgc_id'));
				
					$newsquence = $this->getNewCurSequence();
					$activity_id = 'BA'.Input::get('psgc_id').$newsquence;

				$data['activity_id'] = $activity_id;
				$data['kc_mode'] = Session::get('accelerated');
				/*START Insert CEAC table for reference*/
				$this->reference->insertCeac([
					'psgc_id' => $data['psgc_id'],
					'program_id' => $data['program_id'],
					'cycle_id'	=> $data['cycle_id'],
            		'start_date' => $this->setDate( isset($data['start_date']) ? $data['start_date'] : ""),
            		'end_date' => $this->setDate(isset($data['end_date']) ? $data['end_date'] : ""),
					],
					$activity_id,
					'BA',
					$data['purpose']
					);
				/*END*/


				BarangayAssembly::create($data);
				
				return Redirect::to('bassembly/'.$activity_id.'/edit');
			}	
			else{
				$error = 'Barangay Assembly Existing. <a href = "'.URL::to('bassembly/'.$brgy_assembly).'">'.$brgy_assembly.'</a>';
				Session::flash('message', $error);
				return Redirect::to('bassembly/create')
				->withInput(Input::all());	
			}			
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
		$user = Session::get('username');

        $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);

        $brgy_assembly = NCDDP::get_brgy_assembly_by_id($id);
		$sector_data = NCDDP::get_sectors_by_id($id);
		$sitio_data = NCDDP::get_sitios_by_id($id);
		$issues = NCDDP::get_issues_by_id($id);
		$data['description'] = $brgy_assembly->activity_id;
		return $this->view('bassembly.show')
			->with('title', 'Barangay Assembly')
			->with('username', $user)
			->with('brgy_assembly', $brgy_assembly)
			->with('issues', $issues)
			->with('sector_data', $sector_data)
			->with('sitio_data', $sitio_data)
			->with($data);
			

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::check()){

			if(Approval::isReviewed($id))
	        {
	            Session::flash('message','This data is already reviewed by AC.. Untag this to data');
	            return Redirect::to('/bassembly/'.$id);
	        }	

			
			$user = Session::get('username');
            $data['position'] = $this->user->getEncoderById($user) == null ?  'ACT' :  $this->user->getEncoderById($user);

            $brgy_assembly = NCDDP::get_brgy_assembly_by_id($id);
			$sector_data = NCDDP::get_sectors_by_id($id);
			$sitio_data = NCDDP::get_sitios_by_id($id);
			$sector_list = NCDDP::get_sectors();
			$sector_list = array_combine($sector_list, $sector_list);
			
			$data['description'] = $brgy_assembly->activity_id;
			return $this->view('bassembly.edit')
				->with('title', 'Barangay Assembly')
				->with('username', $user)
				->with('brgy_assembly', $brgy_assembly)
				->with('sector_data', $sector_data)
				->with('sector_list', $sector_list)
				->with('sitio_data', $sitio_data)
				->with($data);
		}else{
			return $this->view('reports.login')
			->with('title', 'Login');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'startdate'   => 'required',
			'enddate'   => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		if ($validator->fails()) {	
			return Redirect::to('bassembly/'.$id.'/edit')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			
				$data = Input::all();

				$data['is_draft'] = isset($data['is_draft']) ? 1 : 0; 

				unset($data["_token"]);
				unset($data["_method"]);
				// unset($data["total_household"]);
				// unset($data["total_families"]);
				unset($data["percentage_household"]);
				unset($data["percentage_families"]);
						$lgu_activity = array('activity_id' => $id, 'startdate' => $this->setDate($data["startdate"]), 'enddate' => $this->setDate($data["enddate"]) );
				// Report::update_ncddp('KC_LGUActivity', $lgu_activity);
				LguActivity::where('activity_id',$id)->update([ 'startdate' => $this->setDate($data["startdate"]), 'enddate' => $this->setDate($data["enddate"]) ]);
				unset($data["startdate"]);
				unset($data["enddate"]);
				
				$data['activity_id'] = $id;
				if(array_key_exists("sector", $data)){
					$sector_data = $data["sector"];
					unset($data["sector"]);
					Report::delete_ncddp('KC_SectorsRep', $id);
					foreach($sector_data as $sector){
						Report::save_ncddp('KC_SectorsRep', array('activity_id'=>$id, 'sector'=>$sector));
					}
				}
				else{
					Report::delete_ncddp('KC_SectorsRep', $id);
				}
				if($data["sitio"] != NULL){
					$sitio_data = $data["sitio"];
					unset($data["sitio"]);
					Report::delete_ncddp('KC_SitioRep', $id);
					foreach($sitio_data as $sitio){
						if($sitio != NULL)
							Report::save_ncddp('KC_SitioRep', array('activity_id'=>$id, 'sitio'=>$sitio));
					}
				}
				
				// Report::update_ncddp('KC_BAssembly', $data);
				// changed this to model so that timestamp will be supported eloquent based
				BarangayAssembly::where('activity_id', $data['activity_id'])
            	->update($data);
				
				Session::flash('message', 'BA Successfully updated');
				return Redirect::to('bassembly/'.$id);
				
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Report::delete_ncddp('KC_SectorsRep', $id);
		Report::delete_ncddp('KC_SitioRep', $id);
		Report::delete_ncddp('KC_BAssembly', $id);

//		BarangayAssembly::where('activity_id',$id)->delete();
//		LguActivity::where('activity_id',$id)->delete();

		Report::delete_ncddp('KC_LGUActivity', $id);
		Report::delete_ncddp('KC_ActivityIssues', $id);

		// redirect
		Session::flash('message', 'BA Successfully deleted!');
		return Redirect::to('bassembly');
	}
	
	/**
	 * SET the is_draft to given resource
	 *
	 * @param  string  $id
	 * @return Response
	 */
	public function setFinal($id)
	{
		BarangayAssembly::where('activity_id', $id)->update(array('is_draft' => 0));
		
        return Response::json(['status'=>'ok']);
	}

	public function pincos()
	{
		

			$user = Session::get('username');
			$data = Input::all();
			$activity_id = $data['activity_id'];
			if(array_key_exists("raise", $data)){
				$raise_by = $data['raise'];
				$issue = $data['issue'];
				$action = $data['action'];
				Report::delete_ncddp('KC_ActivityIssues', $activity_id);
				$i = 0;
				foreach($raise_by as $r){
					DB::table('KC_ActivityIssues')->insert(array(
						'activity_id' => $activity_id,
						'issue_no' => $i,
						'raise_by' => $r,
						'issue' => $issue[$i],
						'action' => $action[$i])
					);
					$i++;
				}
			}else{
					Report::delete_ncddp('KC_ActivityIssues', $activity_id);
			}
			Session::flash('message', 'PINCOS Successfully updated.');
				return Redirect::back();	

	}
    public function approved()
    {
        $input = Input::all();
        try{
            foreach($input['check'] as $ids)
            {
                LguActivity::where('activity_id',$ids)->update(['ac_approved'=>date('Y-m-d')]);
            }
            return json_encode(['status'=>'ok']);
        }catch (Exception $e){
            return json_encode(['status'=>'error']);
        }

    }
}