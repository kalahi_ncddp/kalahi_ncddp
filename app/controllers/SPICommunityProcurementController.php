<?php

class SPICommunityProcurementController extends \BaseController {
	public function show($id) {
		$user = Session::get('username');

		$procurements = SPIProcurementMonitoring::where(array('project_id' => $id))->get();
		$profile = SPIProfile::where(array('project_id' => $id))->first();

		return $this->view('spi_procurement.show')
			->with('username', $user)
			->with('title', 'Community Procurement Monitoring')
			->with('description', $id)
			->with('procurements', $procurements)
			->with('profile', $profile);
	}

	public function edit($id) {
		$user = Session::get('username');

		$procurements = SPIProcurementMonitoring::where(array('project_id' => $id))->get();
		$profile = SPIProfile::where(array('project_id' => $id))->first();

		// get list of procurement modes
		$proc_modes = SPIProcurementMode::lists('mode', 'mode_id');

		// get list of suppliers
		$suppliers = SPISupplier::lists('name', 'id');

		// get list of statuses
		$statuses = SPIProcurementStatus::lists('status', 'status_id');

		$nol_issued_list = ["NPMO" => "NPMO", "RPMO" => "RPMO", "World Bank" => "World Bank"];

		return $this->view('spi_procurement.edit')
			->with('username', $user)
			->with('title', 'Community Procurement Monitoring')
			->with('procurements', $procurements)
			->with('profile', $profile)
			->with('description', $id)
			->with('proc_modes', $proc_modes)
			->with('statuses', $statuses)
			->with('nol_issued_list', $nol_issued_list)
			->with('suppliers', $suppliers);
	}

	public function update($id) {
		$user = Session::get('username');
		
		$data = Input::all();
		unset($data["_token"]);

		// update date completed
		$profile = SPIProfile::where(array('project_id' => $data['project_id']))->first();

		// store barangays covered
//        echo '<pre>';
//        dd($data);
		for ($i = 1; $i < count($data["package_no"]); $i++) {
			$proc_data = array();
			$proc_data['eproc_date'] = $this->setDate($data["eproc_date"][$i]);
			$proc_data['canvas_start'] = $this->setDate($data["canvas_start"][$i]);
			$proc_data['canvas_end'] = $this->setDate($data["canvas_end"][$i]);
			$proc_data['prebid_date'] = $this->setDate($data["prebid_date"][$i]);
			$proc_data['open_date'] = $this->setDate($data["open_date"][$i]);
			$proc_data['pqual_date'] = $this->setDate($data["pqual_date"][$i]);
			$proc_data['act_date'] = $this->setDate($data["act_date"][$i]);
			$proc_data['rpmo_date'] = $this->setDate($data["rpmo_date"][$i]);
			$proc_data['package_no'] = $data["package_no"][$i];
			$proc_data['package'] = $data["package"][$i];
			$proc_data['mode'] = $data["mode"][$i];
			$proc_data['nol_issued'] = $data["nol_issued"][$i];
			$proc_data['supplier_id'] = $data["supplier_id"][$i];
			$proc_data['contract_amnt'] = $data["contract_amnt"][$i];
			$proc_data['status'] = $data["status"][$i];
			$proc_data['project_id'] = $id;

			$proc = SPIProcurementMonitoring::where(array('project_id' => $id, 'package_no' => $data['package_no'][$i]))->first();

			if (isset($proc)) {
				$proc->fill($proc_data);
				$proc->save();
			} else {
				SPIProcurementMonitoring::create($proc_data);
			}
		}

		SPIProcurementMonitoring::where(array('project_id' => $id))->whereNotIn('package_no', $data["package_no"])->delete();

		Session::flash('success', "Community Procurement Monitoring successfully updated");
		return Redirect::route('spi_profile.procurement.show', $id);
	}
}