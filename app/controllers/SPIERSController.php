<?php

class SPIERSController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return "Hello Chan!";
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', array('Test sadasdasd'));

		$is_successful = false;
		$record_id     = Input::get('in-recordid',   false);
		$worker_ids    = Input::get('in-workerid',   array());

//        echo '<pre>';
//        dd($worker_ids[0]);
//        in-pantawid:on
//in-ip:on
//in-slp:on
//		$monolog->addInfo('record id', array($record_id));
//		$monolog->addInfo('worker id[]', array($worker_ids));

		if($record_id)
		{
			$spi_erslist   = SPIERSList::find($record_id);
			$monolog->addInfo('ERS LIST : ', array($spi_erslist));

			foreach($worker_ids as $worker_id)
			{
				$monolog->addInfo('worker id', array($worker_id));

				$spi_worker    = SPIWorker::find($worker_id);
				$spi_ersrecord = new SPIERSRecord;
				$spi_ersrecord->record_id = $record_id;
		        $monolog->addInfo('record id to', array($record_id));

				$spi_ersrecord->worker()->associate($spi_worker);
                $spi_ersrecord->current_work = $spi_worker->nature_work;
                $spi_ersrecord->rate_hour = $spi_worker->rate_hour;
				$spi_ersrecord->rate_day  = $spi_worker->rate_day;
				$spi_ersrecord->save();	
				$monolog->addInfo('record', array($spi_ersrecord));
			}
			$is_successful = true;
		}

		Session::put("spi-ers.$record_id.create", $is_successful);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', Session::all());

		$user          = Session::get('username');
		$spi_erslist   = SPIERSList::find($id);
		$monolog->addInfo('Log Message >>>', array($id));

		$spi_profile   = $spi_erslist->project;
		//$mncpl_workers = $spi_profile->workers;
		$muni_psgc_id   = Report::get_muni_psgc($user);
		$mncpl_workers  = SPIWorker::allFromMunicipality($muni_psgc_id)->get();

		$monolog->addInfo('ERS List', array($spi_erslist));
		$workers       = SPIERSRecord::allFromERSList($id)->paginate(10);
		
		$monolog->addInfo('Workers', array($workers));
		$create_status  = Session::pull("spi-ers.$id.create", null);
		$monolog->addInfo('Log Message', array("spi-ers.$id.create"));
		$destroy_status = Session::pull("spi-ers.$id.destroy", null);
        $natureofworklists = DB::table('rf_natureofwork')->lists('nature_of_work','nature_of_work');
        $typeofworklists = DB::table('rf_natureofwork')->groupBy('type')->lists('type','type');
		return $this->view('spiers.edit')
			->with('title', ('ERS Period for ' . $spi_profile->project_name))
			->with('spi_profile_name', $spi_profile->project_name)
			->with('record_id', $id)
			->with('project_id', $spi_profile->project_id)
			->with('create_status', $create_status)
			->with('destroy_status', $destroy_status)
			->with('spi_erslist', $spi_erslist)
			->with('workers', $workers)
			->with('municipal_workers', $mncpl_workers)
            ->with('natureofworklists',$natureofworklists)
            ->with('typeofworklists',$typeofworklists)
            ->with('username', $user);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$monolog = Log::getMonolog();
		$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
		$monolog->addInfo('Log Message', array("Ready for update"));
//        echo '<pre>';
//        dd(Input::all());
        $work             = Input::get('in-natureofwork',false);
		$rate_hour        = Input::get('in-ratehours',   0);
		$rate_day         = Input::get('in-rateday',   0);
		$work_hours       = Input::get('in-workhours',   0);
		$work_days        = Input::get('in-workdays',   0);
		$plan_cash        = Input::get('in-plancash',   0);
		$plan_bayanihan   = Input::get('in-planbayanihan',   0);
		$actual_cash      = Input::get('in-actualcash',   0);
		$actual_bayanihan = Input::get('in-actualbayanihan',   0);


		if(!($rate_hour  === false || $rate_day === false || $work_hours   === false ||
			 $work_days  === false || $plan_cash      === false || $plan_bayanihan === false ||
			 $actual_cash  === false || $actual_bayanihan      === false)
		)
		{
			$monolog->addInfo('Log Message', array("All complete"));
			$spi_ersrecord = SPIERSRecord::find($id);
            $spi_ersrecord->current_work = $work;
			$spi_ersrecord->rate_hour   = $rate_hour;
			$spi_ersrecord->rate_day    = $rate_day;
			$spi_ersrecord->work_hours  = $work_hours;
			$spi_ersrecord->work_days   = $work_days;
			$spi_ersrecord->plan_cash   = $plan_cash;
			$spi_ersrecord->plan_lcc    = $plan_bayanihan;
			$spi_ersrecord->actual_cash = $actual_cash;
			$spi_ersrecord->actual_lcc  = $actual_bayanihan;
			$spi_ersrecord->save();
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record_id = Input::get('in-recordid',   false);
		$affectedRows = SPIERSRecord::find($id)->delete();
		$is_successful = ($affectedRows > 0) ? TRUE : FALSE;
		Session::put("spi-ers.$record_id.destroy", $is_successful);	
	}


}
