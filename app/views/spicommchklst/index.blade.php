@extends('layouts.default')

@section('content')
	    <div class="row">
		    <div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="col-xs-10" style="padding-left: 0;">{{{$program}}} <small>Program</small></h4>
						@if($position->position=='ACT')
                    <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                    <span class="hidden module">spi-comm</span>
             @endif
						<a id="spi-ocicomm_modal-add_show" class="btn btn-primary pull-right" 
							href="">
							<i class="fa fa-plus"></i> Add New
						</a>

						<div class="clearfix"></div>
					</div>
					<div class="panel-body">
						@if ($store_status !== null)
							@if ($store_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> A New Committee was successfully added!</strong>
								</div>
							@elseif ($store_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Committee can not be added</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if ($update_status !== null)
							@if ($update_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> Committee was successfully updated!</strong>
								</div>
							@elseif ($update_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Committee can not be updated</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if ($destroy_status !== null)
							@if ($destroy_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> Committee was successfully removed!</strong>
								</div>
							@elseif ($destroy_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Committee can not be removed</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						<table class="table table-striped table-bordered" id="spi-commtschklst_table">
							<thead>
								<tr>
									@if($position->position=='ACT')
								    <th>Select all
								        <input type="checkbox" id="selecctall"/>
								    </th>
								    @endif
									<th style="width: 25%;">Committee</th>
									<th style="width: 12.5%;">Male members</th>
									<th style="width: 12.5%;">Female members</th>
									<th style="width: 15%;">Date Organized</th>
									<th style="width: 15%;">Regular Meetings Conducted(Frequency of Meeting)</th>
									<th style="width: 15%; text-align: center;">Action</th>
								</tr>
							</thead>
							<tbody>
							@foreach ($committees as $committee)
								<tr data-rowid="{{{$committee['id']}}}" data-commname="{{$committee['comm_name']}}">
                              @if($position->position=='ACT')
									<td>
                                  @if($committee->is_draft==0)
                                    {{ $reference->approval_status($committee->id, 'ACT') }}
                                  @else
                                      <span class="label label-warning">draft</span>
                                  @endif
                            </td>
                                @endif

									<td class="cell-commname" data-value="{{$committee['comm_name']}}">{{$committee['comm_name']}}</td>
									<td class="cell-nomale" data-value="{{$committee['no_male']}}" style="text-align: center;">
										{{$committee['no_male']}}
									</td>
									<td class="cell-nofemale" data-value="{{$committee['no_female']}}" style="text-align: center;">
										{{$committee['no_female']}}
									</td>
									<td class="cell-dateorg" data-value="{{date("M/d/Y", strtotime($committee['date_org']))}}">
										{{ toDate($committee['date_org'])}}
									</td>
									<td class="cell-freq_meetings" data-value="{{$committee['freq_meetings']}}">
										{{$committee['freq_meetings']}}
									</td>

										<td style="text-align: center">
	    							@if( !is_review($committee->id) )
											<a href="" class="btn btn-sm btn-info spi-ocicomm_modal-edit_show" style="margin-right: 0.5rem;"><i class="glyphicon glyphicon-edit"></i> Edit</a>
											<a href="" class="btn btn-sm btn-danger spi-ocicomm_modal-del_show"><i class="glyphicon glyphicon-trash"></i> Delete</a>
									@endif	
										</td>	
				
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<!-- lets a
			<a href="" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-pencil"></i></a>dd show modal ref to show url -->
		<div class="modal fade" id="spi-ocicomm_modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add Committee to program</h4>
                    </div>
                    <div class="modal-body">
						{{ Form::open(array('id' => 'spi-ocicomm_modal_form', 'route' => 'spi-comm.store')) }}
							<input type="hidden" id="in-programid" name="in-programid" value="{{{$program}}}">
							<input type="hidden" id="in-cycleid" name="in-cycleid" value="{{{$cycle}}}">
							<input type="hidden" id="in-lgupsgc" name="in-lgupsgc" value="{{{$lgu_psgc}}}">
							<input type="hidden" id="in-lgucovered" name="in-lgucovered" value="{{{$lgu_covered}}}">

							<div class="form-group">
								<label for="in-province">Province</label>
								<input type="text" id="in-province" name="in-province" class="form-control" 
									value="{{{$province}}}" readonly="readonly">
							</div>
							<div class="form-group">
								<label for="in-municipalityname">Municipality</label>
								<input type="text" id="in-municipalityname" name="in-municipalityname" class="form-control"
									value="{{{$municipality}}}" readonly="readonly">
								<input type="hidden" id="in-municipalitypsgc" name="in-municipalitypsgc" value="{{{$municipality_psgc}}}">
							</div>

							<div class="form-group">
								<label for="in-municipalityname">Region</label>
								<input type="text" id="in-region" name="in-region" class="form-control"
									value="{{{$region}}}" readonly="readonly">
								<input type="hidden" id="in-region" name="in-region" value="{{{$region}}}">
							</div>

						  <div class="form-group">
							<label for="in-commname">Committee Name</label>
							<input type="text" class="form-control" placeholder="What is the committee name?"
								   id="in-commname" name="in-commname">
						  </div>

						  <div class="form-group">
							<label for="in-malenum">No. of Male members</label>
							<input type="number" class="form-control" placeholder="How many Male members?"
								   id="in-malenum" name="in-malenum">
						  </div>

						  <div class="form-group">
							<label for="in-femalenum">No. of Female members</label>
							<input type="number" class="form-control" placeholder="How many Female members?"
								   id="in-femalenum" name="in-femalenum">
						  </div>

						  <div class="form-group">
							<label for="in-dateorg_cont">Date Organized</label>
							<div class='input-group date' id='in-dateorg_cont'>
				                <input class="form-control" required="required" type="text" 
				                	   id="in-dateorg" name="in-dateorg" value="{{date('m/d/Y')}}"> <br>
				                <span class="input-group-addon"><span class="fa fa-calendar">
				                      </span>
				                </span>
				            </div>
						  </div>

						  <div class="form-group">
							<label for="in-regmeetings">Regular Meetings conducted(Frequency of Meeting)</label>
							<!-- <textarea rows="3" class="form-control" placeholder="Reular meetings are conducted ..."
									  id="in-regmeetings" name="in-regmeetings"> -->
							<textarea class="form-control" rows="5" id="in-regmeetings" name="in-regmeetings" placeholder="Regular meetings are conducted .."></textarea>		  
							</textarea>
						  </div>
						{{ Form::close() }}
					</div>
					<div class="modal-footer">
						<a id="spi-ocicomm_modal_form_submit" href="" class="btn btn-primary edit-url">Submit</a>
						<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
					</div>
	            </div>
            </div>
        </div>

		<div class="modal fade" id="spi-ocicomm_modal-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
						<form>
							<input type="hidden" id="del-id" name="del-id" value="">
							<p class="col-xs-2" style="text-align: right;">
								<i class="glyphicon glyphicon-question-sign" style="color: #f4543c; font-size: 50px;"></i>
							</p>
							<p class="col-xs-10" style="font-size: 15px; padding-top: 15px; padding-bottom: 15px;">Are you sure you want to delete 
								<em id="del-commname"></em>
								 to current program?</p>
							<div class="clearfix"></div>
						</form>
					</div>
					<div class="modal-footer">
						<a id="spi-ocicomm_modal_delform_submit" href="" class="btn btn-danger">Yes</a>
						<a href="" class="btn bg-navy" data-dismiss="modal">No</a>
					</div>
	            </div>
            </div>
        </div>

		<div class="modal fade" id="spi-ocicomm_modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Update Committee details</h4>
                    </div>
                    <div class="modal-body">
						<form id="spi-ocicomm_editmodal_form">
							<input type="hidden" id="edit-id" name="edit-id" value="">
						  <div class="form-group">
							<label for="edit-commname">Committee Name</label>
							<input type="text" class="form-control" placeholder="What is the committee name?"
								   id="edit-commname" name="edit-commname">
						  </div>

						  <div class="form-group">
							<label for="edit-malenum">No. of Male members</label>
							<input type="number" class="form-control" placeholder="How many Male members?"
								   id="edit-malenum" name="edit-malenum">
						  </div>

						  <div class="form-group">
							<label for="edit-femalenum">No. of Female members</label>
							<input type="number" class="form-control" placeholder="How many Female members?"
								   id="edit-femalenum" name="edit-femalenum">
						  </div>

						  <div class="form-group">
							<label for="edit-dateorg_cont">Date Prepared</label>
							<div class='input-group date' id='in-dateorg_cont'>
				                <input class="form-control" required="required" type="text" 
				                	   id="edit-dateorg" name="edit-dateorg" value=""> <br>
				                <span class="input-group-addon"><span class="fa fa-calendar">
				                      </span>
				                </span>
				            </div>
						  </div>

						  <div class="form-group">
							<label for="edit-regmeetings">Regular Meetings conducted(Frequency of Meeting)</label>
							<textarea rows="3" class="form-control" placeholder="Reular meetings are conducted ..."
									  id="edit-regmeetings" name="edit-regmeetings">
							</textarea>
						  </div>
						</form>
					</div>
					<div class="modal-footer">
						<a id="spi-ocicomm_modal_editform_submit" href="" class="btn btn-primary"> <i class="fa fa-save"></i>  Submit</a>
						<a href="" class="btn btn-default" data-dismiss="modal">Cancel</a>
					</div>
	            </div>
            </div>
        </div>


	<script>
      $(document).ready(function () {
        $('#spi-commtschklst_table').dataTable();

		$('#in-malenum, #edit-malenum, #in-femalenum, #edit-femalenum').keydown(function(event) {
				if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
				}
				else {
					if (event.keyCode < 48 || event.keyCode > 57 ) {
						event.preventDefault();	
					}	
				}
			});
        /*
         *  ADDING a committee
         */
		$('#spi-ocicomm_modal-add_show').on('click', function(event)
		{
			$('#spi-ocicomm_modal-add').modal('show');
			return false;
		});

		$('#spi-ocicomm_modal_form_submit').on('click', function()
		{
			//$('#spi-ocicomm_modal_form').submit();
			$.post( window.location.href, $( "#spi-ocicomm_modal_form" ).serialize())
			.done(function(msg){
				$('#spi-ocicomm_modal-add').modal('hide');
				window.location = window.location.href;
			});

			return false;
		});
		/* end of ADDING a committee */
        /*
         *  UPDATING a committee
         */
		$('.spi-ocicomm_modal-edit_show').on('click', function(event){
			var $row = $(event.currentTarget).parents('tr');
			$('#edit-id').val($row.data('rowid'));
			$('#edit-commname').val($row.find('td.cell-commname').data('value'));
			$('#edit-malenum').val($row.find('td.cell-nomale').data('value'));
			$('#edit-femalenum').val($row.find('td.cell-nofemale').data('value'));									
			$('#edit-dateorg').val($row.find('td.cell-dateorg').data('value'));
			$('#edit-regmeetings').val($row.find('td.cell-freq_meetings').data('value'));

			$('#spi-ocicomm_modal-edit').modal('show');
			return false;
      	});		

		$('#spi-ocicomm_modal_editform_submit').on('click', function(event)
		{
			var $url = window.location.href + '/' + $('#edit-id').val();
			console.log($url);
			$.ajax({
		  		type: "PUT",
		  		url:  $url,
		  		data: $( "#spi-ocicomm_editmodal_form" ).serialize()
			})
		  	.done(function( msg ) {
		    	$('#spi-ocicomm_modal-edit').modal('hide');
		    	window.location = window.location.href;
		  	});

			return false;
		});
		/* end of UPDATING a committee */
        /*
         *  DELETING a committee
         */
		$('.spi-ocicomm_modal-del_show').on('click', function(event){
			var id = $(event.currentTarget).parents('tr').data('rowid');
			$('#del-id').val(id);
			var commname = $(event.currentTarget).parents('tr').data('commname');
			$('#del-commname').text(commname);
			$('#spi-ocicomm_modal-del').modal('show');
			return false;
      	});

		$('#spi-ocicomm_modal_delform_submit').on('click', function(event)
		{
			var $url = window.location.href + '/' + $('#del-id').val();
			console.log($url);
			$.ajax({
		  		type: "DELETE",
		  		url: $url
			})
		  	.done(function( msg ) {
		    	$('#spi-ocicomm_modal-del').modal('hide');
		    	window.location = window.location.href;
		  	});

			return false;
		});
		/* end of DELETING a committee */
	});
    </script>
@stop