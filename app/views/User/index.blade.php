@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
<div class="col-md-12">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">User</h3>
            <div class="box-tools pull-right">
               <a href="{{ URL::to('user/create'); }}">
                <span class="btn btn-primary" >Add User</span>
               </a>
            </div>
        </div>
        <div class="box-primary">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>Position</td>
                        <td>Name</td>
                        <td>Username</td>
                        <td>Date Added</td>
                        <td>Option</td>
                    </tr>
                </thead>

                   @foreach($users as $user)
                       <tr>
                            <td>{{ $user->position }}</td>
                            <td>{{ $user->fullname() }}
                            <td>{{ $user->username }}</td>
                            <td>{{ toDate($user->created_at) }}</td>
                            <td>
                                <a class="btn btn-info" href="{{ URL::to('user/'.$user->id.'/edit') }}">Edit</a>
                                <span class="btn btn-danger delete" data-id="{{$user->id}}">delete</span>
                            </td>
                       </tr>
                   @endforeach

            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.delete').on('click',function(){
            var con = confirm('are you sure that do you want to delete this user');
            var id = $(this).data('id');
            if(con){
                $.post('user/'+id,{'_method':'delete'},function(){
                    alert('success');
                    window.location.reload();
                });
            }
        });
    });
</script>
@stop
