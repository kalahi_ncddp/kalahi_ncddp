@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
<div class="col-md-12">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit</h3>
        </div>
        <div class="box-primary">
            <div class="col-md-12">

                {{ Form::open(array('action' => array('UserManagementController@update',$user->id), 'method' => 'PUT')) }}
                    <div class="form-group">
                        <label for="">Username</label>
                        {{ Form::text('username',$user->username,array('class'=>'form-control')) }}
                    </div>
                    <div class="form-group">
                        <label for="">first name</label>
                        {{ Form::text('first_name',$user->first_name,array('class'=>'form-control')) }}
                    </div>

                    <div class="form-group">
                        <label for="">last name</label>
                        {{ Form::text('last_name',$user->last_name,array('class'=>'form-control')) }}
                    </div>

                    <div class="form-group">
                        <label for="">middle name</label>
                        {{ Form::text('middle_name',$user->middle_name,array('class'=>'form-control')) }}
                    </div>

                    <div class="form-group">
                        <label for="">new password (leave blank if you don't want to change the password)</label>
                        {{ Form::password('new_password',array('class'=>'form-control','placeholder'=>'Enter your new password')) }}
                    </div>

                    <div class="form-group">
                        <label for="">Role</label>

                        {{ Form::select('position',$positions,$user->position| '' ,array('class'=>'form-control')) }}

                    </div>

                    <div class="form-group">
                        <label for=""></label>
                        {{ Form::select('psgc_id',$municipalities,$user->psgc_id,array('class'=>'form-control hidden','hidden')) }}
                    </div>

                    <div class="form-group">
                        <label for=""></label>
                    </div>
                    <div class="form-group">
                        {{ Form::submit('update',array('class'=>'btn btn-primary')); }}
                        <a class="btn bg-navy" href="{{ URL::to('user') }}">Close</a>
                    </div>
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop
