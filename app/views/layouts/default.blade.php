<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
     <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
     <title>{{ isset($title)? $title : 'KC-NCDDP Database System' }}</title>

      <!-- BOOTSTRAP STYLES -->

    <link href="{{ URL::to('public') }}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('public') }}/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ URL::to('public') }}/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="{{ URL::to('public') }}/css/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="{{ URL::to('public') }}/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="{{ URL::to('public') }}/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="{{ URL::to('public') }}/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="{{ URL::to('public') }}/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ URL::to('public') }}/css/AdminLTE.css" rel="stylesheet" type="text/css" />

     <link href="{{ URL::to('public') }}/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="{{ URL::to('public') }}/css/custom.css" rel="stylesheet" type="text/css" />
    
    <link href="{{ URL::to('public') }}/css/custom/styles.css" rel="stylesheet" type="text/css" />



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="{{ URL::to('public')}}/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="{{ URL::to('public')}}/js/bootstrap.min.js" type="text/javascript"></script>
  </head>

<body class="skin-black">
    <div  id="wrapper" class="wrapper row-offcanvas row-offcanvas-left">
      <div class="navbar navbar-default navbar-cls-top role="navigation" style="margin-bottom: 0"">
          <div class="navbar-header">
              <a href="{{ URL::to('/') }}" class="navbar-brand logo">KC-NCDDP v{{ isset($version) ? $version : ' test' }}</a>
                <span class="hidden" id="url">{{ URL::to('/') }}</span>
              <!-- <img src="{{ URL::to('public/img/KALAHI_CIDSS-NCDDP_Logo3.jpg') }}" class="user-image img-responsive"/> -->
              <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
          </div>
          <div class="navbar-collapse collapse" id="navbar-main">

            <div class="navbar-left">
              <ul class="nav navbar-nav">
                    <li class="dropdown">
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Social Preparation <span class="caret"></span></a>
                          <ul class="dropdown-menu" aria-labelledby="themes">
                              <li class="treeview">
                                  <a href="{{ URL::to('ceac/monitoring') }}" class="trigger ">CEAC Monitoring with ACT Plans</a>
                              </li>
                              <li> <a href="{{ URL::to('brgyprofile') }}"> Brgy profile</a> </li>

                              <li><a href="{{ URL::to('muniprofile') }}"> Municipal profile</a> </li>

                                 <li><a href="{{ URL::to('lguengagement') }}"> PTA Integration Plans Checklist</a> </li>

                              <li><a href="{{ URL::to('bassembly') }}">Barangay Assembly</a></li>

                              <li><a href="{{ URL::to('volunteer') }}"> Volunteer Profile</a></li>

                              <!-- <li><a href="{{ URL::to('comm_trainings') }}">Community Training</a></li> -->
                              <li><a href="{{ URL::to('mun_trainings') }}">Municipal Training</a></li>
                              <li><a href="{{ URL::to('brgy_trainings') }}">Community Training</a></li>
                              <li><a href="{{ URL::to('psa') }}">Barangay Participatory Situation Analysis</a></li>
                              <li><a href="{{ URL::to('mibf') }}">  {{ Session::get('accelerated') == 1? 'Municipal Forum' : 'Municipal Inter Barangay Forum (MIBF)'; }}</a></li>
                              <li><a href="{{ URL::to('mlprap') }}"> Municipal Local Poverty Reduction Action Plan (MLPRAP)</a></li>
                              <li><a href="{{ URL::to('grs-barangay') }}"> GRS Barangay Installation</a></li>
                              <li> <a href="{{ URL::to('grs-municipality') }}"> GRS Municipality Installation</a> </li>
                              <li> <a href="{{ URL::to('grs-intake') }}"> GRS Intake</a> </li>
                          </ul>
                    </li>
                    <li class="dropdown ">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Sub Project Implementation <span class="caret"></span></a>
                      <ul class="dropdown-menu" aria-labelledby="themes">
                        <li><a href="{{ URL::to('spi_profile') }}">Sub Project Profile</a></li>
                        <li><a href="{{ URL::to('mlcc') }}">Municipal Consolidated Status of LCC</a></li>
                         <li><a href="{{ URL::to('spi-comm') }}">Oversight and Coordinating Committees Checklist</a></li>
                        <li><a href="{{ URL::to('spi-subworkers') }}">CDD Sub-Project ERS</a></li>
                        <li><a href="{{ URL::to('spi-munsusplan') }}">Municipal Sustainability Plan</a></li>
                        <li><a href="{{ URL::to('spi-techassist') }}">Technical Assistance</a></li>
                        <li><a href="{{ URL::to('spi-fnddpsa') }}">Funded PSA Priorities</a></li>
                        <li><a href="{{ URL::to('spi-accomplishment') }}">Sub-Project Accomplishment Report</a></li>
                        <li><a href="{{ URL::to('spi-disaster') }}">Damaged Sub-Project by Disaster </a></li>
                      </ul>
                    </li>
                    
                    @if($position->position=='ACT')
                    <li class="dropdown">
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Admin<span class="caret"></span></a>

                           <ul class="dropdown-menu" aria-labelledby="themes">
                               <li><a class="download" href="{{ URL::to('download/json') }}">Export Data</a></li>
                               <li><a href="{{ URL::to('upload/json') }}">Synchronize Data</a></li>

                             <li><a href="{{ URL::to('user') }}">User Administration</a></li>
                             <li><a href="{{ URL::to('retrieve-comment') }}">Update Comments</a></li>
                            <li>
                              <a href="{{ URL::to('http://localhost/reports_kalahi_ncddp') }}">Reports</a>
                            </li>
                          </ul>
                         
                    </li>
                   
                  @endif



              </ul>
              
            </div>
            <div class="navbar-right">
              
              <ul class="nav navbar-nav navbar-right">
                  <li class="{{ Session::get('accelerated') == 1? 'accelerated' : 'regular' }}"><span >{{ Session::get('accelerated') == 1? 'Accelerated' : 'Regular' }}</span></li>
                  <li style=" padding: 15px 10px;">
                  <li class="dropdown notifications-menu">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <i class="fa fa-warning"></i>
                          <span class="label label-warning">{{ isset($count) ? $count : 0 }}</span>
                      </a>
                      <ul class="dropdown-menu">
                          <li class="header">There is {{ isset($count) ? $count : 0  }} comments from higher levels today</li>
                          <li>
                              <!-- inner menu: contains the actual data -->
                              <ul class="menu">
                                  @foreach($comments as $comment )
                                  <li>
                                      <a href="{{ URL::to( $comment->module_type .'/'.$comment->activity_id) }}">
                                          <i class="ion ion-ios7-people info"></i> {{ $comment->office }}
                                          {{ $comment->module_type }}

                                      </a>
                                  </li>
                                  @endforeach
                              </ul>
                          </li>
                          <li class="footer"><a href="#">..</a></li>
                      </ul>
                  </li>
                  <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-user"></i>
                            <span>{{ Report::get_staff_province($username); }}</span>
                            <span>, {{ Report::get_staff_municipality($username); }}</span>
                            <span> <i class="caret"></i></span>
                        </a>
                        <ul class="dropdown-menu">

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <span>{{ isset($position) ? $position->username.'<br>'.$position->position:'' }}</span>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ URL::to('auth/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
              </ul>

            </div>





        </div>
      </div>

      <aside class="right-side strech">

         

          <section class="content">
                      <div class="row">
                          <div class="col-xs-12">
                              @yield('attention')
                              @if(Session::has('message'))
                                <div class="alert alert-success">
                                  {{ Session::get('message') }}
                                </div>
                              @endif
                              @yield('content')
                               @if(Session::has('error'))
                                  <div class="alert alert-danger">
                                    {{ Session::get('error') }}
                                  </div>
                                @endif
                                 @if(Session::has('exportError'))
                                      <div class="alert alert-danger">
                                        {{ Session::get('exportError') }}
                                      </div>
                                    @endif
                          </div>
                      </div>
          </section>
      </aside>
    </div>
     <!-- /. WRAPPER  -->

    <!-- DATA TABLE SCRIPTS -->

    <!-- modal cofirm box -->
     @include('modals.confirm')
     @include('modals.unreview')

     @if(Session::get('modals'))
         @include(('modals.notice'))
         <script>
            $('#notice').modal('show');
         </script>
     @endif

     @if(Session::get('update_volunteer'))
        <script>
          var conf = confirm(' The system detected that you have migrated from 1.4.x which addresses the voluteer cycle bug. THIS WILL CONVERT ALL CYCLES OF VOLUNTEER INTO NCDDP-1 cycle');

          if(conf){
            window.location.href = '{{ URL::to('/bugfix') }}';
          }
        </script>
     @endif
    {{ HTML::script('public/js/datetimepicker/moment.js') }}
    {{ HTML::script('public/js/datetimepicker/bootstrap-datetimepicker.min.js') }}

     <!-- Sparkline -->
    <script src="{{ URL::to('public')}}/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="{{ URL::to('public')}}/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="{{ URL::to('public')}}/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ URL::to('public')}}/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="{{ URL::to('public')}}/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="{{ URL::to('public')}}/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
        <!-- mask -->
    <script src="{{ URL::to('public')}}/js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
    <script src="{{ URL::to('public')}}/js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>

    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ URL::to('public')}}/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- iCheck -->
         <!-- DATA TABES SCRIPT -->
    <script src="{{ URL::to('public')}}/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="{{ URL::to('public')}}/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL::to('public')}}/js/AdminLTE/app.js" type="text/javascript"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ URL::to('public')}}/js/AdminLTE/dashboard.js" type="text/javascript"></script>
    <script src="{{ URL::to('public')}}/js/priceFormat.min.js" type="text/javascript"></script>

    {{ HTML::script('public/js/movable-columns.js') }}
    <!-- AdminLTE for demo purposes -->
    {{ HTML::script('public/js/grs.js') }}
    {{ HTML::script('public/js/custom.js') }}

    <script>
        $(document).ready(function(){
             $date = moment().format('dddd, MM/DD/YYYY');
               $('#date').text($date);
        });
    </script>
</body>
</html>