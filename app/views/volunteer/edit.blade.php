@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
  <a class="btn btn-default" href="{{ URL::to('volunteer') }}">Go Back</a>
	    {{ Form::model($volunteer, array('action' => array('VolunteerController@update', $volunteer->volunteer_id), 'method' => 'PUT')) }}
	    <div class="row">
		    <div class="col-md-12">
                    <div class="panel panel-default">
			    		<div class="panel-heading">
			    			Details
			    		</div>
						<div class="panel-body">
							<div class="table-responsive">
								 <div class="col-md-6">
						
								<table class="table table-striped table-bordered table-hover">
								 	<tr>
						                <th>Barangay</th>
						                <td>{{ $barangay = Report::get_barangay($volunteer->psgc_id) }}</td>
						            </tr>
						            <tr>
						                <th>KC Code</th>
						                <td></td>
						            </tr>
						            <tr>
						                <th>Municipality</th>
						                <td>{{ $municipality = Report::get_municipality_by_brgy_id($volunteer->psgc_id) }}</td>
						            </tr>
						            <tr>
						                <th>Province</th>
						                <td>{{ $province = Report::get_province_by_brgy_id($volunteer->psgc_id) }}</td>
						            </tr>
						        </table>
						    </div>
						    	 <div class="col-md-6">
						
								<table class="table table-striped table-bordered table-hover">
								 	

						            <tr>
						                <th>Region</th>
						                <td>{{ $region = Report::get_region_by_brgy_id($volunteer->psgc_id) }}</td>
						            </tr>
						            
						           
						             <tr>
						                <th>Cycle</th>
						                <td>{{ $volunteer->cycle_id }}</td>
						            </tr>
						              <tr>
						                <th>Program</th>
						                <td>{{ $volunteer->program_id }}</td>
						            </tr>
						            
						             <tr>
						                <th>Date Appointed</th>
						                <td>{{ Form::input('text', 'date_appointed', toDate($volunteer->date_appointed) , array('class' => ' date form-control', 'max' => date("Y-m-d"), 'required')) }}
										</td>
						            </tr>
						            <!--
						             <tr>
						                <th>Sector Represented</th>
						                <td>{{ Form::select('sector_represented', $sector_list, $volunteer->sector_represented, array('class' => 'form-control', 'required')) }}
										</td>
						            </tr>
						-->
								</table>
							</div>
						</div>
					</div>
															
				<div class="panel panel-default">
					<div class="panel-heading">
						Sectors Represented
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
							@foreach($sector_list as $sector)
									@if(in_array($sector, $sector_data))
										<div class = "form-control"><input type="checkbox" name="sector[]" value="{{$sector}}" checked>&nbsp{{$sector}}</div>
									@else
										<div class = "form-control"><input type="checkbox" name="sector[]" value="{{$sector}}">&nbsp{{$sector}}</div>
								@endif
							@endforeach
							</table>
						</div>
					</div>
				</div>
							
						
						
						
						

							
				
				<div class="hidden">
					{{ Form::text('beneficiary_id', $volunteer->beneficiary_id, array('class' => 'form-control', 'id' => 'beneficiary_id')) }}
				</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Volunteer Profile
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                            		 <div class="col-md-6">
						
                                <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <th>Last Name</th>
                                        <td>{{ Form::text('lastname', $beneficiary->lastname, array('class' => 'form-control', 'id' => 'lastname', 'placeholder' => 'Last Name', 'required')) }}</td>
                                    </tr>
                                    <tr>
                                        <th>First Name</th>
                                        <td>{{ Form::text('firstname',  $beneficiary->firstname, array('class' => 'form-control', 'id' => 'firstname', 'placeholder' => 'First Name', 'required')) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Middle Name</th>
                                        <td>{{ Form::text('middlename',  $beneficiary->middlename, array('class' => 'form-control','maxlength'=>'4',  'id' => 'middlename', 'placeholder' => 'Middle Name' )) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Birthdate</th>
                                        <td>{{ Form::input('text', 'birthdate', toDate($beneficiary->birthdate), array('class' => 'form-control date', 'id' => 'birthdate', 'required')) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Age</th>
                                        <td>{{ Form::text('age', Input::old('age'), array('class' => 'form-control', 'id' => 'age', 'disabled')) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Sex
                                        </th>
                                        <td>{{ Form::select('sex', array('M' => 'Male', 'F' => 'Female'), $beneficiary->sex[0], array('class' => 'form-control', 'id' => 'sex', 'required')) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Civil Status</th>
                                        <td>{{ Form::select('civil_status', $civil_status_list, $beneficiary->civil_status, array('class' => 'form-control', 'id' => 'civil_status', 'required')) }}</td>
                                    </tr>
                                </table>
                            </div>

                            	 <div class="col-md-6">
						
								<table class="table table-striped table-bordered table-hover">
								 	<tr>
                                    <tr>
                                        <th>Number of Children</th>
                                        <td>{{ Form::input('number', 'no_children', $beneficiary->no_children, array('class' => 'form-control', 'id' => 'no_children', 'required', 'min' => '0')) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Address</th>
                                        <td>{{ Form::text('address', $beneficiary->address, array('class' => 'form-control', 'id' => 'address')) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Contact Number</th>
                                        <td>{{ Form::text('contact_no', $beneficiary->contact_no, array('class' => 'form-control', 'id' => 'contact_no')) }}</td>
                                    </tr>
                                	<tr>
                                        <th>Current position in BLGU</th>
                                        <td>{{ Form::select('current_position_blgu', $position_blgu, $beneficiary->current_position_blgu, array('class' => 'form-control', 'id' => 'current_position_blgu')) }}</td>
                                    </tr> 
                                    <tr>
                                        <th>Educational Attainment</th>
                                        <td>{{ Form::select('educ_attainment', $educ_level_list, $beneficiary->educ_attainment, array('class' => 'form-control', 'id' => 'educ_attainment')) }}</td>


                                    </tr>
                                    <tr>
                                        <th>Occupation</th>
                                        <td>{{ Form::text('occupation', $beneficiary->occupation, array('class' => 'form-control', 'id' => 'occupation')) }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>


                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <tr>
                                        <td>
                                            {{ Form::checkbox('is_ip', '1', $beneficiary->is_ip, array( 'id' => 'is_ip')) }}
                                            IP

                                            {{ Form::checkbox('is_ipleader', '1', $beneficiary->is_ipleader, array( 'id' => 'is_ipleader','disabled')) }}
                                            IP Leader
                                        </td>
                                        <td>
                                            {{ Form::checkbox('is_ppbene', '1', $volunteer->is_ppbene, array( 'id' => 'is_ppbene')) }}
                                            PP Beneficiary

                                            {{ Form::checkbox('is_ppleader', '1', $volunteer->is_ppleader, array( 'id' => 'is_ppleader', 'disabled')) }}
                                            PP Leader

                                        </td>
                                        <td>

                                            {{ Form::checkbox('is_slpbene', '1', $volunteer->is_slpbene, array( 'id' => 'is_slpbene')) }}
                                            SLP Beneficiary

                                            {{ Form::checkbox('is_slpleader', '1', $volunteer->is_slpleader, array( 'id' => 'is_slpleader','disabled')) }}
                                            SLP Leader

                                        </td>
                                        <tr>

                                        <tr>
                                        <td>
                                            {{ Form::checkbox('is_bspmc', '1', $volunteer->is_bspmc, array( 'id' => 'is_bspmc')) }}
                                            BSPMC  Chair

                                        </td>
                                        <td>
                                            Start Date: {{ Form::input('text', 'aschair_start', toDate($volunteer->aschair_start), array('class' => ' date form-control', 'id' => 'aschair_start')) }}

                                        </td>
                                        <td>
                                            End Date: {{ Form::input('text', 'aschair_end', toDate($volunteer->aschair_end), array('class' => ' date form-control', 'id' => 'aschair_end')) }}

                                        </td>
                                        <tr>
                                    </table>
                                </div>
                            </div>
                        </div>
															
<?php $count=0; ?>
				
				<div class="panel panel-default">
					<div class="panel-heading">
						KC-NCDDP Committee Membership
		    		</div>
					<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<button id="add" type="button" >Add New Entry</button>
							<br>
							<div class="hidden"> 
								<div id="committee_option">
									@foreach($volcomm_list as $volcomm_id => $volcomm)
										<option value="{{$volcomm_id}}">{{$volcomm}}</option>
									@endforeach
								</div>
								<div id="position_option">
									@foreach($volpost_list as $volpost)
										<option value="{{$volpost}}">{{$volpost}}</option>
									@endforeach
								</div>
							</div>


								<table id="items" class="table table-bordered">
								    <thead>
								        <tr>
								            <th>Committee</th>
								            <th>Position</th>
								            <th>Start</th>
								            <th>End</th>
								            <th>Options</th>
								        </tr>
								    </thead>
								    <tbody>



								@foreach($committee_data as $comm)
								  <tr>
								    <td>
								        {{ Form::select('committee[]', $volcomm_list, $comm->committee, array('class' => 'form-control', 'placeholder' => 'Committee', 'onchange'=>'checkduplicate2(this)', 'id'=>$count, 'required')) }}
								    </td>
								    <td>
								        {{ Form::select('position[]', $volpost_list, $comm->position, array('class' => 'form-control', 'placeholder' => 'Position', 'onchange'=>'checkduplicate2(this)', 'id'=>'position:'.$count, 'required')) }}
								    </td>
								    <td>
								        <input type="text" class="form-control date" name="startdate[]" value="{{ toDate($comm->startdate) }}" id="startdate:{{$count}}" onchange="setStartLimit1(this)">
								    </td>
								    <td>
								        <input type="text" class="form-control date" name="enddate[]" value="{{ toDate($comm->enddate)}}" id="enddate:{{$count}}" onchange="setEndLimit1(this)">
								    </td>
								    <td>
                                            <button type="button" class="delete"><i class="fa fa-trash-o"></i></button>
								    </td>
								   </tr>
								<? $count++ ?>
								@endforeach
								 </tbody>
                              </table>

							
					    </table>

					</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading">
						Previous Trainings Attended (NON KC-NCDDP)
		    		</div>
					<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<button id="add1" type="button" >Add New Entry</button>
							<br>
							<div id="items1">
								@foreach($training_data as $training)
								<div class="input-group">
									<div class="form-inline">
										<input type="text" class="form-control" name="training_title[]" value="{{$training->training_title}}" onchange="checkduplicate(this)" placeholder="Training Title" id="{{ $count }}" required>
										<input type="text" class="form-control" name="training_prov[]" value="{{$training->training_prov}}" placeholder="Training Provider" id="provider:{{ $count }}" onchange="checkduplicate(this)" required>
										<input type="number" class="form-control" name="year_attended[]" value="{{$training->year_attended}}" placeholder="Year Attended" id="year:{{ $count }}" min="0" max="{{ date('Y') }}" onchange="checkduplicate(this)" required>
									</div>
									<div class="input-group-addon"><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div>
									<br>
								</div>
								<? $count++ ?>
								@endforeach
							</div>
						</table>
					</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading">
						Other Organization (NON KC-NCDDP)
		    		</div>
					<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<button id="add2" type="button" >Add New Entry</button>
							<br>
							<div id="items2">
								@foreach($org_data as $org)
								<div class="input-group">
									<div class="form-inline">
										<input type="text" class="form-control" name="org_name[]" value="{{$org->org_name}}" onchange="checkduplicate1(this)" placeholder="Committee" id="{{ $count }}" required>
										<input type="text" class="form-control" name="position_org[]" value="{{$org->position}}" placeholder="Position" id="position_org:{{ $count }}" onchange="checkduplicate1(this)" required>
										</div>
									<div class="input-group-addon"><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div>
									<br>
								</div>
								<? $count++; ?>
								@endforeach
							</div>
						</table>
					</div>
					</div>
				</div>
				<div class="hidden" id="count">{{ $count }}</div>																																			
			</div>
		</div>
				<div class="form-group pull-right">
								<label>Save as draft</label>
					<input type="checkbox" name="is_draft" {{ $volunteer->is_draft==1 ? 'checked': '' }}>
				</div>								
				<!-- {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
                 <a class="btn bg-navy" href="{{ URL::to('volunteer') }}">Close</a> -->
                   <div class="col-md-12 ">
									<div class="form-group pull-right">
						                <button class="btn btn-primary" id="save">
											<i class="fa fa-save"></i>
											Submit 
										</button>
		                 				<a class="btn btn-default" href="{{ URL::to('volunteer') }}">Cancel</a>		
									</div>
								</div>
								<div class="clearfix"></div>
				{{ Form::close() }}
		
		<script>
			$(document).ready(function(){
				var count = $("#count").text();
				 $('.date').datetimepicker({pick12HourFormat: false, pickTime:false});
				$("#add").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items").find('tbody').append('<tr><td><select class="form-control" placeholder="Committee" id="'+count+'" required="required" name="committee[]" onchange="checkduplicate2(this)">'+$("#committee_option").html() +'</select></td><td><select class="form-control" placeholder="Committee" id="position:'+count+'" required="required" name="position[]" onchange="checkduplicate2(this)">'+$("#position_option").html() +'</select></td><td><input type="text" class="form-control date" id="startdate:'+count+'" name="startdate[]" onchange="setStartLimit1(this)" required></td><td><input type="text" class="form-control date" id="enddate:'+count+'" name="enddate[]" onchange="setEndLimit1(this)" ></td><td><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></td></tr>');
					 $('.date').datetimepicker({pick12HourFormat: false, pickTime:false});
					count++;

					 $('input[name="startdate[]"]').change(function(){
				      	var elem = $(this);
				      	// console.log(elem.parent().find('input[name="enddate[]"]').val());
	        			elem.parent().parent().find('input[name="enddate[]"]').data("DateTimePicker").setMinDate(new Date(elem.val() ));
	   			 	  }).change();
				});
				
				$("#add1").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items1").append('<div class="input-group"><div class="form-inline"><input type="text" class="form-control" name="training_title[]" onchange="checkduplicate(this)" placeholder="Training Title" id="'+count+'" required><input type="text" class="form-control" name="training_prov[]" placeholder="Training Provider" id="provider:'+count+'" onchange="checkduplicate(this)" required><input type="number" class="form-control" name="year_attended[]" placeholder="Year Attended" id="year:'+count+'" min="1980" max="'+(new Date).getFullYear()+'" onchange="checkduplicate(this)" required></div><div class="input-group-addon"><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div><br></div>');
					count++;
				});
				
				$("#add2").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items2").append('<div class="input-group"><div class="form-inline"><input type="text" class="form-control" name="org_name[]" onchange="checkduplicate1(this)" placeholder="Committee" id="'+count+'" required><input type="text" class="form-control" name="position_org[]" placeholder="Position" id="position_org:'+count+'" onchange="checkduplicate1(this)" required></div><div class="input-group-addon"><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div><br></div>');
					count++;
				});
				
				$("body").on("click", ".delete", function (e) {
					$(this).parent("div").parent("div").remove();
					$(this).parent("td").parent("tr").remove();

				});

				   $('input[name=aschair_start]').change(function(){
	       				 $('input[name=aschair_end]').data("DateTimePicker").setMinDate(new Date($('input[name=aschair_start]').val() ));
	   			 }).change();


				      $('input[name="startdate[]"]').change(function(){
				      	var elem = $(this);
				      	// console.log(elem.parent().find('input[name="enddate[]"]').val());
	        			elem.parent().parent().find('input[name="enddate[]"]').data("DateTimePicker").setMinDate(new Date(elem.val() ));
	   			 	  }).change();
				  // $('input[name=aschair_start]').data("DateTimePicker").setMinDate(new Date($('input[name=aschair_]').val() ));

				// $("input[name=aschair_start]").change(function(){
				// 	var start = $("input[name=aschair_start]").val();
				// 	var end =  $("input[name=aschair_end]").val();
				// 	 $("input[name=aschair_start]").date
				// 	if(start >= end) {
				// 		start[0].setCustomValidity('The start date must be less tha');
				// 	}
				// });
			
				



				$("#is_ip").change(function(){
					if($("#is_ip").is(":checked")){
						$("#is_ipleader").removeAttr('disabled');
					}
					else{
						$("#is_ipleader").attr('disabled','disabled');
						$("#is_ipleader").prop('checked', false);	
					}
				}).change();
				
				
				$("#is_ppbene").change(function(){
					if($("#is_ppbene").is(":checked")){
						$("#is_ppleader").removeAttr('disabled');
					}
					else{
						$("#is_ppleader").attr('disabled','disabled');
						$("#is_ppleader").prop('checked', false);	
					}
				}).change();
				
				$("#is_slpbene").change(function(){
					if($("#is_slpbene").is(":checked")){
						$("#is_slpleader").removeAttr('disabled');
					}
					else{
						$("#is_slpleader").attr('disabled','disabled');
						$("#is_slpleader").prop('checked', false);	
					}
				}).change();
				
					$("#is_bspmc").change(function(){
					if($("#is_bspmc").is(":checked")){
						$("#aschair_start").removeAttr('disabled');
						$("#aschair_end").removeAttr('disabled');
						$('.date').datetimepicker({pick12HourFormat: false, pickTime:false});
					}
					else{
						$("#aschair_start").attr('disabled','disabled');
						$("#aschair_end").attr('disabled','disabled');
					}
				});
				
				$("#aschair_start").change(function (e) {
					$("#aschair_end").attr('min', $("#aschair_start").val());
				});
			
				$("#aschair_end").change(function (e) {
					$("#aschair_start").attr('max', $("#aschair_end").val());
				});
				
				
				
				setAge();
			});
			
			function setAge(){
				var today = new Date();
				var birthdate = new Date(document.getElementById("birthdate").value);
				var age = today.getFullYear() - birthdate.getFullYear();
    			var m = today.getMonth() - birthdate.getMonth();
    			if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
        			age--;
    			}
    			document.getElementById("age").value = age;
    			
    			e = document.getElementById("birthdate");
    			if(age < 15){
    				alert(e.value);
					e.setCustomValidity("Age must be 15+");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function setEndLimit(e){
				e.previousSibling.setAttribute("max", e.value);
				checkduplicate1(e);
			}
			
			function setStartLimit(e){
				e.nextSibling.setAttribute("min", e.value);
				checkduplicate1(e);
			}
			
			function setEndLimit1(e){
				e.previousSibling.setAttribute("max", e.value);
				checkduplicate2(e);
			}
			
			function setStartLimit1(e){
				e.nextSibling.setAttribute("min", e.value);
				checkduplicate2(e);
			}
			$("#birthdate").change(function(){
                setAge();
            });
			function checkduplicate(e){
				
				var count = -1;
				var id = e.id;
				id = id.substr(id.indexOf(":") + 1);
				
				var title = document.getElementById(id);
				var provider = document.getElementById("provider:"+id).value;
				var year = document.getElementById("year:"+id).value;
				var titles = document.getElementsByName('training_title[]');
				var providers = document.getElementsByName('training_prov[]');
				var years = document.getElementsByName('year_attended[]');

				for (i=0; i<titles.length; i++) {
    				if(title.value === titles[i].value && provider === providers[i].value && year === years[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					title.setCustomValidity("Duplicate Entry not allowed");
				}else{
					title.setCustomValidity("");
				}
				
			}
			function setAge(){
                var today = new Date();
                var birthdate = new Date(document.getElementById("birthdate").value);
                var age = today.getFullYear() - birthdate.getFullYear();
                var m = today.getMonth() - birthdate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
                    age--;
                }
                document.getElementById("age").value = age;

                e = document.getElementById("birthdate");
                if(age < 15){
                    e.setCustomValidity("Age must be 15+");
                }
                else if(age >= 99){
                    e.setCustomValidity("Age must be 99 below");
                }
                else{
                    e.setCustomValidity("");
                }
            }
			function checkduplicate1(e){
				
				var count = -1;
				var id = e.id;
				id = id.substr(id.indexOf(":") + 1);
				var org = document.getElementById(id);
				var position = document.getElementById("position_org:"+id).value;
				var startdate = document.getElementById("startdate_org:"+id).value;
				var enddate = document.getElementById("enddate_org:"+id).value;
				var orgs = document.getElementsByName('org_name[]');
				var positions = document.getElementsByName('position_org[]');
				var startdates = document.getElementsByName('startdate_org[]');
				var enddates = document.getElementsByName('enddate_org[]');
				
				for (i=0; i<orgs.length; i++) {
    				if(org.value === orgs[i].value && position === positions[i].value && startdate === startdates[i].value && enddate === enddates[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					org.setCustomValidity("Duplicate Entry not allowed");
				}else{
					org.setCustomValidity("");
				}
				
				
			}
			
			function checkduplicate2(e){
				
				var count = -1;
				var id = e.id;
				id = id.substr(id.indexOf(":") + 1);
				var comm = document.getElementById(id);
				var position = document.getElementById("position:"+id).value;
				var startdate = document.getElementById("startdate:"+id).value;
				var enddate = document.getElementById("enddate:"+id).value;
				var comms = document.getElementsByName('committee[]');
				var positions = document.getElementsByName('position[]');
				var startdates = document.getElementsByName('startdate[]');
				var enddates = document.getElementsByName('enddate[]');
				
				for (i=0; i<comms.length; i++) {
    				if(comm.value === comms[i].value && position === positions[i].value && startdate === startdates[i].value && enddate === enddates[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					comm.setCustomValidity("Duplicate Entry not allowed");
				}else{
					comm.setCustomValidity("");
				}
				
			}
		</script>																		
		
	
@stop