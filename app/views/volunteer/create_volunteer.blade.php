@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')

	<a class="btn btn-default" href="{{ URL::to('volunteer') }}">Go Back</a>
	<br>
	<br>
	    <div class="row">
		    <div class="col-md-12">
				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
					
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">
									Add Volunteer Information
								</h3>
							</div>
							<div class="box-body">
								<div class="col-md-12">
									
									<div class="form-inline">
										{{ Form::label('name', 'Name') }}
										{{ Form::text('lastname', $beneficiary->lastname, array('class' => 'form-control', 'id' => 'lastname', 'placeholder' => 'Last Name', 'disabled')) }}
										{{ Form::text('firstname',  $beneficiary->firstname, array('class' => 'form-control', 'id' => 'firstname', 'placeholder' => 'First Name', 'disabled')) }}
										{{ Form::text('middlename',  $beneficiary->middlename, array('class' => 'form-control','maxlength'=>'4', 'id' => 'middlename', 'placeholder' => 'Middle Initial', 'disabled')) }}
										
					        			<button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#beneficiary_data">
					  						View Full Profile
										</button>
									</div>
									<br>
									<div id="beneficiary_data" class="collapse">
											<div class="col-md-4">
														<div class="form-group">
															{{ Form::label('birthdate', 'Birth Date') }}
															{{ Form::input('date', 'birthdate', $beneficiary->birthdate, array('class' => 'form-control', 'id' => 'birthdate', 'disabled')) }}
														</div>
												
														<div class="form-group">
															{{ Form::label('age', 'Age') }}
															{{ Form::text('age', Input::old('age'), array('class' => 'form-control', 'id' => 'age', 'disabled')) }}
														</div>
												
														<div class="form-group">
															{{ Form::label('sex', 'Sex') }}
															{{ Form::select('sex', array('' =>'Select Gender','Male' => 'Male', 'Female' => 'Female'), $beneficiary->sex, array('class' => 'form-control', 'id' => 'sex', 'disabled')) }}
														</div>
												
											</div>
											<div class="col-md-4">

												<div class="form-group">
													{{ Form::label('civil_status', 'Civil Status') }}
													{{ Form::select('civil_status', array('' =>'Select Civil Status','Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed'), $beneficiary->civil_status, array('class' => 'form-control', 'id' => 'civil_status', 'disabled')) }}
												</div>
										
												<div class="form-group">
													{{ Form::label('no_children', 'Number of children') }}
													{{ Form::input('number', 'no_children', $beneficiary->no_children, array('class' => 'form-control', 'id' => 'no_children', 'disabled', 'min' => '0')) }}
												</div>
												<div class="form-group">
												{{ Form::label('address', 'Address') }}
												{{ Form::text('address', $beneficiary->address, array('class' => 'form-control', 'id' => 'address', 'disabled')) }}
													</div>
											
											
													
											</div>
											<div class="col-md-4">
												<div class="form-group">
													{{ Form::label('contact_no', 'Contact Number') }}
													{{ Form::text('contact_no', $beneficiary->contact_no, array('class' => 'form-control', 'id' => 'contact_no', 'disabled')) }}
												</div>

												<div class="form-group">
													{{ Form::label('educ_attainment', 'Educational Attainment') }}
													{{ Form::text('educ_attainment', $beneficiary->educ_attainment, array('class' => 'form-control', 'id' => 'educ_attainment', 'disabled')) }}
												</div>
												
												<div class="form-group">
													{{ Form::label('occupation', 'Occupation') }}
													{{ Form::text('occupation', $beneficiary->occupation, array('class' => 'form-control', 'id' => 'occupation', 'disabled')) }}
												</div>
												<div class="form-inline">
											
													{{ Form::checkbox('is_ip', '1', $beneficiary->is_ip, array( 'id' => 'is_ip', 'disabled')) }} 
													{{ Form::label('is_ip', 'IP') }}
											
													{{ Form::checkbox('is_ipleader', '1', $beneficiary->is_ipleader, array( 'id' => 'is_ipleader', 'disabled')) }} 
													{{ Form::label('is_ipleader', 'IP Leader') }}
								
												</div>
											</div>
										</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						{{ Form::open(array('url' => 'volunteer/store_volunteer')) }}
						<div class="box">
							<div class="box-body">
								<div class="row">
									<div class="col-md-6">
										<div class="hidden">
											{{ Form::text('beneficiary_id', $beneficiary->beneficiary_id, array('class' => 'form-control', 'id' => 'beneficiary_id')) }}
										</div>
										
										<div class="form-group">
											{{ Form::label('barangay', 'Barangay') }}
											{{ Form::select('barangay', $brgy_list, Input::old('barangay'), array('class' => 'form-control', 'required', 'id' => 'barangay')) }}
										</div>
										
										<div class="form-group">
											{{ Form::label('psgc_id', 'PSGC Code') }}
											{{ Form::text('psgc_id', Input::old('psgc_id'), array('class' => 'form-control', 'required', 'readonly')) }}
										</div>
										
										<div class="form-group">
											{{ Form::label('municipality', 'Municipality') }}
											{{ Form::text('municipality', $municipality, array('class' => 'form-control', 'disabled')) }}
										</div>
										
										<div class="form-group">
											{{ Form::label('province', 'Province') }}
											{{ Form::text('province', $province, array('class' => 'form-control', 'disabled')) }}
										</div>
										<div class="form-group">
											{{ Form::label('program_id', 'Program') }}
											{{ Form::select('program_id',$program_lists, $program, array('class' => 'form-control', 'required')) }}
										</div>
										
									</div>

									<div class="col-md-6">
										

										<div class="form-group">
											{{ Form::label('cycle_id', 'Cycle') }}
											{{ Form::select('cycle_id', [''=>'Select Cycle']+$cycle_list, $cycle, array('class' => 'form-control', 'required')) }}
										</div>
										
										
										<div class="form-group">
											{{ Form::label('date_appointed', 'Date of Volunteer’s Appointment') }}
											{{ Form::input('text', 'date_appointed', Input::old('date_appointed'), array('class' => ' date form-control', 'max' => date("Y-m-d"), 'required')) }}
										</div>
										
										<div class="form-inline">
											
											{{ Form::checkbox('is_ppbene', '1', Input::old('is_ppbene'), array( 'id' => 'is_ppbene')) }} 
											{{ Form::label('is_ppbene', ' Pantawid Pamilya Benefeciary') }}
											
											{{ Form::checkbox('is_ppleader', '1', Input::old('is_ppleader'), array( 'id' => 'is_ppleader', 'disabled')) }} 
											{{ Form::label('is_ppleader', ' Pantawid Pamilya Leader') }}
								
										</div>
										
										<div class="form-inline">
											
											{{ Form::checkbox('is_slpbene', '1', Input::old('is_slpbene'), array( 'id' => 'is_slpbene')) }} 
											{{ Form::label('is_slpbene', 'SLP Benefeciary') }}
											
											{{ Form::checkbox('is_slpleader', '1', Input::old('is_slpleader'), array( 'id' => 'is_slpleader', 'disabled')) }} 
											{{ Form::label('is_slpleader', 'SLP Leader') }}
								
											{{ Form::checkbox('is_bspmc', '1', Input::old('is_bspmc'), array( 'id' => 'is_bspmc')) }} 
											{{ Form::label('is_bspmc', 'BSPMC  Chair') }}
										</div>
										
										<div class="form-group">
											{{ Form::label('aschair_start', 'Start Date') }}
											{{ Form::input('text', 'aschair_start', Input::old('aschair_start'), array('class' => 'date form-control', 'id' => 'aschair_start','required')) }}

										</div>
										
										<div class="form-group">
											{{ Form::label('aschair_end', 'End Date') }}
											{{ Form::input('text', 'aschair_end', Input::old('aschair_end'), array('class' => 'date form-control', 'id' => 'aschair_end')) }}
										</div>
									</div>
								</div> 
								<hr>
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											Sectors Represented
										</div>
										<div class="panel-body">
											<div class="table-responsive">
													@foreach($sector_list as $sector)
														<div class="col-md-3">
															
															<div class="form-control">
																<input type="checkbox" name="sector[]" value="{{$sector}}">&nbsp{{$sector}}
															</div>
													
														</div>
													@endforeach
											</div>
										</div>
									</div>
									

									<?php $count=0; ?>
									
									<div class="panel panel-default">
										<div class="panel-heading">
											Committee Membership
							    		</div>
										<div class="panel-body">
										<div class="table-responsive">
											<table class="table table-striped table-bordered table-hover">
												<button id="add" type="button" class="btn btn-primary">Add New Entry</button>

												<br>
												<br>

												<div class="hidden"> 
													<div id="committee_option">
														@foreach($volcomm_list as $volcomm_id => $volcomm)
															<option value="{{$volcomm_id}}">{{$volcomm}}</option>
														@endforeach
													</div>
													<div id="position_option">
														@foreach($volpost_list as $volpost)
															<option value="{{$volpost}}">{{$volpost}}</option>
														@endforeach
													</div>
												</div>
																<table id="items" class="table table-bordered">
					                                                <thead>
					                                                    <tr>
					                                                        <th>Committee</th>
					                                                        <th>Position</th>
					                                                        <th>Start</th>
					                                                        <th>End</th>
					                                                        <th>Options</th>
					                                                    </tr>
					                                                </thead>
					                                                <tbody>
					                                                </tbody>
					                                            </table>
											</tablmmme>
										</div>
										</div>
									</div>

									<div class="panel panel-default">
													<div class="panel-heading">
														Trainings Attended
										    		</div>
													<div class="panel-body">
													<div class="table-responsive">
														<table class="table table-striped table-bordered table-hover">
															<button id="add1" type="button" class="btn btn-primary">Add New Entry</button>
															<br>
															<br>		
															<div id="items1">
																@foreach($training_data as $training)
																<div class="input-group">
																	<div class="form-inline">
																		<input type="text" class="form-control" name="training_title[]" value="{{$training->training_title}}" onchange="checkduplicate(this)" placeholder="Training Title" id="{{ $count }}" required>
																		<input type="text" class="form-control" name="training_prov[]" value="{{$training->training_prov}}" placeholder="Training Provider" id="provider{{ $count }}" onchange="checkduplicate(this)" required>
																		<input type="number" class="form-control" name="year_attended[]" value="{{$training->year_attended}}" placeholder="Year Attended" id="year{{ $count }}" min="1980" max="{{ date('Y') }}" onchange="checkduplicate(this)" required>
																	</div>
																	<div class="input-group-addon"><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div>
																	<br>
																</div>
																<? $count++ ?>
																@endforeach
															</div>
															
														
														</table>
													</div>
													</div>
												</div>
												
												<div class="panel panel-default">
													<div class="panel-heading">
														Other Organization
										    		</div>
													<div class="panel-body">
													<div class="table-responsive">
														<table class="table table-striped table-bordered table-hover">
															<button id="add2" type="button" class="btn btn-primary">Add New Entry</button>
															<br>
															<br>
															<div id="items2">
																@foreach($org_data as $org)
																<div class="input-group">
																	<div class="form-inline">
																		<input type="text" class="form-control" name="org_name[]" value="{{$org->org_name}}" onchange="checkduplicate1(this)" placeholder="Committee" id="{{ $count }}" required>
																		<input type="text" class="form-control" name="position_org[]" value="{{$org->position}}" placeholder="Position" id="position_org{{ $count }}" onchange="checkduplicate1(this)" required>
																		</div>
																	<div class="input-group-addon"><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div>
																	<br>
																</div>
																<? $count++; ?>
																@endforeach
															</div>
														</table>
													</div>
													</div>
												</div>
												<div class="hidden" id="count">{{ $count }}</div>



								</div>
								<div class="col-md-12 ">
									<div class="form-group pull-right">
						                <button class="btn btn-primary" id="save">
											<i class="fa fa-save"></i>
											Submit 
										</button>
		                 				<a class="btn btn-default" href="{{ URL::to('volunteer') }}">Cancel</a>		
									</div>
								</div>
								<div class="clearfix"></div>
							</div> 
						</div>

						{{ Form::close() }}
					</div>
				</div>
				
				
				
				
				
			

				
				{{ Form::close() }}
			</div>
		</div>
		
		<script>
			$(document).ready(function(){
				var count = $("#count").text();
			     $('.date').datetimepicker({pick12HourFormat: false, pickTime:false});
			     $("#aschair_start").attr('disabled','disabled');
                 $("#aschair_end").attr('disabled','disabled');
				$("#barangay").change(function (e) {
					$('.hidden').hide()
					$("#psgc_id").attr('value', $("#barangay").val());
					var selector = "option[value='" +$("#barangay").val()+ "']";
					$(selector).attr('selected', 'selected');
					
				});
				
				 $('.date').datetimepicker({pick12HourFormat: false, pickTime:false});

                $("#add").click(function (e) {
                					//Append a new row of code to the "#items" div
                					$("#items").find('tbody').append('<tr><td><select class="form-control" placeholder="Committee" id="'+count+'" required="required" name="committee[]" onchange="checkduplicate2(this)">'+$("#committee_option").html() +'</select></td><td><select class="form-control" placeholder="Committee" id="position:'+count+'" required="required" name="position[]" onchange="checkduplicate2(this)">'+$("#position_option").html() +'</select></td><td><input type="text" class="form-control date" id="startdate:'+count+'" name="startdate[]" onchange="setStartLimit1(this)" required></td><td><input type="text" class="form-control date" id="enddate:'+count+'" name="enddate[]" onchange="setEndLimit1(this)" ></td><td><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></td></tr>');
                					 $('.date').datetimepicker({pick12HourFormat: false, pickTime:false});
                					count++;
                				});
				
				$("#add1").click(function (e) {
					//Append a new row of code to the "#items" div
                        $("#items1").append('<div class="input-group"><div class="form-inline"><input type="text" class="form-control" name="training_title[]" onchange="checkduplicate(this)" placeholder="Training Title" id="'+count+'" required><input type="text" class="form-control" name="training_prov[]" placeholder="Training Provider" id="provider'+count+'" onchange="checkduplicate(this)" required><input type="number" class="form-control" name="year_attended[]" placeholder="Year Attended" id="year'+count+'" min="1980" max="'+(new Date).getFullYear()+'"  onchange="checkduplicate(this)" required></div><div class="input-group-addon"><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div><br></div>');
					count++;
				});
				
				$("#add2").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items2").append('<div class="input-group"><div class="form-inline"><input type="text" class="form-control" name="org_name[]" onchange="checkduplicate1(this)" placeholder="Committee" id="'+count+'" required><input type="text" class="form-control" name="position_org[]" placeholder="Position" id="position_org:'+count+'" onchange="checkduplicate1(this)" required></div><div class="input-group-addon"><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div><br></div>');
					count++;
				});
				
				$("body").on("click", ".delete", function (e) {
					$(this).parent("div").parent("div").remove();
										$(this).parent('td').parent("tr").remove();
				});
				
				
				$("#is_ppbene").change(function(){
					if($("#is_ppbene").is(":checked")){
						$("#is_ppleader").removeAttr('disabled');
					}
					else{
						$("#is_ppleader").attr('disabled','disabled');
						$("#is_ppleader").prop('checked', false);	
					}
				});
				
				$("#is_slpbene").change(function(){
					if($("#is_slpbene").is(":checked")){
						$("#is_slpleader").removeAttr('disabled');
					}
					else{
						$("#is_slpleader").attr('disabled','disabled');
						$("#is_slpleader").prop('checked', false);	
					}
				});
				
				$("#is_bspmc").change(function(){
					if($("#is_bspmc").is(":checked")){
						$("#aschair_start").removeAttr('disabled');
						$("#aschair_end").removeAttr('disabled');
						$('.date').datetimepicker({pick12HourFormat: false, pickTime:false});
					}
					else{
						$("#aschair_start").attr('disabled','disabled');
						$("#aschair_end").attr('disabled','disabled');
					}
				});
				
				$("#aschair_start").change(function (e) {
					$("#aschair_end").attr('min', $("#aschair_start").val());
				});
			
				$("#aschair_end").change(function (e) {
					$("#aschair_start").attr('max', $("#aschair_end").val());
				});
				
				$("#save").click(function (){
					checkBarangay();
				});
				
//				$("#sector_represented").multiselect();
				
				setAge();
			});
			
			
			// 
			
			function setAge(){
				var today = new Date();
				var birthdate = new Date(document.getElementById("birthdate").value);
				var age = today.getFullYear() - birthdate.getFullYear();
    			var m = today.getMonth() - birthdate.getMonth();
    			if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
        			age--;
    			}
    			document.getElementById("age").value = age;
			}
			
			function setEndLimit(e){
				e.previousSibling.setAttribute("max", e.value);
				checkduplicate1(e);
			}
			
			function setStartLimit(e){
				e.nextSibling.setAttribute("min", e.value);
				checkduplicate1(e);
			}
			
			function setEndLimit1(e){
				e.previousSibling.setAttribute("max", e.value);
				checkduplicate2(e);
			}
			
			function setStartLimit1(e){
				e.nextSibling.setAttribute("min", e.value);
				checkduplicate2(e);
			}
			
			function checkBarangay(){
				brgy = document.getElementById("barangay");
				if(brgy.value === "NULL"){
					brgy.setCustomValidity("Select a Barangay");
				}else{
					brgy.setCustomValidity("");
				}
			}
			
			function checkduplicate(e){
				
				var count = -1;
				var id = e.id;
				id = id.substr(id.length - 1);
				var title = document.getElementById(id);
				var provider = document.getElementById("provider"+id).value;
				var year = document.getElementById("year"+id).value;
				var titles = document.getElementsByName('training_title[]');
				var providers = document.getElementsByName('training_prov[]');
				var years = document.getElementsByName('year_attended[]');

				for (i=0; i<titles.length; i++) {
    				if(title.value === titles[i].value && provider === providers[i].value && year === years[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					title.setCustomValidity("Duplicate Entry not allowed");
				}else{
					title.setCustomValidity("");
				}
				
			}
			
			function checkduplicate1(e){
				
				var count = -1;
				var id = e.id;
				id = id.substr(id.length - 1);
				var org = document.getElementById(id);
				var position = document.getElementById("position_org"+id).value;
				var startdate = document.getElementById("startdate_org"+id).value;
				var enddate = document.getElementById("enddate_org"+id).value;
				var orgs = document.getElementsByName('org_name[]');
				var positions = document.getElementsByName('position_org[]');
				var startdates = document.getElementsByName('startdate_org[]');
				var enddates = document.getElementsByName('enddate_org[]');
				
				for (i=0; i<orgs.length; i++) {
    				if(org.value === orgs[i].value && position === positions[i].value && startdate === startdates[i].value && enddate === enddates[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					org.setCustomValidity("Duplicate Entry not allowed");
				}else{
					org.setCustomValidity("");
				}
				
			}
			
			function checkduplicate2(e){
				
				var count = -1;
				var id = e.id;
				id = id.substr(id.length - 1);
				var comm = document.getElementById(id);
				var position = document.getElementById("position"+id).value;
				var startdate = document.getElementById("startdate"+id).value;
				var enddate = document.getElementById("enddate"+id).value;
				var comms = document.getElementsByName('committee[]');
				var positions = document.getElementsByName('position[]');
				var startdates = document.getElementsByName('startdate[]');
				var enddates = document.getElementsByName('enddate[]');
				
				for (i=0; i<comms.length; i++) {
    				if(comm.value === comms[i].value && position === positions[i].value && startdate === startdates[i].value && enddate === enddates[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					comm.setCustomValidity("Duplicate Entry not allowed");
				}else{
					comm.setCustomValidity("");
				}
				
			}
			
		</script>
@stop