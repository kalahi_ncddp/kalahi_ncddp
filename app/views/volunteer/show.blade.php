@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
	        <div class="col-md-12">
	    		@if($volunteer->is_draft==1)
        		 	<div class="alert alert-info">This is still a draft  <a href="{{ URL::to('volunteer/' .$volunteer->volunteer_id.'/edit') }}">edit the document</a> to set it in final draft</div>
         		@endif
	    		 <h2>
        		   <a class="btn btn-default" href="{{ URL::to('volunteer') }}">Go Back</a>
				 <div class="pull-right">
	    	@if( !is_review($volunteer->volunteer_id) )

                     <a class="btn  btn-small btn-info" href="{{ URL::to('volunteer/' .$volunteer->volunteer_id.'/edit') }}">
                   <i class="fa fa-edit"></i>  Edit
                     </a>

                     <button class="btn btn-danger" data-toggle="modal" data-target="#myModal">
                        Delete Volunteer Record
                     </button>
                     <button class="btn btn-danger" data-toggle="modal" data-target="#myModal1">
                        Delete Volunteer Profile Record
                     </button>
                     @endif
				 </div>
	        </div>
	    </div>

	    <div class="row">
		    <div class="col-md-12">
		     {{ viewComment($volunteer->volunteer_id) }}
                <div class="panel panel-default">
		    		<div class="panel-heading">
		    			View Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">
							 	<tr>
					                <th>Barangay</th>
					                <td>{{ $barangay = Report::get_barangay($volunteer->psgc_id) }}</td>
					            </tr>
					          
					            <tr>
					                <th>Municipality</th>
					                <td>{{ $municipality = Report::get_municipality_by_brgy_id($volunteer->psgc_id) }}</td>
					            </tr>
					            <tr>
					                <th>Province</th>
					                <td>{{ $province = Report::get_province_by_brgy_id($volunteer->psgc_id) }}</td>
					            </tr>
					            <tr>
					                <th>Region</th>
					                <td>{{ $region = Report::get_region_by_brgy_id($volunteer->psgc_id) }}</td>
					            </tr>
					            
					           </table>
					       </div>
					       <div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">
					             <tr>
					                <th>Cycle</th>
					                <td>{{ $volunteer->cycle_id }}</td>
					            </tr>
					              <tr>
					                <th>Program</th>
					                <td>{{ $volunteer->program_id }}</td>
					            </tr>
					            
					             <tr>
					                <th>Date Appointed</th>
					                <td>{{ toDate($volunteer->date_appointed) }}</td>
					            </tr>
					             <tr>
					                <th>Sector Represented</th>
					                <td>{{ $volunteer->sector_represented }}</td>
					            </tr>
					
							</table>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Volunteer Profile
		    		</div>
		    		<div class="panel-body">

						<div class="table-responsive">
							 <div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">
							 	<tr>
					                <th>Last Name</th>
					                <td>{{ $beneficiary->lastname }}</td>
					            </tr>
					            <tr>
					                <th>First Name</th>
					                <td>{{ $beneficiary->firstname }}</td>
					            </tr>
					            <tr>
					                <th>Middle Initial</th>
					                <td>{{ $beneficiary->middlename }}</td>
					            </tr>
					            <tr>
					                <th>Birthdate</th>
					                <td><div id="birthdate"> {{ toDate($beneficiary->birthdate) }}</div></td>
					            </tr>
					            <tr>
					                <th>Age</th>
					                <td><div id="age"> </div></td>
					            </tr>
					            <tr>
					                <th>Sex</th>
					                <td>@if($beneficiary->sex[0] == 'M' ) 
					                		Male
				                		@else 
				                		Female
				                		@endif  
					                	</td>
					            </tr>
					            <tr>
					                <th>Civil Status</th>
					                <td>{{ $beneficiary->civil_status }}</td>
					            </tr>
					        </table>
					    </div>

					     <div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					                <th>Number of Children</th>
					                <td>{{ $beneficiary->no_children }}</td>
					            </tr>
					            <tr>
					                <th>Address</th>
					                <td>{{ $beneficiary->address }}</td>
					            </tr>
					            <tr>
					                <th>Contact Number</th>
					                <td>{{ $beneficiary->contact_no }}</td>
					            </tr>
					            <tr>
					            	<th>Current position in BLGU</th>
					            	<td>{{ $beneficiary->current_position_blgu }}</td>
					            <tr>
					                <th>Educational Attainment</th>
					                <td>{{ $beneficiary->educ_attainment }}</td>
					            </tr>
					            <tr>
					                <th>Occupation</th>
					                <td>{{ $beneficiary->occupation }}</td>
					            </tr>
					    	</table>
						</div>
					</div>
					
		
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
							 	<tr>
							 	<td> 
							 		{{ Form::checkbox('is_ip', '1', $beneficiary->is_ip, array( 'id' => 'is_ip', 'disabled')) }} 
									IP
							 	 
							 		{{ Form::checkbox('is_ipleader', '1', $beneficiary->is_ipleader, array( 'id' => 'is_ipleader', 'disabled')) }} 
									IP Leader
								</td>
								<td> 
									
									{{ Form::checkbox('is_ppbebe', '1', $volunteer->is_ppbene, array( 'id' => 'is_ip', 'disabled')) }} 
									 Pantawid Pamilya Beneficiary
							 	 
							 		{{ Form::checkbox('is_ppleader', '1', $volunteer->is_ppleader, array( 'id' => 'is_ppleader', 'disabled')) }} 
									 Pantawid Pamilya Leader
								</td>
								<td> 
									
									{{ Form::checkbox('is_slpbene', '1', $volunteer->is_slpbene, array( 'id' => 'is_slpbene', 'disabled')) }} 
									SLP Beneficiary
							 	 
							 		{{ Form::checkbox('is_slpleader', '1', $volunteer->is_slpleader, array( 'id' => 'is_slpleader', 'disabled')) }} 
									SLP Leader
									
							 	</td>
							 	<tr>
							 	
							 	<tr>
							 	<td> 
							 		{{ Form::checkbox('is_bspmc', '1', $volunteer->is_bspmc, array( 'id' => 'is_bspmc', 'disabled')) }} 
									BSPMC Chair

								</td>
								<td> 
									Date: {{ toDate($volunteer->aschair_start) }} - {{ toDate($volunteer->aschair_end) }}
									
								</td>
							 	</tr>
							</table>
						</div>
					</div>
				
					
					
				</div>
				
				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			KC-NCDDP Committee Membership
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Committee </th>
					                <th>Position</th>
					                <th>Startdate</th>
					                <th>Enddate</th>
					            </tr>
					           
					            @foreach($committee_data as $comm)
					            <tr>
					                <td>{{ $comm_description = Volunteer::get_committee_description($comm->committee)}}</td>
					                <td>{{ $comm->position}}</td>
					                <td>{{ toDate($comm->startdate)}}</td>
					                <td>{{ toDate($comm->enddate)}}</td>
					            </tr>
					            @endforeach
					            
					
							</table>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Previous Trainings Attended (NON KC-NCDDP)
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Title </th>
					                <th>Provider</th>
					                <th>Year</th>
					                
					            </tr>
					           
					            @foreach($training_data as $training)
					            <tr>
					                <td>{{ $training->training_title}}</td>
					                <td>{{ $training->training_prov}}</td>
					                <td>{{ $training->year_attended}}</td>
					            </tr>
					            @endforeach
					            
					
							</table>
						</div>
					</div>
				</div><div class="panel panel-default">
		    		<div class="panel-heading">
		    			Other Organization (NON KC-NCDDP)
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Organization </th>
					                <th>Position</th>

					            </tr>
					           
					            @foreach($org_data as $org)
					            <tr>
					                <td>{{ $org->org_name}}</td>
					                <td>{{ $org->position}}</td>

					            </tr>
					            @endforeach
					            
					
							</table>
						</div>
					</div>
				</div>													
			</div>
		</div>
		
		 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Delete {{ $volunteer->volunteer_id}}</h4>
                    </div>
                    <div class="modal-body">
                    	{{ Form::open(array('url' => 'volunteer/' . $volunteer->volunteer_id)) }}
                    	{{ Form::hidden('_method', 'DELETE') }}
                    		Are you sure you want to delete this volunteer record? 
						<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
						{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
				 		{{ Form::close() }}
					</div>
	            </div>
            </div>
        </div>
        
         <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                	  <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Delete {{ $volunteer->beneficiary_id}}</h4>
                    </div>
                    
                    <div class="modal-body">
                    	{{ Form::open(array('url' => 'volunteer/' . $volunteer->beneficiary_id)) }}
                    	{{ Form::hidden('_method', 'DELETE') }}
                    		Are you sure you want to delete this volunteer profile and all volunteer records related to this volunteer profile? 
						<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
						{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
				 		{{ Form::close() }}
					</div>
	            </div>
            </div>
        </div>
		
		<script>
			$(document).ready(function(){
				setAge();
			});
			
			function setAge(){
				
				var today = new Date();
				var birthdate = new Date(document.getElementById("birthdate").innerHTML);
				var age = today.getFullYear() - birthdate.getFullYear();
    			var m = today.getMonth() - birthdate.getMonth();
    			if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
        			age--;
    			}
    			document.getElementById("age").innerHTML = age;
			}
		</script>																		
		
	
@stop