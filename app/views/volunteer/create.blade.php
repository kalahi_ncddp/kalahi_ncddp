@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
		
	
		@if ($errors->all())
			<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
		@endif
		{{ Form::open(array('url' => 'volunteer')) }}
	    <div class="row volunteer">
		    <div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Add Volunteer Information</h3>
					</div>
					<div class="box-body">
						<!-- Adding importang information -->
						
						<div class="col-md-6">
							<div class="form-group">

								{{ Form::label('lastname', 'Last Name') }}
								<div class="form-inline">
										{{ Form::text('lastname', Input::old('lastname'), array('class' => 'form-control', 'id' => 'lastname', 'placeholder' => 'Last Name', 'required')) }}
								
									<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
				        				Volunteers List
				        			</button>
			        				{{ Form::reset('Reset', array('class' => 'btn btn-danger', 'id' => 'reset')) }}

			        			</div>
								{{ Form::text('beneficiary_id', Input::old('beneficiary_id'), array('class' => 'hidden form-control', 'id' => 'beneficiary_id')) }}
							</div>

							<div class="form-group">
								{{ Form::label('lastname', 'First Name') }}
								{{ Form::text('firstname', Input::old('firstname'), array('class' => 'form-control', 'id' => 'firstname', 'placeholder' => 'First Name', 'required')) }}
							</div>
							<div class="form-group">
								
								{{ Form::label('lastname', 'Middle Initial') }}

									{{ Form::text('middlename', Input::old('middlename'), array('maxlength'=>'4','class' => 'form-control', 'id' => 'middlename', 'placeholder' => 'Middle Initial')) }}
							</div>
							<div class="form-group">
								<label for="">Birthdate <i class="text-red">* required</i></label>
								{{ Form::input('text', 'birthdate', '', array('class' => 'form-control date', 'id' => 'birthdate', 'required')) }}
							</div>
							
							<div class="form-group">
							   <label for="">Age <i class="text-red">* required</i></label>
								{{ Form::text('age', Input::old('age'), array('class' => 'form-control', 'id' => 'age','max'=>'99' , 'readonly')) }}
							</div>
							
							<div class="form-group">
								<label for="">Sex <i class="text-red">* required</i></label>
								{{ Form::select('sex', array('' =>'Select Gender','Male' => 'Male', 'Female' => 'Female'), Input::old('sex'), array('class' => 'form-control', 'id' => 'sex', 'required')) }}
							</div>
							
							<div class="form-group">
								<label for="">Civil Status <i class="text-red">* required</i></label>
								{{ Form::select('civil_status', $civil_status_list, Input::old('civil_status'), array('class' => 'form-control', 'id' => 'civil_status', 'required')) }}
							</div>
							
							
						</div>
						<!-- second column -->

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('no_children', 'Number of children') }}
								{{ Form::input('number', 'no_children', Input::old('no_children') | 0, array('class' => 'form-control', 'id' => 'no_children', 'required', 'min' => '0')) }}
							</div>
							
							<div class="form-group">
								{{ Form::label('address', 'Address') }}
								{{ Form::text('address', Input::old('address'), array('class' => 'form-control', 'id' => 'address', '')) }}
							</div>
							
							<div class="form-group">
								{{ Form::label('contact_no', 'Contact Number') }}
								{{ Form::text('contact_no', Input::old('contact_no'), array('class' => 'form-control', 'id' => 'contact_no', '')) }}
							</div>

							<div class="form-group">
								{{ Form::label('', 'Current Position in BLGU?') }}
								{{ Form::select('current_position_blgu', $position_blgu,  Input::old('current_position_blgu'), array('class' => 'form-control')) }}
								                	
							</div>
							<hr>

							<div class="form-inline">
								
								{{ Form::checkbox('is_ip', '1', Input::old('is_ip'), array( 'id' => 'is_ip')) }} 
								{{ Form::label('is_ip', 'IP') }}
								
								{{ Form::checkbox('is_ipleader', '1', Input::old('is_ipleader'), array( 'id' => 'is_ipleader', 'disabled')) }} 
								{{ Form::label('is_ipleader', 'IP Leader') }}
					
							</div>
							
							<br>
							
							<div class="form-group">
								{{ Form::label('educ_attainment', 'Educational Attainment') }}
								{{ Form::select('educ_attainment', $educ_level_list, Input::old('educ_attainment'), array('class' => 'form-control', 'id' => 'educ_attainment', '')) }}
							</div>
							
							<div class="form-group">
								{{ Form::label('occupation', 'Occupation') }}
								{{ Form::text('occupation', Input::old('occupation'), array('class' => 'form-control', 'id' => 'occupation', '')) }}
							</div>
							
						</div>
						<div class="col-md-12">
							<div class="pull-right">

								<button class="btn btn-primary">
									<i class="fa fa-save"></i>
									Submit 
								</button>
                 				<a class="btn btn-default" href="{{ URL::to('volunteer') }}">Cancel</a>
							</div>
						</div>

						<!-- end -->
						<div class="clearfix"></div>
					</div>
				</div>
				{{ Form::close() }}
			</div>
		</div>
		
		
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Volunteers List</h4>
                    </div>
                    <div class="modal-body">
                    	<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>Last Name</th>
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Birthdate</th>
										<th>Details</th>
									</tr>
								</thead>
								<tbody>
									
									@foreach($data as $volunteer)
									
									<tr>
										
										<td>
											{{ $volunteer->lastname }}
										</td>
										<td>
											{{ $volunteer->firstname }}
										</td>
										<td>
											{{ $volunteer->middlename }}
										</td>
										<td>
											{{ $volunteer->birthdate }}
										</td>

										<td>
											<div class="use_volunteer">
											
				                           
				                            <a class="btn btn-success btn" onclick="$(this).use_volunteer($(this));">
				                            	
				                              Use
				                              <div class="hidden">
												<div class="lastname">{{ $volunteer->lastname }}</div>
												<div class="firstname">{{ $volunteer->firstname }}</div>
												<div class="middlename">{{ $volunteer->middlename }}</div>
												<div class="birthdate">{{ $volunteer->birthdate }}</div>
												<div class="sex">{{ $volunteer->sex }}</div>
												<div class="civil_status">{{ $volunteer->civil_status }}</div>
												<div class="no_children">{{ $volunteer->no_children }}</div>
												<div class="address">{{ $volunteer->address }}</div>
												<div class="contact_no">{{ $volunteer->contact_no }}</div>
												<div class="is_ip">{{ $volunteer->is_ip }}</div>
												<div class="is_ipleader">{{ $volunteer->is_ipleader }}</div>
												<div class="educ_attainment">{{ $volunteer->educ_attainment }}</div>
												<div class="occupation">{{ $volunteer->occupation }}</div>
												<div class="beneficiary_id">{{ $volunteer->beneficiary_id }}</div>
											</div>
				                            </a>
				                            </div>
				                        </td>
				                       
				                	</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
	            </div>
            </div>
        </div>
		
		<script>
			$(document).ready(function(){
				var count = 0;
				
				$("#barangay").change(function (e) {
					$('.hidden').hide()
					$("#psgc_id").attr('value', $("#barangay").val());
					var selector = "option[value='" +$("#barangay").val()+ "']";
					$(selector).attr('selected', 'selected');
					
				});
				
				$("#add1").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items1").append('<div class="input-group"><div class="form-inline"><input type="text" class="form-control" name="training_title[]" onchange="checkduplicate(this)" placeholder="Training Title" id="'+count+'" required><input type="text" class="form-control" name="training_prov[]" placeholder="Training Provider" id="provider'+count+'" onchange="checkduplicate(this)" required><input type="number" class="form-control" name="year_attended[]" placeholder="Year Attended" id="year'+count+'" min="0" max="'+(new Date).getFullYear()+'" onchange="checkduplicate(this)" required></div><div class="input-group-addon"><button type="button" class="delete">-</button></div><br></div>');
					count++;
				});
				
				$("#add2").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items2").append('<div class="input-group"><div class="form-inline"><input type="text" class="form-control" name="org_name[]" onchange="checkduplicate1(this)" placeholder="Committee" id="'+count+'" required><input type="text" class="form-control" name="position[]" placeholder="Position" id="position'+count+'" onchange="checkduplicate1(this)" required><input type="date" class="form-control" name="startdate[]" onchange="setStartLimit(this)" id="startdate'+count+'" required><input type="date" class="form-control" name="enddate[]" onchange="setEndLimit(this)" id="enddate'+count+'" required></div><div class="input-group-addon"><button type="button" class="delete">-</button></div><br></div>');
					count++;
				});
				
				$("body").on("click", ".delete", function (e) {
					$(this).parent("div").parent("div").remove();
				});
				
				
				$('#dataTables-example').dataTable();
				
				$.fn.use_volunteer = function(vol){

					$(".form-control").attr('readonly', 'readonly');
					$(".form-control").attr('readonly', 'readonly');
					$("#lastname").attr('value', vol.find(".lastname").text());
					$("#firstname").attr('value', vol.find(".firstname").text());
					$("#middlename").attr('value', vol.find(".middlename").text());
					$("#birthdate").attr('value', vol.find(".birthdate").text());
					$("#civil_status").find(":selected").text(vol.find(".civil_status").text());
					$("#sex").find(":selected").text(vol.find(".sex").text());
					$("#no_children").attr('value', vol.find(".no_children").text());
					$("#address").attr('value', vol.find(".address").text());
					$("#contact_no").attr('value', vol.find(".contact_no").text());
					$("#is_ip").prop('checked', vol.find(".is_ip").text() == '1');
					$("#is_ipleader").prop('checked', vol.find(".is_ipleader").text() == '1');
					$("#educ_attainment").find(":selected").text(vol.find(".educ_attainment").text());
					$("#occupation").attr('value', vol.find(".occupation").text());
					$("#beneficiary_id").attr('value', vol.find(".beneficiary_id").text());
					$("#items1").remove();
					$("#items2").remove();
					$("#myModal").modal('toggle');
					setAge()
				};
				
				$("#reset").click(function(){
					$(".form-control").attr('value', "");
					$(".form-control").removeAttr('readonly');
					$("#age").attr('readonly', 'readonly');
				});
				
				$("#birthdate").change(function(){
					setAge();
					
				});
				
				$("#is_ip").change(function(){
					if($("#is_ip").is(":checked")){
						$("#is_ipleader").removeAttr('disabled');
					}
					else{
						$("#is_ipleader").attr('disabled','disabled');
						$("#is_ipleader").prop('checked', false);	
					}
				});
				
			});
			
			function setAge(){
				var today = new Date();
				var birthdate = new Date(document.getElementById("birthdate").value);
				var age = today.getFullYear() - birthdate.getFullYear();
    			var m = today.getMonth() - birthdate.getMonth();
    			if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
        			age--;
    			}
    			document.getElementById("age").value = age;
    			
    			e = document.getElementById("birthdate");
    			if(age < 15){
					e.setCustomValidity("Age must be 15+");
				}
				else if(age >= 99){
				    e.setCustomValidity("Age must be 99 below");
				}
				else{
					e.setCustomValidity("");
				}
			}
			
			function setEndLimit(e){
				e.previousSibling.setAttribute("max", e.value);
				checkduplicate1(e);
			}
			
			function setStartLimit(e){
				e.nextSibling.setAttribute("min", e.value);
				checkduplicate1(e);
			}
			
			function checkduplicate(e){
				var count = -1;
				var id = e.id;
				id = id.substr(id.length - 1);
				var title = document.getElementById(id);
				var provider = document.getElementById("provider"+id).value;
				var year = document.getElementById("year"+id).value;
				var titles = document.getElementsByName('training_title[]');
				var providers = document.getElementsByName('training_prov[]');
				var years = document.getElementsByName('year_attended[]');
				for (i=0; i<titles.length; i++) {
    				if(title.value === titles[i].value && provider === providers[i].value && year === years[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					title.setCustomValidity("Duplicate Entry not allowed");
				}else{
					title.setCustomValidity("");
				}
			}
			
			function checkduplicate1(e){
				
				var count = -1;
				var id = e.id;
				id = id.substr(id.length - 1);
				var org = document.getElementById(id);
				var position = document.getElementById("position"+id).value;
				var startdate = document.getElementById("startdate"+id).value;
				var enddate = document.getElementById("enddate"+id).value;
				var orgs = document.getElementsByName('org_name[]');
				var positions = document.getElementsByName('position[]');
				var startdates = document.getElementsByName('startdate[]');
				var enddates = document.getElementsByName('enddate[]');
				
				for (i=0; i<orgs.length; i++) {
    				if(org.value === orgs[i].value && position === positions[i].value && startdate === startdates[i].value && enddate === enddates[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					org.setCustomValidity("Duplicate Entry not allowed");
				}else{
					org.setCustomValidity("");
				}
			}
			
		</script>
@stop