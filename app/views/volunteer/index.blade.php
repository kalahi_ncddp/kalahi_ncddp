@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	    <div class="row">
		    <div class="col-md-12">
		    	<div class="panel panel-default">
		    		<div class="panel-heading">
		    			{{ $title }}
		    			@if($position->position=='ACT')
						    <span class="btn btn-default pull-right" id="approved" style="margin:0 10px">Review</span>
						    			    <span class="hidden module">volunteer</span>
			            @endif
			            <a class="btn  btn-primary pull-right" href="{{ URL::to('volunteer/create') }}">
						 <i class="fa fa-plus"></i> Add New
						</a>
						<div class="clearfix"></div>
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
						</div>
						<div class="col-xs-6 pull-right">
							<div class="dataTables_filter" id="dataTables-example_filter">
								<label>Search: 
									<input type="text" aria-controls="dataTables-example" value="{{ Input::get('search','') }}" class="search">
								</label>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										@if($position->position=='ACT')
										<th>
										<input type="checkbox" id="selecctall"/>
										Reviewed</th>
									    @endif
										<th>Region</th>
										<th>Province</th>
										<th>Municipality</th>
										<th>Barangay</th>
										<th>Cycle</th>
										<th>Program</th>
										<th>Last Name</th>
										<th>First Name</th>
										<th>Details</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data as $volunteer)
									<tr>
									    
										@if($position->position=='ACT')
											<td>
                                           @if($volunteer->is_draft==0)
                                             {{ $reference->approval_status($volunteer->volunteer_id , 'ACT') }}
                                           @else
                                               <span class="label label-warning">draft</span>
                                           @endif
                                           </td>
                                         @endif

			                            
										<td>
											{{ $region = Report::get_region_by_brgy_id($volunteer->psgc_id) }}
										</td>
										<td>
											{{ $province = Report::get_province_by_brgy_id($volunteer->psgc_id) }}
										</td>
										<td>
											{{ $municipality = Report::get_municipality_by_brgy_id($volunteer->psgc_id) }}
										</td>
										<td>
											{{ $barangay = Report::get_barangay($volunteer->psgc_id) }}
										</td>
										<td>
											{{ $volunteer->cycle_id }}
										</td>
										<td>
											{{ $volunteer->program_id }}
										</td>
										<td>
											{{ $last_name = Volunteer::get_last_name($volunteer->beneficiary_id) }}
										</td>
										<td>
											{{ $first_name = Volunteer::get_first_name($volunteer->beneficiary_id) }}
										</td>
										
										<td>
				                            <a class="btn btn-success btn" href="{{ URL::to('volunteer/' .$volunteer->volunteer_id) }}">
				                              View Details {{ hasComment($volunteer->volunteer_id) }}
				                            </a>
				                        </td>
									</tr>
									@endforeach
								</tbody>
							</table>
							<div class="pull-left">
								
								Showing  1 to
								{{ $data->count() }}  of {{ $total }} entries
							</div>
							<div class="pull-right">
								{{ $data->links(); }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<script>
      $(document).ready(function () {
         	// $('#dataTables-example').dataTable({
          //   "aoColumnDefs": [
          //             { 'bSortable': false, 'aTargets': [ 0 ] }
          //          ]});

      		$('.search').keypress(function(e){
      			var key = e.keycode || e.which
      			if(key == 13){
      				var search_value = $(this).val();
      				window.location.href="{{ URL::to('/volunteer') }}?search="+search_value;
      			}
      		});

      });
    </script>
@stop