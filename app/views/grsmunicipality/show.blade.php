@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif

		{{ HTML::link(URL::to('grs-municipality'), 'Go Back','', array('class' => 'btn btn-default btn')) }}

	     <div class="pull-right">
		     @if($grs->is_draft==1)
	        		 	<div class="alert alert-info">This is still a draft  <a href="{{ URL::to('grs-municipality/' .$grs->reference_no.'/edit') }}">edit the document</a> to set it in final draft</div>
	         @endif
	    	@if( !is_review($grs->reference_no) )
				{{ Form::open(array('url' => 'grs-municipality/' . $grs->reference_no)) }}
				{{ Form::hidden('_method', 'DELETE') }}
				<button class="btn btn-danger" type="submit" value="Delete">
					<i class="fa fa-trash-o"></i> DELETE
				</button>
				<!-- Check if review or not  -->
				    
			
				 <a class="btn  btn-small btn-info" href="{{ URL::to('grs-municipality/' .$grs->reference_no.'/edit') }}">
				 	<i class="fa fa-edit"></i>	Edit
				 </a>
			@endif
		</div>
		<br/>
		<br/>
	    <div class="box box-primary">
	        <div class="box-header">
	            <div class="box-title">GRS Municipality</div>
	        </div>
	        <div class="box-body">
               <table class="table table-bordered">
                    	<tr>
                            <td>Municipal Orientation on GRS Conducted </td>
                            <td class="date_orientation"  style="word-wrap: break-word">{{ toDate($grs->date_orientation) }}</td>
                        </tr>
                        <tr>
                            <td>Fact Finding Committee Established</td>
                            <td class="date_ffcomm"  style="word-wrap: break-word"> {{ toDate($grs->date_ffcomm) }} </td>
                        </tr>
                        <tr>
                            <td>
                                GRS Training for ACT/MCT
                            </td>
                            <td class="date_training"  style="word-wrap: break-word">{{ toDate($grs->date_training) }}</td>
                        </tr>
                        <tr>
                        <td>
                            Information materials availability with Grievance Hotline
                        </td>
                            <td class="date_inspect"  style="word-wrap: break-word"> {{ toDate($grs->date_inspect) }} </td>
                        </tr>
                        <tr class="hidden">
                            <td>
                            Means of Reporting Grievances available <td class="date_meansrept"  style="word-wrap: break-word"> {{ toDate($grs->date_meansrept) }} </td>
                            </td>
                        </tr>

                        <tr>

                            <td>
                                Grievance/Suggestion box installed at barangay <td class="is_boxinstalled"  style="word-wrap: break-word">

                                {{ $grs->is_boxinstalled == 1 ? 'Yes':'No'  }}
                                </td>
                            </td>
                        </tr>
                        <tr>

                        <td>
                                Phone Number <td class="phone_no" style="word-wrap: break-word" >
                                {{ $grs->phone_no }}
                                </td>
                        </td>
                        </tr>
                        <tr>

                            <td>
                                Office Address <td class="address" style="word-wrap: break-word">
                                {{ $grs->address }}
                                </td>
                            </td>
                        </tr>
                        <tr>

                            <td>
                                Remarks <td class="remarks" style="word-wrap: break-word"></td>
                            </td>
                        </tr>
               </table>
	        </div>
	    </div>

@stop