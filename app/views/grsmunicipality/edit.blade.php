@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
			<div class="col-md-12">
				<h4>Edit Installation GRS Municipality</h4>
			</div>
		</div>
	  
	    <div class="row">
		    <div class="col-md-12">
				
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
				{{ Form::open( ['url' => Request::url()]) }}

				<div class="col-md-4 left-border">

				
					<!-- barangay -->
					
					<div class="form-group">
						{{ Form::label('Municipality', 'Municipality') }}
						{{ Form::select('municipality',[ $data->municipality_psgc => $data->municipality], $data->municipality_psgc,array('class' => 'form-control municipal','placeholder' => '','','readonly')) }}		
					</div>

					<div class="form-group">
							{{ Form::label('Province','Province') }}
						{{ Form::text('province',$data->province->province , array('class' => 'form-control province','placeholder' => '','','readonly')) }}
					</div>

					<div class="form-group">
						{{ Form::label('Region','Region') }}
						{{ Form::text('region',  $data->province->region->region_name, array('class' => 'form-control region','readonly')) }}
					</div>

					<div class="form-group hidden">
						{{ Form::label('KC Class','KC Class') }}
						{{ Form::select('kc_class',$kc_class, $data->kc_class, array('class' => 'form-control kc_class')) }}
						
					</div>

					<div class="form-group">
						{{ Form::label('Cycle','Cycle') }}
						{{ Form::select('cycle_id', $cycles, $grsmunicipality->cycle_id, array('class' => 'form-control cycle', 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('Program','Program') }}
						{{ Form::select('program_id',$programs, $grsmunicipality->program_id , array('class' => 'form-control program')) }}
					</div>

				</div>


				<div class="col-md-4 left-border">

					<div class="form-group">
						
						<label for="">Municipal Orientation on GRS Conducted <i class="text-red">required</i></label>
						<div class='input-group date' id='datetimepicker10'>
			                {{ Form::text('date_orientation', toDate($grsmunicipality->date_orientation), array('class' => 'form-control idc date','required')) }}
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>

					<div class="form-group">
						{{ Form::label('Municipal document on MLGU commitment for GRS','Municipal document on MLGU commitment for GRS') }}
						<div class='input-group date'>
							{{ Form::text('date_ffcomm', toDate($grsmunicipality->date_ffcomm), array('class' => 'form-control date')) }}
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>


					<div class="form-group">
						{{ Form::label('GRS Training for ACT/MCT','GRS Training for ACT/MCT') }}
						<div class='input-group date'>

							{{ Form::text('date_training', toDate($grsmunicipality->date_training) , array('class' => 'form-control date')) }}
							<span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>

					<div class="form-group">
						{{ Form::label('Information materials availability with Grievance Hotline','Information materials availability with Grievance Hotline') }}
						<div class='input-group date'>
							{{ Form::text('date_inspect', toDate($grsmunicipality->date_inspect), array('class' => 'form-control date')) }}
							<span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>



					<div class="form-group hidden">
						{{ Form::label('No. of brochures/pamplet','') }}
						{{ Form::input('number','no_brochures', $grsmunicipality->no_brochures, array('class' => 'form-control ')) }}
					</div>
			
					<div class="form-group hidden">
						{{ Form::label('No. of Tarp available','') }}
						{{ Form::input('number','no_tarpauline',  $grsmunicipality->no_tarpauline, array('class' => 'form-control ')) }}
					</div>
				</div>


				<div class="col-md-4 left-border">
					



					<div class="form-group hidden">
						{{ Form::label('No. of posters available','') }}
						{{ Form::input('number','no_posters', $grsmunicipality->no_posters, array('class' => 'form-control')) }}
					</div>

					<div class="form-group hidden">
						{{ Form::label('Others','') }}
						{{ Form::input('number','other_mat', $grsmunicipality->other_mat, array('class' => 'form-control')) }}
					</div>

					<div class="form-group hidden">
						{{ Form::label('Means of Reporting Grievances available','') }}
						{{ Form::text('date_meansrept', toDate($grsmunicipality->date_meansrept), array('class' => 'form-control date')) }}
					</div>


					<div class="form-group">
						
						{{ Form::label('Grievance/Suggestion box installed at Municipal ') }}
						{{ Form::checkbox('is_boxinstalled','1', $grsmunicipality->is_boxinstalled == 1 ? true : false ) }}
					</div>

					<div class="form-group">
						{{ Form::label('Phone Number') }}
						{{ Form::text('phone_no', $grsmunicipality->phone_no, array('class' => 'form-control')) }}
					</div>


					<div class="form-group">
						{{ Form::label('Office Address') }}
						{{ Form::text('address',  $grsmunicipality->address, array('class' => 'form-control')) }}
					</div>
					
					<div class="form-group">
						{{ Form::label('Remarks (for information materials availability) ') }}
						{{ Form::textarea('remarks', $grsmunicipality->remarks, array('class' => 'form-control')) }}
					</div>
						
						<div class="form-group">
								<label>Save as draft</label>
							  <input type="checkbox" name="is_draft" {{ $grsmunicipality->is_draft==1 ? 'checked': '' }}>
							</div>

						{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
						<a href="{{ URL::to('grs-municipality') }}" class="btn bg-navy">close</a>
				</div>
			
				

				{{ Form::close() }}
			</div>
		</div>
		<!-- add js module -->
		<script>
		$('document').ready(function(){

		               $('input[name=date_ffcomm]').data("DateTimePicker").setMinDate(new Date($('input[name=date_orientation]').val()));

        	           $('input[name=date_training]').data("DateTimePicker").setMinDate(new Date($('input[name=date_ffcomm]').val() ));

        	           $('input[name=date_inspect]').data("DateTimePicker").setMinDate(new Date($('input[name=date_training]').val() ));
		});

		        $('input[name=date_orientation]').change(function(){
        	        $('input[name=date_ffcomm]').data("DateTimePicker").setMinDate(new Date($('input[name=date_orientation]').val()));
        	    });
        	    $('input[name=date_ffcomm]').change(function(){
        	        $('input[name=date_training]').data("DateTimePicker").setMinDate(new Date($('input[name=date_ffcomm]').val() ));
        	    });
        	     $('input[name=date_training]').change(function(){
        	        $('input[name=date_inspect]').data("DateTimePicker").setMinDate(new Date($('input[name=date_training]').val() ));
        	     });
		</script>
@stop