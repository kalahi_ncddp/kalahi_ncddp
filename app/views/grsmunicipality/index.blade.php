@extends('layouts.default')

@section('username')
	
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	
	    <div class="row">
		    <div class="col-md-12">
				<!-- Advanced Tables -->
				
				
		    	<div class="panel panel-default">
		    		<div class="panel-heading">
		    			  {{ $title }}
		    		    @if($position->position=='ACT')
                            <span class="btn btn-default pull-right" id="approved">Review</span>
                            <span class="hidden module">grs-municipality</span>
                        @endif
		    			<a class="btn  btn-primary pull-right" href="{{ URL::to('grs-municipality/create') }}">
						<i class="fa fa-plus"></i> Add New
						</a>
						
						<div class="clearfix"></div>
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
                                    @if($position->position=='ACT')
                                    <th>Select all
                                        <input type="checkbox" id="selecctall"/>
                                    </th>
                                    @endif
									<th>Cycle ID</th>
										<th>Region Abbrev</th>
										<th>Province</th>
										<th>Municipality</th>
										<th>Program</th>
										<th>Details</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($grsmunicipalities as  $grsmunicipality) 
										<tr>
										     @if($position->position=='ACT')
										    <td>
                                               @if($grsmunicipality->is_draft==0)
                                                 {{ $reference->approval_status($grsmunicipality->reference_no , 'ACT') }}
                                               @else
                                                   <span class="label label-warning">draft</span>
                                               @endif
										    </td>
                                             @endif
											<td>
												{{ $grsmunicipality->cycle_id }}
											</td>
											<td>
												{{ $grsmunicipality->municipality->province['region']['region_abbr'] }}
											</td>
											<td>
												{{ $grsmunicipality->municipality->province['province'] }}
											</td>
											<td>
												{{ $grsmunicipality->municipality['municipality'] }}
											</td>
										
											<td>
												{{ isset($grsmunicipality->program_id) ? $grsmunicipality->program_id : 'not yet available' }}
											</td>

                                            <td>
                                                @if($grsmunicipality->is_draft==0)
                                                    <a class="btn btn-success" href="{{ URL::to('grs-municipality/'.$grsmunicipality->reference_no) }}"><i class="fa fa-eye"></i> &nbsp; View details</a>
                                                    {{--<button class="btn btn-success btn view-muni" data-psgc="{{ $grsmunicipality->psgc_id }}" data-cycle="{{ $grsmunicipality->cycle_id }}">--}}
                                                     {{--<i class="fa fa-eye"></i> View Details--}}
                                                    {{--</button>--}}
                                                @else
                                                    <a class="btn btn-warning" href="{{ URL::to('grs-municipality/'.$grsmunicipality->reference_no) }}"><i class="fa fa-eye"></i> &nbsp; View details ( draft )</a>
                                                    {{-- <button class="btn btn-warning btn view-muni" data-psgc="{{ $grsmunicipality->psgc_id }}" data-cycle="{{ $grsmunicipality->cycle_id }}"> --}}
                                                    {{-- <i class="fa fa-eye"></i>  View Details( draft ) --}}
                                                    {{-- </button> --}}
                                                @endif
                                           </td>



										
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- lets add show modal ref to show url -->
		 <div class="modal fade" id="show-municipality" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body">
                    	
                    		<table class="table table-bordered barangay_list" style="table-layout: fixed">
                    			<tr>
                    				<td>Municipal Orientation on GRS Conducted </td>
                    				<td class="date_orientation"  style="word-wrap: break-word"></td>
                    			</tr>
                    			<tr>
                    				<td>Fact Finding Committee Established</td>
                    				<td class="date_ffcomm"  style="word-wrap: break-word"></td>
                    			</tr>
                    			<tr>
	                    			<td>
	                    				Fact Finding Committee Established
	                    			</td>
	                    				<td class="date_ffcomm"  style="word-wrap: break-word"></td>
                    			</tr>
                    			<tr>
	                    			<td>
	                    				GRS Training for ACT/MCT 
	                    			</td>
	                    			<td class="date_training"  style="word-wrap: break-word"></td>
                    			</tr>
                    			<tr>
                    			<td>
                    				Information materials availability with Grievance Hotline
                    			</td>
                    				<td class="date_inspect"  style="word-wrap: break-word"></td>
                    			</tr>
                    			<tr class="hidden">
                    				<td>
                    				Means of Reporting Grievances available <td class="date_meansrept"  style="word-wrap: break-word"></td>
                    				</td>
                    			</tr>
                    			
                    			<tr>
                    				
	                    			<td>
	                    				Grievance/Suggestion box installed at barangay <td class="is_boxinstalled"  style="word-wrap: break-word"></td>
	                    			</td>
                    			</tr>
                    			<tr>
                    				
                    			<td>
 								     	Phone Number <td class="phone_no" style="word-wrap: break-word" ></td>
                    			</td>
                    			</tr>
                    			<tr>
                    				
	                    			<td>
	                    				Office Address <td class="address" style="word-wrap: break-word"></td>
	                    			</td>
                    			</tr>
                    			<tr>
                    				
	                    			<td>
	                    				Remarks <td class="remarks" style="word-wrap: break-word"></td>
	                    			</td>
                    			</tr>
                    		</table>
                    	
					</div>
					<div class="modal-footer">

					</div>
	            </div>
            </div>
        </div>
	<script>
      $(document).ready(function () {
       $('#dataTables-example').dataTable({
            "aoColumnDefs": [
                      { 'bSortable': false, 'aTargets': [ 0 ] }
                   ]});
        $('#selecctall').click(function(event) {  //on click
            if(this.checked) { // check select status
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"
                });
            }else{
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"
                });
            }
        });
        // approved certeain documents
        $('#approved').on('click',function(){
               var c = confirm('Approve the selected documents?')
               if(c)
               {
                   var arr = [],program=[],cycle =[];
                   $('.checkbox1').each(function(i) {
                        arr[i] = $(this).val();
                   });
                   $('.cycle').each(function(i) {
                        cycle[i] = $(this).val();
                   });
                   $('.program').each(function(i) {
                        program[i] = $(this).val();
                   });
                   $.get('{{ URL::to('reference_approved_grs') }}',{ 
                   		'check' : arr,
                   		'program' : program,
                   		'cycle' : cycle,
                   		'reference_type' : 'grsmuni' },function(res){
                        // window.location.reload();
                   });
               }
        });
      });
    
    </script>
@stop