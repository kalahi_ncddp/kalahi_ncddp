@extends('layouts.default')

@section('content')
	    <div class="row">
		    <div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="col-xs-5" style="padding-left: 0;">  {{ $title }}</h4>
						@if($position->position=='ACT')
                    <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                    <span class="hidden module">spi-munsusplan</span>
             @endif
						<a id="spi-munsusplan_modal-add_show" class="btn btn-primary pull-right" 
							href="">
							<i class="fa fa-plus"></i> Add New Sustainability Plan
						</a>
						<div class="clearfix"></div>
					</div>
					<div class="panel-body">

						@if ($store_status !== null)
							@if ($store_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> A New Sustainability Plan was successfully added!</strong>
								</div>
							@elseif ($store_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Sustainability Plan can not be added</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if ($update_status !== null)
							@if ($update_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> Sustainability Plan was successfully updated!</strong>
								</div>
							@elseif ($update_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Sustainability Plan can not be updated</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if ($destroy_status !== null)
							@if ($destroy_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> Sustainability Plan was successfully removed!</strong>
								</div>
							@elseif ($destroy_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Sustainability Plan can not be removed</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif

						<table class="table table-striped table-bordered" id="spi-commtschklst_table">
							<thead>
								<tr>
									<th>
		                            @if($position->position=='ACT')
		                            <input type="checkbox" id="selecctall"/>
		                            @endif
		                            </th>
									<th style="width: 10%;">Cycle</th>
									<th style="width: 10%;">Program</th>
									<th style="width: 10%;">Date Accomplished</th>
									<th style="width: 40%;">Remarks</th>
									<th style="width: 20%; text-align: center;">Action</th>
								</tr>
							</thead>
							<tbody>

							@foreach ($plans as $plan)
								<tr data-rowid="{{{$plan['id']}}}">
									<td>
                               @if($position->position=='ACT')
                                  @if($plan->is_draft==0)
                                    {{ $reference->approval_status($plan->id, 'ACT') }}
                                  @else
                                      <span class="label label-warning">draft</span>
                                  @endif
                                @endif
                            </td>
									<td class="cell-cycleid" data-value="{{$plan['cycle_id']}}">{{$plan['cycle_id']}}</td>
									<td class="cell-programid" data-value="{{$plan['program_id']}}">{{$plan['program_id']}}</td>
									<td class="cell-datecreated" data-value="{{$plan['date_created']}}" style="text-align: center;">
										{{$plan['date_created']}}
									</td>
									<td class="cell-remarks" data-value="{{$plan['remarks']}}">
										{{$plan['remarks']}}
									</td>
									<td style="text-align: center">
										@if( !is_review($plan->id) )
										<a href="" class="btn btn-sm btn-info spi-munsusplan_modal-edit_show" style="margin-right: 0.5rem;"><i class="glyphicon glyphicon-edit"></i>Edit</0>
										<a href="" class="btn btn-sm btn-danger spi-munsusplan_modal-del_show"><i class="glyphicon glyphicon-trash"></i>Delete</a>
										@endif
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<!-- lets a
			<a href="" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-pencil"></i></a>dd show modal ref to show url -->


		<div class="modal fade" id="spi-munsusplan_modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                       	

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">New Sustainability Plan</h4>
                    </div>
                    <div class="modal-body">
						{{ Form::open(array('id' => 'spi-munsusplan_modal_form', 'route' => 'spi-munsusplan.store')) }}
							<div class="form-group">
								<label for="in-province">Province</label>
								<input type="text" id="in-province" name="in-province" class="form-control" 
									value="{{{$province}}}" readonly="readonly">
							</div>
							<div class="form-group">
								<label for="in-municipalityname">Municipality</label>
								<input type="text" id="in-municipalityname" name="in-municipalityname" class="form-control"
									value="{{{$municipality}}}" readonly="readonly">
								<input type="hidden" id="in-municipalitypsgc" name="in-municipalitypsgc" value="{{{$municipality_psgc}}}">
							</div>
							<div class="form-group">
								<label for="in-cycleid">Cycle</label>
								<input type="text" id="in-cycleid" name="in-cycleid" class="form-control"
									value="{{{$cycle}}}" readonly="readonly">
							</div>
							<div class="form-group">
								<label for="in-programid">Program</label>
								<input type="text" id="in-programid" name="in-programid" class="form-control" 
									value="{{{$program}}}" readonly="readonly">
							</div>
						    <div class="form-group">
							 	<label for="in-datecreated_cont">Date</label>
								<div class='input-group date' id='in-datecreated_cont'>
				                	<input class="form-control" required="required" type="text" 
				                	   id="in-datecreated" name="in-datecreated" value="{{date('m/d/Y')}}"> <br>
				                	<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					            </div>
		  				    </div>
						    <div class="form-group">
								<label for="in-remarks">Remarks</label>
								<!-- <textarea rows="2" class="form-control" placeholder="Put your remarks here..."
										  id="in-remarks" name="in-remarks">
								</textarea> -->
								<textarea class="form-control" rows="5" id="in-remarks" name="in-remarks" placeholder="Put your remarks here..."></textarea>		  
							</textarea>
						    </div>
						{{ Form::close() }}
					</div>
					<div class="modal-footer">
						<a id="spi-munsusplan_modal_form_submit" href="" class="btn btn-primary">
							<i class="glyphicon glyphicon-floppy-disk"></i> Submit
						</a>
						<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
					</div>
	            </div>
            </div>
        </div>

		<div class="modal fade" id="spi-munsusplan_modal-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
						<form>
							<input type="hidden" id="del-id" name="del-id" value="">
							<p class="col-xs-2" style="text-align: right;">
								<i class="glyphicon glyphicon-question-sign" style="color: #f4543c; font-size: 30px;"></i>
							</p>
							<p class="col-xs-10" style="font-size: 15px; padding-top: 5px; padding-bottom: 5px;">Are you sure you want to delete 
								 this sustainability plan?</p>
							<div class="clearfix"></div>
						</form>
					</div>
					<div class="modal-footer">
						<a id="spi-munsusplan_modal_delform_submit" href="" class="btn btn-danger">Yes</a>
						<a href="" class="btn bg-navy" data-dismiss="modal">No</a>
					</div>
	            </div>
            </div>
        </div>


		<div class="modal fade" id="spi-munsusplan_modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Update Municipal Sustainability Plan</h4>
                    </div>
                    <div class="modal-body">
						<form id="spi-munsusplan_editmodal_form">
							<input type="hidden" id="edit-id" name="edit-id" value="">
							<div class="form-group">
								<label for="edit-province">Province</label>
								<input type="text" id="edit-province" name="edit-province" class="form-control" 
									value="{{{$province}}}" readonly="readonly">
							</div>
							<div class="form-group">
								<label for="edit-municipalityname">Municipality</label>
								<input type="text" id="edit-municipalityname" name="edit-municipalityname" class="form-control"
									value="{{{$municipality}}}" readonly="readonly">
								<input type="hidden" id="edit-municipalitypsgc" name="edit-municipalitypsgc" value="{{{$municipality_psgc}}}">
							</div>
							<div class="form-group">
								<label for="edit-cycleid">Cycle</label>
								<input type="text" id="edit-cycleid" name="edit-cycleid" class="form-control"
									value="{{{$cycle}}}" readonly="readonly">
							</div>
							<div class="form-group">
								<label for="edit-programid">Program</label>
								<input type="text" id="edit-programid" name="edit-programid" class="form-control" 
									value="{{{$program}}}" readonly="readonly">
							</div>
						    <div class="form-group">
							 	<label for="edit-datecreated_cont">Date</label>
								<div class='input-group date' id='edit-datecreated_cont'>
				                	<input class="form-control" required="required" type="text" 
				                	   id="edit-datecreated" name="edit-datecreated" value="{{date('m/d/Y')}}"> <br>
				                	<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					            </div>
		  				    </div>
						    <div class="form-group">
								<label for="edit-remarks">Remarks</label>
								<textarea rows="2" class="form-control" placeholder="Put your remarks here..."
										  id="edit-remarks" name="edit-remarks">
								</textarea>
						    </div>
						    <div class="form-group">
								<label>Save as draft</label>
							  <input type="checkbox" name="is_draft" >
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<a id="spi-munsusplan_modal_editform_submit" href="" class="btn btn-primary">Submit</a>
						<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
					</div>
	            </div>
            </div>
        </div>

	<script>
      $(document).ready(function () {
        $('#spi-commtschklst_table').dataTable();

        /*
         *  ADDING a Municipal Sustainability Plan
         */
		$('#spi-munsusplan_modal-add_show').on('click', function(event)
		{
			$('#spi-munsusplan_modal-add').modal('show');
			return false;
		});

		$('#spi-munsusplan_modal_form_submit').on('click', function()
		{
			$.post( window.location.href, $( "#spi-munsusplan_modal_form" ).serialize())
			.done(function(msg){
				$('#spi-munsusplan_modal-add').modal('hide');
				window.location = window.location.href;
			});

			return false;
		});
		/* end of ADDING a committee */
        /*
         *  UPDATING a committee
         */
		$('.spi-munsusplan_modal-edit_show').on('click', function(event){
			var $row = $(event.currentTarget).parents('tr');
			$('#edit-id').val($row.data('rowid'));
			$('#edit-cycleid').val($row.find('td.cell-cycleid').data('value'));
			$('#edit-programid').val($row.find('td.cell-programid').data('value'));
			$('#edit-datecreated').val($row.find('td.cell-datecreated').data('value'));									
			$('#edit-remarks').val($row.find('td.cell-remarks').data('value'));

			$('#spi-munsusplan_modal-edit').modal('show');
			return false;
      	});		

		$('#spi-munsusplan_modal_editform_submit').on('click', function(event)
		{
			var $url = window.location.href + '/' + $('#edit-id').val();
			console.log($url);
			
			$.ajax({
		  		type: "PUT",
		  		url:  $url,
		  		data: $( "#spi-munsusplan_editmodal_form" ).serialize()
			})
		  	.done(function( msg ) {
		    	$('#spi-munsusplan_modal-edit').modal('hide');
		    	window.location = window.location.href;
		  	});

			return false;
		});
		/* end of UPDATING a committee */
        /*
         *  DELETING a committee
         */
		$('.spi-munsusplan_modal-del_show').on('click', function(event){
			var id = $(event.currentTarget).parents('tr').data('rowid');
			$('#del-id').val(id);
			$('#spi-munsusplan_modal-del').modal('show');
			return false;
      	});

		$('#spi-munsusplan_modal_delform_submit').on('click', function(event)
		{
			var $url = window.location.href + '/' + $('#del-id').val();
			console.log($url);

			$.ajax({
		  		type: "DELETE",
		  		url: $url
			})
		  	.done(function( msg ) {
		    	$('#spi-munsusplan_modal-del').modal('hide');
		    	window.location = window.location.href;
		  	});

			return false;
		});
		/* end of DELETING a committee */
	});
    </script>
@stop