@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('spi_profile') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    {{ HTML::linkRoute('asset.index', 'Back to Asset List',
      null, array('class' => 'btn btn-default btn')) }}
    {{ HTML::linkRoute('asset.edit', 'Edit',
      array($asset->property_no), array('class' => 'btn btn-info btn')) }}
  </div>
</div>

<hr />

<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="30%">Property No./Asset</th>
					<td>{{ $asset->property_no }}</td>
				</tr>
				<tr>
					<th>Part No./Serial</th>
					<td>{{ $asset->serial_no }}</td>
				</tr>
				<tr>
					<th>Description</th>
					<td>{{ $asset->description }}</td>
				</tr>
				<tr>
					<th>Date Acquired</th>
					<td>{{ toDate($asset->date_acquired) }}</td>
				</tr>
				<tr>
					<th>Quantity</th>
					<td>{{ $asset->quantity }}</td>
				</tr>
				<tr>
					<th>Unit</th>
					<td>{{ $asset->unit }}</td>
				</tr>
				<tr>
					<th>Level of Issuance</th>
					<td>{{ $asset->level_issuance }}</td>
				</tr>
				<tr>
					<th>Issued To</th>
					<td>{{ $issued_to_name }}</td>
				</tr>
				<tr>
					<th>Position</th>
					<td>{{ $issued_to_position }}</td>
				</tr>
				<tr>
					<th>PO Number</th>
					<td>{{ $asset->po_number }}</td>
				</tr>
				<tr>
					<th>Reference Number/Invoice</th>
					<td>{{ $asset->reference_no }}</td>
				</tr>
				<tr>
					<th>Acquisition Cost</th>
					<td>{{ $asset->acquisition_cost }}</td>
				</tr>
				<tr>
					<th>Remarks</th>
					<td>{{ $asset->remarks }}</td>
				</tr>
				<tr>
					<th>Status of Equipment</th>
					<td>{{ $asset->status }}</td>
				</tr>
				<tr>
					<th>Estimated Useful Life</th>
					<td>
						<div class="input-group">
							{{ $asset->useful_life }} years
						</div>
					</td>
				</tr>
				<tr>
					<th>Salvage Value</th>
					<td>{{ $asset->salvage_value }}</td>
				</tr>
				<tr>
					<th>Balance</th>
					<td>{{ $asset->balance() }}</td>
				</tr>
				<tr>
					<th>Fund Source</th>
					<td>{{ $asset->fund_source }}</td>
				</tr>
				<tr>
					<th>PAR (Property Accountability Receipt)</th>
					<td>{{ $asset->par }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		
	});

	function round_off(num, places) {
		var dec = Math.pow(10, places); 
  	return Math.round(num * dec)/dec;
	}
</script>
@stop