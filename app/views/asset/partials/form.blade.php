<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="30%">Property No./Asset</th>
					<td>{{ Form::text('property_no', $asset->property_no, ['class' => 'form-control', 'required']) }}</td>
				</tr>
				<tr>
					<th>Part No./Serial</th>
					<td>{{ Form::text('serial_no', $asset->serial_no, ['class' => 'form-control', 'required']) }}</td>
				</tr>
				<tr>
					<th>Description</th>
					<td>{{ Form::textarea('description', $asset->description, ['class' => 'form-control', 'size' => '30x3', '']) }}</td>
				</tr>
				<tr>
					<th>Date Acquired</th>
					<td>{{ Form::text('date_acquired', $asset->date_acquired, array('class' => 'form-control date','max' => date("m/d/Y"), 'required')) }}</td>
				</tr>
				<tr>
					<th>Quantity</th>
					<td>{{ Form::input('number', 'quantity', $asset->quantity, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
				</tr>
				<tr>
					<th>Unit</th>
					<td>{{ Form::text('unit', $asset->unit, ['class' => 'form-control', 'required']) }}</td>
				</tr>
				<tr>
					<th>Level of Issuance</th>
					<td>{{ Form::select('level_issuance', $level_list, $asset->level_issuance, ['class' => 'form-control', 'required']) }}</td>
				</tr>
				<tr>
					<th>Issued To</th>
					<td>{{ Form::select('issued_to', $staff_list, $asset->issued_to, ['class' => 'form-control', 'required']) }}</td>
				</tr>
				<tr>
					<th>PO Number</th>
					<td>{{ Form::text('po_number', $asset->po_number, ['class' => 'form-control', 'required']) }}</td>
				</tr>
				<tr>
					<th>Reference Number/Invoice</th>
					<td>{{ Form::text('reference_no', $asset->reference_no, ['class' => 'form-control', 'required']) }}</td>
				</tr>
				<tr>
					<th>Acquisition Cost</th>
					<td>{{ Form::input('number', 'acquisition_cost', $asset->acquisition_cost, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
				</tr>
				<tr>
					<th>Remarks</th>
					<td>{{ Form::textarea('remarks', $asset->remarks, ['class' => 'form-control', 'size' => '30x3', '']) }}</td>
				</tr>
				<tr>
					<th>Status of Equipment</th>
					<td>{{ Form::text('status', $asset->status, ['class' => 'form-control', 'required']) }}</td>
				</tr>
				<tr>
					<th>Estimated Useful Life</th>
					<td>
						<div class="input-group">
							{{ Form::input('number', 'useful_life', $asset->useful_life, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}
							<span class="input-group-addon">years</span>
						</div>
					</td>
				</tr>
				<tr>
					<th>Salvage Value</th>
					<td>{{ Form::input('number', 'salvage_value', $asset->salvage_value, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
				</tr>
				<tr>
					<th>Fund Source</th>
					<td>{{ Form::text('fund_source', $asset->fund_source, ['class' => 'form-control', 'required']) }}</td>
				</tr>
				<tr>
					<th>PAR (Property Accountability Receipt)</th>
					<td>{{ Form::text('par', $asset->par, ['class' => 'form-control', 'required']) }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>

{{ Form::submit('Save Asset', array('class' => 'btn btn-primary')) }}
{{ HTML::linkRoute('asset.index', 'Close',
  null, array('class' => 'btn bg-navy btn')) }}
{{ Form::close() }}

<script>
	$(document).ready(function() {
		
	});

	function round_off(num, places) {
		var dec = Math.pow(10, places); 
  	return Math.round(num * dec)/dec;
	}
</script>