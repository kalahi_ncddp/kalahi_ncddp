@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')	
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
	@endif
	<div class="panel panel-default">
		<div class="panel-heading">
			Records
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover data-table">
					<thead>
						<tr>
							<th width="25%">Property No./Asset</th>
							<th width="25%">Part No./Serial</th>
							<th width="25%">Issued To</th>
							<th width="15%">Date Acquired</th>
							<th width="10%">Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($assets as $asset)
						<tr>
							<td>{{ $asset->property_no }}</td>
							<td>{{ $asset->serial_no }}</td>
							<td>{{ $asset->issued_to }}</td>
							<td>{{ toDate($asset->date_acquired) }}</td>
							<td>
								{{ HTML::linkRoute('asset.show', 'View Details',
									array($asset->property_no), array('class' => 'btn btn-success btn')) }}
	            </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<div class="panel-footer text-right">
			{{ HTML::decode(HTML::linkRoute('asset.create', "<i class='fa fa-plus'></i> Add Asset",
        null, array('class' => 'btn btn-info btn'))) }}
		</div>
	</div>
		
	 <style type="text/css" media="screen">
    table { border-collapse: collapse; border: solid thin black; }
    table th { cursor: move; }

    movable-columns-hover { background-color: #fff7d9; }
  </style>
  <script type="text/javascript">
    $(function() {
      $('table').movableColumns();
      // click also work, so that you can still sort your column:
      $('th').click(function(){ window.console && console.log('clicked') });
    });
  </script>
@stop