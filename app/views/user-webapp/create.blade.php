@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
<div class="col-md-12">
        @if ($errors->all())
                    <div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
                @endif
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add Users</h3>
        </div>
        <div class="box-primary">
            <div class="col-md-12">

                {{ Form::open(array('url' => 'user-admin')) }}
                    <div class="form-group">
                        <label for="">Username</label>
                        {{ Form::text('username','',array('class'=>'form-control')) }}
                    </div>
                    <div class="form-group">
                        <label for="">first name</label>
                        {{ Form::text('first_name','',array('class'=>'form-control')) }}
                    </div>

                    <div class="form-group">
                        <label for="">last name</label>
                        {{ Form::text('last_name','',array('class'=>'form-control')) }}
                    </div>

                    <div class="form-group">
                        <label for="">middle name</label>
                        {{ Form::text('middle_name','',array('class'=>'form-control')) }}
                    </div>

                    <div class="form-group">
                        <label for="">Password</label>
                        {{ Form::text('password','',array('class'=>'form-control')) }}
                    </div>
                    

                    <div class="form-group">
                        <label for="">User Level: {{ $title_in_select }}</label>
                        {{ Form::select('psgc_id',$psgc,'',array('class'=>'form-control','')) }}
                    </div>


                    <div class="form-group">
                        <label for=""></label>
                    </div>
                    <div class="form-group">
                        {{ Form::submit('Add',array('class'=>'btn btn-primary')); }}
                    </div>
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop
