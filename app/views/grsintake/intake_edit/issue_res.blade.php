
<div class="row">
    <div class="col-md-12">
        <h4>Resolution Details</h4>
        <hr/>
        <div class="col-md-6">
            <div class="form-group">

                <label for="type">Status of Resolution</label>
                {{ Form::select('resolution_status',$status_resolution,$intake->resolution_status,array('class'=>'form-control resolution','required')) }}
            </div>
            <div class="form-group date-resolved">
                <label for="Category">Date Resolved</label>
                {{ Form::text('resolution_date',toDate($intake->resolution_date),array('class'=>'form-control date',''))  }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Complainant's Feedback on Resolution of Grievance</label>
                {{ Form::select('resolution_level',$status_level,$intake->resolution_level,array('class'=>'form-control')) }}

                <!-- {{ Form::text('resolution_level',$intake->resolution_level,array('class'=>'form-control','required')) }} -->
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <h4>Details of Issue/Concern</h4>
        <hr/>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Nature of Issue/Concern</label>
                {{ Form::select('concern_nature',[''=>'Select Issue/Concern']+$concern_nature,$intake->concern_nature,array('class'=>'form-control','required'))  }}
            </div>

            <div class="form-group">
                <label for="">Category of Concern</label>
                {{ Form::select('concern_category',[''=>'Select Concern']+$concern_category,$intake->concern_category,array('class'=>'form-control')) }}
            </div>

            <div class="form-group">
                <label for="">Subject of Complaint</label>
                {{ Form::select('subject_complaint',[''=>'Select Subject of Complaints']+$subject_complaint,$intake->subject_complaint,array('class'=>'form-control')) }}
            </div>
            <div class="form-group">
                <label for="">Action Taken/Resolution</label>
                {{ Form::textarea('actions',$intake->actions,array('class'=>'form-control actions')) }}
            </div>
        </div>
        <div class="col-md-6">

            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Details/Narrative Summary</label>
                    {{ Form::textarea('narrative_summ',$intake->narrative_summ,array('class'=>'form-control','style'=>'margin: 0px -5px 0px 0px; height: 178px; width: 543px;')) }}
                </div>


                <div class="form-group">
                    <label for="">Recommendations</label>
                    {{ Form::textarea('recommendations',$intake->recommendations,array('class'=>'form-control recommendations')) }}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">

                </div>



                <div class="clearfix"></div>