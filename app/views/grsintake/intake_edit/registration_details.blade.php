               <div class="col-md-12">

                    <div class="form-group ">
                         <span class="btn bg-orange pull-right" data-toggle="modal" data-target="#intake">use pincos and grievances</span>
                         <br/>
                        <label for="">Municipality</label>
                        {{ Form::select('psgc_id',$municipalities,$municipality,array('class'=>'form-control','readonly')) }}
                    </div>

                    <div class="form-group">
                        <label for="">Region</label>
                        {{ Form::select('region_id',$regions,$region,array('class'=>'form-control','readonly','disabled')) }}
                    </div>

                    <div class="form-group">
                        <label for="">Barangay</label>
                        {{ Form::select('barangay_id',$barangay, $intake->barangay_psgc,array('class'=>'form-control barangay','','') ) }}
                    </div>

                    <div class="form-group">
                        <label for="">Cycle</label>
                        {{ Form::select('cycle_id',$cycles,$intake->cycle_id,array('class'=>'form-control')) }}
                    </div>

                    <div class="form-group">
                        <label for="">Program</label>
                        {{ Form::select('program_id',$programs,$intake->program_id,array('class'=>'form-control'))  }}
                    </div>

               </div>


               <div class="clearfix"></div>

