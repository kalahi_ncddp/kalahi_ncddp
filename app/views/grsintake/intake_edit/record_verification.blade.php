                <div class="row">
                    <div class="col-md-12">
                        <h4>Registration Details</h4>
                        <hr/>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="">Grievance Form</label>
                                {{ Form::select('grievance_form',$grievance_form,$intake->grievance_form,array('class'=>'form-control grievance_form','required')) }}
                            </div>
                            <div class="form-group">
                                <label for="">Intake Level</label>
                                {{ Form::select('intake_level',$intake_level,empty($intake->intake_level) ? 'municipal':$intake->intake_level,array('class'=>'form-control','required')) }}
                            </div>


                            <div class="form-group">
                                <label for="">Intake Officer</label>
                                {{ Form::text('intake_officer',$intake->intake_officer,array('class'=>'form-control ' ,'maxlength'=>'40'))  }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Designation</label>
                                {{ Form::text('off_desigination',$intake->off_desigination,array('class'=>'form-control','maxlength'=>'40'))  }}
                            </div>

                            <div class="form-group">
                                <label for="">Mode of Filing</label>
                                {{ Form::select('filing_mode',$mode_of_filling,$intake->filing_mode,array('class'=>'form-control','required')) }}
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
               </div>

               <div class="row">
                   <div class="col-md-12">
                       <h4>Complainant/Sender's Profile</h4>
                       <hr/>
                       <div class="col-md-6">

                           <div class="form-group">
                               <label for="">Sender</label>
                               {{ Form::text('sender',$intake->sender,array('class'=>'form-control','maxlength'=>'40','readonly'))  }}
                           </div>

                           <div class="form-group">
                               <label for="">Sex</label>
                               {{ Form::select('sender_sex',$genders,$intake->sender_sex,array('class'=>'form-control'))  }}
                           </div>

                           <div class="form-group">
                               <label for="">Group/ Organization/Institution</label>
                               {{ Form::textarea('sender_org',$intake->sender_org,array('class'=>'form-control','maxlength'=>'40')) }}
                           </div>


                       </div>

                       <div class="col-md-6">
                           <div class="form-group">
                               <label for="">Designation</label>
                               {{ Form::select('sender_designation',$designations,$intake->sender_designation,array('class'=>'form-control')) }}
                           </div>
                           <div class="form-group">
                               <label for="">IP</label>

                               {{ Form::select('sender_designation',[""=>"Is IP?",1=>'Yes', 0=>'No'],$intake->is_sender_ip,array('class'=>'form-control'))}}
                           </div>

                           <div class="form-group">
                               <label for="">IP Groupname</label>
                               {{ Form::select('sender_ipgroup',$ip_groupname,$intake->sender_ipgroup,array('class'=>'form-control')) }}
                           </div>

                           <div class="form-group">
                               <label for="">Contact Information</label>
                               {{ Form::number('sender_contact',$intake->sender_contact,array('class'=>'form-control'))  }}
                           </div>
                       </div>
                   </div>
               </div>
             <div class="clearfix"></div>