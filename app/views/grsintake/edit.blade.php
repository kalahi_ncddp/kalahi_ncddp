@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
 {{ Form::model($intake, array('action' => array('GrsIntakeController@update', $intake->reference_no), 'method' => 'PATCH')) }}
    <div class="col-md-12">
    <a class="btn btn-default" href="{{ URL::to('grs-intake/'.$intake->reference_no) }}">Go Back</a>

    </div>
 <br/>
 <br/>
 <div class="col-md-12">
                        <div class="box box-primary">
                             <div class="box-header">
                                  <h3 class="box-title">GRS Registration Details</h3>
                                   <div class="box-tools pull-right">
                                         <span class="btn bg-orange" data-toggle="modal" data-target="#intake">Look up for Grievances</span>
                                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                                   </div>

                             </div>
                             <div class="box-body">
                                 @include('grsintake/intake_edit/record_verification')
                             </div>
                         </div>

                       <div class="box box-primary">
                            <div class="box-header">
                                 <h3 class="box-title">Issue and Resolution</h3>
                                 <div class="box-tools pull-right">
                                       <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                                  </div>
                            </div>
                            <div class="box-body">
                                  @include('grsintake/intake_edit/issue_res')
                            </div>
                        </div>
                          <div class="box box-primary">
                               <div class="box-header">
                                    <h3 class="box-title">Actions Taken, Status</h3>
                                    <div class="box-tools pull-right">
                             <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                        </div>
                  </div>
                  <div class="box-body">
                        @include('grsintake/intake_edit/action')
                  </div>
              </div>
       </div>

          <div class="col-md-12 ">
            <div class="row">
               <div class="form-group pull-right">
                    <label>Save as draft</label>
                    <input type="checkbox" name="is_draft" {{ $intake->is_draft==1 ? 'checked': '' }}>
                  </div>
                  <div class="clearfix"></div>

                <div class="form-group pull-right">
                    <button class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        Save
                    </button>
                    <a class="btn btn-default" href="{{ URL::to('grs-intake') }}">Close</a>

                </div>
            </div>
           </div>
    </div>
    @include('modals/search_pincos')
    <script>
        $(document).ready(function(){
            $('.resolution').change(function(){
                if($(this).val()=="Resolved"){
                    $('.date-resolved').fadeIn();
                    $('.actions').attr('required','required');
                }

                else{

                    $('.date-resolved').fadeOut();
                    $('.actions').removeAttr('required');
                }

            });
            $('.date').datetimepicker({pick12HourFormat: false, pickTime:false});
            $('.date-resolved').fadeOut();
//            $('form').submit(function(e){
//                var recordVerifacation = $('#record_verification'),
//                    registration_details = $('#registration_details'),
//                    issue_resolution    = $('#issue_resolution'),
//                    action = $('#action');
//
//                var rv = "",rd = "" , ir = "", a = "";
//                $(this).find('.form-control').each(function(i,elem){
//                        if(elem.required) {
//                            if($(this).val() == ""){
//                               rv += $(this).siblings('label').text() + " is required \n";
//                            }
//                        }
//                });
//
//                if(rv==""){
//                  var confirmation = confirm('Are you sure that do you want to submit this?');
//                  if(confirmation){
//                    return true;
//
//                  }else{
//                    return false;
//                  }
//                }else{
//                  alert(rv);
//                }
//
//
////                if(rd==""){
////  						console.log('w');
//////  						var confirmation = confirm('Are you sure that do you want to submit this?');
//////  						if(confirmation){
//////  							return true;
//////
//////  						}else{
//////  							return false;
//////  						}
////  				}else{
////  						alert(rd);
////
////  				}
//
//                e.preventDefault();
//            });

            $('.select-pincos').on('click',function() {
              var activity_id = $(this).data('id');
              var issue       = $(this).data('issue');

              $.get('{{ URL::to('') }}/pincos-grievances',{activity_id:activity_id,issue_no:issue},function(res){
                     $('select[name="grievance_form"]').val('pincos');
                     $('textarea[name="narrative_summ"]').val(res.issue);
                     $('textarea[name="narrative"]').eq(0).val(res.action);
                     $('#intake').modal('hide');
              });
            })

        });

       $(function(){
              var hash = window.location.hash;
              hash && $('ul.nav a[href="' + hash + '"]').tab('show');

              $('.nav-tabs a').click(function (e) {

                    $(this).tab('show');
                    var scrollmem = $('body').scrollTop();
                    window.location.hash = this.hash;
                    $('html,body').scrollTop(scrollmem);
              });
       });
    </script>
@stop
