@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">

            {{ Form::open(array('url' => 'grs-intake/' . $intake->reference_no)) }}
          {{ Form::hidden('_method', 'DELETE') }}
           <a class="btn btn-default" href="{{ URL::to('grs-intake') }}">Close</a>
        <div class="form-group pull-right">
          
           {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
           <a class="btn  btn-small btn-info" href="{{ URL::to('grs-intake/' .$intake->reference_no.'/edit') }}">Edit</a>
           <a class="btn bg-olive" href="#" id="print">Print (printer version friendly)</a>
        
        </div>
        <br/>
        <br/>
    </div>

</div>
  
    
        <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">GRS Registration Details</h3>
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Region  :</label>
                       {{ getRegion($intake->psgc_id)  }}
                    </div>
                    <div class="form-group">
                        <label for="">Province  :</label>
                       {{ getProvince($intake->psgc_id)  }}
                    </div>

                    <div class="form-group ">

                        <label for="">Municipality  :</label>
                       {{ getMunicipality($intake->psgc_id)  }}
                    </div>

                    <div class="form-group">
                        <label for="">Barangay  :</label>
                       {{ getBarangay($intake->psgc_id)  }}
                    </div>

                    <div class="form-group">
                        <label for="">Cycle  :</label>
                        {{ $intake->cycle_id }}
                    </div>

                    <div class="form-group">
                        <label for="">Program :</label>
                        {{ $intake->program_id }}
                    </div>
                    <div class="form-group">
                        <label for="">Grievance Form: </label>
                        <span style="text-transform: capitalize">{{ $intake->grievance_form }}</span>
                    </div>
                    <div class="form-group">
                        <label for="">Intake Level: </label>
                        {{ $intake->intake_level }}
                    </div>
                    <div class="form-group">
                        <label for="">Intake Date: </label>
                        <span style="text-transform: capitalize">{{ toDate( $intake->intake_date )}}</span>
                    </div>

                    <div class="form-group">
                        <label for="">Intake Officer: </label>
                        <span style="text-transform: capitalize">{{ $intake->intake_officer }}</span>
                    </div>

                    <div class="form-group">
                        <label for="">Designation: </label>
                        {{ $intake->off_desigination }}
                    </div>

                    <div class="form-group">
                        <label for="">Mode of Filing: </label>
                        {{ $intake->filing_mode }}
                    </div>
                </div>
                <div class="col-md-6">
                   <h4>Complainant/Sender's Profile</h4>
                   <hr/>
                   <div class="form-group">
                       <label for="">Sender: </label>
                       {{ $intake->sender }}
                   </div>

                   <div class="form-group">
                       <label for="">Sex: </label>
                       {{ $intake->sex()  }}
                   </div>

                   <div class="form-group">
                       <label for="">Group/ Organization/Institution: </label>
                       {{ $intake->sender_org }}
                   </div>

                   <div class="form-group">
                       <label for="">Designation: </label>
                       {{ $intake->sender_designation }}
                   </div>
                   <div class="form-group">
                       <label for="">IP: </label>
                       {{ $intake->is_sender_ip == 1 ? 'Yes' : 'No' }}
                   </div>

                   <div class="form-group">
                       <label for="">IP Groupname: </label>
                       {{ $intake->sender_ipgroup }}
                   </div>

                   <div class="form-group">
                       <label for="">Contact Information: </label>
                       {{ $intake->sender_contact }}
                   </div>
                </div>
                <div class="clearfix"></div>
          </div>
      </div>


          <div class="panel panel-default">
                  <div class="panel-heading">Resolution Details</div>
                  <div class="panel-body">

                    <div class="form-group">
                        <label for="type">Status of Resolution : </label>
                        {{ $intake->resolution_status() }}
                    </div>
                    @if($intake->resolution_status()=='Resolved')
                    <div class="form-group">
                        <label for="Category">Date Resolved : </label>
                        {{ toDate($intake->resolution_date) }}
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="">Complainant's Feedback on Resolution of Grievance : </label>
                        {{ $intake->resolution_level }}
                    </div>

                  </div>
          </div>


          <div class="panel panel-default">
                  <div class="panel-heading">Details of Issue/Concern</div>
                  <div class="panel-body">
                     <div class="form-group">
                        <label for="">Nature of Issue/Concern : </label>
                        {{ $intake->resolution() }}
                    </div>

                    <div class="form-group">
                        <label for="">Category of Concern : </label>
                        {{ $intake->concern_category }}
                    </div>

                    <div class="form-group">
                        <label for="">Subject of Complaint : </label>
                        {{ $intake->subject_complaint }}
                    </div>


                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Details/Narrative Summary : </label>
                        {{ $intake->narrative_summ }}
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="">Action Taken/Resolution : </label>
                        {{ $intake->actions }}
                    </div>

                </div>
                 <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="">Recommendations : </label>
                                        {{ $intake->recommendations }}
                                    </div>
                                </div>

                  </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">Action</div>
            <div class="panel-body">
            <div class="col-md-12 ">
                <table class="table table-bordered actions">
                    <thead>
                        <tr>
                            <td>Update</td>
                            <td>Date</td>
                            <td>Updated By</td>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($intake->updates as $action)

                           <tr>
                                <td>
                                    {{ $action->narrative }}
                                </td>
                                <td>
                                    {{ toDate($action->date)  }}
                                </td>
                                <td>
                                    {{ $action->update_by }}
                                </td>
                           </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>

            </div>
          </div>

    </div>
    <script>
        $(document).ready(function(){
        $('#print').on('click',function(e){
                                    e.preventDefault();
                                    window.print();
                                });
        				$("form").submit(function(){

        					var confirmation = confirm('are you sure to this action?');
        					if(confirmation){
        						var secondConfirmation = confirm('this action will reflect to the data of '+ $('small').text() )
        						if(secondConfirmation)
        							return true;
        						else
        							return false;
        					}else{
        						return false;
        					}


        				});
        			});
    </script>

@stop
