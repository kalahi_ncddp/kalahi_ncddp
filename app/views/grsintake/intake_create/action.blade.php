<div class="col-md-12 ">
    <table class="table table-bordered table-striped actions">
        <thead>
            <tr>
                <td>Update</td>
                <td>Date</td>
                <td>Updated By</td>
                <td>Option</td>
            </tr>
        </thead>
        <tbody >

           <tr>
                <td>
                    <div class="form-group">
                        {{ Form::textarea('narrative[]','',array('class'=>'form-control','placeholder'=>'Say something here....','')) }}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ Form::text('date[]','',array('class'=>'form-control date','')) }}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ Form::select('update_by[]',$officers,'',array('class'=>'form-control','')) }}
                    </div>

                </td>
                <td>
                    <span class="btn delete-field"><i class="fa fa-trash-o"></i></span>
                </td>
           </tr>
        </tbody>

    </table>
</div>

    <table class="templates hidden">

                 <tr>
                     <td>
                         <div class="form-group">
                            <b></b>
                             {{ Form::textarea('narrative[]','',array('class'=>'form-control','placeholder'=>'Say something here....')) }}
                         </div>
                     </td>
                     <td>
                         <div class="form-group">
                             {{ Form::text('date[]','',array('class'=>'form-control date')) }}
                         </div>
                     </td>
                     <td>
                         <div class="form-group">
                             {{ Form::select('update_by[]',$officers,'',array('class'=>'form-control')) }}
                         </div>

                     </td>
                     <td>
                         <span class="btn delete-field"><i class="fa fa-trash-o"></i></span>
                     </td>
                </tr>

    </table>


    <div class="col-md-12">
        <span class="btn bg-olive add">
            <i class="fa fa-plus"></i>
            Add New Record
        </span>
    </div>

<div class="clearfix"></div>

<script>
    $(document).ready(function(){

            var event = (function($){
                return {
                    addField : function(){
                        $template = $('.templates tbody').html();
                        $('.actions tbody').append( $template );
                        $('.date').datetimepicker({pick12HourFormat: false, pickTime:false});
                    },
                    deleteField : function(){
                        var confirms  = confirm('delete this field?');
                        if(confirms){
                            $(this).parent().parent().remove();
                        }
                    }
                }
            })(jQuery);

            $('.add').on('click',event.addField);
            $('body').on('click','.delete-field',event.deleteField);




    });

</script>