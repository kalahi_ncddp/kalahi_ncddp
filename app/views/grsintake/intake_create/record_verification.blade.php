                <div class="col-md-6">
                    <h4>Registration Details</h4>
                    <hr/>
                    <div class="form-group">
                        <label for="">Grievance Form</label>
                        {{ Form::select('grievance_form',$grievance_form,'',array('class'=>'form-control','required')) }}
                    </div>
                    <div class="form-group">
                        <label for="">Intake Level</label>
                        {{ Form::select('intake_level',$intake_level,'',array('class'=>'form-control','required')) }}
                    </div>
                    <div class="form-group">
                        <label for="">Intake Date</label>
                        {{ Form::text('intake_date','',array('class'=>'form-control date','required'))  }}
                    </div>

                    <div class="form-group">
                        <label for="">Intake Officer</label>
                        {{ Form::text('intake_officer','',array('class'=>'form-control ','required'))  }}
                    </div>

                    <div class="form-group">
                        <label for="">Designation</label>
                        {{ Form::text('off_desigination','',array('class'=>'form-control','required'))  }}
                    </div>

                    <div class="form-group">
                        <label for="">Mode of Filing</label>
                        {{ Form::select('filing_mode',$mode_of_filling,'',array('class'=>'form-control','required')) }}
                    </div>



                    <div class="clearfix"></div>
               </div>

                           <div class="col-md-6">
                               <h4>Complainant/Sender's Profile</h4>
                               <hr/>
                               <div class="form-group">
                                   <label for="">Sender</label>
                                   {{ Form::text('sender','',array('class'=>'form-control'))  }}
                               </div>

                               <div class="form-group">
                                   <label for="">Sex</label>
                                   {{ Form::select('sender_sex',$genders,'',array('class'=>'form-control'))  }}
                               </div>

                               <div class="form-group">
                                   <label for="">Group/ Organization/Institution</label>
                                   {{ Form::textarea('sender_org','',array('class'=>'form-control')) }}
                               </div>

                               <div class="form-group">
                                   <label for="">Designation</label>
                                   {{ Form::select('sender_designation',$designations,'',array('class'=>'form-control')) }}
                               </div>
                               <div class="form-group">
                                   <label for="">IP</label>
                                   {{ Form::checkbox('is_sender_ip','',array('class'=>'form-control')) }}
                               </div>

                               <div class="form-group">
                                   <label for="">IP Groupname</label>
                                   {{ Form::select('sender_ipgroup',$ip_groupname,'',array('class'=>'form-control')) }}
                               </div>

                               <div class="form-group">
                                   <label for="">Contact Information</label>
                                   {{ Form::text('sender_contact','',array('class'=>'form-control'))  }}
                               </div>

                           </div>
                             <div class="clearfix"></div>