 <div class="col-md-12">
                    <h4>Resolution Details</h4>
                    <div class="form-group">
                        <label for="type">Status of Resolution</label>
                        {{ Form::select('resolution_status',$status_resolution,'',array('class'=>'form-control resolution','required')) }}
                    </div>

                    <div class="form-group date-resolved">
                        <label for="Category">Date Resolved</label>
                        {{ Form::text('resolution_date','',array('class'=>'form-control date'))  }}
                    </div>

                    <div class="form-group">
                        <label for="">Complainant's Feedback on Resolution of Grievance</label>
                        {{ Form::text('resolution_level','',array('class'=>'form-control','required')) }}
                    </div>
                </div>
                <div class="col-md-6">
                 <h4>Details of Issue/Concern</h4>
                 <hr/>
                    <div class="form-group">
                        <label for="">Nature of Issue/Concern</label>
                        {{ Form::text('concern_nature','',array('class'=>'form-control','required'))  }}
                    </div>

                    <div class="form-group">
                        <label for="">Category of Concern</label>
                        {{ Form::text('concern_category','',array('class'=>'form-control','required')) }}
                    </div>

                    <div class="form-group">
                        <label for="">Subject of Complaint</label>
                        {{ Form::text('subject_complaint','',array('class'=>'form-control','required')) }}
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Details/Narrative Summary</label>
                        {{ Form::textarea('narrative_summ','',array('class'=>'form-control','required')) }}
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="">Action Taken/Resolution</label>
                        {{ Form::textarea('actions','',array('class'=>'form-control','required')) }}
                    </div>

                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="">Recommendations</label>
                        {{ Form::textarea('recommendations','',array('class'=>'form-control','required')) }}
                    </div>
                </div>

                <div class="clearfix"></div>