@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')

	<div class="box box-primary">
		<div class="box-header">
			{{ $title }}
			<div class="box-tools pull-right">
			    <a href="{{ URL::to('grs-intake/create')  }}">
				    <button class="btn btn-primary">
				        <i class="fa fa-plus"></i>
				        Add Record
				    </button>

			    </a>
			    @if($position->position=='ACT')
                            <span class="btn btn-default pull-right" id="approved">Review</span>
                            <span class="hidden module">grs-intake</span>
                        @endif
			</div>

		</div>
		<div class="box-body">
			<table class="table table-bordered" id="intakes">
				<thead>
					<tr>
						 @if($position->position=='ACT')
                        <th>Select all
                            <input type="checkbox" id="selecctall"/>
                        </th>
                        @endif
					    <td>Region</td>
					    <td>Province</td>
						<td>Municipality</td>
						<td>Barangay</td>
						<td>Cycle</td>
						<td>Program</td>
						<td>Intake Date</td>
						<td>Intake Level</td>
						<td>Status of Resolution</td>
						<td>Option</td>
					</tr>
				</thead>

				<tbody>
                    @foreach($intakes as $intake)
					<tr>
 						@if($position->position=='ACT')
					    <td>
                           @if($intake->is_draft==0)
                             {{ $reference->approval_status($intake->reference_no , 'ACT') }}
                           @else
                               <span class="label label-warning">draft</span>
                           @endif
					    </td>
                         @endif
						<td>{{ getRegion($intake->psgc_id) }}</td>

                        <td>{{ getProvince($intake->psgc_id) }}</td>
                        <td>{{ getMunicipality($intake->psgc_id) }}</td>
                        <td>{{ getBarangay($intake->psgc_id) }}</td>

                        <td>{{ $intake->cycle_id }}</td>
						<td>{{ $intake->program_id }}</td>
						<td>{{ toDate($intake->intake_date) }}</td>
						<td>{{ $intake->intake_level }}</td>
						<td>{{ $intake->resolution_status }}</td>
						<td>
						    <a class="btn btn-primary" href="{{ URL::to( 'grs-intake/'.$intake->reference_no ) }}">View</a>
						</td>
					</tr>
					@endforeach
				</tbody>

			</table>
		</div>
	</div>
	
	<script>
      $(document).ready(function () {
        $('#intakes').dataTable(

        );
      });
    </script>
@stop