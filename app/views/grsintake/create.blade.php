@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
 {{ Form::open(array('url' => 'grs-intake','novalidate')) }}
@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
    <div class="col-md-12">

      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">

            <li class="active"><a href="#record_verification" data-toggle="tab">Record Verification</a></li>

        </ul>
    {{-- Record Verification--}}
        <div class="tab-content">
            <div class="tab-pane active" id="record_verification">

                <div class="col-md-6">

                    <div class="form-group ">
                        <br/>
                        <label for="">Region</label>
                        {{ Form::select('region_id',$regions,$region,array('class'=>'form-control region_list')) }}
                    </div>

                    <div class="form-group">
                        <label for="">Province</label>
                        {{ Form::select('province_id',$provinces,'',array('class'=>'form-control province_list')) }}

                    </div>
                    <div class="form-group">
                        <label for="">Municipality</label>
                        {{ Form::select('municipality_id',$municipalities,'',array('class'=>'form-control municipality_list')) }}
                    </div>

                    <div class="form-group">
                        <label for="">Barangay</label>
                        {{ Form::select('barangay_id',$barangay, '',array('class'=>'form-control barangay_list') ) }}
                    </div>
                    <div class="form-group">
                        <label for="">Program</label>
                        {{ Form::select('program_id',$programs,'',array('class'=>'form-control'))  }}
                    </div>
                </div>

                <div class="col-md-6">


                    <div class="form-group">
                        <label for="">Cycle</label>
                        {{ Form::select('cycle_id',$cycles,'',array('class'=>'form-control')) }}
                    </div>
                    <div class="form-group">
                        <label for="">Intake Date</label>
                        {{ Form::text('intake_date','',array('class'=>'form-control date','required'))  }}
                    </div>

                    <div class="form-group">
                        <label for="">Sender</label>
                        {{ Form::text('sender','',array('class'=>'form-control','maxlength'=>'40'))  }}
                    </div>
                    <div class="form-group">
                        <label for="">Intake Level</label>
                        <?php
                        $current_login_intake_level ="";
                        switch($user->office_level){
                            case 'ACT':
                                $current_login_intake_level = 'municipal';
                                break;
                            case 'SRPMO':
                                $current_login_intake_level = 'sub region';
                                break;
                            case 'RPMO':
                                $current_login_intake_level = 'regional';
                                break;
                            case 'NPMO':
                                $current_login_intake_level = 'national';
                                break;
                        }
                        ?>
                        {{ Form::select('intake_level',$intake_level,$current_login_intake_level,array('class'=>'form-control','required')) }}
                    </div>
                </div>





                <div class="col-md-12">
                    <div class="form-group pull-right">

                        <button class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Save
                        </button>
                        <a class="btn btn-default" href="{{ URL::to('grs-intake') }}">Close</a>

                        <br/>
                        <br/>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>

        </div>


      </div>
    </div>

    @include('modals/search_pincos')
    <script>
        $(document).ready(function(){
            $('.date').datetimepicker({pick12HourFormat: false, pickTime:false});

            $('.date-resolved').fadeOut();
            $('.resolution').change(function(){
              if($(this).val()=="r")
                $('.date-resolved').fadeIn();
              else
                $('.date-resolved').fadeOut();
            });
            $('.grievance_form').change(function(){
               if($(this).val()=="intake"){
                    $('.recommendations').attr('disabled','true')
               }else{
                    $('.recommendations').removeAttr('disabled','true')
               }
            });                      
            $('form').submit(function(e){
                var recordVerifacation = $('#record_verification'),
                    registration_details = $('#registration_details'),
                    issue_resolution    = $('#issue_resolution'),
                    action = $('#action');

                var rv = "",rd = "" , ir = "", a = "";
                $(this).find('.form-control').each(function(i,elem){
                        if(elem.required) {
            					      if($(this).val() == ""){
                                          $(this)[0].setCustomValidity($(this).siblings('label').text()+"is required");
//            					         rv += $(this).siblings('label').text() + " is required \n";
            					      }
            					  }
                });

                if(rv==""){
                  var confirmation = confirm('Are you sure that do you want to submit this?');
                  if(confirmation){
                    return true;

                  }else{
                    return false;
                  }
                }else{
                  alert(rv);
                }

                e.preventDefault();
            });

            $('.select-pincos').on('click',function() {
              var activity_id = $(this).data('id');
              var issue       = $(this).data('issue');

              $.get('{{ URL::to('') }}/pincos-grievances',{activity_id:activity_id,issue_no:issue},function(res){
                     $('select[name="grievance_form"]').val('pincos');
                     $('textarea[name="narrative_summ"]').val(res.issue);
                     $('textarea[name="actions"]').eq(0).val(res.action);
                     $('input[name="sender"]').val(res.raise_by);
                     $('#intake').modal('hide');
              });
            })

        });


       $(function(){
              var hash = window.location.hash;
              hash && $('ul.nav a[href="' + hash + '"]').tab('show');

              $('.nav-tabs a').click(function (e) {

                    $(this).tab('show');
                    var scrollmem = $('body').scrollTop();
                    window.location.hash = this.hash;
                    $('html,body').scrollTop(scrollmem);
              });
       });
    </script>
@stop
