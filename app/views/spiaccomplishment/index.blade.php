@extends('layouts.default')

@section('content')
    <div class="row">
	    <div class="col-md-12">
			<!-- Advanced Tables -->
			
	    	<div class="panel panel-default">
	    		<div class="panel-heading">
	    			<h4>  {{ $title }}</h4>
	    			@if($position->position=='ACT')
			               <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
			               <span class="hidden module">spi-subworkers</span>
			             @endif
			        <!--
					<a href="http://localhost/kalahi-spi/mlprap/create" class="btn btn-success pull-right">Add New</a>
					//-->
			        <div class="clearfix"></div>
	    		</div>

				<div class="panel-body">
					<div class="table-responsive">
						<div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
							<table class="table table-striped table-bordered table-hover dataTable" 
									id="dataTables-example" aria-describedby="dataTables-example_info">
							<thead>
								<tr>
									@if($position->position=='ACT')
								    <th>Select all
								        <input type="checkbox" id="selecctall"/>
								    </th>
								    @endif
									<th style="width: 25%;">Title</th>
									<th style="width: 20%;">Barangay</th>
									<th style="width: 20%;">Program</th>
									<th style="width: 5%;">Cycle</th>
									<th style="width: 20%;">% Completed</th>
									<th style="width: 10%; text-align: center;">Action</th>
							</thead>
							
							<tbody>
								@foreach ($projects as $project ) 
									<tr>
		                               @if($position->position=='ACT')
										<td>
		                                  @if($project->is_draft==0)
		                                    {{ $reference->approval_status($project->project_id, 'ACT') }}
		                                  @else
		                                      <span class="label label-warning">draft</span>
		                                  @endif
		                            </td>
		                                @endif
										<td>
											{{ $project->project_name }}
										</td>
										<td>
											{{ getBarangay($project->barangay_id()) }}
										</td>
										<td>
											{{ $project->program_id }}
										</td>
										<td>
											{{ $project->cycle_id }}
											

										</td>
										<td style="text-align: center;">
											<?php $accomplished = ($project->report == '') ? 0:$project->report->accomplished; ?>
											<div class="progress">
											  <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" 
											  	aria-valuenow="{{ $project->accomplished() }}" aria-valuemin="0" aria-valuemax="100"  style="width: {{ $project->accomplished() }}%">
											    {{ round($project->accomplished(),2) }}%
											  </div>
											</div>
										</td>
										<td  style="text-align: center;">
											<a href="spi-accomplishment/{{ $project->project_id }}" class="btn btn-xs btn-info"
												style="width: 100%;">
												<i class="glyphicon glyphicon-info-sign"></i> Info
											</a>
										</td>
									</tr>
								@endforeach

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="custom_confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
	            </div>
	            <div class="modal-body">
	           	  
				     </div>
	           <div class="modal-footer">
	             <button class="btn btn-warning confirm">Confirm</button>
	             <button class="btn  closes">Close</button>
	           </div>
	        </div>
	    </div>
	</div>
	<script>
      	$(document).ready(function () {
        	$('#dataTables-example').dataTable();
      	});
    </script>
@stop