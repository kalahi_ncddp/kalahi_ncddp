@extends('layouts.default')

@section('content')
<div class="row">
    <div class="col-md-12">
		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h4>Project: <small>{{$project->project_name}}</small></h4>
    		</div>
			<div class="panel-body">
			    <div class="table-responsive col-xs-6 col-md-6">
			        <table class="table table-bordered table-striped">
				        <tbody>
					        <tr>
					          <th width="40%">Project ID</th>
					          <td id="in-projectid" data-value="{{$project->project_id}}">{{$project->project_id}}</td>
					        </tr>
					        <tr>
					          <th>Physical Target</th>
					          <td>{{$project->phys_target}} {{$project->phys_target_unit}}</td>
					        </tr>
					    	<tr>
					          <th>Region</th>
					          <td>{{$project->municipality->province->region->region_name}}</td>
					        </tr>
					        <tr>
					          <th>Province</th>
					          <td>{{$project->municipality->province->province}}</td>
					        </tr>
					        <tr>
					          <th>Municipality</th>
					          <td>{{$project->municipality->municipality}}</td>
					        </tr>
					        <tr>
					          <th>Barangay</th>
							
					          
					          <td>{{ getBarangay($project->barangay_id()) }}</td>
					        </tr>
				      	</tbody>
			        </table>
			    </div>

			    <div class="table-responsive col-xs-6 col-md-6">
			        <table class="table table-bordered table-striped">
				        <tbody>
					        <tr>
					          <th width="40%">Total Sub-Project Cost</th>
					          <td>
					          	<?php
					          		$total = doubleval($project->cost_infra) + doubleval($project->cost_others) + 
					          				 doubleval($project->cost_mgmnt) + doubleval($project->cost_training);
					          		echo number_format($total, 2);
					          	?>
					          </td>
					        </tr>
					        <tr>
					          <th>Direct Cost</th>
					          <td>{{number_format($project->cost_infra,2)}}</td>
					        </tr>
					    	<tr>
					          <th>Indirect Cost</th>
					          <td>{{number_format($project->cost_others, 2)}}</td>
					        </tr>
					        <tr>
					          <th>Date Started</th>
					          <td>{{date('m/d/Y', strtotime($project->date_started))}}</td>
					        </tr>
					        <tr>
					          <th>Target Completion Date</th>
					          <td>
					          <?php
					          	if ($project->date_completed != NULL) echo date('m/d/Y', strtotime($project->date_completed));
					          ?>
					          </td>
					        </tr>
					        <tr>
					          <th>Name of Implementation</th>
					          <td>{{$project->barangay->barangay}}</td>
					        </tr>
				      	</tbody>
			        </table>
			    </div>
				<?php
					$accomplished = 0;
					foreach($summary as $months_summary)
					{
						$accomplished += $months_summary->noncumm_actl_physical;
					}
				?>
			    <div class="table-responsive col-xs-12 col-md-12">
			        <table class="table table-bordered table-striped">
				        <tbody>
					        <tr>
					          <th width="20%">% Completed</th>
					          <td>
									<?php $proj_accomplished = ($project->report == '') ? 0 : $project->report->accomplished; ?>
									<div class="progress">
									  <div style="width: {{$accomplished}}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="{{$accomplished}}" role="progressbar" class="progress-bar progress-bar-success progress-bar-striped">
									    {{ round($accomplished ,2)}}%
									  </div>
									</div>
					          </td>
					        </tr>
				      	</tbody>
			        </table>
			    </div>
			  </div>
		</div>

		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h4 class="col-xs-6 col-md-6" style="padding-left: 0;">Accomplishment Summary</h4>
				<div class="clearfix"></div>
    		</div>
			<div class="panel-body main-body">
			<?php
				$months = array('', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
				$sum_months = array();
				$phys_prog_periodic = array();
				$phys_actl_periodic = array();
				$phys_prog_cummlatv = array();
				$phys_actl_cummlatv = array();
				$slippage           = array();
				$fncl_prog_periodic = array();
				$fncl_actl_periodic = array();
				$fncl_prog_cummlatv = array();
				$fncl_actl_cummlatv = array();
				foreach($summary as $months_summary)
				{
					$sum_months[]    = $months_summary->month_num;
					$phys_prog_periodic[] = $months_summary->noncumm_plnd_physical;
					$phys_actl_periodic[] = $months_summary->noncumm_actl_physical;
					$phys_prog_cummlatv[] = $months_summary->cumm_plnd_physical;
					$phys_actl_cummlatv[] = $months_summary->cumm_actl_physical;
					$slippage[]      = $months_summary->slippage;
					$fncl_prog_periodic[] = $months_summary->noncumm_plnd_financial;
					$fncl_actl_periodic[] = $months_summary->noncumm_actl_financial;
					$fncl_prog_cummlatv[] = $months_summary->cumm_plnd_financial;
					$fncl_actl_cummlatv[] = $months_summary->cumm_actl_financial;
				}
				$with_data = (count($sum_months) > 0) ? true : false; 
			?>
				<table class="table table-striped table-bordered">
					<tr>
						<th rowspan="5" style="width: 5%; vertical-align: middle;">Physical Accomplishment</th>
						<th style="width: 15%;"></th>
						@if ($with_data)
						@foreach($sum_months as $value)
						<th style="text-align: center;">Month {{$value}}</th>
						@endforeach
						@else
						<td rowspan="10" style="text-align: center; vertical-align: middle;"> No Plans and Accomplishments set.</td>
						@endif
					</tr>
					<tr>
						<th>Program Periodic</th>
						@foreach($phys_prog_periodic as $value)
						<td style="text-align: center;">{{{ isset($value) ? (number_format($value,2) . '%') : '' }}}</td>
						@endforeach
					</tr>
					<tr>
						<th>Actual Periodic</th>
						@foreach($phys_actl_periodic as $value)
						<td style="text-align: center;">{{{ isset($value) ? (number_format($value,2) . '%') : '' }}}</td>
						@endforeach
					</tr>
					<tr>
						<th>Program Cumulative</th>
						@foreach($phys_prog_cummlatv as $value)
						<td style="text-align: center;">{{{ isset($value) ? (number_format($value,2) . '%') : '' }}} </td>
						@endforeach
					</tr>
					<tr>
						<th>Actual Cumulative</th>
						@foreach($phys_actl_cummlatv as $value)
						<td style="text-align: center;">{{{ isset($value) ? (number_format($value,2) . '%') : '' }}}</td>
						@endforeach
					</tr>
					<tr>
						<th colspan="2" style="text-align: right;">SLIPPAGE</th>
						@foreach($slippage as $value)
						<td style="text-align: center;">{{{ isset($value) ? (number_format($value,2) . '%') : '' }}}</td>
						@endforeach
					</tr>
					<tr>
						<th rowspan="4" style="width: 5%; vertical-align: middle;">Financial Accomplishment</th>
						<th>Program Periodic</th>
						@foreach($fncl_prog_periodic as $value)
						<td style="text-align: center;">{{number_format($value,2)}}</td>
						@endforeach
					</tr>
					<tr>
						<th>Actual Periodic</th>
						@foreach($fncl_actl_periodic as $value)
						<td style="text-align: center;">{{number_format($value,2)}}</td>
						@endforeach
					</tr>
					<tr>
						<th>Program Cumulative</th>
						@foreach($fncl_prog_cummlatv as $value)
						<td style="text-align: center;">{{number_format($value,2)}}</td>
						@endforeach
					</tr>
					<tr>
						<th>Actual Cumulative</th>
						@foreach($fncl_actl_cummlatv as $value)
						<td style="text-align: center;">{{number_format($value,2)}}</td>
						@endforeach
					</tr>
				</table>
			</div>
		</div>
		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h4 class="col-xs-6 col-md-6" style="padding-left: 0;">Program of Works</h4>
				<a id="spi-accomplish_modal-add_show" href="" class="btn btn-success pull-right">
					<i class="fa fa-plus"></i> Add Program of Works
				</a>
				<div class="clearfix"></div>
    		</div>
			<div class="panel-body main-body">
				@if ($create_status !== null)
					@if ($create_status === TRUE)
						<div class="alert alert-success" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-ok"></i> New Program of Work successfully added!</strong>
						</div>
					@elseif ($create_status === FALSE)
						<div class="alert alert-danger" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-remove"></i> Program of Work can not be added</strong> 
							Please contact DSWD about this incident.
						</div>
					@endif
				@endif
				@if ($update_status !== null)
					@if ($update_status === TRUE)
						<div class="alert alert-success" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-ok"></i> Program of Work successfully updated!</strong>
						</div>
					@elseif ($update_status === FALSE)
						<div class="alert alert-danger" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-remove"></i> Program of Work can not be updated</strong> 
							Please contact DSWD about this incident.
						</div>
					@endif
				@endif
				@if ($destroy_status !== null)
					@if ($destroy_status === TRUE)
						<div class="alert alert-success" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-ok"></i> Program of Work successfully removed!</strong>
						</div>
					@elseif ($destroy_status === FALSE)
						<div class="alert alert-danger" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-remove"></i> Program of Work can not be removed</strong> 
							Please contact DSWD about this incident.
						</div>
					@endif
				@endif
				<table class="table table-striped table-bordered dataTable" id="dataTables-example" aria-describedby="dataTables-example_info">
					<thead>
						<tr>
							<th style="width: 10%;">Item #</th>
							<th style="width: 25%;">Scope of Work</th>
							<th style="width: 7%;">Quantity</th>
							<th style="width: 7%;">Unit</th>
							<th style="width: 8%;">Unit Cost</th>
							<th style="width: 12%;">Amount</th>
							<th style="width: 24%;">Weight</th>
							<th style="width: 12%; text-align: center;">Action</th>
						</tr>
					</thead>
					<tbody>
					<?php
						$items_already_added = array();
						$item_ids = array();
					?>
						@foreach($acc_report_items as $item)
							<?php 
							$items_already_added[] = $item->item_no; 
							$item_ids[$item->item_no] = $item->item_id;
							?>
							<tr data-rowid="{{$item->id}}">
								<td class="row-item_no"    data-value="{{$item->item_no}}" data-id="{{$item->item_id}}">{{$item->item_id}}</td>
								<td class="row-item_scope" data-value="{{$item->item->scope_of_work}}">{{$item->item->scope_of_work}}</td>
								<td class="row-item_qty"   data-value="{{$item->quantity}}">{{number_format($item->quantity, 2)}}</td>
								<td class="row-item_unit"  data-value="{{$item->item->unit}}">{{$item->item->unit}}</td>
								<td class="row-item_cost"  data-value="{{$item->unit_cost}}">{{number_format($item->unit_cost, 2)}}</td>
								<td class="row-item_amount">{{number_format($item->amount,2)}}</td>
								<td class="row-item_pweight" data-value="{{$item->p_weight}}">
									<?php $p_weight = ($item->p_weight == '') ? 0 : $item->p_weight; ?>
									<div class="progress">
									  <div style="width: {{$p_weight}}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="{{$p_weight}}" role="progressbar" class="progress-bar progress-bar-info progress-bar-striped">
									    {{number_format($p_weight, 2)}}%
									  </div>
									</div>
								</td>
								<td style="text-align: center;">
									<a href="spi-accomplishment/{{$id}}" class="btn btn-xs btn-warning spi-accomplishment_modal-update_show" style="margin-right: 0.5rem;"><i class="glyphicon glyphicon-pencil"></i></0>
									<a href="spi-accomplishment/{{$id}}" class="btn btn-xs btn-danger spi-accomplishment_modal-del_show"><i class="glyphicon glyphicon-trash"></i></a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h4 class="col-xs-3 col-md-3" style="padding-left: 0;">Plans per Month</h4>
    			<div class="pull-right alert alert-info" style="padding-top: 5px; padding-bottom: 5px; margin-bottom: 0px;">
    				Add a month by clicking <strong><i class="fa fa-plus"></i>Add Month</strong> and enter plans per work item by clicking
    				<strong><i class="fa fa-plus"></i>Add Program of Works</strong> </div>
				<div class="clearfix"></div>
    		</div>
			<div class="panel-body">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<?php 
					$monthly_plans = array();
					foreach($plans as $plan)
					{
						$id = $plan->month_num;
						$monthly_plans[$id][] = $plan;
					}
					$monolog = Log::getMonolog();
					$monolog->pushHandler(new \Monolog\Handler\FirePHPHandler());
					$monolog->addInfo('PLANS', array($monthly_plans));	

					foreach ($monthly_plans as $key => $value) {
						$monolog->addInfo('KEY', array($key));
						$monolog->addInfo('ITEMS', array($value));
					}
				?>
				@foreach($monthly_plans as $id => $items)
				<?php
					$value=$id;
					$v_id='planned-months_' + $id;
					$month_startend  = $items[0]->startend;
				?>
					<div class="planned-months panel panel-default" data-value="{{$value}}">
						<div class="panel-heading" role="tab" id="heading{{$id}}">
							<h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#{{$v_id}}" aria-expanded="true" aria-controls="{{$v_id}}"
						        	style="display: block;">
						          <span >Month {{$id}}</span>
						          <span class="pull-right">{{$month_startend}}</span>
						        </a>
						    </h4>
					    </div>
						<div id="{{$v_id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$id}}">
  					        <div class="panel-body">
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th style="width: 15%;">Item #</th>
											<th style="width: 30%;">Scope of Work</th>
											<th style="width: 25%;">Physical (%)</th>
											<th style="width: 25%;">Financial (Php)

											</th>
											<th style="width: 5%; text-align: center;">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if(count($items) == 0) {?> <tr><td colspan="5">No Items</td></tr> <?php } 
										$startend = '';
										?>
										@foreach($items as $item)
										<?php
											$startend = $item->startend;
										?>
										<tr>
											<td class="row-planitem_no" data-value="{{$item->item_no}}">{{$item_ids[$item->item_no]}}</td>
											<td>{{$item->item->scope_of_work}}</td>
											<td>{{number_format($item->physical, 2)}}</td>
											<td>{{number_format($item->financial, 2)}}</td>
											<td style="text-align: center;" class=" ">
												<a class="btn btn-xs btn-danger spi-planneditems_modal-del_show" data-value="{{$item->id}}"><i class="glyphicon glyphicon-trash"></i></a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								<button type="button" class="btn btn-primary spi-planneditems_modal-add_show" data-monthnum="{{$item->month_num}}" data-startend="{{$startend}}"><i class="fa fa-plus"></i> Add Program of Works </button>
					        </div>
						</div>
					</div>
				@endforeach
				</div>
				<button id="spi-planneditem_modal-add_show" type="button" class="btn btn-default"><i class="fa fa-plus"></i> Add Month</button>
			</div>
		</div>

		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h4 class="col-xs-3 col-md-3" style="padding-left: 0;">Accomplishment per Month</h4>
				<div class="pull-right alert alert-info" style="padding-top: 5px; padding-bottom: 5px; margin-bottom: 0px;">
    				Enter accomplishment per scope of work by clicking <strong><i class="glyphicon glyphicon-pencil"></i></strong> </div>
				<div class="clearfix"></div>
    		</div>
			<div class="panel-body">
				<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
				@foreach($monthly_plans as $id => $items)
				<?php
					$month_startend = $items[0]->startend;
				?>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="heading2-{{$id}}">
							<h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion2" 
						        href="#{{$id . 2}}" aria-expanded="true" style="display: block;margin-top: 0;  margin-bottom: 0;  font-size: 26px;  color: inherit;  text-align: center;  font-weight: bold;" aria-controls="{{$id . 2}}"
						        	style="display: block;">
						          <span>Month {{$id}}</span>
						          <span class="pull-right">{{$month_startend}}</span>
						        </a>
						    </h4>
					    </div>
						<div id="{{$id . 2}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2-{{$id}}">
  					        <div class="panel-body">
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th style="width: 15%;">Item #</th>
											<th style="width: 20%;">Scope of Work</th>
											<th style="width: 15%;">Planned Physical (%)</th>
											<th style="width: 15%;">Planned Financial (Php)</th>
											<th style="width: 15%;">Actual Physical (%)</th>
											<th style="width: 15%;">Actual Financial (Php)</th>
											<th style="width: 5%; text-align: center;">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if(count($items) == 0) {?> <tr><td colspan="5">No Items</td></tr> <?php } ?>
										@foreach($items as $item)
										<tr>
											<td>{{$item_ids[$item->item_no]}}</td>
											<td>{{$item->item->scope_of_work}}</td>
											<td>{{number_format($item->physical, 2)}}</td>
											<td>{{number_format($item->financial, 2)}}</td>
											<td>{{{ isset($item->actual->physical) ? number_format($item->actual->physical, 2) : '' }}}</td>
											<td>{{{ isset($item->actual->financial) ? number_format($item->actual->financial, 2) : '' }}}</td>
											<td style="text-align: center;  min-width: 100px;" class=" ">
												<a style="margin-right: 0.5rem;" 
													class="btn btn-xs btn-warning spi-accmplisheditems_modal-add_show"
													 data-value="{{$item->id}}" data-pow="{{$item->item->scope_of_work}}"
													 data-weight="{{$item->p_weight}}" data-physical="{{$item->physical}}"
													 data-financial="{{$item->financial}}" data-item="{{$item->item_no}}"
													 data-aphysical="{{{ isset($item->actual->physical) ? $item->actual->physical : '' }}}"
													 data-afinancial="{{{ isset($item->actual->financial) ? $item->actual->financial : '' }}}">
													<i class="glyphicon glyphicon-pencil"></i>
												</a>
												
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
					        </div>
						</div>
					</div>
				@endforeach
				</div>
			</div>
		</div>
	</div>
</div>		

<div class="modal fade" id="spi-accomplishment_modal-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
				<form id="spi-disaster_modal_edit-delform">
					<input type="hidden" id="del-id" name="del-id" value="">
					<input type="hidden" id="method" name="method" value="delete">
					<p class="col-xs-2" style="text-align: right;">
						<i class="glyphicon glyphicon-question-sign" style="color: #f4543c; font-size: 30px;"></i>
					</p>
					<p class="col-xs-10" style="font-size: 15px; padding-top: 5px; padding-bottom: 5px;">Are you sure you want to remove 
						 this Accomplishment Item?</p>
					<div class="clearfix"></div>
				</form>
			</div>
			<div class="modal-footer">
				<a id="spi-accomplishment_modal_edit-delform_submit" href="" class="btn btn-danger">Yes</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">No</a>
			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="spi-accomplish_modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">New Program of Work</h4>
            </div>
            <div class="modal-body">
				{{ Form::open(array('id' => 'spi-accomplish_modal_form')) }}
					<div class="form-group col-xs-12 col-md-12">
						<label for="in-itemnum"> Scope of Work </label>
						<select name="in-itemnum" id="in-itemnum" class="form-control">
							<option value="">-- Select Scope of Work --</option>
							<option value="*"> ** New Item ** </option>
							@foreach($work_items as $item)
							<?php 
								if(!in_array($item->item_no, $items_already_added))
								{
							?>
									<option id="op{{$item->item_no}}" value="{{$item->item_no}}" 
										data-scope="{{$item->scope_of_work}}" data-unit="{{$item->unit}}">
										 {{$item->scope_of_work}}  
									</option>
							<?php
								}
							?>
							@endforeach
						</select>
					</div>

					<div class="form-group  col-xs-12 col-sm-12 in-itemid">
						<label for="in-itemid">Item Number</label>
						<input type="text" class="form-control" id="in-itemid" style="margin-top: 2%;">
					</div>  

					<div class="form-group  col-xs-12 col-sm-12 in-itemscope" style="display: none;">
						<label for="in-itemscope">Scope of Work</label>
						<input type="text" class="form-control" id="in-itemscope"  disabled="disabled" style="margin-top: 2%;">
					</div>  


					<div class="form-group  col-xs-6 col-sm-6">
						<label for="in-itemunit">Unit</label>
						<input type="text" class="form-control" id="in-itemunit" disabled="disabled">
					</div>  

					<div class="form-group  col-xs-6 col-sm-6">
						<label for="in-itemunitcost">Unit Cost</label>
						<input type="text" class="form-control" id="in-itemunitcost">
					</div>  

					<div class="form-group  col-xs-12 col-sm-12">
						<label for="in-itemamount">Quantity</label>
						<input type="text" class="form-control" id="in-itemamount">
					</div>  
				    <div class="clearfix"></div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<input type="hidden" id="in-accreportid" value="">
				<a id="spi-accomplish_modal_form_submit" href="{{ URL::to('spi-accomplishment')}}" class="btn btn-success">
					<i class="glyphicon glyphicon-floppy-disk"></i> Submit
				</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="spi-accomplish_modal-update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Accomplishment Item</h4>
            </div>
            <div class="modal-body">
				{{ Form::open(array('id' => 'spi-accomplish_modal_editform')) }}
					<div class="form-group col-xs-12 col-md-12" style="display: none;">
						<label for="edit-itemnum"> Item No. </label>
						<input type="text" class="form-control" id="edit-itemnum"  disabled="disabled">
					</div>

					<div class="form-group col-xs-12 col-md-12">
						<label for="edit-itemid"> Item No. </label>
						<input type="text" class="form-control" id="edit-itemid"  disabled="disabled">
					</div>

					<div class="form-group  col-xs-12 col-sm-12">
						<label for="edit-itemscope">Scope of Work</label>
						<input type="text" class="form-control" id="edit-itemscope"  disabled="disabled">
					</div>  

					<div class="form-group  col-xs-6 col-sm-6">
						<label for="edit-itemunit">Unit</label>
						<input type="text" class="form-control" id="edit-itemunit" disabled="disabled">
					</div>  

					<div class="form-group  col-xs-6 col-sm-6">
						<label for="edit-itemunitcost">Unit Cost</label>
						<input type="text" class="form-control" id="edit-itemunitcost">
					</div>  

					<div class="form-group  col-xs-12 col-sm-12">
						<label for="edit-itemamount">Quantity</label>
						<input type="text" class="form-control" id="edit-itemamount">
					</div>  
				    <div class="clearfix"></div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<input type="hidden" id="edit-accitemid" value="">
				<input type="hidden" id="edit-accreportid" value="">
				<a id="spi-accomplish_modal_updateform_submit" href="{{ URL::to('spi-accomplishment')}}" class="btn btn-success">
					<i class="glyphicon glyphicon-floppy-disk"></i> Submit
				</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
			</div>
        </div>
    </div>
</div>


<div class="modal fade" id="spi-planneditem_modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">New Monthly Plan</h4>
            </div>
            <div class="modal-body">
				{{ Form::open(array('id' => 'spi-planneditem_modal_form')) }}
					<div class="form-group  col-xs-12 col-sm-12">
						<label>Month Number</label>
						<input type="text" class="form-control" id="in-monthnum">
					</div>  
					<div class="form-group col-xs-12 col-sm-12">
					    <label for="reservationtime">Start Date - End Date</label>
					    <div class="input-group">
					      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					      <input type="text" name="in-startend" id="in-startend" class="form-control" />
					    </div>
					  </div>
				    <div class="clearfix"></div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<div class="alert alert-danger pull-left" style="margin-bottom: 0px; margin-left: 0px; padding: 5px 10px; display: none;">Please complete all Fields </div>
				<a id="spi-planneditem_modal_form_submit" href="{{ URL::to('spi-accomplishment')}}" class="btn btn-success">
					<i class="glyphicon glyphicon-floppy-disk"></i> Submit
				</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
			</div>
        </div>
    </div>
</div>


<div class="modal fade" id="spi-planneditems_modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-body">
				{{ Form::open(array('id' => 'spi-planneditems_modal_form')) }}
					<div class="form-group col-xs-12 col-md-12">
						<label for="in-item"> Scope of Work </label>
						<select name="in-item" id="in-item" class="form-control">
							<option value="" data-weight=""> -- Select Scope of Work -- </option>
							@foreach($acc_report_items as $item)
								<option value="{{$item->item_no}}" data-weight="{{$item->p_weight}}" data-financial="{{$item->amount}}">
									 {{$item_ids[$item->item_no]}} - {{$item->item->scope_of_work}}  
								</option>
							@endforeach
						</select>
					</div>  

					<div class="form-group  col-xs-12 col-sm-12">
						<label for="in-weight-ro">Weight(%)</label>
						<input type="text" class="form-control" id="in-weight-ro" readonly="readonly">
					</div>  
					<div class="form-group  col-xs-12 col-sm-12">
						<label for="in-weight-ro">Financial(%)</label>
						<input type="text" class="form-control" id="in-financial-ro" readonly="readonly">
					</div>  

					<div class="form-group  col-xs-12 col-sm-12">
						<label for="in-physical">Physical(%)</label>
						<input type="text" class="form-control" id="in-physical">
					</div>  

					<div class="form-group  col-xs-12 col-sm-12">
						<label for="in-financial">Financial(Php)</label>
						<input type="text" class="form-control" id="in-financial">
					</div>  
				    <div class="clearfix"></div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<div class="alert alert-danger pull-left" style="margin-bottom: 0px; margin-left: 0px; padding: 5px 10px; display: none;">Please complete all Fields </div>
				<a id="spi-planneditems_modal_form_submit" href="{{ URL::to('spi-accomplishment')}}" class="btn btn-success" 
					data-monthnum="" data-project="{{$project->project_id}}">
					<i class="glyphicon glyphicon-floppy-disk"></i> Submit
				</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="spi-planneditems_modal-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
				<form id="spi-planneditems_modal_edit-delform">
					<input type="hidden" id="del-planid" name="del-planid" value="">
					<input type="hidden" id="method" name="method" value="delete">
					<p class="col-xs-2" style="text-align: right;">
						<i class="glyphicon glyphicon-question-sign" style="color: #f4543c; font-size: 30px;"></i>
					</p>
					<p class="col-xs-10" style="font-size: 15px; padding-top: 5px; padding-bottom: 5px;">Are you sure you want to remove 
						 this Program of Work from this month?</p>
					<div class="clearfix"></div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="alert alert-danger pull-left" style="margin-bottom: 0px; margin-left: 0px; padding: 5px 10px; display: none;">You must clear the actual plan first </div>
				<a id="spi-planneditems_modal_edit-delform_submit" href="" class="btn btn-danger">Yes</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">No</a>
			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="spi-accmplisheditems_modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-body">
				{{ Form::open(array('id' => 'spi-accmplisheditems_modal_form')) }}
					<div class="form-group  col-xs-12 col-sm-12">
						<label for="in-acc-item">Scope of Work</label>
						<input type="text" class="form-control" id="in-acc-item" readonly="readonly">
					</div>  

					<div class="form-group  col-xs-12 col-sm-12">
						<label for="in-acc-weight-ro">Weight(%)</label>
						<input type="text" class="form-control" id="in-acc-weight-ro" readonly="readonly">
					</div>  

					<div class="form-group  col-xs-6 col-sm-6">
						<label for="in-acc-physical-ro">Planned Physical(%)</label>
						<input type="text" class="form-control" id="in-acc-physical-ro" readonly="readonly">
					</div>  

					<div class="form-group  col-xs-6 col-sm-6">
						<label for="in-acc-physical">Actual Physical(%)</label>
						<input type="text" class="form-control" id="in-acc-physical">
					</div>  

					<div class="form-group  col-xs-6 col-sm-6">
						<label for="in-acc-financial-ro">Planned Financial(Php)</label>
						<input type="text" class="form-control" id="in-acc-financial-ro" readonly="readonly">
					</div>  

					<div class="form-group  col-xs-6 col-sm-6">
						<label for="in-acc-financial">Actual Financial(Php)</label>
						<input type="text" class="form-control" id="in-acc-financial">
					</div>  					
				    <div class="clearfix"></div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<div class="alert alert-danger pull-left" style="margin-bottom: 0px; margin-left: 0px; padding: 5px 10px; display: none;">Please complete all Fields </div>
				<a id="spi-accmplisheditems_modal_form_submit" href="{{ URL::to('spi-accomplishment')}}" class="btn btn-success" 
					data-year="" data-month="" data-project="{{$project->project_id}}" data-value="">
					<i class="glyphicon glyphicon-floppy-disk"></i> Submit
				</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
			</div>
        </div>
    </div>
</div>


<script>
function yearValidation(year,ev) {

  var text = /^[0-9]+$/;
  if(ev.type=="blur" || year.length==4 && ev.keyCode!=8 && ev.keyCode!=46) {
    if (year != 0) {
        if ((year != "") && (!text.test(year))) {

            alert("Please Enter Numeric Values Only");
            return false;
        }

        if (year.length != 4) {
            alert("Year is not proper. Please check");
            return false;
        }
        var current_year=new Date().getFullYear();
        if((year < 1920) || (year > current_year))
            {
            alert("Year should be in range 1920 to current year");
            return false;
            }
        return true;
    } }
}

    $(document).ready(function () {
	    $('#dataTables-example').dataTable();

    	setTimeout(function(){
    		$('.main-body > div.alert').slideUp();
    	}, 5000);


		$('#in-itemunitcost, #in-itemamount, #in-pweight, #in-aweight, #in-daystarget, #in-daysactual,'+
		  '#edit-itemunitcost, #edit-itemamount, #edit-pweight, #edit-aweight, #edit-daystarget, #edit-daysactual,'+
		  '#in-physical, #in-financial, #in-acc-physical, #in-acc-financial').keydown(function(event) {
				if ( event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
				}
				else {
					if (event.keyCode < 48 || event.keyCode > 57 ) {
						event.preventDefault();	
					}	
				}
			});

        /*
         *  ADDING a FUNDED PSA PRIORITY
         */
		$('#spi-accomplish_modal-add_show').on('click', function(event)
		{
			console.log('asdasd');
			$('#spi-accomplish_modal-add').modal('show');
			return false;
		});

		$('#in-itemnum').change(function(event)
		{
			var itemnum = $(event.currentTarget).val();
			if (itemnum == '*')
			{
				console.log('new');
				$('#in-itemscope').removeAttr('disabled').val('')
				$('.in-itemscope').slideDown();
				$('#in-itemunit').removeAttr('disabled').val('');
			}
			else
			{
				$('.in-itemscope').hide();
				$('#in-itemscope').attr('disabled', 'disabled').val($('#op' + itemnum).data('scope'));
				$('#in-itemunit').attr('disabled', 'disabled').val($('#op' + itemnum).data('unit'));
			}

			return false;
		});

		$('#spi-accomplish_modal_form_submit').click(function(event)
		{
			var itemnum = $('#in-itemnum').val();
			if(itemnum != '')
			{
					$('.modal').loading(true);

				$.ajax({
			  		type: "POST",
			  		url:  $(event.currentTarget).attr('href'),
					data: {
						'in-projectid'       : $('#in-projectid').data('value'),
		  				'in-itemnum'         : itemnum,
		  				'in-itemid'          : $('#in-itemid').val(),
		  				'in-itemscope'       : $('#in-itemscope').val(),
		  				'in-itemunit'        : $('#in-itemunit').val(),
		  				'in-itemunitcost'    : $('#in-itemunitcost').val(),
		  				'in-itemamount'      : $('#in-itemamount').val(),
		  				'in-accmplsh'        : $('#in-accmplsh').val()
					}
				})
			  	.done(function( msg ) {
			  		console.log(msg);
			  		console.log('DONE');
			  		$('#spi-accomplish_modal-add').modal('hide');
			  		window.location = window.location.href;
					$('.modal').loading(false);

			 	});
			}

			return false;
		});

		/*
		 * DELETING an Accomplishment Item
		 */
	 	$('.spi-accomplishment_modal-del_show').on('click', function(event){
			var id = $(event.currentTarget).parents('tr').data('rowid');
			$('#del-id').val(id);
			$('#spi-accomplishment_modal-del').modal('show');
			return false;
      	});

		$('#spi-accomplishment_modal_edit-delform_submit').on('click', function(event)
		{
			var $url = window.location.href;
			console.log('URL : ' + $url);
			
			$.ajax({
		  		type: "DELETE",
		  		url:  $url,
		  		data: {
		  			'del-id' : $('#del-id').val(),
		  			'mode'   : 'del_items'
		  		 }
			})
		  	.done(function( msg ) {
		    	$('#spi-accomplishment_modal-del').modal('hide');
		    	window.location = window.location.href;
		  	});

			return false;
		});

		/*
		 * UPDATING an Accomplishment Item
		 */
	 	$('.spi-accomplishment_modal-update_show').on('click', function(event){
	 		console.log('YOOOOODDDDAAA');
	 		
			var $current_row = $(event.currentTarget).parents('tr');
			
			var id = $(event.currentTarget).parents('tr').data('rowid');
			$('#edit-accitemid').val(id);

			var item_no     = $current_row.find('.row-item_no').data('value');
			$('#edit-itemnum').val(item_no);
			var item_id     = $current_row.find('.row-item_no').data('id');
			$('#edit-itemid').val(item_id);
			var item_scope  = $current_row.find('.row-item_scope').data('value');
			$('#edit-itemscope').val(item_scope);
			var item_qty    = $current_row.find('.row-item_qty').data('value');
			$('#edit-itemamount').val(parseFloat(item_qty).toFixed(2));
			var item_unit   = $current_row.find('.row-item_unit').data('value');
			$('#edit-itemunit').val(item_unit);
			var item_cost   = $current_row.find('.row-item_cost').data('value');
			$('#edit-itemunitcost').val(parseFloat(item_cost).toFixed(2));
			var item_pweight = $current_row.find('.row-item_pweight').data('value');
			$('#edit-pweight').val(parseFloat(item_pweight).toFixed(2));
			
			$('#spi-accomplish_modal-update').modal('show');
			return false;
      	});

		$('#spi-accomplish_modal_updateform_submit').on('click', function(event)
		{
			var $url = window.location.href;
			console.log('URL : ' + $url);
					$('.modal').loading(true);
		
			$.ajax({
		  		type: "PUT",
		  		url:  $url,
				data: {
					'in-accitemid'       : $('#edit-accitemid').val(),
					'in-projectid'       : $('#in-projectid').data('value'),
	  				'in-itemnum'         : $('#edit-itemnum').val(),
	  				'in-itemscope'       : $('#edit-itemscope').val(),
	  				'in-itemunit'        : $('#edit-itemunit').val(),
	  				'in-itemunitcost'    : $('#edit-itemunitcost').val(),
	  				'in-itemamount'      : $('#edit-itemamount').val(),
	  				'in-itempweight'     : $('#edit-pweight').val(),
	  				'in-itemaweight'     : $('#edit-aweight').val(),
	  				'in-daystarget'      : $('#edit-daystarget').val(),
	  				'in-daysactual'      : $('#edit-daysactual').val(),
		  		    'in-mode'            : 'add_item'
				}
			})
		  	.done(function( msg ) {
		  		console.log(msg);
		  		console.log('DONE');
		  		$('#spi-accomplish_modal-update').modal('hide');
		  		window.location = window.location.href;
					$('.modal').loading(false);

		 	});

			return false;
		});


		$('#spi-planneditem_modal-add_show').on('click', function(event)
		{
			console.log('asdasd');
			console.log($('.planned-months:last').data('value'));
			if($('.planned-months').length == 0)
			{
				$('#in-monthnum').val(1);	
			}
			else
			{
				$('#in-monthnum').val(parseInt($('.planned-months:last').data('value')) + 1);		
			}
			
			$('#spi-planneditem_modal-add').modal('show');
			return false;
		});

		function zeroFill( number, width )
		{
		  width -= number.toString().length;
		  if ( width > 0 )
		  {
		    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
		  }
		  return number + ""; // always return a string
		}

		$('#spi-planneditem_modal_form_submit').on('click', function(event)
		{
			$('#spi-planneditem_modal-add .alert').slideUp();
			var month_num  = $('#in-monthnum').val();
			var id         = 'planned-months_' + month_num;
			if(month_num)
			{
				if($('#' + id).length == 0)
				{
					var html = '<div class="planned-months panel panel-default" data-value="'+ month_num + '">'+
						       '<div class="panel-heading" role="tab" id="heading'+month_num+'">'+
						       '<h4 class="panel-title">'+
						       '<a data-toggle="collapse" data-parent="#accordion" href="#'+id+'" aria-expanded="false" aria-controls="'+id+'" '+ 
						       ' style="display: block;">' +
						          'Month ' + month_num +
						       '</a>'+
						       '</h4>'+
						       '</div>'+
						       '<div id="'+ id + '" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading'+month_num+'">'+
						       '<div class="panel-body">'+
							   '<table class="table table-striped table-bordered">'+
									'<thead>'+
											'<th style="width: 25%;">Physical</th>'+
											'<th style="width: 25%;">Financial</th>'+
											'<th style="width: 5%; text-align: center;">Action</th>'+
										'</tr>'+
									'</thead>'+
									'<tbody>'+
										'<tr><td colspan="5">No Items</td></tr>'+
									'</tbody>'+
								'</table>'+
								'<button type="button" class="btn btn-primary spi-planneditems_modal-add_show" '+
								'data-monthnum="' + month_num + '" data-startend="'+ $('#in-startend').val() +'"><i class="fa fa-plus"></i> Add Program of Works </button>'+
						       '</div>'+
						       '</div>'+
						  	   '</div>';

					var $panels  = $('#accordion > .panel');
					var i= 0;
					for(; i < $panels.length; ++i)
					{
						if(parseInt(month_num) < parseInt($panels.eq(i).data('value')))
						{
							console.log('pasok');
							break;
						}
					}

					if(i == $panels.length) $('#accordion').append(html);
					else $panels.eq(i).before(html);

					$('#in-monthnum').val('');
					$('#spi-planneditem_modal-add').modal('hide');
					$('#' + id).promise().done(function()
					{
						$('#accordion .collapsed').collapse('hide');
					});
				}
				else
				{
					$('#spi-planneditem_modal-add .alert').text('Month already exists.').slideDown();		
				}
			}
			else
			{
				$('#spi-planneditem_modal-add .alert').text('Please complete all fields.').slideDown();
			}
			
			return false;
		});

		$('body').on('click', '.spi-planneditems_modal-add_show', function(event)
		{
			console.log('asdasd');
			console.log($(event.currentTarget).data('monthnum'));	

			var $current_panel = $(event.currentTarget).parent();
			var $current_items = $current_panel.find('.row-planitem_no');
			$('#spi-planneditems_modal-add #in-item option').show();
			for(var i=0; i < $current_items.length; ++i)
			{
				$('#spi-planneditems_modal-add #in-item option[value="' + $current_items.eq(i).data('value') + '"]').hide();
			}

			console.log($(event.currentTarget).data('monthnum'));
			$('#spi-planneditems_modal_form_submit').data('monthnum', $(event.currentTarget).data('monthnum'));
			$('#spi-planneditems_modal_form_submit').attr('data-monthnum', $(event.currentTarget).data('monthnum'))
			$('#spi-planneditems_modal_form_submit').attr('data-startend', $(event.currentTarget).data('startend'))
			$('#spi-planneditems_modal-add').modal('show');
			return false;
		});

		$('#spi-planneditems_modal_form_submit').on('click', function(event)
		{
			$('#spi-planneditems_modal-add .alert').slideUp();
			console.log('asdasd');
			var month_num     = $(event.currentTarget).data('monthnum');	
			var acc_report_id = $(event.currentTarget).data('project');
			var item          = $('#in-item').val();
			var physical      = $('#in-physical').val();
			var financial     = $('#in-financial').val();
			var weight        = $('#in-weight-ro').val();
			var startend      = $('#in-startend').val();

			if(item && physical && financial)
			{
				if(parseFloat(weight) >= parseFloat(physical))
				{
					var $url = window.location.href;
					console.log('URL : ' + $url);
					$('.modal').loading(true);
				
					$.ajax({
				  		type: "PUT",
				  		url:  $url,
						data: {
							'in-projectid'   : acc_report_id,
			  				'in-monthnum'    : month_num,
			  				'in-startend'    : startend,
			  				'in-item'        : item,
			  				'in-physical'    : physical,
			  				'in-financial'   : financial,
			  				'in-mode'        : 'add_plan'
						}
					})
				  	.done(function( msg ) {
				  		console.log(msg);
						$('#spi-planneditems_modal-add form input,select').val('');
						$('#spi-planneditems_modal-add').modal('hide');
				  		window.location = window.location.href;
					$('.modal').loading(false);

				 	}).error(function(){
					$('.modal').loading(false);
				 		
				 	});
				}
				else
				{
					$('#spi-planneditems_modal-add .alert').text('Please enter Physical value less or equal to weight.').slideDown();		
				}
			}
			else
			{
				$('#spi-planneditems_modal-add .alert').text('Please complete all fields.').slideDown();
			}

			return false;
		});

		$('#in-item').change(function(event)
		{
			var item = $(event.currentTarget).val();
			var weight = $('#in-item option[value="'+ item +'"]').data('weight');
			var financial = $('#in-item option[value="'+ item +'"]').data('financial');

			$('#in-weight-ro').val(parseFloat(weight).toFixed(2));
			$('#in-financial-ro').val(parseFloat(financial).toFixed(2));

			return false;
		});

		$('body').on('click', '.spi-planneditems_modal-del_show', function(event)
		{
			console.log($(event.currentTarget).data('value'));	
			$('#del-planid').val($(event.currentTarget).data('value'));
			$('#spi-planneditems_modal-del').modal('show');
			return false;
		});

		$('#spi-planneditems_modal_edit-delform_submit').on('click', function(event)
		{
			var $url = window.location.href;
			console.log('URL : ' + $url);
			$('.modal').loading(true);
			
			$.ajax({
		  		type: "DELETE",
		  		url:  $url,
		  		data: {
		  			'del-id' : $('#del-planid').val(),
		  			'mode'   : 'del_plans'
		  		 }
			})
		  	.done(function( msg ) {
		    	$('#spi-planneditems_modal-del').modal('hide');
		    	window.location = window.location.href;
				$('.modal').loading(false);

		  	}).error(function(res){
				$('.modal').loading(false);

		  		$('#spi-planneditems_modal-del .alert').text(res.message).slideDown();	

		  		  setTimeout(function(){ $('#spi-planneditems_modal-del .alert').hide(); }, 2000);
		  	});

			return false;

		});


		$('body').on('click', '.spi-accmplisheditems_modal-add_show', function(event)
		{
			var id = $(event.currentTarget).data('value');
			console.log(id);
			$('#spi-accmplisheditems_modal_form_submit').data('value', id);
			var item = $(event.currentTarget).data('item');
			$('#in-acc-item').val($(event.currentTarget).data('pow'));
			var weight = $('#in-item option[value="'+ item +'"]').data('weight');	
			$('#in-acc-weight-ro').val(parseFloat(weight).toFixed(2));
			$('#in-acc-physical-ro').val(parseFloat($(event.currentTarget).data('physical')).toFixed(2));
			$('#in-acc-financial-ro').val(parseFloat($(event.currentTarget).data('financial')).toFixed(2));
			if($(event.currentTarget).data('aphysical')){
				console.log('asdads');
				$('#in-acc-physical').val(parseFloat($(event.currentTarget).data('aphysical')).toFixed(2));	
			}
			if($(event.currentTarget).data('afinancial')){
				$('#in-acc-financial').val(parseFloat($(event.currentTarget).data('afinancial')).toFixed(2));	
			}
			
			$('#spi-accmplisheditems_modal-add').modal('show');

			return false;
		});

		$('#spi-accmplisheditems_modal_form_submit').on('click', function(event)
		{
			$('#spi-accmplisheditems_modal-add .alert').slideUp();
			var id         = $(event.currentTarget).data('value');
			var weight     = $('#in-acc-weight-ro').val();
			var physical   = $('#in-acc-physical').val();
			var financial  = $('#in-acc-financial').val();

			if(physical && financial)
			{
				if(parseFloat(weight) >= parseFloat(physical))
				{
					var $url = window.location.href;
					console.log('URL : ' + $url);
					$('.modal').loading(true);
					$.ajax({
				  		type: "PUT",
				  		url:  $url,
						data: {
							'id'   		     : id,
			  				'in-physical'    : physical,
			  				'in-financial'   : financial,
			  				'in-mode'        : 'add_actual'
						}
					})
				  	.done(function( msg ) {
				  		console.log(msg);
				  		console.log('DONE');
						$('#spi-accmplisheditems_modal-add input,select').val('');
						$('#spi-accmplisheditems_modal-add').modal('hide');
				  		window.location = window.location.href;

				 	});
				}
				else
				{
					$('#spi-accmplisheditems_modal-add .alert').text('Please enter a Physical value less or equal to weight.').slideDown();
				}
			}
			else
			{
				$('#spi-accmplisheditems_modal-add .alert').text('Please complete all fields.').slideDown();
			}

			return false;
		});

		$('#in-startend').daterangepicker({format: 'MM/DD/YYYY' });
		$('.planned-months .panel-collapse:last').collapse();
		$('#accordion2 .panel-collapse:last').collapse();
	});
</script>
@stop