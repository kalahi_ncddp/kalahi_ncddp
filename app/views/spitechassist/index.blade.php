@extends('layouts.default')

@section('content')
	    <div class="row">
		    <div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="col-xs-5" style="padding-left: 0;">  {{ $title }}</h4>
						@if($position->position=='ACT')
                    <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                    <span class="hidden module">spi-techassist</span>
             @endif

						<a id="spi-techassist_modal-add_show" class="btn btn-primary pull-right" 
							href="">
							<i class="fa fa-plus"></i> Add New Technical Assistance
						</a>
						<div class="clearfix"></div>
					</div>
					<div class="panel-body">

						@if ($store_status !== null)
							@if ($store_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> A New Technical Assistance was successfully added!</strong>
								</div>
							@elseif ($store_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Technical Assistance can not be added</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if ($update_status !== null)
							@if ($update_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> Technical Assistance was successfully updated!</strong>
								</div>
							@elseif ($update_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Technical Assistance can not be updated</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if ($destroy_status !== null)
							@if ($destroy_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> Technical Assistance was successfully removed!</strong>
								</div>
							@elseif ($destroy_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Technical Assistance can not be removed</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif

						<table class="table table-striped table-bordered" id="spi-commtschklst_table">
							<thead>
								<tr>
									<th>
		                          @if($position->position=='ACT')
		                            <input type="checkbox" id="selecctall"/>
		                            @endif
		                            </th>
									<th style="width: 25%;">Barangay</th>
									<th style="width: 10%;">Cycle</th>
									<th style="width: 20%;">Program</th>
									<th style="width: 10%;">Date From</th>
									<th style="width: 10%;">Date To</th>
									<th style="width: 10%;">Category</th>
									<th style="width: 15%; text-align: center;">Action</th>
								</tr>
							</thead>
							<tbody>

							@foreach ($assistances as $assistance)
								<tr data-rowid="{{{$assistance['id']}}}">
									<td>
                               @if($position->position=='ACT')
                                  @if($assistance->is_draft==0)
                                    {{ $reference->approval_status($assistance->id, 'ACT') }}
                                  @else
                                      <span class="label label-warning">draft</span>
                                  @endif
                                @endif
                            </td>

									<td class="cell-barangay" data-value="{{$assistance['barangay_psgc']}}">
										{{($assistance['barangay_psgc'] != '') ? 
											$barangays[$assistance['barangay_psgc']] : '-- All Barangays --'}}
									</td>
									<td class="cell-cycleid" data-value="{{$assistance['cycle_id']}}">{{$assistance['cycle_id']}}</td>
									<td class="cell-programid" data-value="{{$assistance['program_id']}}">{{$assistance['program_id']}}</td>
									<td class="cell-datefrom" data-value="{{$assistance['startdate']}}">
										{{date("m/d/Y", strtotime($assistance['startdate']))}}
									</td>
									<td class="cell-dateto" data-value="{{$assistance['enddate']}}">
										{{date("m/d/Y", strtotime($assistance['enddate']))}}
									</td>
									<td class="cell-category" data-value="{{$assistance['tech_category']}}">
										{{$assistance['tech_category']}}
									</td>
									<td style="text-align: center">
										<input type="hidden" class="cell-ceacactivity" data-value="{{$assistance['activity_code']}}"/>
										<input type="hidden" class="cell-providedby" data-value="{{$assistance['provider']}}"/>
										<input type="hidden" class="cell-details" data-value="{{$assistance['tech_details']}}"/>
	    								@if( !is_review($assistance->id) )
										

										<a href="" class="btn btn-sm btn-info spi-techassist_modal-edit_show" style="margin-right: 0.5rem;"><i class="glyphicon glyphicon-edit"></i> Edit</0>
										<a href="" class="btn btn-sm btn-danger spi-techassist_modal-del_show"><i class="glyphicon glyphicon-trash"></i> Delete</a>
										@endif

									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<!-- lets a
			<a href="" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-pencil"></i></a>dd show modal ref to show url -->


		<div class="modal fade" id="spi-techassist_modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">New Technical Assistance</h4>
                    </div>
                    <div class="modal-body">
						{{ Form::open(array('id' => 'spi-techassist_modal_form', 'route' => 'spi-techassist.store')) }}
							<div class="form-group col-xs-6 col-md-6">
								<label for="in-cycleid">Cycle</label>
								<input type="text" id="in-cycleid" name="in-cycleid" class="form-control"
									value="{{{$cycle}}}" readonly="readonly">
							</div>
							<div class="form-group col-xs-6 col-md-6">
								<label for="in-programid">Program</label>
								<input type="text" id="in-programid" name="in-programid" class="form-control" 
									value="{{{$program}}}" readonly="readonly">
							</div>
							<div class="form-group col-xs-6 col-md-6">
								<label for="in-province">Province</label>
								<input type="text" id="in-province" name="in-province" class="form-control" 
									value="{{{$province}}}" readonly="readonly">
							</div>


							<div class="form-group col-xs-6 col-md-6">
								<label for="in-municipalityname">Municipalitys</label>
								<input type="text" id="in-municipalityname" name="in-municipalityname" class="form-control"
									value="{{{$municipality}}}" readonly="readonly">
								<input type="hidden" id="in-municipalitypsgc" name="in-municipalitypsgc" value="{{{$municipality_psgc}}}">
							</div>


							


							<div class="form-group col-xs-6 col-md-6">
								<label for="in-barangay">Barangay</label>
								<select name="in-barangay" id="in-barangay" class="form-control">
									<option value="">-- for all Barangays --</option>
									@foreach($barangays as $id => $name)
									<option value="{{$id}}">{{$name}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group col-xs-6 col-md-6">
								<label for="in-ceacactivity">Activity</label>
								<select name="in-ceacactivity" id="in-ceacactivity" class="form-control">
									<option value="">-- Select Activity --</option>
									@foreach($activities as $activity)
									<option value="{{$activity['activity_code']}}"
										class="{{{($activity['is_brgy_activity'] == 1) ? 'ceac_brgy' : 'ceac_muni'}}}">
										{{$activity['activity_name']}}
									</option>
									@endforeach
								</select>
							</div>
						    <div class="form-group col-xs-6 col-md-6">
							 	<label for="in-datefrom_cont">Date From</label>
								<div class='input-group date' id='in-datefrom_cont'>
				                	<input class="form-control" required="required" type="text" 
				                	   id="in-datefrom" name="in-datefrom" value="{{date('m/d/Y')}}"> <br>
				                	<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					            </div>
		  				    </div>
						    <div class="form-group col-xs-6 col-md-6">
							 	<label for="in-dateto_cont">Date To</label>
								<div class='input-group date' id='in-dateto_cont'>
				                	<input class="form-control" required="required" type="text" 
				                	   id="in-dateto" name="in-dateto" value="{{date('m/d/Y')}}"> <br>
				                	<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					            </div>
		  				    </div>
							<div class="form-group col-xs-6 col-md-6">
								<label for="in-providedby">Provided by</label>
								<select name="in-providedby" id="in-providedby" class="form-control">
									<option value="">-- Select Provider --</option>
									@foreach($tech_provider as $provider)
									<option value="{{$provider['name']}}">{{$provider['name']}}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group col-xs-6 col-md-6">
								<label for="in-providedto">Provided To</label>
								<select name="in-providedto" id="in-providedto" class="form-control">
									<option value="">-- Select Provider --</option>
									@foreach($tech_provider_to as $provider_to)
									<option value="{{$provider_to['name']}}">{{$provider_to['name']}}</option>
									@endforeach
								</select>
							</div>


							<div class="form-group col-xs-6 col-md-6">
								<label for="in-category">Category</label>
								<select name="in-category" id="in-category" class="form-control">
									<option value="">-- Select Category --</option>
									@foreach($tech_category as $category)
									<option value="{{$category['category']}}">{{$category['category']}}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group col-xs-6 col-md-6">
								<label for="in-category">Purpose</label>
								<input type="text" id="in-purpose" name="in-purpose" class="form-control">
							</div>
						    <div class="form-group col-xs-12 col-md-12">
								<label for="in-details">Details</label>
								<textarea rows="2" class="form-control" placeholder="Put your remarks here..."
										  id="in-details" name="in-details">
								</textarea>
						    </div>
						    <div class="clearfix"></div>
						{{ Form::close() }}
					</div>
					<div class="modal-footer">
						<a id="spi-techassist_modal_form_submit" href="" class="btn btn-primary">
							<i class="glyphicon glyphicon-floppy-disk"></i> Submit
						</a>
						<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
					</div>
	            </div>
            </div>
        </div>

		<div class="modal fade" id="spi-techassist_modal-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
						<form>
							<input type="hidden" id="del-id" name="del-id" value="">
							<p class="col-xs-2" style="text-align: right;">
								<i class="glyphicon glyphicon-question-sign" style="color: #f4543c; font-size: 30px;"></i>
							</p>
							<p class="col-xs-10" style="font-size: 15px; padding-top: 5px; padding-bottom: 5px;">Are you sure you want to delete 
								 this technical assistance?</p>
							<div class="clearfix"></div>
						</form>
					</div>
					<div class="modal-footer">
						<a id="spi-techassist_modal_delform_submit" href="" class="btn btn-danger">Yes</a>
						<a href="" class="btn bg-navy" data-dismiss="modal">No</a>
					</div>
	            </div>
            </div>
        </div>

		<div class="modal fade" id="spi-techassist_modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Technical Assistance Details</h4>
                    </div>
                    <div class="modal-body">
						{{ Form::open(array('id' => 'spi-techassist_editmodal_form', 'route' => 'spi-techassist.update')) }}
							<input type="hidden" id="edit-id" name="edit-id" value="">
							<div class="form-group col-xs-6 col-md-6">
								<label for="edit-cycleid">Cycle</label>
								<input type="text" id="edit-cycleid" name="edit-cycleid" class="form-control"
									value="{{{$cycle}}}" readonly="readonly">
							</div>
							<div class="form-group col-xs-6 col-md-6">
								<label for="edit-programid">Program</label>
								<input type="text" id="edit-programid" name="edit-programid" class="form-control" 
									value="{{{$program}}}" readonly="readonly">
							</div>
							<div class="form-group col-xs-6 col-md-6">
								<label for="edit-province">Province</label>
								<input type="text" id="edit-province" name="edit-province" class="form-control" 
									value="{{{$province}}}" readonly="readonly">
							</div>
							<div class="form-group col-xs-6 col-md-6">
								<label for="edit-municipalityname">Municipality</label>
								<input type="text" id="edit-municipalityname" name="edit-municipalityname" class="form-control"
									value="{{{$municipality}}}" readonly="readonly">
								<input type="hidden" id="edit-municipalitypsgc" name="edit-municipalitypsgc" value="{{{$municipality_psgc}}}">
							</div>
							<div class="form-group col-xs-6 col-md-6">
								<label for="edit-barangay">Barangay</label>
								<select name="edit-barangay" id="edit-barangay" class="form-control edit-field">
									<option value="">-- for all Barangays --</option>
									@foreach($barangays as $id => $name)
									<option value="{{$id}}">{{$name}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group col-xs-6 col-md-6">
								<label for="edit-ceacactivity">CEAC Activity</label>
								<select name="edit-ceacactivity" id="edit-ceacactivity" class="form-control edit-field">
									<option value="">-- Select Activity --</option>
									@foreach($activities as $activity)
									<option value="{{$activity['activity_code']}}"
										class="{{{($activity['is_brgy_activity'] == 1) ? 'ceac_brgy' : 'ceac_muni'}}}">
										{{$activity['activity_name']}}
									</option>
									@endforeach
								</select>
							</div>
						    <div class="form-group col-xs-6 col-md-6">
							 	<label for="edit-datefrom_cont">Date From</label>
								<div class='input-group date' id='edit-datefrom_cont'>
				                	<input class="form-control edit-field" required="required" type="text" 
				                	   id="edit-datefrom" name="edit-datefrom" value="{{date('m/d/Y')}}"> <br>
				                	<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					            </div>
		  				    </div>
						    <div class="form-group col-xs-6 col-md-6">
							 	<label for="edit-dateto_cont">Date To</label>
								<div class='input-group date' id='edit-dateto_cont'>
				                	<input class="form-control edit-field" required="required" type="text" 
				                	   id="edit-dateto" name="edit-dateto" value="{{date('m/d/Y')}}"> <br>
				                	<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					            </div>
		  				    </div>
							<div class="form-group col-xs-6 col-md-6">
								<label for="edit-providedby">Provided by</label>
								<select name="edit-providedby" id="edit-providedby" class="form-control edit-field">
									<option value="">-- Select Provider --</option>
									@foreach($tech_provider as $provider)
									<option value="{{$provider['name']}}">{{$provider['name']}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group col-xs-6 col-md-6">
								<label for="edit-category">Category</label>
								<select name="edit-category" id="edit-category" class="form-control edit-field">
									<option value="">-- Select Category --</option>
									@foreach($tech_category as $category)
									<option value="{{$category['category']}}">{{$category['category']}}</option>
									@endforeach
								</select>
							</div>
						    <div class="form-group col-xs-12 col-md-12">
								<label for="edit-details">Details</label>
								<textarea rows="2" class="form-control edit-field"
										  id="edit-details" name="edit-details">
								</textarea>
						    </div>
						    <div class="clearfix"></div>
						{{ Form::close() }}
					</div>
					<div class="modal-footer">
						<a id="spi-techassist_modal_init_edit" href="" class="btn btn-primary">
							<i class="glyphicon glyphicon-save"></i> Save
						</a>
						<a id="spi-techassist_modal_editform_submit" href="" class="btn btn-success">
							<i class="glyphicon glyphicon-floppy-disk"></i> Submit
						</a>
						<a href="" class="btn bg-navy" data-dismiss="modal">Close</a>
					</div>
	            </div>
            </div>
        </div>

	<script>
      $(document).ready(function () {
        $('#spi-commtschklst_table').dataTable();

        /*
         *  ADDING a Municipal Sustainability Plan
         */
		$('#spi-techassist_modal-add_show').on('click', function(event)
		{
			console.log('asdasd');
			$('#spi-techassist_modal-add').modal('show');
			return false;
		});

		$('#spi-techassist_modal_form_submit').on('click', function()
		{
			console.log('asdasd');
			$.post( window.location.href, $( "#spi-techassist_modal_form" ).serialize())
			.done(function(msg){
				$('#spi-techassist_modal-add').modal('hide');
				window.location = window.location.href;
			});

			return false;
		});

		$('option.ceac_brgy').hide();
		$('option.ceac_muni').show();

		$('#in-barangay').change(function(event)
		{
			var barangay = $(event.currentTarget).val();
			if (barangay != '')
			{
				$('option.ceac_muni').hide();
				$('option.ceac_brgy').show();
			}
			else
			{
				$('option.ceac_brgy').hide();
				$('option.ceac_muni').show();
			}

			return false;
		});
		/* end of ADDING a committee */
        /*
         *  UPDATING a committee
         */
		$('.spi-techassist_modal-edit_show').on('click', function(event){

			var $row = $(event.currentTarget).parents('tr');
			$('#edit-id').val($row.data('rowid'));
			$('#edit-cycleid').val($row.find('td.cell-cycleid').data('value'));
			$('#edit-barangay').val($row.find('.cell-barangay').data('value'));
			$('#edit-ceacactivity').val($row.find('.cell-ceacactivity').data('value'));
			$('#edit-programid').val($row.find('td.cell-programid').data('value'));									
			$('#edit-providedby').val($row.find('.cell-providedby').data('value'));									
			$('#edit-category').val($row.find('td.cell-category').data('value'));									
			$('#edit-details').val($row.find('.cell-details').data('value'));	
			$('#edit-datefrom').val($.trim($row.find('.cell-datefrom').text()));	
			$('#edit-dateto').val($.trim($row.find('.cell-dateto').text()));	

			// $('.edit-field').prop('disabled', true);
			$('#spi-techassist_modal_editform_submit').fadeOut('fast');
			$('#spi-techassist_modal_init_edit').fadeIn('medium');
			$('#spi-techassist_modal-edit').modal('show');
			
			return false;
      	});		

		$('#spi-techassist_modal_init_edit').on('click', function(event)
		{
			$('#spi-techassist_modal_init_edit').fadeOut('', function()
			{
				$('.edit-field').prop('disabled', false);

				$('#spi-techassist_modal_editform_submit').fadeIn();
			});
		

			return false;
		});

		$('#spi-techassist_modal_editform_submit').on('click', function(event)
		{
			var $url = window.location.href + '/' + $('#edit-id').val();
			console.log($url);
			
			$.ajax({
		  		type: "PUT",
		  		url:  $url,
		  		data: $( "#spi-techassist_editmodal_form" ).serialize()
			})
		  	.done(function( msg ) {
		    	$('#spi-techassist_modal-edit').modal('hide');
		    	window.location = window.location.href;
		  	});

			return false;
		});
		/* end of UPDATING a committee */
        /*
         *  DELETING a committee
         */
		$('.spi-techassist_modal-del_show').on('click', function(event){
			var id = $(event.currentTarget).parents('tr').data('rowid');
			$('#del-id').val(id);
			$('#spi-techassist_modal-del').modal('show');
			return false;
      	});

		$('#spi-techassist_modal_delform_submit').on('click', function(event)
		{
			var $url = window.location.href + '/' + $('#del-id').val();
			console.log($url);

			$.ajax({
		  		type: "DELETE",
		  		url: $url
			})
		  	.done(function( msg ) {
		    	$('#spi-techassist_modal-del').modal('hide');
		    	window.location = window.location.href;
		  	});

			return false;
		});
		/* end of DELETING a committee */
	});
    </script>
@stop