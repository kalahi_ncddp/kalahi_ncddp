@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')

<div class="row">
  <div class="col-md-12">
    {{ HTML::linkRoute('spi_profile.show', 'Back to Sub Project Profile',
    	array($profile->project_id), array('class' => 'btn btn-default btn')) }}
	    	@if( !is_review($profile->project_id) )

    {{ HTML::linkRoute('spi_profile.procurement.edit', 'Edit',
      array($profile->project_id), array('class' => 'btn btn-info btn')) }}
      @endif
  </div>
</div>

<hr />

<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<tr>
					<th>Municipality</th>
					<td>{{ Report::get_municipality($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Barangay</th>
					<td>{{ Report::get_barangay($profile->barangay_id) }}</td>
				</tr>
				<tr>
					<th width="40%">Sub Project Name</th>
					<td>
						{{ $profile->project_name }}
						{{ Form::hidden('project_id', $profile->project_id) }}
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		Packages
	</div>
	<div class="panel-body">
		<div class="table-responsive" style="overflow: scroll">
			<table class="table table-bordered table-striped">
				<tr>
					<th rowspan="2" style="min-width: 100px">Package No.</th>
					<th rowspan="2" style="min-width: 150px">Package</th>
					<th rowspan="2" style="min-width: 150px">Mode of Procurement</th>
					<th rowspan="2" style="min-width: 150px">NOL Issued By</th>
					<th rowspan="2" style="min-width: 150px">Name of Supplier/Contractor</th>
					<th rowspan="2" style="min-width: 150px">Amount of Contract</th>
					<th rowspan="2" style="min-width: 150px">Pre-Procurement</th>
					<th colspan="2" style="min-width: 150px">Serving of Canvas or Posting of Invitation to Bid/Quote</th>
					<th rowspan="2" style="min-width: 150px">Pre Bid/Contractors Conf.</th>
					<th rowspan="2" style="min-width: 150px">Canvas/Bid Opening</th>
					<th rowspan="2" style="min-width: 150px">Post Qualification</th>
					<th rowspan="2" style="min-width: 150px">Date at ACT</th>
					<th rowspan="2" style="min-width: 150px">Date at RPMO</th>
					<th rowspan="2" style="min-width: 150px">Status</th>
					<th rowspan="2" style="min-width: 150px">Action</th>
				</tr>
				<tr>
					<th  style="min-width: 150px">Start Date</th>
					<th  style="min-width: 150px">End Date</th>
				</tr>
				@foreach ($procurements as $proc)
				<tr>
					<td>
						{{ $proc->package_no }}
					</td>
					<td>
						{{ $proc->package }}
					</td>
					<td>
						{{ $proc->mode }}
					</td>
					<td>
						{{ $proc->nol_issued }}
					</td>
					<td>
						{{ $proc->supplier_id }}
					</td>
					<td>
						{{ $proc->contract_amnt }}
					</td>
					<td>
						{{ toDate($proc->eproc_date) }}
					</td>
					<td>
						{{ toDate($proc->canvas_start) }}
					</td>
					<td>
						{{ toDate($proc->canvas_end) }}
					</td>
					<td>
						{{ toDate($proc->prebid_date) }}
					</td>
					<td>
						{{ toDate($proc->open_date) }}
					</td>
					<td>
						{{ toDate($proc->pqual_date) }}
					</td>
					<td>
						{{ toDate($proc->act_date) }}
					</td>
					<td>
						{{ toDate($proc->rpmo_date) }}
					</td>
					<td>
						{{ $proc->status }}
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>

@stop