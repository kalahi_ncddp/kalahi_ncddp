<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">	
			<table class="table table-bordered table-striped">
				<tr>
					<th>Municipality</th>
					<td>{{ Report::get_municipality($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Barangay</th>
					<td>{{ Report::get_barangay($profile->barangay_id) }}</td>
				</tr>
				<tr>
					<th width="40%">Sub Project Name</th>
					<td>
						{{ $profile->project_name }}
						{{ Form::hidden('project_id', $profile->project_id) }}
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		Packages
		<input type="button" class="btn btn-primary pull-right"
							value="Add Package" onclick="add_package()">
						 <div class="clearfix"></div>
	</div>
	<div class="panel-body">
		<div class="table-responsive" style="overflow: scroll">
			<table class="table table-bordered table-striped proc_table">
				<tr>
					<th rowspan="2" style="min-width: 100px">Package No.</th>
					<th rowspan="2" style="min-width: 150px">Package</th>
					<th rowspan="2" style="min-width: 150px">Mode of Procurement</th>
					<th rowspan="2" style="min-width: 150px">NOL Issued By</th>
					<th rowspan="2" style="min-width: 150px">Name of Supplier/Contractor</th>
					<th rowspan="2" style="min-width: 150px">Amount of Contract</th>
					<th rowspan="2" style="min-width: 150px">Pre-Procurement</th>
					<th colspan="2" style="min-width: 150px">Serving of Canvas or Posting of Invitation to Bid/Quote</th>
					<th rowspan="2" style="min-width: 150px">Pre Bid/Contractors Conf.</th>
					<th rowspan="2" style="min-width: 150px">Canvas/Bid Opening</th>
					<th rowspan="2" style="min-width: 150px">Post Qualification</th>
					<th rowspan="2" style="min-width: 150px">Date at ACT</th>
					<th rowspan="2" style="min-width: 150px">Date at RPMO</th>
					<th rowspan="2" style="min-width: 150px">Status</th>
					<th rowspan="2" style="min-width: 150px">Action</th>
				</tr>
				<tr>
					<th  style="min-width: 150px">Start Date</th>
					<th  style="min-width: 150px">End Date</th>
				</tr>
	
				@foreach ($procurements as $proc)
				<tr>
					<td>
						{{ Form::input('number', 'package_no[]', $proc->package_no, ['class' => 'form-control', 'min' => 0, 'step' => '1']) }}
					</td>
					<td>
						{{ Form::text('package[]', $proc->package, ['class' => 'form-control', '']) }}
					</td>
					<td>
						{{ Form::select('mode[]', $proc_modes, $proc->mode, ['class' => 'form-control', '']) }}
					</td>
					<td>
						{{ Form::select('nol_issued[]',[""=>"Select Issue","Not Required"=>"Not Required"]+$nol_issued_list, $proc->nol_issued, ['class' => 'form-control']) }}
					</td>
					<td>
						{{ Form::text('supplier_id[]',  $proc->supplier_id, ['class' => 'form-control']) }}
					</td>
					<td>
						{{ Form::input('number', 'contract_amnt[]', $proc->contract_amnt, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}
					</td>
					<td>
						{{ Form::input('text', 'eproc_date[]', isset($proc->eproc_date) ? date("m/d/Y", strtotime($proc->eproc_date)) : null, array('class' => 'form-control date', '')) }}
					</td>
					<td>
						{{ Form::input('text', 'canvas_start[]', isset($proc->canvas_start) ? date("m/d/Y", strtotime($proc->canvas_start)) : null, array('class' => 'form-control date', '')) }}
					</td>
					<td>
						{{ Form::input('text', 'canvas_end[]', isset($proc->canvas_end) ? date("m/d/Y", strtotime($proc->canvas_end)) : null, array('class' => 'form-control date', '')) }}
					</td>
					<td>
						{{ Form::input('text', 'prebid_date[]', isset($proc->prebid_date) ? date("m/d/Y", strtotime($proc->prebid_date)) : null, array('class' => 'form-control date', '')) }}
					</td>
					<td>
						{{ Form::input('text', 'open_date[]', isset($proc->open_date) ? date("m/d/Y", strtotime($proc->open_date)) : null, array('class' => 'form-control date', '')) }}
					</td>
					<td>
						{{ Form::input('text', 'pqual_date[]', isset($proc->pqual_date) ? date("m/d/Y", strtotime($proc->pqual_date)) : null, array('class' => 'form-control date', '')) }}
					</td>
					<td>
						{{ Form::input('text', 'act_date[]', isset($proc->act_date) ? date("m/d/Y", strtotime($proc->act_date)) : null, array('class' => 'form-control date', '')) }}
					</td>
					<td>
						{{ Form::input('text', 'rpmo_date[]', isset($proc->rpmo_date) ? date("m/d/Y", strtotime($proc->spmo_date)) : null, array('class' => 'form-control date', '')) }}
					</td>
					<td>
						{{ Form::select('status[]', $statuses, $proc->status, ['class' => 'form-control']) }}
					</td>
					<td><input type="button" class="btn btn-sm btn-danger btn-block" value="Delete" onclick="remove_package(this)"></td>
				</tr>
				@endforeach
				<tr id="template_proc_row" class="hide">
					<td>
						{{ Form::input('number', 'package_no[]', null, ['class' => 'form-control', 'min' => 0, 'step' => '1']) }}
					</td>
					<td>
						{{ Form::text('package[]', null, ['class' => 'form-control']) }}
					</td>
					<td>
						{{ Form::select('mode[]', $proc_modes, null, ['class' => 'form-control']) }}
					</td>
					<td>
						{{ Form::select('nol_issued[]', [""=>"Select Issue","Not Required"=>"Not Required"]+$nol_issued_list, "RPMO", ['class' => 'form-control']) }}
					</td>
					<td>
						{{ Form::text('supplier_id[]', null, ['class' => 'form-control']) }}
					</td>
					<td>
						{{ Form::input('number', 'contract_amnt[]', null, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}
					</td>
					<td>
						{{ Form::input('text', 'eproc_date[]', null, array('class' => 'form-control date')) }}
					</td>
					<td>
						{{ Form::input('text', 'canvas_start[]', null, array('class' => 'form-control date')) }}
					</td>
					<td>
						{{ Form::input('text', 'canvas_end[]', null, array('class' => 'form-control date')) }}
					</td>
					<td>
						{{ Form::input('text', 'prebid_date[]', null, array('class' => 'form-control date')) }}
					</td>
					<td>
						{{ Form::input('text', 'open_date[]', null, array('class' => 'form-control date')) }}
					</td>
					<td>
						{{ Form::input('text', 'pqual_date[]', null, array('class' => 'form-control date')) }}
					</td>
					<td>
						{{ Form::input('text', 'act_date[]', null, array('class' => 'form-control date')) }}
					</td>
					<td>
						{{ Form::input('text', 'rpmo_date[]', null, array('class' => 'form-control date')) }}
					</td>
					<td>
						{{ Form::select('status[]', $statuses, "O", ['class' => 'form-control']) }}
					</td>
					<td><input type="button" class="btn btn-sm btn-danger btn-block" value="Remove" onclick="remove_package(this)"></td>
				</tr>
				<tr id="proc_btn">
					<td colspan="16">
						
					</td>
				</tr>

			</table>
		</div>
	</div>
</div>
<!-- 
{{ Form::submit('Save Community Procurement Monitoring', array('class' => 'btn btn-primary')) }}
{{ HTML::linkRoute('spi_profile.show', 'Close',
  array($profile->project_id), array('class' => 'btn bg-navy btn')) }} -->

   <div class="col-md-12 ">
						<div class="form-group pull-right">
{{ Form::submit('Save Community Procurement Monitoring', array('class' => 'btn btn-primary')) }}
						            
				<!-- <a class="btn btn-default" href="{{URL::to('mun_trainings')}}">Cancel</a> -->
					{{ HTML::linkRoute('spi_profile.show', 'Cancel',
  array($profile->project_id), array('class' => 'btn btn-default')) }}
				
						</div>
					</div>
		                 			
						<div class="clearfix"></div>
{{ Form::close() }}

<script>
	$(document).ready(function() {
		
	});

	function add_package() {
		var row = $("#template_proc_row").clone();
		$(row).removeClass("hide");
		$("#proc_btn").before(row);

		$('.date').datepicker({
      format: 'mm/dd/yyyy',
      setDate: new Date()
    });
	}

	function remove_package(btn) {
		$(btn).parent().parent().remove();
	}

	function calculate_total_labor() {
		var tr_list = $("table.labor_generated tbody tr");
		var total = 0;

		for (var i = 0; i < tr_list.length-1; i++) {
			var num = parseInt($(tr_list[i]).find("td:nth-child(2) input").val() || 0);
			var days = parseInt($(tr_list[i]).find("td:nth-child(3) input").val() || 0);
			var rate = parseFloat($(tr_list[i]).find("td:nth-child(4) input").val() || 0);

			var cur_total = (num * days * rate);
			$(tr_list[i]).find("td:nth-child(5)").html(cur_total);

			total += cur_total;
		}

		$(tr_list[tr_list.length-1]).find("td:last-child").html(total);
	}

	function checkduplicate(e){
		var count = -1;
		var elements = document.getElementsByName('barangay[]');
		for (i=0; i<elements.length; i++) {
				if(e.value === elements[i].value){
					count++;
				}
		}
		if(count == 1){	
			e.setCustomValidity("Duplicate Entry not allowed");
		}else{
			e.setCustomValidity("");
		}
	}
</script>