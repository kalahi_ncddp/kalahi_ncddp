@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop
@section('content')
	<a href="{{ URL::previous() }}"class="btn btn-default"> Go Back </a>

	<hr>
	<div class="box box-primary">
		<div class="box-header">
			<h4 class="box-title">Activities</h4>
			<div class="box-tools pull-right">
                    <span>Legend:<br> <span style="background-color:yellow">yellow</span> Actual Dates based on encoded data in database modules</span>
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
            </div>
		</div>
		<div class="box-body">
			<table class="table table-bordered">
				<thead class="text-center">
					<tr>
					    <td rowspan="2">Seq of Activities</td>
						<td rowspan="2">Activity Code</td>
						<td rowspan="2">Activity Name</td>
						<td colspan="2">Planned</td>
						<td colspan="2">Actual</td>


						<td rowspan="2" style="width: 100px;">Option</td>
					</tr>
					<tr >
						<td style="width: 173px;">Start</td>
						<td style="width: 173px;">End</td>
						<td style="width: 173px;">Start</td>
						<td style="width: 173px;">End</td>
					</tr>
				</thead>
				<tbody>
					@foreach ($activities as $activity) 
					<tr>
					<td>{{ $activity->activity_no }}</td>
						<td class="activity_code">{{ $activity->activity_code }}</td>
						<td>{{ $activity->activity_name }}</td>
						<td>
							<span class="la-text">{{ toDate($activity->planBrgy($psgc_id,$cycle_id,$program)['startdate']) }}</span>
							<input  class="form-control dates plan-start hidden" value="{{ toDate($activity->planBrgy($psgc_id,$cycle_id,$program)['startdate']) }}"  name="date[]">
						</td>
						<td>
							<span class="la-text">{{ toDate($activity->planBrgy($psgc_id,$cycle_id,$program)['enddate']) }}</span>
							<input  class="form-control dates plan-end hidden" value="{{ toDate($activity->planBrgy($psgc_id,$cycle_id,$program)['enddate']) }}" name="date[]">
						</td>
						{{-- actual start--}}
						<td>
							<?php
								$date_starts = $activity->get_actual_date_brgy($psgc_id,$program,$cycle_id)['startdate'];
								$date_actual_start =  toDate($activity->planBrgy($psgc_id,$cycle_id,$program)['actual_start']);
								$date_start = isset($date_starts)  ?  toDate($date_starts) : $date_actual_start ;
							?>
							<span class="la-text" >
                                @if(isset($date_starts))
                                    <b style="background-color:yellow">{{  toDate($date_starts) }}</b>
                                @endif
							    <br>
							    {{ toDate($date_actual_start) }}
							 </span>

							@if(!isset($date_starts))
								<input  class="form-control dates actual-start hidden" value="{{  toDate($date_start) }}" name="date[]">
							@else
                                <span class="la-info hidden">
                                    {{ toDate($date_start) }}
                               </span>
							@endif
						</td>
						<td>
							<?php

								$date_ends = $activity->get_actual_date_brgy($psgc_id,$program,$cycle_id)['enddate'];
								$date_actual_end =  toDate( $activity->planBrgy($psgc_id,$cycle_id,$program)['actual_end'] );
								$end = $date_actual_end;
								$date_end = isset($date_ends)  ?  toDate($date_ends) : $date_actual_end ;
							?>

                            <span class="la-text">
                                {{-- print the actual date--}}
                                @if(isset($date_ends))
                                         <b style="background-color:yellow">{{  toDate($date_ends) }} </b><br>
                                @endif
                                {{-- print eh temporary actual date--}}
                                <span> {{ $end }} </span>
                            </span>
                            {{-- check if actual date from lgu is not empty --}}
							@if(!isset($date_ends))
									<input  class="form-control dates actual-end hidden" value="{{ toDate($date_end) }}" name="date[]">
							@else
							{{-- use the temporary --}}
							    <span class="la-info hidden">
                                      {{ toDate($date_end) }}
                                 </span>
							@endif


						</td>
						<td>
							<span class="btn btn-default edit"><i class="fa fa-edit"></i></span>

							<span class="btn btn-info save hidden"><i class="fa fa-save"></i></span>
							<span class="btn btn-warning cancel hidden"><i class="fa fa-times"></i></span>

						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	
	<script>
		$(document).ready(function(){
			$('.dates').datetimepicker({
			            pick12HourFormat: false,
			            pickTime:false
			});
			$('.edit').on('click',function(){

				$(this).parent().parent().find('input').removeClass('hidden');
				$(this).parent().parent().find('.save').removeClass('hidden');
				$(this).parent().parent().find('.cancel').removeClass('hidden');

				$(this).parent().parent().find('span.la-text').addClass('hidden');

				$(this).parent().parent().find('span.la-info').removeClass('hidden');

				$(this).addClass('hidden');
			});
			$('.cancel').on('click',function(){
				$(this).parent().parent().find('input').addClass('hidden');
				$(this).parent().parent().find('.save').addClass('hidden');
				$(this).parent().parent().find('.edit').removeClass('hidden');

				$(this).parent().parent().find('span.la-text').removeClass('hidden');
				$(this).parent().parent().find('span.la-info').addClass('hidden');

				$(this).addClass('hidden');
			});
			$('input').blur(function(){
				var elem = $(this).parent().parent();
				var actual_start = elem.find('input.actual-start').val(),
					actual_end   = elem.find('input.actual-end').val(),
					plan_start	 = elem.find('input.plan-start').val(),
					plan_end	 = elem.find('input.plan-end').val();

				elem.find('input.plan-end').data("DateTimePicker").setMinDate(new Date(plan_start));
				elem.find('input.actual-end').data("DateTimePicker").setMinDate(new Date(actual_start));
			});
			// check if the date is valid
			function isValidDate(date)
			{
			    var matches = /^(\d{2})[\/](\d{2})[\/](\d{4})$/.exec(date);
			    if (matches == null) return false;
			    var d = matches[2];
			    var m = matches[1] - 1;
			    var y = matches[3];
			    var composedDate = new Date(y, m, d);
			    return composedDate.getDate() == d &&
			            composedDate.getMonth() == m &&
			            composedDate.getFullYear() == y;
			}
			$('.save').on('click',function(){
				var elem = $(this).parent().parent();
				var actual_start = elem.find('input.actual-start').val(),
					actual_end   = elem.find('input.actual-end').val(),
					plan_start	 = elem.find('input.plan-start').val(),
					plan_end	 = elem.find('input.plan-end').val();
				var test = isValidDate(plan_start) || isValidDate(plan_end); 

				if(test){
					// HIDE THIS INPUT DATE
					elem.find('input').addClass('hidden');
					// HIDE LABEL
					$(this).parent().parent().find('span.la-text').removeClass('hidden');
					$(this).parent().parent().find('.edit').removeClass('hidden');
				$(this).parent().parent().find('span.la-info').addClass('hidden');

					// HIDE SAVE BUTTON
					$(this).addClass('hidden');

//					elem.find('input.plan-start').siblings().text(plan_start);
//					elem.find('input.plan-end').siblings().text(elem.find('input.plan-end').val());
//					elem.find('input.actual-start').siblings().text(elem.find('input.actual-start').val());
//					elem.find('input.actual-end').siblings().text(elem.find('input.actual-end').val());
					var data = {
						'plan_start'   : elem.find('input.plan-start').val(),
						'plan_end'     : elem.find('input.plan-end').val(),
						'actual_start' : elem.find('input.actual-start').val(),
						'actual_end'   : elem.find('input.actual-end').val(),
						'activity_code': elem.find('.activity_code').text(),
						'cycle_id'	   : '{{ $cycle_id }}',
						'program'      : '{{ $program }}',
						'psgc_id'	   : '{{ $psgc_id }}',
						'actual_start' : actual_start,
						'actual_end'   : actual_end,
						'reference_no' : '{{ $reference_no }}'
					}
//					$.post('', data ,function (res) {
//						alert('updated');
//					});

					$.post('', data ,function (res) {
                        if(res.result=='success'){
                        elem.find('input.plan-start').siblings().text(plan_start);
                        elem.find('input.plan-end').siblings().text(elem.find('input.plan-end').val());
                        elem.find('input.actual-start').siblings().text(elem.find('input.actual-start').val());
                        elem.find('input.actual-end').siblings().text(elem.find('input.actual-end').val());
                        elem.find('input.actual-end').data("DateTimePicker").setMinDate(new Date(actual_start));
                         alert('Successfully updated');
                        }else if(res.result=='error'){
                            alert(res.message);
                             elem.find('input.plan-start').val();
                              elem.find('input.plan-end').val();
                               elem.find('input.actual-start').val();
                              elem.find('input.actual-end').val()

                        }
                    });
				}else{
					alert('not a valid date please check your input based on this format mm/dd/yyyy ');
				}

			});

		});
	</script>
@stop
