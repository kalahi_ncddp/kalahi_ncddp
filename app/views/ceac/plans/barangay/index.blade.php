@extends('layouts.default')

@section('username')
	
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	<div class="row">
			
	        <div class="col-md-12">
	    		<h2> CEAC Barangay plans </h2>    
	    		<a class="btn  btn-primary" href="{{ URL::to('ceac/plans/barangay/create') }}">
				 Add New
				</a>
	        </div>
	    </div>
	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
				<!-- Advanced Tables -->
				
				
		    	<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Records
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>PSGC NO. </th>
										<th>Program Id</th>
										<th>Cycle Id</th>
										<th>Start Date</th>
										<th>End date</th>
										<th>Remarks</th>
										<td>Option</td>
									</tr>
								</thead>
								<tbody>
									@foreach ($barangay as  $barangay) 
										<tr>
											<td>{{ $barangay->psgc_id }}</td>
											<td>{{ $barangay->program_id }}</td>
											<td>{{ $barangay->cycle_id }}</td>
											<td>{{ $barangay->startdate }}</td>
											<td>{{ $barangay->enddate }}</td>
											<td>{{ $barangay->remarks }}</td>
											<td> 
												<a href="{{URL::to('/ceac/plans/barangay') }}/edit/{{ $barangay->psgc_id }}" class="btn btn-default">Edit </a>

												<a href="{{URL::to('/ceac/plans/barangay') }}/delete/{{ $barangay->psgc_id }}" class="btn btn-danger delete-plan">Delete </a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();

        $('.delete-plan').on('click',function(){
        	
        	var confirms = confirm('are you sure that you want to delete this');
        	if( confirms ){
				return true;        		
        	} else {
        		return false;
        	}
        });
      });
    </script>
@stop