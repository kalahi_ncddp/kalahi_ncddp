@extends('layouts.default')

@section('username')
	
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	<div class="row">
			
	        <div class="col-md-12">
	    		<h2> CEAC MONITORING with ACT PLAN </h2>    
	    		<a class="btn  btn-primary" href="{{ URL::to('ceac/plans/municipality/create') }}">
				 Add New
				</a>
	        </div>
	    </div>
	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
				<!-- Advanced Tables -->
				
				
		    	<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Records
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>PSGC NO. </th>
										<th>Program Id</th>
										<th>Cycle Id</th>
										<th>Start Date</th>
										<th>End date</th>
										<th>Remarks</th>
										<td>Option</td>
									</tr>
								</thead>
								<tbody>
									@foreach ($municipality as  $municipal) 
										<tr>
											<td>{{ $municipal->psgc_id }}</td>
											<td>{{ $municipal->program_id }}</td>
											<td>{{ $municipal->cycle_id }}</td>
											<td>{{ $municipal->startdate }}</td>
											<td>{{ $municipal->enddate }}</td>
											<td>{{ $municipal->remarks }}</td>
											<td> 
												<a href="{{URL::to('/ceac/plans/municipality') }}/edit/{{ $municipal->psgc_id }}" class="btn btn-default">Edit </a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop