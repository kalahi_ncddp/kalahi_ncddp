@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
			<div class="col-md-12">
				<h4>Edit {{ $ceac->psgc_id }}</h4>
			</div>
		</div>
	  
	    <div class="row">
		    <div class="col-md-12">
				
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
				{{ Form::open(array('url' => 'ceac/plans/municipality/update/'.$ceac->psgc_id)) }}

				<div class="col-md-6 left-border">

				
					<!-- barangay -->
					<div class="form-group">
						{{ Form::label('Municipality', 'Municipality') }}
						{{ Form::select('psgc_id',[ $data->municipality_psgc => $data->municipality], $data->municipality_psgc ,array('class' => 'form-control municipal','placeholder' => '','','readonly')) }}		
					</div>
					

				<div class="form-group">
							{{ Form::label('Province','Province') }}
						{{ Form::text('province',$data->province->province , array('class' => 'form-control province','placeholder' => '','','readonly')) }}
					</div>

					<div class="form-group">
						{{ Form::label('Region','Region') }}
						{{ Form::text('region',  $data->province->region->region_name, array('class' => 'form-control region','readonly')) }}
					</div>


			

					<div class="form-group">
						{{ Form::label('Cycle','Cycle') }}
						{{ Form::select('cycle_id', $cycles, $ceac->cycle_id, array('class' => 'form-control cycle', 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('Fund Source','Fund Source') }}
						{{ Form::text('program_id', $ceac->program_id, array('class' => 'form-control program','readonly')) }}
					</div>

				</div>


				<div class="col-md-6 left-border">

					<div class="form-group">
						{{ Form::label('Purpose','Purpose') }}
						{{ Form::select('activity_code', $purposes, $ceac->activity_code, array('class' => 'form-control cycle', 'required')) }}
					</div>
					<div class="form-group">
						{{ Form::label('Start Date','Start Date') }}
						<div class='input-group date' id='datetimepicker10'>
			                {{ Form::text('startdate', $ceac->startdate, array('class' => 'form-control idc ','required')) }}
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>

					<div class="form-group">
						{{ Form::label('End Date','End Date') }}
						<div class='input-group date'>
							{{ Form::text('enddate', $ceac->enddate, array('class' => 'form-control ','required')) }}
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>

					<div class="form-group">
						{{ Form::label('Remarks','Remarks') }}
						{{ Form::textarea('remarks', $ceac->remarks, array('class' => 'form-control ','')) }}
					</div>


						{{ Form::submit('update', array('class' => 'btn btn-primary')) }}
						<a href="{{ URL::to('ceac/plans/barangay/') }}" class="btn btn-default">close</a>
				</div>
				{{ Form::close() }}
			</div>
		</div>
		<!-- add js module -->
		
@stop