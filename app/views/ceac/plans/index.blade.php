@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	<div class="col-md-12 title">
		<h3>{{ $title }}</h3>
	</div>
		
	<div class="row">
		<div class="container">
			<table class="table table-bordered">
				<thead>
					<tr>
						<td>Region</td>
						<td>Municipal</td>
						<td>Barangay</td>
						<td>Fund Source</td>
						<td>Cycle</td>
						<td>Project</td>
						<td>Plans</td>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>	
@stop