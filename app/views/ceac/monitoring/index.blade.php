@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		
	<div class="row">
		<div class="container">
			
			<div class="clearfix"></div>
			<br>
			<div class="row">
			    <div class="box box-primary">
			        <div class="box-header">
			            <div class="box-title">Add Ceac</div>
			            <div class="box-tools pull-right">
                            <button class="btn btn-success" id="add-ceac">Add CEAC</button>
			            </div>
			        </div>
			        <div class="box-body">

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>Region</td>
                                    <td>Province</td>
                                    <td>Municipality</td>
                                    <td>Program</td>
                                    <td>Cycle</td>
                                    <td>Option</td>


                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($ceac as $ceacs)
                                    <tr>
                                        <td>{{ $ceacs->municipality->province->region->region_name }}</td>
                                        <td>{{ $ceacs->municipality->province->province}}</td>
                                        <td>{{ $ceacs->municipality->municipality }}</td>
                                        <td>{{ $ceacs->program_id}}</td>
                                        <td>{{ $ceacs->cycle_id }}</td>
                                        <td>
                                            <a href="{{ URL::to('ceac/monitoring/'.$ceacs->cycle_id.'/'.$ceacs->program_id.'/'.$ceacs->psgc_id) }}" class="btn monitor btn-success">View Monitorings {{ hasComment($ceacs->reference_no) }}</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
			        </div>
			    </div>
			</div>
		</div>
	</div>	

	{{--@include('modals.activity_ceac')--}}
    @include('modals.select_ceac');
	<script type="text/javascript">
	    @if($position->position != 'ACT')
	        alert('You dont have sufficient right to view this CEAC');

	        window.location.href="{{ URL::to('') }}";
	   @endif
	    var ceacController; ceacController=(function($){
			var ceac = {
				onProceed : function()
				{
				        var cycle_id = $('.cycle').val();
                        var program     = $('.program ').val();
                        var psgc_id  = $('.psgc_id').val();
                        $('.cycles').text(cycle_id);
                        $('.programs').text(program);
                        var arr = {
                            'cycle_id' : cycle_id,
                            'program_id' : program
                        };
                        $('.modal').loading(true);

                        $.post('{{ URL::to("ceac/monitoring/create") }}', arr ,function(res){
                                if(res.result){
                                    $("#select-ceac").find(".modal-body").append("<div class='alert alert-success'>Success</div>");
                                    $(".modal").loading(false);
                                    window.location.reload();
                                }else{
                                    $("#select-ceac").find(".modal-body .alert").remove();
                                     $("#select-ceac").find(".modal-body").append("<div class='alert alert-warning'>Duplicate entry</div>");
                                     $(".modal").loading(false);
                                }
                        });


				},
				addCceac : function()
				{
				    $('#select-ceac').modal('show');
				}

			}
			return ceac;
		})(jQuery);

		// events
//		$('.cycle').change( ceacController.onChangeInCycle );
//		$('.program ').change( ceacController.onChangeInprogram );
		$('.proceed').on('click',ceacController.onProceed );
		$('#add-ceac').on('click',ceacController.addCceac );
		// $('.view-activity').on( 'click' , ceacController.onViewActivity  );

	</script>

@stop