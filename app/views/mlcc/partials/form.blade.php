<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
							<div class="col-md-6">

			<table class="table table-bordered table-striped">
				<tr>
					<th width="30%">As of</th>
					<td>{{ Form::input('text', 'mlccmain[as_of_date]', isset($mlcc->as_of_date) ? date("m/d/Y", strtotime($mlcc->as_of_date)) : null, array('class' => 'form-control date','max' => date("m/d/Y"), 'required')) }}</td>
				</tr>
				<tr>
					<th>Province</th>
					<td>
						{{ Form::hidden('mlccmain[municipal_psgc]', $mlcc->municipal_psgc) }}
						{{ Report::get_province($mlcc->municipal_psgc) }}
					</td>
				</tr>
				<tr>
					<th>Municipality</th>
					<td>{{ Report::get_municipality($mlcc->municipal_psgc) }}</td>
				</tr>
				<tr>
					<th>No. of Barangays</th>
					<td>{{ Form::input('number', 'mlccmain[no_of_barangays]', $mlcc->no_of_barangays, ['class' => 'form-control', 'min' => 0,'readonly']) }}</td>
				</tr>
				<tr>
					<th>Program</th>
					<td>{{ Form::select('program_id',[''=>'Select Program']+$programs ,$mlcc->program_id ,['class'=>'form-control','required'])}}</td>
				</tr>

				<tr>
					<th>Cycle</th>
					<td>{{ Form::select('cycle_id',[''=>'Select Cycle']+$cycles ,$mlcc->cycle_id ,['class'=>'form-control','required'])}}</td>
				</tr>
				<tr>
					<th>Grant</th>
					<td>{{ Form::currency( 'kc_grant', $mlcc->kc_grant, ['class' => 'form-control prices', 'step' => 'any', 'min' => 0]) }}</td>
				</tr>
			</table>
		</div>
							<div class="col-md-6">
			<table class="table table-bordered table-striped">

				<tr>
					<th>Total Planned</th>
					<td class="total_planned">0</td>
				</tr>
				<tr>
					<th class="text-right">Cash</th>
					<td>{{ Form::currency('planned_cash', $mlcc->planned_cash, ['class' => 'form-control prices general_planned', 'step' => 'any', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th class="text-right">In-Kind</th>
					<td>{{ Form::currency( 'planned_inkind', $mlcc->planned_inkind, ['class' => ' prices form-control general_planned', 'step' => 'any', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>Total Actual</th>
					<td class="total_actual">0</td>
				</tr>
				<tr>
					<th class="text-right">Cash</th>
					<td>{{ Form::currency( 'actual_cash', $mlcc->actual_cash, ['class' => 'prices form-control general_actual', 'step' => 'any', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th class="text-right">In-Kind</th>
					<td>{{ Form::currency( 'actual_inkind', $mlcc->actual_inkind, ['class' => ' prices form-control general_actual', 'step' => 'any', 'min' => 0]) }}</td>
				</tr>
			</table>
		</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		Details
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped labor_generated">
				<thead>
					<tr>
						<th rowspan="2">Category</th>
						<th colspan="3">PLGU</th>
						<th colspan="3">MLGU</th>
						<th colspan="3">BLGU</th>
						<th colspan="3">Others</th>
						<th colspan="3">Total</th>
					</tr>
					<tr>
						<th>Planned</th>
						<th>Actual</th>
						<th>Balance</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Balance</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Balance</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Balance</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Balance</th>
					</tr>
				</thead>
				<tbody>
					@foreach($mlcc_det_list as $mlcc_det)
					<tr>
						<td>
							{{ $mlcc_det->category }}
							{{ Form::hidden('category[]', $mlcc_det->category) }}
						</td>
						<td>{{ Form::input('number', "plgu_planned[$mlcc_det->category]", $mlcc_det->plgu_planned, ['class' => 'form-control plgu', 'step' => 'any', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', "plgu_actual[$mlcc_det->category]", $mlcc_det->plgu_actual, ['class' => 'form-control plgu', 'step' => 'any', 'min' => 0]) }}</td>
						<td class="plgu_bal">0</td>
						<td>{{ Form::input('number', "mlgu_planned[$mlcc_det->category]", $mlcc_det->mlgu_planned, ['class' => 'form-control mlgu', 'step' => 'any', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', "mlgu_actual[$mlcc_det->category]", $mlcc_det->mlgu_actual, ['class' => 'form-control mlgu', 'step' => 'any', 'min' => 0]) }}</td>
						<td class="mlgu_bal">0</td>
						<td>{{ Form::input('number', "blgu_planned[$mlcc_det->category]", $mlcc_det->blgu_planned, ['class' => 'form-control blgu', 'step' => 'any', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', "blgu_actual[$mlcc_det->category]", $mlcc_det->blgu_actual, ['class' => 'form-control blgu', 'step' => 'any', 'min' => 0]) }}</td>
						<td class="blgu_bal">0</td>
						<td>{{ Form::input('number', "others_planned[$mlcc_det->category]", $mlcc_det->others_planned, ['class' => 'form-control others', 'step' => 'any', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', "others_actual[$mlcc_det->category]", $mlcc_det->others_actual, ['class' => 'form-control others', 'step' => 'any', 'min' => 0]) }}</td>
						<td class="others_bal">0</td>
						<td class="total_det_planned">0</td>
						<td class="total_det_actual">0</td>
						<td class="total_det_bal">0</td>
					</tr>
					@endforeach
					<tr>
						<th class="text-right">Total</th>
						<td class="plgu_planned_total">0</td>
						<td class="plgu_actual_total">0</td>
						<td class="plgu_balance_total">0</td>
						<td class="mlgu_planned_total">0</td>
						<td class="mlgu_actual_total">0</td>
						<td class="mlgu_balance_total">0</td>
						<td class="blgu_planned_total">0</td>
						<td class="blgu_actual_total">0</td>
						<td class="blgu_balance_total">0</td>
						<td class="others_planned_total">0</td>
						<td class="others_actual_total">0</td>
						<td class="others_balance_total">0</td>
						<td class="overall_planned_total">0</td>
						<td class="overall_actual_total">0</td>
						<td class="overall_balance_total">0</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="form-group pull-right">
                <label>Save as draft</label>
                <input type="checkbox" name="is_draft" {{ $mlcc->is_draft==1 ? 'checked': '' }}>
              </div>
<!-- {{ Form::submit('Save MLCC', array('class' => 'btn btn-primary')) }}
{{ HTML::linkRoute('mlcc.index', 'Close',
  null, array('class' => 'btn bg-navy btn')) }} -->
  <div class="col-md-12 ">
									<div class="form-group pull-right">
						                <button class="btn btn-primary" id="save">
											<i class="fa fa-save"></i>
											Submit 
										</button>
		                 				<!-- <a class="btn btn-default" href="{{ URL::to('mlcc') }}">Cancel</a>	 -->
		                 				{{ HTML::linkRoute('mlcc.index', 'Cancel',
  null, array('class' => 'btn btn-default')) }}		
									</div>
								</div>
								<div class="clearfix"></div>	
{{ Form::close() }}

<script>
	$(document).ready(function() {
		$(".general_planned").change(function() {
			calculate_general_total('planned');
		});
		$(".general_actual").change(function() {
			calculate_general_total('actual');
		});

		calculate_general_total('planned');
		calculate_general_total('actual');

		var sections = ["plgu", "mlgu", "blgu", "others"];
		for (var i = 0; i < sections.length; i++) {
			$("input." + sections[i]).change(function() {
				calculate_detailed_total(this, $(this).attr("class").replace("form-control", "").trim());
			});
		}
	});

	function calculate_general_total(typ) {
		var cash = parseFloat($("input[name='mlccmain[" + typ + "_cash]']").val() || 0);
		var inkind = parseFloat($("input[name='mlccmain[" + typ + "_inkind]']").val() || 0);

		$(".total_" + typ).html(cash + inkind);
	}

	function calculate_detailed_total(obj, typ) {
		var tr = $(obj).parent().parent();
		var planned = parseFloat($(tr).find("td input[name*='" + typ + "_planned']").val() || 0);
		var actual = parseFloat($(tr).find("td input[name*='" + typ + "_actual']").val() || 0);

		// update individual balance
		$(tr).find("td." + typ + "_bal").html(planned - actual);

		// update total right
		var planned_total = 0;
		$(tr).find("td input[name*='_planned']").each(function() {
			planned_total += parseFloat($(this).val() || 0);
		});
		var actual_total = 0;
		$(tr).find("td input[name*='_actual']").each(function() {
			actual_total += parseFloat($(this).val() || 0);
		});
		$(tr).find("td.total_det_planned").html(planned_total);
		$(tr).find("td.total_det_actual").html(actual_total);
		$(tr).find("td.total_det_bal").html(planned_total - actual_total);

		// update total down
		planned_total = 0;
		$("input[name*='" + typ + "_planned']").each(function() {
			planned_total += parseFloat($(this).val() || 0);
		});
		actual_total = 0;
		$("input[name*='" + typ + "_actual']").each(function() {
			actual_total += parseFloat($(this).val() || 0);
		});
		$("td." + typ + "_planned_total").html(planned_total);
		$("td." + typ + "_actual_total").html(actual_total);
		$("td." + typ + "_balance_total").html(planned_total - actual_total);

		// update overall total
		planned_total = 0;
		$("td.total_det_planned").each(function() {
			planned_total += parseFloat($(this).html() || 0);
		});
		actual_total = 0;
		$("td.total_det_actual").each(function() {
			actual_total += parseFloat($(this).html() || 0);
		});
		$("td.overall_planned_total").html(planned_total);
		$("td.overall_actual_total").html(actual_total);
		$("td.overall_balance_total").html(planned_total - actual_total);
	}
</script>