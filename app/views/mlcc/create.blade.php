@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')

	@if ($errors->all())
		<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
	@endif

	{{ Form::open(array('route' => array('mlcc.store'), 'method' => 'post', 'role' => 'form')) }}

	@include('mlcc.partials.form')

@stop