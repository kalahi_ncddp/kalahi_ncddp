@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <a class="btn btn-default" href="{{ URL::to('mlcc') }}">Go Back</a>
  	@if($mlcc->is_draft==1)
                <div class="alert alert-info">This is still a draft  <a href="{{ URL::to('mlcc/' .$mlcc->mlcc_id.'/edit') }}">edit the document</a> to set it in final draft</div>
           @endif
	    	@if( !is_review($mlcc->mlcc_id) )

           <div class="pull-right">
           {{ Form::open(array('route' => array('mlcc.destroy', $mlcc->mlcc_id), 'method' => 'delete')) }}
                 {{ HTML::linkRoute('mlcc.edit', 'Edit',
                   array($mlcc->mlcc_id), array('class' => 'btn btn-info btn')) }}
                 <button type="submit" class="btn btn-danger btn-mini"
                   onclick="return confirm('Are you sure you want to delete this record?')">Delete</button>
              @endif
               {{ Form::close() }}
           </div>

  </div>
</div>

<hr />

<div class="panel panel-default">
  <div class="panel-heading">
    General Information
  </div>
  <div class="panel-body">
    <div class="table-responsive">
							<div class="col-md-6">

      <table class="table table-bordered table-striped">
        <tr>
					<th width="30%">As of</th>
					<td>{{ toDate($mlcc->as_of_date) }}</td>
				</tr>
				<tr>
					<th>Province</th>
					<td>{{ Report::get_province($mlcc->municipal_psgc) }}</td>
				</tr>
				<tr>
					<th>Municipality</th>
					<td>{{ Report::get_municipality($mlcc->municipal_psgc) }}</td>
				</tr>
				<tr>
					<th>No. of Barangays</th>
					<td>{{ $mlcc->no_of_barangays }}</td>
				</tr>
				<tr>
					<th>Program</th>
					<td>{{ $mlcc->program_id }}</td>
				</tr>
				<tr>
					<th>Cycle</th>
					<td>{{ $mlcc->cycle_id }}</td>
				</tr>
			

				<tr>
					<th>Grant</th>
					<td>{{ $mlcc->kc_grant }}</td>
				</tr>
				</table>
		</div>
							<div class="col-md-6">
      <table class="table table-bordered table-striped">
				<tr>
					<th>Total Planned</th>
					<td class="total_planned">{{ $mlcc->planned_cash + $mlcc->planned_inkind }}</td>
				</tr>
				<tr>
					<th class="text-right">Cash</th>
					<td>{{ $mlcc->planned_cash }}</td>
				</tr>
				<tr>
					<th class="text-right">In-Kind</th>
					<td>{{ $mlcc->planned_inkind }}</td>
				</tr>
				<tr>
					<th>Total Actual</th>
					<td class="total_actual">{{ $mlcc->actual_cash + $mlcc->actual_inkind }}</td>
				</tr>
				<tr>
					<th class="text-right">Cash</th>
					<td>{{ $mlcc->actual_cash }}</td>
				</tr>
				<tr>
					<th class="text-right">In-Kind</th>
					<td>{{ $mlcc->actual_inkind }}</td>
				</tr>
      </table>
  </div>
    </div>
  </div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		Details
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped labor_generated">
				<thead>
					<tr>
						<th rowspan="2">Category</th>
						<th colspan="3">PLGU</th>
						<th colspan="3">MLGU</th>
						<th colspan="3">BLGU</th>
						<th colspan="3">Others</th>
						<th colspan="3">Total</th>
					</tr>
					<tr>
						<th>Planned</th>
						<th>Actual</th>
						<th>Balance</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Balance</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Balance</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Balance</th>
						<th>Planned</th>
						<th>Actual</th>
						<th>Balance</th>
					</tr>
				</thead>
				<tbody>
					@foreach($mlcc_det_list as $mlcc_det)
					<tr>
						<td>{{ $mlcc_det->category }}</td>
						<td class="plgu planned">{{ $mlcc_det->plgu_planned }}</td>
						<td class="plgu actual">{{ $mlcc_det->plgu_actual }}</td>
						<td class="plgu balance">{{ $mlcc_det->plgu_planned - $mlcc_det->plgu_actual }}</td>
						<td class="mlgu planned">{{ $mlcc_det->mlgu_planned }}</td>
						<td class="mlgu actual">{{ $mlcc_det->mlgu_actual }}</td>
						<td class="mlgu balance">{{ $mlcc_det->mlgu_planned - $mlcc_det->mlgu_actual }}</td>
						<td class="blgu planned">{{ $mlcc_det->blgu_planned }}</td>
						<td class="blgu actual">{{ $mlcc_det->blgu_actual }}</td>
						<td class="blgu balance">{{ $mlcc_det->blgu_planned - $mlcc_det->blgu_actual }}</td>
						<td class="others planned">{{ $mlcc_det->others_planned }}</td>
						<td class="others actual">{{ $mlcc_det->others_actual }}</td>
						<td class="others balance">{{ $mlcc_det->others_planned - $mlcc_det->others_actual }}</td>
						<td class="total_det_planned">0</td>
						<td class="total_det_actual">0</td>
						<td class="total_det_bal">0</td>
					</tr>
					@endforeach
					<tr>
						<th class="text-right">Total</th>
						<td class="plgu_planned_total"></td>
						<td class="plgu_actual_total">0</td>
						<td class="plgu_balance_total">0</td>
						<td class="mlgu_planned_total">0</td>
						<td class="mlgu_actual_total">0</td>
						<td class="mlgu_balance_total">0</td>
						<td class="blgu_planned_total">0</td>
						<td class="blgu_actual_total">0</td>
						<td class="blgu_balance_total">0</td>
						<td class="others_planned_total">0</td>
						<td class="others_actual_total">0</td>
						<td class="others_balance_total">0</td>
						<td class="overall_planned_total">0</td>
						<td class="overall_actual_total">0</td>
						<td class="overall_balance_total">0</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		calculate_detailed_total();
	});

	function calculate_detailed_total() {
		$("table.labor_generated tr").each(function() {
			var tr = $(this);

			// update total right
			var planned_total = 0;
			$(tr).find("td.planned").each(function() {
				planned_total += parseFloat($(this).html() || 0);
			});
			var actual_total = 0;
			$(tr).find("td.actual").each(function() {
				actual_total += parseFloat($(this).html() || 0);
			});
			$(tr).find("td.total_det_planned").html(planned_total);
			$(tr).find("td.total_det_actual").html(actual_total);
			$(tr).find("td.total_det_bal").html(planned_total - actual_total);
		});

		// update total down
		var types = ["plgu", "mlgu", "blgu", "others"];
		for (var i = 0; i < types.length; i++) {
			console.log("here");
			var typ = types[i];

			planned_total = 0;
			$("td[class='" + typ + " planned']").each(function() {
				planned_total += parseFloat($(this).html() || 0);
			});
			actual_total = 0;
			$("td[class='" + typ + " actual']").each(function() {
				actual_total += parseFloat($(this).html() || 0);
			});
			console.log(planned_total);
			$("td." + typ + "_planned_total").html(planned_total);
			$("td." + typ + "_actual_total").html(actual_total);
			$("td." + typ + "_balance_total").html(planned_total - actual_total);
		}

		// update overall total
		planned_total = 0;
		$("td.total_det_planned").each(function() {
			planned_total += parseFloat($(this).html() || 0);
		});
		actual_total = 0;
		$("td.total_det_actual").each(function() {
			actual_total += parseFloat($(this).html() || 0);
		});
		$("td.overall_planned_total").html(planned_total);
		$("td.overall_actual_total").html(actual_total);
		$("td.overall_balance_total").html(planned_total - actual_total);
	}
</script>
@stop