@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')	
	<div class="panel panel-default">
		<div class="panel-heading">
			  {{ $title }}
			@if($position->position=='ACT')
                    <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                    <span class="hidden module">mlcc</span>
             @endif
			{{ HTML::linkRoute('mlcc.create', 'New MLCC', array(), array('class' => 'btn btn-primary pull-right btn')) }}
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover data-table">
					<thead>

						<tr>
							<th>
                           @if($position->position=='ACT')
                            <input type="checkbox" id="selecctall"/>
                            @endif
							<th width="30%">As of Date</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($mlcc_list as $mlcc)
						<tr>
							<td>
                               @if($position->position=='ACT')
                                  @if($mlcc->is_draft==0)
                                    {{ $reference->approval_status($mlcc->mlcc_id, 'ACT') }}
                                  @else
                                      <span class="label label-warning">draft</span>
                                  @endif
                                @endif
                            </td>
							<td>{{ toDate($mlcc->as_of_date) }}</td>
							<td>
								{{ HTML::linkRoute('mlcc.show', 'View Details',
									array($mlcc->mlcc_id), array('class' => 'btn btn-success btn')) }}
	            </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<div class="panel-footer text-right">
			
		</div>
	</div>

	<script>
    $(document).ready(function () {
      $('.data-table').dataTable();
    });
  </script>
@stop