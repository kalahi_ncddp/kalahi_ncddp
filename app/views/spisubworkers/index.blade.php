@extends('layouts.default')

@section('content')
    <div class="row">
	    <div class="col-md-12">
			<!-- Advanced Tables -->
			
	    	<div class="panel panel-default">
	    		<div class="panel-heading">
	    			<h4>List of Sub-Projects</h4>

			        <!--
					<a href="http://localhost/kalahi-spi/mlprap/create" class="btn btn-success pull-right">Add New</a>
					//-->
			        <div class="clearfix"></div>
	    		</div>

				<div class="panel-body">
					<div class="table-responsive">

						<div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
							

							<table class="table table-striped table-bordered table-hover dataTable" 
							
									id="dataTables-example" aria-describedby="dataTables-example_info">
							<thead>

								<tr>

									<th style="width: 30%;">Title</th>
									<th style="width: 20%;">Lead Barangay</th>
									<th style="width: 25%;">Program</th>
									<th style="width: 10%;">Cycle</th>
									<th style="width: 5%;">ERS</th>
									<th style="width: 10%; text-align: center;">Action</th>
								</tr>
							</thead>
							
							<tbody>
								@foreach ($sprojectprofiles as $sprojectprofile ) 
									<tr>

										<td>
											{{ $sprojectprofile->project_name }}
										</td>
										<td>

											{{ getBarangay($sprojectprofile->barangay_id()) }}

										</td>
										<td>
											{{ $sprojectprofile->program_id }}
										</td>
										<td>
											{{ $sprojectprofile->cycle_id }}
										</td>
										<td>
											{{ $sprojectprofile->ers->count() }}
										</td>
										<td  style="text-align: center;">
											<!--
											<a href="spi-subworkers/{{ $sprojectprofile->project_id }}" class="btn btn-sm btn-info"
												style="width: 48%;">
												Workers <i class="glyphicon glyphicon-user"></i>
											</a>
											//-->
											<a href="spi-erslist/{{ $sprojectprofile->project_id }}" class="btn btn-success btn">
												ERS <i class="glyphicon glyphicon-tasks"></i>
											</a>
										</td>
									</tr>
								@endforeach

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="custom_confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
	            </div>
	            <div class="modal-body">
	           	  
				     </div>
	           <div class="modal-footer">
	             <button class="btn btn-warning confirm">Confirm</button>
	             <button class="btn  closes">Close</button>
	           </div>
	        </div>
	    </div>
	</div>
	<script>
      	$(document).ready(function () {
        	$('#dataTables-example').dataTable();
      	});
    </script>
@stop