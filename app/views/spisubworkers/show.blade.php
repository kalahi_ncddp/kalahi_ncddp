@extends('layouts.default')

@section('content')
<div class="row">
    <div class="col-md-12">
    	<div class="panel panel-default">
    		<div class="panel-heading">
    			<h4 class="col-xs-8 col-md-8" style="padding-left: 0;">{{$spi_profile_name}} <small>Workers</small></h4>
					<a id="spi-addworker_modal_show" class="btn btn-success pull-right" style="margin-left: 5px;" data-value="{{ URL::to('spi-worker/generateid')}}">
						<i class="glyphicon glyphicon-plus"></i> Add New Worker
					</a>
					<a id="spi-addexworker_modal_show" class="btn btn-success pull-right">
						<i class="glyphicon glyphicon-import"></i> Add Existing Workers
					</a>
		        <div class="clearfix"></div>
    		</div>

			<div class="panel-body">
				<div class="table-responsive">
					<div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
						@if ($create_status !== null)
							@if ($create_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> New Worker successfully added!</strong>
								</div>
							@elseif ($create_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Worker can not be added</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if ($destroy_status !== null)
							@if ($destroy_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> Worker successfully removed!</strong>
								</div>
							@elseif ($destroy_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Worker can not be removed</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						<table class="table table-striped table-bordered table-hover dataTable" id="dataTables-example" aria-describedby="dataTables-example_info">
							<thead>
								<tr>
									<th style="width: 20%;">Name</th>
									<th style="width: 10%;">Worker ID</th>
									<th style="width: 5%;">Gender</th>
									<th style="width: 5%;">Age</th>
									<th style="width: 10%;">Birthday</th>
									<th style="width: 10%;">Work Nature</th>
									<th style="width: 10%;">Hourly Rate</th>
									<th style="width: 10%;">Daily Rate</th>
									<th style="width: 5%;">4Ps</th>
									<th style="width: 5%;">Seac</th>
									<th style="width: 10%;">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($workers as $worker)
								<tr data-rowid="{{$worker->worker_id}}">
									<td>
										{{$worker->firstname . ' ' . $worker->middlename . ' ' . $worker->lastname}}
									</td>
									<td>
										{{$worker->worker_id}}
									</td>
									<td>
										{{($worker->sex == 0) ? 'Male' : 'Female'}}
									</td>
									<td>
										{{$worker->age}}
									</td>
									<td>
										{{date("m/d/Y", strtotime($worker->birthday))}}
									</td>
									<td>
										{{$worker->nature_work}}
									</td>
									<td>
										<?php
											setlocale(LC_MONETARY, "es_ES");
											$locale = localeconv();
											echo $locale['currency_symbol'], number_format($worker->rate_hour, 2, $locale['decimal_point'], $locale['thousands_sep']);
										?>
									</td>
									<td>
										<?php
											setlocale(LC_MONETARY, "es_ES");
											$locale = localeconv();
											echo $locale['currency_symbol'], number_format($worker->rate_day, 2, $locale['decimal_point'], $locale['thousands_sep']);
										?>
									</td>
									<td>
										{{($worker->is_pantawid == 1) ? 'Yes' : 'No'}}
									</td>
									<td>
										{{($worker->is_seac == 1) ? 'Yes' : 'No'}}
									</td>
									<td style="text-align: center;">
										<!--
										<a href="" class="btn btn-sm btn-warning spi-viewworker_modal_show">
												<i class="glyphicon glyphicon-eye-open"></i>
										</a>
										//-->
										<a href="" class="btn btn-sm btn-danger spi-subworker_modal-del_show">
											<i class="glyphicon glyphicon-trash"></i>
										</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

<div class="modal fade" id="spi-addworker_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">New Worker for this Project</h4>
			</div>
			<div class="modal-body">
				<form id="spi-addworker_modal_formx">
					<div class="form-group col-xs-12 col-md-12">
						<label for="in-workerid">Worker ID</label>
		                <input class="form-control" required="required" type="text" 
		                	   id="in-workerid" name="in-workerid" value="" disabled="disabled"> 
			                <!-- 
						<div class='input-group' id='in-workerid_cont'>
			                <input class="form-control" required="required" type="text" 
			                	   id="in-workerid" name="in-workerid" value="" disabled="disabled"> 
			                <br>
			                <span class="input-group-btn">
			                	<button id="generate_id" class="btn btn-default" style="cursor: pointer;" data-value="{{ URL::to('spi-worker/generateid')}}">Generate</button>
			                </span>
			            </div>
			                //-->
					</div>  

					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-firstname">First Name</label>
						<input type="text" class="form-control" id="in-firstname">
					</div>  
					
					<div class="form-group  col-xs-4 col-sm-4">
						<label for="in-lastname">Last Name</label>
						<input type="text" class="form-control" id="in-lastname">
					</div>  

					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-middlename">Middle Name</label>
						<input type="text" class="form-control" id="in-middlename">
					</div>
					
					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-sex">Sex</label>
						<select id="in-sex" class="form-control">
						  <option value="">	</option>
						  <option value="0">Male</option>
						  <option value="1">Female</option>
						</select>
					</div>  
					
					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-age">Age</label>
						<input type="number" class="form-control" id="in-age">
					</div>  
					
					<div class="form-group col-xs-4 col-md-4">
						<label for="in-datebday_cont">Birthday</label>
						<div class='input-group date' id='in-datebday_cont'>
			                <input class="form-control" required="required" type="text" 
			                	   id="in-birthday" name="in-birthday" value=""> <br>
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>  

					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-natureofwork">Nature of Work</label>
						<select id="in-natureofwork" class="form-control">
						  <option value="">	</option>
						  <option value="Foreman/Project Supervisor">Foreman/Project Supervisor</option>
						  <option value="Carpenter">Carpenter</option>
						  <option value="Mason">Mason</option>
						  <option value="Plumber">Plumber</option>
						  <option value="Electrician">Electrician</option>
						  <option value="Laborer/Helper">Laborer/Helper</option>
						  <option value="Other Skilled">Other Skilled</option>
						  <option value="Other Unskilled">Other Unskilled</option>
						</select>
					</div>  

					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-hourrate">Hourly Rate</label>
						<input type="number" class="form-control" id="in-hourrate">
					</div>  

					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-dailyrate">Daily Rate</label>
						<input type="number" class="form-control" id="in-dailyrate">
					</div>  

				    <div class="form-group">
				    	<div class="col-xs-12 col-md-12">
				      		<div class="checkbox">
				        		<label><input type="checkbox" id="in-pantawid" name="in-pantawid">
				        			Pantawid Pamilyang Pilipino Program
				        		</label>
				      		</div>
				    	</div>
				  	</div>
				  	 <div class="form-group">
                        <div class="col-xs-12 col-md-12">
                            <div class="checkbox">
                                <label><input type="checkbox" id="in-slp" name="in-slp">
                                    SLP
                                </label>
                            </div>
                        </div>
                    </div>
<!--
					<div class="form-group">
				    	<div class="col-xs-6 col-md-6">
				      		<div class="checkbox">
				        		<label><input type="checkbox" id="in-seac" name="in-seac">
				        			Self Employment Assistance - Kaunlaran (SEA-K)
				        		</label>
				      		</div>
				    	</div>
				  	</div>
//-->
					<div class="clearfix"></div>
				</form>
			</div>
		   <div class="modal-footer">
		   		<input type="hidden" class="form-control" id="in-projectid" value="{{$project_id}}">
				<button id="spi-addworker_modal_form_submit" class="btn btn-success confirm">Submit</button>
				<button class="btn closes" data-dismiss="modal">Close</button>
		   </div>
		</div>
	</div>
</div>


<div class="modal fade" id="spi-addexworker_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Add Existing Workers to Project</small></h4>
			</div>
			<div class="modal-body">
				<table class="table table-striped table-bordered table-hover dataTable" id="dataTables-example2" aria-describedby="dataTables-example_info">
					<thead>
						<tr>
							<th style="width: 40%;">Name</th>
							<th style="width: 10%;">Worker ID</th>
							<th style="width: 5%;">Gender</th>
							<th style="width: 5%;">Age</th>
							<th style="width: 10%;">Work</th>
							<th style="width: 10%;">Hourly Rate</th>
							<th style="width: 10%;">Daily Rate</th>
							<th style="width: 10%; text-align: center;">Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($all_workers as $worker)
						<tr data-rowid="{{$worker->worker_id}}">
							<td>
								{{$worker->firstname . ' ' . $worker->middlename . ' ' . $worker->lastname}}
							</td>
							<td>
								{{$worker->worker_id}}
							</td>
							<td>
								{{($worker->sex == 1) ? 'Male' : 'Female'}}
							</td>
							<td>
								{{$worker->age}}
							</td>
							<td>
								{{$worker->nature_work}}
							</td>
							<td>
								<?php
									setlocale(LC_MONETARY, "es_ES");
									$locale = localeconv();
									echo $locale['currency_symbol'], number_format($worker->rate_hour, 2, $locale['decimal_point'], $locale['thousands_sep']);
								?>
							</td>
							<td>
								<?php
									setlocale(LC_MONETARY, "es_ES");
									$locale = localeconv();
									echo $locale['currency_symbol'], number_format($worker->rate_day, 2, $locale['decimal_point'], $locale['thousands_sep']);
								?>
							</td>
							<td style="text-align: center;">
								<?php
								 if($project_id !== $worker->project_id)
								 {
								?>
								<button type="submit" class="btn btn-sm btn-success spi-addexworker_submit"
									value="{{$worker->worker_id}}">
									<i class="glyphicon glyphicon-import"></i> Add to Project
								</button>
								<?php
								}
								else
								{
								?>
								<button class="btn btn-sm btn-success spi-ersworker_modal-del_show" disabled="disabled">
									Already exists
								</button>
								<?php
								}
								?>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="spi-subworker_modal-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
				<form>
					<input type="hidden" id="del-id" name="del-id" value="">
					<p class="col-xs-2" style="text-align: right;">
						<i class="glyphicon glyphicon-question-sign" style="color: #f4543c; font-size: 30px;"></i>
					</p>
					<p class="col-xs-10" style="font-size: 15px; padding-top: 5px; padding-bottom: 5px;">Are you sure you want to remove 
						 this worker?</p>
					<div class="clearfix"></div>
				</form>
			</div>
			<div class="modal-footer">
				<a id="spi-subworker_modal_delform_submit" href="" class="btn btn-danger">Yes</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">No</a>
			</div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
	    $('#dataTables-example').dataTable();
	    $('#dataTables-example2').dataTable();

    	setTimeout(function(){
    		$('div.alert').slideUp();
    	}, 5000);

		$('#in-age').keydown(function(event) {
				if (event.keyCode == 8 || event.keyCode == 9) {
				}
				else {
					if (event.keyCode < 48 || event.keyCode > 57 ) {
						event.preventDefault();	
					}	
				}
			});

		$('#in-hourrate, #in-dailyrate').keydown(function(event) {
				if ( event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
				}
				else {
					if (event.keyCode < 48 || event.keyCode > 57 ) {
						event.preventDefault();	
					}	
				}
			});
	    /*
	     *  ADDING a worker
	     */
	    $('#generate_id').on('click', function(event)
	  	{
	  		var $url = $(event.currentTarget).data('value');
	  		$.ajax({
	  			type: "GET",
		  		url:  $url,
		  	})
		  	.done(function( msg ) {
	    		$('#in-workerid').val(msg);
	  		});
	  		return false;
	  	});

	  	$('#spi-addworker_modal_show').on('click', function(event)
	  	{
	  		var $url = $(event.currentTarget).data('value');
	  		$.ajax({
	  			type: "GET",
		  		url:  $url
		  	})
		  	.done(function( msg ) {
	    		$('#in-workerid').val(msg);
	    		$('#spi-addworker_modal').modal('show');
	  		});

			return false;
	  	});

	  	$('#spi-addworker_modal_form_submit').on('click', function()
	  	{
			var $url = window.location.href; 
			$.ajax({
	  			type: "PUT",
		  		url:  $url,
		  		data: {
	  				'in-projectid'    : $('#in-projectid').val(),
	  				'in-workerid'     : $('#in-workerid').val(),
	  				'in-firstname'    : $('#in-firstname').val(),
		  			'in-lastname'     : $('#in-lastname').val(),
		  			'in-middlename'   : $('#in-middlename').val(),
		  			'in-sex'          : $('#in-sex').val(),
	   				'in-age'          : $('#in-age').val(),
	   				'in-birthday'     : $('#in-birthday').val(),
	   				'in-natureofwork' : $('#in-natureofwork').val(),
	   				'in-hourrate'     : $('#in-hourrate').val(),
	   				'in-dailyrate'    : $('#in-dailyrate').val(),
		   			'in-pantawid'     : $('#in-pantawid').val(),
		   			'in-seac'         : $('#in-seac').val(),
		   			'in-slp'          : $('#in-slp').val()
		  		}
			})
	  		.done(function( msg ) {
	    		$('#spi-addworker_modal').modal('hide');
	    		console.log(msg)
	    		// window.location = window.location.href;
	  		});

			return false;
	  	});

	  	$('#spi-addexworker_modal_show').on('click', function()
	  	{
			$('#spi-addexworker_modal').modal('show');
			return false;
	  	});

	  	$('.spi-addexworker_submit').on('click', function(event)
	  	{
			var $url = window.location.href; 
			$.ajax({
	  			type: "PUT",
		  		url:  $url,
		  		data: {
	  				'in-projectid'    : $('#in-projectid').val(),
	  				'in-workerid'     : $(event.currentTarget).val(),
	  				'in_existing'     : true
		  		}
			})
	  		.done(function( msg ) {
				$('#spi-addexworker_modal').modal('hide');
	    		window.location = window.location.href;
	  		});

			return false;
	  	});
	    /*
	     *  DELETING a worker
	     */
		$('.spi-subworker_modal-del_show').on('click', function(event){
			var id = $(event.currentTarget).parents('tr').data('rowid');
			$('#del-id').val(id);
			$('#spi-subworker_modal-del').modal('show');
			return false;
	  	});

		$('#spi-subworker_modal_delform_submit').on('click', function(event)
		{
			var $url = window.location.href;

			$.ajax({
		  		type: "DELETE",
		  		url: $url,
				data: {
	  				'in-workerid'    : $('#del-id').val(),
				}
			})
		  	.done(function( msg ) {
		    	$('#spi-subworker_modal-del').modal('hide');
		    	window.location = window.location.href;
		 	});

			return false;
		});
		/* end of DELETING a committee */
	});
</script>
@stop