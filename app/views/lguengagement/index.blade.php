@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif

	<div class="panel panel-default">
		<div class="panel-heading">
			{{ $title }}
			@if($position->position=='ACT')
                <span class="btn btn-default pull-right" id="approved" style="margin:0 10px">Review</span>
                                <span class="hidden module">lguengagement</span>
            @endif
			<a class="btn  btn-primary pull-right" href="{{ URL::to('lguengagement/create') }}"><i class="fa fa-plus"></i> Add New</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
						    @if($position->position=='ACT')
                            <th>
                            <input type="checkbox" id="selecctall"/>
                            Reviewed</th>
                            @endif
							<th>Region</th>
							<th>Province</th>
							<th>Municipality</th>
							<th>Program</th>	
							<th>Cycle</th>
							<th>Municipal and LGU MOA Signed</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $lgu_engagement)
						<tr>
                            @if($position->position=='ACT')
						    <td>
                               @if($lgu_engagement->is_draft==0)
                                 {{ $reference->approval_status($lgu_engagement->engagement_id , 'ACT') }}

                               @else
                                   <span class="label label-warning">draft</span>
                               @endif
                            </td>
                             @endif

							<td>
								{{ $region = Report::get_region($lgu_engagement->municipal_psgc) }}
							</td>
							<td>
								{{ $province = Report::get_province($lgu_engagement->municipal_psgc) }}
							</td>
							<td>
								{{ $municipality = Report::get_municipality($lgu_engagement->municipal_psgc) }}
							</td>
							<td>
								{{ $lgu_engagement->program_id }}
							</td>
							<td>
								{{ $lgu_engagement->cycle_id }}
							</td>
							<td>
								{{ toDate($lgu_engagement->moa_signed_date) }}
							</td>

							<td>
								@if($lgu_engagement->is_draft==0)
		                            <a class="btn btn-success btn" href="{{ URL::to('lguengagement/' .$lgu_engagement->engagement_id) }}">
		                         <i class="fa fa-eye"></i>     View Details  {{ hasComment($lgu_engagement->engagement_id) }}
		                            </a>
		                        @else
		                        	<a class="btn btn-warning btn" href="{{ URL::to('lguengagement/' .$lgu_engagement->engagement_id) }}">
		                          <i class="fa fa-eye"></i>    View Details ( draft )
		                            </a>
								@endif
	                    
	                        </td>
							
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop