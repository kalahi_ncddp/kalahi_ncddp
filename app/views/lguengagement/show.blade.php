@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
    <div class="col-md-12">
   @if($lguengagement->is_draft==1)

            	        	<div class="alert alert-info">This is still a draft  <a href="{{ URL::to('lguengagement/' .$lguengagement->engagement_id.'/edit') }}">edit the document</a> to set it in final draft</div>
            	         @endif
            	         		<a class="btn bg-navy" href="{{ URL::to('lguengagement') }}">Close</a>

        <div class="pull-right">
    	    {{ Form::open(array('url' => 'lguengagement/' . $lguengagement->engagement_id)) }}
	    	@if( !is_review($lguengagement->engagement_id) )

             <a class="btn  btn-small btn-info" href="{{ URL::to('lguengagement/' .$lguengagement->engagement_id.'/edit') }}">
                <i class="fa fa-edit"></i>Edit
             </a>
    		@endif
                				{{ Form::hidden('_method', 'DELETE') }}
                				<button class="btn btn-danger" type="submit" value="Delete">
                					<i class="fa fa-trash-o"></i> DELETE
                				</button>

            {{ Form::close() }}

        </div>

		 {{ Form::close() }}
	</div>

	 	<!--LGU Engagement-->
	 	<div class="col-md-12">
	 			     {{ viewComment($lguengagement->engagement_id) }}
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Basic Information</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	            <div class="box-body">

					<div class="table-responsive">
					<div class="col-md-6">
						<table class="table table-striped  table-hover">
				            <tr>
				                <th>Region</th>
				                <td>{{ $region = Report::get_region($lguengagement->municipal_psgc) }}</td>
				            </tr>
				            <tr>
				                <th>Province</th>
				                <td>{{ $province = Report::get_province($lguengagement->municipal_psgc) }}</td>
				            </tr>
						 	<tr>
				                <th>Municipality</th>
				                <td>{{ $municipality = Report::get_municipality($lguengagement->municipal_psgc) }}</td>
				            </tr>
				        </table>
				    </div>
				    <div class="col-md-6">
						<table class="table table-striped  table-hover">
				            <tr>
				                <th>Program</th>
				                <td>{{ $lguengagement->program_id }}</td>
				            </tr>
				             <tr>
				                <th>Cycle</th>
				                <td>{{ $lguengagement->cycle_id }}</td>
				            </tr>
				            <tr>
			                	<th>Municipal and LGU MOA Signed</th>
			                	<td>
			                		{{ toDate($lguengagement->moa_signed_date) }}
			                	</td>
			            	</tr>

						</table>
					</div>
					</div>
	            <div class="clearfix"></div>
	            </div>
	        </div>
	 	</div>

	 	<!--PTA Integration Checklist -->
    	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Accountability</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					<table class="table table-striped table-bordered table-hover">
						<tr>
							<th colspan="3" style="text-align:center"> Legislation/Resolution/Ordinances passed in support of CDD and Participatory Governance </th>
						</tr>
						<tr>
							<th></th>
			                <th>Resolution Number</th>
			                <th>Date of Approval</th>
						</tr>
						<tr>
			                <td>Institutionalization of People’s PTA in LGU development, planning and poverty reduction programs, plans and activities</td>
			                <td>
			                	{{ $lguengagement->pta_resolution_no }}
			                </td>
			                <td>
			                	{{ toDate($lguengagement->pta_approval_date) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Convergence of NGA property reduction PPAs in the municipality</td>
			                <td>
			                	{{ $lguengagement->nga_resolution_no }}
			                </td>
			                <td>
			                	{{ toDate($lguengagement->nga_approval_date) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Formation of MIAC</td>
			                <td>
			                	{{ $lguengagement->miac_resolution_no }}
			                </td>
			                <td>
			                	{{ toDate($lguengagement->miac_approval_date) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Formation of MCT</td>
			                <td>
			                	{{$lguengagement->mct_resolution_no }}
			                </td>
			                <td>
			                	{{  toDate($lguengagement->mct_approval_date) }}
			                </td>
			            </tr>
			            <tr>
			                <td>NGO and PO Accreditation Guidelines</td>
			                <td>
			                	{{ $lguengagement->ngopo_resolution_no }}
			                </td>
			                <td>
			                	{{ toDate($lguengagement->ngopo_approval_date) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Support to BDP Formulation and BDP-MDP implementation including GAD plans</td>
			                <td>
			                	{{ $lguengagement->gad_resolution_no }}
			                </td>
			                <td>
			                	{{ toDate($lguengagement->gad_approval_date) }}
			                </td>
			            </tr>
			            <tr>
			            	<td>Municipal resolution expanding MDC membership to include CDD volunteers/representative</td>
			            	<td>
			            		{{ $lguengagement->mdcmem_resolution_no }}
			            	</td>
			            	 <td>
			                	{{toDate($lguengagement->mdcmem_approval_date) }}
			                </td>
			            </tr>
			            <tr>
			            	<td></td>
			            	<td colspan='2' >No. of CDD Volunteers as members of MDC</td>
			            </tr>
			            <tr>
			            	<td></td>
			                <td>

				                <table class="table">
				                	<tr>
				                		<td>Male</td>
				                		<td>{{ $lguengagement->mdcmem_male_no }}</td>
				                	</tr>
				                </table>
			                </td>
			                <td>
			                	<table class="table">
				                	<tr>
				                		<td>Female</td>
				                		<td>{{ $lguengagement->mdcmem_female_no }}</td>

				                	</tr>
				                </table>
			                </td>

			            </tr>
					</table>

					<br>

					<table class="table table-striped table-bordered table-hover">
						<tr>
							<th colspan="2" style="text-align:center"> Support to KC Implementation </th>
						</tr>
						<tr>
			                <td>Allocation of LCC (SB Resolution number)</td>
			                <td>
			                	{{ $lguengagement->lcc_resolution_no }}
			                </td>
			            </tr>
			            <tr>
			                <td>Allocation of LCC (Approval Date)</td>
			                <td>
			                	{{ $lguengagement->lcc_approval_date }}
			                </td>
			            </tr>
			            <tr>
			                <td>Opening of Municipal Trust Fund (Account No.)</td>
			                <td>
			                	{{ $lguengagement->trust_account_no }}
			                </td>
			            </tr>
			            <tr>
			                <td>Opening of Municipal Trust Fund (Date Opened)</td>
			                <td>
			                	{{ $lguengagement->trust_opened_date }}
			                </td>
			            </tr>
			             <tr>
			                <td>Provision of KC Municipal Office, furniture and equipment</td>
			                <td>
			                	<table class="table">
			                		<tr>
			                			<td>
			                				{{ Form::label('kc_office', 'KC-NCDDP Municipal Office Address') }}
			                			</td>
			                			<td>
						                	{{ Form::text('kc_office', $lguengagement->kc_office, array('class'=>'form-control')) }}
			                			</td>
			                		</tr>
			                	</table>

			                	{{ Form::label('kc_equipment', 'Equipment provided for KC-NCDDP') }}
			                	<!-- {{ Form::checkbox('kc_equipment', '1', $lguengagement->kc_equipment,array('id'=>'kc_equipment')) }}  -->
			                	<table class="table equipment" disabled>
			                		<tr>
			                			<th>Equipment Item</th>
			                			<th>Quantity</th>
			                			<th>Date Provided</th>
			                			<th>End-user</th>
			                			<th>Functionality</th>
			                			<th>	</th>
			                		</tr>
									<tbody id="equipment">
										@foreach($lgumdcequipments as $equipment)
											<tr>
												<td>{{  $equipment->equipment_type }}</td>
												<td>{{ $equipment->quantity }}</td>
												<td>{{ toDate($equipment->date) }}</td>
												<td>{{ $equipment->end_user }}</td>
												<td>{{ $equipment->functionality }}</td>

											</tr>
										@endforeach
									</tbody>
			                	</table>

			                </td>
			            </tr>
			            <tr>
			                <td>Provision of personnel (No. of Staff provided)</td>
			                <td>
			                	{{ $lguengagement->no_staff }}
			                </td>
			            </tr>
			            <tr>
			                <td>Assist community volunteers in KC/NCDD procurement and Fiduciary processes ( No. of TAs provided)</td>
			                <td>
			                	{{ $lguengagement->no_tas }}
			                </td>
			            </tr>
					</table>

					<br>
<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Transparency</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	            <div class="box-body">
					<div class="table-responsive">

					<table class="table table-striped table-bordered table-hover">
						<tr>
							<th colspan="3" style="text-align:center"> Available Information on State of Municipality </th>
						</tr>
						<tr>
							<th>Checklist</th>
			                <th>Location of Posting</th>
			                <th>Date Posted</th>
						</tr>
						<tr>
			                <td>Income and Expenditure</td>
			                <td>
			                	{{ $lguengagement->incexp_location_post }}
			                </td>
			                <td>
			                	{{ toDate($lguengagement->incexp_post_date) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Budget and data used for formulating the budget</td>
			                <td>
			                	{{ $lguengagement->budget_location_post }}
			                </td>
			                <td>
			                	{{ toDate($lguengagement->budget_post_date) }}
			                </td>
			            </tr>
			            <tr>
			                <td>LGU Plans and Activities</td>
			                <td>
			                	{{ $lguengagement->plan_location_post }}
			                </td>
			                <td>
			                	{{ toDate($lguengagement->plan_post_date) }}
			                </td>
			            </tr>
					</table>
			</div>
	            </div>

	        </div>
	 	</div>

<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Participation</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	            <div class="box-body">
					<div class="table-responsive">
					<br>

					<table class="table table-striped table-bordered table-hover">
			            <tr>

			            	<th colspan="4" style="text-align:center"> NGO-PO engaged and represented in MDC and LSB</th>
			            </tr>
			            <tr>
			            	<td></td>
			            	<td>Male</td>
			            	 <td>Female</td>
			            	 <td>Total</td>
			            </tr>
			            <tr>
			            	<td>No. of NGO-PO accredited </td>
			                <td>
			                	<!-- {{ Form::input('number', 'no_ngopo_male', $lguengagement->no_ngopo_male, array('class' => 'form-control', 'min'=>'0')) }} -->
			               	</td>
			               	<td></tf>
			               	<td>
			                	{{  $lguengagement->ngopo_accredited}}

			               	</td>


			            </tr>
			            <tr>
			            	<td>No. of NGO-PO represented in MDC </td>
			                <td>
			                	{{ $lguengagement->no_ngopo_represented_male }}
			               	</td>
			               	<td>
			                	{{ $lguengagement->no_ngopo_represented_female }}

			               	</td>
			               		<!-- <td>
			                	{{ Form::input('number', 'total_mdc', $lguengagement->total_mdc, array('class' => 'form-control','disabled')) }}

			               	</td> -->
			               	<td class="total_mdc">{{ $lguengagement->no_ngopo_represented_male + $lguengagement->no_ngopo_represented_female}}</td>
			            </tr>

			             <tr>
			            	<td>No. of NGO-PO represented in LSB </td>
			                <td>
			                	{{  $lguengagement->lsb_represented_male }}
			               	</td>
			               	<td>
			                	{{ $lguengagement->lsb_represented_female }}

			               	</td>
			               <!-- 	<td>
			                	{{ Form::input('number', 'total_lsb', $lguengagement->total_lsb, array('class' => 'form-control', 'min'=>'0','disabled')) }}

			               	</td> -->
			               	<td class="total_lsb">{{ $lguengagement->lsb_represented_male + $lguengagement->lsb_represented_female}}</td>

			            </tr>









			            <!--
			            	<th colspan="2" style="text-align:center"> NGO-PO engaged and represented in MDC and LSB</th>
			            </tr>
			            <tr>
			            	<td>No. of NGO-PO accredited </td>
			                <td>
			                	{{ $lguengagement->no_ngopo_male }}
			               	</td>
			            </tr>
			            <tr>
			            	<td>No. of NGO-PO represented in MDC</td>
			                <td>
			                	{{ $lguengagement->no_ngopo_represented_male}}
			               	</td>
			            </tr>

			            <tr>
			            	<td>No. of NGO-PO represented in LSB </td>
			                <td>
			                	{{ $lguengagement->no_ngopo_represented_female}}
			               	</td>
			            </tr> -->
			            <!-- <tr>
			            	<td>TOTAL </td>
			                <td>
			                	{{ $lguengagement->no_ngopo_represented }}
			               	</td>
			            </tr> -->
					</table>
	</div>
	            </div>

	        </div>
	 	</div>
					<br>

					<table class="table table-striped table-bordered table-hover">
						<tr>
			            	<th colspan="4" style="text-align:center"> Representation of highly marginalized/vulnerable groups in MDC and BDC</th>
			            </tr>
			            <tr>
			            	<th>Headcount </th>
			                <th>Male</th>
			                <th>Female</th>
			               	<th>Total</th>
			            </tr>
			            <tr>
			            	<td>Pantawid Pamilya Representatives</td>
			                <td>
			                	{{ $lguengagement->no_4p_male }}
			               	</td>
			                <td>
			                	{{ $lguengagement->no_4p_female }}
			                </td>
							<td class="total_4p">{{ $lguengagement->no_4p_male + $lguengagement->no_4p_female}}</td>
			            </tr>
			            <tr>
			            	<td> IPs representatives</td>
			                <td>
			                	{{ $lguengagement->no_ip_male }}
			               	</td>
			                <td>
			                	{{ $lguengagement->no_ip_female }}
			                </td>
							<td class="total_ip">{{ $lguengagement->no_ip_male + $lguengagement->no_ip_female}}</td>
			            </tr>
			            <tr>
			            	<td> Women representatives</td>
			                <td>
			                	{{ $lguengagement->no_women_male }}
			               	</td>
			                <td>
			                	{{ $lguengagement->no_women_female }}
			                </td>
							<td class="total_women">{{ $lguengagement->no_women_male + $lguengagement->no_women_female}}</td>
			            </tr>
			            <tr>
			            	<td> Youth representatives</td>
			                <td>
			                	{{ $lguengagement->no_youth_male }}
			               	</td>
			                <td>
			                	{{ $lguengagement->no_youth_female }}
			                </td>
							<td class="total_youth">{{ $lguengagement->no_youth_male + $lguengagement->no_youth_female}}</td>
			            </tr>
			            <tr>
			            	<td> Elderly representatives</td>
			                <td>
			                	{{  $lguengagement->no_elderly_male}}
			               	</td>
			                <td>
			                	{{ $lguengagement->no_elderly_female }}
			                </td>
							<td class="total_elderly">{{ $lguengagement->no_elderly_male + $lguengagement->no_elderly_female}}</td>
			            </tr>
			            <tr>
			            	<td> PWD representatives</td>
			                <td>
			                	{{ $lguengagement->no_pwd_male }}
			               	</td>
			                <td>
			                	{{ $lguengagement->no_pwd_female }}
			                </td>
							<td class="total_pwd">{{ $lguengagement->no_pwd_male + $lguengagement->no_pwd_female}}</td>
			            </tr>
					</table>

					<br>

					<!-- <table class="table table-striped table-bordered table-hover">
					          	<tr>
					          		<th colspan="2" style="text-align:center">Regular LGU-CSO consultation and dialogue (Date)</th>
					          	</tr>
					          	<tr>
					          		<th> # </th>
					          		<th> Date Conducted </th>
					          	</td>
					        	<?php $count = 1; ?>

					            		@foreach($lguengagementdates as $le)
					            			@if($le->activity_type == 1)
					            				<tr>
					            				<td>
					            					{{ $count }}
					            				</td>
					            				<td>
					            					{{ $le->date_conducted}}
					            				</td>
					            				</tr>
					            				<?php $count++; ?>
						        			@endif
						        		@endforeach


					</table>
					<br> -->
					    <div class="col-md-4">

					<table class="table table-striped table-bordered table-hover">
					          	<tr>
					          		<th style="text-align:center">Regular LGU-CSO consultation and dialogue (Date)</th>
					          	</tr>

					            	<tr>
					            	<td id="items1" >
					            		@foreach($lguengagementdates as $le)
					            			@if($le->activity_type == 1)
					            				<div class="input-group">
						            				<input type="hidden" class="form-control" value="1" name="activity_type[]">
					            					<input type="date" class="form-control consultation" value="{{$le->date_conducted}}" readonly="readonly" name="date_conducted[]" onchange="checkduplicate(this)">
					            					<span class="input-group-addon">
							            			<!-- <span class="fa fa-trash-o delete "> -->
							            			</span>
							        				</span>
						        				</div>
						        			@endif
						        		@endforeach
						        	</td>
					            	</tr>
					</table>
					<br>
					<!-- <button id="add1" class="btn btn-info form-control" type="button" ><i class="fa fa-plus"></i>Add Consultation Date</button> -->

					</div>

					<br>

					<div class="col-md-4">

                        <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <th style="text-align:center">Conduct participatory review and assessment of the gender responsiveness of GAD Plans and GAD funded projects (Date)</th>
                                    </tr>

                                        <tr>
                                        <td id="items2" >
                                            @foreach($lguengagementdates as $le)
                                                @if($le->activity_type == 2)
                                                    <div class="input-group">
                                                        <input type="hidden" class="form-control" value="2" name="activity_type[]">
                                                        <input type="date" class="form-control review" value="{{$le->date_conducted}}" name="date_conducted[]" readonly="readonly" onchange="checkduplicate(this)">
                                                        <span class="input-group-addon">
                                                        <!-- <span class="fa fa-trash-o delete "> -->
                                                        </span>
                                                        </span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        </tr>
                        </table>
                        <br>
                        <!-- <button id="add2" class="btn btn-info form-control" type="button" ><i class="fa fa-plus"></i>Add Review Date</button> -->


					<div class="clearfix"></div>
					</div>


					<div class="col-md-4">

                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th style="text-align:center">NGO participation in project implementation and monitoring</th>
                            </tr>
                            <tr>
                            <td id="items3" >
                                @foreach($lguengagementdates as $le)
                                    @if($le->activity_type == 3)
                                        <div class="input-group">
                                            <input type="hidden" class="form-control" value="3" name="activity_type[]">
                                            <input type="date" class="form-control monitor" value="{{$le->date_conducted}}" name="date_conducted[]" readonly="readonly" onchange="checkduplicate(this)">
                                            <span class="input-group-addon">
                                            <!-- <span class="fa fa-trash-o delete "> -->
                                            </span>
                                            </span>
                                        </div>
                                    @endif
                                @endforeach
                            </td>
                            </tr>
                        </table>
                        <br>
                        <!-- <button id="add3" class="btn btn-info form-control" type="button" ><i class="fa fa-plus"></i>Add Implementation & Monitoring Date</button> -->

					</div>
                    <div class="clearfix"></div>

					<!-- <table class="table table-striped table-bordered table-hover">
					          	<tr>
					          		<th colspan="2" style="text-align:center">Regular LGU-CSO consultation and dialogue (Date)</th>
					          	</tr>
					          	<tr>
					          		<th> # </th>
					          		<th> Date Conducted </th>
					          	</td>
					          	<?php $count = 1; ?>

					            		@foreach($lguengagementdates as $le)
					            			@if($le->activity_type == 2)
					            				<tr>
					            				<td>
					            					{{ $count }}
					            				</td>
					            				<td>
					            					{{ $le->date_conducted}}
					            				</td>
					            				</tr>
					            				<?php $count++; ?>
						        			@endif
						        		@endforeach


					</table>
					<br>

					<br>

					<table class="table table-striped table-bordered table-hover">
					          	<tr>
					          		<th colspan="2" style="text-align:center">NGO participation in project implementation and monitoring</th>
					          	</tr>
					        	<tr>
					          		<th> # </th>
					          		<th> Date Conducted </th>
					          	</td>
					            <?php $count = 1; ?>

					            		@foreach($lguengagementdates as $le)
					            			@if($le->activity_type == 3)
					            				<tr>
					            				<td>
					            					{{ $count }}
					            				</td>
					            				<td>
					            					{{ $le->date_conducted}}
					            				</td>
					            				</tr>
					            				<?php $count++; ?>
						        			@endif
						        		@endforeach

					</table> -->
					<br>


			 	</div>
			 </div>
		 </div>

		 <!--MLGU Technical Assistance Checklist-->
  <!--   	 <div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">MLGU Technical Assistance Checklist</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					
					
					<table class="table table-striped table-bordered table-hover">
						<tr>
			                <th>Organizing C/MIAC issued (EO Number)</th>
			                <td>
			                	{{ $lguengagement->miac_eo_no }}
			                </td>
			            </tr>
			            <tr>
			                <th>Organizing C/MIAC issued (EO Date)</th>
			                <td>
			                	{{ $lguengagement->miac_eo_date }}
			                </td>
			            </tr>
			            <tr>
			                <th>Organizing C/MCT issued (EO Number)</th>
			                <td>
			                	{{ $lguengagement->mct_eo_no }}
			                </td>
			            </tr>
			            <tr>
			                <th>Organizing C/MCT issued (EO Date)</th>
			                <td>
			                	{{ $lguengagement->mct_eo_date }}
			                </td>
			            </tr>
			            <tr>
			                <th>Name of GAD focal person</th>
			                <td>
			                	{{ $lguengagement->focal_person }}
			                </td>
			            </tr>
			            <tr>
			                <th>Name of encoder</th>
			                <td>
			                	{{ $lguengagement->encoder }}
			                </td>
			            </tr>
			            <tr>
			                <th>DWSD MAT/KC Office address</th>
			                <td>
			                	{{ $lguengagement->office_address }}
			                </td>
			            </tr>
			            <tr>
			                <th>Latitude</th>
			                <td>
			                	{{ $lguengagement->latitude }}
			                </td>
			            </tr>
			            <tr>
			                <th>Longitude</th>
			                <td>
			                	{{ $lguengagement->longitude }}
			                </td>
			            </tr>
			            <tr>
			                <th>Equipments and Logistic support provided</th>
			                <td>
			                	{{ Form::checkbox('mlgu_logistics', '1', $lguengagement->mlgu_logistics, array('class' => 'form-control', 'disabled')) }} 
			                	{{ Form::label('mlgu_logistics', 'Yes') }}	
			                </td>
			            </tr>
					</table>
					
					
			 	</div>
			 </div>
		 </div>
		  -->
		 <!--MDC Expansion Checklist-->
    	<!--  <div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">MDC Expansion Checklist</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					<table class="table table-striped table-bordered table-hover">
						<tr> </tr>
			            <tr>	
			            	<td> Resolution No. </td>
			            	<td>
			                	{{ $lguengagement->mdc_resolution_no }}
			                </td>
			                <td> Date </td>
			                <td>
			                	{{ $lguengagement->mdc_date }}
			                </td>
			            <tr>
			        </table> 
			        <br>
			        <table class="table table-striped table-bordered table-hover">
			            <tr>
			            	<th colspan="4" style="text-align:center"> Municipal resolution expanding MDC membership</th>
			            </tr>
			            <tr>
			            	<th>Headcount </th>
			                <th>Male</th>
			                <th>Female</th>
			               	<th>Total</th>
			            </tr>
			            <tr>
			            	<td> CDD Volunteers</td>
			                <td>
			                	{{ $lguengagement->no_cdd_male }}
			               	</td>
			                <td>
			                	{{ $lguengagement->no_cdd_female }}
			                </td>
							<td class="total_cdd">{{ $lguengagement->no_cdd_male + $lguengagement->no_cdd_female}}</td>
			            </tr>
			            <tr>
			            	<td> NGO/PO Representatives </td>
			                <td>
			                	{{ $lguengagement->no_ngopo_male }}
			               	</td>
			                <td>
			                	{{ $lguengagement->no_ngopo_female }}
			                </td>
							<td class="total_ngopo">{{ $lguengagement->no_ngopo_male + $lguengagement->no_ngopo_female}}</td>
			            </tr>
			            <tr>
			            	<td> LGU Representative </td>
			                <td>
			                	{{ $lguengagement->no_lgu_male }}
			               	</td>
			                <td>
			                	{{ $lguengagement->no_lgu_female }}
			                </td>
							<td class="total_lgu">{{ $lguengagement->no_lgu_male + $lguengagement->no_lgu_female}}</td>
			            </tr>
					</table>
			 	</div>
			 </div>
		 </div> -->
		
	</div>
	
	
	<div class="clearfix"></div>

@stop