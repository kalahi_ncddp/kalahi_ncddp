@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
    <div class="col-md-12">
	 	{{ Form::model($lguengagement, array('action' => array('LGUEngagementController@update', $lguengagement->engagement_id), 'method' => 'PUT')) }}
	 	
	 	<!--LGU Engagement-->
	 	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Basic Information</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	            <div class="box-body">
					<div class="table-responsive">
					<div class="col-md-6">

						<table class="table table-striped  table-hover">
				            <tr>
				                <th>Region</th>
				                <td>{{ $region = Report::get_region($lguengagement->municipal_psgc) }}</td>
				            </tr>
				            <tr>
				                <th>Province</th>
				                <td>{{ $province = Report::get_province($lguengagement->municipal_psgc) }}</td>
				            </tr>
						 	<tr>
				                <th>Municipality</th>
				                <td>{{ $municipality = Report::get_municipality($lguengagement->municipal_psgc) }}</td>
				            </tr>
				        </table>
				    </div>

					<div class="col-md-6">
						<table class="table table-striped  table-hover">

				            <tr>
				                <th>Program</th>
				                <td>{{ $lguengagement->program_id }}</td>
				            </tr>
				             <tr>
				                <th>Cycle</th>
				                <td>{{ $lguengagement->cycle_id }}</td>
				            </tr>
				            <tr>
			                	<th>Municipal and LGU MOA Signed</th>
			                	<td>
			                		{{ Form::input('date', 'moa_signed_date', $lguengagement->moa_signed_date, array('class' => 'form-control', 'max' => date("m/d/Y"), 'required')) }}
			                	</td>
			            	</tr>
			            
						</table>
					</div>
					</div>
	            <div class="clearfix"></div>
	            </div>

	        </div>
	 	</div>
	 	
	 	<!--PTA Integration Checklist -->
    	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Accountability</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					<table class="table table-striped table-bordered table-hover">
						<tr> 
							<th colspan="3" style="text-align:center"> Legislation/Resolution/Ordinances passed in support of CDD and Participatory Governance </th>
						</tr>
						<tr> 
							<th></th>
			                <th>Resolution Number</th>
			                <th>Date of Approval</th>
						</tr>
						<tr>
			                <td>Institutionalization of People’s PTA in LGU development, planning and poverty reduction programs, plans and activities</td>
			                <td>
			                	{{ Form::text('pta_resolution_no', $lguengagement->pta_resolution_no, array('class' => 'form-control', 'oninput'=>'setDate(this, pta_approval_date)')) }}
			                </td>
			                <td>
			                	{{ Form::input('date', 'pta_approval_date', $lguengagement->pta_approval_date, Input::old('pta_approval_date'), array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'pta_approval_date')) }}
			               		<!-- {{ Form::input('date', 'project_date', Input::old('project_date'), array('class' => 'form-control','max' => date("m/d/Y"))) }} -->
			                </td>
			            </tr>
			            <tr>
			                <td>Convergence of NGA property reduction PPAs in the municipality</td>
			                <td>
			                	{{ Form::text('nga_resolution_no', $lguengagement->nga_resolution_no, array('class' => 'form-control', 'oninput'=>'setDate(this, nga_approval_date)')) }}
			                </td>
			                <td>
			                	{{ Form::input('date', 'nga_approval_date', $lguengagement->nga_approval_date, Input::old('nga_approval_date'), array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'nga_approval_date')) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Formation of MIAC</td>
			                <td>
			                	{{ Form::text('miac_resolution_no', $lguengagement->miac_resolution_no, array('class' => 'form-control', 'oninput'=>'setDate(this, miac_approval_date)')) }}
			                </td>
			                <td>
			                	{{ Form::input('date', 'miac_approval_date', $lguengagement->miac_approval_date, Input::old('miac_approval_date'), array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'miac_approval_date')) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Formation of MCT</td>
			                <td>
			                	{{ Form::text('mct_resolution_no', $lguengagement->mct_resolution_no, array('class' => 'form-control', 'oninput'=>'setDate(this, mct_approval_date)')) }}
			                </td>
			                <td>
			                	{{ Form::input('date', 'mct_approval_date', $lguengagement->mct_approval_date, Input::old('mct_approval_date'), array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'mct_approval_date')) }}
			                </td>
			            </tr>
			            <tr>
			                <td>NGO and PO Accreditation Guidelines</td>
			                <td>
			                	{{ Form::text('ngopo_resolution_no', $lguengagement->ngopo_resolution_no, array('class' => 'form-control', 'oninput'=>'setDate(this, ngopo_approval_date)')) }}
			                </td>
			                <td>
			                	{{ Form::input('date', 'ngopo_approval_date', $lguengagement->ngopo_approval_date, Input::old('ngopo_approval_date'),array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'ngopo_approval_date')) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Support to BDP Formulation and BDP-MDP implementation including GAD plans</td>
			                <td>
			                	{{ Form::text('gad_resolution_no', $lguengagement->gad_resolution_no, array('class' => 'form-control', 'oninput'=>'setDate(this, gad_approval_date)')) }}
			                </td>
			                <td>
			                	{{ Form::input('date', 'gad_approval_date', $lguengagement->gad_approval_date, Input::old('gad_approval_date'), array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'gad_approval_date')) }}
			                </td>
			            </tr>
			            <tr>
			            	<td>Municipal resolution expanding MDC membership to include CDD volunteers/representative</td>
			            	<td>
			            		{{ Form::text('mdcmem_resolution_no', $lguengagement->mdcmem_resolution_no,  array('class' => 'form-control', 'oninput'=>'setDate(this, mdcmem_approval_date)')) }}
			            	</td>
			            	 <td>
			                	{{ Form::input('date', 'mdcmem_approval_date', $lguengagement->mdcmem_approval_date,Input::old('mdcmem_approval_date'),array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'mdcmem_approval_date')) }}
			                </td>
			            </tr>
			            <tr>
			            	<td></td>
			            	<td colspan='2' >No. of CDD Volunteers as members of MDC</td>
			            </tr>
			            <tr>
			            	<td></td>
			                <td>
			                	
				                <table class="table">
				                	<tr>
				                		<td>Male</td>
				                		<td>{{ Form::number('mdcmem_male_no',$lguengagement->mdcmem_male_no,array('class'=>'form-control','min'=>'0')) }}</td>
				                	</tr>
				                </table>
			                </td>
			                <td>
			                	<table class="table">
				                	<tr>
				                		<td>Female</td>
				                		<td>{{ Form::number('mdcmem_female_no',$lguengagement->mdcmem_female_no,array('class'=>'form-control','min'=>'0')) }}</td>
				                
				                	</tr>
				                </table>
			                </td>
			            	
			            </tr>
					</table>
					
					<br>
					
					<table class="table table-striped table-bordered table-hover">
						<tr> 
							<th colspan="2" style="text-align:center"> Support to KC Implementation </th>
						</tr>
						<tr>
			                <td>Allocation of LCC (SB Resolution number)</td>
			                <td>
			                	{{ Form::text('lcc_resolution_no', $lguengagement->lcc_resolution_no, array('class' => 'form-control', 'oninput'=>'setDate(this, lcc_approval_date)')) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Allocation of LCC (Approval Date)</td>
			                <td>
			                	{{ Form::input('date', 'lcc_approval_date', $lguengagement->lcc_approval_date, Input::old('lcc_approval_date'),array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'lcc_approval_date')) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Opening of Municipal Trust Fund (Account No.)</td>
			                <td>
			                	{{ Form::text('trust_account_no', $lguengagement->trust_account_no, array('class' => 'form-control', 'oninput'=>'setDate(this, trust_opened_date)')) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Opening of Municipal Trust Fund (Date Opened)</td>
			                <td>
			                	{{ Form::input('date', 'trust_opened_date', $lguengagement->trust_opened_date, Input::old('trust_opened_date'), array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'trust_opened_date')) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Provision of KC Municipal Office, furniture and equipment</td>
			                <td>
			                	<table class="table">
			                		<tr>
			                			<td>
			                				{{ Form::label('kc_office', 'KC-NCDDP Municipal Office Address') }}	
			                			</td>
			                			<td>
						                	{{ Form::text('kc_office', $lguengagement->kc_office, array('class'=>'form-control')) }} 
			                			</td>
			                		</tr>
			                	</table>
			                	
			                	
			                	{{ Form::label('kc_equipment', 'Equipment provided for KC-NCDDP? check if yes') }}
			                
			                	<!-- {{ Form::checkbox('is_equipment', '1',isset($lguengagement->is_equipment) ? true : false ,array('id'=>'kc_equipment') ) }}  -->
					         {{ Form::checkbox('is_equipment', '1', $lguengagement->is_equipment, array( 'id' => 'kc_equipment')) }} 

			                	<table class="table equipment" disabled>
			                		<tr>
			                			<th>Equipment Item</th>
			                			<th>Quantity</th>
			                			<th>Date Provided</th>
			                			<th>End-user</th>
			                			<th>Functionality</th>
			                			<th>	</th>
			                		</tr>
                                        <tbody id="equipment">
										@foreach($lgumdcequipments as $equipment)
											<tr>
												<td><input type="text" name="equipment_type[]" value="{{  $equipment->equipment_type }}" class="form-control" placeholder="Equipment/item Name/Type"/></td>
												<td><input type="number" name="quantity[]" value="{{ $equipment->quantity }}" class="form-control", min="0", placeholder="Quantity"/></td>
												<td><input type="text" value="{{ toDate($equipment->date) }}" name="equipment_date[]" class="form-control date" placeholder="Date"/></td>
												<td><input type="text" name="end_user[]" value="{{ $equipment->end_user }}" class="form-control" placeholder="End user"/></td>
												<td><input type="text" name="functionality[]" value="{{ $equipment->functionality }}" class="form-control" placeholder="Functionality"/></td>
												<td class="">
													<span class="btn delete ">
									            		<span class="fa fa-trash-o ">
									            		</span>
									        		</span>
												</td>
											</tr>
										@endforeach
									</tbody>
			                	</table>
			                	<button id="add_equipment" disabled class="btn equipment btn-info form-control" type="button" ><i class="fa fa-plus"></i>Add Equipment</button>
					
			                </td>
			            </tr>
			            <tr>
			                <td>Provision of personnel (No. of Staff provided)</td>
			                <td>
			                	{{ Form::input('number','no_staff', $lguengagement->no_staff, array('class' => 'form-control', 'min' =>'0')) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Assist community volunteers in KC/NCDD procurement and Fiduciary processes ( No. of TAs provided)</td>
			                <td>
			                	{{ Form::input('number','no_tas', $lguengagement->no_tas, array('class' => 'form-control', 'min' =>'0')) }}
			                </td>
			            </tr>
					</table>
					
					<br>
					<table class="table table-striped table-bordered table-hover">
						<tr> 
							<th colspan="3" style="text-align:center"> Available Information on State of Municipality </th>
						</tr>
						<tr> 
							<th>Checklist</th>
			                <th>Location of Posting</th>
			                <th>Date Posted</th>
						</tr>
						<tr>
			                <td>Income and Expenditure</td>
			                <td>
			                	{{ Form::text('incexp_location_post', $lguengagement->incexp_location_post, array('class' => 'form-control', 'oninput'=>'setDate(this, incexp_post_date)')) }}
			                </td>
			                <td>
			                	{{ Form::input('date', 'incexp_post_date', $lguengagement->incexp_post_date, Input::old('incexp_post_date'), array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'incexp_post_date')) }}
			                </td>
			            </tr>
			            <tr>
			                <td>Budget and data used for formulating the budget</td>
			                <td>
			                	{{ Form::text('budget_location_post', $lguengagement->budget_location_post, array('class' => 'form-control', 'oninput'=>'setDate(this, budget_post_date)')) }}
			                </td>
			                <td>
			                	{{ Form::input('date', 'budget_post_date', $lguengagement->budget_post_date, Input::old('budget_post_date'),array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'budget_post_date')) }}
			                </td>
			            </tr>
			            <tr>
			                <td>LGU Plans and Activities</td>
			                <td>
			                	{{ Form::text('plan_location_post', $lguengagement->plan_location_post,array('class' => 'form-control', 'oninput'=>'setDate(this, plan_post_date)')) }}
			                </td>
			                <td>
			                	{{ Form::input('date', 'plan_post_date', $lguengagement->plan_post_date, Input::old('plan_location_post'), array('class' => 'form-control', 'max' => date("m/d/Y"), 'disabled', 'id'=>'plan_post_date')) }}
			                </td>
			            </tr>
					</table>
					
					<br> 
					
					<table class="table table-striped table-bordered table-hover">
			            <tr>
			            	<th colspan="4" style="text-align:center"> NGO-PO engaged and represented in MDC and LSB</th>
			            </tr>
			            <tr>
			            	<td></td>
			            	<td>Male</td>
			            	 <td>Female</td> 
			            	 <td>Total</td>
			            </tr>
			            <tr>
			            	<td>No. of NGO-PO accredited </td>
			                <td>
			                	<!-- {{ Form::input('number', 'no_ngopo_male', $lguengagement->no_ngopo_male, array('class' => 'form-control', 'min'=>'0')) }} -->
			               	</td>
			               	<td></tf>
			               	<td>
			                	{{ Form::input('number', 'ngopo_accredited', $lguengagement->ngopo_accredited, array('class' => 'form-control', 'min'=>'0')) }}
			               		
			               	</td>

			               	
			            </tr>
			            <tr>
			            	<td>No. of NGO-PO represented in MDC </td>
			                <td>
			                	{{ Form::input('number', 'no_ngopo_represented_male', $lguengagement->no_ngopo_represented_male, array('class' => 'form-control', 'min'=>'0')) }}
			               	</td>
			               	<td>
			                	{{ Form::input('number', 'no_ngopo_represented_female', $lguengagement->no_ngopo_represented_female, array('class' => 'form-control', 'min'=>'0')) }}
			               		
			               	</td>
			               		<!-- <td>
			                	{{ Form::input('number', 'total_mdc', $lguengagement->total_mdc, array('class' => 'form-control','disabled')) }}
			               		
			               	</td> -->
			               	<td class="total_mdc">{{ $lguengagement->no_ngopo_represented_male + $lguengagement->no_ngopo_represented_female}}</td>
			            </tr>

			             <tr>
			            	<td>No. of NGO-PO represented in LSB </td>
			                <td>
			                	{{ Form::input('number', 'lsb_represented_male', $lguengagement->lsb_represented_male, array('class' => 'form-control', 'min'=>'0')) }}
			               	</td>
			               	<td>
			                	{{ Form::input('number', 'lsb_represented_female', $lguengagement->lsb_represented_female, array('class' => 'form-control', 'min'=>'0')) }}
			               		
			               	</td>
			               <!-- 	<td>
			                	{{ Form::input('number', 'total_lsb', $lguengagement->total_lsb, array('class' => 'form-control', 'min'=>'0','disabled')) }}
			               		
			               	</td> -->
			               	<td class="total_lsb">{{ $lguengagement->lsb_represented_male + $lguengagement->lsb_represented_female}}</td>

			            </tr>

			           <!--  <tr>
			            	<td>TOTAL</td>
			                <td>
			                	{{ Form::input('number', 'ngo_total','', array('class' => 'form-control ngo_total', 'readonly'=>'readonly')) }}
			               	</td>
			               	<td class="ngo_total"></td>
			               	<td>
			                	{{ Form::input('number', 'no_ngopo_represented_female', $lguengagement->no_ngopo_represented_female, array('class' => 'form-control', 'min'=>'0')) }}
			               		
			               	</td>
			            </tr> -->
					</table>
					
					<br> 
					
					<table class="table table-striped table-bordered table-hover">
						<tr>
			            	<th colspan="4" style="text-align:center"> Representation of highly marginalized/vulnerable groups in MDC and BDC</th>
			            </tr>
			            <tr>
			            	<th>Headcount </th>
			                <th>Male</th>
			                <th>Female</th>
			               	<th>Total</th>
			            </tr>
			            <tr>
			            	<td>Pantawid Pamilya Representatives</td>
			                <td>
			                	{{ Form::input('number', 'no_4p_male', $lguengagement->no_4p_male, array('class' => 'form-control', 'min'=>'0')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'no_4p_female', $lguengagement->no_4p_female, array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
							<td class="total_4p">{{ $lguengagement->no_4p_male + $lguengagement->no_4p_female}}</td>
			            </tr>
			            <tr>
			            	<td> IPs representatives</td>
			                <td>
			                	{{ Form::input('number', 'no_ip_male', $lguengagement->no_ip_male, array('class' => 'form-control', 'min'=>'0')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'no_ip_female', $lguengagement->no_ip_female, array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
							<td class="total_ip">{{ $lguengagement->no_ip_male + $lguengagement->no_ip_female}}</td>
			            </tr>
			            <tr>
			            	<td> Women representatives</td>
			                <td>
			                	{{ Form::input('number', 'no_women_male', $lguengagement->no_women_male, array('class' => 'form-control', 'min'=>'0')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'no_women_female', $lguengagement->no_women_female, array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
							<td class="total_women">{{ $lguengagement->no_women_male + $lguengagement->no_women_female}}</td>
			            </tr>
			            <tr>
			            	<td> Youth representatives</td>
			                <td>
			                	{{ Form::input('number', 'no_youth_male', $lguengagement->no_youth_male, array('class' => 'form-control', 'min'=>'0')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'no_youth_female', $lguengagement->no_youth_female, array('class' => 'form-control', 'min'=>'0' )) }}
			                </td>
							<td class="total_youth">{{ $lguengagement->no_youth_male + $lguengagement->no_youth_female}}</td>
			            </tr>
			            <tr>
			            	<td> Elderly representatives</td>
			                <td>
			                	{{ Form::input('number', 'no_elderly_male', $lguengagement->no_elderly_male, array('class' => 'form-control', 'min'=>'0')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'no_elderly_female', $lguengagement->no_elderly_female, array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
							<td class="total_elderly">{{ $lguengagement->no_elderly_male + $lguengagement->no_elderly_female}}</td>
			            </tr>
			            <tr>
			            	<td> PWD representatives</td>
			                <td>
			                	{{ Form::input('number', 'no_pwd_male', $lguengagement->no_pwd_male, array('class' => 'form-control', 'min'=>'0')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'no_pwd_female', $lguengagement->no_pwd_female, array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
							<td class="total_pwd">{{ $lguengagement->no_pwd_male + $lguengagement->no_pwd_female}}</td>
			            </tr>
					</table>
					
					<br> 
					<div class="col-md-4">

					<table class="table table-striped table-bordered table-hover">
					          	<tr>
					          		<th style="text-align:center">Regular LGU-CSO consultation and dialogue (Date)</th>
					          	</tr>

					            	<tr>
					            	<td id="items1" >
					            		@foreach($lguengagementdates as $le)
					            			@if($le->activity_type == 1)
					            				<div class="input-group">
						            				<input type="hidden" class="form-control" value="1" name="activity_type[]">
					            					<input type="date" class="form-control consultation" value="{{$le->date_conducted}}" name="date_conducted[]" onchange="checkduplicate(this)">
					            					<span class="input-group-addon">
							            			<span class="fa fa-trash-o delete ">
							            			</span>
							        				</span>
						        				</div>
						        			@endif
						        		@endforeach
						        	</td>
					            	</tr>
					</table>
					<br>
					<button id="add1" class="btn btn-info form-control" type="button" ><i class="fa fa-plus"></i>Add Consultation Date</button>

					</div>

					<div class="col-md-4">

                        <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <th style="text-align:center">Conduct participatory review and assessment of the gender responsiveness of GAD Plans and GAD funded projects (Date)</th>
                                    </tr>

                                        <tr>
                                        <td id="items2" >
                                            @foreach($lguengagementdates as $le)
                                                @if($le->activity_type == 2)
                                                    <div class="input-group">
                                                        <input type="hidden" class="form-control" value="2" name="activity_type[]">
                                                        <input type="date" class="form-control review" value="{{$le->date_conducted}}" name="date_conducted[]" onchange="checkduplicate(this)">
                                                        <span class="input-group-addon">
                                                        <span class="fa fa-trash-o delete ">
                                                        </span>
                                                        </span>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </td>
                                        </tr>
                        </table>
                        <br>
                        <button id="add2" class="btn btn-info form-control" type="button" ><i class="fa fa-plus"></i>Add Review Date</button>


					<div class="clearfix"></div>
					</div>


					<div class="col-md-4">

                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th style="text-align:center">NGO participation in project implementation and monitoring</th>
                            </tr>
                            <tr>
                            <td id="items3" >
                                @foreach($lguengagementdates as $le)
                                    @if($le->activity_type == 3)
                                        <div class="input-group">
                                            <input type="hidden" class="form-control" value="3" name="activity_type[]">
                                            <input type="date" class="form-control monitor" value="{{$le->date_conducted}}" name="date_conducted[]" onchange="checkduplicate(this)">
                                            <span class="input-group-addon">
                                            <span class="fa fa-trash-o delete ">
                                            </span>
                                            </span>
                                        </div>
                                    @endif
                                @endforeach
                            </td>
                            </tr>
                        </table>
                        <br>
                        <button id="add3" class="btn btn-info form-control" type="button" ><i class="fa fa-plus"></i>Add Implementation & Monitoring Date</button>

					</div>
                    <div class="clearfix"></div>
			 	</div>
			 </div>
		 </div>

		 <div class="form-group pull-right">
								<label>Save as draft</label>
							  <input type="checkbox" name="is_draft" {{ $lguengagement->is_draft==1 ? 'checked': '' }}>
							</div>
	 
		 <a  href="{{ URL::to('lguengagement') }}" class="btn btn-default">close</a>
  <div class="col-md-12 ">
									<div class="form-group pull-right">
						                <button class="btn btn-primary" id="save">
											<i class="fa fa-save"></i>
											Submit 
										</button>
		                 				<a class="btn btn-default" href="{{ URL::to('lguengagement') }}">Cancel</a>		
									</div>
								</div>
								<div class="clearfix"></div>
		{{ Form::close() }}
		 
		
	</div>
	
	
	<div class="clearfix"></div>
	
	<script>
		$('#add_equipment').click(function(){
			$("#equipment").append('<tr>'+
										'<td><input type="text" name="equipment_type[]" class="form-control" placeholder="Equipment/item Name/Type"/></td>'+
										'<td><input type="number" name="quantity[]" class="form-control" min="0" placeholder="Quantity"/></td>'+
										'<td><input type="date" name="equipment_date[]" class="form-control date" placeholder="Date"/></td>'+
										'<td><input type="text" name="end_user[]" class="form-control" placeholder="End user"/></td>'+
										'<td><input type="text" name="functionality[]" class="form-control" placeholder="Functionality"/></td>'+
										'<td class="">'+
											'<span class="btn delete ">'+
							            		'<span class="fa fa-trash-o ">'+
							            		'</span>'+
							        		'</span>'+
										'</td>');
		})
		$("#add1").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items1").append('<div class="input-group">'+
						            		'<input type="hidden" class="form-control" value="1" name="activity_type[]">'+						            		
					            			'<input type="date" class="form-control consultation" name="date_conducted[]" onchange="checkduplicate(this)">'+
					            			'<span class="input-group-addon">'+
							            		'<span class="fa fa-trash-o delete ">'+
							            		'</span>'+
							        		'</span>'+
						        		'</div>');
				});
				
		$("#add2").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items2").append('<div class="input-group">'+
						            		'<input type="hidden" class="form-control" value="2" name="activity_type[]">'+						            		
					            			'<input type="date" class="form-control review" name="date_conducted[]" onchange="checkduplicate(this)">'+
					            			'<span class="input-group-addon">'+
							            		'<span class="fa fa-trash-o delete ">'+
							            		'</span>'+
							        		'</span>'+
						        		'</div>');
				});
				
		$("#add3").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items3").append('<div class="input-group">'+
						            		'<input type="hidden" class="form-control" value="3" name="activity_type[]">'+						            		
					            			'<input type="date" class="form-control monitor" name="date_conducted[]" onchange="checkduplicate(this)">'+
					            			'<span class="input-group-addon">'+
							            		'<span class="fa fa-trash-o delete ">'+
							            		'</span>'+
							        		'</span>'+
						        		'</div>');
				});
				
		$("body").on("click", ".delete", function (e) {
				$(this).parent().parent().remove();
			});
	
		/* get the total */
			var total = (function($){
				return {
					
					total4p : function(){
						var total_ = parseInt($("input[name='no_4p_male']").val()) + parseInt($("input[name='no_4p_female']").val()) ;
						$('.total_4p').text(total_ || '0');
					},
					totalNGOREP : function(){
						var total_ = parseInt($("input[name='no_ngopo_represented_male']").val()) + parseInt($("input[name='no_ngopo_represented_female']").val()) ;
						$('.total_mdc').text(total_ || '0');
					},
					totalNGOREPLGU : function(){
						var total_ = parseInt($("input[name='lsb_represented_male']").val()) + parseInt($("input[name='lsb_represented_female']").val()) ;
						$('.total_lsb').text(total_ || '0');
					},
					totalip : function(){
						var total_ = parseInt($("input[name='no_ip_male']").val()) + parseInt($("input[name='no_ip_female']").val()) ;
						$('.total_ip').text(total_ || '0');
					},
					totalwomen : function(){
						var total_ = parseInt($("input[name='no_women_male']").val()) + parseInt($("input[name='no_women_female']").val()) ;
						$('.total_women').text(total_ || '0');
					},
					totalyouth : function(){
						var total_ = parseInt($("input[name='no_youth_male']").val()) + parseInt($("input[name='no_youth_female']").val()) ;
						$('.total_youth').text(total_ || '0');
					},
					totalelderly : function(){
						var total_ = parseInt($("input[name='no_elderly_male']").val()) + parseInt($("input[name='no_elderly_female']").val()) ;
						$('.total_elderly').text(total_ || '0');
					},
					totalpwd : function(){
						var total_ = parseInt($("input[name='no_pwd_male']").val()) + parseInt($("input[name='no_pwd_female']").val()) ;
						$('.total_pwd').text(total_ || '0');
					},
					totalcdd : function(){
						var total_ = parseInt($("input[name='no_cdd_male']").val()) + parseInt($("input[name='no_cdd_female']").val()) ;
						$('.total_cdd').text(total_ || '0');
					}
					// totalngopo : function(){
					// 	var total_ = parseInt($("input[name='no_ngopo_male']").val()) + parseInt($("input[name='no_ngopo_female']").val()) ;
					// 	$('.total_ngopo').text(total_ || '0');
					// },
					// totallgu : function(){
					// 	var total_ = parseInt($("input[name='no_lgu_male']").val()) + parseInt($("input[name='no_lgu_female']").val()) ;
					// 	$('.total_lgu').text(total_ || '0');
					// }
					// totalNGO : function(){
					// 	var total_ = parseInt($("input[name='no_ngopo_male']").val()) + parseInt($("input[name='no_ngopo_represented_male']").val()) + parseInt($("input[name='no_ngopo_represented_female']").val()) ;
					// 	$('.ngo_total').val(total_ || '0');
					// }
					
				}
			})(jQuery);

				total.totalNGOREP();
				total.totalNGOREPLGU();

			$("input[name='no_4p_male']").change(total.total4p);
			$("input[name='no_4p_female']").change(total.total4p);
			$("input[name='no_ip_male']").change(total.totalip);
			$("input[name='no_ip_female']").change(total.totalip);
			$("input[name='no_women_male']").change(total.totalwomen);
			$("input[name='no_women_female']").change(total.totalwomen);
			$("input[name='no_youth_male']").change(total.totalyouth);
			$("input[name='no_youth_female']").change(total.totalyouth);
			$("input[name='no_elderly_male']").change(total.totalelderly);
			$("input[name='no_elderly_female']").change(total.totalelderly);
			$("input[name='no_pwd_male']").change(total.totalpwd);
			$("input[name='no_pwd_female']").change(total.totalpwd);
			$("input[name='no_cdd_male']").change(total.totalcdd);
			$("input[name='no_cdd_female']").change(total.totalcdd);
			// $("input[name='no_ngopo_male']").change(total.totalngopo);
			// $("input[name='no_ngopo_female']").change(total.totalngopo);
			// $("input[name='no_lgu_male']").change(total.totallgu);
			// $("input[name='no_lgu_female']").change(total.totallgu);
			// $("input[name='no_ngopo_male']").keyup(total.totalNGO);
			$("input[name='no_ngopo_represented_male']").keyup(total.totalNGOREP);
			$("input[name='no_ngopo_represented_female']").keyup(total.totalNGOREP);
			$("input[name='lsb_represented_male']").keyup(total.totalNGOREPLGU);
			$("input[name='lsb_represented_female']").keyup(total.totalNGOREPLGU);


			function setDate(e, d){
				if(e.value != ""){
					d.removeAttribute("disabled");
					d.setAttribute("required", "required");
				}
				else{
					d.value = "0000-00-00";
					d.setAttribute("disabled","disabled");
					d.removeAttribute("required");
				}
			}
			
			/*Check Duplicate Entry*/
			function checkduplicate(e){
				
				var count = -1;
				var elements = document.getElementsByClassName(e.className);
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					e.setCustomValidity("Duplicate Entry not allowed");
					
				}else{
					e.setCustomValidity("");
				}
				
			}
			$("#kc_equipment").change(function(){
					if($(this).is(":checked")){
						$(".equipment").removeAttr('disabled');
					}
					else{
						$(".equipment").attr('disabled','disabled');
						// $("#is_ppleader").prop('checked', false);	
					}
				});

			// var total = (function($){
			// 	return {
					
					

			// 	}
			// })(jQuery);
   //              total.totalNGO();
               

			// $("input[name='no_ngopo_male']").keyup(total.totalNGO);
			// $("input[name='no_ngopo_represented_male']").keyup(total.totalNGO);
			// $("input[name='no_ngopo_represented_female']").keyup(total.totalNGO);
	
	</script>
	
@stop