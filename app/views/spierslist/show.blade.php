@extends('layouts.default')

@section('content')
<div class="row">
    <div class="col-md-12">
		<!-- Advanced Tables -->
		
    	<div class="panel panel-default">
    		<div class="panel-heading">
    			<h4 class="col-xs-8 col-md-8" style="padding-left: 0;">{{$spi_profile_name}} <small>ERS List</small></h4>
					@if($position->position=='ACT')
                       <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                       <span class="hidden module">spi_erslist</span>
                     @endif
					<a id="spi-adders_modal_show" href="" class="btn btn-primary pull-right">
						<i class="glyphicon glyphicon-plus"></i>  Add ERS
					</a>

		        <div class="clearfix"></div>
    		</div>

			<div class="panel-body">
				<div class="table-responsive">
					<div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
						@if ($update_status !== null)
							@if ($update_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> ERS successfully added!</strong>
								</div>
							@elseif ($update_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> ERS can not be added</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if ($destroy_status !== null)
							@if ($destroy_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> ERS successfully removed!</strong>
								</div>
							@elseif ($destroy_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> ERS can not be removed</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						<table class="table table-striped table-bordered table-hover dataTable" id="dataTables-example" aria-describedby="dataTables-example_info">
						<thead>
							<tr>
							    @if($position->position=='ACT')
                                <th>Select all
                                    <input type="checkbox" id="selecctall"/>
                                </th>
                                @endif
								<th style="width: 15%;">Date of Reporting</th>
								<th style="width: 15%;">Period Start Date</th>
								<th style="width: 15%;">Period End Date</th>
								<th style="width: 15%;">Days</th>
								<th style="width: 15%;">Total Labor Cost</th>
								<th style="width: 15%; text-align: center;">Action</th>

						</thead>
						
						<tbody>
							@foreach ($ers_list as $ers)
							<tr data-rowid="{{{$ers['record_id']}}}">
                                   @if($position->position=='ACT')
							    <td>
                                      @if($ers->is_draft==0)
                                        {{ $reference->approval_status($ers->record_id, 'ACT') }}
                                      @else
                                          <span class="label label-warning">draft</span>
                                      @endif
                                </td>
                                    @endif
								<td>
									{{date("m/d/Y", strtotime($ers['date_reporting']))}}
								</td>
								<td>
									{{date("m/d/Y", strtotime($ers['date_start']))}}
								</td>
								<td>
									{{date("m/d/Y", strtotime($ers['date_ending']))}}
								</td>
								<td>
								<?php
									$date_start  = strtotime($ers['date_start']);
									$date_ending = strtotime($ers['date_ending']);
									$date_diff   = ($date_ending - $date_start)/(60*60*24) + 1;
									echo $date_diff;
								?>
								</td>

								<td>{{ $ers->totalLabor() }}</td>
		     						
								<td>
									@if( !is_review($ers->record_id) )
									

									<a href="{{ URL::to('spi-ers/'. $ers['record_id'] . '/edit') }}" class="btn btn-sm btn-info">
											<i class="glyphicon glyphicon-edit"></i> Edit
									</a>&nbsp;&nbsp;<a href="" class="btn btn-sm btn-danger spi-adders_modal-del_show">
										<i class="glyphicon glyphicon-trash"></i> Delete

									</a>
									@endif
								</td>
							</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="spi-adders_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Add New ERS</h4>
            </div>
            <div class="modal-body">
				<form role="form" id="spi-adders_modal_form">
					<div class="form-group">
						<label for="in-datereport_cont">Date of Reporting</label>
						<div class='input-group date' id='in-datereport_cont'>
			                <input class="form-control" required="required" type="text" 
			                	   id="in-datereport" name="in-datereport" placeholder="When is/was the reporting date?" value=""> <br>
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>  
					
					<div class="form-group">
						<label for="in-datestart_cont">Period Start Date</label>
						<div class='input-group date' id='in-datestart_cont'>
			                <input class="form-control" required="required" type="text" 
			                	   id="in-datestart" name="in-datestart" placeholder="When is/was the starting date?" value=""> <br>
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>  
					
					<div class="form-group">
						<label for="in-dateending_cont">Period End Date</label>
						<div class='input-group date' id='in-dateending_cont'>
			                <input class="form-control" required="required" type="text" 
			                	   id="in-dateending" name="in-dateending" placeholder="When is/was the end date?" value=""> <br>
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>  
				</form>
		    </div>
           <div class="modal-footer">
	            <div id="spi-adders_modal_form_error" class="alert alert-danger pull-left" role="alert" 
	            	 style="padding-top: 5px; padding-bottom: 5px; margin-bottom: 0px; margin-left: 0px; padding-left: 15px; display: none;">
			      End Date must be before Start Date
			    </div>
				<button id="spi-adders_modal_form_submit"  class="btn btn-success confirm" data-value="{{ URL::to('spi-ers') }}">
					<i class="glyphicon glyphicon-floppy-save"></i> Submit</button>
				<button class="btn bg-navy closes" data-dismiss="modal">Close</button>
           </div>
        </div>
    </div>
</div>


<div class="modal fade" id="spi-adders_modal-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
				<form>
					<input type="hidden" id="del-id" name="del-id" value="">
					<p class="col-xs-2" style="text-align: right;">
						<i class="glyphicon glyphicon-question-sign" style="color: #f4543c; font-size: 30px;"></i>
					</p>
					<p class="col-xs-10" style="font-size: 15px; padding-top: 5px; padding-bottom: 5px;">Are you sure you want to delete 
						 this ERS?</p>
					<div class="clearfix"></div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="{{ URL::to('spi-erslist') }}" id="spi-adders_modal_delform_submit" href="" class="btn btn-danger">Yes</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">No</a>
			</div>
        </div>
    </div>
</div>

<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();

    	setTimeout(function(){
    		$('div.alert').slideUp();
    	}, 5000);
        /*
         *  ADDING ERS 
         */
         
   	    $('#spi-adders_modal_show').on('click', function()
	    {
		    $('#spi-adders_modal').modal('show');
		    return false;
	    });

		$('#spi-adders_modal_form_submit').on('click', function(event)
		{
			$('div.form-group').removeClass('has-error');
			$('#spi-adders_modal_form_error').slideUp();

			var report_date = $('#in-datereport').val();
			var start_date  = $('#in-datestart').val();
			var end_date    = $('#in-dateending').val();

			console.log('Report Date : ' + report_date);
			console.log('Start Date  : ' + start_date);
			console.log('End Date    : ' + end_date);

			if(report_date && start_date && end_date)
			{
				var d_report_date = new Date(report_date);
				var d_start_date  = new Date(start_date);
				var d_end_date    = new Date(end_date);

				if(!(d_end_date <= d_report_date))
				{
					$('#in-datereport').parents('div.form-group').addClass('has-error');
					$('#in-dateending').parents('div.form-group').addClass('has-error');
					$('#spi-adders_modal_form_error').text('End Date must be earlier or the same as Report Date').slideDown();
				}
				else if(!(d_start_date <= d_report_date))
				{
					$('#in-datereport').parents('div.form-group').addClass('has-error');
					$('#in-dateending').parents('div.form-group').addClass('has-error');
					$('#spi-adders_modal_form_error').text('Start Date must be earlier or the same as Report Date').slideDown();
				}
				else if(!(d_start_date <= d_end_date))
				{
					console.log('Start Date not earlier than end date');
					$('#in-datestart').parents('div.form-group').addClass('has-error');
					$('#in-dateending').parents('div.form-group').addClass('has-error');
					$('#spi-adders_modal_form_error').text('Start Date must be earlier or the same as End Date').slideDown();
				}
				else
				{
					var goto_url = $(event.currentTarget).data('value');
					$.ajax({
				  		type: "PUT",
				  		url:  window.location.href,
				  		data: $( "#spi-adders_modal_form" ).serialize()
					})
					.done(function(msg){
						$('#spi-adders_modal').modal('hide');
						console.log(msg);
						console.log(goto_url + '/' + msg + '/edit');
						window.location = goto_url + '/' + msg + '/edit';
					});				
				}
			}
			else
			{
				$('#spi-adders_modal_form_error').text('Please enter a value before clicking Submit.').slideDown();
			}

			return false;
		});
      
		/* end of ADDING ERS */
		/*
         *  DELETING a committee
         */
		$('.spi-adders_modal-del_show').on('click', function(event){
			var id = $(event.currentTarget).parents('tr').data('rowid');
			$('#del-id').val(id);
			$('#spi-adders_modal-del').modal('show');
			return false;
      	});

      	$('#spi-adders_modal_delform_submit').on('click', function(event)
		{
			var $url = $(event.currentTarget).attr('href') + '/' + $('#del-id').val();

			$.ajax({
		  		type: "DELETE",
		  		url: $url
			})
		  	.done(function( msg ) {
		    	$('#spi-adders_modal-del').modal('hide');
		    	window.location = window.location.href;
		  	});
			return false;
		});
	});
</script>
@stop