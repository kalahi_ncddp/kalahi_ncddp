@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')

        <a class="btn btn-default" href="{{ URL::to('mlprapdetails/'.$mlprapdetails->plan_id) }}">Go Back</a>
		<div class="row">
			<div class="col-md-12">
				<h4>Edit Action Plan</h4>
			</div>
		</div>
	  
	    <div class="row">
		    <div class="col-md-12">
				
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				{{ Form::open(array('route' => array('mlprapdetails.update', $mlprapdetails->actionplan_id), 'method' => 'PUT')) }}
				{{ Form::token() }}
				
					<div class="col-md-4 left-border">
						<div class="form-group">
							{{ Form::label('project_name', 'Project Name') }}
							{{ Form::text('project_name', $mlprapdetails->project_name, array('class'=>'form-control', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('coverage', 'Areas Covered') }}
							{{ Form::text('coverage', $mlprapdetails->coverage, array('class'=>'form-control', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('stakeholders', 'Stakeholders') }}
							{{ Form::text('stakeholders', $mlprapdetails->stakeholders, array('class'=>'form-control', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('agency', 'Agency') }}
                              @foreach($agency as $agencies)
                                    @if(in_array($agencies, $mlprapdetails->agency))
                                        <div class = "form-control"><input type="checkbox" name="agency[]" value="{{$agencies}}" checked>&nbsp{{$agencies}}</div>
                                    @else
                                        <div class = "form-control"><input type="checkbox" name="agency[]" value="{{$agencies}}">&nbsp{{$agencies}}</div>
                                    @endif
                                @endforeach
						</div>

						<div class="form-group">
							{{ Form::label('beneficiaries', 'Beneficiaries') }}
							{{ Form::text('beneficiaries', $mlprapdetails->beneficiaries, array('class'=>'form-control', 'required')) }}
						</div>
						
					</div>


					<div class="col-md-4 left-border">
						<div class="form-group">
							{{ Form::label('problem', 'Priority Problem') }}
							{{ Form::textarea('problem', $mlprapdetails->problem, array('class'=>'form-control', 'required', 'rows'=>'5')) }}
						</div>
						<div class="form-group">
							{{ Form::label('gaps', 'Persisting Gaps/Constraints') }}
							{{ Form::textarea('gaps', $mlprapdetails->gaps, array('class'=>'form-control', 'required', 'rows'=>'5')) }}
						</div>

						<div class="form-group">
							{{ Form::label('actions', 'Possible Actions/Interventions') }}
							{{ Form::textarea('actions', $mlprapdetails->actions, array('class'=>'form-control', 'required', 'rows'=>'5')) }}
						</div>
					</div>


					<div class="col-md-4 left-border">
						<div class="form-group">
							<!-- {{ Form::label('timeframe', 'Time Frame') }}
							{{ Form::text('timeframe', $mlprapdetails->timeframe, array('class'=>'form-control', 'required')) }} -->
							{{ Form::label('', 'Time Frame') }}
							<br>
							{{ Form::label('', 'From:') }}
							{{ Form::text('timeframe_from', toDate($mlprapdetails->timeframe_from), array('class'=>'form-control date')) }}
							{{ Form::label('', 'To:') }}
							{{ Form::text('timeframe_to',  toDate($mlprapdetails->timeframe_to), array('class'=>'form-control date')) }}
						</div>

						<div class="form-group">
							{{ Form::label('fund_amount', 'Funding Requirement') }}
							{{ Form::currency('fund_amount',number_format($mlprapdetails->fund_amount,2, '.', ''), array('class'=>'form-control','step'=>'any', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('fund_source', 'Fund Source') }}
							{{ Form::select('fund_source', array('default'=>'') + $fund_sources  + array('others' => 'Others'), $fund_source, array('class' => 'form-control', 'required')) }}

							@if( $fund_source === 'others')
								{{ Form::currency('other_fund_source', $mlprapdetails->fund_source,
												array('class'=>'form-control', 'id'=>'other_fund_source', 'name'=>'other_fund_source')) }}
							@else
								{{ Form::currency('other_fund_source', Input::old('other_fund_source'),
												array('class'=>'form-control', 'id'=>'other_fund_source', 'name'=>'other_fund_source','placeholder'=>'Please Specify')) }}
							@endif
							
						</div>

						<div class="form-group">
							{{ Form::label('lead_group', 'Lead Group') }}
                            <table class="table table-striped table-bordered table-hover">
                                @foreach($leadgroups as $leadgroups_list)
                                    @if(in_array($leadgroups_list, $mlprapdetails->lead_group))
                                        <div class = "form-control"><input type="checkbox" name="lead_group[]" value="{{$leadgroups_list}}" checked>&nbsp{{$leadgroups_list}}</div>
                                    @else
                                        <div class = "form-control"><input type="checkbox" name="lead_group[]" value="{{$leadgroups_list}}">&nbsp{{$leadgroups_list}}</div>
                                    @endif
                                @endforeach
                            </table>

						</div>


						<!-- For MLPRAP redirect -->
						{{ Form::hidden('mlprap_id', $mlprap->action_id)}}
						<div class="form-group">
								<label>Save as draft</label>
							  <input type="checkbox" name="is_draft" {{ $mlprapdetails->is_draft==1 ? 'checked': '' }}>
							</div>
						{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}				
						{{ HTML::linkRoute('mlprapdetails.show', 'Cancel', $mlprap->action_id, array('class' => 'btn btn-default')) }}
					</div>				

				{{ Form::close() }}
			</div>
		</div>


		<script>
			// initial
			var fs = document.getElementById('fund_source').value;

			if( fs === 'others' ) {
				$('#other_fund_source').show();
				fs.required = true;
			}
			else {
				$('#other_fund_source').hide();
				fs.required = false;
			}




			// on change		
				
			$('select[name=fund_source]').change(function () {
			    if ($(this).val() == 'others') {
			        $('#other_fund_source').show();
			        document.getElementById('other_fund_source').required = true;
			    } else {
			        $('#other_fund_source').hide();
			        document.getElementById('other_fund_source').required = false;
			    }
			});
            $('select[name=agency]').change(function () {
			    if ($(this).val() == 'others') {
			        $('#other_agency').show();
			        document.getElementById('other_agency').required = true;
			    } else {
			        $('#other_agency').hide();
			        document.getElementById('other_agency').required = false;
			    }
			});
			$('select[name=lead_group]').change(function () {
			    if ($(this).val() == 'others') {
			        $('#other_lead_group').show();
			        document.getElementById('other_lead_group').required = true;
			    } else {
			        $('#other_lead_group').hide();
			        document.getElementById('other_lead_group').required = false;
			    }
			});
		</script>

		

		
@stop




<?php 

// <!--  Script for dropdown + others field   -->
// 		<script>
// 			var fs = document.getElementById('#other_fund_source');
// 			var lg = document.getElementById('#other_lead_group');

// 			if( fs.options[fs.selectedIndex].text == 'others') {
// 				$('#other_fund_source').show();				
// 			}
// 			else {
// 				$('#other_fund_source').hide();
// 				fs.required = false;
// 			}

// 			if( lg.options[lg.selectedIndex].text == 'others') {
// 				$('#other_lead_group').show();				
// 			}
// 			else {
// 				$('#other_lead_group').hide();
// 				lg.required = false;	
// 			}


// 			$('select[name=fund_source]').change(function () {
// 			    if ($(this).val() == 'others') {
// 			        $('#other_fund_source').show();
// 			        fs.required = true;
// 			    } else {
// 			        $('#other_fund_source').hide();
// 			        fs.required = false;
// 			    }
// 			});

// 			$('select[name=lead_group]').change(function () {
// 			    if ($(this).val() == 'others') {
// 			        $('#other_lead_group').show();
// 			        lg.required = true;
// 			    } else {
// 			        $('#other_lead_group').hide();
// 			        lg.required = false;
// 			    }
// 			});
// 		</script>

// {{ Form::open(array('url' => 'league/edit/' . $league->id , 'method' => 'PUT')) }}
// 	{{ Form::token() }}

		
// 		<p>
// 			{{ Form::label('league_name', 'League Name: ') }} <br>
// 			{{ Form::text('league_name', $league->name ) }} <br>
// 			{{ $errors->first('league_name', '<p style=color:red>:message</p>') }}
// 		</p>
// 		<p>
// 			{{ Form::label('venue', 'Venue: ') }} <br>
// 			{{ Form::text('venue', $league->venue) }} <br>
// 			{{ $errors->first('venue', '<p style=color:red>:message</p>') }}
// 		</p>

// 		{{--   Add More  --}}






// 		{{ Form::hidden('id', $league->id) }}

// 		{{ Form::submit('Save Changes', array('class'=>'btn btn-default')) }}

// 	{{ Form::close() }}

?>







