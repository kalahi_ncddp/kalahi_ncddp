@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
<a class="btn btn-default" href="{{ URL::to('mlprap') }}">Go Back</a>
    <hr />
    <div class="row">
	    <div class="col-md-12">
			<!-- Advanced Tables -->
	
	    	<div class="panel panel-default">
	    		<div class="panel-heading">
	    			Action Plans for {{ $mlprap[0]->for_year }} <!--Date Prepared: {{ date('m/d/Y', strtotime($mlprap[0]->date_prepared))}} -->
			        {{ HTML::linkRoute('mlprapdetails.create', 'Add New', $mlprap_id, array('class'=>'btn btn-primary pull-right')) }}
			        <div class="clearfix"></div>
	    		</div>

				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Project Name</th>
									<th>Areas Covered</th>
									<th>Priority Problem</th>
									<th>Details</th>
									
								</tr>
							</thead>
							<tbody>
								@foreach( $mlprap_details as $key=>$detail )
									<tr>
										<td>
											{{ $detail->project_name }}
										</td>
										<td>
											{{ $detail->coverage }}
										</td>
										<td>
											{{ $detail->problem }}
										</td>
										<td>
											{{ HTML::link('#mlprap_details_' . $key, 'View Details', array('data-toggle'=>'modal', 
																								   'data-target'=>'#mlprap_details_' . $key, 'class' => 'btn btn-success view')) }}
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>





	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>





	<!-- Modal -->
	@foreach( $mlprap_details as $key=>$detail )

		<div class="modal fade" id="mlprap_details_{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">

	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    <h4 class="modal-title" id="myModalLabel">Action Plan Details</h4>
	                </div>

	                <div class="modal-body">
	                	
	                		<ul>
	                			<li>
	                				Project Name: <b>{{ $detail->project_name }}</b>
	                			</li>
	                			<li>
	                				Areas Covered (Barangay): <b>{{ $detail->coverage }}</b>
	                			</li>
	                			<li>
	                				Stakeholders: <b>{{ $detail->stakeholders }}</b>
	                			</li>
	                			<li>
	                				Agency: <b>{{ $detail->agency }}</b>
	                			</li>
	                			<li>
	                				Beneficiary: <b>{{ $detail->beneficiaries }}</b>
	                			</li>
	                		</ul>
	                		<ul>
	                			<li>
	                				Priority Problem: <b>{{ $detail->problem }}</b>
	                			</li>
	                		</ul>
	                		<ul>
	                			<li>
	                				Persisting Gaps/Constraints: <b>{{ $detail->gaps }}</b>
	                			</li>
	                		</ul>
	                		<ul>
	                			<li>
	                				Possible Actions/Interventions: <b>{{ $detail->actions }}</b>
	                			</li>
                			</ul>
	                		<ul>
	                			<li>
	                				Time Frame: <b>from: {{ toDate($detail->timeframe_from) }}</b>
	                				 <b>to: {{  toDate($detail->timeframe_to) }}</b>
	                			</li>
	                			<li>
	                				Funding Requirement: <b>{{ number_format($detail->fund_amount,2, '.', '') }}</b>
	                			</li>
	                			<li>
	                				Fund Source: <b>{{  $detail->fund_source }}</b>
	                			</li>
	                			<li>
	                				Lead Group: <b>{{ $detail->lead_group }}</b>
	                			</li>
	                		</ul>
	                	
					</div>
					<div class="modal-footer">

						{{ HTML::linkRoute('mlprapdetails.edit', 'Edit', $detail->actionplan_id, array('class'=>'btn btn-info')) }}
						{{ HTML::linkRoute('mlprapdetails.delete', 'Delete', array($detail->actionplan_id) , array('class'=>'btn btn-danger',
															'onclick'=>"return confirm('Are you sure you want to delete this?');")) }}

					</div>
	            </div>
	        </div>
	    </div>

	@endforeach








@stop