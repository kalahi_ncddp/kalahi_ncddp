@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
			<div class="col-md-12">
				<h4>Add Action Plan</h4>
			</div>
		</div>
	  
	    <div class="row">
		    <div class="col-md-12">
				
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
				{{ Form::open(array('route' => 'mlprapdetails.store', 'method' => 'POST')) }}
				{{ Form::token() }}
				

					<div class="col-md-4 left-border">
						<div class="form-group">
							{{ Form::label('project_name', 'Project Name') }}
							{{ Form::text('project_name', Input::old('project_name'), array('class'=>'form-control', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('coverage', 'Areas Covered') }}
							{{ Form::text('coverage', Input::old('coverage'), array('class'=>'form-control', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('stakeholders', 'Stakeholders') }}
							{{ Form::text('stakeholders', Input::old('stakeholders'), array('class'=>'form-control', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('agency', 'Agency') }}

 							<div class="table-responsive agency-table">
                                    <table class="table table-striped table-bordered table-hover ">
                                        @foreach($agency as $agency_list)

                                                <div class = "form-control"><input type="checkbox" name="agency[]" value="{{$agency_list}}">&nbsp{{$agency_list}}</div>

                                        @endforeach
						  </table>
                                </div>
						</div>

						<div class="form-group">
							{{ Form::label('beneficiaries', 'Beneficiaries') }}
							{{ Form::text('beneficiaries', Input::old('beneficiaries'), array('class'=>'form-control', 'required')) }}
						</div>

					</div>


					<div class="col-md-4 left-border">
						<div class="form-group">
							{{ Form::label('problem', 'Priority Problem') }}
							{{ Form::textarea('problem', Input::old('problem'), array('class'=>'form-control', 'required', 'rows'=>'5')) }}
						</div>

						<div class="form-group">
							{{ Form::label('gaps', 'Persisting Gaps/Constraints') }}
							{{ Form::textarea('gaps', Input::old('gaps'), array('class'=>'form-control', 'required', 'rows'=>'5')) }}
						</div>

						<div class="form-group">
							{{ Form::label('actions', 'Possible Actions/Interventions') }}
							{{ Form::textarea('actions', Input::old('actions'), array('class'=>'form-control', 'required', 'rows'=>'5')) }}
						</div>
					</div>


					<div class="col-md-4 left-border">
						<div class="form-group">
							{{ Form::label('', 'Time Frame') }}
							<br>
							{{ Form::label('', 'From:') }}
							{{ Form::text('timeframe_from', Input::old('timeframe_from'), array('class'=>'form-control date')) }}
							{{ Form::label('', 'To:') }}
							{{ Form::text('timeframe_to', Input::old('timeframe_to'), array('class'=>'form-control date')) }}
						</div>

						<div class="form-group">
							{{ Form::label('fund_amount', 'Funding Requirement') }}
							{{ Form::currency( 'fund_amount', Input::old('fund_amount'), array('class'=>'form-control','step'=>'any', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('fund_source', 'Fund Source') }}
							{{ Form::select('fund_source', array('default' => 'Select fundsource') + $fundsources + array('others' => 'Others'), '',
											array('class' => 'form-control', 'id'=>'fund_source', 'name'=>'fund_source')) }}
							{{ Form::text('other_fund_source', Input::old('other_fund_source'), 
											array('class'=>'form-control', 'id'=>'other_fund_source', 'name'=>'other_fund_source','placeholder'=>'Please Specify')) }}
						</div>

						<!-- <div class="form-group">
							{{ Form::label('lead_group', 'Lead Group') }}
							{{ Form::select('lead_group1', array('default' => 'Select leadgroup') + $leadgroups + array('others' => 'Others'), '',
											array('class' => 'form-control', 'required', 'id'=>'lead_group', 'name'=>'lead_group')) }}
							{{ Form::text('other_lead_group', Input::old('other_lead_group'), 
											array('class'=>'form-control', 'id'=>'other_lead_group', 'name'=>'other_lead_group','placeholder'=>'Please Specify')) }}
						</div>
 -->
						<div class="form-group">
							{{ Form::label('lead_group', 'Lead Group') }}
							

 							<div class="table-responsive lead_group-table">
                                    <table class="table table-striped table-bordered table-hover ">
                                        @foreach($leadgroups as $leadgroups_list)

                                                <div class = "form-control"><input type="checkbox" name="lead_group[]" value="{{$leadgroups_list}}">&nbsp{{$leadgroups_list}}</div>

                                        @endforeach
						  </table>
                                </div>
						</div>					


						<!-- For MLPRAP redirect -->
						{{ Form::hidden('mlprap_id', $mlprap->action_id)}}

						{{ Form::submit('Add', array('class' => 'btn btn-primary')) }}				
						{{ HTML::linkRoute('mlprapdetails.show', 'Cancel', $mlprap->action_id, array('class' => 'btn btn-default')) }}
					</div>				

				{{ Form::close() }}
			</div>
		</div>



		<!--  Script for dropdown + others field   -->
		<script>
			$('#other_fund_source').hide();
			document.getElementById('other_fund_source').required = false;

//			$('#other_lead_group').hide();
//			document.getElementById('other_lead_group').required = false;
//			$('#other_agency').hide();
//            document.getElementById('other_lead_group').required = false;

			$('select[name=fund_source]').change(function () {
			    if ($(this).val() == 'others') {
			        $('#other_fund_source').show();
			        document.getElementById('other_fund_source').required = true;
			    } else {
			        $('#other_fund_source').hide();
			        document.getElementById('other_fund_source').required = false;
			    }
			});



		</script>

		
@stop


