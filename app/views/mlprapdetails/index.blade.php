@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
<a class="btn btn-default" href="{{ URL::to('mlprap') }}">Go Back</a>
    <hr />
    <div class="row">
	    <div class="col-md-12">
			<!-- Advanced Tables -->
	
	    	<div class="panel panel-default">
	    		<div class="panel-heading">
	    			Action Plans for {{ $mlprap[0]->for_year }} <!--Date Prepared: {{ date('m/d/Y', strtotime($mlprap[0]->date_prepared))}} -->
			        {{ HTML::linkRoute('mlprapdetails.create', 'Add New', $mlprap_id, array('class'=>'btn btn-primary pull-right')) }}
			        <div class="clearfix"></div>
	    		</div>

				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Project Name</th>
									<th>Areas Covered</th>
									<th>Priority Problem</th>
									<th>Details</th>
									
								</tr>
							</thead>
							<tbody>
								@foreach( $mlprap_details as $key=>$detail )
									<tr>
										<td>
											{{ $detail->project_name }}
										</td>
										<td>
											{{ $detail->coverage }}
										</td>
										<td>
											{{ $detail->problem }}
										</td>
										<td>
											{{ HTML::link('#mlprap_details_' . $key, 'View Details', array('data-toggle'=>'modal', 
																								   'data-target'=>'#mlprap_details_' . $key, 'class' => 'btn btn-success view')) }}
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>





	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>










@stop