<div class="modal fade" id="mibf_projects" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" style="width: 85%;">
  	{{ Form::open(array('route' => 'spi_profile.create', 'method' => 'POST', 'role' => 'form')) }}
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title">MIBF Project Proposals</h4>
	      </div>
	      <div class="modal-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover data-table">
							<thead>
								<tr>
									<td>Project Title</td>
									<td>Lead Barangay</td>
									<td>Grant</td>
									<td>LCC</td>
								</tr>
							</thead>
							<tbody>
	            	@foreach($project_proposals as $proposal)
	            		<tr>
	            			<td>
	            				{{ $proposal->project_name }}
	            				<input type="hidden" value="{{ $proposal->project_id }}"/>
	            			</td>
			            	<td>{{ Report::get_barangay($proposal->lead_brgy) }}</td>
			            	<td>
							    <?php
							        $kc_amount = isset($proposal->special->kc_amount) ? $proposal->special->kc_amount : $proposal->kc_amount;
							        $lcc_amount = isset($proposal->special->lcc_amount) ? $proposal->special->lcc_amount : $proposal->lcc_amount
							    ?>
								₱ {{ currency_format(floatval($kc_amount)) }}
							</td>
							<td>
								₱{{ currency_format(floatval($lcc_amount)) }}
							  </td>
		            	</tr>
		            @endforeach
		          </tbody>
						</table>
					</div>
					{{ Form::hidden('project_id')}}
				</div>
				<div class="modal-footer text-right">
					{{ Form::submit('Add Sub Project', array('class' => 'btn btn-primary')) }}
				</div>
			</div>
		{{ Form::close() }}
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("div.modal-body table tbody tr").css("cursor", "pointer");

		$("div.modal-body table tbody tr").click(function() {
			$(this).css("background-color", "#F3F4F5");
			$(this).siblings().css("background-color", "white");


			var project_id = $(this).find("td:first-child input[type='hidden']").val();
			$("form input[type='hidden'][name='project_id']").val(project_id);
		});
	});
</script>