 <div class="modal fade" id="add_bdcmeet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Past Year Activities (BA/BDC meetings)</h4>
            </div>
            <div class="modal-body">
            	{{ Form::open(array('url' => 'brgyprofile/submit_bdcpmeet', 'method' => 'POST', 'role' => 'form')) }}
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<tr>
				                <th>Year</th>
				                <td>
				                	{{ Form::text('year_source', Input::old('year_source'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>How many times BAs conducted</th>
				                <td>
				                	{{ Form::text('no_baconducted', Input::old('year_source'), array('class' => 'form-control', 'required')) }}
				            </tr>
				            <tr>
				                <th>How many times did the BDC meet?</th>
				                <td>
				                	{{ Form::text('no_bdcmeet', Input::old('no_bdcmeet'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>What sectors were present on BAs meetings?</th>
				                <td>
				                	{{ Form::textarea('sectors_rep', Input::old('sectors_rep'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>		            
						</table>

						<br>
						<table class="table table-striped table-bordered table-hover">
				            <thead> 
								<th colspan="2" style="text-align:center"> Average Number of attendees during BA meetings  </th>
							</thead>
				            <tr>
				            	<th>No. of barangay households </th>
				                <td>
				                	{{ Form::input('number', 'no_avghousehold', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>
				            </tr>
				            <tr>
				            	<th>No. of male residents </th>
				                <td>
				                	{{ Form::input('number', 'no_avgmale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>
				            </tr>
				            <tr>
				            	<th>No. of male residents </th>
				                <td>
				                	{{ Form::input('number', 'no_avgfemale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>
				            </tr>
						</table>

						<br>
						<table class="table table-striped table-bordered table-hover">
				            <thead> 
								<th colspan="2" style="text-align:center"> Average Number of attendees during BDC meetings  </th>
							</thead>
				            <tr>
				            	<th>No. of male BLGU officials </th>
				                <td>
				                	{{ Form::input('number', 'no_bdcmale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>
				            </tr>
				            <tr>
				            	<th>No. of female BLGU officials</th>
				                <td>
				                	{{ Form::input('number', 'no_bdcfemale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>
				            </tr>
				            <tr>
				            	<th>No. of male PO/CBO representatives </th>
				                <td>
				                	{{ Form::input('number', 'no_bdcmalerep', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>
				            </tr>
				            <tr>
				            	<th>No. of female PO/CBO representatives </th>
				                <td>
				                	{{ Form::input('number', 'no_bdcfemalerep', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>
				            </tr>
						</table>
					</div>
					<br>

				{{ Form::hidden('profile_id', $brgy_profile->profile_id)}}
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{ Form::reset('Reset', array('class' => 'btn btn-danger')) }}
				{{ Form::close() }}
			</div>
        </div>
    </div>
</div>