 <div class="modal fade" id="add_org" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add New Organization</h4>
            </div>
            <div class="modal-body">
            	{{ Form::open(array('url' => 'brgyprofile/submit_org', 'method' => 'POST', 'role' => 'form')) }}
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<tr>
				                <th>Name</th>
				                <td>
				                	{{ Form::text('org_name', Input::old('org_name'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Type of Organization</th>
				                <td>
				                	{{ Form::select('org_type', $orgtype_list, Input::old('org_type'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Formal (Registered)</th>
				                <td>
				                	{{ Form::select('is_registered', array(0 => 'No', 1 => 'Yes'), Input::old('is_registered'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>LGU-Accredited</th>
				                <td>
				                	{{ Form::select('is_lguaccredited', array(0 => 'No', 1 => 'Yes'), Input::old('is_lguaccredited'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Advocacy/Thrust</th>
				                <td>
				                	{{ Form::text('advocacy', Input::old('advocacy'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Area of operation</th>
				                <td>
				                	{{ Form::text('area_operation', Input::old('area_operation'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Marginalized sectors represented</th>
				                <td>
				                	{{ Form::text('sector_rep', Input::old('sector_rep'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>			            
						</table>

						<br>
						<table class="table table-striped table-bordered table-hover">
				            <tr>
				            	<th></th>
				                <th>Male</th>
				                <th>Female</th>
				            </tr>
				            <tr>
				            	<th>No. of members from the barangay </th>
				                <td>
				                	{{ Form::input('number', 'no_memmale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>
				                <td>
				                	{{ Form::input('number', 'no_memfemale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				            	<th>No. of IP members from the barangay </th>
				                <td>
				                	{{ Form::input('number', 'no_ipmale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>
				                <td>
				                	{{ Form::input('number', 'no_ipfemale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				            	<th>No. of Officers </th>
				                <td>
				                	{{ Form::input('number', 'no_ofcmale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>	
				                <td>
				                	{{ Form::input('number', 'no_ofcfemale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr>
						</table>
					</div>
					<br>

				{{ Form::hidden('profile_id', $brgy_profile->profile_id)}}
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{ Form::reset('Reset', array('class' => 'btn btn-danger')) }}
				{{ Form::close() }}
			</div>
        </div>
    </div>
</div>