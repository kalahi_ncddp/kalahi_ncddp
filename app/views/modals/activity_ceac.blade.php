<div class="modal fade" id="ceac_activity" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Activities</h4>
            </div>
            <div class="modal-body">
           		<table class="table table-bordered">
           			<thead>
           				<tr>
           					<td>Activity</td> 
           					<td>Cycle_id</td>
           					<td>Funded By</td>
           					<td>Start Date</td>   
           					<td>End Date</td> 
           			
           				</tr>	
           			</thead>
           			<tbody></tbody>
           		</table>
			</div>
        </div>
    </div>
</div>
