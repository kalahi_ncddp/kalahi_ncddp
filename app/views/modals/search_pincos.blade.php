    <div class="modal fade" id="intake" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style=" width: 85%;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Search Pincos and Grievances</h4>
          </div>
          <div class="modal-body">
            <table class="table table-bordered">
              <thead>
              <tr>
                    <td class="hidden">Issue No.</td>
                    <td>Activity Id</td>
                    <td>Raise By</td>
                    <td>Issue</td>
                    <td>Action</td>
                    <td>Option</td>
                 </tr>
              </thead>
              <tbody>
                @foreach($activity_issues as $issues)
                    <tr>
                        <td class="hidden"> {{ $issues->issue_no }}</td>
                        <td> {{ $issues->activity_id }}</td>
                        <td> {{ $issues->raise_by }}</td>
                        <td> {{ $issues->issue }}</td>
                        <td>{{ $issues->action }}</td>
                        <td>
                            <span class="btn btn-success select-pincos" data-id="{{ $issues->activity_id }}" data-issue="{{ $issues->issue_no }}">select</span>
                        </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>