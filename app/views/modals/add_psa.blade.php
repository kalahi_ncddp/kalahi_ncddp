<div class="modal fade" id="add_psa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

              <h3>Add PSA from Community Training</h3>
            </div>
            <div class="modal-body">
              {{ Datatable::table()->addColumn( 'Training Title','cycle id','program id','Barangay' ,'date_conducted','option')->setUrl(route('api.commtraining'))->render(); }}
		      	</div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
