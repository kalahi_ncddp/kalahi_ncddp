 <div class="modal fade" id="pincos_grievances" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 85%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">PINCOs and Grievances</h4>
                    </div>
                    <div class="modal-body">
                    	 {{ Form::open(array('url' => URL::to('bassembly').'/pincos', 'method' => 'POST', 'role' => 'form')) }}

								<div class="table-responsive">
									<table id="items1" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<td>Raise By</td>
												<td>Issue</td>
												<td>Action</td>
												<td>option</td>
											</tr>
										</thead>
						            	@foreach($issues as $issue)
						            		<tr>
						            			<td>
								            		
								            		<input type="text" class="form-control" value="{{$issue->raise_by}}" name="raise[]" oninput="checkduplicate1(this)" placeholder="Raised By">
								            	</td>
								            	<td>
								            		<textarea type="text" class="form-control" value="{{$issue->issue}}" name="issue[]" oninput="checkduplicate1(this)" placeholder="Issue">{{$issue->issue}}</textarea>
								            	</td>
								            	<td>
								            		<textarea type="text" class="form-control" value="{{$issue->action}}" name="action[]" oninput="checkduplicate1(this)" placeholder="Action">{{$issue->action}}</textarea>
								            	</td>
								            	<td>
							            			<button type="button" class="btn fa fa-trash-o delete"></button>
								            	</td>
						            			
							            	</tr>
							            @endforeach
							           <tr>
							           	<td>
								            <input type="text" class="form-control" name="raise[]" oninput="checkduplicate1(this)" placeholder="Raised By" required>
								        </td>
								        <td>
								            <textarea type="text" class="form-control" name="issue[]" oninput="checkduplicate1(this)" placeholder="Issue" required></textarea>
								        </td>
								        <td>
								        	<textarea type="text" class="form-control" name="action[]" oninput="checkduplicate1(this)" placeholder="Action" ></textarea>
								        </td>
								        <td>
								        	<button type="button" class=" btn fa fa-trash-o delete"></button>
								        </td>
								            
							           </tr>
							          
									</table>
								</div>
									<br>
        				<?php

        					$activity_id = "";
        					if( isset($brgy_assembly->activity_id) ){
        						$activity_id =  $brgy_assembly->activity_id;
        					}
        					else if( isset($psa->activity_id) )
        					{
        						$activity_id =  $psa->activity_id;
        					}
        					else if( isset( $mibf->activity_id )){
        						$activity_id = $mibf->activity_id ;
        					}
        					 else if( isset( $training->reference_no )){
        						$activity_id = $training->reference_no ;
        					}
        					
        				?>
						{{ Form::hidden('activity_id',$activity_id)}}
						{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				        <button id="add1" class="btn btn-olive" type="button" >
				        	<i class="fa fa-plus"></i> Add New Record
				        </button>
						{{ Form::close() }}
					</div>
	            </div>
            </div>
        </div>

        <script type="text/javascript">

				$("#add1").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items1").append('<tr><td>'+
						'<input type="text" class="form-control" name="raise[]" oninput="checkduplicate1(this)" placeholder="Raised By" required></td>'+
						'<td><textarea type="text" class="form-control" name="issue[]" placeholder="Issue"" required></textarea></td>'+
						'<td><textarea type="text" class="form-control" name="action[]" placeholder="Action" required></textarea></td>'+
						'<td><button type="button" class=" btn fa fa-trash-o delete"></button></td></tr>');
				});
				$("body").on("click", ".delete",function(){
					$(this).parent().parent().remove();
				});

				$('#pincos_grievances').on('hidden.bs.modal', function (e) {
				  window.location.reload();
				})
        </script>