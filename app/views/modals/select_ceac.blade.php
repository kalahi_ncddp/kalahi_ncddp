    <div class="modal fade" id="select-ceac" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" >
      <div class="modal-dialog" >
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Select Cycle and Program </h4>
          </div>
          <div class="modal-body">
            <table class="table">
                <tr>
                    <th>Program</th>
                    <th>Cycle</th>
                </tr>
                <tr>
                    <td>
                        {{ Form::select('program',$program,'',['class'=>'form-control program']) }}
                    </td>
                    <td>
                        {{ Form::select('cycle_id',$all_cycles,'',['class'=>'form-control cycle']) }}
                    </td>

                    <td>
                        <span class="btn btn-success proceed">Proceed</span>
                    </td>
                </tr>
            </table>


          </div>
        </div>
      </div>
    </div>