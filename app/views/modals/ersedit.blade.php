<div class="modal fade" id="ers_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Worker</h4>
            </div>
            <div class="modal-body">
                <h5>Worker Rate and Attendance (*Formula : Daily rate * Days work + hourly rate * hours work )</h5>
                <table class="table further_details" border="0" style="padding-left:50px;" data-rowid="">
                    <thead>

                    </thead>
                    <tbody>
                        <tr>
                            <th style="width: 15%;">Work</th>
                            <td>{{ Form::select('',$natureofworklists,'',['class'=>'form-control in-naturework','id'=>'in-natureofwork']) }} </td>
                        </tr>
                        <tr>
                            <th style="width: 15%;">Daily Rate:</th>
                            <td><input type="text" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-dailyrate in-currency" value="0">
                            </td>
                            <th style="width: 15%;">Days Worked:</th>
                            <td><input type="text" value="0" style="width: 90%;" min="0" step="0.01" class="in-daysworked in-time" value="0">
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 15%;">Hourly Rate:</th>
                            <td><input type="text" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-hourlyrate in-currency" value="0">
                            </td>
                            <th style="width: 15%;">Hours Worked:</th>
                            <td><input type="text" value="0" style="width: 90%;" min=0 step="0.01" class="in-hoursworked in-time" value="0">
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 15%;">Total Labor:</th>
                            <td colspan="3"><input type="text" style="width: 95%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-total" readonly disabled value="0"></td>
                        </tr>
                        <tr>
                            <th style="width: 15%;">Plan Cash:</th>
                                <td><input type="text" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-plancash in-currency" value="0">
                                </td>
                            <th style="width: 15%;">Actual Cash:</th>
                            <td><input type="text" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-actualcash in-time" value="0">
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 15%;">Plan Bayanihan:</th>
                                <td><input type="text" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-planbayanihan in-currency" value="0">
                                </td>
                            <th style="width: 15%;">Actual Bayanihan:</th>
                            <td><input type="text" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-actualbayanihan in-time" value="0">
                            </td>
                        </tr>
                    </tbody>
                </table>

			</div>

			<div class="modal-footer">
			     <a href="#" class="btn btn-sm btn-primary pull-right spi-ers_editersrecords">
                                                        <i class="glyphicon glyphicon-save"></i> Apply Changes
                                                    </a>
			</div>
        </div>
    </div>
</div>
