 <div class="modal fade" id="add_proj" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add New Organization</h4>
            </div>
            <div class="modal-body">
            	{{ Form::open(array('url' => 'brgyprofile/submit_brgydevproj', 'method' => 'POST', 'role' => 'form')) }}
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<tr>
				                <th>Project</th>
				                <td>
				                	{{ Form::text('proj_name', Input::old('proj_name'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Nature of Project</th>
				                <td>
				                	{{ Form::text('proj_nature', Input::old('proj_nature'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Project Duration</th>
				                <td>
				                	{{ Form::text('proj_duration', Input::old('proj_duration'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Location</th>
				                <td>
				                	{{ Form::text('location', Input::old('location'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Scope/Coverage/Unit or physical target</th>
				                <td>
				                	{{ Form::text('scope_target', Input::old('scope_target'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Cost of Project</th>
				                <td>
				                	{{ Form::text('proj_cost', Input::old('proj_cost'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Source of Funds</th>
				                <td>
				                	{{ Form::text('fund_source', Input::old('fund_source'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr> 
				            <tr>
				                <th>Implementing Agency</th>
				                <td>
				                	{{ Form::text('impl_agency', Input::old('impl_agency'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Cost Sharing (%)</th>
				                <td>
				                	{{ Form::text('cost_sharing', Input::old('cost_sharing'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>        
						</table>

						<br>
						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:center"> Number of Beneficiaries  </th>
							</thead>
							<tr>
				                <th>Male</th>
				                <td>
				                	{{ Form::input('number', 'no_benemale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Female </th>
				                <td>
				                	{{ Form::input('number', 'no_benefemale', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Pantawid Pamilya HHS</th>
				                <td>
				                	{{ Form::input('number', 'no_pphousehold', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>SLP HHS</th>
				                <td>
				                	{{ Form::input('number', 'no_slphousehold', 0, array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr>
						</table>
					</div>
					<br>

				{{ Form::hidden('profile_id', $brgy_profile->profile_id)}}
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{ Form::reset('Reset', array('class' => 'btn btn-danger')) }}
				{{ Form::close() }}
			</div>
        </div>
    </div>
</div>