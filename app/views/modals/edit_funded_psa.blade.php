<div class="modal fade" id="spi-fnddpsa_modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit PSA Priority</h4>
                    </div>
                    <div class="modal-body">
                    <form method="POST" action="{{ URL::to('spi-fnddpsa/2') }}" accept-charset="UTF-8">
                            <input name="_method" type="hidden" value="PUT">
    						<div class="form-group col-xs-12 col-md-12">
    						    <input type="text" name="id" class="hidden id" value=""/>
								<label for="in-problem">Problem</label>
								<input type="text" name="in-problem" id="in-problem" class="form-control" readonly/>
							</div>

							<div class="form-group  col-xs-12 col-sm-12">
								<label for="in-solution">Solution</label>
								<input type="text" name="in-solution" id="in-solution" class="form-control" readonly/>
							</div>

							<div class="form-group  col-xs-12 col-sm-12">
								<label for="in-solution-categ">Solution Category</label>
								<input type="text" class="form-control" id="in-solution-categ" name="sol-category" readonly="readonly" disabled="disabled">
							</div>
                			<div class="form-group  col-xs-12 col-sm-12">
                                <label for="in-solution-categ">KC-NCDDP Sub Project </label>
                                {{ Form::text('kc_ncddp_sub_project_id','',[ 'class'=>'form-control' , 'id'=>'spi','readonly' ]) }}
                            </div>
                            <div class="form-group  col-xs-12 col-sm-12">
                                <label for="in-solution-categ"> Sub Project </label>
                                <input type="text" name="sub_project" class="form-control" placeholder="If not kc-ncddp label the sub project"  >
                            </div>
                            <div class="form-group  col-xs-12 col-sm-12">
                                <label for="in-solution-categ">KC Fund</label>
                                {{ Form::currency('edit_kc_fund','',['class'=>'form-control','id'=>"kc_ncddp"]) }}
                            </div>

                            <div class="form-group  col-xs-12 col-sm-12">
                                <label for="in-solution-categ">Funder</label>
                                <input type="text" name="funder" class="form-control" placeholder="If not kc nddp project"  >
                            </div>
                            <div class="form-group  col-xs-12 col-sm-12">
                                <label for="in-solution-categ">Fund</label>
                                {{ Form::currency('edit_fund','',['class'=>'form-control','id'=>"kc_ncddp","placeholder"=>"Fund from the donor"]) }}
                            </div>
                            <button class="btn btn-success">Submit</button>
						    <div class="clearfix"></div>
						</form>
					</div>
					<div class="modal-footer">
						{{--<a id="spi-fnddpsa_modal_form_submit" href="" class="btn btn-success">--}}
							{{--<i class="glyphicon glyphicon-floppy-disk"></i> Submit--}}
						{{--</a>--}}
						<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
					</div>
	            </div>
            </div>
        </div>