<div class="modal fade" id="set_list" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" style="width: 85%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">List of Sub Project SET</h4>
      </div>
      <div class="modal-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover data-table">
						<thead>
							<tr>
								<td>Date of Evaluation</td>
								<td>Final Score</td>
								<td>Adjectival Rating</td>
								<td>Details</td>
							</tr>
						</thead>
						<tbody>
            	@foreach($set_list as $set)
            		<tr>
            			<td>{{ toDate($set->date_evaluated) }}</td>
		            	<td>{{ $set->final_rating() }}</td>
		            	<td>{{ $set->adjectival_rating() }}</td>
		            	<td>
		            		{{ HTML::linkRoute('spi_profile.set.show', 'View Details',
											array($profile->project_id, $set->id), array('class' => 'btn btn-success btn')) }}
		            	</td>
	            	</tr>
	            @endforeach
	          </tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer text-right">
				{{ HTML::linkRoute('spi_profile.set.create', 'Add SET',
					array($profile->project_id), array('class' => 'btn btn-primary btn')) }}
			</div>
		</div>
  </div>
</div>