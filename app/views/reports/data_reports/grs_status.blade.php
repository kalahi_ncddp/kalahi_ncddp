@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop


@section('content')
<div class="row">	
	<div class="col-md-12">
		<div class="col-md-4">
			<label for="">Program</label>
			{{ Form::select('program',$program,'',array('class'=>'form-control program') ) }}
		</div>
		<div class="col-md-4">
			<label for="">Cycle</label>
			{{ Form::select('cycle',$cycle,'',array('class'=>'form-control cycle') ) }}
		</div>

		<div class="col-md-4">
			<button class="btn bg-olive generate">Generate</button>
		</div>
	</div>
	<div class="clearfix"></div>
	<br>
	
	<!-- frame for generated pdf if chrome user? -->
	<div class="col-md-12">
		<iframe src="{{ URL::to('reports/grs_status/generate') }}?" frameborder="0" style="width: 88%;display: block;height: 66em;margin: 0 auto;">
		</iframe>
	</div>
</div>
<script>
	$(function(){
		$('.generate').on('click',function(){
			var cycle = $('.cycle').val(),
				program = $('.program').val(),
				prov_psgc = $('.prov_psgc').val(),
				muni_psgc = $('.muni_psgc').val();

			$('iframe').attr('src','{{ URL::to('reports/grs_status/generate') }}?cycle_id='+cycle+'&program_id='+program+'&prov_psgc='+prov_psgc+'&muni_psgc='+muni_psgc);
		});
	});
</script>
@stop