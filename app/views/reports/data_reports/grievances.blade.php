@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop


@section('content')
<div class="row">	
	<div class="col-md-12">
		<div class="col-md-6">
			<label for="">Program</label>
			{{ Form::select('program',$program,'',array('class'=>'form-control program') ) }}
		</div>
		<div class="col-md-6">
			<label for="">Cycle</label>
			{{ Form::select('cycle',$cycle,'',array('class'=>'form-control cycle') ) }}
		</div>
		<br>
		<div class="col-md-6">
        	<label for="">Status</label>              
			<select class="form-control resolution" required="required" name="resolution_status">
				<option value="" selected="selected">Select Status</option>
				<option value="On Going">On Going</option>
				<option value="Pending">Pending</option>
				<option value="Resolved">Resolved</option>
			</select>
		</div>
		
		<div class="col-md-6">
        	<label for="">Grievance Intake</label>
            <select class="form-control intake" required="required" name="grievance_form">
            <option value="" selected="selected">Select Form</option>
            <option value="intake">Regular Intake</option>
            <option value="pincos">Pincos</option></select>
        </div>
        <br>
        <div class="col-md-6">   
        	<label for="">Type</label>
        	<div class="form-inline">
        		<input class="form-control type" name="type[]" type="checkbox" value="A" id="type" checked> A &nbsp;&nbsp;
        		<input class="form-control type" name="type[]" type="checkbox" value="B" id="type" checked> B &nbsp;&nbsp;
        		<input class="form-control type" name="type[]" type="checkbox" value="C" id="type" checked> C &nbsp;&nbsp;
        		<input class="form-control type" name="type[]" type="checkbox" value="D" id="type" checked> D &nbsp;&nbsp;
        	</div>
        </div>        
		<div class="col-md-6">
			
			<button class="btn bg-olive generate">Generate</button>
		</div>
	</div>
	<div class="clearfix"></div>
	<br>
	
	<!-- frame for generated pdf if chrome user? -->
	<div class="col-md-12">
		<iframe src="{{ URL::to('reports/grievances/generate') }}?" frameborder="0" style="width: 100%;display: block;height: 50em;margin: 0 auto;">
		</iframe>
	</div>
</div>
<script>
	$(function(){
		
		$('.generate').on('click',function(){
			var cycle = $('.cycle').val(),
				program = $('.program').val(),
				resolution_status = $('.resolution').val(),
				grievance_intake = $('.intake').val()
				
				var checkedValues = $('input:checkbox:checked').map(function() {
    				return this.value;
				}).get();
			
			$('iframe').attr('src','{{ URL::to('reports/grievances/generate') }}?cycle_id='+cycle+'&program_id='+program+'&resolution_status='+resolution_status+'&grievance_intake='+grievance_intake+'&type='+checkedValues);
		});
	});
</script>
@stop