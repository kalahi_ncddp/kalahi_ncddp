@extends('layouts.default')

@section('username')
  {{ $username }}
@stop

@section('sidebar')
    <li>
      <a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop


@section('content')
<div class="row">
  <div class="col-md-4">
    <label for="">Cycle</label>
    {{ Form::select('cycle',$cycle,'',array('class'=>'form-control cycle') ) }}
  </div>
</div>
<div class="row" style="margin-top:10px;">
  <div class="col-md-4">
    <button class="btn bg-olive generate">Generate</button>
  </div>
</div>
<div class="row" style="margin-top:20px;">
  <!-- frame for generated pdf if chrome user? -->
  <div class="col-md-12">
    <iframe src="{{ URL::to('reports/mibf/generate') }}?" frameborder="0" style="width: 88%;display: block;height: 66em;margin: 0 auto;">
    </iframe>
  </div>
</div>
<script>
  $(function(){
    $('.generate').on('click',function(){
      var cycle = $('.cycle').val();
      $('iframe').attr('src','{{ URL::to('reports/mibf/generate') }}?cycle='+cycle);
    });
  });
</script>
@stop