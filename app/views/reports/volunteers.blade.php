@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
    <li>
    	<a href="{{ URL::to('reports/brgy_assembly') }}"><i class="fa fa-table fa-3x"></i> Brgy. Assembly</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/brgy_assembly2') }}"><i class="fa fa-table fa-3x"></i> Brgy. Assembly 2</a>
    </li>
    <li>
        <a class="active-menu" href="{{ URL::to('reports/volunteers') }}"><i class="fa fa-table fa-3x"></i> Volunteers</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/comm_trainings') }}"><i class="fa fa-table fa-3x"></i> Comm. Trainings</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/mun_trainings') }}"><i class="fa fa-table fa-3x"></i> Mun. Trainings</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/psa1') }}"><i class="fa fa-table fa-3x"></i> PSA 1</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/psa2') }}"><i class="fa fa-table fa-3x"></i> PSA 2</a>
    </li>
@stop

@section('content')
	
		<div class="row">
			
	        <div class="col-md-12">
	    		<h2>Volunteers</h2>    
	        </div>
	    </div>
	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
				<!-- Advanced Tables -->
		    	<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Records
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>Region</th>
										<th>Province</th>
										<th>Municipality</th>
										<th>Cycle</th>
										<th>KC Group</th>
										<th>Barangay</th>
										<th>Last Name</th>
										<th>First Name</th>
										<th>View Details</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data as $volunteer)
										<?php $beneficiary = Report::get_beneficiary($volunteer->beneficiary_id) ?>
									<tr>
										<td>
											{{ $region = Report::get_region($volunteer->psgc_id) }}
										</td>
										<td>
											{{ $province = Report::get_province($volunteer->psgc_id) }}
										</td>
										<td>
											{{ $municipality = Report::get_municipality($volunteer->psgc_id) }}
										</td>
										<td>
											{{ $volunteer->cycle_id }}
										</td>
										<td>
										</td>
										<td>
											{{ $barangay = Report::get_barangay($volunteer->psgc_id) }}
										</td>
										<td>
											{{ $beneficiary->lastname }}
										</td>
										<td>
											{{ $beneficiary->firstname }}
										</td>
										<td>
				                            <button class="btn btn-success btn" data-toggle="modal" data-target="{{'#myModal'.$volunteer->beneficiary_id}}">
				                              View Details
				                            </button>
				                            <div class="modal fade" id="{{'myModal'.$volunteer->beneficiary_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										        <div class="modal-dialog">
										            <div class="modal-content">
										                <div class="modal-header">
										                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										                    <h4 class="modal-title" id="myModalLabel">Full Details</h4>
										                </div>
										                <div class="modal-body">
										                    <div class="panel panel-default">
													    		<div class="panel-heading">
													    			Details
													    		</div>
																<div class="panel-body">
																	<div class="table-responsive">
																		<table class="table table-striped table-bordered table-hover">
																            <tr>
																                <th>Region</th>
																                <td>{{ $region }}</td>
																            </tr>
																            <tr>
																                <th>Province</th>
																                <td>{{ $province }}</td>
																            </tr>
																            <tr>
																                <th>Municipality</th>
																                <td>{{ $municipality }}</td>
																            </tr>
																            <tr>
																                <th>Cycle</th>
																                <td>{{ $volunteer->cycle_id }}</td>
																            </tr>
																            <tr>
																                <th>KC Group</th>
																                <td></td>
																            </tr>
																            <tr>
																                <th>Barangay</th>
																                <td>{{ $barangay }}</td>
																            </tr>
																            <tr>
																                <th>Last Name</th>
																                <td>{{ $beneficiary->lastname }}</td>
																            </tr>
																            <tr>
																                <th>First Name</th>
																                <td>{{ $beneficiary->firstname }}</td>
																            </tr>
																            <tr>
																                <th>Sex</th>
																                <td>{{ $beneficiary->sex }}</td>
																            </tr>
																            <tr>
																                <th>Age</th>
																                <td>{{ Report::get_age($beneficiary->birthdate) }}</td>
																            </tr>
																            <tr>
																                <th>4Ps Beneficiary</th>
																                @if ($volunteer->is_ppbene == 1)
																					<td> Yes
																						<span class="icon-box bg-color-green set-icon">
														                    				<i class="fa fa-check-square"></i>
														                				</span>
																					</td>
																				@else
																					<td> No
																						<span class="icon-box bg-color-red set-icon">
														                    				<i class="fa fa-times"></i>
														                				</span>
																					</td>
																				@endif
																            </tr>
																            <tr>
																                <th>4Ps Parent Leader</th>
															                	@if ($volunteer->is_ppleader == 1)
																					<td> Yes
																						<span class="icon-box bg-color-green set-icon">
														                    				<i class="fa fa-check-square"></i>
														                				</span>
																					</td>
																				@else
																					<td> No
																						<span class="icon-box bg-color-red set-icon">
														                    				<i class="fa fa-times"></i>
														                				</span>
																					</td>
																				@endif
																            <tr>
																                <th>SLP Beneficiary</th>
																                @if ($volunteer->is_slpbene == 1)
																					<td> Yes
																						<span class="icon-box bg-color-green set-icon">
														                    				<i class="fa fa-check-square"></i>
														                				</span>
																					</td>
																				@else
																					<td> No
																						<span class="icon-box bg-color-red set-icon">
														                    				<i class="fa fa-times"></i>
														                				</span>
																					</td>
																				@endif
																            </tr>
																            <tr>
																                <th>SLP Parent Leader</th>
																                @if ($volunteer->is_slpleader == 1)
																					<td> Yes
																						<span class="icon-box bg-color-green set-icon">
														                    				<i class="fa fa-check-square"></i>
														                				</span>
																					</td>
																				@else
																					<td> No
																						<span class="icon-box bg-color-red set-icon">
														                    				<i class="fa fa-times"></i>
														                				</span>
																					</td>
																				@endif
																            </tr>
																            <tr>
																                <th>BSPMC Position</th>
																                @if ($volunteer->is_bspmc == 1)
																					<td> Yes
																						<span class="icon-box bg-color-green set-icon">
														                    				<i class="fa fa-check-square"></i>
														                				</span>
																					</td>
																				@else
																					<td> No
																						<span class="icon-box bg-color-red set-icon">
														                    				<i class="fa fa-times"></i>
														                				</span>
																					</td>
																				@endif
																            </tr>
																            <tr>
																                <th>Name of Committee Membership</th>
																                <td></td>
																            </tr>
																            <tr>
																                <th>Position in Committee Membership</th>
																                <td></td>
																            </tr>
																		</table>
																	</div>
																</div>
															</div>

															<div class="panel panel-default">
													    		<div class="panel-heading">
													    			Reviewed By
													    		</div>
																<div class="panel-body">
																	<div class="table-responsive">
																		<table class="table table-striped table-bordered table-hover">
																			<thead>
																				<tr>
																					<th>ACT</th>
																					<th>SRPMO</th>
																					<th>RPMO</th>
																					<th>NPMO</th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					@if ($volunteer->act_reviewed == 1)
																						<td> Yes
																							<span class="icon-box bg-color-green set-icon">
															                    				<i class="fa fa-check-square"></i>
															                				</span>
																						</td>
																					@else
																						<td> No
																							<span class="icon-box bg-color-red set-icon">
															                    				<i class="fa fa-times"></i>
															                				</span>
																						</td>
																					@endif
																					@if ($volunteer->srpmo_reviewed == 1)
																						<td> Yes
																							<span class="icon-box bg-color-green set-icon">
															                    				<i class="fa fa-check-square"></i>
															                				</span>
																						</td>
																					@else
																						<td> No
																							<span class="icon-box bg-color-red set-icon">
															                    				<i class="fa fa-times"></i>
															                				</span>
																						</td>
																					@endif
																					@if ($volunteer->rpmo_reviewed == 1)
																						<td> Yes
																							<span class="icon-box bg-color-green set-icon">
															                    				<i class="fa fa-check-square"></i>
															                				</span>
																						</td>
																					@else
																						<td> No
																							<span class="icon-box bg-color-red set-icon">
															                    				<i class="fa fa-times"></i>
															                				</span>
																						</td>
																					@endif
																					@if ($volunteer->npmo_reviewed == 1)
																						<td> Yes
																							<span class="icon-box bg-color-green set-icon">
															                    				<i class="fa fa-check-square"></i>
															                				</span>
																						</td>
																					@else
																						<td> No
																							<span class="icon-box bg-color-red set-icon">
															                    				<i class="fa fa-times"></i>
															                				</span>
																						</td>
																					@endif
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
										                </div>
										                <div class="modal-footer">
										                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										                </div>
										            </div>
										        </div>
										    </div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop