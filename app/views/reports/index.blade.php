@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
    <li>
    	<a href="{{ URL::to('reports/brgy_assembly') }}"><i class="fa fa-table fa-3x"></i> Brgy. Assembly</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/brgy_assembly2') }}"><i class="fa fa-table fa-3x"></i> Brgy. Assembly 2</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/volunteers') }}"><i class="fa fa fa-table fa-3x"></i> Volunteers</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/comm_trainings') }}"><i class="fa fa fa-table fa-3x"></i> Comm. Trainings</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/mun_trainings') }}"><i class="fa fa fa-table fa-3x"></i> Mun. Trainings</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/psa1') }}"><i class="fa fa fa-table fa-3x"></i> PSA 1</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/psa2') }}"><i class="fa fa fa-table fa-3x"></i> PSA 2</a>
    </li>
@stop

@section('content')

	<div class="container">
        <div class="text-center">
            <img src="{{ URL::to('public/img/KALAHI_CIDSS-NCDDP_Logo3.jpg') }}" align="middle" width="200px"/>
            <h1><strong>KC-NCDDP Database System <i class="small"> {{$version}} </i></strong><h1>

            
        </div>

        
    </div> 

    @if(Session::get('notice'))
        @include('modals.notice')

        <script>
            $('#notice').modal('show')
        </script>
    @endif
@stop
