@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
    <li>
    	<a href="{{ URL::to('reports/brgy_assembly') }}"><i class="fa fa-table fa-3x"></i> Brgy. Assembly</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/brgy_assembly2') }}"><i class="fa fa-table fa-3x"></i> Brgy. Assembly 2</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/volunteers') }}"><i class="fa fa-table fa-3x"></i> Volunteers</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/comm_trainings') }}"><i class="fa fa-table fa-3x"></i> Comm. Trainings</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/mun_trainings') }}"><i class="fa fa-table fa-3x"></i> Mun. Trainings</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/psa1') }}"><i class="fa fa-table fa-3x"></i> PSA 1</a>
    </li>
    <li>
        <a class="active-menu" href="{{ URL::to('reports/psa2') }}"><i class="fa fa-table fa-3x"></i> PSA 2</a>
    </li>
@stop

@section('content')
	
		<div class="row">
			
	        <div class="col-md-12">
	    		<h2>PSA 2</h2>    
	        </div>
	    </div>
	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
				<!-- Advanced Tables -->
		    	<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Records
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>Region</th>
										<th>Province</th>
										<th>Municipality</th>
										<th>Cycle</th>
										<th>KC Group</th>
										<th>Barangay</th>
										<th>Date Conducted</th>
										<th>View Details</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data as $brgy_assembly)
									<tr>
										<td>
											{{ $region = Report::get_region($brgy_assembly->psgc_id) }}
										</td>
										<td>
											{{ $province = Report::get_province($brgy_assembly->psgc_id) }}
										</td>
										<td>
											{{ $municipality = Report::get_municipality($brgy_assembly->psgc_id) }}
										</td>
										<td>
											{{ $brgy_assembly->cycle_id }}
										</td>
										<td>
										</td>
										<td>
											{{ $barangay = Report::get_barangay($brgy_assembly->psgc_id) }}
										</td>
										<td>
										</td>
										<td>
				                            <button class="btn btn-success btn" data-toggle="modal" data-target="{{'#myModal'.$brgy_assembly->activity_id}}">
				                              View Details
				                            </button>
				                            <div class="modal fade" id="{{'myModal'.$brgy_assembly->activity_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										        <div class="modal-dialog">
										            <div class="modal-content">
										                <div class="modal-header">
										                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										                    <h4 class="modal-title" id="myModalLabel">Full Details</h4>
										                </div>
										                <div class="modal-body">
										                    <div class="panel panel-default">
													    		<div class="panel-heading">
													    			Details
													    		</div>
																<div class="panel-body">
																	<div class="table-responsive">
																		<table class="table table-striped table-bordered table-hover">
																            <tr>
																                <th>Region</th>
																                <td>{{ $region }}</td>
																            </tr>
																            <tr>
																                <th>Province</th>
																                <td>{{ $province }}</td>
																            </tr>
																            <tr>
																                <th>Municipality</th>
																                <td>{{ $municipality }}</td>
																            </tr>
																            <tr>
																                <th>Cycle</th>
																                <td>{{ $brgy_assembly->cycle_id }}</td>
																            </tr>
																            <tr>
																                <th>KC Group</th>
																                <td></td>
																            </tr>
																            <tr>
																                <th>Barangay</th>
																                <td>{{ $barangay }}</td>
																            </tr>
																            <tr>
																                <th>Date Conducted</th>
																                <td></td>
																            </tr>
																            <tr>
																                <th>Rank</th>
																                <td></td>
																            </tr>
																            <tr>
																                <th>Priority Problem</th>
																                <td></td>
																            </tr>
																            <tr>
																                <th>Problem Category</th>
																                <td></td>
																            </tr>
																            <tr>
																                <th>Proposed Solution</th>
																                <td></td>
																            </tr>
																            <tr>
																                <th>Proposed Solution Cateogry</th>
																                <td></td>
																            </tr>
																		</table>
																	</div>
																</div>
															</div>

															<div class="panel panel-default">
													    		<div class="panel-heading">
													    			Reviewed By
													    		</div>
																<div class="panel-body">
																	<div class="table-responsive">
																		<table class="table table-striped table-bordered table-hover">
																			<thead>
																				<tr>
																					<th>ACT</th>
																					<th>SRPMO</th>
																					<th>RPMO</th>
																					<th>NPMO</th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td>
																						
																					</td>
																					<td>
																						
																					</td>
																					<td>
																						
																					</td>
																					<td>
																						
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
										                </div>
										                <div class="modal-footer">
										                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										                </div>
										            </div>
										        </div>
										    </div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop