@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
    <li>
    	<a class="active-menu" href="{{ URL::to('reports/brgy_assembly') }}"><i class="fa fa-home fa-3x"></i> Brgy. Assembly</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/brgy_assembly2') }}"><i class="fa fa-users fa-3x"></i> Brgy. Assembly 2</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/volunteers') }}"><i class="fa fa-list-ol fa-3x"></i> Volunteers</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/comm_trainings') }}"><i class="fa fa-list-ol fa-3x"></i> Comm. Trainings</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/mun_trainings') }}"><i class="fa fa-list-ol fa-3x"></i> Mun. Trainings</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/psa1') }}"><i class="fa fa-list-ol fa-3x"></i> PSA 1</a>
    </li>
    <li>
        <a href="{{ URL::to('reports/psa2') }}"><i class="fa fa-list-ol fa-3x"></i> PSA 2</a>
    </li>
@stop

@section('content')
	<div class="row">
        <div class="col-md-12">
    		<h2>
    			Brgy. Assembly Details
    			<a href="{{ URL::previous() }}" class="btn btn-default btn-danger" style="float: right"><span class="glyphicon glyphicon-arrow-left"></span> Back</a>
    		</h2> 
        </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
	    <div class="col-md-12">
			<!-- Advanced Tables -->
	    	<div class="panel panel-default">
	    		<div class="panel-heading">
	    			Details
	    		</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
				            <tr>
				                <th>Region</th>
				                <td>aaaaaaaaaaaa</td>
				            </tr>
				            <tr>
				                <th>Province</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Municipality</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Cycle</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>KC Group</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Barangay</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Date Conducted</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Purpose</th>
				                <td>{{ $brgy_assembly->purpose }}</td>
				            </tr>
				            <tr>
				                <th>Families in the Brgy</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>HH in the Brgy (A)</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>HH Represented (B)</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Participation Rate (C = B/A * 100)</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Reviewed by ACT</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Reviewed by SRPMO</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Reviewed by RPMO</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Reviewed by NPMO</th>
				                <td>a</td>
				            </tr>
						</table>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
	    		<div class="panel-heading">
	    			Attendees
	    		</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
				            <tr>
				                <th>Male</th>
				                <td>aaaaaaaaaaaa</td>
				            </tr>
				            <tr>
				                <th>Female</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Male IP</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Female IP</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Male 60 yo and above</th>
				                <td>a</td>
				            </tr>
				            <tr>
				                <th>Female 60 yo and above</th>
				                <td>a</td>
				            </tr>
						</table>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
	    		<div class="panel-heading">
	    			4Ps
	    		</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
				            <tr>
				                <th>4Ps HH</th>
				                <td>aaaaaaaaaaaa</td>
				            </tr>
				            <tr>
				                <th>4Ps Families</th>
				                <td>a</td>
				            </tr>
						</table>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
	    		<div class="panel-heading">
	    			SLP
	    		</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
				            <tr>
				                <th>SLP HH</th>
				                <td>aaaaaaaaaaaa</td>
				            </tr>
				            <tr>
				                <th>SLP Families</th>
				                <td>a</td>
				            </tr>
						</table>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
	    		<div class="panel-heading">
	    			IP
	    		</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
				            <tr>
				                <th>IP HH</th>
				                <td>aaaaaaaaaaaa</td>
				            </tr>
				            <tr>
				                <th>IP Families</th>
				                <td>a</td>
				            </tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop