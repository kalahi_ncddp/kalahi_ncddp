@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('spi_profile') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    {{ Form::open(array('route' => array('spi_profile.set.destroy', $set->project_id, $set->id), 'method' => 'delete')) }}
    	{{ HTML::linkRoute('spi_profile.show', 'Back to Sub Project Profile',
      	array($set->project_id), array('class' => 'btn btn-default btn')) }}
	    	@if( !is_review($set->project_id) )

      {{ HTML::linkRoute('spi_profile.set.edit', 'Edit',
        array($set->project_id, $set->id), array('class' => 'btn btn-info btn')) }}
      <button type="submit" class="btn btn-danger btn-mini"
        onclick="return confirm('Are you sure you want to delete this record?')">Delete</button>
        @endif
    {{ Form::close() }}
  </div>
</div>

<hr />

<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				
				<tr>
					<th width="40%">Date of Evaluation</th>
					<td>{{ toDate($set->date_evaluated) }}</td>
				</tr>
				
				<tr>
					<th width="40%">Sub Project Name</th>
					<td>
						{{ $profile->project_name }}
					</td>
				</tr>

				<tr>
					<th>Physical Description</th>
					<td>{{ $set->phys_descrip }}</td>
				</tr>
				<tr>
					<th>Location</th>
					<td>{{ Report::get_barangay($profile->barangay_id) . ", " . Report::get_municipality($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Mode of Implementation</th>
					<td>{{ $set->implementation_mode }}</td>
				</tr>
				<tr>
					<th>Date of Completion</th>
					<td>{{ toDate($profile->date_completed) }}</td>
				</tr>
				<tr>
					<th>O&amp;M Group managing the Sub Project</th>
					<td>{{ $set->om_group }}</td>
				</tr>
				<tr>
					<th>Approved Cost</th>
					<td>
						@if ($profile->mibf_refno !== "")
							{{ currency_format($proposal->kc_amount + $proposal->lcc_amount) }}
						@else
							{{ currency_format($set->approved_grant + $set->approved_lcc) }}
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-right">Grant</td>
					<td>
						@if ($profile->mibf_refno !== "")
							{{ currency_format($proposal->kc_amount) }}
						@else
							{{ currency_format($set->approved_grant) }}
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-right">LCC</td>
					<td>
						@if ($profile->mibf_refno !== "")
							{{ currency_format($proposal->lcc_amount) }}
						@else
							{{ currency_format($set->approved_lcc) }}
						@endif
					</td>
				</tr>
				<tr>
					<th>Actual Construction Cost</th>
					<td class="actual_cost">0</td>
				</tr>
				<tr>
					<td class="text-right">Grant</td>
					<td>{{ $set->actual_grant }}</td>
				</tr>
				<tr>
					<td class="text-right">LCC</td>
					<td>{{ $set->actual_lcc }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		Evaluation
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="40%">Section</th>
					<th>Score</th>
					<!-- <th>Weight (%)</th> -->
				</tr>
				<tr>
					<td>SP Utilization (I)</td>
					<td>{{ $set->sp_utilization }}</td>
					<!-- <td>{{ $set->sp_utilization_wt }}</td> -->
				</tr>
				<tr>
					<td>Organization and Management (II)</td>
					<td>{{ $set->org_and_mgmt }}</td>
					<!-- <td>{{ $set->org_and_mgmt_wt }}</td> -->
				</tr>
				<tr>
					<td>Institutional Linkage (III)</td>
					<td>{{ $set->institutional_linkage }}</td>
					<!-- <td>{{ $set->institutional_linkage_wt }}</td> -->
				</tr>
				<tr>
					<td>Financial Component (IV)</td>
					<td>{{ $set->financial_component }}</td>
					<!-- <td>{{ $set->financial_component_wt }}</td> -->
				</tr>
				<tr>
					<td>Physical/Technical (V)</td>
					<td>{{ $set->physical_technical }}</td>
					<!-- <td>{{ $set->physical_technical_wt }}</td> -->
				</tr>
				<tr>
					<th class="text-right">Numerical Rating</th>
					<td colspan="2">{{ $set->final_rating() }}</td>
				</tr>
				<tr>
					<th class="text-right">Adjectival Rating</th>
					<td colspan="2">{{ $set->adjectival_rating() }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		// add styling
		$("textarea").css("resize", "none");
		$("table table tr td").css("padding-bottom", "5px");
		$("table table tr td").css("padding-left", "5px");
		$("table table tr th").css("padding-bottom", "5px");
		$("table table tr th").css("padding-left", "5px");

		calculate_actual_cost();
	});

	function calculate_actual_cost() {
		var grant = parseFloat($("input[name='actual_grant']").val() || 0);
		var lcc = parseFloat($("input[name='actual_lcc']").val() || 0);

		$("td.actual_cost").html(round_off(grant + lcc, 2));
	}

	function round_off(num, places) {
		var dec = Math.pow(10, places); 
  	return Math.round(num * dec)/dec;
	}
</script>

@stop