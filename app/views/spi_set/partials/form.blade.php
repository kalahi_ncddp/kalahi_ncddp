<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="40%">Date of Evaluation</th>
					<td>{{ Form::input('text', 'date_evaluated', toDate($set->date_evaluated), array('class' => 'form-control date','max' => date("m/d/Y"), 'required')) }}</td>
				</tr>
				<tr>
					<th width="40%">Sub Project Name</th>
					<td>
						{{ $profile->project_name }}
						{{ Form::hidden('project_id', $set->project_id) }}
					</td>
				</tr>
				<tr>
					<th>Physical Description</th>
					<td>{{ Form::textarea('phys_descrip', $set->phys_descrip, ['class' => 'form-control', 'size' => '30x3', 'required']) }}</td>
				</tr>
				<tr>
					<th>Location</th>
					<td>{{ Report::get_barangay($profile->barangay_id) . ", " . Report::get_municipality($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Mode of Implementation</th>
					<td>{{ Form::text('implementation_mode', $set->implementation_mode, ['class' => 'form-control']) }}</td>
				</tr>
				<tr>
					<th>Date of Completion</th>
					<td>{{ Form::input('text', 'date_completed', isset($profile->date_completed) ? date("m/d/Y", strtotime($profile->date_completed)) : null, array('readonly','class' => 'form-control ','max' => date("m/d/Y"))) }}</td>
				</tr>
				<tr>
					<th>O&amp;M Group managing the Sub Project</th>
					<td>{{ Form::input('text', 'om_group', $set->om_group, array('class' => 'form-control')) }}</td>
				</tr>
				<tr>
					<th>Approved Cost</th>
					<td class="approved_cost">
						@if ($profile->mibf_refno !== "")
							{{ currency_format($proposal->kc_amount + $proposal->lcc_amount) }}
						@else
							0.00
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-right">Grant</td>
					<td>
						@if ($profile->mibf_refno !== "")
							{{ currency_format($proposal->kc_amount) }}

						@else
							{{ Form::input('number', 'approved_grant', $set->approved_grant, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-right">LCC</td>
					<td>
						@if ($profile->mibf_refno !== "")
							{{ currency_format($proposal->lcc_amount) }}

						@else
							{{ Form::input('number', 'approved_lcc', $set->approved_lcc, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}
						@endif
					</td>
				</tr>
				<tr>
					<th>Actual Construction Cost</th>
					<td class="actual_cost">0</td>
				</tr>
				<tr>
					<td class="text-right">Grant</td>
					<td>{{ Form::input('number', 'actual_grant', $set->actual_grant, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
				</tr>
				<tr>
					<td class="text-right">LCC</td>
					<td>{{ Form::input('number', 'actual_lcc', $set->actual_lcc, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		Evaluation
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="40%">Section</th>
					<th>Score</th>
					<!-- <th>Weight (%)</th> -->
				</tr>
				<tr>
					<td>SP Utilization (I)</td>
					<td>{{ Form::input('number', 'sp_utilization', $set->sp_utilization, ['class' => 'form-control score', 'min' => 0, 'max' => 5, 'step' => 'any']) }}</td>
					<!-- <td>{{ Form::input('number', 'sp_utilization_wt', $set->sp_utilization_wt, ['class' => 'form-control score', 'step' => 'any', 'required']) }}</td> -->
				</tr>
				<tr>
					<td>Organization and Management (II)</td>
					<td>{{ Form::input('number', 'org_and_mgmt', $set->org_and_mgmt, ['class' => 'form-control score', 'min' => 0, 'max' => 5, 'step' => 'any']) }}</td>
					<!-- <td>{{ Form::input('number', 'org_and_mgmt_wt', $set->org_and_mgmt_wt, ['class' => 'form-control score', 'step' => 'any', 'required']) }}</td> -->
				</tr>
				<tr>
					<td>Institutional Linkage (III)</td>
					<td>{{ Form::input('number', 'institutional_linkage', $set->institutional_linkage, ['class' => 'form-control score', 'max' => 5, 'min' => 0, 'step' => 'any']) }}</td>
					<!-- <td>{{ Form::input('number', 'institutional_linkage_wt', $set->institutional_linkage_wt, ['class' => 'form-control score', 'step' => 'any', 'required']) }}</td> -->
				</tr>
				<tr>
					<td>Financial Component (IV)</td>
					<td>{{ Form::input('number', 'financial_component', $set->financial_component, ['class' => 'form-control score', 'min' => 0, 'max' => 5, 'step' => 'any']) }}</td>
					<!-- <td>{{ Form::input('number', 'financial_component_wt', $set->financial_component_wt, ['class' => 'form-control score', 'step' => 'any', 'required']) }}</td> -->
				</tr>
				<tr>
					<td>Physical/Technical (V)</td>
					<td>{{ Form::input('number', 'physical_technical', $set->physical_technical, ['class' => 'form-control score', 'min' => 0, 'max' => 5, 'step' => 'any']) }}</td>
					<!-- <td>{{ Form::input('number', 'physical_technical_wt', $set->physical_technical_wt, ['class' => 'form-control score', 'step' => 'any', 'required']) }}</td> -->
				</tr>
			</table>
		</div>
	</div>
</div>

{{ Form::submit('Save Evaluation', array('class' => 'btn btn-primary')) }}
{{ HTML::linkRoute('spi_profile.show', 'Close',
  array($set->project_id), array('class' => 'btn bg-navy btn')) }}
{{ Form::close() }}

<script>
	$(document).ready(function() {
		// add styling
		$("input[name='date_evaluated']").change(function(){
		    if(new Date($("input[name='date_evaluated']").val()) > new Date()){
       		    $("input[name='date_evaluated']")[0].setCustomValidity('Future date is not allowed');
		    }else{
		        $("input[name='date_evaluated']")[0].setCustomValidity('');
                if(new Date($("input[name='date_evaluated']").val()) < new Date($("input[name='date_completed']").val())){
                    $("input[name='date_evaluated']")[0].setCustomValidity('Evaluation date should be later than completion date');
                }else{
                    $("input[name='date_evaluated']")[0].setCustomValidity('');
                }
		    }
		});
		$("textarea").css("resize", "none");
		$("table table tr td").css("padding-bottom", "5px");
		$("table table tr td").css("padding-left", "5px");
		$("table table tr th").css("padding-bottom", "5px");
		$("table table tr th").css("padding-left", "5px");

		$("input[name='actual_grant']").change(calculate_actual_cost);
		$("input[name='actual_lcc']").change(calculate_actual_cost);
		calculate_actual_cost();

		@if ($profile->mibf_refno === "")
			$("input[name='approved_grant']").change(calculate_approved_cost);
			$("input[name='approved_lcc']").change(calculate_approved_cost);
			calculate_approved_cost();
		@endif

//		$("input.score").change(calculate_rating);
//		calculate_rating();
	});

	function calculate_actual_cost() {
		var grant = parseFloat($("input[name='actual_grant']").val() || 0);
		var lcc = parseFloat($("input[name='actual_lcc']").val() || 0);

		$("td.actual_cost").html(round_off(grant + lcc, 2));
	}

	function calculate_approved_cost() {
		var grant = parseFloat($("input[name='approved_grant']").val() || 0);
		var lcc = parseFloat($("input[name='approved_lcc']").val() || 0);

		$("td.approved_cost").html(round_off(grant + lcc, 2));
	}

	function round_off(num, places) {
		var dec = Math.pow(10, places); 
  	return Math.round(num * dec)/dec;
	}
</script>