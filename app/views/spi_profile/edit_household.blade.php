@extends('layouts.default')

@section('username')
  {{ $username }}
@stop

@section('sidebar')
  <li>
    <a class="active-menu" href="{{ URL::to('spi_profile') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')

{{ Form::open(array('route' => array('spi_profile.update_household', $profile->project_id), 'method' => 'patch', 'role' => 'form')) }}

<div class="panel panel-default">
  <div class="panel-heading">
    General
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <div class="col-md-6 ">
      <table class="table table-bordered table-striped">
        <tr>
          <th>Municipality</th>
          <td>
            {{ Report::get_municipality($profile->municipal_id) }}
          </td>
        </tr>
        <tr>
          <th width="30%">Barangay</th>
          <td>
            {{ Report::get_barangay($profile->barangay_id) }}
          </td>
        </tr>
        <tr>
          <th width="30%">Fund Source</th>
          <td>
            {{ $profile->program_id }}
          </td>
        </tr>
        <tr>
          <th width="30%">Cycle</th>
          <td>
            {{ $profile->cycle_id }}
          </td>
        </tr>
      </table>
    </div>
    <div class="col-md-6 ">
      <table class="table table-bordered table-striped">
      
        <tr>
          <th width="30%">Sub Project Name</th>
          <td>
            {{ $profile->project_name }}
          </td>
        </tr>
        <tr>
          <th width="30%">Sub Project Type</th>
          <td>
            {{ $profile->proj_subcategory }}
          </td>
        </tr>

        <tr>
          <th>Total Number of Households (HH) in the Barangay</th>
          <td>{{ Form::input('number', 'no_households', $profile->no_households, ['class' => 'form-control']) }}</td>
        </tr>
      </table>
    </div>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    Target Beneficiaries
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-bordered table-striped">
        <tr>
          <th></th>
            <th colspan="2"># of Members within the household</th>

          <th colspan="5"># of Families within the household</th>
        </tr>
        <tr>
          <th>Target Household  / household beneficiaries</th>
          <th>Male</th>
          <th>Female</th>
          <th>Total</th>
          <th>IP</th>
          <th>Pantawid Pamilya</th>
          <th>SLP</th>
        </tr>
        <tr>
          <td>{{ Form::input('number', 'target_household', $profile->target_household, ['class' => 'form-control', 'min'=>'0']) }}</td>
          <td>{{ Form::input('number', 'male_members', $profile->male_members, ['class' => 'form-control', 'min'=>'0']) }}</td>
          <td>{{ Form::input('number', 'female_members', $profile->female_members, ['class' => 'form-control', 'min'=>'0']) }}</td>
          <td>{{ Form::input('number', 'total_families', $profile->total_families, ['class' => 'form-control', 'min'=>'0']) }}</td>
          <td>{{ Form::input('number', 'total_ip', $profile->total_ip, ['class' => 'form-control', 'min'=>'0']) }}</td>
          <td>{{ Form::input('number', 'total_4p', $profile->total_4p, ['class' => 'form-control', 'min'=>'0']) }}</td>
          <td>{{ Form::input('number', 'total_slp', $profile->total_4p, ['class' => 'form-control', 'min'=>'0']) }}</td>

        </tr>
      </table>
    </div>
  </div>
</div>


<div class="panel panel-default">
  <div class="panel-heading">
    Actual Beneficiaries
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-bordered table-striped">
        <tr>
          <th></th>
          <th colspan="2"># of Members within the household</th>
          <th colspan="5"># of Families within the household</th>
        <tr>
                  <th>Actual Household  / household beneficiaries</th>

          <th>Male</th>
          <th>Female</th>
          <th>Total</th>
          <th>IP</th>
          <th>Pantawid Pamilya</th>
          <th>SLP</th>
        </tr>
        <tr>
             <td>{{ Form::input('number', 'actual_household', $profile->actual_household, ['class' => 'form-control', 'min'=>'0']) }}</td>
            <td>{{ Form::input('number', 'actual_male_members', $profile->actual_male_members, ['class' => 'form-control','min'=>'0']) }}</td>
            <td>{{ Form::input('number', 'actual_female_members', $profile->actual_female_members, ['class' => 'form-control','min'=>'0']) }}</td>
            <td>{{ Form::input('number', 'actual_total_families', $profile->actual_total_families, ['class' => 'form-control','min'=>'0']) }}</td>
            <td>{{ Form::input('number', 'actual_total_ip', $profile->actual_total_ip, ['class' => 'form-control','min'=>'0']) }}</td>
            <td>{{ Form::input('number', 'actual_total_4p', $profile->actual_total_4p, ['class' => 'form-control','min'=>'0']) }}</td>
            <td>{{ Form::input('number', 'actual_total_slp', $profile->actual_total_slp, ['class' => 'form-control','min'=>'0']) }}</td>

        </tr>
      </table>
    </div>
  </div>
</div>

{{ Form::submit('Save SP Household Beneficiaries', array('class' => 'btn btn-primary')) }}
{{ HTML::linkRoute('spi_profile.show', 'Close',
  array($profile->project_id), array('class' => 'btn bg-navy btn')) }}
{{ Form::close() }}

<script>
    $('input[name=no_households]').change(function(){
        var no_households = $(this).val();

    });
    $(document).ready(function(){
      // $('input[name=male_members]').change(function(){
      //    var total = parseInt($('input[name=male_members]').val()) +  parseInt($('input[name=female_members]').val());
      //     $('input[name=total_families]').val(total);
      // });
      // $('input[name=female_members]').change(function(){
        
      //    var total = parseInt($('input[name=male_members]').val()) +  parseInt($('input[name=female_members]').val());
      //     $('input[name=total_families]').val(total);
      //   });

      //    $('input[name=actual_male_members]').change(function(){
      //    var total = parseInt($('input[name=actual_male_members]').val()) +  parseInt($('input[name=actual_female_members]').val());
      //     $('input[name=actual_total_families]').val(total);
      // });
      // $('input[name=actual_female_members]').change(function(){
        
      //    var total = parseInt($('input[name=actual_male_members]').val()) +  parseInt($('input[name=actual_female_members]').val());
      //     $('input[name=actual_total_families]').val(total);
      //   });
    });

</script>
@stop