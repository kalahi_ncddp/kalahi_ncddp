@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('spi_profile') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    {{ HTML::linkRoute('spi_profile.show', 'Back to Sub Project Profile',
      array($profile->project_id), array('class' => 'btn btn-default btn')) }}
      @if( !is_review($profile->project_id) )
    {{ HTML::linkRoute('spi_profile.edit_household', 'Edit',
      array($profile->project_id), array('class' => 'btn btn-info btn')) }}
      @endif
  </div>
</div>

<hr />

<div class="panel panel-default">
  <div class="panel-heading">
    General
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <div class="col-md-6 ">
      <table class="table table-bordered table-striped">
        <tr>
          <th>Municipality</th>
          <td>
            {{ Report::get_municipality($profile->municipal_id) }}
          </td>
        </tr>
        <tr>
          <th width="30%">Barangay</th>
          <td>
            {{ Report::get_barangay($profile->barangay_id) }}
          </td>
        </tr>
        <tr>
          <th width="30%">Fund Source</th>
          <td>
            {{ $profile->program_id }}
          </td>
        </tr>
        <tr>
          <th width="30%">Cycle</th>
          <td>
            {{ $profile->cycle_id }}
          </td>
        </tr>
      </table>
    </div>
    <div class="col-md-6 ">
      <table class="table table-bordered table-striped">
      
        <tr>
          <th width="30%">Sub Project Name</th>
          <td>
            {{ $profile->project_name }}
          </td>
        </tr>
        <tr>
          <th width="30%">Sub Project Type</th>
          <td>
            {{ $profile->proj_subcategory }}
          </td>
        </tr>

        <tr>
          <th>Total Number of Households (HH) in the Barangay</th>
          <td>{{ Form::input('number', 'no_households', $profile->no_households, ['class' => 'form-control','readonly']) }}</td>
        </tr>
      </table>
    </div>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    Target Beneficiaries
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-bordered table-striped">
        <tr>
        <th></th>
          <th colspan="2"># of Members within the household</th>
          <th colspan="4"># of Families within the household</th>
        </tr>
        <tr>
                          <th>Target Household  / household beneficiaries</th>

          <th>Male</th>
          <th>Female</th>
          <th>Total</th>
          <th>IP</th>
          <th>Pantawid Pamilya</th>
          <th>SLP</th>
        </tr>
        <tr>
          <td>{{ $profile->target_household }}</td>
          <td>{{ $profile->male_members }}</td>
          <td>{{ $profile->female_members }}</td>
          <td>{{ $profile->total_families }}</td>
          <td>{{ $profile->total_ip }}</td>
          <td>{{ $profile->total_4p }}</td>
          <td>{{ $profile->total_slp }}</td>
        </tr>
      </table>
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    Actual Beneficiaries
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-bordered table-striped">
        <tr>
        <th></th>
          <th colspan="2"># of Members within the household</th>
          <th colspan="4"># of Families within the household</th>
        </tr>
        <tr>
                          <th>Actual Household  / household beneficiaries</th>

          <th>Male</th>
          <th>Female</th>
          <th>Total</th>
          <th>IP</th>
          <th>Pantawid Pamilya</th>
          <th>SLP</th>
        </tr>
        <tr>
                  <td>{{ $profile->actual_household }}</td>

          <td>{{ $profile->actual_male_members }}</td>
          <td>{{ $profile->actual_female_members }}</td>
          <td>{{ $profile->actual_total_families }}</td>
          <td>{{ $profile->actual_total_ip }}</table-bordered>
          <td>{{ $profile->actual_total_4p }}</td>
          <td>{{ $profile->actual_total_slp }}</td>
        </tr>
      </table>
    </div>
  </div>
</div>

@stop