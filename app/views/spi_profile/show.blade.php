@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('spi_profile') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        @if($profile->is_draft==1)
                  <div class="alert alert-info">This is still a draft  <a href="{{ URL::to('spi_profile/' .$profile->project_id.'/edit') }}">edit the document</a> to set it in final draft</div>
             @endif
        @if( !is_review($profile->project_id) )
          <a class="btn btn-default" href="{{ URL::to('spi_profile') }}">Go Back</a>
          <div class="pull-right">
            {{ Form::open(array('route' => array('spi_profile.destroy', $profile->project_id), 'method' => 'delete')) }}
                {{ HTML::linkRoute('spi_profile.edit', 'Edit',
                  array($profile->project_id), array('class' => 'btn btn-info btn')) }}
                <button type="submit" class="btn btn-danger btn-mini"
                  onclick="return confirm('Are you sure you want to delete this record?')"><i class="fa fa-trash-o"></i> Delete</button>

            {{ Form::close() }}
          </div>
         @endif

       <div class="pull-right">
                  {{ HTML::linkRoute('spi_profile.household', 'SP Household Beneficiaries',
                  array($profile->project_id), array('class' => 'btn btn-success btn')) }}

                   {{ HTML::linkRoute('spi_profile.tranche.show', 'SP Tranche',
                  array($profile->project_id), array('class' => 'btn btn-success btn')) }}


                  {{ HTML::linkRoute('spi_profile.certificate', 'Certificate of Completion and Acceptance',
                  array($profile->project_id), array('class' => 'btn btn-success btn')) }}

                  {{ HTML::linkRoute('spi_profile.spcr.show', 'SP Completion Report',
                  array($profile->project_id), array('class' => 'btn btn-success btn')) }}



                  {{ HTML::linkRoute('spi_profile.procurement.show', 'Community Procurement Monitoring',
                  array($profile->project_id), array('class' => 'btn btn-success btn')) }}

                 <span class="btn  btn-success" data-toggle="modal" data-target="#set_list">
                
                  SP Sustainability Evaluation Tool
                </span>
      </div>

  </div>  
</div>  

<div class="panel panel-default">
  <div class="panel-heading">
    Profile
  </div>
  <div class="panel-body">
    <div class="table-responsive">
          <div class="col-md-6 ">
      <table class="table table-bordered table-striped">
        <tr>
          <th width="30%">Barangay</th>
          <td>
            {{ Report::get_barangay($profile->barangay_id()) }}
          </td>
        </tr>
        <tr>
          <th>Municipality</th>
          <td>
            {{ Report::get_municipality($profile->municipal_id) }}
          </td>
          <tr>
          <th>Cycle</th>
          <td>
            
            {{ $profile->cycle_id}}
          </td>
        </tr>
        <tr>
          <th>Province</th>
          <td>{{ Report::get_province($profile->municipal_id) }}</td>
        </tr>
      </table>
    </div>
        <div class="col-md-6 ">
      <table class="table table-bordered table-striped">

        <tr>
          <th>Region</th>
          <td>{{ Report::get_region($profile->municipal_id) }}</td>
        </tr>
        <tr>
          <th>Total Number of Households (HH) in the Barangay</th>
          <td>{{ $profile->no_households }}</td>
        </tr>
        <tr>
          <th>Population</th>
          <td>
            <table style="width: 100%">
              <tr>
                <th>Total</th>
                <th>Male (% of total)</th>
                <th>Female (% of total)</th>
              </tr>
              <tr>
                <td>{{ $profile->population }}</td>
                <td>{{ $profile->percent_male }}</td>
                <td>{{ $profile->percent_female }}</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    General Information
  </div>
  <div class="panel-body">
    <div class="table-responsive">
        <div class="col-md-6 ">
      <table class="table table-bordered table-striped">
        <tr>
          <th width="30%">Sub Project Name</th>
          <td>{{ $profile->project_name }}</td>
        </tr>
        <tr>
          <th>Sub Project Category</th>
          <td>{{ $profile->proj_subcategory }}</td>
        </tr>
        <tr>
          <th width="30%">Project Statement</th>
          <td>{{ $profile->proj_statement }}</td>
        </tr>
      </table>
    </div>
      <div class="col-md-6 ">
      <table class="table table-bordered table-striped">
        
        <tr>
          <th width="30%">Project Purpose</th>
          <td>{{ $profile->proj_purpose }}</td>
        </tr>
        <tr>
          <th width="30%">Date Started</th>
          <td>{{ toDate($profile->date_started) }}</td>
        </tr>
        <tr>
          <th>Planned Date of Completion</th>
          <td>{{ toDate($profile->planned_date_completed) }}</td>
        </tr>
      </table>
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    Description of Project Components
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-bordered table-striped">
        <tr>
          <th width="30%">Physical Target</th>
          <td>{{ $profile->phys_target . " " . $profile->phys_target_unit }}</td>
        </tr>
        <tr>
          <th width="30%">Cost Parameter</th>
          <td>{{ $profile->cost_parameter }}</div>
          </td>
        </tr>
        <tr>
          <th>Labor/Workforce Requirements/Source</th>
          <td>
            <table style="width: 100%">
              <tr>
                <th></th>
                <th>Male</th>
                <th>Female</th>
              </tr>
              <tr>
                <td>Skilled</td>
                <td>{{ $profile->skilled_male }}</td>
                <td>{{ $profile->skilled_female }}</td>
              </tr>
              <tr>
                <td>Non-skilled</td>
                <td>{{ $profile->nonskilled_male }}</td>
                <td>{{ $profile->nonskilled_female }}</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
  </div>
</div>


<!-- 
<div class="panel panel-default">
  <div class="panel-heading">
    Financial Economic Aspect
  </div>
  <div class="panel-body" id="financial_aspect">
    <div class="table-responsive">
      <table class="table table-bordered table-striped">
        <tr>
          <th width="30%">Cost Items Per Component</th>
          <th>CDD Grant</th>
          <th>Total LCC</th>
          <th>Total Cost</th>
        </tr>
        <tr>
          <td>Total Estimated Cost - POW (Infrastructure)</td>
          <td>{{ $profile->grant_infra }}</td>
          <td>{{ $profile->lcc_infra }}</td>
          <td>{{ $profile->cost_infra }}</td>
        </tr>
        <tr>
          <td>Training</td>
          <td>{{ $profile->grant_training }}</td>
          <td>{{ $profile->lcc_training }}</td>
          <td>{{ $profile->cost_training }}</td>
        </tr>
        <tr>
          <td>Women-Specific</td>
          <td>{{ $profile->grant_women }}</td>
          <td>{{ $profile->lcc_women }}</td>
          <td>{{ $profile->cost_women }}</td>
        </tr>
        <tr>
          <td>Management</td>
          <td>{{ $profile->grant_mgmnt }}</td>
          <td>{{ $profile->lcc_mgmnt }}</td>
          <td>{{ $profile->cost_mgmnt }}</td>
        </tr>
        <tr>
          <td>Others</td>
          <td>{{ $profile->grant_others }}</td>
          <td>{{ $profile->lcc_others }}</td>
          <td>{{ $profile->cost_others }}</td>
        </tr>
        <tr>
          <td class="text-right">Total</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
        </tr>
        <tr>
          <td class="text-right">Total (%)</td>
          <td>100%</td>
          <td>0%</td>
          <td>0%</td>
        </tr>
      </table>
    </div>
  </div>
</div>

 -->


<div class="panel panel-default">
  <div class="panel-heading">
    Financial Economic Aspect
  </div>
  <!-- <div class="panel-body" id="financial_aspect"> -->
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-bordered table-striped">
        <tr>
          <th width="30%">Cost Items Per Component</th>
          
          <th>CDD Grant</th>
          <th>Total LCC</th>
          <th>Total Cost</th>
        </tr>
        <tr>
          <td>Total Estimated Cost - POW (Infrastructure)</td>
          <td>{{ Form::input('number', 'grant_infra', $profile->grant_infra, ['class' => 'form-control', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'lcc_infra', $profile->lcc_infra, ['class' => 'form-control', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'cost_infra', $profile->cost_infra, ['class' => 'form-control cost_infra', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td class="cost_infra"></td>
        </tr>
        <tr>
          <td>Training</td>
          <td>{{ Form::input('number', 'grant_training', $profile->grant_training, ['class' => 'form-control', 'min' => 0, 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'lcc_training', $profile->lcc_training, ['class' => 'form-control', 'min' => 0, 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'cost_training', $profile->cost_training, ['class' => 'form-control cost_training','step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td class="cost_training"></td>
        </tr>
        <tr>
          <td>Women-Specific</td>
          <td>{{ Form::input('number', 'grant_women', $profile->grant_women, ['class' => 'form-control', 'min' => 0, 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'lcc_women', $profile->lcc_women, ['class' => 'form-control', 'min' => 0, 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'cost_women', $profile->cost_women, ['class' => 'form-control cost_women', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td class="cost_women"></td>
        </tr>
        <tr>
          <td>Management</td>
          <td>{{ Form::input('number', 'grant_mgmnt', $profile->grant_mgmnt, ['class' => 'form-control', 'min' => 0, 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'lcc_mgmnt', $profile->lcc_mgmnt, ['class' => 'form-control', 'min' => 0, 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'cost_mgmnt', $profile->cost_mgmnt, ['class' => 'form-control cost_mgmnt', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td class="cost_mgmnt"></td>
        </tr>
        <tr>
          <td>Others</td>
          <td>{{ Form::input('number', 'grant_others', $profile->grant_others, ['class' => 'form-control', 'min' => 0, 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'lcc_others', $profile->lcc_others, ['class' => 'form-control', 'min' => 0, 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'cost_others', $profile->cost_others, ['class' => 'form-control cost_others', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td class="cost_others"></td>
        </tr>
        <tr>
          <td class="text-right">Total</td>
          <td>{{ Form::input('number', 'total_cdd', $profile->total_cdd, ['class' => 'form-control total_cdd', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'total_lcc', $profile->total_lcc, ['class' => 'form-control total_lcc', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'total_cost', $profile->total_cost, ['class' => 'form-control total_cost', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          
          <!-- <td>0</td>
          <td>0</td>   -->
        </tr>
        <tr>
          <td class="text-right">Total (%)</td>
          <td>{{ Form::input('number', 'total_cdd_percent', $profile->total_cdd_percent, ['class' => 'form-control total_cdd_percent', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'total_lcc_percent', $profile->total_lcc_percent, ['class' => 'form-control total_lcc_percent', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          <td>{{ Form::input('number', 'total_cost_percent', $profile->total_cost_percent, ['class' => 'form-control total_cost_percent', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
          
          <!-- <td>100%</td>  
          <td>0%</td>
          <td>0%</td> -->
        </tr>
      </table>
    </div>
  </div>
</div>
@include('modals.sp_set_list')

<script>
  $(document).ready(function() {
    $('.data-table').dataTable();
    calculate_financial_aspect_total();
  });

  function calculate_financial_aspect_total() {
    var parent = $("#financial_aspect table");
    var total_list = [];

    var tr = $(parent).find("tr");
    var num_rows = $(tr).length;
    for (var col = 2; col <= $(tr[0]).find("th").length; col++) {
      var total = 0;
      var row = 0;
      for (row = 1; row < num_rows - 2; row++) {
        total += round_off(parseFloat($(tr[row]).find("td:nth-child(" + col + ")").html() || 0), 2);
      }
      $(parent).find("tr:nth-child(" + (row+1) + ") td:nth-child(" + col + ")").html(total);
      total_list.push(total);
    }

    var percent_row = $(parent).find("tr:nth-child(" + num_rows + ")")[0];
    var total_cost = total_list[0];
    
    for (var i = 1; i < total_list.length; i++) {
      var percentage = 0;

      if (total_cost != 0) {
        percentage = round_off((total_list[i] / total_cost) * 100, 2);
      }
      
      $(percent_row).find("td:nth-child(" + (i+2) + ")").html(percentage + "%");
    }
  }

  function round_off(num, places) {
    var dec = Math.pow(10, places); 
    return Math.round(num * dec)/dec;
  }

   /* get the total in male and female */
      var total = (function($){
        return {
          
          totalPersonMale : function(){
                  var thisElement = $(this).val();
                  $("input[name='percent_female']").val(100 - thisElement);
                  var total_ =  ( parseFloat($("input[name='percent_male']").val() ) | 0 ) + ( parseFloat($("input[name='percent_female']").val() ) | 0 );
                  $('.population').val(total_ || '0');

                },
                totalPersonFemale : function(){
                  var thisElement = $(this).val();
                  $("input[name='percent_male']").val(100 - thisElement);
                 var total_ =  ( parseFloat($("input[name='percent_male']").val() ) | 0 ) + ( parseFloat($("input[name='percent_female']").val() ) | 0 );
                  $('.population').val(total_ || '0');

                },
          totalFEA : function(){
            var total_ = parseFloat($("input[name='grant_infra']").val() )  + parseFloat($("input[name='lcc_infra']").val() )  ;
            $('.cost_infra').val(total_ || '0');
          },

          totalTraining : function(){
            var total_ = parseFloat($("input[name='grant_training']").val() )  + parseFloat($("input[name='lcc_training']").val() ) ;
            $('.cost_training').val(total_ || '0');

          },
          totalWomen : function(){
            var total_ = parseFloat($("input[name='grant_women']").val()  )  + parseFloat($("input[name='lcc_women']").val() )  ;
            $('.cost_women').val(total_ || '0');
            total.totalCDD_PERCENT();
            total.totalLCC_PERCENT();
            total.totalCOST();
            total.totalLCOST_PERCENT();
          },
          totalManagement : function(){
            var total_ = parseFloat($("input[name='grant_mgmnt']").val()  )  + parseFloat($("input[name='lcc_mgmnt']").val() )  ;
            $('.cost_mgmnt').val(total_ || '0');
            total.totalCDD_PERCENT();
            total.totalLCC_PERCENT();
            total.totalCOST();
            total.totalLCOST_PERCENT();
          },

          totalOthers : function(){
            var total_ = parseFloat($("input[name='grant_others']").val() ) + parseFloat($("input[name='lcc_others']").val() )  ;
            $('.cost_others').val(total_ || '0');
            total.totalCDD_PERCENT();
            total.totalLCC_PERCENT();
            total.totalCOST();
            total.totalLCOST_PERCENT();
          },

          totalCDD : function(){
            var total_ = parseFloat($("input[name='grant_infra']").val() ) 
            + parseFloat($("input[name='grant_training']").val()) 
            + parseFloat($("input[name='grant_women']").val()) 
            + parseFloat($("input[name='grant_mgmnt']").val()) 
            + parseFloat($("input[name='grant_others']").val())  ;
            $('.total_cdd').val(total_ || '0');
          },

          totalLCC : function(){
            var total_ = parseFloat($("input[name='lcc_infra']").val() ) 
            + parseFloat($("input[name='lcc_training']").val() ) 
            + parseFloat($("input[name='lcc_women']").val() ) 
            + parseFloat($("input[name='lcc_mgmnt']").val() ) 
            + parseFloat($("input[name='lcc_others']").val() )  ;
            $('.total_lcc').val(total_ || '0');
          },

          totalCDD_PERCENT : function(){
            // var total_ = parseFloat($("input[name='total_cdd']").val()) / parseFloat($("input[name='total_cost']").val());
            //cdd total
            var total_cdd = parseFloat($("input[name='grant_infra']").val())  
                        + parseFloat($("input[name='grant_training']").val()) 
                        + parseFloat($("input[name='grant_women']").val()) 
                        + parseFloat($("input[name='grant_mgmnt']").val()) 
                        + parseFloat($("input[name='grant_others']").val()) ;
            /* lcc total*/
            var total_lcc = parseFloat($("input[name='lcc_infra']").val())  
                        + parseFloat($("input[name='lcc_training']").val()) 
                        + parseFloat($("input[name='lcc_women']").val()) 
                        + parseFloat($("input[name='lcc_mgmnt']").val()) 
                        + parseFloat($("input[name='lcc_others']").val())  ;
            var total_cost = total_cdd + total_lcc;
            var cdd_percent = ( total_cdd / total_cost ) * 100;
            $('.total_cdd_percent').val(cdd_percent.toFixed(2));
          },

          totalLCC_PERCENT : function(){
            // var total_ = parseFloat($("input[name='total_cdd']").val()) / parseFloat($("input[name='total_cost']").val());
            //cdd total
            var total_cdd = parseFloat($("input[name='grant_infra']").val()) 
                        + parseFloat($("input[name='grant_training']").val())
                        + parseFloat($("input[name='grant_women']").val())
                        + parseFloat($("input[name='grant_mgmnt']").val())
                        + parseFloat($("input[name='grant_others']").val()) ;
            /* lcc total*/
            var total_lcc = parseFloat($("input[name='lcc_infra']").val()) 
                        + parseFloat($("input[name='lcc_training']").val())
                        + parseFloat($("input[name='lcc_women']").val())
                        + parseFloat($("input[name='lcc_mgmnt']").val())
                        + parseFloat($("input[name='lcc_others']").val()) ;
            var total_cost = total_cdd + total_lcc;
            var lcc_percent = ( total_lcc / total_cost ) * 100;
            $('.total_lcc_percent').val(lcc_percent.toFixed(2));
          },

          totalCOST : function(){
            // var total_ = parseFloat($("input[name='total_cdd']").val()) / parseFloat($("input[name='total_cost']").val());
            //cdd total
            var total_cdd = parseFloat($("input[name='grant_infra']").val()) 
                        + parseFloat($("input[name='grant_training']").val())
                        + parseFloat($("input[name='grant_women']").val())
                        + parseFloat($("input[name='grant_mgmnt']").val())
                        + parseFloat($("input[name='grant_others']").val()) ;
            /* lcc total*/
            var total_lcc = parseFloat($("input[name='lcc_infra']").val()) 
                        + parseFloat($("input[name='lcc_training']").val())
                        + parseFloat($("input[name='lcc_women']").val())
                        + parseFloat($("input[name='lcc_mgmnt']").val())
                        + parseFloat($("input[name='lcc_others']").val()) ;
            var total_cost = total_cdd + total_lcc;

            // var lcc_percent = ( total_cdd / total_lcc ) * 100;
            $('.total_cost').val(total_cost.toFixed(2));
          },

          totalLCOST_PERCENT : function(){
            // var total_ = parseFloat($("input[name='total_cdd']").val()) / parseFloat($("input[name='total_cost']").val());
            //cdd total
            var total_cdd = parseFloat($("input[name='grant_infra']").val()) 
                        + parseFloat($("input[name='grant_training']").val()) 
                        + parseFloat($("input[name='grant_women']").val()) 
                        + parseFloat($("input[name='grant_mgmnt']").val()) 
                        + parseFloat($("input[name='grant_others']").val())  ;
            /* lcc total*/
            var total_lcc = parseFloat($("input[name='lcc_infra']").val())  
                        + parseFloat($("input[name='lcc_training']").val()) 
                        + parseFloat($("input[name='lcc_women']").val()) 
                        + parseFloat($("input[name='lcc_mgmnt']").val()) 
                        + parseFloat($("input[name='lcc_others']").val())  ;
            var total_cost = total_cdd + total_lcc;
            var cdd_percent = ( total_cdd / total_cost ) * 100;
            var lcc_percent = ( total_lcc / total_cost ) * 100;
            var total_cdd_lcc = cdd_percent + lcc_percent;
            $('.total_cost_percent').val(total_cdd_lcc.toFixed(2));
          }







        }
      })(jQuery);
                // total.totalPerson();
                total.totalFEA();
                total.totalTraining();
                total.totalWomen();
                total.totalManagement();
                total.totalOthers();
                total.totalCDD();
                total.totalLCC();
                // total.totalCDD_PERCENT();

      $("input[name='percent_male']").change(total.totalPersonMale);
      $("input[name='percent_female']").change(total.totalPersonFemale);

      $("input[name='grant_infra']").keyup(total.totalFEA);
      $("input[name='lcc_infra']").keyup(total.totalFEA);
      
      $("input[name='grant_training']").keyup(total.totalTraining);
      $("input[name='lcc_training']").keyup(total.totalTraining);
      
      $("input[name='grant_women']").keyup(total.totalWomen);
      $("input[name='lcc_women']").keyup(total.totalWomen);

      $("input[name='grant_mgmnt']").keyup(total.totalManagement);
      $("input[name='lcc_mgmnt']").keyup(total.totalManagement);
      
      $("input[name='grant_others']").keyup(total.totalOthers);
      $("input[name='lcc_others']").keyup(total.totalOthers);

      $("input[name='grant_infra']").keyup(total.totalCDD);
      $("input[name='grant_training']").keyup(total.totalCDD);
      $("input[name='grant_women']").keyup(total.totalCDD);
      $("input[name='grant_mgmnt']").keyup(total.totalCDD);
      $("input[name='grant_others']").keyup(total.totalCDD);

      $("input[name='lcc_infra']").keyup(total.totalLCC);
      $("input[name='lcc_training']").keyup(total.totalLCC);
      $("input[name='lcc_women']").keyup(total.totalLCC);
      $("input[name='lcc_mgmnt']").keyup(total.totalLCC);
      $("input[name='lcc_others']").keyup(total.totalLCC);
</script>
@stop