{{ Form::hidden('mibf_refno', $profile->mibf_refno) }}
{{ Form::hidden('mibf_rank', $profile->mibf_rank) }}

<div class="panel panel-default">
	<div class="panel-heading">
		Profile
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<div class="col-md-6 ">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="30%">Barangays</th>
					<td>
						@if (isset($profile->mibf_refno) && $profile->mibf_refno !== "")
							{{ Report::get_barangay($profile->barangay_id) }}
							{{ Form::hidden('barangay_id', $profile->barangay_id) }}
						@else
							{{ Form::select('barangay_id', [''=>'Select Barangay']+$barangay_list, $profile->barangay_id, array('class' => 'form-control', 'required')) }}
						@endif
					</td>
				</tr>
				<tr>
					<th>Municipality</th>
					<td>
						{{ Report::get_municipality($profile->municipal_id) }}
						{{ Form::hidden('municipal_id', $profile->municipal_id) }}
					</td>
				</tr>
				<tr>
					<th>Province</th>
					<td>{{ Report::get_province($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Region</th>
					<td>{{ Report::get_region($profile->municipal_id) }}</td>
				</tr>
			</table>
		</div>

		<div class="col-md-6 ">
			<table class="table table-bordered table-striped">
				<tr>
				    <th>Program</th>
				    @if (isset($profile->mibf_refno) && $profile->mibf_refno !== "")

                       <td>  {{ Form::text('program_id', isset($profile->program_id) ? $profile->program_id : $program_id, ['class' => 'form-control','readonly']) }} </td>
                    @else
				    <td>{{ Form::select('program_id',isset($programs) ? $programs : [],$profile->program_id, ['class' => 'form-control'] )}}</td>
                    @endif
				</tr>
				<tr>
				    <th>Cycle</th>
				    @if (isset($profile->mibf_refno) && $profile->mibf_refno !== "")
				        <td>  {{ Form::text('cycle_id', isset($profile->cycle_id) ? $profile->cycle_id:$cycle_id , ['class' => 'form-control','readonly']) }} </td>
				    @else
				    <td>{{ Form::select('cycle_id',isset($cycles) ? $cycles : [],$profile->cycle_id, ['class' => 'form-control'] )}}</td>
				    @endif

				</tr>
				<tr>
					<th>Total Number of Households (HH) in the Barangay</th>
					<td>{{ Form::input('number', 'no_households', $profile->no_households, ['class' => 'form-control','min'=>'0']) }}</td>
				</tr>
				<tr>
					<th>Population</th>
					<td>
						<table style="width: 100%">
							<tr>
								
								<th>Male (% of total)</th>
								<th>Female (% of total)</th>
								<th>Total</th>
							</tr>
							<tr>
								
								<td>{{ Form::input('number', 'percent_male', $profile->percent_male, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
								<td>{{ Form::input('number', 'percent_female', $profile->percent_female, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
								<td>{{ Form::input('number', 'population', $profile->population, ['class' => 'form-control population', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
								<td class="population"></td>

							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<div class="col-md-6 ">
			
			<table class="table table-bordered table-striped">
				<tr>
					<th width="30%">Sub Project Name</th>
					 @if (isset($profile->mibf_refno) && $profile->mibf_refno !== "")
					    <td>{{ Form::text('project_name', $profile->project_name, ['class' => 'form-control', 'required', 'readonly'=>'readonly']) }}</td>
					 @else
					 	<td>{{ Form::text('project_name', $profile->project_name, ['class' => 'form-control', 'required']) }}</td>
					 @endif
				</tr>
				<tr>
					<th>Sub Project Category</th>
					<td>{{ Form::select('proj_subcategory', [''=>'Select Sub Project Category']+$subcategories, $profile->proj_subcategory, ['class' => 'form-control', 'required']) }}</td>
				</tr>
				<tr class="hidden">
					<th width="30%">Project Statement</th>
					<td>{{ Form::textarea('proj_statement', $profile->proj_statement, ['class' => 'form-control', '']) }}</td>
				</tr>
			</table>
		</div>
		<div class="col-md-6 ">
			
			<table class="table table-bordered table-striped">
				<tr class="hidden">
					<th width="30%">Project Purpose</th>
					<td>{{ Form::textarea('proj_purpose', $profile->proj_purpose, ['class' => 'form-control']) }}</td>
				</tr>
				<tr>
					<th width="30%">Date Started</th>
					<td>{{ Form::input('text', 'date_started', isset($profile->date_started) ? date("m/d/Y", strtotime($profile->date_started)) : null, array('class' => 'form-control date', '')) }}</td>
				</tr>
				<tr>
					<th width="30%">Planned Date of Completion</th>
					<td>{{ Form::input('text', 'planned_date_completed', isset($profile->planned_date_completed) ? date("m/d/Y", strtotime($profile->planned_date_completed)) : null, array('class' => 'form-control date', '')) }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		Description of Project Components
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="30%">Physical Target</th>
					<td>
						<div class="input-group">
							{{ Form::input('number', 'phys_target', $profile->phys_target, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}
							{{ Form::hidden('phys_target_unit', $profile->phys_target_unit)}}
							<span class="input-group-addon">
								{{ $profile->phys_target_unit }}
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<th width="30%">Cost Parameter</th>
					<td>{{ Form::input('number', 'cost_parameter', $profile->cost_parameter, ['class' => 'form-control', 'min'=>'0', 'step'=>'any']) }}</div>
					</td>
				</tr>
				<tr>
					<th>Labor/Workforce Requirements/Source</th>
					<td>
						<table style="width: 100%">
							<tr>
								<th></th>
								<th>Male</th>
								<th>Female</th>
							</tr>
							<tr>
								<td>Skilled</td>
								<td>{{ Form::input('number', 'skilled_male', $profile->skilled_male, ['class' => 'form-control', 'min'=>'0']) }}</td>
								<td>{{ Form::input('number', 'skilled_female', $profile->skilled_female, ['class' => 'form-control', 'min'=>'0']) }}</td>
							</tr>
							<tr>
								<td>Non-skilled</td>
								<td>{{ Form::input('number', 'nonskilled_male', $profile->nonskilled_male, ['class' => 'form-control', 'min'=>'0']) }}</td>
								<td>{{ Form::input('number', 'nonskilled_female', $profile->nonskilled_female, ['class' => 'form-control', 'min'=>'0']) }}</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		Financial Economic Aspect
	</div>
	<!-- <div class="panel-body" id="financial_aspect"> -->
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="30%">Cost Items Per Component</th>
					
					<th>CDD Grant</th>
					<th>Total LCC</th>
					<th>Total Cost</th>
				</tr>
				<tr>
					<td>Total Estimated Cost - POW (Infrastructure)</td>
					<td>{{ Form::input('number', 'grant_infra', $profile->grant_infra, ['class' => 'form-control', 'step' => 'any']) }}</td>
					<td>{{ Form::input('number', 'lcc_infra', $profile->lcc_infra, ['class' => 'form-control', 'step' => 'any']) }}</td>
					<td>{{ Form::input('number', 'cost_infra', $profile->cost_infra, ['class' => 'form-control cost_infra', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
					<td class="cost_infra"></td>
				</tr>
				<tr>
					<td>Training</td>
					<td>{{ Form::input('number', 'grant_training', $profile->grant_training, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
					<td>{{ Form::input('number', 'lcc_training', $profile->lcc_training, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
					<td>{{ Form::input('number', 'cost_training', $profile->cost_training, ['class' => 'form-control cost_training','step' => 'any', 'readonly'=>'readonly']) }}</td>
					<td class="cost_training"></td>
				</tr>
				<tr>
					<td>Women-Specific</td>
					<td>{{ Form::input('number', 'grant_women', $profile->grant_women, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
					<td>{{ Form::input('number', 'lcc_women', $profile->lcc_women, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
					<td>{{ Form::input('number', 'cost_women', $profile->cost_women, ['class' => 'form-control cost_women', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
					<td class="cost_women"></td>
				</tr>
				<tr>
					<td>Management</td>
					<td>{{ Form::input('number', 'grant_mgmnt', $profile->grant_mgmnt, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
					<td>{{ Form::input('number', 'lcc_mgmnt', $profile->lcc_mgmnt, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
					<td>{{ Form::input('number', 'cost_mgmnt', $profile->cost_mgmnt, ['class' => 'form-control cost_mgmnt', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
					<td class="cost_mgmnt"></td>
				</tr>
				<tr>
					<td>Others</td>
					<td>{{ Form::input('number', 'grant_others', $profile->grant_others, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
					<td>{{ Form::input('number', 'lcc_others', $profile->lcc_others, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
					<td>{{ Form::input('number', 'cost_others', $profile->cost_others, ['class' => 'form-control cost_others', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
					<td class="cost_others"></td>
				</tr>
				<tr>
					<td class="text-right">Total</td>
					<td>{{ Form::input('number', 'total_cdd', $profile->total_cdd, ['class' => 'form-control total_cdd', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
					<td>{{ Form::input('number', 'total_lcc', $profile->total_lcc, ['class' => 'form-control total_lcc', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
					<td>{{ Form::input('number', 'total_cost', $profile->total_cost, ['class' => 'form-control total_cost', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
					
					<!-- <td>0</td>
					<td>0</td>	 -->
				</tr>
				<tr>
					<td class="text-right">Total (%)</td>
					<td>{{ Form::input('number', 'total_cdd_percent', $profile->total_cdd_percent, ['class' => 'form-control total_cdd_percent', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
					<td>{{ Form::input('number', 'total_lcc_percent', $profile->total_lcc_percent, ['class' => 'form-control total_lcc_percent', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
					<td>{{ Form::input('number', 'total_cost_percent', $profile->total_cost_percent, ['class' => 'form-control total_cost_percent', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
					
					<!-- <td>100%</td>	
					<td>0%</td>
					<td>0%</td> -->
				</tr>
			</table>
		</div>
	</div>
</div>
<div class="col-md-12 ">
<div class="form-group pull-right">
                <label>Save as draft</label>
                <input type="checkbox" name="is_draft" {{ $profile->is_draft==1 ? 'checked': '' }}>
              </div>

          </div>
<!-- {{ Form::submit('Save Sub Project', array('class' => 'btn btn-primary')) }} -->
<div class="col-md-12 ">
									<div class="form-group pull-right">
						                <button class="btn btn-primary" id="save">
											<i class="fa fa-save"></i>
											Submit 
										</button>
		                 				{{ HTML::linkRoute('spi_profile.index', 'Cancel',
  null, array('class' => 'btn btn-default')) }}	
									</div>
								</div>
								<div class="clearfix"></div>

{{ Form::close() }}

<script>
	$(document).ready(function() {
		// add styling
		$("textarea").css("resize", "none");
		$("table table tr td").css("padding-bottom", "5px");
		$("table table tr td").css("padding-left", "5px");
		$("table table tr th").css("padding-bottom", "5px");
		$("table table tr th").css("padding-left", "5px");

		// prepare subcategory
		$("select[name='proj_subcategory']").change(function() {
			get_subcategory_unit($(this).val());
		});
		get_subcategory_unit($("select[name='proj_subcategory']").val());

		// prepare financial aspect elements
		$("#financial_aspect table td input").change(function() {
			calculate_financial_aspect_total();
		});
		calculate_financial_aspect_total();
	});

	function get_subcategory_unit(sub_category) {
		var root_url = "<?php echo Request::root(); ?>/"; // put this in php file
		$.post(
			root_url + "spi_category/unit",
			{ 
				"sub_category": sub_category,
				"_token": $("form input[name=_token]").val()
			},
			function(data) {
				$("form span.input-group-addon").html(data.unit);
				$("form input[name='phys_target_unit']").val(data.unit);
			},
			"json"
		);
	}

	function calculate_financial_aspect_total() {
		var parent = $("#financial_aspect table");
		var total_list = [];

		var tr = $(parent).find("tr");
		var num_rows = $(tr).length;
		for (var col = 2; col <= $(tr[0]).find("th").length; col++) {
			var total = 0;
			var row = 0;
			for (row = 1; row < num_rows - 2; row++) {
				total += round_off(parseFloat($(tr[row]).find("td:nth-child(" + col + ") input").val() || 0), 2);
			}
			$(parent).find("tr:nth-child(" + (row+1) + ") td:nth-child(" + col + ")").html(total);
			total_list.push(total);
		}

		var percent_row = $(parent).find("tr:nth-child(" + num_rows + ")")[0];
		var total_cost = total_list[0];
		
		console.log(total_list);
		for (var i = 1; i < total_list.length; i++) {
			var percentage = 0;

			if (total_cost != 0) {
				percentage = round_off((total_list[i] / total_cost) * 100, 2);
			}
			
			$(percent_row).find("td:nth-child(" + (i+2) + ")").html(percentage + "%");
		}
	}

	function round_off(num, places) {
		var dec = Math.pow(10, places); 
  	return Math.round(num * dec)/dec;
	}


	/* get the total in male and female */
			var total = (function($){
				return {
					
					totalPersonMale : function(){
			            var thisElement = $(this).val();
			            $("input[name='percent_female']").val(100 - thisElement);
			            var total_ =  ( parseFloat($("input[name='percent_male']").val() ) | 0 ) + ( parseFloat($("input[name='percent_female']").val() ) | 0 );
			            $('.population').val(total_ || '0');

			          },
			          totalPersonFemale : function(){
			            var thisElement = $(this).val();
			            $("input[name='percent_male']").val(100 - thisElement);
			           var total_ =  ( parseFloat($("input[name='percent_male']").val() ) | 0 ) + ( parseFloat($("input[name='percent_female']").val() ) | 0 );
			            $('.population').val(total_ || '0');

			          },
					totalFEA : function(){
						var total_ = parseFloat($("input[name='grant_infra']").val() )  + parseFloat($("input[name='lcc_infra']").val() )  ;
						$('.cost_infra').val(total_ || '0');
					},

					totalTraining : function(){
						var total_ = parseFloat($("input[name='grant_training']").val() )  + parseFloat($("input[name='lcc_training']").val() ) ;
						$('.cost_training').val(total_ || '0');

					},
					totalWomen : function(){
						var total_ = parseFloat($("input[name='grant_women']").val()  )  + parseFloat($("input[name='lcc_women']").val() )  ;
						$('.cost_women').val(total_ || '0');
						total.totalCDD_PERCENT();
						total.totalLCC_PERCENT();
						total.totalCOST();
						total.totalLCOST_PERCENT();
					},
					totalManagement : function(){
						var total_ = parseFloat($("input[name='grant_mgmnt']").val()  )  + parseFloat($("input[name='lcc_mgmnt']").val() )  ;
						$('.cost_mgmnt').val(total_ || '0');
						total.totalCDD_PERCENT();
						total.totalLCC_PERCENT();
						total.totalCOST();
						total.totalLCOST_PERCENT();
					},

					totalOthers : function(){
						var total_ = parseFloat($("input[name='grant_others']").val() ) + parseFloat($("input[name='lcc_others']").val() )  ;
						$('.cost_others').val(total_ || '0');
						total.totalCDD_PERCENT();
						total.totalLCC_PERCENT();
						total.totalCOST();
						total.totalLCOST_PERCENT();
					},

					totalCDD : function(){
						var total_ = parseFloat($("input[name='grant_infra']").val() ) 
						+ parseFloat($("input[name='grant_training']").val()) 
						+ parseFloat($("input[name='grant_women']").val()) 
						+ parseFloat($("input[name='grant_mgmnt']").val()) 
						+ parseFloat($("input[name='grant_others']").val())  ;
						$('.total_cdd').val(total_ || '0');
					},

					totalLCC : function(){
						var total_ = parseFloat($("input[name='lcc_infra']").val() ) 
						+ parseFloat($("input[name='lcc_training']").val() ) 
						+ parseFloat($("input[name='lcc_women']").val() ) 
						+ parseFloat($("input[name='lcc_mgmnt']").val() ) 
						+ parseFloat($("input[name='lcc_others']").val() )  ;
						$('.total_lcc').val(total_ || '0');
					},

					totalCDD_PERCENT : function(){
						// var total_ = parseFloat($("input[name='total_cdd']").val()) / parseFloat($("input[name='total_cost']").val());
						//cdd total
						var total_cdd = parseFloat($("input[name='grant_infra']").val())  
												+ parseFloat($("input[name='grant_training']").val()) 
												+ parseFloat($("input[name='grant_women']").val()) 
												+ parseFloat($("input[name='grant_mgmnt']").val()) 
												+ parseFloat($("input[name='grant_others']").val()) ;
						/* lcc total*/
						var total_lcc = parseFloat($("input[name='lcc_infra']").val())  
												+ parseFloat($("input[name='lcc_training']").val()) 
												+ parseFloat($("input[name='lcc_women']").val()) 
												+ parseFloat($("input[name='lcc_mgmnt']").val()) 
												+ parseFloat($("input[name='lcc_others']").val())  ;
						var total_cost = total_cdd + total_lcc;
						var cdd_percent = ( total_cdd / total_cost ) * 100;
						$('.total_cdd_percent').val(cdd_percent.toFixed(2));
					},

					totalLCC_PERCENT : function(){
						// var total_ = parseFloat($("input[name='total_cdd']").val()) / parseFloat($("input[name='total_cost']").val());
						//cdd total
						var total_cdd = parseFloat($("input[name='grant_infra']").val()) 
												+ parseFloat($("input[name='grant_training']").val())
												+ parseFloat($("input[name='grant_women']").val())
												+ parseFloat($("input[name='grant_mgmnt']").val())
												+ parseFloat($("input[name='grant_others']").val()) ;
						/* lcc total*/
						var total_lcc = parseFloat($("input[name='lcc_infra']").val()) 
												+ parseFloat($("input[name='lcc_training']").val())
												+ parseFloat($("input[name='lcc_women']").val())
												+ parseFloat($("input[name='lcc_mgmnt']").val())
												+ parseFloat($("input[name='lcc_others']").val()) ;
						var total_cost = total_cdd + total_lcc;
						var lcc_percent = ( total_lcc / total_cost ) * 100;
						$('.total_lcc_percent').val(lcc_percent.toFixed(2));
					},

					totalCOST : function(){
						// var total_ = parseFloat($("input[name='total_cdd']").val()) / parseFloat($("input[name='total_cost']").val());
						//cdd total
						var total_cdd = parseFloat($("input[name='grant_infra']").val()) 
												+ parseFloat($("input[name='grant_training']").val())
												+ parseFloat($("input[name='grant_women']").val())
												+ parseFloat($("input[name='grant_mgmnt']").val())
												+ parseFloat($("input[name='grant_others']").val()) ;
						/* lcc total*/
						var total_lcc = parseFloat($("input[name='lcc_infra']").val()) 
												+ parseFloat($("input[name='lcc_training']").val())
												+ parseFloat($("input[name='lcc_women']").val())
												+ parseFloat($("input[name='lcc_mgmnt']").val())
												+ parseFloat($("input[name='lcc_others']").val()) ;
						var total_cost = total_cdd + total_lcc;

						// var lcc_percent = ( total_cdd / total_lcc ) * 100;
						$('.total_cost').val(total_cost.toFixed(2));
					},

					totalLCOST_PERCENT : function(){
						// var total_ = parseFloat($("input[name='total_cdd']").val()) / parseFloat($("input[name='total_cost']").val());
						//cdd total
						var total_cdd = parseFloat($("input[name='grant_infra']").val()) 
												+ parseFloat($("input[name='grant_training']").val()) 
												+ parseFloat($("input[name='grant_women']").val()) 
												+ parseFloat($("input[name='grant_mgmnt']").val()) 
												+ parseFloat($("input[name='grant_others']").val())  ;
						/* lcc total*/
						var total_lcc = parseFloat($("input[name='lcc_infra']").val())  
												+ parseFloat($("input[name='lcc_training']").val()) 
												+ parseFloat($("input[name='lcc_women']").val()) 
												+ parseFloat($("input[name='lcc_mgmnt']").val()) 
												+ parseFloat($("input[name='lcc_others']").val())  ;
						var total_cost = total_cdd + total_lcc;
						var cdd_percent = ( total_cdd / total_cost ) * 100;
						var lcc_percent = ( total_lcc / total_cost ) * 100;
						var total_cdd_lcc = cdd_percent + lcc_percent;
						$('.total_cost_percent').val(total_cdd_lcc.toFixed(2));
					}







				}
			})(jQuery);
                // total.totalPerson();
                total.totalFEA();
                total.totalTraining();
                total.totalWomen();
                total.totalManagement();
                total.totalOthers();
                total.totalCDD();
                total.totalLCC();
                // total.totalCDD_PERCENT();

			$("input[name='percent_male']").keyup(total.totalPersonMale);
			$("input[name='percent_female']").keyup(total.totalPersonFemale);

			$("input[name='grant_infra']").keyup(total.totalFEA);
			$("input[name='lcc_infra']").keyup(total.totalFEA);
			
			$("input[name='grant_training']").keyup(total.totalTraining);
			$("input[name='lcc_training']").keyup(total.totalTraining);
			
			$("input[name='grant_women']").keyup(total.totalWomen);
			$("input[name='lcc_women']").keyup(total.totalWomen);

			$("input[name='grant_mgmnt']").keyup(total.totalManagement);
			$("input[name='lcc_mgmnt']").keyup(total.totalManagement);
			
			$("input[name='grant_others']").keyup(total.totalOthers);
			$("input[name='lcc_others']").keyup(total.totalOthers);

			$("input[name='grant_infra']").keyup(total.totalCDD);
			$("input[name='grant_training']").keyup(total.totalCDD);
			$("input[name='grant_women']").keyup(total.totalCDD);
			$("input[name='grant_mgmnt']").keyup(total.totalCDD);
			$("input[name='grant_others']").keyup(total.totalCDD);

			$("input[name='lcc_infra']").keyup(total.totalLCC);
			$("input[name='lcc_training']").keyup(total.totalLCC);
			$("input[name='lcc_women']").keyup(total.totalLCC);
			$("input[name='lcc_mgmnt']").keyup(total.totalLCC);
			$("input[name='lcc_others']").keyup(total.totalLCC);

			$("input[name='date_started']").change( function(){
				var  el =$("input[name='date_started']").val();
				var plan = 	$("input[name='planned_date_completed']").val();
				// if( el >= plan ){
				// 	$("input[name='planned_date_completed']")[0].setCustomValidity('Must be later than Date Started');
				// }
				// else 
				// {
				// 	$("input[name='planned_date_completed']")[0].setCustomValidity('');

				// }
			} );

			$("input[name='planned_date_completed']").change( function(){
				var  el = $("input[name='date_started']").val();
				var plan = 	$("input[name='planned_date_completed']").val();
				// if( el >= plan ){
				// 	$("input[name='planned_date_completed']")[0].setCustomValidity('Must be later than Date Started');
				// }
				// else 
				// {
					
				// 	$("input[name='planned_date_completed']")[0].setCustomValidity('');

					
				// }
			} );


			// $("input[name='total_cdd_percent']").change(total.totalCDD_PERCENT);
			// $("input[name='total_lcc_percent']").change(total.totalCDD_PERCENT);
			 // $(document).ready(function() {
      		$("select[name='proj_subcategory']").change(function() {
         		// alert($(this).val());
         if($(this).val()=="Skills Training"){
        
            $("input[name='skilled_male']").attr('disabled','disabled');
            $("input[name='skilled_female']").attr('disabled','disabled');
            $("input[name='nonskilled_male']").attr('disabled','disabled');
            $("input[name='nonskilled_female']").attr('disabled','disabled');
     		}

           
	});
			
</script>