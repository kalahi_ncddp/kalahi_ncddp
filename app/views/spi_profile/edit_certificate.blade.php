@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('spi_profile') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')

{{ Form::open(array('route' => array('spi_profile.update_certificate', $profile->project_id), 'method' => 'patch', 'role' => 'form')) }}

<div class="panel panel-default">
  <div class="panel-heading">
    General
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <div class="col-md-6 ">

      <table class="table table-bordered table-striped">
        <tr>
          <th width="30%">Name of Sub Project</th>
          <td>
            {{ $profile->project_name }}
          </td>
        </tr>
        <tr>
          <th width="30%">GPS Latitude</th>
          <td>{{ Form::text('gps_latitude', $profile->gps_latitude, ['class' => 'form-control']) }}</td>
        </tr>
        <tr>
          <th width="30%">GPS Longitude</th>
          <td>{{ Form::text('gps_longitude', $profile->gps_longitude, ['class' => 'form-control']) }}</td>
        </tr>
        <tr>
          <th width="30%">Project Category</th>
          <td>
            {{ $profile->proj_subcategory }}
          </td>
        </tr>
      </table>
    </div>

    <div class="col-md-6 ">
      
      <table class="table table-bordered table-striped">
        <tr>
          <th width="30%">Physical Measurement</th>
          <td>
            {{ $profile->phys_target . " " . $profile->phys_target_unit }}
          </td>
        </tr>
          <tr>
          <th width="30%">Date</th>
          <td>
         
          {{ Form::input('date', 'project_date', Input::old('project_date'), array('class' => 'form-control','max' => date("m/d/Y"))) }}
           </tr>

           <tr>
            <th width="30%">With approved Final Inspection Report ?</th>
            <td>{{ Form::select('is_final_inspection',[''=>'Yes/No','1'=>'Yes','0'=>'No'],$profile->is_final_inspection) }}</td>
           </tr>
        </div>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
 

<div class="col-md-12 ">
            <div class="form-group pull-right">
{{ Form::submit('Save Certificate', array('class' => 'btn btn-primary')) }}
                           <!--  <button class="btn btn-primary" id="save">
                      <i class="fa fa-plus"></i>
                      Add Municipal Training 
                    </button> -->
        {{ HTML::linkRoute('spi_profile.show', 'Cancel',
  array($profile->project_id), array('class' => 'btn btn-default')) }}
                    
            </div>
          </div>
                          
            <div class="clearfix"></div>{{ Form::close() }}
@stop