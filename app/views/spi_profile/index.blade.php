@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')	
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
	@endif
	<div class="panel panel-default">
		<div class="panel-heading">
			  {{ $title }}
			@if($position->position=='ACT')
                    <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                    <span class="hidden module">spi_profile</span>
             @endif
			{{ HTML::linkRoute('spi_profile.build', 'New Sub Project',
        null, array('class' => 'btn btn-info btn pull-right')) }}
        	
			<span class="btn  btn-primary pull-right"   style="margin:0 10px" data-toggle="modal" data-target="#mibf_projects">
				<i class="fa fa-plus"></i> Add from MIBF
			</span>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<div class="col-xs-6 pull-right">
							<div class="dataTables_filter" id="dataTables-example_filter">
								<label>Search: 
									<input type="text" aria-controls="dataTables-example" value="{{ Input::get('search','') }}" class="search">
								</label>
							</div>
						</div>
				<table class="table table-striped table-bordered table-hover data-table">
					<thead>
						<tr>
							 <th>
                           @if($position->position=='ACT')
                            <input type="checkbox" id="selecctall"/>
                            @endif
                            </th>
							<th width="10%">Lead Barangay</th>
							<th width="30%">Project Title</th>
							<th width="10%">Sub-Category</th>
							<th width="10%">Start Date</th>
							<th width="15%">Completion Date</th>
							<th>Fund Source</th>
							<th>Cycle</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($projects as $spi_profile)
						<tr>
							<td>
                               @if($position->position=='ACT')
                                  @if($spi_profile->is_draft==0)
                                    {{ $reference->approval_status($spi_profile->project_id, 'ACT') }}
                                  @else
                                      <span class="label label-warning">draft</span>
                                  @endif
                                @endif
                            </td>
							<td>{{ Report::get_barangay($spi_profile->barangay_id()) }}</td>
							<td>{{ $spi_profile->project_name }}</td>
							<td>{{ $spi_profile->proj_subcategory }}</td>
							<td>{{ toDate($spi_profile->date_started) }}</td>
							<td>{{ toDate($spi_profile->date_completed) }}</td>
							<td>{{ $spi_profile->program_id }}</td>
							<td>{{ $spi_profile->cycle_id }}</td>

							<td>
								@if($spi_profile->is_draft==0)
		                            <a class="btn btn-success btn" href="{{ URL::to('spi_profile/' .$spi_profile->project_id) }}">
		                            <i class="fa fa-eye"></i>  View Details 
		                            </a>
		                        @else
		                        	<a class="btn btn-warning btn" href="{{ URL::to('spi_profile/' .$spi_profile->project_id) }}">
		                             <i class="fa fa-eye"></i> View Details ( draft )
		                            </a>
								@endif
	                    
	                        </td>
					
						</tr>
						@endforeach
					</tbody>
				</table>
							<div class="pull-right">
								{{ $projects->links(); }}
							</div>

			</div>
		</div>
		<div class="panel-footer text-right">
		</div>
	</div>

	@include('modals.mibf_projects')
		
	 <style type="text/css" media="screen">
    table { border-collapse: collapse; border: solid thin black; }
    table th { cursor: move; }

    movable-columns-hover { background-color: #fff7d9; }
  </style>
  <script type="text/javascript">
    $(function() {
     // click also work, so that you can still sort your column:
      $('th').click(function(){ window.console && console.log('clicked') });
      	$('.search').keypress(function(e){
      			var key = e.keycode || e.which
      			if(key == 13){
      				var search_value = $(this).val();
      				window.location.href="{{ URL::to('/spi_profile') }}?search="+search_value;
      			}
      		});
    });
  </script>
@stop