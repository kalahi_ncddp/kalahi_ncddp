@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('spi_profile') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    {{ HTML::linkRoute('spi_profile.show', 'Back to Sub Project Profile',
      array($profile->project_id), array('class' => 'btn btn-default btn')) }}
         @if( !is_review($profile->project_id) )
    {{ HTML::linkRoute('spi_profile.edit_certificate', 'Edit',
      array($profile->project_id), array('class' => 'btn btn-info btn')) }}
      @endif
  </div>
</div>

<hr />

<div class="panel panel-default">
  <div class="panel-heading">
    General
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <div class="col-md-6 ">

      <table class="table table-bordered table-striped">
        <tr>
          <th width="30%">Name of Sub Project</th>
          <td>
            {{ $profile->project_name }}
          </td>
        </tr>
        <tr>
          <th width="30%">GPS Latitude</th>
          <td>
            {{ $profile->gps_latitude }}
          </td>
        </tr>
        <tr>
          <th width="30%">GPS Longitude</th>
          <td>
            {{ $profile->gps_longitude }}
          </td>
        </tr>
        
      <table class="table table-bordered">
         <tr>
            <th width="30%">With approved Final Inspection Report ?</th>
        <td>{{ Form::select('is_final_inspection',[''=>'Yes/No','1'=>'Yes','0'=>'No'],$profile->is_final_inspection,['readonly','disabled']) }}</td>
       </tr>
      </table>
      </table>
    </div>
      <div class="col-md-6 ">
      <table class="table table-bordered table-striped">

        <tr>
          <th width="30%">Project Category</th>
          <td>
            {{ $profile->proj_subcategory }}
          </td>
        </tr>
        <tr>
          <th width="30%">Physical Measurement</th>
          <td>
            {{ $profile->phys_target . " " . $profile->phys_target_unit }}
          </td>
        </tr>

        <tr>
          <th width="30%">Date</th>
          <td>
            {{ toDate($profile->project_date) }}
          </td>
        </tr>
       
      </table>
     
    </div>
  </div>
</div>

@stop