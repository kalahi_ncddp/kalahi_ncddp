<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- BOOTSTRAP STYLES-->
    {{ HTML::style('/public/css/bootstrap.css') }}
    <!-- FONTAWESOME STYLES-->
    {{ HTML::style('/public/css/font-awesome.css') }}
    <!-- MORRIS CHART STYLES-->
    {{ HTML::style('/public/js/morris/morris-0.4.3.min.css') }}
    <!-- CUSTOM STYLES-->
    {{ HTML::style('/public/css/custom.css') }}
    <!-- GOOGLE FONTS-->
    {{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans') }}
    {{ HTML::style('/public/css/jquery.datetimepicker.css') }}
    {{ HTML::script('/public/js/jquery.js') }}
    {{ HTML::script('/public/js/jquery.datetimepicker.js') }}
    <title>{{ $title }}</title>
</head>

<body>
    <div class="container">
        <div class="text-center">
            @if ($errors->all())
                    <div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
                @endif
            <img src="{{ URL::to('public/img/KALAHI_CIDSS-NCDDP_Logo3.jpg') }}" align="middle" width="200px"/>
            <h1><strong>KC-NCDDP Database System <i class="small">{{$version}} full version</i></strong></h1>
            
            @if(Session::get('updated'))
            <div class="text-center">
                <div class="alert alert-info">System update completed you may login</div>
            </div>
            @endif   
            @if(Session::has('updated_not'))
            <div class="text-center">
                <div class="alert alert-danger">System update failed</div>
            </div>
            @endif      
            <br>          
                        <button class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
            Log In
            </button>

        </div>
    </div>

    <div class="panel-body">
        
        <span style="font-size: 20px" class="is_chrome hidden">this system requires Google Chrome browser </span>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>


                        
                        <h4 class="modal-title" id="myModalLabel">Log In</h4>
                    </div>
                    <div class="modal-body hidden">
                        {{ Form::open(array('url' => 'reports', 'method' => 'POST', 'role' => 'form', 'class' => 'form-signin')) }}
                        <div class="form-group">
                            <input class="form-control" name="username" type="username" id="username" placeholder="Username" required autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control" name="password" type="password" id="password" placeholder="Password" required>
                        </div>
                        <div class="alert alert-info"><h6>Logging in...</h6></div>
                        <div class="alert alert-success"><h6>Success...</h6></div>
                        <div class="alert alert-danger"><h6></h6></div>
                        <span class="btn btn-success regular">Login As Regular</span>
                        <span class="btn btn-danger accelerated">Login As Accelerated</span>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
     <script type="text/javascript">
    $(function(){
            var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
            if(!is_chrome){

                $('.is_chrome').removeClass('hidden');
            }else{
                $('.modal-body').removeClass('hidden');
            }
            $('.panel-body .alert-success').hide();
             $('.panel-body .alert-info').hide();
             $('.panel-body .alert-danger').hide();
            $('.regular').on('click',function(){
                             $('.alert-info').show();
                                            $.post('auth/login',{ username:$("#username").val(), password:$("#password").val() ,'mode':0 } ,function (response){
                                                if(response.status == 'ok'){
                                                    $('.panel-body .alert-info').hide();
                                                    $('.panel-body .alert-success').show();
                                                    window.location.href="{{ URL::to('reports')}}";
                                                }else{
                                                      $('.alert-info').hide();
                                                      $('.alert-success').hide();

                                                      $('.alert-danger').show().find('h6').text('Check you username and password');
                                                }
                            });
            })
            $('.accelerated').on('click',function(){
                             $('.alert-info').show();
                                            $.post('auth/login',{ username:$("#username").val(), password:$("#password").val() ,'mode':1 } ,function (response){
                                                if(response.status == 'ok'){
                                                    $('.alert-info').hide();
                                                    $('.alert-success').show();
                                                    window.location.href="{{ URL::to('reports')}}";
                                                }else{
                                                      $('.alert-info').hide();
                                                      $('.alert-success').hide();

                                                      $('.alert-danger').show().find('h6').text('Check you username and password');
                                                }
                            });
            })
            $('form').submit(function(e){
                e.preventDefault();
                $('.alert-info').show();
                $.post('auth/login',{ username:$("#username").val(), password:$("#password").val() ,'mode':$( "input:checked" ).length } ,function (response){
                    if(response.status == 'ok'){
                        $('.alert-info').hide();
                        $('.alert-success').show();
                        window.location.href="{{ URL::to('reports')}}";
                    }else{
                          $('.alert-info').hide();
                          $('.alert-success').hide();

                          $('.alert-danger').show().find('h6').text('Check you username and password');
                    }
                });
            });
    });

    </script>
     <!-- /. WRAPPER  -->
    {{ HTML::script('public/js/jquery-1.10.2.js') }}
    {{ HTML::script('public/js/bootstrap.min.js') }}
    {{ HTML::script('public/js/bootstrap.js') }}
    {{ HTML::script('public/js/jquery.metisMenu.js') }}
    {{ HTML::script('public/js/morris/raphael-2.1.0.min.js') }}
    {{ HTML::script('public/js/morris/morris.js') }}
    {{ HTML::script('public/js/custom.js') }}
    
</body>
</html>