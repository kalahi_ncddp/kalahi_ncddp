@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')



		    	<div class="panel panel-default">
		    		<div class="panel-heading">
		    			{{ $title }}
		    			@if($position->position!='encoder')
                            <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                            <span class="hidden module">mibf</span>
                        @endif
		    			<!-- <a class="btn  btn-primary pull-right" href="{{ URL::to('mibf/create') }}">
						 Add New
						</a> -->
						<span class="btn  btn-primary pull-right" data-toggle="modal" data-target="#add_mibf">
							<i class="fa fa-plus"></i> Add from Municipal Trainings
						</span>
						<div class="clearfix"></div>
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
									@if($position->position!='encoder')
                                        <th>Select all
                                            <input type="checkbox" id="selecctall"/>
                                        </th>
                                        @endif
										<th>Region</th>
										<th>Province</th>
										<th>Municipality</th>
										<th>Cycle</th>
										<th>Date Conducted</th>
										<th>Purpose</th>
										<th>Program</th>
										<th>Details</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data as $mibf)
									<tr>
									    <td>
									     @if($position->position!='encoder')
                                           @if($mibf->is_draft==0)
                                             {{ $reference->approval_status($mibf->activity_id , 'ACT') }}
                                           @else
                                               <span class="label label-warning">draft</span>
                                           @endif
                                         @endif
									    </td>
			                        	<td>
											{{ $region = Report::get_region($mibf->psgc_id) }}
										</td>
										<td>
											{{ $province = Report::get_province($mibf->psgc_id) }}
										</td>
										<td>
											{{ $municipality = Report::get_municipality($mibf->psgc_id) }}
										</td>


										<td>
											{{ $mibf->cycle_id }}
										</td>
										<td>
											{{ $startdate = Report::get_date_conducted($mibf->activity_id)}}
										</td>
										<td>
											{{ $mibf->purpose }}
										</td>
										<td>
											{{ $mibf->program_id }}
										</td>
										
										 <td>
				                        @if($mibf->is_draft==0)
                                            <a class="btn btn-success" href="{{ URL::to( 'mibf/'.$mibf->activity_id ) }}">View Details
                                            {{ hasComment($mibf->activity_id) }}
                                            </a>

                                        @else
                                         <a class="btn btn-warning" href="{{ URL::to( 'mibf/'.$mibf->activity_id.'/edit' ) }}">View Details( draft )
                                            {{ hasComment($mibf->activity_id) }}
                                         </a>

				                        @endif

                                  		</td>

									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>

	@include('modals.add_mibf')
	<script>
      $(document).ready(function () {
       $('#dataTables-example').dataTable(
              {
                  "aoColumnDefs": [
                            { 'bSortable': false, 'aTargets': [ 0 ] }
                         ]
              }
              )

              $('#selecctall').click(function(event) {  //on click
                  if(this.checked) { // check select status
                      $('.checkbox1').each(function() { //loop through each checkbox
                          this.checked = true;  //select all checkboxes with class "checkbox1"
                      });
                  }else{
                      $('.checkbox1').each(function() { //loop through each checkbox
                          this.checked = false; //deselect all checkboxes with class "checkbox1"
                      });
                  }
              });

              $('#approved').on('click',function(){
                     var c = confirm('Approve the selected documents?')
                     if(c)
                     {
                         var arr = [];
                         $('.checkbox1').each(function(i) {
                              arr[i] = [$(this).val(),$(this).data('purpose')];
                         });
                         $.post('{{ URL::to('') }}/mibf/approved',{'check':arr},function(res){
                             console.log(res);
                         });
                     }
                     else
                     {

                     }
              });

              $.fn.use_mibf = function(vol){
					var data = {
						cycle_id : vol.find(".cycle_id").text(),
						program_id : vol.find(".program_id").text(),
						psgc_id	: vol.find(".psgc_id").text(),
						start_date :vol.find(".start_date").text(),
						end_date : vol.find(".end_date").text(),
						purpose : vol.find('.purpose').text()
					};
					$('.modal').loading(true);
					$.post('{{ URL::to('mibf') }}',data,function(res){
					$('.modal').loading(false);
					
						$("#myModal").modal('toggle');
							if(res.error){	
							window.location.reload();
						}else{
							window.location.href = '{{ URL::to('mibf') }}/' + res.id +'/edit';
						}
					});
				};
      });
    </script>
@stop