@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	
	    <div class="row">
		    <div class="col-md-12">
				<!-- Advanced Tables -->
	            <a class="btn btn-default" href="{{ URL::to('mibf/'.$description) }}">Go back</a>
				
		    	<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Records

                    @if($purpose!='Special')
		    			<a class="btn  btn-primary pull-right" href="{{ URL::to('mibf/' .$id.'/add_proposal/' .$purpose)}}">
							Add New
						</a>
                    @endif
						<div class="clearfix"></div>
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										@if($title == 'Municipal Forum')
										<th>Number</th>
										@else
										<th>Rank</th>
										@endif
										<th>Title of Proposal</th>
										<th>Lead Barangay</th>
										<th>Barangays Covered</th>
										<th> Grant</th>
										<th>LCC Amount</th>
										<th>Total Amount</th>
										<th>Priority (Y/N)</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
								@if($purpose=='Special')

									@foreach($data as $proposal)
									<tr>
										<td>
											{{ $proposal->rank }}
										</td>
										<td>
											{{ $proposal->project_name  }}
										</td>
										<td>
											{{ Report::get_barangay($proposal->lead_brgy) }}
										</td>
										<td>
											@foreach($proposal->projcoverage as $brgy)
													{{  Report::get_barangay($brgy->name) }},
											@endforeach
										</td>
										<td>
										    <?php
										        $kc_amount = isset($proposal->special->kc_amount) ? $proposal->special->kc_amount : $proposal->kc_amount;
										        $lcc_amount = isset($proposal->special->lcc_amount) ? $proposal->special->lcc_amount : $proposal->lcc_amount
										    ?>
											₱ {{ currency_format(floatval($kc_amount)) }}
										</td>
										<td>
											₱{{ currency_format(floatval($lcc_amount)) }}
										  </td>
										<td>
											₱{{ currency_format(floatval($kc_amount) + floatval($lcc_amount))}}
										</td>
										<?php
										    $priority = isset($proposal->special->priority) ? $proposal->special->priority : $proposal->priority;
										    $project_id =  isset($proposal->special->project_id)? $proposal->special->project_id : $proposal->project_id;
										 ?>
										@if($priority == 0)
											<td>
												No
					                        </td>
					                    @else
					                    	<td>
												Yes
				                        	</td>
				                        @endif
				                        <td>
				                        	<a class="btn  btn-small btn-info" href="{{ URL::to('mibf/' .$project_id.'/edit_proposal/' .$purpose) }}">
				 								Edit
				 							</a>
				                        </td>
				                        <td>
				                        	{{ Form::open(array('url' => 'mibf/delete', 'method' => 'DELETE', 'style' => 'display: inline;')) }}
											{{ Form::hidden('id', $project_id) }}
											{{ Form::hidden('action', 'proposal') }}
											{{ Form::hidden('purpose', $purpose) }}
											{{ Form::hidden('activity_id', $proposal->activity_id) }}
											{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
											{{ Form::close() }}
				                        </td>
									</tr>
									@endforeach
								@else

									@foreach($data as $proposal)
									<tr>
										<td>
											{{ $proposal->rank }}
										</td>
										<td>
											{{ $proposal->project_name  }}
										</td>
										<td>
											{{ Report::get_barangay($proposal->lead_brgy) }}
										</td>
										<td>
											@foreach($coverage as $brgy)
												@if($proposal->project_id == $brgy->project_id)
													{{ Report::get_barangay($brgy->name) }},
												@endif
											@endforeach
										</td>
										<td>
											₱ {{ currency_format($proposal->kc_amount) }}
										</td>
										<td>
											₱{{ currency_format($proposal->lcc_amount) }}
										  </td>
										<td>
											₱{{ currency_format(floatval($proposal->kc_amount)+floatval($proposal->lcc_amount))}}
										</td>
										@if($proposal->priority == 0)
											<td>
												No
					                        </td>
					                    @else
					                    	<td>
												Yes
				                        	</td>
				                        @endif
				                        <td>
				                        	<a class="btn  btn-small btn-info" href="{{ URL::to('mibf/' .$proposal->project_id.'/edit_proposal/' .$purpose) }}">
				 								Edit
				 							</a>
				                        </td>
				                        <td>
				                        	{{ Form::open(array('url' => 'mibf/delete', 'method' => 'DELETE', 'style' => 'display: inline;')) }}
											{{ Form::hidden('id', $proposal->project_id) }}
											{{ Form::hidden('action', 'proposal') }}
											{{ Form::hidden('purpose', $purpose) }}
											{{ Form::hidden('activity_id', $proposal->activity_id) }}
											{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
											{{ Form::close() }}
				                        </td>
									</tr>
									@endforeach
								@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop