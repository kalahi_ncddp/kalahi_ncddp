@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('mibf') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
        @if($mibf->is_draft==1)
        		 	<div class="alert alert-info">This is still a draft  <a href="{{ URL::to('mibf/' .$mibf->activity_id.'/edit') }}">edit the document</a> to set it in final draft</div>
         @endif
		<div class="row">
	        <div class="col-md-12">
				<a class="btn  btn-default" href="{{ URL::to('mibf') }}">Go Back</a>
		 		<div class="pull-right">
	    	@if( !is_review($mibf->activity_id) )
		 			
				 {{ Form::open(array('url' => 'mibf/' . $mibf->activity_id)) }}
				 	<a class="btn  btn-small btn-info" href="{{ URL::to('mibf/' .$mibf->activity_id.'/edit') }}">
				 		Edit 
				 	</a>

					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
					@endif
					@if($mibf->purpose == "Prioritization" || $mibf->purpose == "Municipal Forum" || $mibf->purpose == "Special")
						<a class="btn  btn-small btn-info" href="{{ URL::to('mibf/' .$mibf->activity_id.'/show_proposals/' .$mibf->purpose) }}">
					 		Rankings
					 	</a>
					 	{{--<a class="btn  btn-small btn-info" href="{{ URL::to('mibf/' .$mibf->activity_id.'/show_proposals/Special') }}">--}}
				 			{{--Special MIBF--}}
				 		{{--</a>--}}
				 	@endif
				 	 <span class="btn bg-olive" data-toggle="modal" data-target="#pincos_grievances">
			        	PINCOS and Grievances
			        </span>
			        <a class="btn bg-navy" href="{{ URL::to('mibf') }}">Close </a>
				 {{ Form::close() }}	 
		 		</div>
	        </div>
	    </div>

	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
		    		 {{ viewComment($mibf->activity_id) }}

                <div class="panel panel-default">
		    		<div class="panel-heading">
		    			Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
							 	 <tr>
					                <th>Municipality</th>
					                <td>{{ $municipality = Report::get_municipality($mibf->psgc_id) }}</td>
					            </tr>
					            <tr>
					                <th>KC Code</th>
					                <td></td>
					            </tr>
					            <tr>
					                <th>Province</th>
					                <td>{{ $province = Report::get_province($mibf->psgc_id) }}</td>
					            </tr>
					            <tr>
					                <th>Region</th>
					                <td>{{ $region = Report::get_region($mibf->psgc_id) }}</td>
					            </tr>
					            <tr>
					                <th>Start Date</th>
					                <td>{{ $startdate = Report::get_date_conducted($mibf->activity_id) }}</td>
					            </tr>
					            <tr>
					                <th>End Date</th>
					                <td>{{ $enddate = Report::get_end_date($mibf->activity_id) }}</td>
					            </tr>
					             <tr>
					                <th>Cycle</th>
					                <td>{{ $mibf->cycle_id }}</td>
					            </tr>
					              <tr>
					                <th>Program</th>
					                <td>{{ $mibf->program_id }}</td>
					            </tr>
					            <tr>
					                <th>Purpose</th>
					                <td>{{ $mibf->purpose }}</td>
					            </tr>
							</table>
						</div>
					</div>
				</div>
				@if($mibf->purpose == "Criteria Setting Workshop" || $mibf->purpose == "CSW")
					<div class="box box-primary">
			    		<div class="box-header">
			    			
			    			<h3 class="box-title">Criteria List</h3>
			    			<div class="box-tools pull-right">
			                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
			                </div>
			    		</div>
						<div class="box-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
						            	<th>Criterion</th>
						            	<th>Rate</th>
						            </tr>
						            @foreach($criteria as $criterion)
						            <tr>
						                <td>{{$criterion->criteria}}</td>
						                <td>{{$criterion->rate}}</td>
						            </tr>
						            @endforeach
								</table>
							</div>
						</div>
					</div>
				@endif
				
				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Barangay Representative Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					           <tr>
					            	<th>Barangay </th>
					                <th>1st Representative</th>
					                <th>2nd Representative</th>
					                <th>3rd Representative</th>
					            </tr>
					              @foreach($brgy_rep_data as $rep)
					              	<tr>
					            	<td>{{ Report::get_barangay($rep->barangay_psgc)}}</td>
					            	<td>{{ $rep->brgy_rep1}} </td>
					            	<td>{{ $rep->brgy_rep2}} </td>
					            	<td>{{ $rep->brgy_rep3}} </td>
					            	</tr>
								@endforeach
							</table>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Total Number of Barangays </th>
					                <td>{{ $total_barangays }}</td>
					            </tr>
					            <tr>
					                <th>Number of Barangay Represented</th>
					                <td>{{ $total_brgy_rep }}</td>
					        	</tr>
					                <th>Percentage of Barangay Represented</th>
					                <td>{{ ($total_brgy_rep/$total_barangays)*100 }}%</td>
					            </tr>
							</table>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Attendees
		    		</div>
		    		
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Attendees </th>
					                <th>Male</th>
					                <th>Female</th>
					                <th>Total</th>
					            </tr>
					         <?php try { ?>
					            <tr>
					            	<th>No. of Person </th>
					                <td>{{ $mibf->no_atnmale }}</td>
					                <td>{{ $mibf->no_atnfemale }}</td>

					                <td>{{ intval($mibf->no_atnmale) + intval($mibf->no_atnfemale) }}</td>
					            </tr>
					            <tr>
					                <th>No. of IP</th>
					                <td>{{ $mibf->getTraining()->total_ip('M')}}</td>
					                <td>{{ $mibf->getTraining()->total_ip('F')}}</td>
					                <td>{{ intval($mibf->getTraining()->total_ip('M')) + intval($mibf->getTraining()->total_ip('F'))}}</td>
					            </tr>
					                <th>No. of 60 yrs +</th>
					                <td>{{ $mibf->getTraining()->age_participants( 'M', 60)}}</td>
					                <td>{{ $mibf->getTraining()->age_participants('F', 60)}}</td>
					            	<td>{{ intval($mibf->getTraining()->age_participants('F', 60)) + $mibf->getTraining()->age_participants('M', 60) }}</td>
					            </tr>
					            
					            </tr>
					                <th>No. of LGU Officials</th>
					                <td>{{ $mibf->getTraining()->total_lguofficial('M')}}</td>
					                <td>{{ $mibf->getTraining()->total_lguofficial('F')}}</td>
					                <td>{{ intval($mibf->getTraining()->total_lguofficial('M')) +  intval($mibf->getTraining()->total_lguofficial('F'))}}</td>
					            </tr>

					             <tr>
					                <th>No. of Pantawid</th>
					                <td>{{ $mibf->getTraining()->total_pantawid('M') }}</td>
					                <td>{{ $mibf->getTraining()->total_pantawid('F') }}</td>
					                <td>{{ $mibf->getTraining()->total_pantawid('M')+$mibf->getTraining()->total_pantawid('F') }}</td>
					            </tr>

					            <tr>
					                <th>No. of Volunteer</th>
					                <td>{{ $mibf->getTraining()->total_male_vol('M') }}</td>
					                <td>{{ $mibf->getTraining()->total_female_vol('F') }}</td>
					                <td>{{ $mibf->getTraining()->total_male_vol('M')+$mibf->getTraining()->total_female_vol('F') }}</td>
					            <tr>
					            
					                <th>No. of Non Volunteer</th>
					                <td>{{ $mibf->getTraining()->total_non_male_vol('M') }}</td>
					                <td>{{ $mibf->getTraining()->total_non_female_vol('F') }}</td>
					                <td>{{ $mibf->getTraining()->total_non_male_vol('M')+$mibf->getTraining()->total_non_female_vol('F') }}</td>

					            </tr>

					           <?php }catch(Exception $e) {?>
					           	<tr>
					           		<td>No Training yet</td>
					           	</tr>
					           <?php }?>
							</table>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Other Sectors Represented
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								@foreach($sector_data as $sector)
					            <tr>
					                <td>{{$sector}}</td>
					            </tr>
					            @endforeach
							</table>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
													    		
			       
			    <!-- pincols and grivances modal -->
			@include('modals.pincos_grievances')

			        </div>
    			</div>						
			</div>
		</div>
	<script>
			$(document).ready(function(){
			//when the Add Filed button is clicked
			$("#add").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items").append('<div class="input-group"><input type="text" class="form-control" placeholder="Criterion" name="criteria[]" oninput="checkduplicate(this)"><input type="text" class="form-control" placeholder="Rate" name="rate[]" oninput="checkduplicate(this)"> <span class="input-group-addon delete"> <span class="fa fa-trash-o  "></span> </span></div><br>');
			});
			
			$("#add1").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items1").append('<br><div class="input-group"><input type="text" class="form-control" name="raise[]" oninput="checkduplicate1(this)" placeholder="Raised By" required><input type="text" class="form-control" name="issue[]" placeholder="Issue"" required><input type="text" class="form-control" name="action[]" placeholder="Action" required><div class="input-group-addon"><button type="button" class="delete">-</button></div></div>');
			});
			
			$("#add2").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items2").append($("#template").html());
			});

			$("body").on("click", ".delete", function (e) {
				$(this).parent("div").parent("div").remove();
			});
			
			
			$("input[name='startdate']").change(function (e) {
				$("input[name='enddate']").attr('min', $("input[name='startdate']").val());
			});
			
			$("input[name='enddate']").change(function (e) {
				$("input[name='startdate']").attr('max', $("input[name='enddate']").val());
			});
			
			
			$("input[name='no_atnmale']").change(function (e) {
				$("input[name='no_ipmale']").attr('max', $("input[name='no_atnmale']").val());
				$("input[name='no_oldmale']").attr('max', $("input[name='no_atnmale']").val());
			});
			
			$("input[name='no_atnfemale']").change(function (e) {
				$("input[name='no_ipfemale']").attr('max', $("input[name='no_atnfemale']").val());
				$("input[name='no_oldfemale']").attr('max', $("input[name='no_atnfemale']").val());
			});
			
			
			});
			
			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName('sitio[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate1(e){
				var count = -1;
				var elements = document.getElementsByName('document_name[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate2(e){
				var count = -1;
				var elements = document.getElementsByName('rank[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate3(e){
				var count = -1;
				var elements = document.getElementsByName('problem[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			$("form").submit(function(){

            					var confirmation = confirm('are you sure to this action?');
            					if(confirmation){
            							return true;

            					}else{
            						return false;
            					}


            				});
		</script>
	
@stop