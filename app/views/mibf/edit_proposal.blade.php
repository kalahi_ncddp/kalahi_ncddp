@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('mibf') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	
	    <div class="row">
		    <div class="col-md-12">
				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif

				 {{ Form::model($proposal, array('action' => array('MIBFController@update', $proposal->project_id), 'method' => 'PUT')) }}
@if($purpose != "Municipal Forum")
						<div class="form-group">
							{{ Form::label('rank', 'Rank') }}
							{{ Form::text('rank', $proposal->rank, array('class' => 'form-control', 'required','min'=>'1','max'=>'100')) }}
						</div> 
						@endif
						<div class="form-group">
							{{ Form::label('project_name', 'Project Name') }}
							{{ Form::text('project_name', $proposal->project_name, array('class' => 'form-control', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('priority', 'Priority') }}
							{{ Form::select('priority', array(0 => 'No', 1 => 'Yes'), $proposal->priority, array('class' => 'form-control', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('lead_brgy', 'Lead Barangay') }}
							{{ Form::select('lead_brgy', $brgy_list, $proposal->lead_brgy, array('class' => 'form-control', 'required')) }}
						</div>
						<div id="items">
							<br>
							<button id="add" type="button" >Add</button>
							{{ Form::label('barangay', 'Barangays Covered') }}
							@foreach($coverage as $brgy)
							<div class="input-group">
				            	{{ Form::select('barangay[]', $brgy_list1, $brgy->name, array('class' => 'form-control barangay', 'oninput'=>'checkduplicate(this)', 'required')) }}
								<div class="input-group-addon">
									<button type="button" class="delete">
									    <i class="fa fa-trash-o"></i>
									</button>
								</div>
							</div>
							@endforeach
							<div class="input-group">
								{{ Form::select('barangay[]', $brgy_list1, 'default', array('class' => 'form-control barangay', 'oninput'=>'checkduplicate(this)', 'required')) }}
								<div class="input-group-addon">
									<button type="button" class="delete">
									    <i class="fa fa-trash-o"></i>
									</button>
								</div>
							</div>
						</div>
						<br>
						<div class="form-group">
							{{ Form::label('Grant', 'Grant') }}
						    {{ Form::currency('kc_amount',round($proposal->kc_amount,2),array('class' => 'form-control  kc_amount','required','step'=>'any')) }}
						</div>
						<div class="form-group">
                            {{ Form::label('LCC Amount', 'LCC Amount') }}
                            {{ Form::currency('lcc_amount',round($proposal->lcc_amount,2),array('class' => 'form-control  lcc','required','step'=>'any')) }}

                            </div>
                         <div class="form-group">
							<label for="">Total</label>
							<span class="input-sm form-control total">{{round(floatval($proposal->kc_amount)+floatval($proposal->lcc_amount),2) }}</span>
						</div>
				{{ Form::hidden('id', $proposal->project_id)}}
				{{ Form::hidden('action', 'proposal') }}
				{{ Form::hidden('purpose', $purpose) }}
				{{ Form::hidden('old_rank', $proposal->rank) }}
				{{ Form::hidden('activity_id', $proposal->activity_id)}}

				@if($purpose == "Prioritization")
		    		{{ Form::submit('Update Prioritization', array('class' => 'btn btn-primary')) }}
		    	@elseif($purpose == "Special") 
		    		{{ Form::submit('Update Special MIBF', array('class' => 'btn btn-primary')) }}
		    	@elseif($purpose == "Municipal Forum")
		    			    		{{ Form::submit('Update Municipal Forum', array('class' => 'btn btn-primary')) }}

		    	@endif
                 <a class="btn bg-navy" href="{{ URL::to('mibf') }}">Close</a>
				{{ Form::close() }}
			</div>
		</div>

		<script>
			$(document).ready(function(){
			//when the Add Filed button is clicked

//                $(".barangay option[value='"+ $('#lead_brgy').val() + "']").remove();


				$("#add").click(function (e) {
					//Append a new row of code to the "#items" div
					var value =  $('#lead_brgy').val()
					$("#items").append('<div class="input-group">{{ Form::select('barangay[]', $brgy_list1, 'default', array('class' => 'form-control barangay', 'oninput'=>'checkduplicate(this)', 'required')) }}<div class="input-group-addon"><button type="button" class="delete">-</button></div></div>');
				    $(".barangay option[value="+value+"]").hide();
				     $('.brgy').change(function(){
                        var elem = $(this)[0];
                        var value =  $(this).val();
                        if($('#lead_brgy').val() == $(this).val()){
                            elem.setCustomValidity('Please choose a barangay that is not part of the leadgroup');
                        }
                    });
				});
 $('.brgy').change(function(){
                        var elem = $(this)[0];
                        var value =  $(this).val();
                        if($('#lead_brgy').val() == $(this).val()){
                            elem.setCustomValidity('Please choose a barangay that is not part of the leadgroup');
                        }
                    });
				$("body").on("click", ".delete", function (e) {
					$(this).parent("div").parent("div").remove();
				});
				var $total =0;
					$('.kc_amount').keyup(function(){
				    var kc_amount = parseFloat(number_format($('.kc_amount').val(),2));
				    var lcc       = parseFloat(number_format($('.lcc_amount').val(),2));

					var total = parseFloat(lcc + kc_amount);

					$('.total').text(number_format(total.toString(),2));
					 // $('.total').priceFormat({ prefix: '₱ '});
				});
				$('.lcc_amount').keyup(function(){

					var kc_amount = parseFloat(number_format($('.kc_amount').val(),2));
				    var lcc       = parseFloat(number_format($('.lcc_amount').val(),2));
          			
          			 var total = parseFloat(lcc + kc_amount);
                    
                     $('.total').text(number_format(total.toString(),2));
                     // $('.total').priceFormat({ prefix: '₱ '});

				});
			});

			function checkduplicate(e){
				var count = -1;
				var counts = -1;
				var elements = document.getElementsByName('barangay[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}

				}
				if(e.value ==  $('#lead_brgy').val()){
                    counts++;
                }
    				console.log(counts);
                if(counts == 0){
                console.log('s');
                	e.setCustomValidity("You can't choose the same leadgroup");
                }
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
		</script>
@stop