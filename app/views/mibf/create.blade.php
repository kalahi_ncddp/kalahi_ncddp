@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')

	    <div class="row">
		    <div class="col-md-12">
				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
				{{ Form::open(array('url' => 'mibf', 'method' => 'POST', 'role' => 'form')) }}

				<div class="form-group">
					{{ Form::label('municipality', 'Municipality') }}
					{{ Form::text('municipality', $municipality, array('class' => 'form-control', 'readonly')) }}
				</div> 
				
				<div class="form-group">
					{{ Form::label('psgc_id', 'PSGC Code') }}
					{{ Form::text('psgc_id', $psgc_id, array('class' => 'form-control', 'required', 'readonly'	)) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('kc_code', 'KC Code') }}
					{{ Form::text('kc_code', Input::old('kc_code'), array('class' => 'form-control')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('province', 'Province') }}
					{{ Form::text('province', $province, array('class' => 'form-control', 'required', 'readonly')) }}
				</div>

				<div class="form-group">
					{{ Form::label('cycle_id', 'KC NCDDP Cycle') }}
					{{ Form::select('cycle_id', $cycle_list, $cycle, array('class' => 'form-control', 'required')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('program_id', 'Program') }}
					{{ Form::select('program_id', $program_list, $program, array('class' => 'form-control', 'required')) }}
				</div>

				<div class="form-group">
					{{ Form::label('purpose', 'Purpose') }}
					{{ Form::select('purpose', $purpose_list, Input::old('purpose'), array('class' => 'form-control', 'required')) }}
				</div>
				

				{{ Form::submit('Add Municipal Forum', array('class' => 'btn btn-primary')) }}
				
				{{ Form::close() }}
			</div>
		</div>

		<script>
			$(document).ready(function(){
				$("#municipality").change(function (e) {
					$('.hidden').hide()
					$("#psgc_id").attr('value', $("#municipality").val());
					var selector = "option[value='" +$("#municipality").val()+ "']";
					$(selector).attr('selected', 'selected');
					
					$("#cycle_id").attr('value', $("#cycle_id_temp option:selected").text());
					
					var selector2 = "#cycle_id option[value='" +$("#cycle_id_temp option:selected").text()+ "']";
					$(selector2).attr('selected', 'selected');
					
					$("#program_id").attr('value', $("#program_id_temp option:selected").text());
				});
			});
		</script>
	
@stop