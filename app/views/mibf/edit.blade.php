@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('mibf') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<a class="btn  btn-default" href="{{ URL::to('mibf/'. $mibf->activity_id) }}">Go Back</a>
		 		
	    {{ Form::model($mibf, array('action' => array('MIBFController@update', $mibf->activity_id), 'method' => 'PUT')) }}
                <div class="panel panel-default">
		    		<div class="panel-heading">
		    			Details
		    		</div>
					<div class="panel-body">

						<div class="table-responsive">
							<div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">
							 	 <tr>
					                <th>Municipality</th>
					                <td>{{ $municipality = Report::get_municipality($mibf->psgc_id) }}</td>
					            </tr>
					            <tr>
					                <th>KC Code</th>
					                <td></td>
					            </tr>
					            <tr>
					                <th>Province</th>
					                <td>{{ $province = Report::get_province($mibf->psgc_id) }}</td>
					            </tr>
					            <tr>
					                <th>Region</th>
					                <td>{{ $region = Report::get_region($mibf->psgc_id) }}</td>
					            </tr>
					            </table>
							</div>
							<div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">


					            <tr>
					                <th>Start Date </th>
					                <td>
					                	{{ Form::input('text', 'startdate', Report::get_date_conducted($mibf->activity_id), array('class' => 'form-control ','max' => date("m/d/Y"), 'required','readonly')) }}
									</td>
					            </tr>
					            <tr>
					                <th>End Date</th>
					                <td>
					                	{{ Form::input('text', 'enddate', Report::get_end_date($mibf->activity_id), array('class' => 'form-control ','max' => date("m/d/Y"), 'required','readonly')) }}
									</td>
								
					            </tr>
					             <tr>
					                <th>Cycle</th>
					                <td>{{ $mibf->cycle_id }}</td>
					            </tr>
					              <tr>
					                <th>Program</th>
					                <td>{{ $mibf->program_id }}</td>
					            </tr>
					            <tr>
					                <th>Purpose</th>
					                <td>{{ $mibf->purpose }}</td>
					            </tr>
							</table>
						</div>
						</div>
					</div>
				</div>
				@if($mibf->purpose == "Criteria Setting Workshop")
				 	<div class="box box-primary">
				 		<div class="box-header">
				 			 <h3 class="box-title">Criteria List</h3>
				 			 <div class="box-tools pull-right">
				                <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
				            </div>
				 		</div>
					 	<div class="box-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
						            <div id="items1">
							            @foreach($criteria as $criterion)
							            	<div class="input-group">
							            		<input type="text" class="form-control" placeholder="Criterion" value="{{$criterion->criteria}}" name="criteria[]" oninput="checkduplicate(this)" required>
							            		<input type="text" class="form-control" placeholder="Rate" value="{{$criterion->rate}}" name="rate[]" oninput="checkduplicate(this)" required>
							            		 <span class="input-group-addon delete">
								                	 <i class="fa fa-trash-o  "> </i>
								                </span>
							            	</div><br>
							            @endforeach
						            </div>
						            <div class="input-group">
						            		<input type="text" class="form-control" placeholder="Criterion" name="criteria[]" oninput="checkduplicate(this)">
						            		<input type="text" class="form-control" placeholder="Rate" name="rate[]" oninput="checkduplicate(this)">
						            		 <span class="input-group-addon delete-2">
							                	 <span class="fa fa-trash-o  ">
							                      </span>
							                </span>
						            </div><br>
						            <br>
						            <button id="add1" class="btn btn-info form-control" type="button" >
						            	<i class="fa fa-plus"></i>
						            	Add Criterion</button>

								</table>
							</div>
					 	</div>
					</div>
				@endif
				<div class="panel panel-default">
					<div class="panel-heading">
		    			Barangay Representative Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <button id="add" type="button" >Add</button>
					            <table class="table table-bordered" id="items">
                                    <thead>
                                        <th>Barangay</th>
                                        <th>Barangay Representative 1</th>
                                        <th>Barangay Representative 2</th>
                                        <th>Barangay Representative 3</th>
                                        <th>option</th>
                                    </thead>
                                    <tbody>

                                        @foreach($brgy_rep_data as $rep)

                                          <tr>
                                            <td>
                                              {{ Form::select('barangay_psgc[]', $barangay_list, $rep->barangay_psgc, array('class' => 'form-control', 'oninput'=>'checkduplicate(this)', 'required')) }}
                                            </td>
                                            <td>
                                                {{ Form::select('brgy_rep1[]', $brgy_rep_list, $rep->brgy_rep1,  array('class' => 'form-control', 'required')) }}
                                            </td>
                                            <td>
                                                {{ Form::select('brgy_rep2[]', $brgy_rep_list, $rep->brgy_rep2,  array('class' => 'form-control', 'required')) }}
                                            </td>
                                            <td>
                                                {{ Form::select('brgy_rep3[]', $brgy_rep_list, $rep->brgy_rep3,  array('class' => 'form-control', 'required')) }}
                                            </td>
                                            <td>
                                             <button type="button" class="delete"> <i class="fa fa-trash-o  "> </i></button>
                                            </td>
                                          </tr>
                                        @endforeach
                                          <tr>
                                              <td>
                                              {{ Form::select('barangay_psgc[]', $barangay_list, 0, array('class' => 'form-control', 'oninput'=>'checkduplicate(this)','required')) }}
                                              </td>
                                              <td>
                                              {{ Form::select('brgy_rep1[]', $brgy_rep_list, NULL, array('class' => 'form-control', 'required')) }}
                                              </td>
                                              <td>
                                              {{ Form::select('brgy_rep2[]', $brgy_rep_list, NULL, array('class' => 'form-control', 'required')) }}
                                              </td>
                                              <td>
                                              {{ Form::select('brgy_rep3[]', $brgy_rep_list, NULL, array('class' => 'form-control', 'required')) }}
                                              </td>
                                              <td>
                                              <button type="button" class="delete"><i class="fa fa-trash-o  "> </i></button>
                                              </td>
                                          </tr>
                                    </tbody>
								</table>

							</table>																		
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Total Number of Barangays </th>
					                <td></td>
					            </tr>
					            <tr>
					                <th>Number of Barangay Represented</th>
					                <td></td>
					        	</tr>
					                <th>Precentage of Barangay Represented</th>
					                <td></td>
					            </tr>
							</table>
						</div>
					</div>
				</div>
															
		<!-- 		<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Attendees
		    		</div>
		    		
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Attendees </th>
					                <th>Male</th>
					                <th>Female</th>
					            </tr>
					            <tr>
					            	<th>No. of Person </th>
					                <td>
					                	{{ Form::input('number', 'no_atnmale', $mibf->no_atnmale, array('class' => 'form-control', 'min'=>'0', 'required')) }}
					               	</td>
					                <td>
					                	{{ Form::input('number', 'no_atnfemale', $mibf->no_atnfemale, array('class' => 'form-control', 'min'=>'0', 'required')) }}
					                </td>
					            </tr>
					            <tr>
					                <th>No. of IP</th>
					                <td>
					                	{{ Form::input('number', 'no_ipmale', $mibf->no_ipmale, array('class' => 'form-control', 'min'=>'0', 'max'=>$mibf->no_atnmale, 'required')) }}
							        </td>
					                <td>
					                	{{ Form::input('number', 'no_ipfemale', $mibf->no_ipfemale, array('class' => 'form-control', 'min'=>'0', 'max'=>$mibf->no_atnfemale, 'required')) }}
					                </td>
					            </tr>
					                <th>No. of 60 yrs +</th>
					                <td>
					                	{{ Form::input('number', 'no_oldmale', $mibf->no_oldmale, array('class' => 'form-control', 'min'=>'0', 'max'=>$mibf->no_atnmale, 'required')) }}
							       	</td>
					                <td>
					                	{{ Form::input('number', 'no_oldfemale', $mibf->no_oldmale, array('class' => 'form-control', 'min'=>'0', 'max'=>$mibf->no_atnfemale, 'required')) }}
							        </td>
					            </tr>
					            </tr>
					                <th>No. of LGU Officials</th>
					                <td>
					                	{{ Form::input('number', 'no_lgumale', $mibf->no_lgumale, array('class' => 'form-control', 'min'=>'0', 'max'=>$mibf->no_atnmale, 'required')) }}
							       	</td>
					                <td>
					                	{{ Form::input('number', 'no_lgufemale', $mibf->no_lgufemale, array('class' => 'form-control', 'min'=>'0', 'max'=>$mibf->no_atnfemale, 'required')) }}
							        </td>
					            </tr>
					            
							</table>
						</div>
					</div>
				</div>
 -->
				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Other Sectors Represented
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								@foreach($sector_list as $sector)
									@if(in_array($sector, $sector_data))
										<div class = "form-control"><input type="checkbox" name="sector[]" value="{{$sector}}" checked>&nbsp{{$sector}}</div>
									@else
										<div class = "form-control"><input type="checkbox" name="sector[]" value="{{$sector}}">&nbsp{{$sector}}</div>
									@endif
								@endforeach
							</table>
						</div>
					</div>
				</div>
				<div class="form-group pull-right">
                    <label>Save as draft</label>
                    <input type="checkbox" name="is_draft" checked>
                </div>
				{{ Form::hidden('action', 'mibf') }}
				{{ Form::hidden('purpose', $mibf->purpose) }}
				<!-- {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
<a class="btn bg-navy" href="{{ URL::to('mibf') }}">Close</a> -->
 <div class="col-md-12 ">
									<div class="form-group pull-right">
						                <button class="btn btn-primary" id="save">
											<i class="fa fa-save"></i>
											Submit 
										</button>
		                 				<a class="btn btn-default" href="{{ URL::to('mibf') }}">Cancel</a>		
									</div>
								</div>
								<div class="clearfix"></div>				
				{{ Form::close() }}
		
		<script>
			$(document).ready(function(){
			//when the Add Filed button is clicked
			$("#add").click(function (e) {
				//Append a new row of code to the "#items" div
				//$("#items").append($("#template").html());
				$("#items").find('tbody').append('<tr><td>{{ Form::select('barangay_psgc[]', $barangay_list, 0, array('class' => 'form-control', 'oninput'=>'checkduplicate(this)','required')) }}</td><td>{{ Form::select('brgy_rep1[]', $brgy_rep_list, NULL, array('class' => 'form-control', 'required')) }}</td><td>{{ Form::select('brgy_rep2[]', $brgy_rep_list, NULL, array('class' => 'form-control', 'required')) }}</td><td>{{ Form::select('brgy_rep3[]', $brgy_rep_list, NULL, array('class' => 'form-control', 'required')) }}</td><td><button type="button" class="delete"> <i class="fa fa-trash-o  "> </i></button></td></tr>');
			    $("select").change(function(){
                				var parent = $(this).parent().parent();
                				var current = $(this).val();
                //				console.log( parent );
                				console.log(current);
                				var elem = $(this);
                                var count = 0;
                				if(current == "Barangay Captain" || current == "SK Chair" || current == "Secretary" || current == "Treasurer"){
                					$.each(elem.parent().siblings().find('select'),function(i,e){
                                          if(e.value==current){
                //                          console.log(elem);
                                          console.log('s');
                                            count++;

                //                            elem[0].setCustomValidity('Please choose another position')
                                          }
                                          else{
                                            elem[0].setCustomValidity("");
                                          }
                                    });
                				}
                				  if(count>0){
                				    elem.css({'border-color':'red'});
                				    elem.parent().prepend('<b style="color:red">Duplicate!,Choose other representative position</b>');
                                    elem[0].setCustomValidity('Please choose another position')
                                  }
                                  else{
                                   elem.parent().find('b').remove();
                                   elem.css({'border-color':'#ccc'});
                                    elem[0].setCustomValidity("");
                                  }
                			});
			});

			$("#add1").click(function (e) {
				$("#items1").append('<div class="input-group"><input type="text" class="form-control" placeholder="Criterion" name="criteria[]" oninput="checkduplicate(this)"><input type="text" class="form-control" placeholder="Rate" name="rate[]" oninput="checkduplicate(this)"> <span class="input-group-addon delete"> <span class="fa fa-trash-o  "></span> </span></div><br>');
			});

			$("body").on("click", ".delete", function (e) {
				$(this).parent().parent().remove();
			});
			$("body").on("click", ".delete-2", function (e) {
				$(this).parent().remove();
			});
			
			$("input[name='startdate']").change(function (e) {
				$("input[name='enddate']").attr('min', $("input[name='startdate']").val());
			});
			
			$("input[name='enddate']").change(function (e) {
				$("input[name='startdate']").attr('max', $("input[name='enddate']").val());
			});
			
			
			$("input[name='no_atnmale']").change(function (e) {
				$("input[name='no_ipmale']").attr('max', $("input[name='no_atnmale']").val());
				$("input[name='no_oldmale']").attr('max', $("input[name='no_atnmale']").val());
				$("input[name='no_lgumale']").attr('max', $("input[name='no_atnmale']").val());
			});
			
			$("input[name='no_atnfemale']").change(function (e) {
				$("input[name='no_ipfemale']").attr('max', $("input[name='no_atnfemale']").val());
				$("input[name='no_oldfemale']").attr('max', $("input[name='no_atnfemale']").val());
				$("input[name='no_lgufemale']").attr('max', $("input[name='no_atnfemale']").val());
			});
			});
			


			$("select").change(function(){
				var parent = $(this).parent().parent();
				var current = $(this).val();
//				console.log( parent );
				console.log(current);
				var elem = $(this);
                var count = 0;
				if(current == "Barangay Captain" || current == "SK Chair" || current == "Secretary" || current == "Treasurer"){
					$.each(elem.parent().siblings().find('select'),function(i,e){
                          if(e.value==current){
//                          console.log(elem);
                          console.log('s');
                            count++;

//                            elem[0].setCustomValidity('Please choose another position')
                          }
                          else{
                            elem[0].setCustomValidity("");
                          }
                    });
				}
				  if(count>0){
				    elem.css({'border-color':'red'});
				    elem.parent().prepend('<b style="color:red">Duplicate!,Choose other representative position</b>');
                    elem[0].setCustomValidity('Please choose another position')
                  }
                  else{
                   elem.parent().find('b').remove();
                   elem.css({'border-color':'#ccc'});
                    elem[0].setCustomValidity("");
                  }
			});

			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName('barangay_psgc[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}

				if(count == 1)
				{
				    console.log('e');
				    e.style.borderColor = 'red';

					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
		</script>
@stop