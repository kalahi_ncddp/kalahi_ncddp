@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')

	    <div class="row">
		    <div class="col-md-12">
		     <a class="btn btn-default" href="{{ URL::previous() }}">Go back</a>

				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif

<br>
<br>
				{{ Form::open(array('url' => 'mibf/submit_proposal', 'method' => 'POST', 'role' => 'form')) }}
					<div class="form-group">
						{{ Form::label('rank', $title == "Municipal Forum" ? 'Number' : 'Rank' ) }}

						{{ Form::number('rank', Input::old('rank'), array('class' => 'form-control', 'required','min'=>'1','max'=>'100')) }}
					</div>

					<div class="form-group">
						{{ Form::label('project_name', 'Project Name') }}
						{{ Form::text('project_name', Input::old('project_name'), array('class' => 'form-control', 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('priority', 'Priority') }}
						{{ Form::select('priority', array(''=>'Select Priority',0 => 'No', 1 => 'Yes'), Input::old('priority'), array('class' => 'form-control', 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('lead_brgy', 'Lead Barangay') }}
						{{ Form::select('lead_brgy', [''=>'Select Lead Barangay']+$brgy_list, Input::old('lead_brgy'), array('class' => 'form-control', 'required')) }}
					</div>
					<div id="items">
						{{ Form::label('barangay', 'Barangays Covered') }}
						<br>
						<button id="add" type="button" >Add</button>
						<div class="input-group">
							{{ Form::select('barangay[]', $brgy_list1, 'default', array('class' => 'form-control brgy', 'oninput'=>'checkduplicate(this)', 'required')) }}
							<div class="input-group-addon">
								<button type="button" class="delete"><i class="fa fa-trash-o"></i></button>
							</div>
						</div>
					</div>
					<br>
					<div class="form-group">
						{{ Form::label('Grant', 'Grant') }}
						{{ Form::currency('kc_amount','', array('class' => 'form-control kc_amount price','required','step'=>'any')) }}

					</div>
					<div class="form-group">
                    	{{ Form::label('LCC Amount', 'LCC Amount') }}
                    	{{ Form::currency('lcc_amount','', array('class' => 'form-control kc_amount price','required','step'=>'any')) }}
                    </div>
					<div class="form-group">
						<label for="">Total</label>
						<span class="input-sm form-control total"></span>
					</div>
				{{ Form::hidden('id', $id)}}
				{{ Form::hidden('purpose', $purpose)}}
				@if($purpose == "Prioritization" || $purpose == "Municipal Forum" )
		    		{{ Form::submit('Add Prioritization', array('class' => 'btn btn-primary')) }}
		    	@elseif($purpose == "Special") 
		    		{{ Form::submit('Add Special MIBF', array('class' => 'btn btn-primary')) }}
		    	@endif

				{{ Form::close() }}
			</div>
		</div>

		<script>
			$(document).ready(function(){


			//when the Add Filed button is clicked
				$("#add").click(function (e) {
					//Append a new row of code to the "#items" div
					//$("#items").append($("#template").html());
					var value =  $('#lead_brgy').val()
					$("#items").append('<div class="input-group">{{ Form::select('barangay[]', $brgy_list1, 'default', array('class' => 'form-control brgy', 'oninput'=>'checkduplicate(this)', 'required')) }}<div class="input-group-addon"><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div></div>');

                    $(".brgy option[value="+value+"]").hide();

				    $('.brgy').change(function(){
                        var elem = $(this)[0];
                        var value =  $(this).val();
                        if($('#lead_brgy').val() == $(this).val()){
                            $(".barangay option[value="+value+"]").remove();
                            elem.setCustomValidity('Please choose a barangay that is not part of the leadgroup');
                        }
                    });
				});
				$("body").on("click", ".delete", function (e) {
					$(this).parent("div").parent("div").remove();
				});

				$('.kc_amount').keyup(function(){
				    var kc_amount = parseFloat(number_format($('.kc_amount').val(),2));
				    var lcc       = parseFloat(number_format($('.lcc_amount').val(),2));

					var total = parseFloat(lcc + kc_amount);

					$('.total').text(number_format(total.toString(),2));
					 // $('.total').priceFormat({ prefix: '₱ '});
				});
				$('.lcc_amount').keyup(function(){

					var kc_amount = parseFloat(number_format($('.kc_amount').val(),2));
				    var lcc       = parseFloat(number_format($('.lcc_amount').val(),2));
          			
          			 var total = parseFloat(lcc + kc_amount);
                    
                     $('.total').text(number_format(total.toString(),2));
                     // $('.total').priceFormat({ prefix: '₱ '});

				});
			});

            $('.brgy').change(function(){
                var elem = $(this)[0];
                var value =  $(this).val();
                if($('#lead_brgy').val() == $(this).val()){
                    elem.setCustomValidity('Please choose a barangay that is not part of the leadgroup');
                }
            });
			function checkduplicate(e){
				var count = -1;
								var counts = -1;

				var elements = document.getElementsByName('barangay[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}

				if(count == 1){
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}

		</script>
@stop