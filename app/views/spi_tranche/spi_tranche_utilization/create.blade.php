@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')

	@if ($errors->all())
		<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
	@endif
<div class="panel panel-default">
	<div class="panel-heading">
		Add Monthly Utilization Data
	</div>
	<div class="panel-body">
	{{ Form::open(array('route' => array('spi.utilization.store', $subproject), 'method' => 'post', 'role' => 'form')) }}
	<!-- 'project_id',
		 'as_of_date',
		 'grant_utilization',
		 'lcc_utilization' -->
		 <div class="col-md-6">
			<div class="form-group">
				<label for="As of Date">As of Date</label>
				<input type="text" name="as_of_date" class="form-control date">
			</div>
		 </div>
		 <div class="col-md-6">
			<div class="form-group">
				<label for="Grant Utilization">Grant Utilization</label>
				{{ Form::currency('grant_utilization','',['class'=>'form-control']) }}
			</div>
		 </div>
		  <div class="col-md-6">
			<div class="form-group">
				<label for="LCC Utilization">LCC Utilization</label>
				{{ Form::currency('lcc_utilization','',['class'=>'form-control']) }}
			</div>
		 </div>
		 <div class="clearfix">	</div>
		<div class="col-md-6 pull-left">
			<div class="form-group">
				<button class="btn btn-primary">
					<i class="fa fa-save"></i>
					Save
				</button>
			</div>
		 </div>
		  <div class="clearfix">	</div>
	{{ Form::close() }}
@stop