@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('spi_profile') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    {{ HTML::linkRoute('spi_profile.show', 'Back to Sub Project Profile',
      array($tranche->project_id), array('class' => 'btn btn-default btn')) }}
    {{ HTML::linkRoute('spi_profile.tranche.edit', 'Edit',
      array($tranche->project_id), array('class' => 'btn btn-info btn')) }}
  </div>
</div>

<hr />

<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="40%">Sub Project Name</th>
					<td>
						{{ $profile->project_name }}
					</td>
				</tr>
				<tr>
					<th>Physical Target</th>
					<td>{{ $profile->phys_target }} {{ $profile->phys_target_unit }}</td>
				</tr>
				<tr>
					<th>Region</th>
					<td>{{ Report::get_region($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Province</th>
					<td>{{ Report::get_province($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Municipality</th>
					<td>{{ Report::get_municipality($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Barangay(s) Covered</th>
					<td>{{ $profile->brgy_coverage_list() }}</td>
				</tr>
				<tr>
					<th>Total Cost of Sub Project</th>
					<td class="total_cost">
						@if( !empty($proposal) )
							{{ $proposal->kc_amount + $proposal->lcc_amount }}
						@else
							{{ $tranche->kc_amount + $tranche->lcc_amount }}
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-right">Grant Amount</td>
					<td>
						@if ($profile->mibf_refno !== "" &&  !empty($proposal))
							{{ $proposal->kc_amount }}
						@else
							{{ $tranche->kc_amount }}
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-right">LCC</td>
					<td>
						@if ($profile->mibf_refno !== "" && !empty($proposal))
							{{ $proposal->lcc_amount }}
						@else
							{{ $tranche->lcc_amount }}
						@endif
					</td>
				</tr>
				<tr>
					<th>Date Started</th>
					<td>{{ toDate($profile->date_started) }}</td>
				</tr>
				<tr>
					<th>Target Completion Date</th>
					<td>{{ toDate($profile->date_completed) }}</td>
				</tr>
				<tr>
                    <th>ERFR SP ID </th>
                    <td> {{ $tranche->erfr_sp_id }}</td>
                </tr>
			</table>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">
		Tranche Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th rowspan="2">Tranches</th>
						<th colspan="2">Planned</th>
						<th colspan="3">Release</th>
					</tr>
					<tr>
						<th>Amount</th>
						<th>Percent<br/>(of KC-NCDDP Amount)</th>
						<th>Date Released</th>
						<th>Amount</th>
						<th>Percent Released</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1st Tranche</td>
						<td>{{ $tranche->planned_amt_1 }}</td>
						<td>{{ $tranche->planned_percent_1 }}</td>
						<td>{{ toDate($tranche->release_date_1) }}</td>
						<td>{{ $tranche->release_amt_1 }}</td>
						<td>{{ $tranche->release_percent_1 }}</td>
					</tr>
					<tr>
						<td>2nd Tranche</td>
						<td>{{ $tranche->planned_amt_2 }}</td>
						<td>{{ $tranche->planned_percent_2 }}</td>
						<td>{{ toDate($tranche->release_date_2) }}</td>
						<td>{{ $tranche->release_amt_2 }}</td>
						<td>{{ $tranche->release_percent_2 }}</td>
					</tr>
					<tr>
						<td>3rd Tranche</td>
						<td>{{ $tranche->planned_amt_3 }}</td>
						<td>{{ $tranche->planned_percent_3 }}</td>
						<td>{{ toDate($tranche->release_date_3) }}</td>
						<td>{{ $tranche->release_amt_3 }}</td>
						<td>{{ $tranche->release_percent_3 }}</td>
					</tr>

					<tr>
						<td>TOTAL</td>
						<td>{{ $tranche->planned_amt_1 + $tranche->planned_amt_2 + $tranche->planned_amt_3 }}</td>
						<td>{{ $tranche->planned_percent_1 + $tranche->planned_percent_2 + $tranche->planned_percent_3 }}%</td>
						<td class=""></td>
						<td>{{$tranche->release_amt_1 + $tranche->release_amt_2 + $tranche->release_amt_3 }}</td>
						<td>{{ $tranche->release_percent_1 + $tranche->release_percent_2 + $tranche->release_percent_3 }}%</td>
					
						
					</tr>
					<tr>
                    <td></td>
                    </tr>
                    <tr>
                        <td>Final Status of SP Fund Utilization (Annex 11)</td>
                        <td>
                            {{ Form::select('final_status',[''=>'Status','1'=>'Yes','0'=>'No'],$tranche->final_status,['readonly','disabled']) }}
                        </td>
                    </tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        Sub-Project Utilization
    </div>
    <div class="panel-heading">
                    <h4 class="col-xs-6 col-md-6" style="padding-left: 0;">Monthly Utilization</h4>
                    <a id="spi-accomplish_modal-add_show" href="{{ URL::to('spi_profile/'.$tranche->project_id.'/utilization/create') }}" class="btn btn-success pull-right">
                        <i class="fa fa-plus"></i> Add Monthly Utilization Data
                    </a>
                    <div class="clearfix"></div>
                </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th colspan="1">As of Date</th>
                    <th colspan="1">Grant</th>
                    <th colspan="1">LCC</th>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody>
                
              	@foreach($utilization as $utilizations)
             	  <tr>
                    <td>{{ toDate($utilizations->as_of_date) }}</td>
                    <td>{{ currency_format($utilizations->grant_utilization) }}</td>
                    <td>{{ currency_format($utilizations->lcc_utilization) }}</td>
                    <td>
                    	<a href="{{ URL::route('spi.utilization.edit',[$tranche->project_id,'id='.$utilizations->id]) }}" class="btn btn-primary">
							<i class="fa fa-edit"></i>
                    	</a>
                    	<a href="{{ URL::route('spi.utilization.delete',[$tranche->project_id,'id='.$utilizations->id]) }}" class="btn btn-danger">
							<i class="fa fa-trash-o"></i>
                    	</a>
                    </td>
                   </tr>
                
             	@endforeach
                
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop