<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<div class="col-md-6 ">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="40%">Sub Project Name</th>
					<td>
						{{ $profile->project_name }}
						{{ Form::hidden('project_id', $tranche->project_id) }}
					</td>
				</tr>
				<tr>
					<th>Physical Target</th>
					<td>{{ $profile->phys_target }} {{ $profile->phys_target_unit }}</td>
				</tr>
				<tr>
					<th>Region</th>
					<td>{{ Report::get_region($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Province</th>
					<td>{{ Report::get_province($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Municipality</th>
					<td>{{ Report::get_municipality($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Barangay(s) Covered</th>
					<td>{{ $profile->brgy_coverage_list() }}</td>
				</tr>
			</table>
		</div>
		<div class="col-md-6 ">
			<table class="table table-bordered table-striped">
			
				<tr>
					<th>Total Cost of Sub Project</th>
					<td class="total_cost">
						@if ($profile->mibf_refno !== "" &&  !empty($proposal))
							{{ $proposal->kc_amount + $proposal->lcc_amount }}
						@else
							0
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-right">Grant Amount</td>
					<td>
						@if ($profile->mibf_refno !== "" &&  !empty($proposal))
							{{ currency_format($proposal->kc_amount) }}
							{{ Form::hidden('kc_amount', $proposal->kc_amount) }}
						@else
							{{ Form::number( 'kc_amount', $tranche->kc_amount, ['class' => 'form-control prices', 'min' => 0, 'step' => 'any','placeholder'=>'Amount: ex: 20000 ']) }}
							
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-right">LCC Amount</td>
					<td>
						@if ($profile->mibf_refno !== "" &&  !empty($proposal))
							{{ currency_format($proposal->lcc_amount) }}
							{{ Form::hidden('lcc_amount', $proposal->lcc_amount) }}
						@else
							{{ Form::input('number', 'lcc_amount', $tranche->lcc_amount, ['class' => 'form-control', 'min' => 0, 'step' => 'any','placeholder'=>'Amount: ex: 20000 ']) }}
						@endif
					</td>
				</tr>
				<tr>
					<th>Date Started</th>
					<td>{{ toDate($profile->date_started) }}</td>
				</tr>
				<tr>
					<th>Target Completion Date</th>
					<td>{{ toDate($profile->date_completed) }}</td>
				</tr>

				<tr>
				    <th>ERFR SP ID </th>
				    <td> {{ Form::text('erfr_sp_id', $tranche->erfr_sp_id ,['class'=>'form-control','placeholder'=>'Sub Project ID  ']) }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div class="panel panel-default">
                       	<div class="panel-heading">
                       		Tranche Information
                       	</div>
                       	<div class="panel-body">
                       		<div class="table-responsive">
                       			<table class="table table-bordered table-striped">
                       				<thead>
					<tr>
						<th rowspan="2">Tranches</th>
						<th colspan="2">Planned</th>
						<th colspan="3">Release</th>
					</tr>
					<tr>
						<th>Amount</th>
						<th>Percent<br/>(of Grant Amount)</th>
						<th>Date Released</th>
						<th>Amount</th>
						<th>Percent Released</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1st Tranche</td>
						<td>{{ Form::number('planned_amt_1', $tranche->planned_amt_1, ['readonly','class' => 'form-control planned_amt_1 prices', 'min' => 0, 'step' => 'any']) }}</td>
						<td>{{ Form::input('number', 'planned_percent_1', $tranche->planned_percent_1, ['class' => 'form-control percent planned_percent_1', 'min' => 0, 'step' => 'any']) }} <span class="percent-text">%</span></td>
						<td>{{ Form::input('text', 'release_date_1', isset($tranche->release_date_1) ? date("m/d/Y", strtotime($tranche->release_date_1)) : null, array('class' => 'form-control date','max' => date("m/d/Y"))) }}</td>
						<td>{{ Form::number('release_amt_1', $tranche->release_amt_1, ['class' => 'form-control release_amt_1 prices', 'min' => 0, 'step' => 'any']) }}</td>
						<td>{{ Form::input('number', 'release_percent_1', $tranche->release_percent_1, ['class' => 'form-control percent release_percent_1', 'min' => 0, 'step' => 'any']) }}<span class="percent-text">%</span></td>
					</tr>
					<tr>
						<td>2nd Tranche</td>
						<td>{{ Form::number('planned_amt_2', $tranche->planned_amt_2, ['readonly','class' => 'form-control planned_amt_2 prices', 'min' => 0, 'step' => 'any']) }}</td>
						<td>{{ Form::input('number', 'planned_percent_2', $tranche->planned_percent_2, ['class' => 'form-control percent planned_percent_2', 'min' => 0, 'step' => 'any']) }}<span class="percent-text">%</span></td>
						<td>{{ Form::input('text', 'release_date_2', isset($tranche->release_date_2) ? date("m/d/Y", strtotime($tranche->release_date_2)) : null, array('class' => 'form-control date','max' => date("m/d/Y"))) }}</td>
						<td>{{ Form::number('release_amt_2', $tranche->release_amt_2, ['class' => 'form-control release_amt_2 prices', 'min' => 0, 'step' => 'any']) }}</td>
						<td>{{ Form::input('number', 'release_percent_2', $tranche->release_percent_2, ['class' => 'form-control percent release_percent_2', 'min' => 0, 'step' => 'any']) }}<span class="percent-text">%</span></td>
					</tr>
					<tr>
						<td>3rd Tranche</td>
						<td>{{ Form::number('planned_amt_3', $tranche->planned_amt_3, ['readonly','class' => 'form-control planned_amt_3 prices', 'min' => 0, 'step' => 'any']) }}</td>
						<td>{{ Form::input('number', 'planned_percent_3', $tranche->planned_percent_3, ['class' => 'form-control percent planned_percent_3', 'min' => 0, 'step' => 'any']) }}<span class="percent-text">%</span></td>
						<td>{{ Form::input('text', 'release_date_3', isset($tranche->release_date_3) ? date("m/d/Y", strtotime($tranche->release_date_3)) : null, array('class' => 'form-control date','max' => date("m/d/Y"))) }}</td>
						<td>{{ Form::number('release_amt_3', $tranche->release_amt_3, ['class' => 'form-control release_amt_3 prices', 'min' => 0, 'step' => 'any']) }}</td>
						<td>{{ Form::input('number', 'release_percent_3', $tranche->release_percent_3, ['class' => 'form-control percent release_percent_3', 'min' => 0, 'step' => 'any']) }}<span class="percent-text">%</span></td>
					</tr>

					<tr>
						<td>TOTAL</td>
						<td>{{ Form::input('number', 'planned_amount', '0', ['class' => 'form-control planned_amount', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
						<td>{{ Form::input('number', 'planned_percent', '0', ['readonly','class' => 'form-control percent planned_percent', 'step' => 'any', 'readonly'=>'readonly']) }}<span class="percent-text">%</span></td>
						<td class=""></td>
						<td>{{ Form::input('number', 'release_amount', '0', ['class' => 'form-control release_amount', 'step' => 'any', 'readonly'=>'readonly']) }}</td>
						<td>{{ Form::input('number', 'release_percent', '0', ['readonly','class' => 'form-control percent release_percent', 'step' => 'any', 'readonly'=>'readonly']) }}<span class="percent-text">%</span></td>
					
						
					</tr>
					<tr>
					<td></td>
					</tr>
                    <tr>
                        <td>With Approved Final Status of SP Fund Utilization (Annex 11) ?</td>
                        <td>
                            {{ Form::select('final_status',[''=>'Yes/No','1'=>'Yes','0'=>'No'],$tranche->final_status) }}
                        </td>
                    </tr>
					
				</tbody>
			</table>
		</div>
	</div>
</div>
{{ Form::submit('Save SP Tranche', array('class' => 'btn btn-primary')) }}
{{ HTML::linkRoute('spi_profile.show', 'Close',
  array($tranche->project_id), array('class' => 'btn bg-navy btn')) }}
{{ Form::close() }}

<script>
	$(document).ready(function() {
		{{--@if ($profile->mibf_refno === "")--}}
			$("input[name='kc_amount']").change(calculate_total_cost);
			$("input[name='lcc_amount']").change(calculate_total_cost);
			calculate_total_cost();
		{{--@endif--}}
	});

	function calculate_total_cost() {
	    var kc_amount = $("input[name='kc_amount']").val();
         var lcc       = $("input[name='lcc_amount']").val();
		var grant = parseFloat(kc_amount.replace(/[^0-9+.]/g, '') || 0);
		var lcc = parseFloat(lcc.replace(/[^0-9+.]/g, '') || 0);
		/* set max value in tranch input for grant*/
        $("input[name='planned_amt_1']").attr('max',grant);
        $("input[name='planned_amt_2']").attr('max',grant);
        $("input[name='planned_amt_3']").attr('max',grant);

        $("input[name='release_amt_1']").attr('max',grant);
        $("input[name='release_amt_2']").attr('max',grant);
        $("input[name='release_amt_3']").attr('max',grant);

		$("td.total_cost").html(round_off(grant + lcc, 2));
	}

	function round_off(num, places) {
		var dec = Math.pow(10, places); 
  	return Math.round(num * dec)/dec;
	}


	/* get the total in tranche */
			var total = (function($){	
				return {
					
					totalPlannedAmount : function(){
						var total_ = parseFloat($("input[name='planned_amt_1']").val()) + parseFloat($("input[name='planned_amt_2']").val())  + parseFloat($("input[name='planned_amt_3']").val()) ;
						$('.planned_amount').val(total_ || '0');

						if( $('input[name=kc_amount]').val() < total_ )
						{
						    $("input[name='planned_amt_1']")[0].setCustomValidity('Total Planned must be equal to the grant amount');

						}else{
						    $("input[name='planned_amt_1']")[0].setCustomValidity('');

						}
						if( $("input[name='planned_amt_3']").val() != "" ){
						    if( $("input[name='planned_amt_2']").val() != "" ){
						        $("input[name='planned_amt_2']")[0].setCustomValidity('');
						    }else{
						        $("input[name='planned_amt_2']")[0].setCustomValidity('This is required if 3rd Tranche is available');
						    }
						}else{
												        $("input[name='planned_amt_2']")[0].setCustomValidity('');

						}


						/*compute the (planned / grant)* 100 */
						var planned_percent = ($("input[name='planned_amt_1']").val() / $('input[name=kc_amount]').val()) * 100;
						$('.planned_percent_1').val( round_off(planned_percent,3)  ) ;

						var planned_percent = ($("input[name='planned_amt_2']").val() / $('input[name=kc_amount]').val()) * 100;
                        $('.planned_percent_2').val(round_off(planned_percent,3) ) ;

                        var planned_percent = ($("input[name='planned_amt_3']").val() / $('input[name=kc_amount]').val()) * 100;
                        $('.planned_percent_3').val(round_off(planned_percent,3) ) ;
					},

					totalPlannedPercent : function(){
						var total_ = parseInt($("input[name='planned_percent_1']").val()) + parseInt($("input[name='planned_percent_2']").val())  + parseInt($("input[name='planned_percent_3']").val()) ;
						$('.planned_percent').val(total_ || '0');

						var planned_percent = (( parseFloat($("input[name='planned_percent_1']").val()) / 100) * parseFloat($('input[name=kc_amount]').val()));
                        $('.planned_amt_1').val( round_off(planned_percent,3)  ) ;

                       var planned_percent = (( parseFloat($("input[name='planned_percent_2']").val()) / 100) * parseFloat($('input[name=kc_amount]').val()));
                       $('.planned_amt_2').val( round_off(planned_percent,3)  );

                      var planned_percent = (( parseFloat($("input[name='planned_percent_3']").val()) / 100) * parseFloat($('input[name=kc_amount]').val()));
                      $('.planned_amt_3').val( round_off(planned_percent,3)  );

                      var total_ = parseFloat($("input[name='planned_amt_1']").val()) + parseFloat($("input[name='planned_amt_2']").val())  + parseFloat($("input[name='planned_amt_3']").val()) ;
                    $('.planned_amount').val(total_ || '0');
					},

					totalReleaseAmount : function(){
						var total_ = parseFloat($("input[name='release_amt_1']").val()) + parseFloat($("input[name='release_amt_2']").val())  + parseFloat($("input[name='release_amt_3']").val()) ;
						$('.release_amount').val(total_ || '0');


                    if( $('input[name=kc_amount]').val() < total_ )
						{
						    $("input[name='release_amt_1']")[0].setCustomValidity('Total Planned must be equal to the grant amount');

						}else{
						    $("input[name='release_amt_1']")[0].setCustomValidity('');

						}

						 var release_percent = ($("input[name='release_amt_1']").val() / $('input[name=kc_amount]').val()) * 100;
                         $('.release_percent_1').val(  round_off(release_percent,3) ) ;

                          var release_percent = ($("input[name='release_amt_2']").val() / $('input[name=kc_amount]').val()) * 100;
                        $('.release_percent_2').val(round_off(release_percent,3) ) ;

                       var release_percent = ($("input[name='release_amt_3']").val() / $('input[name=kc_amount]').val()) * 100;
                       $('.release_percent_3').val(round_off(release_percent,3)) ;
					},

					totalReleasePercent : function(){
						var total_ = parseFloat($("input[name='release_percent_1']").val()) + parseFloat($("input[name='release_percent_2']").val())  + parseFloat($("input[name='release_percent_3']").val()) ;
						$('.release_percent').val(total_ || '0');

var total_ = parseFloat($("input[name='release_amt_1']").val()) + parseFloat($("input[name='release_amt_2']").val())  + parseFloat($("input[name='release_amt_3']").val()) ;
						$('.release_amount').val(total_ || '0');
						 var release_percent = ($("input[name='release_percent_1']").val() * $('input[name=kc_amount]').val()) / 100;
                         $('.release_amt_1').val(  round_off(release_percent,3) ) ;

                         var release_percent = ($("input[name='release_percent_2']").val() * $('input[name=kc_amount]').val()) / 100;
                         $('.release_amt_2').val(round_off(release_percent,3) ) ;

                           var release_percent = ($("input[name='release_percent_3']").val() * $('input[name=kc_amount]').val()) / 100;
                           $('.release_amt_3').val(round_off(release_percent,3)) ;
					}
					
				}
			})(jQuery);
                total.totalPlannedAmount();
                total.totalPlannedPercent();
                total.totalReleaseAmount();
                total.totalReleasePercent();
                // total.totalTraining();
                // total.totalWomen();
                // total.totalManagement();
                // total.totalOthers();

			$(".planned_amt_1").change(total.totalPlannedAmount);
			$(".planned_amt_2").change(total.totalPlannedAmount);
			$(".planned_amt_3").change(total.totalPlannedAmount);

			$(".planned_percent_1").keyup(total.totalPlannedPercent);
			$(".planned_percent_2").keyup(total.totalPlannedPercent);
			$(".planned_percent_3").keyup(total.totalPlannedPercent);

			$(".release_amt_1").keyup(total.totalReleaseAmount);
			$(".release_amt_2").keyup(total.totalReleaseAmount);
			$(".release_amt_3").keyup(total.totalReleaseAmount);

			$(".release_percent_1").keyup(total.totalReleasePercent);
			$(".release_percent_2").keyup(total.totalReleasePercent);
			$(".release_percent_3").keyup(total.totalReleasePercent);

			// $("input[name='grant_infra']").keyup(total.totalFEA);
			// $("input[name='lcc_infra']").keyup(total.totalFEA);
			
			// $("input[name='grant_training']").keyup(total.totalTraining);
			// $("input[name='lcc_training']").keyup(total.totalTraining);
			
			// $("input[name='grant_women']").keyup(total.totalWomen);
			// $("input[name='lcc_women']").keyup(total.totalWomen);

			// $("input[name='grant_mgmnt']").keyup(total.totalManagement);
			// $("input[name='lcc_mgmnt']").keyup(total.totalManagement);
			
			// $("input[name='grant_others']").keyup(total.totalOthers);
			// $("input[name='lcc_others']").keyup(total.totalOthers);

</script>