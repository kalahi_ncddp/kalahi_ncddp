<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
     <title>{{ isset($title)? $title : 'KC-NCDDP Database System' }}</title>

      <!-- BOOTSTRAP STYLES-->
    <style>
      body{
        font-family: 'Source Sans Pro', sans-serif;
      }
      table {
        max-width: 100%;
        background-color: transparent;
      }
      th {
        text-align: left;
      }
      .table {
        width: 100%;
        margin-bottom: 20px;
      }
      .table > thead > tr > th,
      .table > tbody > tr > th,
      .table > tfoot > tr > th,
      .table > thead > tr > td,
      .table > tbody > tr > td,
      .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
      }
      .table > thead > tr > th {
        vertical-align: bottom;
        border-bottom: 2px solid #ddd;
        font-size: 12px;
      }
      .table > caption + thead > tr:first-child > th,
      .table > colgroup + thead > tr:first-child > th,
      .table > thead:first-child > tr:first-child > th,
      .table > caption + thead > tr:first-child > td,
      .table > colgroup + thead > tr:first-child > td,
      .table > thead:first-child > tr:first-child > td {
        border-top: 0;
      }
      .table > tbody + tbody {
        border-top: 2px solid #ddd;
      }
      .table .table {
        background-color: #fff;
      }
      .table-condensed > thead > tr > th,
      .table-condensed > tbody > tr > th,
      .table-condensed > tfoot > tr > th,
      .table-condensed > thead > tr > td,
      .table-condensed > tbody > tr > td,
      .table-condensed > tfoot > tr > td {
        padding: 5px;
      }
      .table-bordered {
        border: 1px solid #ddd;
      }
      .table-bordered > thead > tr > th,
      .table-bordered > tbody > tr > th,
      .table-bordered > tfoot > tr > th,
      .table-bordered > thead > tr > td,
      .table-bordered > tbody > tr > td,
      .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
      }
      .table-bordered > thead > tr > th,
      .table-bordered > thead > tr > td {
        border-bottom-width: 2px;
      }
      .table-striped > tbody > tr:nth-child(odd) > td,
      .table-striped > tbody > tr:nth-child(odd) > th {
        background-color: #f9f9f9;
      }
      .table-hover > tbody > tr:hover > td,
      .table-hover > tbody > tr:hover > th {
        background-color: #f5f5f5;
      }
      table col[class*="col-"] {
        position: static;
        display: table-column;
        float: none;
      }
      table td[class*="col-"],
      table th[class*="col-"] {
        position: static;
        display: table-cell;
        float: none;
      }
      .table > thead > tr > td.active,
      .table > tbody > tr > td.active,
      .table > tfoot > tr > td.active,
      .table > thead > tr > th.active,
      .table > tbody > tr > th.active,
      .table > tfoot > tr > th.active,
      .table > thead > tr.active > td,
      .table > tbody > tr.active > td,
      .table > tfoot > tr.active > td,
      .table > thead > tr.active > th,
      .table > tbody > tr.active > th,
      .table > tfoot > tr.active > th {
        background-color: #f5f5f5;
      }
      .table-hover > tbody > tr > td.active:hover,
      .table-hover > tbody > tr > th.active:hover,
      .table-hover > tbody > tr.active:hover > td,
      .table-hover > tbody > tr.active:hover > th {
        background-color: #e8e8e8;
      }
      .table > thead > tr > td.success,
      .table > tbody > tr > td.success,
      .table > tfoot > tr > td.success,
      .table > thead > tr > th.success,
      .table > tbody > tr > th.success,
      .table > tfoot > tr > th.success,
      .table > thead > tr.success > td,
      .table > tbody > tr.success > td,
      .table > tfoot > tr.success > td,
      .table > thead > tr.success > th,
      .table > tbody > tr.success > th,
      .table > tfoot > tr.success > th {
        background-color: #dff0d8;
      }
      .table-hover > tbody > tr > td.success:hover,
      .table-hover > tbody > tr > th.success:hover,
      .table-hover > tbody > tr.success:hover > td,
      .table-hover > tbody > tr.success:hover > th {
        background-color: #d0e9c6;
      }
      .table > thead > tr > td.info,
      .table > tbody > tr > td.info,
      .table > tfoot > tr > td.info,
      .table > thead > tr > th.info,
      .table > tbody > tr > th.info,
      .table > tfoot > tr > th.info,
      .table > thead > tr.info > td,
      .table > tbody > tr.info > td,
      .table > tfoot > tr.info > td,
      .table > thead > tr.info > th,
      .table > tbody > tr.info > th,
      .table > tfoot > tr.info > th {
        background-color: #d9edf7;
      }
      .table-hover > tbody > tr > td.info:hover,
      .table-hover > tbody > tr > th.info:hover,
      .table-hover > tbody > tr.info:hover > td,
      .table-hover > tbody > tr.info:hover > th {
        background-color: #c4e3f3;
      }
      .table > thead > tr > td.warning,
      .table > tbody > tr > td.warning,
      .table > tfoot > tr > td.warning,
      .table > thead > tr > th.warning,
      .table > tbody > tr > th.warning,
      .table > tfoot > tr > th.warning,
      .table > thead > tr.warning > td,
      .table > tbody > tr.warning > td,
      .table > tfoot > tr.warning > td,
      .table > thead > tr.warning > th,
      .table > tbody > tr.warning > th,
      .table > tfoot > tr.warning > th {
        background-color: #fcf8e3;
      }
      .table-hover > tbody > tr > td.warning:hover,
      .table-hover > tbody > tr > th.warning:hover,
      .table-hover > tbody > tr.warning:hover > td,
      .table-hover > tbody > tr.warning:hover > th {
        background-color: #faf2cc;
      }
      .table > thead > tr > td.danger,
      .table > tbody > tr > td.danger,
      .table > tfoot > tr > td.danger,
      .table > thead > tr > th.danger,
      .table > tbody > tr > th.danger,
      .table > tfoot > tr > th.danger,
      .table > thead > tr.danger > td,
      .table > tbody > tr.danger > td,
      .table > tfoot > tr.danger > td,
      .table > thead > tr.danger > th,
      .table > tbody > tr.danger > th,
      .table > tfoot > tr.danger > th {
        background-color: #f2dede;
      }
      .table-hover > tbody > tr > td.danger:hover,
      .table-hover > tbody > tr > th.danger:hover,
      .table-hover > tbody > tr.danger:hover > td,
      .table-hover > tbody > tr.danger:hover > th {
        background-color: #ebcccc;
      }
    </style>

  </head>

<body >
    <h3>Barangay Assembly Household Participation Rate</h3>

    <span style="float:right;display:block;text-align:right">Region: <i>{{ $region }}</i></span>

    <span style="float:right;display:block;text-align:right">Cycle : <i>{{ $cycle }}</i></span>
    
    <!-- Iterate all fundsource since the data must be per fundsource -->
    <!--  take not fundsource is equal to program /program_id -->

    @foreach($fundsources as $fundsource)
      <span>Fund Source : <i>{{ $fundsource }}</i></span>

      <?php 
        // get all municipality model
         $bareports = $reports_instance->getHouseholdParticipationRate($office_level,$fundsource,$cycle,$mode);

      ?>
      <table class="table table-bordered">
        <thead>


          <tr>
            <th>{{ $office_level == 'ACT' ? 'Barangay' : 'Municipality' }}</th>
            <th>1st BA(%)</th>
            <th>2nd BA(%)</th>
            <th>3rd BA(%)</th>
            <th>4th BA(%)</th>
            <th>5th BA(%)</th>
     
          </tr>
        </thead>
        <tbody>

             <!-- iterate all municipal modelto get report -->

             {{--IF ACT LEVEL MUST ITERATE PER BARANGAY--}}

             <?php 
                  $one_ba = 0; 
                  $two_ba = 0; 
                  $three_ba = 0; 
                  $four_ba = 0; 
                  $fifth_ba = 0;
                ?>
             <?php if($office_level == 'ACT'){ ?>

                 @foreach($bareports->barangay as $ba)
                 <tr>
                    <td><h5>{{ $ba->barangay }}</h5></td>
                    <td><h5>{{ $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'1st BA') }}</h5></td>
                    <td><h5>{{ $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'2nd BA') }}</h5></td>
                    <td><h5>{{ $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'3rd BA') }}</h5></td>
                    <td><h5>{{ $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'4th BA') }}</h5></td>
                    <td><h5>{{ $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'5th BA') }}</h5></td>
                 </tr>
                 <?php 
                    $one_ba += $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'1st BA') ; 
                    $two_ba += $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'2nd BA'); 
                    $three_ba += $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'3rd BA') ; 
                    $four_ba += $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'4th BA') ; 
                    $fifth_ba += $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'5th BA') ; 
                    
                  ?>
                @endforeach
                    <tr>
                  <td>TOTAL</td>
                  <td>{{ $one_ba }}</td>
                  <td>{{ $two_ba }}</td>
                  <td>{{ $three_ba }}</td>
                  <td>{{ $four_ba }}</td>
                  <td>{{ $fifth_ba}} </td>
                </tr>


               {{-- ELSE MUST ITERATE PER MUNICIPALITY--}}

                <?php 
                  $one_ba = 0; 
                  $two_ba = 0; 
                  $three_ba = 0; 
                  $four_ba = 0; 
                  $fifth_ba = 0;
                ?>
             <?php } else { ?>
                 @foreach($bareports as $ba)
                 <tr>
                    <td>{{ $ba->municipality }}, {{ $ba->province->province}}</td>
                    <td>{{ $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'1st BA') }}</td>
                    <td>{{ $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'2nd BA') }}</td>
                    <td>{{ $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'3rd BA') }}</td>
                    <td>{{ $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'4th BA') }}</td>
                    <td>{{ $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'5th BA') }}</td>
                 </tr>
                 <?php 
                    $one_ba += $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'1st BA') ; 
                    $two_ba += $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'2nd BA'); 
                    $three_ba += $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'3rd BA') ; 
                    $four_ba += $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'4th BA') ; 
                    $fifth_ba += $ba->barangayAssembliesBATotal($office_level,$fundsource,$cycle,$mode,'5th BA') ; 
                    
                  ?>
                @endforeach
                      <tr>
                  <td>TOTAL</td>
                  <td>{{ $one_ba }}</td>
                  <td>{{ $two_ba }}</td>
                  <td>{{ $three_ba }}</td>
                  <td>{{ $four_ba }}</td>
                  <td>{{ $fifth_ba}} </td>
                </tr>
            <?php } ?>
        <tbody>
      </table>  

    @endforeach



</body>
</html>