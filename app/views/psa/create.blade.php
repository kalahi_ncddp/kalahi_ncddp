@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')

				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
				{{ Form::open(array('url' => 'psa')) }}

				<div class="form-group">
					<label for="">Barangay <i class="text-red">*</i></label>
					{{ Form::select('barangay', $brgy_list, Input::old('barangay'), array('class' => 'form-control', 'required','id'=>'barangay')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('psgc_id', 'PSGC Code') }}
					{{ Form::text('psgc_id', Input::old('psgc_id'), array('class' => 'form-control', 'required', 'readonly')) }}
				</div>
				
				<div class="form-group hidden">
					{{ Form::label('kc_code', 'KC Code') }}
					{{ Form::text('kc_code', Input::old('kc_code'), array('class' => 'form-control')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('municipality', 'Municipality') }}
					{{ Form::text('municipality', $municipality, array('class' => 'form-control', 'readonly')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('province', 'Province') }}
					{{ Form::text('province', $province, array('class' => 'form-control', 'readonly')) }}
				</div>

				<div class="form-group">
					{{ Form::label('cycle_id', 'Cycle') }}
					{{ Form::select('cycle_id', $cycle_list, $cycle, array('class' => 'form-control', 'required')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('program_id', 'Program') }}
					{{ Form::text('program_id', $program, array('class' => 'form-control', 'required')) }}
				</div>
				

				{{ Form::submit('Add PS Analysis', array('class' => 'btn btn-primary')) }}
		         <a class="btn bg-navy" href="{{ URL::to('psa')}}">Close</a>
				{{ Form::close() }}
			
		
		<script>
			$(document).ready(function(){
				$("#barangay").change(function (e) {
					$('.hidden').hide()
					$("#psgc_id").attr('value', $("#barangay").val());
					var selector = "option[value='" +$("#barangay").val()+ "']";
					$(selector).attr('selected', 'selected');
					
				});

				$('form').submit(function(){
					var warning = "";
					var null_exist = false;
					$('form .form-control').each(function(i,e){
					   if(e.required) {
					      if($(this).val() == ""){

					         warning += $(this).siblings('label').text() + " is required \n";
					      } 
					   }
					});
					
					if(warning==""){
					
					}else{
						alert(warning);
						return null_exist;
					}			
						
				});
			});
		</script>
@stop