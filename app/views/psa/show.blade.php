@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif

			@if($psa->is_draft==1)
	                		 	<div class="alert alert-info">This is still a draft  <a href="{{ URL::to('psa/' .$psa->activity_id.'/edit') }}">edit the document</a> to set it in final draft</div>
             @endif
		 <a class="btn  btn-default" href="{{ URL::to('psa') }}">Go Back</a>
		 <div class="pull-right">


	    	@if( !is_review($psa->activity_id) )

			 {{ Form::open(array('url' => 'psa/' . $psa->activity_id)) }}
				{{ Form::hidden('_method', 'DELETE') }}
				{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
		 	<a class="btn  btn-small btn-info" href="{{ URL::to('psa/' .$psa->activity_id.'/edit') }}">
		 		Edit
		 	</a>
            @endif
			 @if($position=="ACT" )
				<a class="btn  btn-small btn-info" href="{{ URL::to('psa/' .$psa->activity_id.'/edit') }}">
			 		Edit
			 	</a>
			 @endif
            <a class="btn bg-navy" href="{{ URL::to('psa/'.$psa->activity_id.'/priority-problems') }}">Priority Problems and Proposed Solutions</a>
			<a class="btn bg-olive" data-toggle="modal" data-target="#pincos_grievances">

	        	PINCOS and Grievances
	        </a>
			 {{ Form::close() }}</h2> 
		 </div>
		<br>
		<br>
		 {{ viewComment($psa->activity_id) }}
        <div class="box box-primary">
    		<div class="box-header">
    			<div class="box-title">
    				Details
    			</div>
    			<div class="box-tools pull-right">
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                </div>
    		</div>
			<div class="box-body">
				<div class="table-responsive">
					<div class="col-md-6">

					<table class="table table-striped table-bordered table-hover">

			            <tr>
			                <th>Region</th>
			                <td>{{ $region = Report::get_region_by_brgy_id($psa->psgc_id) }}</td>
			            </tr>
			            <tr>
			                <th>Municipality</th>
			                <td>{{ $municipality = Report::get_municipality_by_brgy_id($psa->psgc_id) }}</td>
			            </tr>
			            <tr>
			                <th>Province</th>
			                <td>{{ $province = Report::get_province_by_brgy_id($psa->psgc_id) }}</td>
			            </tr>
			         	<tr>
                            <th>Barangay</th>
                            <td>{{ $barangay = Report::get_barangay($psa->psgc_id) }}</td>
                        </tr>

                    </table>
                </div>
					<div class="col-md-6">

						<table class="table table-striped tabled-bordered table-hover">

			               <tr>
			                <th>Start Date</th>
			                <td>{{ $startdate = Report::get_date_conducted($psa->activity_id) }}</td>
			            </tr>
			            <tr>
			                <th>End Date</th>
			                <td>{{ $enddate = Report::get_end_date($psa->activity_id) }}</td>
			            </tr>
			              <tr>
			                <th>Program</th>
			                <td>{{ $psa->program_id }}</td>
			            </tr>
			             <tr>
			                <th>Cycle</th>
			                <td>{{ $psa->cycle_id }}</td>
			            </tr>

					</table>
				</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>



		<div class="box box-primary">
    		<div class="box-header">
    			<div class="box-title">
    				Attendees
    			</div>
    			<div class="box-tools pull-right">
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                </div>
    		</div>
    		
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
			            <tr>
			            	<th>Attendees </th>
			                <th>Male</th>
			                <th>Female</th>
			                <th>Total</th>
			            </tr>
			            <tr>

			            	<th>No. of Person </th>
			                <td>{{ !empty( $psa->no_atnmale ) ? $psa->no_atnmale : '0'  }}</td>
			                <td>{{ !empty($psa->no_atnfemale  ) ? $psa->no_atnfemale : '0' }}</td>
			                <td>{{ intval( !empty( $psa->no_atnmale ) ?$psa->no_atnmale : 0) +  intval( !empty( $psa->no_atnfemale ) ? $psa->no_atnfemale : 0 ) }}</td>
			            </tr>
			            <tr>
			                <th>&nbsp;&nbsp;&nbsp; No. of IP</th>
			                <td>{{ $psa->getTraining()->total_ip('M') }}</td>
			                <td>{{  $psa->getTraining()->total_ip('F')}}</td>
			                <td>{{ intval( $psa->getTraining()->total_ip('M')) + intval($psa->getTraining()->total_ip('F'))}}</td>
			            </tr>
			                <th>&nbsp;&nbsp;&nbsp;No. of 60 yrs +</th>
			                <td>{{ $psa->getTraining()->age_participants( 'M', 60)  }}</td>
			                <td>{{ $psa->getTraining()->age_participants('F', 60) }}</td>
			            	<td>{{ (intval( $psa->getTraining()->age_participants('F', 60)  )  + intval( $psa->getTraining()->age_participants('M', 60) )) }}</td>
			            </tr>
				         <tr>

			            </tr>
					</table>
				</div>
			</div>
			
		</div>
		
		<div class="box box-primary">
    		<div class="box-header">
    			<div class="box-title">
    				PSA Documents
    			</div>
    			<div class="box-tools pull-right">
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                </div>
    		</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
			            <tr>
			            	<th>Document Name </th>
			                <th>Date</th>
			            </tr>

			            @foreach($psa_document as $document)
			            <tr>
			                <td>{{ $document->document_name}}</td>
			                <td>{{ $document->date!=0 ? date('m/d/Y',strtotime($document->date)) : 'no date' }}</td>
			            </tr>
			            @endforeach
			            
			
					</table>
				</div>
			</div>
		</div>

		<div class="box box-primary">
    		<div class="box-header">
    			<div class="box-title">
    				Purok/Sitios Represented
    			</div>
    			<div class="box-tools pull-right">
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                </div>
    		</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
			            @foreach($sitio_data as $sitio)
			            <tr>
			                <td>{{$sitio}}</td>
			            </tr>
			            @endforeach
					</table>
				</div>
			</div>
		</div>

		<div class="box box-primary">
    		<div class="box-header">
    			<div class="box-title">
    				Other Sectors Represented
    			</div>
    			<div class="box-tools pull-right">
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                </div>
    		</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						@foreach($sector_data as $sector)
			            <tr>
			                <td>{{$sector}}</td>
			            </tr>
			            @endforeach
					</table>
				</div>
			</div>
		</div>
		<div class="box box-primary">
        <!-- pincols and grivances modal -->
		@include('modals.pincos_grievances')

       
															
											
	<script>
			$(document).ready(function(){
			//when the Add Filed button is clicked
			$("#add").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items").append('<div class="input-group"><input type="text" class="form-control" name="sitio[]" oninput="checkduplicate(this)" placeholder="sitio"><div class="input-group-addon"><button type="button" class="delete">-</button></div></div>');
			});
			
			$("#add1").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items1").append('<br><div class="input-group"><input type="text" class="form-control" name="raise[]" oninput="checkduplicate1(this)" placeholder="Raised By" required><input type="text" class="form-control" name="issue[]" placeholder="Issue"" required><input type="text" class="form-control" name="action[]" placeholder="Action" required><div class="input-group-addon"><button type="button" class="delete">-</button></div></div>');
			});
			
			$("#add2").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items2").append($("#template").html());
			});

			$("body").on("click", ".delete", function (e) {
				$(this).parent("div").parent("div").remove();
			});
			
			
			$("input[name='startdate']").change(function (e) {
				$("input[name='enddate']").attr('min', $("input[name='startdate']").val());
			});
			
			$("input[name='enddate']").change(function (e) {
				$("input[name='startdate']").attr('max', $("input[name='enddate']").val());
			});
			
			
			$("input[name='no_atnmale']").change(function (e) {
				$("input[name='no_ipmale']").attr('max', $("input[name='no_atnmale']").val());
				$("input[name='no_oldmale']").attr('max', $("input[name='no_atnmale']").val());
			});
			
			$("input[name='no_atnfemale']").change(function (e) {
				$("input[name='no_ipfemale']").attr('max', $("input[name='no_atnfemale']").val());
				$("input[name='no_oldfemale']").attr('max', $("input[name='no_atnfemale']").val());
			});
			
			
			});
			
			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName('sitio[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate1(e){
				var count = -1;
				var elements = document.getElementsByName('document_name[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate2(e){
				var count = -1;
				var elements = document.getElementsByName('rank[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate3(e){
				var count = -1;
				var elements = document.getElementsByName('problem[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}


			// added script 

			$('.draft').on('click',function(){
				var con = confirm('Are you sure that you want to set it to final draft');

				if(con){
					$.get('{{ $psa->activity_id }}/set',function(){
						window.location.href = '{{ URL::to('psa') }}';
					});
				}

			});
			$(document).ready(function(){
				$("form").submit(function(){

					var confirmation = confirm('are you sure to this action?');
					if(confirmation){
						var secondConfirmation = confirm('this action will reflect to the data of '+ $('small').text() )
						if(secondConfirmation)
							return true;
						else
							return false;
					}
					else
						return false;

				});
			});
			
		</script>												
		
	
@stop