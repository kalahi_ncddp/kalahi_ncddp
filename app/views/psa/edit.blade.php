@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				<a href="{{ URL::previous(); }}" class="btn btn-default">Go back</a>
	    {{ Form::model($psa, array('action' => array('PSAController@update', $psa->activity_id), 'method' => 'PUT')) }}
	    <div class="row">
		    <div class="col-md-12">
	            <div class="box box-primary">
		    		<div class="box-header">
		    			<div class="box-title">
		    				Details
		    			</div>
		    			<div class="box-tools pull-right">
		                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
		                </div>
		    		</div>
					<div class="box-body">
						<div class="table-responsive">
							<div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					                <th>Region</th>
					                <td>{{ $region = Report::get_region_by_brgy_id($psa->psgc_id) }}</td>
					            </tr>
					            <tr>
					                <th>Province</th>
					                <td>{{ $province = Report::get_province_by_brgy_id($psa->psgc_id) }}</td>
					            </tr>
					            <tr>
					                <th>Municipality</th>
					                <td>{{ $municipality = Report::get_municipality_by_brgy_id($psa->psgc_id) }}</td>
					            </tr>
							 	<tr>
					                <th>Barangay</th>
					                <td>{{ $barangay = Report::get_barangay($psa->psgc_id) }}</td>
					            </tr>
					            <tr>
					                <th>KC Code</th>
					                <td></td>
					            </tr>
					        </table>
					    </div>
					          <div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">

					            <tr>
					                <th>Start Date - End Date</th>
					                <td>
					                	<div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control pull-left" value="{{ Report::get_date_conducted($psa->activity_id) }} - {{ Report::get_end_date($psa->activity_id) }}" id="daterange" required/>
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div><!-- /.input group -->
                                    </div>

				                	{{ Form::input('text', 'startdate'	, Report::get_date_conducted($psa->activity_id), array('class' => 'form-control hidden', 'max' => date("m/d/Y"), '')) }}
				                	{{ Form::input('text', 'enddate', Report::get_end_date($psa->activity_id), array('class' => ' hidden form-control', 'max' => date("m/d/Y"), '')) }}
									</td>
					            </tr>
					             <tr>
					                <th>Cycle</th>
					                <td>{{ $psa->cycle_id }}</td>
					            </tr>
					              <tr>
					                <th>Program</th>
					                <td>{{ $psa->program_id }}</td>
					            </tr>
					        </table>
					    </div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>

			<!-- 	<div class="box box-primary">
		    		<div class="box-header">
		    			<div class="box-title">
		    				Attendees
		    			</div>
		    			<div class="box-tools pull-right">
		                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
		                </div>
		    		</div>
		    		
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Attendees </th>
					                <th>Male</th>
					                <th>Female</th>
					                <th>Total</th>
					            </tr>
					            <tr>

					            	<th>No. of Person </th>
					                <td>
					                	<?php 


					                		$atnmale = '';
				                			$atnfemale = '';
				                			$no_ipmale = '';
				                			$no_ipfemale = '';
				                			$no_oldmale = '';
				                			$no_oldfemale = '';
					                		if( $psa->getTraining()!=null ) {
					                			$atnmale = $psa->getTraining()->total_participants_by_genders('M') | 0;
					                			$atnfemale =  $psa->getTraining()->total_participants_by_genders('F') | 0 ;
					                			$no_ipmale = $psa->getTraining()->total_ip('M')| 0;
					                			$no_ipfemale = $psa->getTraining()->total_ip('F') |0;
					                			$no_oldmale = $psa->getTraining()->age_participants('M')|0;
					                			$no_oldfemale = $psa->getTraining()->age_participants('F') | 0;

					                		} else {
					                			$atnmale = 0;
					                			$atnfemale = 0;
					                			$no_ipmale = 0;
					                			$no_ipfemale = 0;
					                			$no_oldmale = 0; 
					                			$no_oldfemale = 0;
					                		}
					                	?>
					                	{{ Form::input('number', 'no_atnmale', $psa->no_atnmale  == 0? $atnmale : $psa->no_atnmale, array('class' => 'form-control', 'min'=>'0', 'required')) }}
					               	</td>
					                <td>
					                	
					                	{{ Form::input('number', 'no_atnfemale', $psa->no_atnfemale  == 0? $atnfemale : $psa->no_atnfemale, array('class' => 'form-control', 'min'=>'0', 'required')) }}
					                </td>
									<td class="total_person"></td>
					            </tr>
					            <tr>
					                <th>No. of IP</th>
					                <td>
					                	
					                	{{ Form::input('number', 'no_ipmale', $psa->no_ipmale  == 0? $no_ipmale : $psa->no_atnmale, array('class' => 'form-control', 'min'=>'0', 'max' => $psa->no_ipmale, 'required')) }}
							        </td>
					                <td>
					                	{{ Form::input('number', 'no_ipfemale', $psa->no_ipfemale== 0? $no_ipfemale : $psa->no_atnmale, array('class' => 'form-control', 'min'=>'0', 'max' => $psa->no_atnfemale,'required')) }}
					                </td>
					                 <td class="total_ip"></td>
					            </tr>
					                <th>No. of 60 yrs +</th>
					                <td>
					                	{{ Form::input('number', 'no_oldmale', $psa->no_oldmale == 0? $no_oldmale : $psa->no_oldmale, array('class' => 'form-control', 'min'=>'0', 'max' => $psa->no_atnmale, 'required')) }}
							       	</td>
					                <td>
					                	{{ Form::input('number', 'no_oldfemale', $psa->no_oldfemale == 0?$no_oldfemale : $psa->no_oldfemale, array('class' => 'form-control', 'min'=>'0', 'max' => $psa->no_atnfemale,'required')) }}
							        </td>
							         <td class="total_old"></td>
					            </tr>
							</table>
						</div>
					</div>
				</div> -->
				
				<div class="box box-primary">
		    		<div class="box-header">
		    			<div class="box-title">
		    				PSA Documents
		    			</div>
		    			<div class="box-tools pull-right">
		                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
		                </div>
		    		</div>
					<div class="box-body">
						<div class="table-responsive">
							<table id="items1" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<tr>
					          			<td>Document Name</td>
					          			<td>Date </td>
					          			<td>Option</td>
					          		</tr>
					          	</thead>
					            @foreach($psa_document as $document)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="document name" value="{{$document->document_name}}" name="document_name[]" oninput="checkduplicate1(this)" placeholder="document name" required>
					            		</td>
					            		<td>
						            		<input  class="form-control dates {{$document->document_name}}" value="{{ date('m/d/Y',strtotime($document->date)) }}" name="date[]">
					            		</td>
					            		<td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            			<script type="text/javascript">
					            				$('.dates .{{$document->document_name}}').val('{{ date('m/d/Y',strtotime($document->date)) }}');
					            			</script>
					            		</td>
					            	</tr>
					            @endforeach
					          
					            <tr>
					            	<td>
					            		<input type="text" class="form-control" placeholder="document name" name="document_name[]" oninput="checkduplicate1(this)" palceholder="document name" {{ empty($psa_document)? 'required':'' }}>
					            	</td>
					            	<td>
					            		<input  class="form-control dates"  name="date[]">
					            	</td>
					            	<td>
					            		<button type="button" class="btn delete">
					            			<i class="fa fa-trash-o"></i>
					            		</button>
					            	</td>
					            </tr>
							</table>
					        <button id="add1" class="btn btn-info form-control " type="button" >
					            	<i class="fa fa-plus disabled"></i>
					            	Add PSA Document</button>
						</div>
					</div>
				</div>
				<div class="box box-primary">
		    		<div class="box-header">
		    			<div class="box-title">
		    				Purok/Sitios Represented
		    			</div>
		    			<div class="box-tools pull-right">
		                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
		                </div>
		    		</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <div class="input-group">
					            		<input type="text" class="form-control" placeholder="Name of sitio eg. STO SITIO" name="sitio[]" oninput="checkduplicate(this)">
					            		 <span class="input-group-addon">
						                	 <span class="fa fa-trash-o  delete">
						                      </span>
						                </span>
					            </div><br>
					            <div id="items">
						            @foreach($sitio_data as $sitio)
						            	<div class="input-group">
						            		<input type="text" class="form-control" placeholder="Name of sitio eg. STO SITIO" value="{{$sitio}}" name="sitio[]" oninput="checkduplicate(this)" required>
						            		 <span class="input-group-addon">
							                	 <span class="fa fa-trash-o delete ">
							                      </span>
							                </span>
						            	</div><br>
						            @endforeach
					            </div>
					            <br>
					            <button id="add" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add Sitio</button>
							</table>
						</div>
					</div>
				</div>


				<div class="box box-primary">
		    		<div class="box-header">
		    			<div class="box-title">
		    				Other Sectors Represented
		    			</div>
		    			<div class="box-tools pull-right">
		                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
		                </div>
		    		</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								@foreach($sector_list as $sector)
									@if(in_array($sector, $sector_data))
										<div class = "form-control"><input type="checkbox" name="sector[]" value="{{$sector}}" checked>&nbsp{{$sector}}</div>
									@else
										<div class = "form-control"><input type="checkbox" name="sector[]" value="{{$sector}}">&nbsp{{$sector}}</div>
									@endif
								@endforeach
							</table>
						</div>
					</div>
				</div>
				
				{{--<div class="box box-primary">--}}
		    		{{--<div class="box-header">--}}
		    			{{--<div class="box-title">--}}
		    				{{--Priority Problems and Proposed Solutions--}}
		    			{{--</div>--}}
		    			{{--<div class="box-tools pull-right">--}}
		                    {{--<span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>--}}
		                {{--</div>--}}
		    		{{--</div>--}}
					{{--<div class="box-body">--}}
						{{--<div class="table-responsive">--}}
							{{--<table id="items2" class="table table-striped table-bordered table-hover">--}}
									{{--<thead>--}}
										{{--<tr>--}}
											{{--<td>Rank</td>--}}
											{{--<td>Problem</td>--}}
											{{--<td>Problem Category</td>--}}
											{{--<td>Solution</td>--}}
											{{--<td>Solution Category</td>--}}
											{{--<td>Option</td>--}}
										{{--</tr>	--}}
									{{--</thead>--}}
					                {{--@foreach($psa_problem as $problem)--}}
					            	{{--<tr>--}}
					            		{{--<td>--}}
					            			{{--<input type="number" min="0" class="form-control" value="{{$problem->rank}}" name="rank[]" oninput="checkduplicate2(this)" required>--}}
					            		{{--</td>--}}
					            		{{--<td>--}}
					            			{{--<textarea type="text" class="form-control" value="{{$problem->problem}}" name="problem[]" oninput="checkduplicate3(this)" required>{{$problem->problem}}</textarea>--}}
					            		{{--</td>--}}
					            		{{--<td>--}}
						            		{{--<select class="form-control" value="{{$problem->problem_cat}}" name="problem_cat[]" required>--}}
						            			{{--@foreach($problem_cat as $pcat)--}}
						            				{{--<option value="{{$pcat}}" {{  $problem->problem_cat==$pcat ? 'selected' : ''  }}> {{$pcat}} </option>--}}
						            			{{--@endforeach--}}
						            		{{--</select>--}}
					            		{{--</td>--}}
					            		{{--<td>--}}
					            			{{--<textarea type="text" class="form-control" value="{{$problem->solution}}" name="solution[]" required>{{$problem->solution}}</textarea>--}}
					            		{{--</td>--}}
					            		{{--<td>--}}
						            		{{--<select class="form-control" value="{{$problem->solution_cat}}" name="solution_cat[]" required>--}}
						            			{{--@foreach($solution_cat as $pcat)--}}
						            				{{--<option value="{{$pcat}}" {{  $problem->solution_cat==$pcat ? 'selected' : ''  }}> {{$pcat}} </option>--}}
						            			{{--@endforeach--}}
						            		{{--</select>--}}
					            		{{--</td>--}}
					            		{{--<td>--}}
					            			{{--<button type="button" class="delete"> --}}
					            				{{--<i class="fa fa-trash-o"></i>--}}
					            			{{--</button>--}}
					            		{{--</td>--}}
					            	{{--</tr>--}}
					           		{{--@endforeach--}}
					            	{{--<tr id="template">--}}
					            		{{--<td>--}}
					            			{{--<input type="number" min="0" class="form-control" value="" name="rank[]" oninput="checkduplicate2(this)" {{ empty($psa_document)? 'required':'' }}>--}}
					            		{{--</td>--}}
					            		{{--<td>--}}
					            			{{--<textarea type="text" class="form-control" value="" name="problem[]" oninput="checkduplicate3(this)" required></textarea>--}}
					            		{{--</td>--}}
					            		{{--<td>--}}
						            		{{--<select class="form-control" value="" name="problem_cat[]" required>--}}
						            			{{--@foreach($problem_cat as $pcat)--}}
						            				{{--<option value="{{$pcat}}"> {{$pcat}} </option>--}}
						            			{{--@endforeach--}}
						            		{{--</select>--}}
					            		{{--</td>--}}
					            		{{--<td>--}}
					            			{{--<textarea type="text" class="form-control" value="" name="solution[]" required></textarea>--}}
					            		{{--</td>--}}
					            		{{--<td>--}}
						            		{{--<select class="form-control" value="" name="solution_cat[]" required>--}}
						            			{{--@foreach($solution_cat as $pcat)--}}
						            				{{--<option value="{{$pcat}}"> {{$pcat}} </option>--}}
						            			{{--@endforeach--}}
						            		{{--</select>--}}
					            		{{--</td>--}}
					            		{{--<td>--}}
					            			{{--<button type="button" class="delete"> --}}
					            				{{--<i class="fa fa-trash-o"></i>--}}
					            			{{--</button>--}}
					            		{{--</td>--}}
					            	{{--</tr>--}}
					            {{----}}
					           {{----}}
							{{--</table>--}}
					        {{--<button id="add2" class="btn btn-info form-control" type="button" >--}}
					        	{{--<i class="fa fa-plus"></i>--}}
					        	{{--Add more Priority Problems--}}
					        {{--</button>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--</div>	--}}
				<div class="form-group pull-right">
					<label>Save as draft</label>
					 	    <input type="checkbox" name="is_draft" {{ $psa->is_draft==1 ? 'checked': '' }}>
				
				</div>
					<!-- {{ Form::submit('Save', array('class' => 'btn btn-info')) }}
					 <a href="{{ URL::to('psa') }}" class="btn bg-navy">close</a>
 -->
 <div class="col-md-12 ">
									<div class="form-group pull-right">
						                <button class="btn btn-primary" id="save">
											<i class="fa fa-save"></i>
											Submit 
										</button>
		                 				<a class="btn btn-default" href="{{ URL::to('psa') }}">Cancel</a>		
									</div>
								</div>
								<div class="clearfix"></div>														
				

				{{ Form::close() }}
			</div>
		</div>
		<script>
			$(document).ready(function(){
			//when the Add Filed button is clicked
			$('body .dates').datetimepicker({
			            pick12HourFormat: false,
			            pickTime:false
			});
			$('#daterange').daterangepicker(
			  { 
			    format: 'MM/DD/YYYY'
			  },
			  function(start, end, label) {
			   	   	$('input[name="startdate"]').val(start.format('MM/DD/YYYY'));
			  		$('input[name="enddate"]').val(end.format('MM/DD/YYYY'));
			  }
			);
			$("#add").click(function (e) {
				//Append a new row of code to the "#items" div
					$("#items").append('<div class="input-group"><input type="text" class="form-control" placeholder="Name of sitio eg. STO SITIO" name="sitio[]" oninput="checkduplicate(this)"><span class="input-group-addon"> <span class="fa fa-trash-o  delete"></span></span></div><br>');

			});
			
			$("#add1").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items1").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="document name" name="document_name[]" oninput="checkduplicate1(this)" palceholder="document name" required>'+
					            	'</td>'+
					            	'<td><input type="text" class="form-control dates"  name="date[]"></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				$('body .dates').datetimepicker({
				            pick12HourFormat: false,
				            pickTime:false
				});
				// $('.dates').inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
			});
			
			$("#add2").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items2").append('<tr>'+ $("#template").html() +'</tr>');
			});

			$("body").on("click", ".delete", function (e) {
				$(this).parent().parent().remove();
			});
			
			
			$("input[name='startdate']").change(function (e) {
				$("input[name='enddate']").attr('min', $("input[name='startdate']").val());
			});
			
			$("input[name='enddate']").change(function (e) {
				$("input[name='startdate']").attr('max', $("input[name='enddate']").val());
			});
			
			
			$("input[name='no_atnmale']").change(function (e) {
				$("input[name='no_ipmale']").attr('max', $("input[name='no_atnmale']").val());
				$("input[name='no_oldmale']").attr('max', $("input[name='no_atnmale']").val());
			});
			
			$("input[name='no_atnfemale']").change(function (e) {
				$("input[name='no_ipfemale']").attr('max', $("input[name='no_atnfemale']").val());
				$("input[name='no_oldfemale']").attr('max', $("input[name='no_atnfemale']").val());
			});
			
			
			});
			
			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName('sitio[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate1(e){
				var count = -1;
				var elements = document.getElementsByName('document_name[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate2(e){
				var count = -1;
				var elements = document.getElementsByName('rank[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate3(e){
				var count = -1;
				var elements = document.getElementsByName('problem[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count > 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			/* get the total in attendees */
	
			var total = (function($){
				return {
					
					totalPerson : function(){
						var total_ = parseInt($("input[name='no_atnfemale']").val()) + parseInt($("input[name='no_atnmale']").val()) ;
						$('.total_person').text(total_ || '0');
					},
					totalIP : function(){
						var total_ = parseInt($("input[name='no_ipfemale']").val()) + parseInt($("input[name='no_ipmale']").val()) ;
						$('.total_ip').text(total_ || '0');
					},
					totalOld : function(){
						var total_ = parseInt($("input[name='no_oldfemale']").val()) + parseInt($("input[name='no_oldmale']").val()) ;
						$('.total_old').text(total_ || '0');
					}

				}
			})(jQuery);


			$("input[name='no_atnmale']").keyup(total.totalPerson);
			$("input[name='no_ipmale']").keyup(total.totalIP);
			$("input[name='no_oldmale']").keyup(total.totalOld);
			$("input[name='no_atnfemale']").keyup(total.totalPerson);
			$("input[name='no_ipfemale']").keyup(total.totalIP);
			$("input[name='no_oldfemale']").keyup(total.totalOld);

		</script>
	
@stop