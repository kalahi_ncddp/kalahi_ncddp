@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')

				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
	<div class="panel panel-default">
		<div class="panel-heading">
			{{ $title }}
			 @if($position->position!='encoder')
                    <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                    <span class="hidden module">psa</span>
             @endif
			<!-- <a class="btn  btn-primary pull-right" href="{{ URL::to('psa/create') }}"><i class="fa fa-plus"></i> Add New</a> -->

			<span class="btn  btn-primary pull-right" data-toggle="modal" data-target="#add_psa">
				<i class="fa fa-plus"></i> Add from Community Tranings
			</span>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
	                            @if($position->position!='encoder')
    						    <th>Select all
    						        <input type="checkbox" id="selecctall"/>
    						    </th>
    						    @endif
							<th>Region</th>
							<th>Province</th>
							<th>Municipality</th>
							<th>Cycle</th>
							<th>KC Group</th>
							<th>Barangay</th>
							<th>Date Conducted</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $psa)
						<tr>
                            <td>
                            @if($position->position!='encoder')
                               @if($psa->is_draft==0)
                                 {{ $reference->approval_status($psa->activity_id , 'ACT') }}
                               @else
                                   <span class="label label-warning">draft</span>
                               @endif
                             @endif
                            </td>
							<td>
								{{ $region = Report::get_region_by_brgy_id($psa->psgc_id) }}
							</td>
							<td>
								{{ $province = Report::get_province_by_brgy_id($psa->psgc_id) }}
							</td>
							<td>
								{{ $municipality = Report::get_municipality_by_brgy_id($psa->psgc_id) }}
							</td>
							<td>
								{{ $psa->cycle_id }}
							</td>
							<td>
							</td>
							<td>
								{{ $barangay = Report::get_barangay($psa->psgc_id) }}
							</td>
							<td>
								{{ $startdate = Report::get_date_conducted($psa->activity_id) }}
							</td>
							
							<td>
	                            @if($psa->is_draft==0)
		                            <a class="btn btn-success btn" href="{{ URL::to('psa/' .$psa->activity_id) }}">
		                              View Details {{ hasComment($psa->activity_id) }}
		                            </a>
		                        @else
		                        	<a class="btn btn-warning btn" href="{{ URL::to('psa/' .$psa->activity_id) }}">
		                              View ( draft )
		                            </a>
								@endif
	                        </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@include('modals.add_psa')
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable(
                {
                    "aoColumnDefs": [
                              { 'bSortable': false, 'aTargets': [ 0 ] }
                           ]
                }
                )

                $('#selecctall').click(function(event) {  //on click
                    if(this.checked) { // check select status
                        $('.checkbox1').each(function() { //loop through each checkbox
                            this.checked = true;  //select all checkboxes with class "checkbox1"
                        });
                    }else{
                        $('.checkbox1').each(function() { //loop through each checkbox
                            this.checked = false; //deselect all checkboxes with class "checkbox1"
                        });
                    }
                });


                $.fn.use_psa = function(vol){
					var data = {
						cycle_id : vol.find(".cycle_id").text(),
						program_id : vol.find(".program_id").text(),
						psgc_id	: vol.find(".psgc_id").text(),
						start_date :vol.find(".start_date").text(),
						end_date : vol.find(".end_date").text(),
						
					};
					$('.modal').loading(true);

					$.post('{{ URL::to('psa') }}',data,function(res){
					$('.modal').loading(false);
						$("#myModal").modal('toggle');
						if(res.error){
							window.location.reload();
						}else{	
							window.location.href = '{{ URL::to('psa') }}/' + res.id;
						}
					});
				};
				
      });
    </script>
@stop