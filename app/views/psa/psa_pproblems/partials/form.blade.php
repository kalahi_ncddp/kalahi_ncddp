
<div class="col-md-6">
     <div class="box box-primary">
        <div class="box-header">
            <div class="box-title">Create Priority Proposal and Solutions</div>
        </div>
        <div class="box-body">

            <div class="col-xs-12">
                <div class="form-group">
                    {{ Form::label('psgc_id', 'rank') }}
                    {{ Form::number('rank',!empty($problem->rank) ? $problem->rank : '' , array('class' => 'form-control', 'required','placeholder'=>'Ex: 1')) }}
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    {{ Form::label('', 'Problem') }}
                    {{ Form::textarea('problem',!empty($problem->problem) ? $problem->problem : '' , array('class' => 'form-control', 'required','placeholder'=>'Ex: There is no electricity in the barangay etc...')) }}
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    {{ Form::label('', 'Problem Category') }}
                    {{ Form::select('problem_cat',$problem_cat,!empty($problem->problem_cat) ? $problem->problem_cat : '' , array('class' => 'form-control', 'required')) }}
                </div>
            </div>
             <div class="clearfix"></div>
        </div>
     </div>

     <div class="clearfix"></div>


</div>

<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header">
            <div class="box-title">Solutions</div>
        </div>
        <div class="box-body">
            <table class="table table-bordered">
                <thead>
                    <th>Solution</th>
                    <th>Solution Category</th>
                    <th></th>
                </thead>
                <tbody>
                    @foreach($psa_solutions as $psa_solution)
                        <tr>
                            <td> {{ Form::textarea('solution[]',$psa_solution->solution,['class'=>'form-control','required','placeholder'=>'Ex: Call a provider etc..','required']) }}</td>
                            <td>{{ Form::select('solution_cat[]',$solution_cat,$psa_solution->solution_cat,['class'=>'form-control','required']) }}</td>
                            <td>
                                <span class="btn btn-default"><i class="fa fa-trash-o"></i></span>
                            </td>
                        </tr>
                    @endforeach
                    <span class="hidden template">

                    </span>

                </tbody>
            </table>
            <button id="add_solutions" class="btn btn-info form-control" type="button" >
                <i class="fa fa-plus"></i>
                Add Solutions
            </button>
        </div>
    </div>
</div>
     {{ Form::submit('Submit Priority Problems and Solution', array('class' => 'btn btn-primary')) }}
<script>
    $('#add_solutions').on('click',function(){
        var template = ' <tr> '+
                        '    <td> '+
                        '       {{ Form::textarea('solution[]','',['class'=>'form-control','required','placeholder'=>'Ex: Call a provider etc..','required']) }} '+
                       '    </td>'+
                       '   <td>'+
                        '       {{ Form::select('solution_cat[]',$solution_cat,'',['class'=>'form-control','required']) }}'+
                         '   </td>'+
                        '    <td>'+
                        '        <span class="btn btn-default delete"><i class="fa fa-trash-o"></i></span>'+
                       '     </td>'+
                       ' </tr>';
        $('table tbody').append(template);
    });

    $("body").on("click", ".delete", function (e) {
    				$(this).parent().parent().remove();
    });
</script>

