@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('spi_profile') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <a href="{{ URL::to('psa/'.$problem->activity_id.'/priority-problems') }}" class="btn btn-default">Go Back</a>
    @if(!is_review($problem->activity_id,$level = 'ACT'))
    <a href="{{ URL::to('psa/'.$problem->activity_id.'/priority-problems/'.$problem->project_id.'/edit') }}" class="btn btn-info pull-right">Edit</a>
    @endif
    {{--{{ HTML::link('spi_profile.spcr.edit', 'Edit',--}}
      {{--array($spcr->project_id), array('class' => 'btn btn-info btn')) }}--}}
  </div>
</div>

<hr />

<div class="col-md-6">
     <div class="box box-primary">
        <div class="box-header">
            <div class="box-title">Create Priority Proposal and Solutions</div>
        </div>
        <div class="box-body">

            <div class="col-xs-12">
                <div class="form-group">
                    {{ Form::label('psgc_id', 'rank') }}
                    {{  $problem->rank  }}
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    {{ Form::label('', 'Problem') }}
                    {{$problem->problem }}
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    {{ Form::label('', 'Problem Category') }}
                    {{  $problem->problem_cat }}
                </div>
            </div>
             <div class="clearfix"></div>
        </div>
     </div>

     <div class="clearfix"></div>


</div>

<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header">
            <div class="box-title">Solutions</div>
        </div>
        <div class="box-body">
            <table class="table table-bordered">
                <thead>
                    <th>Solution</th>
                    <th>Solution Category</th>

                </thead>
                <tbody>
                    @foreach($psa_solutions as $psa_solution)
                        <tr>
                            <td> {{ $psa_solution->solution }} </td>
                            <td>{{ $psa_solution->solution_cat }}</td>
                        </tr>
                    @endforeach
                    <span class="hidden template">

                    </span>

                </tbody>
            </table>

        </div>
    </div>
</div>

@stop