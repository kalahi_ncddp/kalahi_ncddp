@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')

	@if ($errors->all())
		<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
	@endif
<a class="btn btn-default" href="{{ URL::to('psa/'.$id.'/priority-problems')}}">Go Back</a><br/>
	{{ Form::open(array('url' => array('psa/'.$id.'/priority-problems/'), 'method' => 'post', 'role' => 'form')) }}
	    @include('psa.psa_pproblems.partials.form')
    {{ Form::close() }}
@stop