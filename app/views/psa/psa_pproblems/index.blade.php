@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')

				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
    <a class="btn btn-default" href="{{ URL::to('psa/'.$id) }}">Go Back</a>
	<div class="panel panel-default">
		<div class="panel-heading">
			Records
            @if(!is_review($id))
			 <a class="btn  btn-primary pull-right" href="{{ URL::to('psa/'.$id.'/priority-problems/create') }}"><i class="fa fa-plus"></i> Add New</a>
            @endif
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>

							<th>Rank</th>
							<th>Problem</th>
							<th>Problem Category</th>
							<th>Number of Solutions</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($problems as $problem)
						<tr>
                            <td>
                                {{ $problem->rank }}
                            </td>
							<td>
								{{ $problem->problem }}
							</td>
							<td>
							    {{ $problem->problem_cat }}
							</td>
							<td>
								{{ $problem->solutions->count() }}
							</td>
							<td>
                                <a class="btn btn-success btn" href="{{ URL::to('psa/' .$problem->activity_id.'/priority-problems/'.$problem->project_id) }}">
                                  View
                                </a>
	                        </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@include('modals.add_psa')
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable(
                {
                    "aoColumnDefs": [
                              { 'bSortable': false, 'aTargets': [ 0 ] }
                           ]
                }
                )

                $('#selecctall').click(function(event) {  //on click
                    if(this.checked) { // check select status
                        $('.checkbox1').each(function() { //loop through each checkbox
                            this.checked = true;  //select all checkboxes with class "checkbox1"
                        });
                    }else{
                        $('.checkbox1').each(function() { //loop through each checkbox
                            this.checked = false; //deselect all checkboxes with class "checkbox1"
                        });
                    }
                });


                $.fn.use_psa = function(vol){
					var data = {
						cycle_id : vol.find(".cycle_id").text(),
						program_id : vol.find(".program_id").text(),
						psgc_id	: vol.find(".psgc_id").text(),
						start_date :vol.find(".start_date").text(),
						end_date : vol.find(".end_date").text(),
						
					};
					$('.modal').loading(true);

					$.post('{{ URL::to('psa') }}',data,function(res){
					$('.modal').loading(false);
					
						$("#myModal").modal('toggle');
						if(res.error){
							window.location.reload();
						}else{	
							window.location.href = '{{ URL::to('psa') }}/' + res.id;
						}
					});
				};
				
      });
    </script>
@stop