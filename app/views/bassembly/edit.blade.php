@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	@if ($errors->all())
		<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
	@endif
    <div class="col-md-12">
	 
	 <div class="col-md-12">
	 	{{ HTML::linkRoute('bassembly.show', 'Back to '.$brgy_assembly->activity_id,$brgy_assembly->activity_id, array('class' => 'btn btn-default btn')) }}
	 </div>
	 <br>
	 <br>
	 
	 {{ Form::model($brgy_assembly, array('action' => array('BAssemblyController@update', $brgy_assembly->activity_id), 'method' => 'PUT')) }}
	 	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Barangay Assembly Information</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	            <div class="box-body">
					<div class="table-responsive">
						<div class="col-md-6">  
				
						<table class="table table-striped  table-hover">
						 	<tr>
				                <th>Barangay</th>
				                <td>{{ $barangay = Report::get_barangay($brgy_assembly->psgc_id) }}</td>
				            </tr>
				            <tr>
				                <th>KC-NCDDP Code</th>
				                <td></td>
				            </tr>
				            <tr>
				                <th>Municipality</th>
				                <td>{{ $municipality = Report::get_municipality_by_brgy_id($brgy_assembly->psgc_id) }}</td>
				            </tr>
				            <tr>
				                <th>Province</th>
				                <td>{{ $province = Report::get_province_by_brgy_id($brgy_assembly->psgc_id) }}</td>
				            </tr>
				            <tr>
				                <th>Region</th>
				                <td>{{ $region = Report::get_region_by_brgy_id($brgy_assembly->psgc_id) }}</td>
				            </tr>
				        </table>
				    </div>

				    <div class="col-md-6">  
					<table class="table table-striped table-bordered table-hover">
				            <tr>
				                <th>Start - End Date</th>
				                <td>
				                	<div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control pull-left" value="{{ Report::get_date_conducted($brgy_assembly->activity_id) }} - {{ Report::get_end_date($brgy_assembly->activity_id) }}" placeholder="mm/dd/yyyy - mm/dd/yyyy"id="daterange" required/>
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div><!-- /.input group -->
                                    </div>

				                	{{ Form::input('text', 'startdate'	, Report::get_date_conducted($brgy_assembly->activity_id), array('class' => 'form-control hidden', 'max' => date("m/d/Y"))) }}
				                	{{ Form::input('text', 'enddate', Report::get_end_date($brgy_assembly->activity_id), array('class' => 'hidden  form-control', 'max' => date("m/d/Y"))) }}
								</td>
				            </tr>
				             <tr>
				                <th>Cycle</th>
				                <td>{{ $brgy_assembly->cycle_id }}</td>
				            </tr>
				              <tr>
				                <th>Program</th>
				                <td>{{ $brgy_assembly->program_id }}</td>
				            </tr>
				            <tr>
				                <th>Purpose</th>
				                <td>{{ $brgy_assembly->purpose }}</td>
				            </tr>
						</table>
					</div>
					</div>
	            	<div class="clearfix"></div>
	            </div>
	        </div>
	 	</div>
    	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Attendees</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					<table class="table table-striped table-bordered table-hover">
						<tr>
							<td></td>
							<td>Represented</td>
							<td>Total  in the Brgy</td>
							<td>Participation Rate</td>
						</tr>
						<tr>
			                <th>Household</th>
			                <td>
			                	{{ Form::input('number', 'no_household', $brgy_assembly->no_household, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
			                <td>
			                	{{ Form::input('number', 'total_household', $brgy_assembly->total_household, array('class' => 'form-control','required' ,'min'=>'0')) }}
			                </td>
			                 <td>
			                	{{ Form::input('text', 'percentage_household', '', array('class' => 'form-control', 'readonly')) }}
			                </td>
			            </tr>
			            
					</table>
					<br>
					<table class="table table-striped table-bordered table-hover">
			            <tr>
			            	<th>Attendees </th>
			                <th>Male</th>
			                <th>Female</th>
			               	<th>Total</th>
			            </tr>
			            <tr>
			            	<th>No. of Person </th>
			                <td>
			                	{{ Form::input('number', 'no_atnmale', $brgy_assembly->no_atnmale, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'no_atnfemale', $brgy_assembly->no_atnfemale, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
							<td>
								{{ Form::text('',0,['class'=>'form-control total_person',''])}}
							</td>
			            </tr>
			            <tr>
			                <th>No. of IP</th>
			                <td>
			                	{{ Form::input('number', 'no_ipmale', $brgy_assembly->no_ipmale, array('class' => 'form-control', 'min'=>'0',   'max' => $brgy_assembly->no_atnmale==0?'999999999' : $brgy_assembly->no_atnmale, 'required')) }}
                                 					        </td>
			                <td>
			                	{{ Form::input('number', 'no_ipfemale', $brgy_assembly->no_ipfemale, array('class' => 'form-control', 'min'=>'0', 'max' => $brgy_assembly->no_atnfemale==0?'999999999' : $brgy_assembly->no_atnfemale,'required')) }}
			                </td>
			                <td>
								{{ Form::text('',0,['class'=>'form-control total_ip',''])}}
			                </td>
			            </tr>
			                <th>No. of 60 yrs +</th>
			                <td>
			                	{{ Form::input('number', 'no_oldmale', $brgy_assembly->no_oldmale, array('class' => 'form-control', 'min'=>'0', 'max' => $brgy_assembly->no_atnmale==0 ? '999999999' : $brgy_assembly->no_atnmale , 'required')) }}
					       	</td>
			                <td>
			                	{{ Form::input('number', 'no_oldfemale', $brgy_assembly->no_oldfemale, array('class' => 'form-control', 'min'=>'0', 'max' => $brgy_assembly->no_atnfemale==0 ? '9999999999' : $brgy_assembly->no_atnfemale ,'required')) }}
					        </td>
					        <td>
								{{ Form::text('',0,['class'=>'form-control total_old',''])}}
					        	
					        </td>

			            </tr>
			           
					</table>
			 	</div>
			 </div>
		 </div>
		 <div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			<div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					<table class="table table-striped table-bordered table-hover">
			            <tr>
			            	<th>Participations </th>
			                <th>Households</th>
			                <th>Families</th>
			            </tr>
			            <tr>
			            	<th>Pantawid Pamilya</th>
			                <td>
			                	{{ Form::input('number', 'no_pphousehold', $brgy_assembly->no_pphousehold, array('class' => 'form-control attendees', 'min'=>'0', 'required')) }}
					        </td>
			                <td>
			                	{{-- This line has validation for 4PS HH and families --}}
			                	{{-- Form::input('number', 'no_ppfamilies', $brgy_assembly->no_ppfamilies, array('class' => 'form-control family', 'min' => $brgy_assembly->no_pphousehold==0 ? '0' : $brgy_assembly->no_pphousehold, 'required'))  --}}
			                	{{-- END --}}
			                	
			                	{{ Form::input('number', 'no_ppfamilies', $brgy_assembly->no_ppfamilies, array('class' => 'form-control family' , 'required')) }}
					        </td>
			            </tr>
			            <tr>
			                <th>SLP beneficiaries</th>
			                <td>
			                	{{ Form::input('number', 'no_slphousehold', $brgy_assembly->no_slphousehold, array('class' => 'form-control attendees', 'min'=>'0', 'required')) }}
					        </td>
			                <td>
			                	{{ Form::input('number', 'no_slpfamilies', $brgy_assembly->no_slpfamilies, array('class' => 'form-control family', 'min' => $brgy_assembly->no_slphousehold==0 ? '0' : $brgy_assembly->no_slphousehold, 'required')) }}
					        </td>
			            </tr>
			                <th>IPs beneficiaries</th>
			                <td>
			                	{{ Form::input('number', 'no_iphousehold', $brgy_assembly->no_iphousehold, array('class' => 'form-control attendees', 'min'=>'0', 'required')) }}
					        </td>
			                <td>
			                {{ Form::input('number', 'no_ipfamilies', $brgy_assembly->no_ipfamilies, array('class' => 'form-control family','min' =>   $brgy_assembly->no_iphousehold ==0 ? '0' : $brgy_assembly->no_iphousehold, 'required')) }}
					        </td>
			            </tr>
			            
					</table>
			 	</div>
			 </div>
		 </div>

		<!-- RE -->
		<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Purok/Sitios Represented</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
				            <div id="items">
					            @foreach($sitio_data as $sitio)
					            	<div class="input-group">
					            		<input type="text" class="form-control" placeholder="Name of sitio eg. STO SITIO" value="{{$sitio}}" name="sitio[]" oninput="checkduplicate(this)" required>
					            		 <span class="input-group-addon delete">
						                	 <span class="fa fa-trash-o  ">
						                      </span>
						                </span>
					            	</div><br>
					            @endforeach
				            </div>
				            <div class="input-group">
				            		<input type="text" class="form-control" placeholder="Name of sitio eg. STO SITIO" name="sitio[]" oninput="checkduplicate(this)">
				            		 <span class="input-group-addon delete">
					                	 <span class="fa fa-trash-o  ">
					                      </span>
					                </span>
				            </div><br>
				            <br>
				            <button id="add" class="btn btn-info form-control" type="button" >
				            	<i class="fa fa-plus"></i>
				            	Add Sitio</button>
						</table>
					</div>
			 	</div>
			</div>
		</div>

		<!-- other sect -->

		<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Other Sectors Represented</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
			 		<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							@foreach($sector_list as $sector)
								@if(in_array($sector, $sector_data))
									<div class = "form-control"><input type="checkbox" name="sector[]" value="{{$sector}}" checked>&nbsp{{$sector}}</div>
								@else
									<div class = "form-control"><input type="checkbox" name="sector[]" value="{{$sector}}">&nbsp{{$sector}}</div>
								@endif
							@endforeach
						</table>
					</div>
			 	</div>
			</div>
		</div>
		<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Highlights</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
			 			{{ Form::textarea( 'highlights', $brgy_assembly->highlights, array('class' => 'form-control')) }}

			 				<div class="form-group pull-right">
								<label>Save as draft</label>
							  <input type="checkbox" name="is_draft" {{ $brgy_assembly->is_draft==1 ? 'checked': '' }}>
							</div>
							<!-- <button class="btn btn-primary">
									<i class="fa fa-save"></i>
									Submit 
								</button>
								 <a href="{{ URL::to('bassembly') }}" class="btn bg-navy">close</a> -->
								 <div class="col-md-12 ">
									<div class="form-group pull-right">
						                <button class="btn btn-primary" id="save">
											<i class="fa fa-save"></i>
											Submit 
										</button>
		                 				<a class="btn btn-default" href="{{ URL::to('bassembly') }}">Cancel</a>		
									</div>
								</div>
								<div class="clearfix"></div>

							{{ Form::close() }}
			 	</div>
			</div>
		</div>
		<!-- highlights -->
	</div>
	<div class="clearfix"></div>

		<script>
		    //helper function for custom validity
(function (exports) {
                            function valOrFunction(val, ctx, args) {
                                if (typeof val == "function") {
                                    return val.apply(ctx, args);
                                } else {
                                    return val;
                                }
                            }

                            function InvalidInputHelper(input, options) {
                                input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

                                function changeOrInput() {
                                    if (input.value == "") {
                                        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                                    } else {
                                        input.setCustomValidity("");
                                    }
                                }

                                function invalid() {
                                    if (input.value == "") {
                                        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                                    } else {
                                       console.log("INVALID!"); input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
                                    }
                                }

                                input.addEventListener("change", changeOrInput);
                                input.addEventListener("input", changeOrInput);
                                input.addEventListener("invalid", invalid);
                                changeOrInput();
                            }
                            exports.InvalidInputHelper = InvalidInputHelper;
                        })(window);
			$(document).ready(function(){


                $('.attendees').each(function(i,e){
                    InvalidInputHelper( e, {
                                      defaultText: "Please check your input",

                                      emptyText: "Please check your input",

                                      invalidText: function (input) {
                                        return 'Value must be less than or equal to the Represented Household in the Barangay (' + input.max + ')';
                                      }
                                    });
                });
                // $('.family').each(function(i,e){
                //                          InvalidInputHelper(e , {
                //                                   defaultText: "Please enter an email address!",

                //                                   emptyText: "Please enter an email address!",

                //                                   invalidText: function (input) {
                //                                     console.log(input);
                //                                     return 'Value must be greater than or equal  (' + input.min + ')';
                //                                   }
                //                                 });
                // });

				$('#daterange').daterangepicker(
				  {
				    format: 'MM/DD/YYYY',
				    startDate : moment().format('MM/DD/YYYY'),
				    endDate : moment().format('MM/DD/YYYY')
				  },
				  function(start, end, label) {
				   	   	$('input[name="startdate"]').val(start.format('MM/DD/YYYY'));
				  		$('input[name="enddate"]').val(end.format('MM/DD/YYYY'));
				  }
				);
				//when the Add Filed button is clicked
				$("#add").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items").append('<div class="input-group"><input type="text" class="form-control" placeholder="Name of sitio eg. STO SITIO" name="sitio[]" oninput="checkduplicate(this)"><span class="input-group-addon"> <span class="fa fa-trash-o  delete"></span></span></div><br>');
					});

				$("body").on("click", ".delete", function (e) {
					$(this).parent().remove();
				});


				$("input[name='startdate']").change(function (e) {
					$("input[name='enddate']").attr('min', $("input[name='startdate']").val());
				});

				$("input[name='enddate']").change(function (e) {
					$("input[name='startdate']").attr('max', $("input[name='enddate']").val());
				});

				$("input[name='total_household']").change(function (e) {
					$("input[name='no_household']").attr('max', $("input[name='total_household']").val());
				});
                // $("input[name='total_families']").change(function (e) {
				// 	$("input[name='no_families']").attr('max', $("input[name='total_household']").val());
				// });

				$("input[name='no_atnmale']").change(function (e) {

					var no_household = parseInt($("input[name='no_household']").val());
                    var male   = $("input[name='no_atnfemale']").val();
                    var female = $("input[name='no_atnmale']").val();
                    var total = parseInt(male) + parseInt(female);

                    if(no_household<total){
                        // alert('Total number of Person must be greater than or equal to Household represented in the barangay');
                    }
					$("input[name='no_ipmale']").attr('max', $("input[name='no_atnmale']").val());
					$("input[name='no_oldmale']").attr('max', $("input[name='no_atnmale']").val());

				});

				$("input[name='no_atnfemale']").change(function (e) {

					var no_household = parseInt($("input[name='no_household']").val());
                    var male   = $("input[name='no_atnfemale']").val();
                    var female = $("input[name='no_atnmale']").val();
                    var total = parseInt(male) + parseInt(female);
                    if(no_household<total){
                         // alert('Total number of Person must be greater than or equal to Household represented in the barangay');
                    }
					$("input[name='no_ipfemale']").attr('max', $("input[name='no_atnfemale']").val());
					$("input[name='no_oldfemale']").attr('max', $("input[name='no_atnfemale']").val());
				});

				$("input[name='no_household']").keyup(function (e) {
					$("input[name='no_pphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='no_slphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='no_iphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");
				});

				

				$("input[name='total_household']").change(function (e) {


					$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");
				});
//				comment from last docs min household mustFamilies must be greater than or equal to Households
				$("input[name='no_pphousehold']").change(function(e){
					$("input[name='no_ppfamilies']").attr('min', $("input[name='no_pphousehold']").val());
				});
				$("input[name='no_slphousehold']").change(function(e){
                	$("input[name='no_slpfamilies']").attr('min', $("input[name='no_slphousehold']").val());
                });
				$("input[name='no_iphousehold']").change(function(e){
                    $("input[name='no_ipfamilies']").attr('min', $("input[name='no_iphousehold']").val());
                });
//				end of comment

				$("input[name='total_families']").change(function (e) {
				 // $("input[name='no_ppfamilies']").attr('max', $("input[name='total_families']").val());
     //                            					$("input[name='no_slpfamilies']").attr('max', $("input[name='total_families']").val());
     //                            					$("input[name='no_ipfamilies']").attr('max', $("input[name='total_families']").val());
					// // $("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");
				});

               	$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");

                $("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");

			});

			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName('sitio[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}


			/* get the total in attendees */
			var total = (function($){
				return {
					totalPerson : function(){
					// validation for total male and female over represented houehold
						var total_ = parseInt($("input[name='no_atnfemale']").val()) + parseInt($("input[name='no_atnmale']").val()) ;
						$('.total_person').val(total_ || '0');
						
						if( total_ < $("input[name='no_household']").val() ){
							$('.total_person')[0].setCustomValidity('Must be greater than or equal to number of Households represented');
						}else{
							$('.total_person')[0].setCustomValidity('');
						}
					},
					totalIP : function(){
						var total_ = parseInt($("input[name='no_ipfemale']").val()) + parseInt($("input[name='no_ipmale']").val()) ;
						$('.total_ip').val(total_ || '0');
						if( total_ > $("input[name='no_household']").val() ){
							$('.total_ip')[0].setCustomValidity('Must be less than or equal to number of Households');
						}else{
							$('.total_ip')[0].setCustomValidity('');
						}
					},
					totalOld : function(){
						var total_ = parseInt($("input[name='no_oldfemale']").val()) + parseInt($("input[name='no_oldmale']").val()) ;
						$('.total_old').val(total_ || '0');
					
						if( total_ > $("input[name='no_household']").val() ){
							$('.total_ip')[0].setCustomValidity('Must be less than or equal to number of Households');
						}else{
							$('.total_ip')[0].setCustomValidity('');
						}
					}


				}
			})(jQuery);
                total.totalPerson();
                total.totalIP();
                total.totalOld();

			$("input[name='no_atnmale']").keyup(total.totalPerson);
			$("input[name='no_ipmale']").keyup(total.totalIP);
			$("input[name='no_oldmale']").keyup(total.totalOld);
			$("input[name='no_atnfemale']").keyup(total.totalPerson);
			$("input[name='no_ipfemale']").keyup(total.totalIP);
			$("input[name='no_oldfemale']").keyup(total.totalOld);
			 $("input[name='no_household']").keyup(total.totalPerson);
			


		</script>
	
@stop