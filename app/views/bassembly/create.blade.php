@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				<!-- if there are creation errors, they will show here -->
		
		<div class="col-md-12">
		 	<div class="box box-primary">
	            <div class="box-body">
					{{ Form::open(array('url' => 'bassembly','novalidate')) }}
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Barangay <i class="text-red">*</i></label>
							{{ Form::select('barangay', $brgy_list, Input::old('barangay'), array('class' => 'form-control', 'required','id'=>'barangay')) }}
						</div>
						
						<div class="form-group">
							{{ Form::label('psgc_id', 'PSGC Code') }}
							{{ Form::text('psgc_id', Input::old('psgc_id'), array('class' => 'form-control', 'required', 'readonly')) }}
						</div>
						
						<div class="form-group hidden">
							{{ Form::label('kc_code', 'KC-NCDDP Code') }}
							{{ Form::text('kc_code', Input::old('kc_code'), array('class' => 'form-control')) }}
						</div>
						
						<div class="form-group">
							{{ Form::label('municipality', 'Municipality') }}
							{{ Form::text('municipality', $municipality, array('class' => 'form-control', 'readonly')) }}
						</div>
						
						<div class="form-group">
							{{ Form::label('province', 'Province') }}
							{{ Form::text('province', $province, array('class' => 'form-control', 'readonly')) }}
						</div>
						<div class="form-group">
							<label for="">Cycle <i class="text-red">*</i></label>
							{{ Form::select('cycle_id', [''=>'Select Cycle']+$cycle_list, Input::old('cycle_id'), array('class' => 'form-control', 'required')) }}
						</div>
						
					
						<div class="form-group">
							{{ Form::label('program_id', 'Program') }}
							{{ Form::select('program_id', [''=>'Select Program']+$program_list, Input::old('program'), array('class' => 'form-control','required')) }}
						</div>

						<div class="form-group">
							<label for="">Purpose <i class="text-red">*</i></label>
							{{ Form::select('purpose', $purpose_list, Input::old('purpose'), array('class' => 'form-control', 'required')) }}
						</div>

						
						{{ Form::submit('Add Barangay Assembly', array('class' => 'btn btn-primary')) }}
					
					 <!-- clear floats -->
					 <a class="btn bg-navy" href="{{ URL::to('bassembly')}}">Close</a>
					{{ Form::close() }}
				</div>
					 <div class="clearfix"></div>
			</div>
		</div>
		
		<script>
			$(document).ready(function(){
				$("#barangay").change(function (e) {
					$('.hidden').hide()
					$("#psgc_id").attr('value', $("#barangay").val());
					var selector = "option[value='" +$("#barangay").val()+ "']";
					$(selector).attr('selected', 'selected');
					
				});	

				$('form').submit(function(){
					var warning = "";
					var null_exist = false;
					$('form .form-control').each(function(i,e){
					   if(e.required) {
					      if($(this).val() == ""){

					         warning += $(this).siblings('label').text() + " is required \n";
					      } 
					   }
					});
					
					if(warning==""){
					}else{
						alert(warning);
						return null_exist;
					}			
						
				});
			});
		</script>
@stop