@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif

		{{ HTML::linkRoute('bassembly.index', 'Back to Barangay Assembly','', array('class' => 'btn btn-default btn')) }}
	    
	     <div class="pull-right">
		     @if($brgy_assembly->is_draft==1)
	        		 	<div class="alert alert-info">This is still a draft  <a href="{{ URL::to('bassembly/' .$brgy_assembly->activity_id.'/edit') }}">edit the document</a> to set it in final draft</div>
	         @endif
	    	@if( !is_review($brgy_assembly->activity_id) )
				{{ Form::open(array('url' => 'bassembly/' . $brgy_assembly->activity_id)) }}
				{{ Form::hidden('_method', 'DELETE') }}
				<button class="btn btn-danger" type="submit" value="Delete">
					<i class="fa fa-trash-o"></i> DELETE
				</button>
				<!-- Check if review or not  -->
				    
			
				 <a class="btn  btn-small btn-info" href="{{ URL::to('bassembly/' .$brgy_assembly->activity_id.'/edit') }}">
				 	<i class="fa fa-edit"></i>	Edit
				 </a>
			@endif
			 <span class="btn bg-olive" data-toggle="modal" data-target="#pincos_grievances">
				PINCOS and Grievances
			</span>
	     </div>

		<br>
		<br>
	 
		 {{ Form::close() }}

	    <!-- /. ROW  -->


        {{ viewComment($brgy_assembly->activity_id) }}
        <div class="box box-primary">
    		<div class="box-header">
    			<h3 class="box-title">Details</h3>
    			<div class="box-tools pull-right">
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                </div>
    		</div>
			<div class="box-body">
					<div class="table-responsive">
						 <div class="col-md-6">  
						<table class="table table-striped table-bordered table-hover">
						 	<tr>
				                <th>Barangay</th>
				                <td>{{ $barangay = Report::get_barangay($brgy_assembly->psgc_id) }}</td>
				            </tr>
				        
				            <tr>
				                <th>Municipality</th>
				                <td>{{ $municipality = Report::get_municipality_by_brgy_id($brgy_assembly->psgc_id) }}</td>
				            </tr>
				            <tr>
				                <th>Province</th>
				                <td>{{ $province = Report::get_province_by_brgy_id($brgy_assembly->psgc_id) }}</td>
				            </tr>
				            <tr>
				                <th>Region</th>
				                <td>{{ $region = Report::get_region_by_brgy_id($brgy_assembly->psgc_id) }}</td>
				            </tr>
				            <tr>
				                <th>Start Date</th>
				                <td>{{ $startdate = Report::get_date_conducted($brgy_assembly->activity_id) }}</td>
				            </tr>
				            <tr>
				                <th>End Date</th>
				                <td>{{ $enddate = Report::get_end_date($brgy_assembly->activity_id) }}</td>
				            </tr>
				        </table>
				    </div>
				    <div class="col-md-6">  
						<table class="table table-striped table-bordered table-hover">

				             <tr>
				                <th>Cycle</th>
				                <td>{{ $brgy_assembly->cycle_id }}</td>
				            </tr>
				              <tr>
				                <th>Program</th>
				                <td>{{ $brgy_assembly->program_id }}</td>
				            </tr>
				            <tr>
				                <th>Purpose</th>
				                <td>{{ $brgy_assembly->purpose }}</td>
				            </tr>
						</table>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="box box-primary">
    		<div class="box-header">
    			<h3 class="box-title">Attendance Details</h3>
    			<div class="box-tools pull-right">
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                </div>
    		</div>
    		<div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						<tr>
							<td></td>
							<td>Represented</td>
							<td>Total in the Brgy</td>
							<td>Participation Rate</td>
						</tr>
						<tr>
			                <th>Household</th>
			                <td>{{ $hh_brgy = $brgy_assembly->no_household}}</td>
			          		<td>{{ $brgy_assembly->total_household }}</td>
							<td> {{ round((intval($brgy_assembly->no_household )/intval($brgy_assembly->total_household==0? 1 : $brgy_assembly->total_household))*100) }}% </td>
			            </tr>
			     
					</table>
				</div>
			</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
			            <tr>
			            	<th>Attendees </th>
			                <th>Male</th>
			                <th>Female</th>
			                <th>Total</th>
			            </tr>
			            <tr>
			            	<th>No. of Person </th>
			                <td>{{ $brgy_assembly->no_atnmale}}</td>
			                <td>{{ $brgy_assembly->no_atnfemale}}</td>
			                <td>{{  intval($brgy_assembly->no_atnfemale) + intval($brgy_assembly->no_atnmale) }}</td>
			            </tr>
			            <tr>
			                <th>No. of IP</th>
			                <td>{{ $brgy_assembly->no_ipmale}}</td>
			                <td>{{ $brgy_assembly->no_ipfemale}}</td>
			                <td>{{  intval($brgy_assembly->no_ipmale) + intval($brgy_assembly->no_ipfemale) }}</td>
			            </tr>
			            <tr>
			                <th>No. of 60 yrs +</th>
			                <td>{{ $brgy_assembly->no_oldmale}}</td>
			                <td>{{ $brgy_assembly->no_oldfemale}}</td>
			                <td>{{  intval($brgy_assembly->no_oldmale) + intval($brgy_assembly->no_oldfemale) }}</td>
			            </tr>
			           
					</table>
				</div>
			</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
			            <tr>
			            	<th>Households / families participation </th>
			                <th>Households</th>
			                <th>Families</th>
			            </tr>
			            <tr>
			            	<th>Pantawid Pamilya beneficiaries</th>
			                <td>{{ $brgy_assembly->no_pphousehold}}</td>
			                <td>{{ $brgy_assembly->no_ppfamilies}}</td>
			            </tr>
			            <tr>
			                <th>SLP beneficiaries</th>
			                <td>{{ $brgy_assembly->no_slphousehold}}</td>
			                <td>{{ $brgy_assembly->no_slpfamilies}}</td>
			            </tr>
			                <th>IPs beneficiaries</th>
			                <td>{{ $brgy_assembly->no_iphousehold}}</td>
			                <td>{{ $brgy_assembly->no_ipfamilies}}</td>
			            </tr>
			            
					</table>
				</div>
			</div>
		</div>

		<div class="box box-primary">
    		<div class="box-header">
    			
    			<h3 class="box-title">Purok/Sitios Represented</h3>
    			<div class="box-tools pull-right">
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                </div>
    		</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
			            @foreach($sitio_data as $sitio)
			            <tr>
			                <td>{{$sitio}}</td>
			            </tr>
			            @endforeach
					</table>
				</div>
			</div>
		</div>

		<div class="box box-primary">
    		<div class="box-header">
    			
    			<h3 class="box-title">Other Sectors Represented</h3>
    			<div class="box-tools pull-right">
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                </div>
    		</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						@foreach($sector_data as $sector)
			            <tr>
			                <td>{{$sector}}</td>
			            </tr>
			            @endforeach
					</table>
				</div>
			</div>
		</div>

		<div class="box box-primary">
    		<div class="box-header">
    			<h3 class="box-title">Highlights</h3>
    			<div class="box-tools pull-right" >
                    <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                </div>
    		</div>
			<div class="box-body" style=" word-wrap: break-word;">
				{{$brgy_assembly->highlights}}
			</div>
		</div>
		<!-- pincols and grivances modal -->
		@include('modals.pincos_grievances')
	
	<script>
			$(document).ready(function(){
			//when the Add Filed button is clicked
				$("#add").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items").append('<div class="input-group"><input type="text" class="form-control" name="sitio[]" oninput="checkduplicate(this)" placeholder="sitio"><div class="input-group-addon"><button type="button" class="delete">-</button></div></div>');
				});
				
				$("#add2").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items2").append($("#template").html());
				});

				$("body").on("click", ".delete", function (e) {
					$(this).parent("div").parent("div").remove();
				});
				
				
				$("input[name='startdate']").change(function (e) {
					$("input[name='enddate']").attr('min', $("input[name='startdate']").val());
				});
				
				$("input[name='enddate']").change(function (e) {
					$("input[name='startdate']").attr('max', $("input[name='enddate']").val());
				});
				
				
				$("input[name='no_atnmale']").change(function (e) {
					$("input[name='no_ipmale']").attr('max', $("input[name='no_atnmale']").val());
					$("input[name='no_oldmale']").attr('max', $("input[name='no_atnmale']").val());
				});
				
				$("input[name='no_atnfemale']").change(function (e) {
					$("input[name='no_ipfemale']").attr('max', $("input[name='no_atnfemale']").val());
					$("input[name='no_oldfemale']").attr('max', $("input[name='no_atnfemale']").val());
				});
			
			
			});
			
			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName('sitio[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate1(e){
				var count = -1;
				var elements = document.getElementsByName('document_name[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate2(e){
				var count = -1;
				var elements = document.getElementsByName('rank[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}
			
			function checkduplicate3(e){
				var count = -1;
				var elements = document.getElementsByName('problem[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count == 1){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}


			// added script 

			$('.draft').on('click',function(){
				var con = confirm('Are you sure that you want to set it to final draft');

				if(con){
					$.get('{{ $brgy_assembly->activity_id }}/set',function(){
						window.location.href = '{{ URL::to('bassembly') }}';
					});
				}

			});
			$(document).ready(function(){
                        $('.date').datetimepicker({pick12HourFormat: false, pickTime:false});
             });


		</script>
															
															
		
	
@stop