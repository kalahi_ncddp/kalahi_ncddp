@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop
@section('content')
	@if ($errors->all())
		<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
	@endif

	<div class="panel panel-default">
		<div class="panel-heading">
			{{ $title }}
			 @if($position->position=='ACT')
			    <span class="btn btn-default pull-right" id="approved">Review</span>
			    <span class="hidden module">bassembly</span>
            @endif
			<a class="btn  btn-primary pull-right" style="
    margin: 0 10px" href="{{ URL::to('bassembly/create')  }}"><i class="fa fa-plus"></i> Add New</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
						    @if($position->position=="ACT")
						    <th>Select all
						        <input type="checkbox" id="selecctall"/>
						    </th>
						    @endif
							<th>Region</th>
							<th>Province</th>
							<th>Municipality</th>
							<th>Cycle</th>
							
							<th>Barangay</th>
							<th>Date Conducted</th>
							<th>Purpose</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						
						<?php foreach ($data as $brgy_assembly) : ?> 

						<?php endforeach; ?>
						
						@foreach($data as $brgy_assembly)
					
						<tr>
                           @if($position->position=="ACT")
						    <td>

                               @if($brgy_assembly->is_draft==0)
                                 {{ $reference->approval_status($brgy_assembly->activity_id , 'ACT') }}
                               @else
                                   <span class="label label-warning">draft</span>
                               @endif
                            </td>
                            @endif
							<td>
								{{ $region = Report::get_region_by_brgy_id($brgy_assembly->psgc_id) }}
							</td>
							<td>
								{{ $province = Report::get_province_by_brgy_id($brgy_assembly->psgc_id) }}
							</td>
							<td>
								{{ $municipality = Report::get_municipality_by_brgy_id($brgy_assembly->psgc_id) }}
							</td>
							<td>
								{{ $brgy_assembly->cycle_id }}
							</td>
							<td>
								{{ $barangay = Report::get_barangay($brgy_assembly->psgc_id) }}
							</td>
							<td>
								 {{ Report::get_date_conducted($brgy_assembly->activity_id)  }}
							</td>
							<td>
								{{ $brgy_assembly->purpose }}
							</td>
							<td>
								@if($brgy_assembly->is_draft==0)
		                            <a class="btn btn-success btn" href="{{ URL::to('bassembly/' .$brgy_assembly->activity_id) }}">
		                   <i class="fa fa-eye"></i>           View Details{{ hasComment($brgy_assembly->activity_id) }}
		                            </a>
		                        @else
		                        	<a class="btn btn-warning btn" href="{{ URL::to('bassembly/' .$brgy_assembly->activity_id) }}">
		                        <i class="fa fa-eye"></i>      View Details( draft )
		                            </a>
								@endif
	                    
	                        </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable(
        {
            "aoColumnDefs": [
                      { 'bSortable': false, 'aTargets': [ 0 ] }
                   ]
        }
        )



      });
    </script>
@stop