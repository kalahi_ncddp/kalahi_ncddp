@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
			<div class="col-md-12">
				<h4>Add Installation GRS barangay</h4>
			</div>
		</div>
	  
	    <div class="row">
		    <div class="col-md-12">
				
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
				{{ Form::open(array('url' => 'grs-barangay/create')) }}

				<div class="col-md-4 left-border">

				
					<!-- barangay -->
					<div class="form-group">
						{{ Form::label('Barangay', 'Barangay') }}
						{{ Form::select('psgc_id', [''=>'Select Barangay']+$barangay_lists, Input::old('psgc_id'), array('class' => 'form-control barangay', 'required')) }}
					</div>
					
					<div class="form-group">
						{{ Form::label('Municipality', 'Municipality') }}
						{{ Form::text('municipality', $municipality, array('class' => 'form-control municipal','placeholder' => '','','readonly')) }}		
					</div>

					<div class="form-group">
							{{ Form::label('Province','Province') }}
						{{ Form::text('province', $province, array('class' => 'form-control province','placeholder' => '','','readonly')) }}
					</div>

					<div class="form-group">
						{{ Form::label('Region','Region') }}
						{{ Form::text('region', $region, array('class' => 'form-control region','readonly')) }}
					</div>

					<div class="form-group hidden">
						{{ Form::label('KC Class','KC Class') }}
						{{ Form::select('kc_class', $kc_class, '', array('class' => 'form-control kc_class', '')) }}
						
					</div>

					<div class="form-group">
						{{ Form::label('Cycle','Cycle') }}
						{{ Form::select('cycle_id', [''=>'Select Cycle']+$cycles, Input::old('cycle_id'), array('class' => 'form-control cycle', 'required')) }}
					</div>

					<div class="form-group">
						{{ Form::label('Program','Program') }}
						{{ Form::select('program_id',[''=>'Select Program']+$programs, Input::old('program_id'), array('class' => 'form-control program')) }}
					</div>

				</div>


				<div class="col-md-4 left-border">

					<div class="form-group">
						{{ Form::label('GRS Orientation conducted','GRS Orientation conducted') }}
						<div class='input-group ' id='datetimepicker10'>
			                {{ Form::text('date_infodess', '', array('class' => 'form-control idc date','required')) }}
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>

					<div class="form-group">
						{{ Form::label('GRS Volunteers identified','GRS Volunteers identified') }}
						<div class='input-group '>
							{{ Form::text('date_voliden', '', array('class' => 'form-control date','')) }}
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>

					<div class="form-group hidden">
						{{ Form::label('Fact-finding Committee organized','Fact-finding Committee organized') }}
						<div class='input-group '>
							{{ Form::text('date_ffcomm', '', array('class' => 'form-control date','')) }}
							<span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>

					<div class="form-group">
						{{ Form::label('Training of community members on handling grievances conducted','Training of community members on handling grievances conducted') }}
						<div class='input-group '>
							{{ Form::text('date_training', '', array('class' => 'form-control date','')) }}
							<span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>

					<div class="form-group">
						{{ Form::label('Information materials availability with Grievance Hotline','Information materials availability with Grievance Hotline') }}
						<div class='input-group '>
							{{ Form::text('date_inspect', '', array('class' => 'form-control date ','')) }}
							<span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>


					<div class="form-group hidden">
						{{ Form::label(' Manual at the Bgy. Hall','Manual at the Bgy. Hall') }}
						{{ Form::input('number', 'no_manuals', '', array('class' => 'form-control')) }}
					</div>

					<div class="form-group hidden">
						{{ Form::label('No. of brochures/pamplet','') }}
						{{ Form::input('number','no_brochures', '', array('class' => 'form-control ')) }}
					</div>
			
					<div class="form-group hidden">
						{{ Form::label('No. of Tarp available','') }}
						{{ Form::input('number','no_tarpauline', '', array('class' => 'form-control ')) }}
					</div>
				</div>


				<div class="col-md-4 left-border">
					



					<div class="form-group hidden">
						{{ Form::label('No. of posters available','') }}
						{{ Form::input('number','no_posters', '', array('class' => 'form-control')) }}
					</div>

					<div class="form-group hidden">
						{{ Form::label('Means of Reporting Grievances available','') }}
						{{ Form::text('date_meansrept', '', array('class' => 'form-control date')) }}
					</div>


					<div class="form-group">
						{{ Form::label('Grievance/Suggestion box installed at Barangay ') }}
						{{ Form::checkbox('is_boxinstalled', null,'', array('class' => '')) }}
					</div>

					<div class="form-group">
						{{ Form::label('Phone Number') }}
						{{ Form::number('phone_no', '', array('class' => 'form-control')) }}
					</div>


					<div class="form-group">
						{{ Form::label('Office Address') }}
						{{ Form::text('address', '', array('class' => 'form-control')) }}
					</div>
					
					<div class="form-group">
						{{ Form::label('Remarks (for information materials availability)') }}
						{{ Form::textarea('remarks', '', array('class' => 'form-control')) }}
					</div>
				

						{{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
						<a href="{{ URL::to('grs-barangay') }}" class="btn bg-navy">close</a>
				</div>
			
				

				{{ Form::close() }}
			</div>
		</div>
		<!-- add js module -->
		<script>
		        $('input[name=date_infodess]').change(function(){
        	        $('input[name=date_voliden]').data("DateTimePicker").setMinDate(new Date($('input[name=date_infodess]').val()));
        	    });
        	    $('input[name=date_voliden]').change(function(){
        	        $('input[name=date_training]').data("DateTimePicker").setMinDate(new Date($('input[name=date_voliden]').val() ));
        	    });
        	     $('input[name=date_ffcomm]').change(function(){
        	        $('input[name=date_training]').data("DateTimePicker").setMinDate(new Date($('input[name=date_ffcomm]').val() ));
        	     });
		</script>
@stop