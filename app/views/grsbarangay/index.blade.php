@extends('layouts.default')

@section('username')
	
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	
	    <div class="row">
		    <div class="col-md-12">
				<!-- Advanced Tables -->
				
				
		    	<div class="panel panel-default">
		    		<div class="panel-heading">
		    		{{ $title }}
		    			@if($position->position=='ACT')
                            <span class="btn btn-default pull-right" id="approved">Review</span>
                            <span class="hidden module">grs-barangay</span>
                        @endif

		    			<a class="btn  btn-primary pull-right" href="{{ URL::to('grs-barangay/create') }}">
						<i class="fa fa-plus"></i> Add New
						</a>
						<div class="clearfix"></div>
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
									    @if($position->position=='ACT')
                                        <th>Select all
                                            <input type="checkbox" id="selecctall"/>
                                        </th>
                                        @endif
										<th>Cycle ID</th>
										<th>Region Abbrev</th>
										<th>Province</th>
										<th>Municipality</th>
										<th>Barangay</th>
										<th>Program</th>
										<th>Details</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($grsbarangays as  $grsbarangay) 
										<tr>
										     @if($position->position=='ACT')
										    <td>
                                               @if($grsbarangay->is_draft==0)
                                                 {{ $reference->approval_status($grsbarangay->reference_no , 'ACT') }}
                                               @else
                                                   <span class="label label-warning">draft</span>
                                               @endif
										    </td>
                                             @endif
											<td>
												{{ $grsbarangay->cycle_id }}
											</td>
											<td>
												{{ $grsbarangay->barangay->province['region']['region_abbr'] }}
											</td>
											<td>
												{{ $grsbarangay->barangay->province['province'] }}
											</td>
											<td>
												{{ $grsbarangay->barangay->municipality['municipality'] }}
											</td>
											<td>
												{{ $grsbarangay->barangay->barangay }}
											</td>
											<td>
												{{ isset($grsbarangay->program_id) ? $grsbarangay->program_id: 'not yet available'  }}
											</td>

											<td>
												@if($grsbarangay->is_draft==0)
                                                    <a class="btn btn-success" href="{{ URL::to('grs-barangay/'.$grsbarangay->reference_no) }}"><i class="fa fa-eye"></i> &nbsp; View details</a>
						                             {{--<button class="btn btn-success btn view" data-psgc="{{ $grsbarangay->psgc_id }}" data-cycle="{{ $grsbarangay->cycle_id }}">--}}
						                           {{--<i class="fa fa-eye"></i>   View Details {{ hasComment($grsbarangay->reference_no) }}--}}
						                            {{--</button>--}}
						                        @else
                                                    <a class="btn btn-warning" href="{{ URL::to('grs-barangay/'.$grsbarangay->reference_no) }}"><i class="fa fa-eye"></i> &nbsp; View details</a>

						                        	{{--<button class="btn btn-warning btn view" data-psgc="{{ $grsbarangay->psgc_id }}" data-cycle="{{ $grsbarangay->cycle_id }}">--}}
						                            {{--<i class="fa fa-eye"></i>   View Details( draft )--}}
						                            {{--</button>--}}
												@endif
					                    
					                        </td>	

										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- lets add show modal ref to show url -->
		 <div class="modal fade" id="show-barangay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body">
                    	
                    		<ul class="barangay_list">
                    			<li>
                    				Information dissemination conducted <b class="date_infodess"></b>
                    			</li>
                    			<li>
                    				GRS Volunteers identified <b class="date_voliden"></b>
                    			</li>
                    <!--			<li>
                    				Fact-finding Committee organized <b class="date_ffcomm"></b>
                    			</li> -->
                    			<li>
                    				Training of community members on handling grievances conducted <b class="date_training"></b>
                    			</li>
                    			<li>
                    				Information materials availability with Grievance Hotline <b class="date_inspect"></b>
                    			</li>

                    			<li>
                    				Grievance/Suggestion box installed at Barangay <b class="is_boxinstalled"></b>
                    			</li>
                    			<li>
 									Phone Number <b class="phone_no"></b>
                    			</li>
                    			<li>
                    				Office Address <b class="address"></b>
                    			</li>
                    			<li>
                    				Remarks <b class="remarks"></b>
                    			</li>
                    		</ul>
                    	
					</div>
					<div class="modal-footer">
						
						<a href="" class="btn btn-info edit-url"><i class="fa fa-edit"></i>Edit</a>
						<a href=""class="btn btn-danger delete-barangay" >Delete</a>
					
					</div>
	            </div>
            </div>
        </div>
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop