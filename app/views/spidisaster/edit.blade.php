@extends('layouts.default')

@section('content')
<div class="row">
    <div class="col-md-12">
		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h4>Municipality</small></h4>
    		</div>
			<div class="panel-body">
			    <div class="table-responsive col-xs-12 col-md-12">
			        <table class="table table-bordered table-striped">
				        <tbody>
					        <tr>
					          <th width="40%">Calamity Type</th>
					          <td>
					          <!-- 	<select name="in_calamity_type" required="required" class="form-control">
					          		<option value="Typhoon" <?php if($disaster->calamity_type == 'Typhoon') echo 'selected'?> >
					          			Typhoon
					          		</option>
					          		<option value="Earthquake" <?php if($disaster->calamity_type == 'Earthquake') echo 'selected'?> >Earthquake</option>
					          		<option value="Tsunami" <?php if($disaster->calamity_type == 'Tsunami') echo 'selected'?> >Tsunami</option>
					          		<option value="Manmade" <?php if($disaster->calamity_type == 'Mandmade') echo 'selected'?> >Man-made</option>
					          		<option value="Landslide" <?php if($disaster->calamity_type == 'Landslide') echo 'selected'?> >Landslide</option>
					          	</select> -->
					          		<input type="text" id="in-in_calamity_type" name="in-in_calamity_type" class="form-control" 
					          		required="required" value="{{{$disaster->calamity_type}}}">
					          </td>
					        </tr>
					        <tr>
					          <th>Description of Calamity</th>
					          <td>
					          <!-- 	<textarea rows="3" cols="30" id="in_calamity_desc" name="in_calamity_desc" 
					          		class="form-control" style="resize: none;">
					          		{{$disaster->description}}
					          	</textarea> -->

					          	<textarea class="form-control" rows="5" id="in_calamity_desc" name="in_calamity_desc">{{$disaster->description}}</textarea>
					          </td>
					        </tr>
					    	<tr>
					          <th>Date Occured</th>
					          <td>
								<div id="in-dateoccured_cont" class="input-group date">
				                	<input type="text" value="{{toDate($disaster->date_occured)}}" name="in-dateoccured" id="in-dateoccured"
				                	 required="required" class="form-control"> <br>
				                	<span class="input-group-addon"><span class="fa fa-calendar glyphicon glyphicon-calendar"></span></span>
					            </div>
					          </td>
					        </tr>
					        <tr>
					          <th>Province</th>
					          <td>
					          	<input type="text" id="in-province" name="in-province" class="form-control" value="{{{$province}}}" 
					          		readonly="readonly">
					          </td>
					        </tr>
					        <tr>
					          <th>Municipality</th>
					          <td>
					          	<input type="text" id="in-municipalityname" name="in-municipalityname" class="form-control" 
					          		value="{{{$municipality}}}" readonly="readonly">
					          </td>
					        </tr>
				      	</tbody>
			        </table>
			    </div>
			</div>
		</div>
		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h5 class="col-xs-6 col-md-6" style="padding-left: 0;">SubProjects</h5>
    			<a class="btn btn-success pull-right" href="{{ URL::to('spi-disaster/'.$disaster->projdisaster_id.'/export')}}">Export to XLSX</a>
				&nbsp;<a id="spi-disaster_modal-add_show" href="" class="btn btn-primary pull-right">
					<i class="fa fa-plus"></i> Add SubProject
				</a>
				<div class="clearfix"></div>
    		</div>
			<div class="panel-body">
				@if ($store_status !== null)
					@if ($store_status === TRUE)
						<div class="alert alert-success" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-ok"></i> Subproject was successfully added!</strong>
						</div>
					@elseif ($store_status === FALSE)
						<div class="alert alert-danger" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-remove"></i> Subproject can not be added</strong> 
							Please contact DSWD about this incident.
						</div>
					@endif
				@endif
				@if ($update_status !== null)
					@if ($update_status === TRUE)
						<div class="alert alert-success" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-ok"></i> Subproject was successfully updated!</strong>
						</div>
					@elseif ($update_status === FALSE)
						<div class="alert alert-danger" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-remove"></i> Subproject can not be updated</strong> 
							Please contact DSWD about this incident.
						</div>
					@endif
				@endif
				@if ($destroy_status !== null)
					@if ($destroy_status === TRUE)
						<div class="alert alert-success" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-ok"></i> Subproject was successfully removed!</strong>
						</div>
					@elseif ($destroy_status === FALSE)
						<div class="alert alert-danger" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-remove"></i> Subproject can not be removed</strong> 
							Please contact DSWD about this incident.
						</div>
					@endif
				@endif	
				<table class="table table-striped table-bordered dataTable" id="dataTables-example" aria-describedby="dataTables-example_info">
					<thead>
						<tr>
							<th style="width: 10%;">Barangay</th>
							<th style="width: 10%;">Sub-project Name</th>
							<th style="width: 10%;">Completion Date</th>
							<th style="width: 10%;">Fund Source</th>
							<th style="width: 10%;">Cycle</th>

							<th style="width: 9%;">Total SP Cost</th>
							<th style="width: 9%;">Functionality of SP</th>
							<th style="width: 9%;">Estimated Damages</th>
							<th style="width: 9%;">Estimated Cost of Rehab / Reconstruction</th>
							<th style="width: 13%; text-align: center;">Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($disaster->items as $item)
							<tr data-rowid="{{$item->projdisaster_item_id}}">
								<td class="row-barangay" data-value="{{ isset($item->barangay->barangay) ? $item->barangay->barangay_psgc : '' }}">{{ isset($item->barangay->barangay) ? $item->barangay->barangay: '' }}</td>
								<td class="row-project_name" data-value="{{ isset($item->project_name) ? $item->project_name : '' }}">{{ isset($item->project_name) ? $item->project_name : '' }}</td>
								<td class="row-date_completed" data-value="{{ toDate($item->date_completed)}}">{{ toDate($item->date_completed)}}</td>
								<td class="row-fund_source" data-value="{{$item->fund_source}}">{{$item->fund_source}}</td>
								<td class="row-cycle" data-value="{{$item->cycle_id}}">{{$item->cycle_id}}</td>

								<td class="row-total_sp" data-value="{{$item->total_sp}}">{{$item->total_sp}}</td>
								<td class="row-functionality_sp" data-value="{{$item->functionality_sp}}">{{$item->functionality_sp}}</td>
								<td class="row-est_damages" data-value="{{$item->est_damages}}">{{$item->est_damages}}</td>
								<td class="row-est_cost" data-value="{{$item->est_cost}}">{{$item->est_cost}}</td>
								<td style="text-align: center;">
									<a href="spi-disaster/{{ $item->projdisaster_id }}" class="btn btn-xs btn-info spi-disaster_modal-edit-update_show" style="margin-right: 0.5rem;"><i class="glyphicon glyphicon-edit"> </i>Edit</a>
									<a href="spi-disaster/{{ $item->projdisaster_id }}" class="btn btn-xs btn-danger spi-disaster_modal-del_show"><i class="glyphicon glyphicon-trash"></i> Delete</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>		
{{--ADDING MODAL--}}
<div class="modal fade" id="spi-disaster_modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Damage Sub-Project</h4>
            </div>
            <div class="modal-body">
				{{ Form::open(array('id' => 'spi-disaster_modal_editform', 'route' => 'spi-disaster.update')) }}
					<input type="hidden" id="method" name="method" value="add">
					<div class="form-group col-xs-6 col-md-6 hidden">
						<label for="in-cycleid">Cycle</label>
						<input type="text" id="in-cycleid" name="in-cycleid" class="form-control"
							value="{{{$cycle}}}" readonly="readonly">
					</div>
					<div class="form-group col-xs-6 col-md-6 hidden">
						<label for="in-programid">Program</label>
						<input type="text" id="in-programid" name="in-programid" class="form-control" 
							value="{{{$program}}}" readonly="readonly">
					</div>
					<div class="form-group col-xs-6 col-md-6 hidden">
						<label for="in-province">Province</label>
						<input type="text" id="in-province" name="in-province" class="form-control" 
							value="{{{$province}}}" readonly="readonly">
					</div>
					<div class="form-group col-xs-6 col-md-6 hidden">
						<label for="in-municipalityname">Municipality</label>
						<input type="text" id="in-municipalityname" name="in-municipalityname" class="form-control"
							value="{{{$municipality}}}" readonly="readonly">
						<input type="hidden" id="in-municipalitypsgc" name="in-municipalitypsgc" value="{{{$municipality_psgc}}}">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-barangay">Barangay</label>
						<select name="in-barangay" id="in-barangay" class="form-control">
							<option value="">-- Select a Barangay --</option>
							@foreach($barangays as $id => $name)
							<option value="{{$id}}">{{$name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-subproject">Sub Project</label>
						<input type="text" name="in-subproject" id="in-subproject" class="form-control"/>
							
						
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-province">Fund Source</label>
						<input type="text" id="in-projprogram" name="in-projprogram" class="form-control" 
							value="">
					</div>

					<div class="form-group col-xs-6 col-md-6">
						<label for="in-province">Cycle</label>
						<input type="text" id="in-cycle" name="in-cycle" class="form-control" 
							value="">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-datecompltd">Date Completed</label>
						<input type="text" id="in-datecompltd" name="in-datecompltd" class="form-control date"
							value="">
					</div>
				    <div class="form-group col-xs-6 col-md-6">
                        <label for="in-datecompltd">Total SP Cost</label>
                        {{--<input type="text" id="in-total_sp" name="in-total_sp" class="form-control "--}}
                            {{--value="">--}}
                        {{ Form::currency("in-total_sp",'',['class'=>'form-control','id'=>'in-total_sp']) }}

                    </div>
                    <div class="form-group col-xs-6 col-md-6">
                        <label for="in-datecompltd">Functionality</label>
                       <!-- <input type="text" id="in-functionality" name="in-functionality" class="form-control "
                            value=""> -->
                            <select name="in-functionality_sp" id="in-functionality_sp" class="form-control">
									<option value="">-- Select Functionality --</option>
									@foreach($functionality_sp as $function)
									<option value="{{$function['functionality']}}">{{$function['functionality']}}</option>
									@endforeach
								</select>
                    </div>
				    <div class="form-group col-xs-6 col-md-6">
						<label for="in-estdamages">Estimated Damages</label>
						{{--<input type="text" id="in-estdamages" name="in-estdamages" class="form-control" value="">--}}
                        {{ Form::currency("in-estdamages",'',['class'=>'form-control','id'=>'in-estdamages']) }}

				    </div>
				    <div class="form-group col-xs-6 col-md-6">
						<label for="in-estcost">Estimated Cost</label>
						{{--<input type="text" id="in-estcost" name="in-estcost" class="form-control" value="">--}}
						{{ Form::currency("in-estcost",'',['class'=>'form-control','id'=>'in-estcost']) }}
				    </div>
				    <div class="clearfix"></div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<a id="spi-disaster_modal_editform_submit" href="" class="btn btn-primary">
					<i class="glyphicon glyphicon-save"></i> Submit
				</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
			</div>
        </div>
    </div>
</div>
{{--Confirm Modal--}}
<div class="modal fade" id="spi-disaster_modal-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
				<form id="spi-disaster_modal_edit-delform">
					<input type="hidden" id="del-id" name="del-id" value="">
					<input type="hidden" id="method" name="method" value="delete">
					<p class="col-xs-2" style="text-align: right;">
						<i class="glyphicon glyphicon-question-sign" style="color: #f4543c; font-size: 30px;"></i>
					</p>
					<p class="col-xs-10" style="font-size: 15px; padding-top: 5px; padding-bottom: 5px;">Are you sure you want to remove 
						 this project to this Disaster?</p>
					<div class="clearfix"></div>
				</form>
			</div>
			<div class="modal-footer">
				<a id="spi-disaster_modal_edit-delform_submit" href="" class="btn btn-danger">Yes</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">No</a>
			</div>
        </div>
    </div>
</div>
{{-- EDIT MODAL--}}
<div class="modal fade" id="spi-disaster_modal-edit-update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Sub-project for Calamity</h4>
            </div>
            <div class="modal-body">
				<form id="spi-disaster_modal_edit-updateform">
					<input type="hidden" id="edit-id" name="edit-id" value="">
					<input type="hidden" id="method" name="method" value="update">
					<div class="form-group col-xs-6 col-md-6">
						<label for="edit-cycleid">Cycle</label>
						<input type="text" id="edit-cycleid" name="edit-cycleid" class="form-control"
							value="{{{$cycle}}}" readonly="readonly">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="edit-programid">Program</label>
						<input type="text" id="edit-programid" name="edit-programid" class="form-control" 
							value="{{{$program}}}" readonly="readonly">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="edit-province">Province</label>
						<input type="text" id="edit-province" name="edit-province" class="form-control" 
							value="{{{$province}}}" readonly="readonly">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="edit-municipalityname">Municipality</label>
						<input type="text" id="edit-municipalityname" name="edit-municipalityname" class="form-control"
							value="{{{$municipality}}}" readonly="readonly">
						<input type="hidden" id="edit-municipalitypsgc" name="edit-municipalitypsgc" value="{{{$municipality_psgc}}}">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="edit-barangay">Barangay</label>
						<select name="edit-barangay" id="edit-barangay" class="form-control">
							<option value="">-- Select a Barangay --</option>
							@foreach($barangays as $id => $name)
							<option value="{{$id}}">{{$name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="edit-subproject">Sub Project</label>
						<input type="text" name="in-subproject" id="edit-subproject" class="form-control"/>
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="edit-province">Fund Source</label>
						<input type="text" id="edit-fund_source" name="edit-projprogram" class="form-control"
							value="">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-province">Cycle</label>
						<input type="text" id="in-cycle" name="in-cycle" class="form-control" 
							value="">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="edit-datecompltd">Date Completed</label>
						<input type="text" id="edit-datecompltd" name="edit-datecompltd" class="form-control" 
							value="">
					</div>
					 <div class="form-group col-xs-6 col-md-6">
                        <label for="in-datecompltd">Total SP Cost</label>
                        <input type="text" id="edit-total_sp" name="edit-total_sp" class="form-control "
                            value="">
                    </div>
                    <div class="form-group col-xs-6 col-md-6">
                        <label for="in-datecompltd">Functionality</label>
                        <input type="text" id="edit-functionality_sp" name="edit-functionality_sp" class="form-control "
                            value="">
                    </div>
				    <div class="form-group col-xs-6 col-md-6">
						<label for="edit-estdamages">Estimated Damages</label>
						<input type="text" id="edit-estdamages" name="edit-estdamages" class="form-control" value="">
				    </div>
				    <div class="form-group col-xs-6 col-md-6">
						<label for="edit-estcost">Estimated Cost</label>
						<input type="text" id="edit-estcost" name="edit-estcost" class="form-control" value="">
				    </div>
				    <div class="clearfix"></div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<a id="spi-disaster_modal_edit-updateform_submit" href="" class="btn btn-primary">
					<i class="glyphicon glyphicon-floppy-disk"></i> Submit
				</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
			</div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
	    $('#dataTables-example').dataTable();

    	setTimeout(function(){
    		$('div.alert').slideUp();
    	}, 5000);

		$('#in-estdamages, #in-estcost, #edit-estcost, #edit-estdamages').keydown(function(event) {
				if ( event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
				}
				else {
					if (event.keyCode < 48 || event.keyCode > 57 ) {
						event.preventDefault();	
					}	
				}
			});

		$('#spi-disaster_modal-add_show').on('click', function(event)
		{
			console.log('asdasd');

			$('#spi-disaster_modal-edit').modal('show');
			return false;
		});


		$('#in-subproject').change(function(event)
		{
			var project_id = $(event.currentTarget).val();
			console.log(project_id);
			if(project_id != '')
			{
				var program     = $('#in-subproject option[value="' + project_id + '"]').data('program');	
				var datecompltd = $('#in-subproject option[value="' + project_id + '"]').data('datecompltd');	
				$('#in-projprogram').val(program);
				$('#in-datecompltd').val(datecompltd);
			}
			else
			{
				$('#in-projprogram').val('');
				$('#in-datecompltd').val('');
			}
			return false;
		});

		/*
		 *
		 * ADDING a SubProject to Disaster
		 *
		 *
		//  */
		$('#spi-disaster_modal_editform_submit').on('click', function(event)
		{
			var $url = window.location.href;
			console.log($url);
			$('.modal').loading(true);
			
			$.ajax({
		   		type: "PUT",
		   		url:  $url,
		   		data: $( "#spi-disaster_modal_editform" ).serialize()
			})
		   	.done(function( msg ) {
		   		console.log(msg);
		     	$('#spi-techassist_modal-edit').modal('hide');
		     	window.location = window.location.href;
				// $('.modal').loading(false);

		   	});

			return false;
		});


		/*
		 * DELETING a SubProject to Disaster
		 */
	 	$('.spi-disaster_modal-del_show').on('click', function(event){
			var id = $(event.currentTarget).parents('tr').data('rowid');
			$('#del-id').val(id);
			$('#spi-disaster_modal-del').modal('show');
			return false;
      	});

		$('#spi-disaster_modal_edit-delform_submit').on('click', function(event)
		{
			

			var $url = window.location.href;
			console.log($url);
			
			$.ajax({
		  		type: "PUT",
		  		url:  $url,
		  		data: $( "#spi-disaster_modal_edit-delform" ).serialize()
			})
		  	.done(function( msg ) {
		    	$('#spi-disaster_modal-del').modal('hide');
		    	window.location.href = window.location.href;
		  	});

			return false;
		});


		/*
		 * UPDATING a SubProject to Disaster
		 */

		$('#edit-subproject').change(function(event)
		{
			var project_id = $(event.currentTarget).val();
			console.log(project_id);
			if(project_id != '')
			{
				var program     = $('#edit-subproject option[value="' + project_id + '"]').data('program');	
				var datecompltd = $('#edit-subproject option[value="' + project_id + '"]').data('datecompltd');	
				$('#edit-projprogram').val(program);
				$('#edit-datecompltd').val(datecompltd);
			}
			else
			{
				$('#edit-projprogram').val('');
				$('#edit-datecompltd').val('');
			}
			return false;
		});


		/* filled data when editing or updating when you click the edit from disaster */
	 	$('.spi-disaster_modal-edit-update_show').on('click', function(event){
			var $current_row   = $(event.currentTarget).parents('tr');
			var id             = $current_row.data('rowid');
			$('#edit-id').val(id);
			var barangay_psgc  = $current_row.find('.row-barangay').data('value');
			$('#edit-barangay').val(barangay_psgc);
			var project_id = $current_row.find('.row-project_name').data('value');
			$('#edit-subproject').val(project_id);
			var est_damages    = $current_row.find('.row-est_damages').data('value');
			$('#edit-estdamages').val(est_damages);
			var est_cost       = $current_row.find('.row-est_cost').data('value');
			$('#edit-estcost').val(est_cost);
			var cycle      = $current_row.find('.row-cycle').data('value');
			$('#edit-cycle').val(cycle);

			var total_sp       = $current_row.find('.row-total_sp').data('value');
            $('#edit-total_sp').val(total_sp);

            var functionality_sp       = $current_row.find('.row-functionality_sp').data('value');
            $('#edit-functionality_sp').val(functionality_sp);
            var fund_source = $current_row.find('.row-fund_source').data('value');
			$('#edit-fund_source').val(project_id);

			console.log(barangay_psgc);

			$('#spi-disaster_modal-edit-update').modal('show');
			return false;
      	});

		$('#spi-disaster_modal_edit-updateform_submit').on('click', function(event)
		{
			$('.modal').loading(true);
			var $url = window.location.href;
			console.log('hello');
			$.ajax({
		  		type: "PUT",
		  		url:  $url,
		  		data: $( "#spi-disaster_modal_edit-updateform" ).serialize()
			})
		  	.done(function( msg ) {
		    	$('#spi-disaster_modal-del').modal('hide');
//		    	window.location = window.location.href;
		    	$('.modal').loading(false);

		  	});

			return false;
		});
	});
</script>
@stop