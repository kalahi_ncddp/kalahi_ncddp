@extends('layouts.default')

@section('content')
    <div class="row">
	    <div class="col-md-12">
			<!-- Advanced Tables -->
			
	    	<div class="panel panel-default">
	    		<div class="panel-heading">
	    			<h4 class="col-xs-10 col-md-10">  {{ $title }}</h4>
	    			@if($position->position=='ACT')
                    <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                    <span class="hidden module">spi-disaster</span>
             @endif
					<a id="spi-disaster_modal-add_show" href="" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i>Add Disaster</a>
			        <div class="clearfix"></div>
	    		</div>

				<div class="panel-body">
						@if ($store_status !== null)
							@if ($store_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> A New Disaster was successfully added!</strong>
								</div>
							@elseif ($store_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Disaster can not be added</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if ($update_status !== null)
							@if ($update_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> Technical Assistance was successfully updated!</strong>
								</div>
							@elseif ($update_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Technical Assistance can not be updated</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if ($destroy_status !== null)
							@if ($destroy_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> Disaster was successfully removed!</strong>
								</div>
							@elseif ($destroy_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> Disaster can not be removed</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
					<div class="table-responsive">
						<div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
							<table class="table table-striped table-bordered table-hover dataTable" 
									id="dataTables-example" aria-describedby="dataTables-example_info">
							<thead>
								<tr>
									 <th>
		                            @if($position->position=='ACT')
		                            <input type="checkbox" id="selecctall"/>
		                            @endif
		                            </th>
									<th style="width: 15%;">Calamity Type</th>
									<th style="width: 45%;">Description of Calamity</th>
									<th style="width: 10%;">Date Occured</th>
									<th style="width: 10%;">No. of Subprojects</th>
									<th style="width: 15%; text-align: center;">Action</th>
							</thead>
							
							<tbody>
								@foreach ($disasters as $disaster ) 
								<tr data-rowid="{{{$disaster->projdisaster_id}}}">
									 <td>
		                               @if($position->position=='ACT')
		                                  @if($disaster->is_draft==0)
		                                    {{ $reference->approval_status($disaster->projdisaster_id, 'ACT') }}
		                                  @else
		                                      <span class="label label-warning">draft</span>
		                                  @endif
		                                @endif
		                            </td>

									<td class="cell-calamity_type">
										{{{$disaster->calamity_type}}}
									</td>
									<td class="cell-description">
										{{{$disaster->description}}}
									</td>
									<td class="cell-date_occured">
										{{{toDate($disaster->date_occured)}}}
									</td>
									<td>
									{{ $disaster->items->count()}}
									</td>
									<td style="text-align: center">
										
	    								@if( !is_review($disaster->projdisaster_id) )

										<a href="spi-disaster/{{ $disaster->projdisaster_id }}" class="btn btn-sm btn-info spi-techassist_modal-edit_show" style="margin-right: 0.5rem;"><i class="glyphicon glyphicon-edit"></i> Edit</a>
										<a href="" class="btn btn-sm btn-danger spi-disaster_modal-del_show"><i class="glyphicon glyphicon-trash"></i> 	Delete</a>
										@endif
									</td>
								</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="spi-disaster_modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">New Disaster</h4>
                </div>
                <div class="modal-body">
					{{ Form::open(array('id' => 'spi-disaster_modal_form', 'route' => 'spi-disaster.store')) }}
						<div class="form-group col-xs-12 col-md-12">
							<label for="in-cycleid">Calamity Type</label>
				          	<!-- <select name="in_calamity_type" required="required" class="form-control">
				          		<option value="">-- Select Disaster --</option>
				          		<option value="Typhoon">Typhoon</option>
				          		<option value="Earthquake">Earthquake</option>
				          		<option value="Tsunami">Tsunami</option>
				          		<option value="Manmade">Man-made</option>
				          		<option value="Landslide">Landslide</option>
				          	</select> -->
				          	<input type="text" id="in_calamity_type" name="in_calamity_type" class="form-control"
					            placeholder="Ex: Typhoon"		required="required">
						</div>
					    <div class="form-group col-xs-12 col-md-12">
							<label for="in_dateoccured_cont">Date Occurred</label>
							<div id="in-dateoccured_cont" class="input-group date">
			                	<input type="text" value="12/18/2014" name="in-dateoccured" id="in-dateoccured"
			                	 required="required" class="form-control"> <br>
			                	<span class="input-group-addon"><span class="fa fa-calendar glyphicon glyphicon-calendar"></span></span>
				            </div>
					    </div>
						<div class="form-group col-xs-6 col-md-6">
							<label for="in-province">Province</label>
							<input type="text" id="in-province" name="in-province" class="form-control" 
								value="{{{$province}}}" readonly="readonly">
						</div>
						<div class="form-group col-xs-6 col-md-6">
							<label for="in-municipalityname">Municipality</label>
							<input type="text" id="in-municipalityname" name="in-municipalityname" class="form-control"
								value="{{{$municipality}}}" readonly="readonly">
							<input type="hidden" id="in-municipalitypsgc" name="in-municipalitypsgc" value="{{{$municipality_psgc}}}">
						</div>
					    <div class="form-group col-xs-12 col-md-12">
							<label for="in_calamity_desc">Description</label>
							<!-- <input type="text" style="height: 100px" class="form-control"
									  id="in_calamity_desc" name="in_calamity_desc"> -->
						 <textarea class="form-control" rows="5" id="in_calamity_desc" name="in_calamity_desc"></textarea>
							
					    </div>
					    <div class="clearfix"></div>
					{{ Form::close() }}
				</div>
				<div class="modal-footer">
					<a id="spi-disaster_modal_form_submit" href="" class="btn btn-primary">
						<i class="glyphicon glyphicon-floppy-disk"></i> Submit
					</a>
					<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
				</div>
            </div>
        </div>
    </div>


	<div class="modal fade" id="spi-disaster_modal-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
					<form>
						<input type="hidden" id="del-id" name="del-id" value="">
						<p class="col-xs-2" style="text-align: right;">
							<i class="glyphicon glyphicon-question-sign" style="color: #f4543c; font-size: 30px;"></i>
						</p>
						<p class="col-xs-10" style="font-size: 15px; padding-top: 5px; padding-bottom: 5px;">Are you sure you want to delete 
							 this Disaster?</p>
						<div class="clearfix"></div>
					</form>
				</div>
				<div class="modal-footer">
					<a id="spi-disaster_modal_delform_submit" href="" class="btn btn-danger">Yes</a>
					<a href="" class="btn bg-navy" data-dismiss="modal">No</a>
				</div>
            </div>
        </div>
    </div>

	<script>
      	$(document).ready(function () {
        	$('#dataTables-example').dataTable();

		    /*
		     *  ADDING a Municipal Sustainability Plan
		     */
			$('#spi-disaster_modal-add_show').on('click', function(event)
			{
				$('#spi-disaster_modal-add').modal('show');
				return false;
			});


			$('#spi-disaster_modal_form_submit').on('click', function()
			{   $('.modal').loading(true);
				$.post( window.location.href, $( "#spi-disaster_modal_form" ).serialize())
				.done(function(msg){
					$('#spi-disaster_modal-add').modal('hide');
					window.location = window.location.href;
				});

				return false;
			});

			/*
	         *  DELETING a committee
	         */
			$('.spi-disaster_modal-del_show').on('click', function(event){
				var id = $(event.currentTarget).parents('tr').data('rowid');
				$('#del-id').val(id);
				$('#spi-disaster_modal-del').modal('show');
				return false;
	      	});

			$('#spi-disaster_modal_delform_submit').on('click', function(event)
			{
				var $url = window.location.href + '/' + $('#del-id').val();
				console.log($url);
                $('.modal').loading(true);
				$.ajax({
			  		type: "DELETE",
			  		url: $url
				})
			  	.done(function( msg ) {
			    	$('#spi-disaster_modal-del').modal('hide');
			    	window.location = window.location.href;
			  	});

				return false;
			});
			/* end of DELETING a committee */
      	});
    </script>
@stop