@extends('layouts.default')

@section('content')
<div class="row">
    <div class="col-md-12">
		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h4>Municipality</small></h4>
    		</div>
			<div class="panel-body">
			    <div class="table-responsive col-xs-12 col-md-12">
			        <table class="table table-bordered table-striped">
				        <tbody>
					        <tr>
					          <th width="40%">Calamity Types</th>
					          <td>
					          	<!-- <select name="in_calamity_type" required="required" class="form-control">
					          		<option value="Typhoon">Typhoon</option>
					          		<option value="Earthquake">Earthquake</option>
					          		<option value="Tsunami">Tsunami</option>
					          		<option value="Landslide">Landslide</option>
					          		<option value="Manmade">Man-made</option>
					          	</select> -->
					          	<input type="text" id="in-in_calamity_type" name="in-in_calamity_type" class="form-control" 
					          		required="required">

					          </td>
					        </tr>
					        <tr>
					          <th>Description of Calamity</th>
					          <td>
					          	<textarea class="form-control" rows="5" id="in_calamity_desc" name="in_calamity_desc"></textarea>
					          </td>
					        </tr>
					    	<tr>
					          <th>Date Occured</th>
					          <td>
								<div id="in-dateoccured_cont" class="input-group date">
				                	<input type="text" value="" name="in-dateoccured" id="in-dateoccured"
				                	 required="required" class="form-control"> <br>
				                	<span class="input-group-addon"><span class="fa fa-calendar glyphicon glyphicon-calendar"></span></span>
					            </div>
					          </td>
					        </tr>
					        <tr>
					          <th>Province</th>
					          <td>
					          	<input type="text" id="in-province" name="in-province" class="form-control" value="{{{$province}}}" 
					          		readonly="readonly">
					          </td>
					        </tr>
					        <tr>
					          <th>Municipality</th>
					          <td>
					          	<input type="text" id="in-municipalityname" name="in-municipalityname" class="form-control" 
					          		value="{{{$municipality}}}" readonly="readonly">
					          </td>
					        </tr>
				      	</tbody>
			        </table>
			    </div>
			</div>
		</div>
		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h5 class="col-xs-6 col-md-6" style="padding-left: 0;">SubProjects</h5>
				<a id="spi-disaster_modal-add_show" href="" class="btn btn-primary pull-right">
					<i class="fa fa-plus"></i> Add SubProject
				</a>
				<div class="clearfix"></div>
    		</div>
			<div class="panel-body">
				<table class="table table-striped table-bordered dataTable" id="dataTables-example" aria-describedby="dataTables-example_info">
					<thead>
						<tr>
							<th style="width: 10%;">Barangay</th>
							<th style="width: 15%;">Sub-project Name</th>
							<th style="width: 10%;">Cycle / Phase</th>
							<th style="width: 10%;">Completion Date</th>
							<th style="width: 10%;">Fund Source</th>
							<th style="width: 10%;">Total SP Cost</th>
							<th style="width: 10%;">Functionality</th>
							<th style="width: 10%;">Estimated Damages</th>
							<th style="width: 10%;">Estimated Cost</th>
							<th style="width: 5%; text-align: center;">Action</th>
						</tr>
					</thead>
					
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>		

<div class="modal fade" id="spi-disaster_modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Damaged Sub-Project by Calamity</h4>
            </div>
            <div class="modal-body">
				{{ Form::open(array('id' => 'spi-disaster_modal_form', 'route' => 'spi-disaster.store')) }}
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-cycleid">Cycle</label>
						<input type="text" id="in-cycleid" name="in-cycleid" class="form-control"
							value="{{{$cycle}}}" readonly="readonly">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-programid">Program</label>
						<input type="text" id="in-programid" name="in-programid" class="form-control" 
							value="{{{$program}}}" readonly="readonly">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-province">Province</label>
						<input type="text" id="in-province" name="in-province" class="form-control" 
							value="{{{$province}}}" readonly="readonly">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-municipalityname">Municipality</label>
						<input type="text" id="in-municipalityname" name="in-municipalityname" class="form-control"
							value="{{{$municipality}}}" readonly="readonly">
						<input type="hidden" id="in-municipalitypsgc" name="in-municipalitypsgc" value="{{{$municipality_psgc}}}">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-barangay">Barangay</label>
						<select name="in-barangay" id="in-barangay" class="form-control">
							<option value="">-- Select a Barangay --</option>
							@foreach($barangays as $id => $name)
							<option value="{{$id}}">{{$name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-subproject">Sub Project</label>
						<select name="in-subproject" id="in-subproject" class="form-control">
							<option value="">-- Select a Sub-Project --</option>
							@foreach($all_projects as $project)
							<option value="{{$project->project_id}}" 
								data-program="{{$project->program_id}}"
								data-datecompltd="{{$project->date_completed}}">
								{{$project->project_name}}
							</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-province">Fund Source</label>
						<input type="text" id="in-projprogram" name="in-projprogram" class="form-control" 
							value="" readonly="readonly">
					</div>
					<div class="form-group col-xs-6 col-md-6">
						<label for="in-datecompltd">Date Completed</label>
						<input type="text" id="in-datecompltd" name="in-datecompltd" class="form-control" 
							value="" readonly="readonly">
					</div>
				    <div class="form-group col-xs-6 col-md-6">
						<label for="in-estdamages">Estimated Damages</label>
						<input type="number" id="in-estdamages" placeholder="Specific Damage" name="in-estdamages" class="form-control" value="">
				    </div>
				    <div class="form-group col-xs-6 col-md-6">
						<label for="in-estcost">Estimated Cost</label>
						<input type="number" id="in-estcost" name="in-estcost" class="form-control" value="">
				    </div>
				    <div class="clearfix"></div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<a id="spi-disaster_modal_form_submit" href="" class="btn btn-success">
					<i class="glyphicon glyphicon-floppy-disk"></i> Submit
				</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
			</div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
	    $('#dataTables-example').dataTable();

		$('#spi-disaster_modal-add_show').on('click', function(event)
		{
			console.log('asdasd');
			$('#spi-disaster_modal-add').modal('show');
			return false;
		});

		$('#spi-disaster_modal_form_submit').on('click', function()
		{
			$.post( window.location.href, $( "#spi-disaster_modal_form" ).serialize())
			.done(function(msg){
				$('#spi-disaster_modal-add').modal('hide');
				window.location = window.location.href;
			});

			return false;
		});


		$('#in-subproject').change(function(event)
		{
			var project_id = $(event.currentTarget).val();
			console.log(project_id);
			if(project_id != '')
			{
				var program     = $('#in-subproject option[value="' + project_id + '"]').data('program');	
				var datecompltd = $('#in-subproject option[value="' + project_id + '"]').data('datecompltd');	
				$('#in-projprogram').val(program);
				$('#in-datecompltd').val(datecompltd);
			}
			else
			{
				$('#in-projprogram').val('');
				$('#in-datecompltd').val('');
			}
			return false;
		});

	});
</script>
@stop