@extends('layouts.default')

@section('content')
<div class="row">
    <div class="col-md-12">
		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h4>Municipality</small></h4>
    		</div>
			<div class="panel-body">
			    <div class="table-responsive col-xs-12 col-md-12">
			        <table class="table table-bordered table-striped">
				        <tbody>
					        <tr>
					          <th width="40%">Calamity Type</th>
					          <td></td>
					        </tr>
					        <tr>
					          <th>Description of Calamity</th>
					          <td></td>
					        </tr>
					    	<tr>
					          <th>Date Occured</th>
					          <td></td>
					        </tr>
					        <tr>
					          <th>Province</th>
					          <td></td>
					        </tr>
					        <tr>
					          <th>Municipality</th>
					          <td></td>
					        </tr>
				      	</tbody>
			        </table>
			    </div>
			</div>
		</div>
		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h5 class="col-xs-6 col-md-6" style="padding-left: 0;">Accomplishment per Item</h5>
				<a id="spi-accomplish_modal-add_show" href="" class="btn btn-success pull-right">
					<i class="fa fa-plus"></i> Add Accomplishment Item
				</a>
				<div class="clearfix"></div>
    		</div>
			<div class="panel-body">
				@if ($create_status !== null)
					@if ($create_status === TRUE)
						<div class="alert alert-success" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-ok"></i> New Accomplishment Item successfully added!</strong>
						</div>
					@elseif ($create_status === FALSE)
						<div class="alert alert-danger" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-remove"></i> Accomplishment Item can not be added</strong> 
							Please contact DSWD about this incident.
						</div>
					@endif
				@endif
				@if ($destroy_status !== null)
					@if ($destroy_status === TRUE)
						<div class="alert alert-success" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-ok"></i> Accomplishment Item successfully removed!</strong>
						</div>
					@elseif ($destroy_status === FALSE)
						<div class="alert alert-danger" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-remove"></i> Accomplishment Item can not be removed</strong> 
							Please contact DSWD about this incident.
						</div>
					@endif
				@endif
				<table class="table table-striped table-bordered dataTable" id="dataTables-example" aria-describedby="dataTables-example_info">
					<thead>
						<tr>
							<th style="width: 10%;">Item #</th>
							<th style="width: 15%;">Scope of Work</th>
							<th style="width: 10%;">Quantity</th>
							<th style="width: 10%;">Unit</th>
							<th style="width: 10%;">Unit Cost</th>
							<th style="width: 10%;">Amount</th>
							<th style="width: 10%;">% Weight</th>
							<th style="width: 15%;">% Accomplished</th>
							<th style="width: 10%; text-align: center;">Action</th>
						</tr>
					</thead>
					
					<tbody>
						@foreach($accomplishment_items as $acc_item)
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>		


<div class="modal fade" id="spi-accomplish_modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">New Accomplishment Item</h4>
            </div>
            <div class="modal-body">
				{{ Form::open(array('id' => 'spi-accomplish_modal_form')) }}
					<div class="form-group col-xs-12 col-md-12">
						<label for="in-itemnum"> Item No. </label>
						<select name="in-itemnum" id="in-itemnum" class="form-control">
							<option value="">-- Select Item No. --</option>
							<option value="*"> ** New Item ** </option>
							@foreach($work_items as $item)
							<option id="op{{$item->item_no}}" value="{{$item->item_no}}" 
								data-scope="{{$item->scope_of_work}}" data-unit="{{$item->unit}}">
								 {{$item->item_no}} - {{$item->scope_of_work}}  
							</option>
							@endforeach
						</select>
					</div>

					<div class="form-group  col-xs-12 col-sm-12">
						<label for="in-itemscope">Scope of Work</label>
						<input type="text" class="form-control" id="in-itemscope"  disabled="disabled">
					</div>  

					<div class="form-group  col-xs-6 col-sm-6">
						<label for="in-itemunitcost">Unit Cost</label>
						<input type="text" class="form-control" id="in-itemunitcost">
					</div>  

					<div class="form-group  col-xs-6 col-sm-6">
						<label for="in-itemunit">Unit</label>
						<input type="text" class="form-control" id="in-itemunit" disabled="disabled">
					</div>  

					<div class="form-group  col-xs-12 col-sm-12">
						<label for="in-itemamount">Amount</label>
						<input type="text" class="form-control" id="in-itemamount">
					</div>  

					<div class="form-group  col-xs-12 col-sm-12">
						<label for="in-weight">% Weight</label>
						<input type="text" class="form-control" id="in-weight">
					</div>  
					
				    <div class="clearfix"></div>
				{{ Form::close() }}
			</div>
			<div class="modal-footer">
				<a id="spi-accomplish_modal_form_submit" href="{{ URL::to('spi-accomplishment')}}" class="btn btn-success">
					<i class="glyphicon glyphicon-floppy-disk"></i> Submit
				</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
			</div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
	    $('#dataTables-example').dataTable();

        /*
         *  ADDING a FUNDED PSA PRIORITY
         */
		$('#spi-accomplish_modal-add_show').on('click', function(event)
		{
			console.log('asdasd');
			$('#spi-accomplish_modal-add').modal('show');
			return false;
		});

		$('#in-itemnum').change(function(event)
		{
			var itemnum = $(event.currentTarget).val();
			if (itemnum == '*')
			{
				$('#in-itemscope').removeAttr('disabled').val('');
				$('#in-itemunit').removeAttr('disabled').val('');
			}
			else
			{
				$('#in-itemscope').attr('disabled', 'disabled').val($('#op' + itemnum).data('scope'));
				$('#in-itemunit').attr('disabled', 'disabled').val($('#op' + itemnum).data('unit'));
			}

			return false;
		});

		$('#spi-accomplish_modal_form_submit').click(function(event)
		{
			var itemnum = $('#in-itemnum').val();
			if(itemnum != '')
			{
				$.ajax({
			  		type: "POST",
			  		url:  $(event.currentTarget).attr('href'),
					data: {
						'in-projectid'       : $('#in-projectid').data('value'),
		  				'in-itemnum'         : itemnum,
		  				'in-itemscope'       : $('#in-itemscope').val(),
		  				'in-itemunit'        : $('#in-itemunit').val(),
		  				'in-itemunitcost'    : $('#in-itemunitcost').val(),
					}
				})
			  	.done(function( msg ) {
			  		console.log(msg);
			  		console.log('DONE');
			 	});
			}

			return false;
		});
	});
</script>
@stop