@extends('layouts.default')

@section('content')
	    <div class="row">
		    <div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="col-xs-5" style="padding-left: 0;">  {{ $title }}</h4>
						@if($position->position=='ACT')
                        			    <span class="btn btn-default pull-right" id="approved">Review</span>
                        			    <span class="hidden module">spi-fnddpsa</span>
                                    @endif
						<a id="spi-fnddpsa_modal-add_show" class="btn btn-success pull-right" 
							href="">
							<i class="fa fa-plus"></i> Add New PSA Priority
						</a>
						<div class="clearfix"></div>
					</div>
					<div class="panel-body">

						@if ($store_status !== null)
							@if ($store_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i>New PSA Priority was successfully added!</strong>
								</div>
							@elseif ($store_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> PSA Priority can not be added</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if (Session::get('updated_status') !== null)
							@if (Session::get('updated_status') === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> PSA Priority was successfully updated!</strong>
								</div>
							@elseif ($update_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> PSA Priority can not be updated</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif
						@if ($destroy_status !== null)
							@if ($destroy_status === TRUE)
								<div class="alert alert-success" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-ok"></i> PSA Priority was successfully removed!</strong>
								</div>
							@elseif ($destroy_status === FALSE)
								<div class="alert alert-danger" role="alert" style="margin-left: 0">
									<strong><i class="glyphicon glyphicon-remove"></i> PSA Priority can not be removed</strong> 
									Please contact DSWD about this incident.
								</div>
							@endif
						@endif

						<table class="table table-striped table-bordered" id="spi-fnddpsa_table">
							<thead>
								<tr>
								@if($position->position=="ACT")
                                <th>Select all
                                    <input type="checkbox" id="selecctall"/>
                                </th>
                                @endif
									<th style="width: 20%;">Barangay</th>
									<th style="width: 20%;">Problem</th>
									<th style="width: 20%;">Solution</th>
									<th style="width: 5%;">Category</th>
									<th style="width: 5%;">Sub Project</th>

									<th style="width: 15%;">KC Fund</th>
									<th style="width: 15%;"> Funder</th>
									<th style="width: 15%;"> Fund</th>
									<th style="width: 5%; text-align: center;">Action</th>
								</tr>
							</thead>
							<tbody>

							@foreach ($psaprios as $psaprio)
								<tr data-rowid="{{ $psaprio->id }}">
								    @if($position->position=="ACT")
                                    <td>

                                       @if($psaprio->is_draft==0)
                                         {{ $reference->approval_status($psaprio->id , 'ACT') }}
                                       @else
                                           <span class="label label-warning">draft</span>
                                       @endif
                                    </td>
                                    @endif
									<td class="cell-cycleid" >{{$psaprio->problem()->psa->barangay()->barangay}}</td>
									<td class="cell-problem" data-value="{{ $psaprio->prob_project_id }}">{{ $psaprio->problem()['problem'] }}</td>
									<td class="cell-solution" data-value="{{ $psaprio->kcsoltn_project_id }}">{{ $psaprio->solution()['solution'] }}</td>
									<td class="cell-category" data-value="{{ $psaprio->solution()['solution_cat'] }}">{{ $psaprio->solution()['solution_cat'] }}</td>
									<td class="sub-project">{{ empty($psaprio->kc_ncddp_sub_project_id) ? $psaprio->sub_project: $psaprio->spi->project_name }}</td>
									<td class="kc_fund" data-value="{{ $psaprio->kc_fund }}">{{ $psaprio->kc_fund }}</td>
									<td class="funder" data-value="{{$psaprio->funder  }}">{{ $psaprio->funder }}</td>
									<td class="fund" data-value="{{ $psaprio->fund }}">{{ $psaprio->fund }}</td>
									<td style="text-align: center;min-width:100px">
									    {{ Form::open(array('url' => 'spi-fnddpsa/' . $psaprio->id)) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                            <a href="" class="btn btn-sm btn-warning spi-fnddpsa_modal-edit_show" style="margin-right: 0.5rem;"><i class="glyphicon glyphicon-pencil"></i></a>
                                            <button class="btn btn-sm btn-danger spi-munsusplan_modal-del_show"><i class="glyphicon glyphicon-trash"></i></button>
									    {{ Form::close() }}
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="spi-fnddpsa_modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">New PSA Priority</h4>
                    </div>
                    <div class="modal-body">
						{{ Form::open(array('id' => 'spi-fnddpsa_modal_form', 'route' => 'spi-fnddpsa.store')) }}
    						<div class="form-group col-xs-12 col-md-12">
								<label for="in-problem">Problem</label>
								<select name="in-problem" id="in-problem" class="form-control" required>
									<option value="" selected disabled>-- Select Problem --</option>
									@foreach($problems as $problem)
									<option value="{{$problem->project_id}}" data-psa="{{ $problem->activity_id }}">{{$problem->problem}}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group  col-xs-12 col-sm-12">
								<label for="in-solution">Solution</label>
								<select name="in-solution" id="in-solution" class="form-control" required>

								</select>
							</div>  

							<div class="form-group  col-xs-12 col-sm-12">
								<label for="in-solution-categ">Solution Category</label>
								<input type="text" class="form-control" id="in-solution-categ" name="sol-category" readonly="readonly" disabled="disabled">
							</div>  
                			<div class="form-group  col-xs-12 col-sm-12">
                                <label for="in-solution-categ">KC-NCDDP Sub Project </label>
                                {{ Form::select('kc_ncddp_sub_project_id',[],'',[ 'class'=>'form-control' , 'id'=>'spi','readonly' ]) }}
                            </div>
                            <div class="form-group  col-xs-12 col-sm-12">
                                <label for="in-solution-categ"> Sub Project </label>
                                <input type="text" name="sub_project" class="form-control" placeholder="If not kc-ncddp label the sub project"  >
                            </div>
                            <div class="form-group  col-xs-12 col-sm-12">
                                <label for="in-solution-categ">KC Fund</label>
                                {{ Form::currency('kc_fund','',['class'=>'form-control','id'=>"kc_ncddp"]) }}
                            </div>

                            <div class="form-group  col-xs-12 col-sm-12">
                                <label for="in-solution-categ">Funder</label>
                                <input type="text" name="funder" class="form-control" placeholder="If not kc nddp project"  >
                            </div>
                            <div class="form-group  col-xs-12 col-sm-12">
                                <label for="in-solution-categ">Fund</label>
                                {{ Form::currency('fund','',['class'=>'form-control','id'=>"kc_ncddp","placeholder"=>"Fund from the donor"]) }}
                            </div>
                            <button class="btn btn-success">Submit</button>
						    <div class="clearfix"></div>
						{{ Form::close() }}
					</div>
					<div class="modal-footer">
						{{--<a id="spi-fnddpsa_modal_form_submit" href="" class="btn btn-success">--}}
							{{--<i class="glyphicon glyphicon-floppy-disk"></i> Submit--}}
						{{--</a>--}}
						<a href="" class="btn bg-navy" data-dismiss="modal">Cancel</a>
					</div>
	            </div>
            </div>
        </div>
		@include('modals.edit_funded_psa')
	<script>
      	$(document).ready(function () {
        	$('#spi-fnddpsa_table').dataTable();

	        /*
	         *  ADDING a FUNDED PSA PRIORITY
	         */
			$('#spi-fnddpsa_modal-add_show').on('click', function(event)
			{
				console.log('asdasd');
				$('#spi-fnddpsa_modal-add').modal('show');
				return false;
			});
			$('.spi-fnddpsa_modal-edit_show').on('click', function(event)
			{

			    var item = $(this).parent().parent();
			    console.log(item);

			    var modal = $('#spi-fnddpsa_modal-edit');

				modal.find('.id').val(item.data('rowid'));
				modal.find('#in-problem').val(item.find('.cell-problem').text());
				modal.find('#in-solution').val(item.find('.cell-solution').text());
                modal.find('input[name="sub_project"]').val(item.find('.sub-project').text());
                modal.find('#in-solution-categ').val(item.find('.cell-category').text());
                modal.find('.edit_kc_fund').val(item.find('.kc_fund').text());
                modal.find('input[name="funder"]').val(item.find('.funder').text());
                modal.find('.edit_fund').val(item.find('.fund').text());

				$('#spi-fnddpsa_modal-edit').modal('show');
				return false;
			});





			$("#in-problem").change(function(){
			    var data = {
			        id : $(this).val(),
			        psa : $(this).find('option:selected').data('psa')
			    };
			    $('.modal').loading(true);
                $.get('{{ URL::to('fnddpsa/solutions') }}',data,function(res){
                    console.log(res);
                    $('.modal').loading(false);
                    $('#in-solution option').remove();
                        $('#in-solution').append(
                            '<option selected disabled>Select Solution</option>'
                        );
                    $.each(res,function(i,o){
                        $('#in-solution').append(
                            '<option data-cat="'+o.solution_cat+'" value="'+o.solution_id+'">'+o.solution+'</option>'
                        );
                    })
                })
			});

			$('#in-solution').change(function(){
                var cat = $(this).find(':selected').data('cat');
                $('#in-solution-categ').val(cat);
                var data = {
                    id : $(this).val()
                };
                $('.modal').loading(true);
                $.get('{{ URL::to('fnddpsa/spi') }}',data,function(res){
                                      $('.modal').loading(false);
                                      $('#spi option').remove();
                                      $.each(res,function(i,o){
                                          $('#spi').append(
                                              '<option value="'+o.project_id+'">'+o.project_name+'</option>'
                                          );
                                      })
                                  })
			});
/*

			$('option.ceac_brgy').hide();
			$('option.ceac_muni').show();

			$('#in-barangay').change(function(event)
			{
				var barangay = $(event.currentTarget).val();
				if (barangay != '')
				{
					$('option.ceac_muni').hide();
					$('option.ceac_brgy').show();
				}
				else
				{
					$('option.ceac_brgy').hide();
					$('option.ceac_muni').show();
				}

				return false;
			});
*/
		});
    </script>
@stop