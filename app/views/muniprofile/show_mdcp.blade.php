@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
    <a class="btn btn-default" href="{{ URL::to('muniprofile/'.$id) }}">Go Back</a>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4>Municipal Development Council Profile(Non-MLGU Official)</h4>
			<a class="btn  btn-primary pull-right" href="{{ URL::to('muniprofile/'.$id.'/create_person') }}"><i class="fa fa-plus"></i> Add New</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>Last name</th>
							<th>First name</th>
							<th>Middle initial</th>
							<th>Sex</th>
							<th>Age</th>
							<th>Civil Status</th>
							<th>Sector Represented</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $muni_mdcp)
						<tr>
							<td>
								{{ $muni_mdcp->last_name }}
							</td>
							<td>
								{{ $muni_mdcp->first_name }}
							</td>
							<td>
								{{ $muni_mdcp->middle_name }}
							</td>
							<td>
								{{ $muni_mdcp->sex }}
							</td>
							<td>
								{{ $muni_mdcp->age }}
							</td>
							<td>
								{{ $muni_mdcp->civil_status }}
							</td>
							<td>
								{{ $muni_mdcp->sector_rep }}
							</td>
							<td>
	                            <a class="btn btn-success btn" href="{{ URL::to('muniprofile/' .$muni_mdcp->person_id. '/show_person') }}">
	                       <i class="fa fa-eye"></i>       View Details 
	                            </a>
	                        </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop