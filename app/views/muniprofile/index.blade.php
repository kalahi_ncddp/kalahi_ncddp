@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif

	<div class="panel panel-default">
		<div class="panel-heading">
			Records
			@if($position->position=='ACT')
                    <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                    <span class="hidden module">muniprofile</span>
             @endif
			<a class="btn  btn-primary pull-right" href="{{ URL::to('muniprofile/create') }}"><i class="fa fa-plus"></i> Add New</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
						    <th>
                            @if($position->position=='ACT')
                            <input type="checkbox" id="selecctall"/>
                            @endif
                            </th>
							<th>Region</th>
							<th>Province</th>
							<th>Municipality</th>
							<th>Cycle</th>	
							<th>Program</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $muni_profile)
						<tr>
						    <td>
                               @if($position->position=='ACT')
                                  @if($muni_profile->is_draft==0)
                                    {{ $reference->approval_status($muni_profile->profile_id, 'ACT') }}
                                  @else
                                      <span class="label label-warning">draft</span>
                                  @endif
                                @endif
                            </td>
							<td>
								{{ $region = Report::get_region($muni_profile->municipal_psgc) }}
							</td>
							<td>
								{{ $province = Report::get_province($muni_profile->municipal_psgc) }}
							</td>
							<td>
								{{ $municipality = Report::get_municipality($muni_profile->municipal_psgc) }}
							</td>
							<td>
								{{ $muni_profile->program_id }}
							</td>
							<td>
								{{ $muni_profile->cycle_id }}
							</td>
							
							<td>
								@if($muni_profile->is_draft==0)
		                            <a class="btn btn-success btn" href="{{ URL::to('muniprofile/' .$muni_profile->profile_id) }}">
		                            <i class="fa fa-eye"></i>   View Details {{ hasComment($muni_profile->profile_id) }}
		                            </a>
		                        @else
		                        	<a class="btn btn-warning btn" href="{{ URL::to('muniprofile/' .$muni_profile->profile_id) }}">
		                             <i class="fa fa-eye"></i>  View Details( draft )
		                            </a>
								@endif
	                    
	                        </td>


						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop