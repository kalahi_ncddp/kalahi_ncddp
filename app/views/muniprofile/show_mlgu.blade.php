@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
<a class="btn btn-default" href="{{ URL::to('muniprofile/'.$id) }}">Go Back</a>
	<div class="panel panel-default">
		<div class="panel-heading">
			MLGU Officials Profile
			<a class="btn  btn-primary pull-right" href="{{ URL::to('muniprofile/'.$id.'/create_person_mlgu') }}"><i class="fa fa-plus"></i> Add New</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>Last Name</th>
							<th>First Name</th>
							<th>Middle Initial</th>
							<th>Sex</th>
							<th>Age</th>
							<th>Civil Status</th>
							<th>IP Group</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $muni_mlgu)
						<tr>
							<td>
								{{ $muni_mlgu->last_name }}
							</td>

							<td>
								{{ $muni_mlgu->first_name }}
							</td>

							<td>
								{{ $muni_mlgu->middle_name }}
							</td>
							<td>
								{{ $muni_mlgu->sex }}
							</td>
							<td>
								{{ $muni_mlgu->age }}
							</td>
							<td>
								{{ $muni_mlgu->civil_status }}
							</td>
							<td>
								{{ $muni_mlgu->ip_group }}
							</td>
							<td>
	                            <a class="btn btn-success btn" href="{{ URL::to('muniprofile/' .$muni_mlgu->person_id. '/show_person_mlgu') }}">
	                          <i class="fa fa-eye"></i>    View Details 
	                            </a>
	                        </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop