@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('mibf') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
	        <div class="col-md-12">

				 <!-- {{ Form::open(array('url' => 'muniprofile/' .$id. '/show_mlgu')) }} -->
				 {{ Form::open(array('url' => 'muniprofile/' .$id. '/show_mlgu/delete', 'method' => 'POST')) }}
				 
				 	<a class="btn  btn-small btn-info" href="{{ URL::to('muniprofile/' .$id.'/edit_person_mlgu') }}">
				<i class="fa fa-edit"></i>		Edit 
				 	</a>

					<!-- {{ Form::hidden('_method', 'DELETE') }} -->
					{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}

			        <a class="btn bg-navy" href="{{ URL::to('muniprofile/' .$person->profile_id. '/show_mlgu') }}">Close </a>
				 {{ Form::close() }}	 
	        </div>
	    </div>

	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
                <div class="panel panel-default">
		    		<div class="panel-heading">
		    			Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
							 	<tr>
					                <th>Last Name</th>
					                <td>{{ $person->last_name }}</td>
					            </tr>

					            <tr>
					                <th>First Name</th>
					                <td>{{ $person->first_name }}</td>
					            </tr>

					            <tr>
					                <th>Middle initial</th>
					                <td>{{ $person->middle_name }}</td>
					            </tr>
					            <tr>
					                <th>Sex</th>
					                @if($person->sex == 0)
					                	<td>Male</td>
					                @else
					                	<td>Female</td>
					                @endif
					            </tr>
					            <tr>
					                <th>Age</th>
					                <td>{{ $person->age }}</td>
					            </tr>
					            <tr>
					                <th>Civil Status</th>
					                <td>{{ $person->civil_status }}</td>
					            </tr>
					            <tr>
					                <th>IP Group</th>
					                <td>{{ $person->ip_group }}</td>
					            </tr>
					            <tr>
					                <th>Educational Attainment</th>
					                <td>{{ $person->educ_attainment }}</td>
					            </tr>
					            <tr>
					                <th>Position</th>
					                <td>{{ $person->position }}</td>
					            </tr>
					            <tr>
					                <th>Start Date</th>
					                <td>{{ Report::change_date_format($person->startdate) }}</td>
					            </tr>
					            <tr>
					                <th>End Date</th>
					                <td>{{ Report::change_date_format($person->enddate) }}</td>
					            </tr>
							</table>
						</div>
					</div>
				</div>					
			</div>
		</div>
	<script>
			$("form").submit(function(){

            					var confirmation = confirm('are you sure to this action?');
            					if(confirmation){
            							return true;

            					}else{
            						return false;
            					}


            				});
		</script>
	
@stop