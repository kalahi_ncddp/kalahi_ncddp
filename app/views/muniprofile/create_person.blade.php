@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	<div class="row">
	    </div>
	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alerft alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
	<!-- 		<a class="btn btn-default" href="{{ URL::to('brgyprofile/'.$id.'/show_mdcp') }}">Go back</a> -->
				<a class="btn btn-default" href="{{ URL::to('muniprofile/'.$id.'/show_mdcp') }}">Go back</a>
	
				
				<div class="box box-primary">
	            	<div class="box-body">
						{{ Form::open(array('url' => 'muniprofile/submit_person', 'method' => 'POST', 'role' => 'form')) }}
						<div class="table-responsive">
							<table class="table table-striped table-bordered bdc table-hover">
								<tr>
					                <th class="width: 21%;">Last Name</th>
					                <td>
					                	{{ Form::text('last_name', Input::old('last_name'), array('class' => 'form-control', 'required')) }}
					                </td>
					            </tr>

					            <tr>
					                <th class="width: 21%;">First name</th>
					                <td>
					                	{{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control', 'required')) }}
					                </td>
					            </tr>

					            <tr>
					                <th class="width: 21%;">Middle Initial</th>
					                <td>
					                	{{ Form::text('middle_name', Input::old('middle_name'), array('class' => 'form-control', 'maxlength'=>'4')) }}
					                </td>
					            </tr>
					            <tr>
					                <th class="width: 21%;">Sex</th>
					                <td>
					                	{{ Form::select('sex', array(''=>'Selec Gender','Male' => 'Male', 'Female' => 'Female'), Input::old('sex'), array('class' => 'form-control')) }}
					                </td>
					            </tr>
					            <tr>
					                <th class="width: 21%;">Age</th>
					                <td>
					                	{{ Form::input('number', 'age', Input::old('age'), array('class' => 'form-control', 'min'=>'15','max'=>'99')) }}
					                </td>
					            </tr>
					            <tr>
					                <th class="width: 21%;">Civil Status</th>
					                <td>
					                	{{ Form::select('civil_status', [''=>'Select Civil Status']+$status_list, Input::old('civil_status'), array('class' => 'form-control')) }}
					                </td>
					            </tr>
					              <tr>
					                <th class="width: 21%;">{{ Form::label('is_ipgroup', 'IP Group?') }}	</th>
					                <td>
					                	<!-- {{ Form::checkbox('is_ipgroup', '1', array( 'class' => 'form-control', 'id'=>'is_ipgroup')) }}  -->
					         			{{ Form::checkbox('is_ipgroup', '1',  array( 'id' => 'is_ipgroup')) }} 

							
					                </td>
					            </tr>
					            <tr>
					                <th class="width: 21%;">IP Group</th>
					                <td>
					                	{{ Form::select('ip_group', [''=>'Select IP Group']+$group_list, Input::old('ip_group'), array('class' => 'form-control')) }}
					                </td>
					            </tr>
					            <tr>
					                <th class="width: 21%;">Educational Attainment</th>
					                <td>
					                	{{ Form::select('educ_attainment', [''=>'Select Educational Attainment']+$level_list, Input::old('educ_attainment'), array('class' => 'form-control')) }}
					                </td>
					            </tr>
					            <tr>
					                <th class="width: 21%;">Position in MDC</th>
					                <td>
					                	{{ Form::select('position', [''=>'Select Position in MDC']+$position_list, Input::old('position'), array('class' => 'form-control')) }}
					                {{-- Form::text('position', Input::old('position'), array('class' => 'form-control')) --}}
					                </td>
					            </tr>

								<tr>
							        <th class="width: 21%;">Date of Service (inclusive date)</th>

					                <td>
					                	<div class="form-group">
		                                    <div class="input-group">
		                                        <input type="text" class="form-control pull-left" value="" placeholder="mm/dd/yyyy - mm/dd/yyyy"id="daterange" required/>
		                                        <div class="input-group-addon">
		                                            <i class="fa fa-calendar"></i>
		                                        </div>
		                                    </div><!-- /.input group -->
		                                </div>

					                	{{ Form::text('startdate', Report::change_date_format(Input::old('startdate')), array('class' => 'form-control hidden', 'max' => date("m/d/Y"))) }}
					                	{{ Form::text('enddate', Report::change_date_format(Input::old('enddate')), array('class' => 'hidden  form-control', 'max' => date("m/d/Y"))) }}
									</td>
						        </tr>
					            <tr>
					                <th class="width: 21%;">Sector Represented</th>
					                <td>
					                	{{--{{ Form::text('sector_rep', Input::old('sector_rep'), array('class' => 'form-control')) }}--}}
					                	<!-- {{ Form::select('sector_rep', array('Sector 1' => 'Sector 1', 'Sector 2' => 'Sector 2'), Input::old('sector_rep'), array('class' => 'form-control', 'required')) }} -->
					                    		<div class="col-md-12">
                                        		 	<div class="box box-primary">
                                        		 		<div class="box-header">
                                        		 			 <h3 class="box-title">Other Sectors Represented</h3>
                                        		 			 <div class="box-tools pull-right">
                                                                <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                                                            </div>
                                        		 		</div>
                                        			 	<div class="box-body">
                                        			 		<div class="table-responsive">
                                        						<table class="table table-striped table-bordered table-hover">
                                        							@foreach($sector_list as $sector)

                                        									<div class = "form-control"><input type="checkbox" name="sector_rep[]" value="{{$sector}}">&nbsp{{$sector}}</div>
                                        							@endforeach
                                        						</table>
                                        					</div>
                                        			 	</div>
                                        			</div>
                                        		</div>
					                 </td>
					            </tr>
					        </table>
					    </div>
						<br>
					{{ Form::hidden('id', $id)}}
					{{ Form::submit('Create Person', array('class' => 'btn btn-primary')) }}
					{{ Form::close() }}
				</div>
			</div>
		</div>

		<script>
		    //helper function for custom validity
(function (exports) {
                            function valOrFunction(val, ctx, args) {
                                if (typeof val == "function") {
                                    return val.apply(ctx, args);
                                } else {
                                    return val;
                                }
                            }

                            function InvalidInputHelper(input, options) {
                                input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

                                function changeOrInput() {
                                    if (input.value == "") {
                                        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                                    } else {
                                        input.setCustomValidity("");
                                    }
                                }

                                function invalid() {
                                    if (input.value == "") {
                                        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                                    } else {
                                       console.log("INVALID!"); input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
                                    }
                                }

                                input.addEventListener("change", changeOrInput);
                                input.addEventListener("input", changeOrInput);
                                input.addEventListener("invalid", invalid);
                                changeOrInput();
                            }
                            exports.InvalidInputHelper = InvalidInputHelper;
                        })(window);
			$(document).ready(function(){


                $('.attendees').each(function(i,e){
                    InvalidInputHelper( e, {
                                      defaultText: "Please enter an email address!",

                                      emptyText: "Please enter an email address!",

                                      invalidText: function (input) {
                                        return 'Value must be less than or equal to the Represented Household in the Barangay (' + input.max + ')';
                                      }
                                    });
                });
                $('.family').each(function(i,e){
                                         InvalidInputHelper(e , {
                                                  defaultText: "Please enter an email address!",

                                                  emptyText: "Please enter an email address!",

                                                  invalidText: function (input) {
                                                    console.log(input);
                                                    return 'Value must be less than or equal to the Total Families in the Barangay (' + input.max + ')';
                                                  }
                                                });
                });

				$('#daterange').daterangepicker(
				  {
				    format: 'MM/DD/YYYY',
				    startDate : moment().format('MM/DD/YYYY'),
				    endDate : moment().format('MM/DD/YYYY')
				  },
				  function(start, end, label) {
				   	   	$('input[name="startdate"]').val(start.format('MM/DD/YYYY'));
				  		$('input[name="enddate"]').val(end.format('MM/DD/YYYY'));
				  }
				);
				//when the Add Filed button is clicked
				$("#add").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items").append('<div class="input-group"><input type="text" class="form-control" placeholder="Name of sitio eg. STO SITIO" name="sitio[]" oninput="checkduplicate(this)"><span class="input-group-addon"> <span class="fa fa-trash-o  delete"></span></span></div><br>');
					});

				$("body").on("click", ".delete", function (e) {
					$(this).parent().remove();
				});


				$("input[name='startdate']").change(function (e) {
					$("input[name='enddate']").attr('min', $("input[name='startdate']").val());
				});

				$("input[name='enddate']").change(function (e) {
					$("input[name='startdate']").attr('max', $("input[name='enddate']").val());
				});

				$("input[name='total_household']").change(function (e) {
					$("input[name='no_household']").attr('max', $("input[name='total_household']").val());
				});

				$("input[name='total_families']").change(function (e) {
					$("input[name='no_families']").attr('max', $("input[name='total_families']").val());
				});

				$("input[name='no_atnmale']").change(function (e) {
					$("input[name='no_ipmale']").attr('max', $("input[name='no_atnmale']").val());
					$("input[name='no_oldmale']").attr('max', $("input[name='no_atnmale']").val());
				});

				$("input[name='no_atnfemale']").change(function (e) {
					$("input[name='no_ipfemale']").attr('max', $("input[name='no_atnfemale']").val());
					$("input[name='no_oldfemale']").attr('max', $("input[name='no_atnfemale']").val());
				});

				$("input[name='no_household']").change(function (e) {
					$("input[name='no_pphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='no_slphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='no_iphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");
				});

				$("input[name='no_families']").change(function (e) {
//					$("input[name='no_ppfamilies']").attr('max', $("input[name='no_families']").val());
//					$("input[name='no_slpfamilies']").attr('max', $("input[name='no_families']").val());
//					$("input[name='no_ipfamilies']").attr('max', $("input[name='no_families']").val());
					$("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");
				});


				$("input[name='total_household']").change(function (e) {


					$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");
				});

				$("input[name='total_families']").change(function (e) {
				 $("input[name='no_ppfamilies']").attr('max', $("input[name='total_families']").val());
                                					$("input[name='no_slpfamilies']").attr('max', $("input[name='total_families']").val());
                                					$("input[name='no_ipfamilies']").attr('max', $("input[name='total_families']").val());
					$("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");
				});

               	$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");

                $("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");

			});


				$("#is_ipgroup").change(function (e) {

									if($("#is_ipgroup").is(":checked") ){
									    	// $("select[name='ipgroup']").removeAttr('disabled');
									    	$("select[name='ip_group']").removeAttr('disabled');
										
									}
									else if(!$("#is_ipgroup").is(":checked") ){
										$("select[name='ip_group']").attr('disabled','disabled');
										
									}


									// else{
				                        				
									// }
								
								});

			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName('sitio[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}


			/* get the total in attendees */
			var total = (function($){
				return {
					
					totalPerson : function(){
						var total_ = parseInt($("input[name='no_atnfemale']").val()) + parseInt($("input[name='no_atnmale']").val()) ;
						$('.total_person').text(total_ || '0');
					},
					totalIP : function(){
						var total_ = parseInt($("input[name='no_ipfemale']").val()) + parseInt($("input[name='no_ipmale']").val()) ;
						$('.total_ip').text(total_ || '0');
					},
					totalOld : function(){
						var total_ = parseInt($("input[name='no_oldfemale']").val()) + parseInt($("input[name='no_oldmale']").val()) ;
						$('.total_old').text(total_ || '0');
					}

				}
			})(jQuery);
                total.totalPerson();
                total.totalIP();
                total.totalOld();

			$("input[name='no_atnmale']").keyup(total.totalPerson);
			$("input[name='no_ipmale']").keyup(total.totalIP);
			$("input[name='no_oldmale']").keyup(total.totalOld);
			$("input[name='no_atnfemale']").keyup(total.totalPerson);
			$("input[name='no_ipfemale']").keyup(total.totalIP);
			$("input[name='no_oldfemale']").keyup(total.totalOld);
			


		</script>
@stop