@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
		@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
		
	      
	    <!-- /. ROW  -->
	    
	    
    <div class="col-md-12">
    	
    	@if($muni_profile->is_draft==1)
	        		 	<div class="alert alert-info">This is still a draft  <a href="{{ URL::to('muniprofile/' .$muni_profile->profile_id.'/edit') }}">edit the document</a> to set it in final draft</div>
	         @endif
	          @if( !is_review($muni_profile->profile_id) )
    	{{ Form::open(array('url' => 'muniprofile/' . $muni_profile->profile_id)) }}
		{{ Form::hidden('_method', 'DELETE') }}
		{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
		<a class="btn  btn-small btn-info" href="{{ URL::to('muniprofile/' .$muni_profile->profile_id.'/edit') }}">
			<i class="fa fa-edit"></i>Edit
		</a>
		@endif
		<a class="btn btn-small btn-success" href="{{ URL::to('muniprofile/'.$muni_profile->profile_id.'/show_mlgu') }}">MLGU</a>
		<a class="btn btn-small btn-success" href="{{ URL::to('muniprofile/'.$muni_profile->profile_id.'/show_mdcp') }}">MDC Profile</a>
		<a class="btn bg-navy" href="{{ URL::to('muniprofile') }}">Close</a>
		
		 {{ Form::close() }}
	 	<!--Basic info-->
	 	<div class="col-md-12">
	 			 {{ viewComment($muni_profile->profile_id) }}
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Basic Information</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	            <div class="box-body">
					<div class="table-responsive">
						<table class="table table-striped  table-hover">
						 	<tr>
				                <th>Municipality</th>
				                <td>{{ $municipality = Report::get_municipality($muni_profile->municipal_psgc) }}</td>
				            </tr>
				            <tr>
				                <th>Province</th>
				                <td>{{ $province = Report::get_province($muni_profile->municipal_psgc) }}</td>
				            </tr>
				            <tr>
				                <th>Region</th>
				                <td>{{ $region = Report::get_region($muni_profile->municipal_psgc) }}</td>
				            </tr>
				             <tr>
				                <th>Cycle</th>
				                <td>{{ $muni_profile->cycle_id }}</td>
				            </tr>
				              <tr>
				                <th>Program</th>
				                <td>{{ $muni_profile->program_id }}</td>
				            </tr>
				            
				            <tr>
				                <th>Number of Barangays</th>
				                <td>{{ $muni_profile->barangay_number }}
			           			</td>
				            </tr>
				            <tr>
			                <th>Inclusive Years of KC</th>
			                <td>
			                	{{ $muni_profile->year_source }}
			                </td>
			            </tr>
						</table>
					</div>
	            </div>

	        </div>
	 	</div>
	 	
	 	<!--Financial Resources -->
		<div class="col-md-12">
			<h3>Empowerment</h3>
			<div class="table-responsive" style="overflow: scroll">
							<table id="items6" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="15" style="text-align:left">What organization operate within the municipality? </th>
					          	</thead>
					          		<tr>
					          			<th style="min-width: 150px">Name</th>
					          			<th style="min-width: 150px">Type of Organization </th>
					          			<th style="min-width: 150px">Formal?</th>
					          			<th style="min-width: 150px">LGU-accredited?</th>
					          			<th style="min-width: 150px">Advocacy</th>
					          			<th style="min-width: 150px">Area of Operation</th>
					          			<th style="min-width: 150px">Years operation</th>
					          			<th style="min-width: 150px">Years operating in municipality</th>
					          			<th style="min-width: 150px">Active or inactive organization</th>
					          			<th style="min-width: 150px">Activities or Services</th>
					          			<th style="min-width: 150px">Total male members from municipality</th>
					          			<th style="min-width: 150px">Male IP members from municipality</th>
					          			<th style="min-width: 150px">Female IP members from municipality</th>
					          			<th style="min-width: 150px">Marginalized sectors represented</th>
					          			<!-- <th style="min-width: 150px">Option</th> -->
					          		</tr>
					          	
					            @foreach($muni_empowerment as $empowerment)
					            	<tr>
					            		<td>						            		
					            			{{$empowerment->org_name}}
					            		</td>
					            		<td>						            		
					            			{{$empowerment->org_type}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_formal}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_accredited}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_advocacy}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_area}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_years_operating}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_active_inactive}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_act_service}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_total_male}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_total_female}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_male_ip}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_female_ip}}
					            		</td>

					            		<td>						            		
					            			{{$empowerment->org_marginalized}}
					            		</td>




					            		
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        
					</div>

					<br>
					
					<div class="table-responsive">
							<table id="items4" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="3" style="text-align:left">What is the total annual budget of the municipality? How much was the IRA in the previous year?  What are other sources of funds of the municipality? </th>
					          	</thead>
					          		<tr>
					          			<th>Fund Source</th>
					          			<th>Amount </th>
					          			<!-- <th>Option</th> -->
					          		</tr>
					          	
					            @foreach($muni_fundsource as $fundsource)
					            	<tr>
					            		<td>						            		
					            			{{$fundsource->fund_source}}
					            		</td>
					            		<td>						            		
					            			{{ number_format($fundsource->amount,2)}}
					            		</td>
					            		<!-- <td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td> -->
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        
					</div>

					<br>
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Financial Resources</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					<table id="budget" class="table table-striped table-bordered table-hover">
						<thead> 
							<th colspan="2" style="text-align:left"> How were municipal funds allocated in the previous year?</th>
						</thead>
						<tr>
			                <th>Environment</th>
			                <td>
			                	{{ currency_format($muni_profile->alloc_env); }}
			                </td>
			            </tr>
			            <tr>
			                <th>Economic sector </th>
			                <td>
			                	{{  currency_format($muni_profile->alloc_econ); }}
			                </td>
			            </tr>
			            <tr>
			                <th>Insfrastructure</th>
			                <td>
			                	{{currency_format($muni_profile->alloc_infra);}}
			                </td>
			            </tr>
			            <tr>
			                <th>Social Development</th>
			                <td>
			                	{{currency_format($muni_profile->alloc_basic); }}
			                </td>
			            </tr>
			           <!--  <tr>
			                <th>Education</th>
			                <td>
			                	{{ Form::input('number', 'alloc_educ', $muni_profile->alloc_educ, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
			            </tr>
			            <tr>
			                <th>Peace and order</th>
			                <td>
			                	{{ Form::input('number', 'alloc_peace', $muni_profile->alloc_peace, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
			            </tr> -->
			            <tr>
			                <th>Institutional Sector</th>
			                <td>
			                	{{ currency_format($muni_profile->alloc_inst); }}
			                </td>
			            </tr>
			            <tr>
			                <th> Gender and Development</th>
			                <td>
			                	{{  currency_format($muni_profile->alloc_gender); }}
			                </td>
			            </tr>
			            <tr>
			                <th> DRRM</th>
			                <td>
			                	{{ currency_format($muni_profile->alloc_drrm); }}
			                </td>
			            </tr>
			            
			            <tr>
			                <th> Other Allocations</th>
			                <td>
			                	{{ currency_format($muni_profile->alloc_others); }}
			                </td>
			            </tr>
			            <tr>
			            	<th> Total </th>
			            	<td class="totalbudget"> {{ currency_format($muni_profile->alloc_env + $muni_profile->alloc_econ + $muni_profile->alloc_infra + $muni_profile->alloc_basic + $muni_profile->alloc_educ + $muni_profile->alloc_peace + $muni_profile->alloc_inst + $muni_profile->alloc_gender + $muni_profile->alloc_drrm + $muni_profile->alloc_others)}}</td>
			            </tr>
					</table>
					<br>
					
					
					
					

					<div class="table-responsive">
							<table id="items7" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="3" style="text-align:left">How was the GAD Fund utilized by the municipality? </th>
					          	</thead>
					          		<tr>
					          			<th>Activity</th>
					          			<th>Cost </th>
					          			<!-- <th>Option</th> -->
					          		</tr>
					          	
					            @foreach($muni_gadfund as $gad_fund)
					            	<tr>
					            		<td>						            		
					            			{{$gad_fund->activity}}
					            		</td>
					            		<td>						            		
					            			{{$gad_fund->cost}}
					            		</td>
					            		<!-- <td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td> -->
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        <!-- <button id="add7" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add GAD Fund</button> -->
					</div>


					<div class="table-responsive">
							<table id="items8" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="3" style="text-align:left">How was the DRRM fund utilized by the municipality? </th>
					          	</thead>
					          		<tr>
					          			<th>Activity</th>
					          			<th>Cost </th>
					          			<!-- <th>Option</th> -->
					          		</tr>
					          	
					            @foreach($muni_drrmfund as $drrm_fund)
					            	<tr>
					            		<td>						            		
					            			{{$drrm_fund->drrm_activity}}
					            		</td>
					            		<td>						            		
					            			{{$drrm_fund->drrm_cost}}
					            		</td>
					            		<!-- <td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td> -->
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        <!-- <button id="add8" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add DRRM Fund</button> -->
					</div>
					
					
			 	</div>
			 </div>
		 </div>
		  <!--Socio Economic Situation -->
	 	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Socio Economic Situation</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	          	<div class="box-body">		
						<div class="table-responsive">
							<table id="items1" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="5" style="text-align:left">Identify top three income-generating activities in the Municipality, average income per activity and number of HHs involve </th>
					          	</thead>
					          		<tr>
					          			<th>Activity</th>
					          			<th>Average Income </th>
					          			<th>No. Household Involved </th>
					          			<th>Seasonality </th>
					          			<!-- <th>Option</th> -->
					          		</tr>
					          	
					            @foreach($muni_income as $income)
					            	<tr>
					            		<td>						            		
					            			{{$income->activity}}
					            		</td>
					            		<td>						            		
					            			{{$income->avg_income}}
					            		</td>
					            		<td>						            		
					            			{{$income->act_hdinvolved}}
					            		</td>
					            		<td>						            		
					            			{{$income->seasonality}}
					            		</td>
					            		<!-- <td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td> -->
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					    <!--     <button id="add1" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add Income Generating Activity</button> -->
						</div>	
				</div>
	        </div>
	 	</div>
		 <!--Other Resources -->
		

		 <!--Land Tenure Status -->
		<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Land Tenure Status</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					
					<div class="table-responsive">
							<table id="items5" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="1" style="text-align:left">Tenurial Status </th>
					          		<th colspan="1" style="text-align:left">No. of HHs</th>
					          		<th colspan="1" style="text-align:left">No. of HH Head Male</th>
					          		<th colspan="1" style="text-align:left">No. of HH Head Female</th>
					          	</thead>
					     <tr>
			                <td>Owner</td>
			                <td>{{ $muni_profile->hhs_owner }}
			            	</td>
			            	<td>{{  $muni_profile->headmale_owner}}
			            	</td>	
			            	<td>{{  $muni_profile->headffemale_owner }}
			            	</td>
			            </tr>
			            <tr>
			                <td>Tenant</td>
			                <td>{{ $muni_profile->hhs_tenant }}
			            	</td>
			            	<td>{{ $muni_profile->headmale_tenant}}
			            	</td>
			            	<td>{{  $muni_profile->headfemale_tenant }}
			            	</td>
			            </tr>
			            <tr>
			                <td>Renting</td>
			                <td>{{ $muni_profile->hhs_renting }}
			            	</td>
			            	<td>{{ $muni_profile->headmale_renting }}
			            	</td>
			            	<td>{{  $muni_profile->headfemale_renting }}
			            	</td>
			            </tr>
			            <tr>
			                <td>Squatting(?)</td>
			                <td>{{ $muni_profile->hhs_squatting }}
			            	</td>
			            	<td>{{  $muni_profile->headmale_squatting }}
			            	</td>
			            	<td>{{ $muni_profile->headfemale_squatting}}
			            	</td>
			            </tr>
			            


			          
							</table>
							<br>
							<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"></th>
							</thead>
							<tr>
				                <th>What is the average annual houshold income?</th>
				                <td>
				                	{{ $muni_profile->householdincome_average }}
				                	<!-- {{ Form::text('householdincome_average', Input::old('householdincome_average'), array('class' => 'form-control', 'required')) }} -->
				                </td>
				            </tr>
				            <tr>
				                <th>What is the average annual income for male-headed HHs?</th>
				                <td>
				                	{{ $muni_profile->maleheaded_hhs }}
				                	<!-- {{ Form::text('maleheaded_hhs', Input::old('maleheaded_hhs'), array('class' => 'form-control', 'required')) }} -->
				                </td>
				            </tr>
				             <tr>
				                <th>What is the average annual income for female-headed HHs?</th>
				                <td>
				                	{{ $muni_profile->femaleheaded_hhs }}
				                	<!-- {{ Form::text('femaleheaded_hhs', Input::old('femaleheaded_hhs'), array('class' => 'form-control', 'required')) }} -->
				                </td>
				            </tr>

				             <tr>
				                <th>What is the average annual income for IP-headed HHs?</th>
				                <td>
				                	{{ $muni_profile->ipheaded_hss}}
				                	<!-- {{ Form::text('ipheaded_hss', Input::old('ipheaded_hss'), array('class' => 'form-control', 'required')) }} -->
				                </td>
				            </tr>
				            
				         
						</table>
							<br>
					       
					</div>
					
					<br>
					
					
			 	</div>
			 </div>
		 </div>



		 
		



	 	 <!--Mode of Transportation and Cost -->
	 	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Mode of Transportation and Cost</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	          	<div class="box-body">		
						<div class="table-responsive">
							<table id="items9" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="3" style="text-align:center">Mode of Transportation and Cost </th>
					          	</thead>
					          		<tr>
					          			<th>Mode of Transportation	</th>
					          			<th>Cost (Minimum - Maximum</th>
					          			<!-- <th>No. Household Involved </th>
					          			<th>Seasonality </th> -->
					          			<!-- <th>Option</th> -->
					          		</tr>
					          	
					            @foreach($muni_mode_transpo as $mode_transpo)
					            	<tr>
					            		<td>						            		
					            			{{$mode_transpo->mode_transportation	}}
					            		</td>
					            		<td>						            		
					            			{{$mode_transpo->mode_cost}}
					            		</td>
					            		
					            		<!-- <td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td> -->
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        <!-- <button id="add9" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add Mode of Transportation and Cost</button> -->
						</div>	
				</div>
	        </div>
	 	</div>

@stop