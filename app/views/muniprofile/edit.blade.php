@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
    <div class="col-md-12">
	 	{{ Form::model($muni_profile, array('action' => array('MuniProfileController@update', $muni_profile->profile_id), 'method' => 'PUT')) }}
	 	
	 	<!--Basic info-->
	 	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Basic Information</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	            <div class="box-body">
					<div class="table-responsive">
						<table class="table table-striped  table-hover">
				            <tr>
				                <th>Municipality</th>
				                <td>{{ $municipality = Report::get_municipality($muni_profile->municipal_psgc) }}</td>
				            </tr>
				            <tr>
				                <th>Province</th>
				                <td>{{ $province = Report::get_province($muni_profile->municipal_psgc) }}</td>
				            </tr>
				            <tr>
				                <th>Region</th>
				                <td>{{ $region = Report::get_region($muni_profile->municipal_psgc) }}</td>
				            </tr>
				             <tr>
				                <th>Cycle</th>
				                <td>{{ $muni_profile->cycle_id }}</td>
				            </tr>
				              <tr>
				                <th>Program</th>
				                <td>{{ $muni_profile->program_id }}</td>
				            </tr>
				           
				            <tr>
				                <th>Number of Barangays</th>
				                <td>{{ Form::input('number', 'barangay_number', $muni_profile->barangay_number, array('readonly','class' => 'form-control', 'min'=>'0')) }}
			           			</td>
				            </tr>
				            <tr>
			                <th>Inclusive Years of KC</th>
			                <td>
			                	{{ Form::input('number', 'year_source', $muni_profile->year_source, array('class' => 'form-control', 'min'=>'0', 'max'=>date("Y"))) }}
			                </td>
			            </tr>
						</table>
					</div>
	            </div>

	        </div>
	 	</div>
	 	
	 	
	 	<!--Financial Resources -->
		<div class="col-md-12">
			<h3>Empowerment</h3>
			<div class="table-responsive" style="overflow: scroll">
							<table id="items6" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="15" style="text-align:left">What organization operate within the municipality? </th>
					          	</thead>
					          		<tr>
					          			<th style="min-width: 200px">Name</th>
					          			<th style="min-width: 200px">Type of Organization </th>
					          			<th style="min-width: 200px">Formal?(Registered)</th>
					          			<th style="min-width: 200px">LGU-Accredited?</th>
					          			<th style="min-width: 200px">Advocacy</th>
					          			<th style="min-width: 200px">Area of Operation</th>
					          			
					          			<th style="min-width: 200px">Years operating in municipality</th>
					          			<th style="min-width: 200px">Active or inactive organization</th>
					          			<th style="min-width: 200px">Activities</th>
					          			<th style="min-width: 200px">Total male members from municipality</th>
					          			<th style="min-width: 200px">Total female members from municipality</th>
					          			<th style="min-width: 200px">Male IP members from municipality</th>
					          			<th style="min-width: 200px">Female IP members from municipality</th>
					          			<th style="min-width: 200px">Marginalized sectors represented</th>
					          			<th style="min-width: 200px">Option</th>
					          		</tr>
					          	
					            @foreach($muni_empowerment as $empowerment)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" value="{{$empowerment->org_name}}" name="org_name[]" oninput="checkduplicate(this)" required>
					            		</td>
					            		<td>						            		
					            			<input type="text" class="form-control" value="{{$empowerment->org_type}}" name="org_type[]" oninput="checkduplicate(this)" required>
					            		</td>

					            		<td>						            		
					            			<input type="text" class="form-control" value="{{$empowerment->org_formal}}" name="org_formal[]" oninput="checkduplicate(this)" required>
					            		</td>

					            		<td>						            		
					                	{{ Form::select('org_accredited[]', array('' => 'Yes or No?','Yes' => 'Yes', 'No' => 'No'), $empowerment->org_accredited, array('class' => 'form-control')) }}
					            			
					            			<!-- <select type="text" class="form-control" value="{{$empowerment->org_accredited}}" name="org_accredited[]" oninput="checkduplicate(this)" required> -->
					            		</td>

					            		<td>						            		
					            			<input type="text" class="form-control" value="{{$empowerment->org_advocacy}}" name="org_advocacy[]" oninput="checkduplicate(this)" required>
					            		</td>

					            		<td>						            		
					                	{{ Form::select('org_area[]', array('' => 'Choose Area','National' => 'National', 'Region' => 'Region', 'Provincial' => 'Provincial','Municiapl'=>'Municipal','Inter-Barangay'=>'Inter-Barangay','Barangay'=>'Barangay'), $empowerment->org_area, array('class' => 'form-control')) }}

					            			<!-- <input type="text" class="form-control" value="{{$empowerment->org_area}}" name="org_area[]" oninput="checkduplicate(this)" required> -->
					            		</td>

					            		<td>						            		

					            			<input type="number" class="form-control" value="{{$empowerment->org_years_operating}}" name="org_years_operating[]" oninput="checkduplicate(this)" required>
					            		</td>

					            		<td>					
					                	{{ Form::select('org_active_inactive[]', array('' => 'Active or Inactive','Active' => 'Active', 'Inactive' => 'Inactive'), $empowerment->org_active_inactive, array('class' => 'form-control')) }}

					            			<!-- <input type="text" class="form-control" value="{{$empowerment->org_active_inactive}}" name="org_active_inactive[]" oninput="checkduplicate(this)" required> -->
					            		</td>

					            		<td>						            		
					            			<input type="text" class="form-control" value="{{$empowerment->org_act_service}}" name="org_act_service[]" oninput="checkduplicate(this)" required>
					            		</td>

					            		<td>						            		
					            			<input type="number" class="form-control" value="{{$empowerment->org_total_male}}" name="org_total_male[]" oninput="checkduplicate(this)" required>
					            		</td>

					            		<td>						            		
					            			<input type="number" class="form-control" value="{{$empowerment->org_total_female}}" name="org_total_female[]" oninput="checkduplicate(this)" required>
					            		</td>

					            		<td>						            		
					            			<input type="number" class="form-control" value="{{$empowerment->org_male_ip}}" name="org_male_ip[]" oninput="checkduplicate(this)" required>
					            		</td>

					            		<td>						            		
					            			<input type="number" class="form-control" value="{{$empowerment->org_female_ip}}" name="org_female_ip[]" oninput="checkduplicate(this)" required>
					            		</td>

					            		<td>						            		
					            			<input type="text" class="form-control" value="{{$empowerment->org_marginalized}}" name="org_marginalized[]" oninput="checkduplicate(this)" required>
					            		</td>




					            		<td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td>
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        <button id="add6" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add Organization</button>
					</div>

					<br><br>
					
					<div class="table-responsive">
							<table id="items4" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="3" style="text-align:left">What is the total annual budget of the municipality? How much was the IRA in the previous year? What are other sources of funds of the municipality? </th>
					          	</thead>
					          		<tr>
					          			<th>Fund Source</th>
					          			<th>Amount </th>
					          			<th>Option</th>
					          		</tr>
					          	
					            @foreach($muni_fundsource as $fundsource)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Fund Source" value="{{$fundsource->fund_source}}" name="fund_source[]" oninput="checkduplicate(this)" required>
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" placeholder="Amount (PhP)" value="{{$fundsource->amount}}" name="amount[]" required>
					            		</td>
					            		<td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td>
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        <button id="add4" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add Fund Source</button>
					</div>

					<br>
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Financial Resources</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					<table id="budget" class="table table-striped table-bordered table-hover">
						<thead> 
							<th colspan="2" style="text-align:left">How were municipal funds allocated in the previous year? </th>
						</thead>
						<tr>
			                <th>Environment</th>
			                <td>
			                	{{ Form::currency('alloc_env', number_format($muni_profile->alloc_env,2), array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
			            </tr>
			            <tr>
			                <th>Economic sector </th>
			                <td>
			                	{{ Form::currency('alloc_econ', number_format($muni_profile->alloc_econ,2), array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
			            </tr>
			            <tr>
			                <th>Insfrastructure</th>
			                <td>
			                	{{ Form::currency('alloc_infra', number_format($muni_profile->alloc_infra,2), array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
			            </tr>
			            <tr>
			                <th>Social Development</th>
			                <td>
			                	{{ Form::currency('alloc_basic', number_format($muni_profile->alloc_basic,2), array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
			            </tr>
			           <!--  <tr>
			                <th>Education</th>
			                <td>
			                	{{ Form::input('number', 'alloc_educ', $muni_profile->alloc_educ, array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
			            </tr>
			            <tr>
			                <th>Peace and order</th>
			                <td>
			                	{{ Form::input('number', 'alloc_peace', $muni_profile->alloc_peace, array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
			            </tr> -->
			            <tr>
			                <th>Institutional Sector</th>
			                <td>
			                	{{ Form::currency('alloc_inst', number_format($muni_profile->alloc_inst,2), array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
			            </tr>
			            <tr>
			                <th> Gender and Development</th>
			                <td>
			                	{{ Form::currency('alloc_gender', number_format($muni_profile->alloc_gender,2), array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
			            </tr>
			            <tr>
			                <th> DRRM</th>
			                <td>
			                	{{ Form::currency('alloc_drrm', number_format($muni_profile->alloc_drrm,2), array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
			            </tr>
			            
			            <tr>
			                <th> Other Allocations</th>
			                <td>
			                	{{ Form::currency('alloc_others', number_format($muni_profile->alloc_others,2), array('class' => 'form-control', 'min'=>'0')) }}
			                </td>
			            </tr>
			            <tr>
			            	<th> Total </th>
			            	<td class="totalbudget"> {{ currency_format($muni_profile->alloc_env + $muni_profile->alloc_econ + $muni_profile->alloc_infra + $muni_profile->alloc_basic + $muni_profile->alloc_educ + $muni_profile->alloc_peace + $muni_profile->alloc_inst + $muni_profile->alloc_gender + $muni_profile->alloc_drrm + $muni_profile->alloc_others) }}</td>
			            </tr>
					</table>
					<br>
					
					
					
					

					<div class="table-responsive">
							<table id="items7" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="3" style="text-align:left">How was GAD Fund utilized by the municipality?</th>
					          	</thead>
					          		<tr>
					          			<th>Activity</th>
					          			<th>Cost </th>
					          			<th>Option</th>
					          		</tr>
					          	
					            @foreach($muni_gadfund as $gad_fund)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Fund Source" value="{{$gad_fund->activity}}" name="gad_activity[]" oninput="checkduplicate(this)" required>
					            		</td>
					            		<td>						            		
					            			<input type="text" class="form-control price" placeholder="Amount (PhP)" value="{{$gad_fund->cost}}" name="cost[]" required>
					            		</td>
					            		<td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td>
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        <button id="add7" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add GAD Fund</button>
					</div>
					<br>
					<br>

					<div class="table-responsive">
							<table id="items8" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="3" style="text-align:left">How was the DRRM fund utilized by the municipality?</th>
					          	</thead>
					          		<tr>
					          			<th>Activity</th>
					          			<th>Cost </th>
					          			<th>Option</th>
					          		</tr>
					          	
					            @foreach($muni_drrmfund as $drrm_fund)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Activity" value="{{$drrm_fund->drrm_activity}}" name="drrm_activity[]" oninput="checkduplicate(this)" required>
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" placeholder="Amount (PhP)" value="{{$drrm_fund->drrm_cost}}" name="drrm_cost[]" required>
					            		</td>
					            		<td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td>
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        <button id="add8" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add DRRM Fund</button>
					</div>
					
					
			 	</div>
			 </div>
		 </div>
		  <!--Socio Economic Situation -->
	 	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Socio Economic Situation</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	          	<div class="box-body">		
						<div class="table-responsive">
							<table id="items1" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="5" style="text-align:left">Identify top three income-generating activities in the Municipality, average income per activity and number of HHs involved </th>
					          	</thead>
					          		<tr>
					          			<th>Activity</th>
					          			<th>Average Income </th>
					          			<th>No. Household Involved </th>
					          			<th>Seasonality </th>
					          			<th>Option</th>
					          		</tr>
					          	
					            @foreach($muni_income as $income)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Activity" value="{{$income->activity}}" name="activity[]" oninput="checkduplicate(this)" required>
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" placeholder="Average Income" value="{{$income->avg_income}}" name="avg_income[]" required>
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" placeholder="No. Household" value="{{$income->act_hdinvolved}}" name="act_hdinvolved[]" required>
					            		</td>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Seasonality" value="{{$income->seasonality}}" name="seasonality[]" required>
					            		</td>
					            		<td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td>
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        <button id="add1" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add Income Generating Activity</button>
						</div>	
				</div>
	        </div>
	 	</div>
		 <!--Other Resources -->
	<!-- 	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Other Resources</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					
					<div class="table-responsive">
							<table id="items5" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="6" style="text-align:center">Technical Assistance Resources </th>
					          	</thead>
					          		<tr>
					          			<th>Equipment Type</th>
					          			<th>Current Condition </th>
					          			<th>Current Capability </th>
					          			<th>Fuel, POL Product Consumption </th>
					          			<th>Prevailing Rental Sales </th>
					          			<th>Option</th>
					          		</tr>
					          	
					            @foreach($muni_techresource as $techresource)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Equipment Type" value="{{$techresource->type_equiptment}}" name="type_equiptment[]" oninput="checkduplicate(this)" required>
					            		</td>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Current Condition" value="{{$techresource->curr_condition}}" name="curr_condition[]" required>
					            		</td>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Current Capability" value="{{$techresource->curr_capability}}" name="curr_capability[]" required>
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" placeholder="Consumption" value="{{$techresource->consumption}}" name="consumption[]" required>
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" placeholder="Rental Rate" value="{{$techresource->rental_rate}}" name="rental_rate[]" required>
					            		</td>
					            		<td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td>
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        <button id="add5" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add Resource</button>
					</div>
					
					<br>
					
					
			 	</div>
			 </div>
		 </div> -->

		 <!--Land Tenure Status -->
		<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Land Tenure Status</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					
					<div class="table-responsive">
							<table id="items5" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="1" style="text-align:left">Tenurial Status </th>
					          		<th colspan="1" style="text-align:left">No. of HHs</th>
					          		<th colspan="1" style="text-align:left">No. of HH Head Male</th>
					          		<th colspan="1" style="text-align:left">No. of HH Head Female</th>
					          	</thead>
					     <tr>
			                <td>Owner</td>
			                <td>{{ Form::input('number', 'hhs_owner', $muni_profile->hhs_owner, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>
			            	<td>{{ Form::input('number', 'headmale_owner', $muni_profile->headmale_owner, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>	
			            	<td>{{ Form::input('number', 'headffemale_owner', $muni_profile->headffemale_owner, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>
			            </tr>
			            <tr>
			                <td>Tenant</td>
			                <td>{{ Form::input('number', 'hhs_tenant', $muni_profile->hhs_tenant, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>
			            	<td>{{ Form::input('number', 'headmale_tenant', $muni_profile->headmale_tenant, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>
			            	<td>{{ Form::input('number', 'headfemale_tenant', $muni_profile->headfemale_tenant, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>
			            </tr>
			            <tr>
			                <td>Renting</td>
			                <td>{{ Form::input('number', 'hhs_renting', $muni_profile->hhs_renting, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>
			            	<td>{{ Form::input('number', 'headmale_renting', $muni_profile->headmale_renting, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>
			            	<td>{{ Form::input('number', 'headfemale_renting', $muni_profile->headfemale_renting, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>
			            </tr>
			            <tr>
			                <td>Squatting(?)</td>
			                <td>{{ Form::input('number', 'hhs_squatting', $muni_profile->hhs_squatting, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>
			            	<td>{{ Form::input('number', 'headmale_squatting', $muni_profile->headmale_squatting, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>
			            	<td>{{ Form::input('number', 'headfemale_squatting', $muni_profile->headfemale_squatting, array('class' => 'form-control', 'min'=>'0')) }}
			            	</td>
			            </tr>
			            


			          
							</table>
							<br>
							<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"></th>
							</thead>
							<tr>
				                <th>What is the average annual houshold income?</th>
				                <td>
				                	{{ Form::input('number', 'householdincome_average', $muni_profile->householdincome_average, array('class' => 'form-control', 'min'=>'0')) }}
				                	<!-- {{ Form::text('householdincome_average', Input::old('householdincome_average'), array('class' => 'form-control')) }} -->
				                </td>
				            </tr>
				            <tr>
				                <th>What is the average annual income for male-headed HHs?</th>
				                <td>
				                	{{ Form::input('number', 'maleheaded_hhs', $muni_profile->maleheaded_hhs, array('class' => 'form-control', 'min'=>'0')) }}
				                	<!-- {{ Form::text('maleheaded_hhs', Input::old('maleheaded_hhs'), array('class' => 'form-control')) }} -->
				                </td>
				            </tr>
				             <tr>
				                <th>What is the average annual income for female-headed HHs?</th>
				                <td>
				                	{{ Form::input('number', 'femaleheaded_hhs', $muni_profile->femaleheaded_hhs, array('class' => 'form-control', 'min'=>'0')) }}
				                	<!-- {{ Form::text('femaleheaded_hhs', Input::old('femaleheaded_hhs'), array('class' => 'form-control')) }} -->
				                </td>
				            </tr>

				             <tr>
				                <th>What is the average annual income for IP-headed HHs?</th>
				                <td>
				                	{{ Form::input('number', 'ipheaded_hss', $muni_profile->ipheaded_hss, array('class' => 'form-control', 'min'=>'0')) }}
				                	<!-- {{ Form::text('ipheaded_hss', Input::old('ipheaded_hss'), array('class' => 'form-control')) }} -->
				                </td>
				            </tr>
				            
				         
						</table>
							<br>
					       
					</div>
					
					<br>
					
					
			 	</div>
			 </div>
		 </div>



		 
		



	 	 <!--Mode of Transportation and Cost -->
	 	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Mode of Transportation and Cost</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	          	<div class="box-body">		
						<div class="table-responsive">
							<table id="items9" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="3" style="text-align:center">Mode of Transportation and Cost </th>
					          	</thead>
					          		<tr>
					          			<th>Mode of Transportation	</th>
					          			<th>Cost (Minimum - Maximum</th>
					          			<!-- <th>No. Household Involved </th>
					          			<th>Seasonality </th> -->
					          			<th>Option</th>
					          		</tr>
					          	
					            @foreach($muni_mode_transpo as $mode_transpo)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Mode of Transportation" value="{{$mode_transpo->mode_transportation	}}" name="mode_transportation[]" oninput="checkduplicate(this)" required>
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" placeholder="Cost" value="{{$mode_transpo->mode_cost}}" name="mode_cost[]" required>
					            		</td>
					            		
					            		<td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td>
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        <button id="add9" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add Mode of Transportation and Cost</button>
						</div>	
				</div>
	        </div>
	 	</div>
		
		<div class="box-body">
			 			
			 				<div class="form-group">
								<label>Save as draft</label>
							  <input type="checkbox" name="is_draft" {{ $muni_profile->is_draft==1 ? 'checked': '' }}>
							</div>
			 
		
		 {{ Form::submit('Save', array('class' => 'btn btn-info')) }}
		 <a class="btn bg-navy" href="{{ URL::to('muniprofile') }}">Close</a>

		{{ Form::close() }}
		 
		
	</div>
	
	
	<div class="clearfix"></div>
	
	

		<script>
			$(document).ready(function(){

				var tbl = $('#budget');
    			$('#budget .price').keyup( function(){
    				calculateSum()
    			} );
    			

    			function calculateSum() {
        			var tbl = $('#budget');
        			var sum = 0;
        			tbl.find('tr').each(function () {
	            		$(this).find('.price').each(function (i,e) {
	            			var value = parseFloat(number_format($(this).val(),2))
	            			console.log(value);
	                		if (value.length != 0) {
	                	    	sum = sum + parseFloat(value);
	               		 	}
	            		});
					
        			});
        			console.log(sum);
            		$('#budget .totalbudget').text(sum.toFixed(2));
    			}
				$("#add1").click(function (e) {
				//Append a new row of code to the "#items" div
					if($('#items1').find('tr').length != 5){
						$("#items1").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Activity" name="activity[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="number" class="form-control" min="0" placeholder="Average Income" name="avg_income[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" placeholder="No. Household" name="act_hdinvolved[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" placeholder="Seasonality" name="seasonality[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
					}else{
						alert('only 3 Socio Economic allowed');
					}
							
				});
				
				$("#add4").click(function (e) {
				//Append a new row of code to the "#items" div
				if($('#items4 tbody').children().length == 1){
							$("#items4").append('<tr><td>'+
					            	'<input type="text" class="form-control" value="IRA" placeholder="Fund Source" name="fund_source[]" oninput="checkduplicate(this)" readonly="readonly" required>'+
					            	'</td>'+
					            	'<td><input type="text" class="form-control price" placeholder="Amount (PhP)" name="amount[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
					}	
					
					else{
						$("#items4").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Fund Source" name="fund_source[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="text" class="form-control fund_amount" placeholder="Amount (PhP)" name="amount[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
					}
				
				});
				$("#add5").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items5").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Equipment Type" name="type_equiptment[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="text" class="form-control" placeholder="Current Condition" name="curr_condition[]" required></td>'+
					            	'<td><input type="text" class="form-control" placeholder="Current Capability" name="curr_capability[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" placeholder="Consumption" name="consumption[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" placeholder="Rental Rate" name="rental_rate[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});

				$("#add6").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items6").append('<tr><td>'+
					            	'<input type="text" class="form-control" name="org_name[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="text" class="form-control" name="org_type[]"  placeholder="ex. PO, NGO, Private institution-csr"></td>'+
					            	'<td>{{ Form::select('org_formal[]', array('' => 'Yes or No?','Yes' => 'Yes', 'No' => 'No'), '', array('class' => 'form-control')) }}</td>'+
					            	'<td>{{ Form::select('org_accredited[]', array('' => 'Yes or No?','Yes' => 'Yes', 'No' => 'No'), '', array('class' => 'form-control')) }}</td>'+
					            	'<td><input type="text" class="form-control" name="org_advocacy[]"  placeholder="Savings, religious, farmers" required></td>'+
					            	'<td>{{ Form::select('org_area[]', array('' => 'Choose Area','National' => 'National', 'Region' => 'Region','Provincial' => 'Provincial', 'Municiapl'=>'Municipal','Inter-Barangay'=>'Inter-Barangay','Barangay'=>'Barangay'), '', array('class' => 'form-control')) }}</td>'+
					            	'<td><input type="text" class="form-control" name="org_years_operating[]" required></td>'+
					            	'<td>{{ Form::select('org_active_inactive[]', array('' => 'Active or Inactive?','Active' => 'Active', 'Inactive' => 'Inactive'), '', array('class' => 'form-control')) }}</td>'+
					            	'<td><input type="text" class="form-control" name="org_act_service[]" required placeholder="(Identify major activities)"></td>'+
					            	'<td><input type="text" class="form-control" name="org_total_male[]" required></td>'+
					            	'<td><input type="text" class="form-control" name="org_total_female[]" required></td>'+
					            	'<td><input type="text" class="form-control" name="org_male_ip[]" required></td>'+
					            	'<td><input type="text" class="form-control" name="org_female_ip[]" required></td>'+
					            	'<td><input type="text" class="form-control" name="org_marginalized[]" required placeholder="Marginalized sectors"></td>'+
					            	
					            	

					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});

				$("#add7").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items7").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Activity" name="gad_activity[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="text" class="form-control price" placeholder="cost (PhP)" min="0" name="cost[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');

					/* BIND PRICE FORMAT*/	
				});

				$("#add8").click(function (e) {
				//Append a new row of code to the "#items" div
					$("#items8").append('<tr><td>'+
						            	'<input type="text" class="form-control" placeholder="Activity" name="drrm_activity[]" oninput="checkduplicate(this)" required>'+
						            	'</td>'+
						            	'<td><input type="text" class="form-control price" placeholder="cost (PhP)" min="0" name="drrm_cost[]" required></td>'+
						            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
						            	'</td>'+
						           '</tr>');
				});
				

				$("#add9").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items9").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Mode of Transportation" name="mode_transportation[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="text" class="form-control price" placeholder="cost (PhP)" min="0" name="mode_cost[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});
				
				$("body").on("click", ".delete", function (e) {
					$(this).parent().parent().remove();
				});
				
				
				
			});
			
			/*Check Duplicate Entry*/
			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName(e.name);
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
				
			}


			// $(function () {
   //  			var tbl = $('#budget');
   //  			tbl.find('tr').each(function () {
   //     		 		$(this).find('input[type=number]').bind("change", function () {
   //          			calculateSum();
   //      			});
   //  			});

   //  			function calculateSum() {
   //      			var tbl = $('#budget');
   //      			var sum = 0;
   //      			tbl.find('tr').each(function () {
   //          		$(this).find('input[type=number]').each(function () {
   //              		if (!isNaN(this.value) && this.value.length != 0) {
   //              	    	sum = sum + parseFloat(this.value);
   //             		 	}
   //          		});
					
   //          		$(this).find('.totalbudget').text(sum.toFixed(2));
   //      			});
   //  			}
			// });
		</script>
	
@stop