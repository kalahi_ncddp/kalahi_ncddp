@extends('layouts.default')

@section('username')
	{{ $username }}
@stop



@section('content')
	<a href="{{ URL::to('brgy_trainings/'.$id); }}" class="btn btn-default">go back</a>
	{{ Form::open(array('url' => 'brgy_trainings/delete_participant/', 'method' => 'POST', 'role' => 'form')) }}
	{{ Form::hidden('reference_no', $id)}}

	    <div class="row">
		    <div class="col-md-12">
				<!-- Advanced Tables -->
				
				
		    	<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Records
		    			<div class="pull-right">
		    				<a class="btn  btn-primary" href="{{ URL::to('brgy_trainings/' .$id.'/add_participant')}}">
							 Add New
							</a>
							<a class="btn btn-danger" data-toggle="modal" data-target="#myModal">
			        			Delete Participant
			        		</a>
			        		<a class="btn bg-navy" href="{{URL::to('brgy_trainings')}}">Close</a>
		    			</div>
		    			<div class="clearfix"></div>
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th> <input type="checkbox" id="participant_all"> </th>
										<th>Barangay</th>
										<th>Last Name</th>
										<th>First Name</th>
										<th>Middle Initial</th>
										<th>Age</th>
										<th>Sex</th>
										
										<th>Volunteer</th>
										<th>Sector</th>
										<td>option</td>
									</tr>
								</thead>
								<tbody>
									@foreach($data as $trainee)
									<tr>
										<td>
											<input type="checkbox" class="form-input" name="participant[]" value="{{$trainee->participant_id}}">
											
										</td>
										<td>{{ isset($trainee->getbarangay()->barangay) ? $trainee->getbarangay()->barangay :'No Added Barangay' }}</td>
										<td>
											{{ $trainee->lastname }}
										</td>
										<td>
											{{ $trainee->firstname }}
										</td>
										<td>
											{{ $trainee->middlename}}
										</td>
										<td>
											{{ $trainee->age }}
										</td>
										<td>
											{{ isset($trainee->beneficiary) ? $trainee->beneficiary->sex  :$trainee->sex}}
										</td>
										
										
										
										<td>
											@if($trainee->is_volunteer)
												{{'Yes'}}
											@else
												{{'No'}}
											@endif
										</td>
										<td>{{ isset($trainee->sector) ? $trainee->sector : 'No Sectors Added yet' }}</td>
										<td>
											@if($trainee->beneficiary_id != null)
											<a href="{{ URL::to('brgy_trainings/'. $trainee->beneficiary_id) }}/edit_participant" class="btn btn-info">EDIT</a>
											@else
											
											     <a href="{{ URL::to('resolved?lastname='.$trainee->lastname.'&firstname='.$trainee->firstname.'&middlename='.$trainee->middlename.'&age='.$trainee->age ) }}" class="btn bg-orange">Resolve</a>
											@endif
										</td>
										
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Delete Participants</h4>
                    </div>
                    <div class="modal-body">
                    		Are you sure you want to delete participants record? 
						<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
						 {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
						
					</div>
	            </div>
            </div>
        </div>
        {{ Form::close() }}
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
        $('#participant_all').change(function(){
       		if($('#participant_all').is(":checked")){
				$("input[name='participant[]']").prop('checked', true);	
			}
			else{
				$("input[name='participant[]']").prop('checked', false);	
			}
		});
      });
    </script>
@stop