@extends('layouts.default')

@section('username')
	{{ $username }}
@stop


@section('content')

	    <div class="row">
		    <div class="col-md-6">
				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
				{{ Form::open(array('url' => 'brgy_trainings', 'method' => 'POST', 'role' => 'form')) }}

				<div class="form-group">
					{{ Form::label('training_title', 'Name/Title of Training') }}
					{{ Form::text('training_title', Input::old('training_title'), array('class' => 'form-control', 'required')) }}
				</div>

				<div class="form-group">
					{{ Form::label('training_cat', 'Training Category') }}
					{{ Form::select('training_cat', $cat_list, Input::old('training_cat'), array('class' => 'form-control', 'required')) }}
				</div>

				<div class="form-group ">
                                    {{ Form::label('start', 'Start') }}
                                    {{ Form::input('text', 'start_date','', array('class' => 'form-control dates start_date','max' => date("m/d/Y"), 'required')) }}
                </div>
                <div class="form-group ">
                                     {{ Form::label('start', 'End') }}
                                     {{ Form::input('text', 'end_date','', array('class' => 'form-control dates end_date','max' => date("m/d/Y"), 'required')) }}
               </div>
				
				<div class="form-group">
					{{ Form::label('barangay', 'Barangay') }}
					{{ Form::select('barangay', [''=>'Select Barangay']+$brgy_list, Input::old('barangay'), array('class' => 'form-control', 'required', 'id'=> 'barangay')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('psgc_id', 'PSGC ID') }}
					{{ Form::text('psgc_id', Input::old('psgc_id'), array('class' => 'form-control', 'required', 'readonly')) }}
				</div>
			</div>
			  <div class="col-md-6">

				<div class="form-group">
					{{ Form::label('municipality', 'Municipality') }}
					{{ Form::text('municipality', $municipality, array('class' => 'form-control', 'disabled')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('province', 'Province') }}
					{{ Form::text('province', $province, array('class' => 'form-control', 'required', 'disabled')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('region', 'Region') }}
					{{ Form::text('region', $region, array('class' => 'form-control', 'required', 'disabled')) }}
				</div>
				
				<div class="form-group">

					{{ Form::label('kc_code', 'KC-NCDDP group') }}

					{{ Form::text('kc_code',$kc_code, array('class' => 'form-control','disabled')) }}
				</div>

				<div class="form-group">
					{{ Form::label('cycle_id', ' Cycle') }}
					{{ Form::select('cycle_id', [''=>'Select KC NCDDP Cycle']+$cycle_list, Input::old('cycle_id'), array('class' => 'form-control', 'required')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('program_id', 'Program') }}
					{{ Form::select('program_id', [''=>'Select Program']+$program_list, Input::old('program_id'), array('class' => 'form-control', 'required')) }}
				</div>

				<div class="col-md-12 ">
						<div class="form-group pull-right">
				{{ Form::submit('Add Community Training', array('class' => 'btn btn-primary')) }}
				<a class="btn bg-navy" href="{{URL::to('brgy_trainings')}}">Close</a>

			</div>
		</div>
				{{ Form::close() }}
			</div>
		</div>

		<script>
			
			$(document).ready(function(){
			 $('.dates').datetimepicker({
                        			            pick12HourFormat: false,
                        			            pickTime:false
                        	});
                        	$("input.start_date").blur(function(){
                            				var elem = $(this);
                            				var actual_start = elem.val();


                            				$("input.end_date").data("DateTimePicker").setMinDate(new Date(actual_start));

                            });
				$("#barangay").change(function (e) {
					$('.hidden').hide()
					$("#psgc_id").attr('value', $("#barangay").val());
					var selector = "option[value='" +$("#barangay").val()+ "']";
					$(selector).attr('selected', 'selected');
					
				});
				
				
			});
			
		
		</script>
	
@stop