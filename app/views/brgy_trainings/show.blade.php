@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	<div class="row">
			
	        <div class="col-md-12">

					<!-- @if($training->is_draft==1)
        		 	<div class="alert alert-info">This is still a draft  <a href="{{ URL::to('brgy_trainings/' .$training->reference_no.'/edit') }}">edit the document</a> to set it in final draft</div>
         			@endif
	
				 	<a class="btn  btn-small btn-info" href="{{ URL::to('brgy_trainings/' .$training->reference_no.'/edit') }}">
				 		Edit
				 	</a>
					<button class="btn btn-danger" data-toggle="modal" data-target="#myModal">
        				Delete Training Record
        			 </button>
        			<a class="btn bg-navy" href="{{URL::to('brgy_trainings')}}">Close</a> -->

        			<div class="pull-left">
	        		{{ HTML::linkRoute('brgy_trainings.show', 'Back to Community Trainings','', array('class' => 'btn btn-default btn')) }}
	        	</div>
	        	<div class="pull-right">
		        		@if($training->is_draft==1)
	        		 	<div class="alert alert-info">This is still a draft  <a href="{{ URL::to('brgy_trainings/' .$training->reference_no.'/edit') }}">edit the document</a> to set it in final draft</div>
	         			@endif
	    				@if( !is_review($training->reference_no) )

					 	<a class="btn  btn-small btn-info" href="{{ URL::to('brgy_trainings/' .$training->reference_no.'/edit') }}">
					 <i class="fa fa-edit"></i>		Edit
					 	</a>
						<button class="btn btn-danger" data-toggle="modal" data-target="#myModal">
	        				Delete Training Record
	        			 </button> 

 <span class="btn bg-olive" data-toggle="modal" data-target="#pincos_grievances">
						PINCOS and Grievances
					</span>
	        			 <a class="btn bg-navy" href="{{ URL::to('brgy_trainings') }}">
					 Close
					</a>
					@endif
	        	</div>
	        </div>
	    </div>
	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
		    		     {{ viewComment($training->reference_no) }}

                <div class="panel panel-default">
		    		<div class="panel-heading">
		    			Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">
							<tr>
					                <th>Barangay</th>
					                <td>{{ Report::get_barangay($training->psgc_id) }}</td>
					            </tr>
							 	 <tr>
					                <th>Municipality</th>

					                <td>{{ Report::get_municipality_by_brgy_id($training->psgc_id) }}</td>

					            </tr>
					            <tr>
					                <th>Province</th>

					                <td>{{ Report::get_province_by_brgy_id($training->psgc_id) }}</td>
					            </tr>
					        </table>
					    </div>
					    <div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					                <th>Region</th>
					                <td>{{ Report::get_region_by_brgy_id($training->psgc_id) }}</td>

					            </tr>
					             <tr>
					                <th>Cycle</th>
					                <td>{{ $training->cycle_id }}</td>
					            </tr>
					              <tr>
					                <th>Program</th>
					                <td>{{ $training->program_id }}</td>
					            </tr>
							</table>
						</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Training Information
		    		</div>

		    		<div class="panel-body">
						<div class="table-responsive">
							  <div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">
								 <tr>
					                <th>Training Title</th>
					                <td>
					                	{{ $training->training_title }}
									</td>
					            </tr>
					            <tr>
					                <th>Training Category</th>
					                <td>
					                	{{ $training->training_cat }}
					                </td>
					            </tr>
					            <tr>
					                <th>Date Conducted</th>
					                <td>
					                	{{ toDate($training->start_date) }} - {{ toDate($training->end_date) }}
									</td>
					            </tr>
					        </table>
					    </div>
					      <div class="col-md-6">
					      	<table class="table table-striped table-bordered table-hover">
					            <tr>
					                <th>Venue</th>
					                <td>
					                	{{ $training->venue }}
					                </td>
					            </tr>
					            <tr>
					                <th>Duration</th>
					                <td>
					                	{{ $training->duration }} day(s)
					                </td>
					            </tr>
					            <tr>
					                <th>Reported By</th>
					                <td>
					                	{{ $training->reported_by }}
					                </td>
					            </tr>
							</table>
							</div>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Attendees
		    		</div>
		    		
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Attendees </th>
					                <th>Male</th>
					                <th>Female</th>
					                <th>Total</th>
					            </tr>
					              <tr>
					            	<th>No. of Person </th>
					                <td>{{ $training->total_participants_by_genders('M')}}</td>
					                <td>{{ $training->total_participants_by_genders('F')}}</td>
					                <td>{{ $training->total_participants_by_genders('M')+$training->total_participants_by_genders('F')}}</td>
					            </tr>
					            <tr>
					                <th>No. of IP</th>
					                <td>{{ $training->total_ip('M') }}</td>
					                <td>{{ $training->total_ip('F') }}</td>
					                <td>{{ $training->total_ip('M')+$training->total_ip('F')}}</td>
					            </tr>
					            <tr>
					                <th>No. of SLP</th>
					                <td>{{ $training->total_slp('M') }}</td>
					                <td>{{ $training->total_slp('F') }}</td>
					                <td>{{ $training->total_slp('M')+$training->total_slp('F') }}</td>
					            </tr>

					             <tr>
					                <th>No. of Pantawid</th>
					                <td>{{ $training->total_pantawid('M') }}</td>
					                <td>{{ $training->total_pantawid('F') }}</td>
					                <td>{{ $training->total_pantawid('M')+$training->total_pantawid('F') }}</td>
					            </tr>

					            <tr>
					                <th>No. of Volunteer</th>
					                <td>{{ $training->total_male_vol('M') }}</td>
					                <td>{{ $training->total_female_vol('F') }}</td>
					                <td>{{ $training->total_male_vol('M')+$training->total_female_vol('F') }}</td>
					            <tr>
					            
					                <th>No. of Non Volunteer</th>
					                <td>{{ $training->total_non_male_vol('M') }}</td>
					                <td>{{ $training->total_non_female_vol('F') }}</td>
					                <td>{{ $training->total_non_male_vol('M')+$training->total_non_female_vol('F') }}</td>

					            </tr>

					           
							</table>
							<a class="btn  btn-small btn-info" href="{{ URL::to('brgy_trainings/'. $training->reference_no .'/enlist_participants') }}">
 								Add Participants
 							</a>
						</div>
					</div>
				</div>			
				
				<div class="panel panel-default">
					<div class="panel-heading">
						Resource Person
		    		</div>
					<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							
							<tr>
					            <th>Name </th>
					            <th>Designation</th>
					            <th>Topics Discussed</th>
					        </tr>
							@foreach($trainor_data as $trainor)
							<tr>
					            <td>{{ $trainor->trainor_name }}</td>
					            <td>{{ $trainor->designation }}</td>
					            <td>{{ $trainor->topics_discussed }}</td>
					        </tr>
								
							@endforeach
							
						</table>
					</div>
					</div>
				</div>
				
				
				
						
			</div>
		</div>
		
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                	  <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Delete {{ $training->reference_no }}</h4>
                    </div>
                    
                    <div class="modal-body">
                    	{{ Form::open(array('url' => 'brgy_trainings/' . $training->reference_no)) }}
                    	{{ Form::hidden('_method', 'DELETE') }}
                    		Are you sure you want to delete this training record? 
						<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
						{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
				 		{{ Form::close() }}
					</div>
	            </div>
            </div>
        </div>
		@include('modals.pincos_grievances')
	
@stop