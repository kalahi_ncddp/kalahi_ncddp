@extends('layouts.default')

@section('username')
	{{ $username }}
@stop


@section('content')

	    <div class="row">
		    <div class="col-md-12">
				<!-- Advanced Tables -->
				
				
		    	<div class="panel panel-default">
		    		<div class="panel-heading">
		    			{{ $title }}
		    			 @if($position->position=='ACT')
                            <span class="btn btn-default pull-right" id="approved" style="margin:0 10px">Review</span>
                                            <span class="hidden module">mun_trainings</span>
                        @endif
		    		 <a class="btn  btn-primary pull-right" href="{{ URL::to('mun_trainings/create') }}">
					 	<i class="fa fa-plus"></i>Add New
					</a>

					<div class="clearfix"></div>
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										@if($position->position=='ACT')
									    <th>Select all
									        <input type="checkbox" id="selecctall"/>
									    </th>
									    @endif
										<th>Comm. Training ID</th>
										<th>Municipality</th>
										<th>Category</th>
										<th>Name of Training</th>
										<th>Date Conducted</th>
										<th>Program</th>
										<th>Total no. of Participants</th>
										<th>Details</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data as $training)
									<tr>
										    @if($position->position=='ACT')
										<td>
                                               @if($training->is_draft==0)
                                                 {{ $reference->approval_status($training->reference_no , 'ACT') }}
                                               @else
                                                   <span class="label label-warning">draft</span>
                                               @endif
										</td>
                                             @endif
										<td>
											{{ $training->reference_no }}
										</td>
										<td>
											{{ $municipality = Report::get_municipality($training->psgc_id) }}
										</td>
										<td>
											{{ $training->training_cat }}
										</td>
				
										
										<td>
											{{ $training->training_title }}
										</td>
										<td>
											{{ toDate($training->start_date) }} - {{ toDate($training->end_date) }}
										</td>
										
										<td>
											{{ $training->program_id }}
										</td>
										<td>
											{{ $training->total_participants() }}
										</td>


										 <td>
				                        @if($training->is_draft==0)

				                        				 <a class="btn btn-success" href="{{ URL::to( 'mun_trainings/'.$training->reference_no ) }}"><i class="fa fa-eye"></i>View Details
													    {{ hasComment($training->reference_no) }}
													    </a>
				                                     
				                                    @else
				                                     <a class="btn btn-warning" href="{{ URL::to( 'mun_trainings/'.$training->reference_no.'/edit' ) }}"><i class="fa fa-eye"></i>View Details( draft )
													    {{ hasComment($training->reference_no) }}
													    </a>
				                                      
				                        @endif
                              
                                  		</td>





									
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable(	);

      });
    </script>
@stop