@extends('layouts.default')

@section('username')
	{{ $username }}
@stop


@section('content')
	    <div class="row">
		    <div class="col-md-12">
				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
				<a href="{{ URL::previous(); }}" class="btn btn-default">Go back</a>

				{{ Form::model($participant, array('action' => array('MunTrainingController@updateParticipants', $participant->beneficiary_id), 'method' => 'PUT')) }}
				
				<div class="row">
					<div class="col-md-6">
						
						
					<div class="form-group">
							{{ Form::label('barangay', 'Barangay') }}
							{{ Form::select('psgc_id', $brgy_list, $participant->psgc_id, array('class' => 'form-control', 'required', 'id' => 'barangay')) }}
						</div>
						<div class="form-group">
							{{ Form::label('lastname', 'Last Name') }}
							{{ Form::text('lastname', $participant->beneficiary->lastname, array('class' => 'form-control', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('firstname', 'First Name') }}
							{{ Form::text('firstname', $participant->firstname, array('class' => 'form-control', 'required')) }}
						</div>
						<div class="form-group">
							{{ Form::label('midlename', 'Middle Initial') }}
							{{ Form::text('middlename',$participant->beneficiary->middlename, array('class' => 'form-control','maxlength'=>'4','id' => 'middlename', 'required','maxlength'=>'4')) }}
						</div>

						<div class="form-group">
							{{ Form::label('age', 'Age') }}
							{{ Form::text('age',$participant->beneficiary->age, array('class' => 'form-control', 'id' => 'age', 'required','min'=>'15','max'=>'99')) }}
						</div>
                        <div class="form-group">
                            {{ Form::label('sector', 'Sector') }}
                            <table class="table table-striped table-bordered table-hover">
                            @foreach($sector_list as $sector)
                                    @if(in_array($sector,  $participant->beneficiary->sector))
                                        <div class = "form-control"><input type="checkbox" name="sector[]" value="{{$sector}}" checked>&nbsp{{$sector}}</div>
                                    @else
                                        <div class = "form-control"><input type="checkbox" name="sector[]" value="{{$sector}}">&nbsp{{$sector}}</div>
                                @endif
                            @endforeach
                            </table>
                            {{--{{ Form::text('sector', $participant->beneficiary->sector, array('class' => 'form-control', 'required','id'=>'sectors')) }}--}}
                        </div>
						<div class="form-group">
							{{ Form::label('Sex', 'Sex') }}
							{{ Form::select('sex', array('M' => 'Male', 'F' => 'Female'), $participant->beneficiary->sex, array('class' => 'form-control', 'id' => 'sex', 'required')) }}
						</div>
						<div class="form-inline">
							{{ Form::checkbox('is_ip', '1', '' , array( 'id' => 'is_ip', $participant->is_ip == 1 ? 'checked' : '')) }} 
							{{ Form::label('is_ip', 'IP') }}
						</div>
						<div class="form-inline">
							{{ Form::checkbox('is_pantawid', '1', '' , array( 'id' => 'is_pantawid', $participant->is_pantawid == 1 ? 'checked' : '')) }} 
							{{ Form::label('is_ip', 'Pantawid') }}
						</div>
						<div class="form-inline">
							{{ Form::checkbox('is_slp', '1', '' , array( 'id' => 'is_slp', $participant->is_slp == 1 ? 'checked' : '')) }} 
							{{ Form::label('is_ip', 'SLP') }}
						</div>					
						<div class="form-inline">
							{{ Form::checkbox('is_lguofficial', '1', '' , array( 'id' => 'is_lguofficial', $participant->is_lguofficial == 1 ? 'checked' : '')) }} 
							{{ Form::label('is_ip', 'LGU Official') }}
						</div>					
						
				
						<br>
				
					</div>
					
				</div>
				
	        		{{ Form::submit('update Participant', array('class' => 'btn btn-primary')) }}

					{{ Form::close() }}
			</div>
		</div>	
		
		
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Volunteers List</h4>
                    </div>
                    <div class="modal-body">
                    	{{ Datatable::table()->addColumn( 'firstname','lastname','middlename','birthdate','option')->setUrl(route('api.volunteer'))->render(); }}
					</div>
	            </div>
            </div>
        </div>
		
		<script>
		$(document).ready(function(){
			setAge();
				$('#dataTables-example').dataTable();
				
				$.fn.use_volunteer = function(vol){
					$(".form-control").attr('readonly', 'readonly');
					$("#barangay").removeAttr('readonly', 'readonly');
					$("#lastname").attr('value', vol.find(".lastname").text());
					$("#firstname").attr('value', vol.find(".firstname").text());
					$("#middlename").attr('value', vol.find(".middlename").text());
					$("#sex").attr('value',vol.find(".sex").text());
					$("#birthdate").attr('value', vol.find(".birthdate").text());
					$("#is_volunteer").attr('value', '1');
					$("#beneficiary_id").attr('value', vol.find(".beneficiary_id").text());
					
					$('#no_children').attr('value',vol.find(".no_children").text());
					$('#civil_status').attr('value',vol.find(".civil_status").text());
					$('#address').attr('value',vol.find(".address").text());
					$('#contact_no').attr('value',vol.find(".contact_no").text());
					$('#educ_attainment').attr('value',vol.find(".educ_attainment").text());
					$('#occupation').attr('value',vol.find(".occupation").text());
					setAge();

					$("#myModal").modal('toggle');
				};
				
				$("#reset").click(function(){
					$(".form-control").attr('value', "");
					$(".form-control").removeAttr('readonly');
				});
				
				$("#birthdate").change(function(){
					setAge();
					
				});
				
			});
		
		function setAge(){
				var today = new Date();
				var birthdate = new Date(document.getElementById("birthdate").value);
				var age = today.getFullYear() - birthdate.getFullYear();
    			var m = today.getMonth() - birthdate.getMonth();
    			if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
        			age--;
    			}
    			document.getElementById("age").value = age;
    			
    			e = document.getElementById("birthdate");
    			if(age < 15){	
					e.setCustomValidity("Age must be 15+");
				}else{
					e.setCustomValidity("");
				}
			}
		</script>
@stop