@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
		<div class="row">
			
	        <div class="col-md-12 ">
	        	<div class="pull-left">
	        		{{ HTML::linkRoute('mun_trainings.show', 'Back to Municipal Trainings','', array('class' => 'btn btn-default btn')) }}
	        	</div>
	        	<div class="pull-right">
		        		@if($training->is_draft==1)
	        		 	<div class="alert alert-info">This is still a draft  <a href="{{ URL::to('mun_trainings/' .$training->reference_no.'/edit') }}">edit the document</a> to set it in final draft</div>
	         			@endif
	    				@if( !is_review($training->reference_no) )

					 	<a class="btn  btn-small btn-info" href="{{ URL::to('mun_trainings/' .$training->reference_no.'/edit') }}">
					 	<i class="fa fa-edit"></i>	Edit
					 	</a>
						<button class="btn btn-danger" data-toggle="modal" data-target="#myModal">
	        				Delete Training Record
	        			 </button> 

					 <span class="btn bg-olive" data-toggle="modal" data-target="#pincos_grievances">
						PINCOS and Grievances
					</span>
	        			 <a class="btn bg-navy" href="{{ URL::to('mun_trainings') }}">
					 Close
					</a>
					@endif
	        	</div>
	        </div>
	    </div>
	    <br>
	 
	    <div class="row">
		    <div class="col-md-12">
		     {{ viewComment($training->reference_no) }}

                <div class="panel panel-default">
		    		<div class="panel-heading">
		    			Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							  <div class="col-md-6">  
							<table class="table table-striped table-bordered table-hover">
							 	 <tr>
					                <th>Municipality</th>
					                <td>{{ $municipality = Report::get_municipality($training->psgc_id) }}</td>
					            </tr>
					            <tr>
					                <th>KC Code</th>
					                <td></td>
					            </tr>
					            <tr>
					                <th>Province</th>
					                <td>{{ $province = Report::get_province($training->psgc_id) }}</td>
					            </tr>
					        </table>
					        </div>
					         <div class="col-md-6">  
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					                <th>Region</th>
					                <td>{{ $region = Report::get_region($training->psgc_id) }}</td>
					            </tr>
					             <tr>
					                <th>Cycle</th>
					                <td>{{ $training->cycle_id }}</td>
					            </tr>
					              <tr>
					                <th>Program</th>
					                <td>{{ $training->program_id }}</td>
					            </tr>
							</table>
						</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Training Information
		    		</div>

		    		<div class="panel-body">
						<div class="table-responsive">
							<div class="col-md-6">  
							<table class="table table-striped table-bordered table-hover">
								 <tr>
					                <th>Training Title</th>
					                <td>
					                	{{ $training->training_title }}
									</td>
					            </tr>
					            <tr>
					                <th>Training Category</th>
					                <td>
					                	{{ $training->training_cat }}
					                </td>
					            </tr>
					            <tr>
					                <th>Date Conducted</th>
					                <td>
					                	{{ toDate($training->start_date) }} - {{ toDate($training->end_date) }}
									</td>
					            </tr>
					        </table>
					    </div>
					    <div class="col-md-6">  
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					                <th>Venue</th>
					                <td>
					                	{{ $training->venue }}
					                </td>
					            </tr>
					            <tr>
					                <th>Duration</th>
					                <td>
					                	{{ $training->duration }} day(s)
					                </td>
					            </tr>
					            <tr>
					                <th>Reported By</th>
					                <td>
					                	{{ $training->reported_by }}
					                </td>
					            </tr>
							</table>
							</div>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Attendees
		    		</div>
		    		
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Attendees </th>
					                <th>Male</th>
					                <th>Female</th>
					                <th>Total</th>
					            </tr>
					            <tr>
					            	<th>No. of Person </th>
					                <td>{{ $training->total_participants_by_genders('M')}}</td>
					                <td>{{ $training->total_participants_by_genders('F')}}</td>
					                <td>{{ $training->total_participants_by_genders('M')+$training->total_participants_by_genders('F')}}</td>
					            </tr>
					            <tr>
					                <th>No. of IP</th>
					                <td>{{ $training->total_ip('M') }}</td>
					                <td>{{ $training->total_ip('F') }}</td>
					                <td>{{ $training->total_ip('M')+$training->total_ip('F')}}</td>
					            </tr>
					            <tr>
					                <th>No. of SLP</th>
					                <td>{{ $training->total_slp('M') }}</td>
					                <td>{{ $training->total_slp('F') }}</td>
					                <td>{{ $training->total_slp('M')+$training->total_slp('F')}}</td>
					            </tr>

					             <tr>
					                <th>No. of Pantawid</th>
					                <td>{{ $training->total_pantawid('M') }}</td>
					                <td>{{ $training->total_pantawid('F') }}</td>
					                <td>{{ $training->total_pantawid('M')+$training->total_pantawid('F') }}</td>
					            </tr>

					            <tr>
					                <th>No. of Volunteer</th>
					                <td>{{ $training->total_male_vol('M') }}</td>
					                <td>{{ $training->total_female_vol('F') }}</td>
					                <td>{{ $training->total_male_vol('M')+$training->total_female_vol('F') }}</td>
					            <tr>
					            
					                <th>No. of Non Volunteer</th>
					                <td>{{ $training->total_non_male_vol('M') }}</td>
					                <td>{{ $training->total_non_female_vol('F') }}</td>
					                <td>{{ $training->total_non_male_vol('M')+$training->total_non_female_vol('F') }}</td>

					            </tr>
							</table>
							<a class="btn  btn-small btn-info" href="{{ URL::to('mun_trainings/'. $training->reference_no .'/enlist_participants') }}">
 								Enlist Participants
 							</a>
						</div>
					</div>
				</div>			
				
				<div class="panel panel-default">
					<div class="panel-heading">
						Resource Person
		    		</div>
					<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							
							<tr>
					            <th>Name </th>
					            <th>Designation</th>
					            <th>Topics Discussed</th>
					        </tr>
							@foreach($trainor_data as $trainor)
							<tr>
					            <td>{{ $trainor->trainor_name }}</td>
					            <td>{{ $trainor->designation }}</td>
					            <td>{{ $trainor->topics_discussed }}</td>
					        </tr>
								
							@endforeach
							
						</table>
					</div>
					</div>
				</div>
				
				
				<div class="panel panel-default">
		    		<div class="panel-heading">
		    			Barangay Representative Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							
							<div class="col-md-6">  
						<div class="table-responsive" style="max-height:250px; overflow-y:scroll;">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Barangay </th>
					                <th>No of Participants</th>
					            </tr>
					            @foreach($barangay_data as $barangay)
					            <tr>
					                <td>{{ $barangay->barangay }}</td>
					                <td>{{ $barangay->getTrainingParticipantCount($training->reference_no) }}</td>
					        	</tr>
					    		@endforeach
							</table>
						</div>
						</div>

						<div class="col-md-6">  
						
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Total Number of Barangays </th>
					                <td>{{ $barangay_count }}</td>
					            </tr>
					            <tr>
					                <th>Number of Barangay Represented</th>
					                <td>{{ $training->getRepresented() }} </td>
					        	<tr>
					        	<tr>
					        		<th>Total number of attendees</th>
					        		<td>{{ $training->total_participants()}}</td>
					        	</tr>
							</table>
						</div>
						</div>
					</div>
				</div>

						
			</div>
		</div>
		
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                	  <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Delete {{ $training->reference_no }}</h4>
                    </div>
                    
                    <div class="modal-body">
                    	{{ Form::open(array('url' => 'mun_trainings/' . $training->reference_no)) }}
                    	{{ Form::hidden('_method', 'DELETE') }}
                    		Are you sure you want to delete this training record? 
						<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
						{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
				 		{{ Form::close() }}
					</div>
	            </div>
            </div>
        </div>
		@include('modals.pincos_grievances')
	
@stop