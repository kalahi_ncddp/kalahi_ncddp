@extends('layouts.default')

@section('username')
	{{ $username }}
@stop


@section('content')
	
	    {{ Form::model($training, array('action' => array('MunTrainingController@update', $training->reference_no), 'method' => 'PUT')) }}
	    <div class="row">
		    <div class="col-md-12">
                <div class="panel panel-default">
		    		<div class="panel-heading">
		    			Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<div class="col-md-6">
							<table class="table table-striped table-bordered table-hover">
							 	 <tr>
					                <th>Municipality</th>
					                <td>{{  Municipality::find($training->psgc_id)->municipality }}{{ $municipality = Report::get_municipality($training->psgc_id) }} </td>
					            </tr>
					             <tr>
					                <th>PSGC ID</th>
					                <td>{{ $training->psgc_id }}</td>
					            </tr>

					            <tr>
					                <th>KC Code</th>
					                <td></td>
					            </tr>
					            <tr>
					                <th>Province</th>
					                <td> {{  $province = Report::get_province($training->psgc_id) }}</td>
					            </tr>
					        </table>
					        </div>
					            <!--asasas-->
					        <div class="col-md-6">    
							<table class="table table-striped table-bordered table-hover">

					            <tr>
					                <th>Region</th>
					                <td>{{ $region = Report::get_region($training->psgc_id) }}</td>
					            </tr>
					             <tr>
					                <th>Cycle</th>
					                <td>{{ $training->cycle_id }}</td>
					            </tr>
					            <tr>
					                <th>Program</th>
					                <td>{{ $training->program_id }}</td>
					            </tr>
							</table>
					         </div>

						</div>
					</div>
				</div>

				<div class="panel panel-default ">
		    		<div class="panel-heading">
		    			Training Information
		    		</div>

		    		<div class="panel-body">
						<div class="table-responsive">
							  <div class="col-md-6">  
							<table class="table table-striped table-bordered table-hover">
								<tr>
					                <th>Training Title</th>
					                <td>
					                	{{ Form::text('training_title', $training->training_title, array('readonly','class' => 'form-control', 'required')) }}
					                </td>
					            </tr>
					            <tr>
					                <th>Training Category</th>
					                <td>
					                	{{ Form::select('training_cat', $cat_list,  $training->training_cat, array('class' => 'form-control', 'required')) }}
					                </td>
					            </tr>
					            <tr>
					               <th>Start Date</th>
					               <td>
					                    {{ Form::input('text', 'start_date',toDate($training->start_date), array('class' => 'form-control dates start_date', 'required')) }}
					               </td>
					          
					            </tr>
					            <tr>
					                  <th>End Date</th>
					                  <td>
                                        {{ Form::input('text', 'end_date',toDate($training->end_date), array('class' => 'form-control dates end_date', 'required')) }}
					                  </td>
					            </tr>
					             </table>
					           </div>
					            <div class="col-md-6">  
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					                <th>Venue</th>
					                <td>
					                	{{ Form::text('venue', Input::old('venue'), array('class' => 'form-control', 'required')) }}
					                </td>
					            </tr>
					            <tr>
					                <th>Duration</th>
					                <td>
					                	<div class ="form-inline">
					                	{{ Form::input('number','duration', getDurationDay($training->start_date,$training->end_date), array('class' => 'form-control duration ', 'required', 'min' => '1', 'readonly'=>'readonly')) }} day(s)

					                	</div>
					                </td>
					            </tr>
					            <tr>
					                <th>Reported By</th>
					                <td>
					                	{{ Form::text('reported_by', Input::old('reported_by'), array('class' => 'form-control', 'required')) }}
					                </td>
					            </tr>
							</table>
						</div>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default hidden">
		    		<div class="panel-heading">
		    			Attendees
		    		</div>
		    		
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Attendees </th>
					                <th>Male</th>
					                <th>Female</th>
					                <th>Total</th>
					            </tr>
					            <tr>
					            	<th>No. of Person </th>
					                <td>
					                	{{ Form::input('number', 'no_atnmale', $training->no_atnmale, array('class' => 'form-control', 'min'=>'0', 'readonly')) }}
					               	</td>
					                <td>
					                	{{ Form::input('number', 'no_atnfemale', $training->no_atnfemale, array('class' => 'form-control', 'min'=>'0', 'readonly')) }}
					                </td>
					                <td>
					                	{{ Form::input('number', 'total_atn', $training->no_atnmale+$training->no_atnfemale, array('class' => 'form-control', 'min'=>'0', 'disabled')) }}
					                </td>
					            </tr>
					            <tr>
					                <th>No. of IP</th>
					                <td>
					                	{{ Form::input('number', 'no_ipmale', $training->no_ipmale, array('class' => 'form-control', 'min'=>'0', 'max'=>$training->no_atnmale, '')) }}
							        </td>
					                <td>
					                	{{ Form::input('number', 'no_ipfemale', $training->no_ipfemale, array('class' => 'form-control', 'min'=>'0', 'max'=>$training->no_atnfemale, '')) }}
					                </td>
					                 <td>
					                	{{ Form::input('number', 'total_ip', $training->no_ipmale+$training->no_ipfemale, array('class' => 'form-control', 'min'=>'0', 'disabled')) }}
					                </td>
					            </tr>
					            </tr>
					                <th>No. of SLP</th>
					                <td>
					                	{{ Form::input('number', 'no_slpmale', $training->no_slpmale, array('class' => 'form-control', 'min'=>'0', 'max'=>$training->no_atnmale, '')) }}
							       	</td>
					                <td>
					                	{{ Form::input('number', 'no_slpfemale', $training->no_slpfemale, array('class' => 'form-control', 'min'=>'0', 'max'=>$training->no_atnfemale, '')) }}
							        </td>
							        <td>
					                	{{ Form::input('number', 'total_slp', $training->no_slpmale+$training->no_slpfemale, array('class' => 'form-control', 'min'=>'0', 'disabled')) }}
					                </td>
					            </tr>
					            
							</table>
						</div>
					</div>
				</div>
				
				

				<?php $count=0 ?>

				<div class="panel panel-default">
					<div class="panel-heading">
						Resource Person
		    		</div>
					<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<button id="add" type="button" class="btn btn-primary" >Add New Entry</button>
							<br>
							<div id="items">
								@foreach($trainor_data as $trainor)
								<div class="input-group">
									<div class="form-inline">
										<input type="text" class="form-control" name="trainor_name[]" value="{{$trainor->trainor_name}}" onchange="checkduplicate(this)" placeholder="Trainor Name" id="{{ $count }}" required>
										<input type="text" class="form-control" name="designation[]" value="{{$trainor->designation}}" placeholder="Designation" id="designation:{{ $count }}" onchange="checkduplicate(this)" required>
										<input type="text" class="form-control" name="topics_discussed[]" value="{{$trainor->topics_discussed}}" placeholder="Topic" id="topic:{{ $count }}" onchange="checkduplicate(this)" required>
									</div>
									<div class=""><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div>
									<br>
								</div>

								<?php $count++ ?>

								@endforeach
							</div>
						</table>
					</div>
					</div>
				</div>
				
				
				
				<div class="panel panel-default hidden">
					<div class="panel-heading">
		    			Barangay Representative Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Total Number of Barangays </th>
					                <td>{{ Form::input('number', 'brgy_count', count($barangay_list), array('class' => 'form-control', 'min'=>'0', 'disabled')) }}
								</tr>
					            <tr>
					                <th>Number of Barangay Represented</th>
					                <td>{{ Form::input('number', 'brgy_rep_count', count($barangay_psgc), array('class' => 'form-control', 'min'=>'0', 'disabled')) }}
					        	</tr>
					                <th>Precentage of Barangay Represented</th>
					                <td>{{ Form::text( 'brgy_rep_percent', number_format(count($barangay_psgc)/count($barangay_list)*100, 2).'%', array('class' => 'form-control', 'min'=>'0', 'disabled')) }}
					              </td>
					            </tr>
							</table>
						</div>
						
						<div class="table-responsive" style="max-height:250px; overflow-y:scroll;">
							<table class="table table-striped table-bordered table-hover">
					            <tr>
					            	<th>Barangay </th>
					                <th>No. of Participants</th>
					            </tr>
					            @foreach($barangay_list as $brgy_id => $brgy)
					            <tr>
					            	<?php $index = array_search($brgy_id, $barangay_psgc);?>
					            	@if(in_array($brgy_id, $barangay_psgc))
					            	 <td>
					                	<div class = "form-control"><input type="checkbox" id="brgycheckbox_{{$count}}" name="brgy_checkbox[]" value="{{$brgy_id}}" checked>&nbsp{{$brgy}}</div>
					                </td>
					                <td>
					                	{{ Form::input('number', 'pax[]', $barangay_pax[$index], array('class' => 'form-control', 'min'=>'0', 'id'=>'pax_'.$count)) }}
					                </td>
					            	@else
					                <td>
					                	<div class = "form-control"><input type="checkbox" id="brgycheckbox_{{$count}}" name="brgy_checkbox[]" value="{{$brgy_id}}">&nbsp{{$brgy}}</div>
					                </td>
					                <td>
					                	{{ Form::input('number', 'pax[]', '0', array('class' => 'form-control', 'min'=>'0', 'id'=>'pax_'.$count, 'disabled')) }}
					                </td>
					                @endif
					                <?php $count++ ?>
								</tr>
								@endforeach
							</table>
						</div>
					</div>
				</div>
				<div class="form-group pull-right">
								<label>Save as draft</label>
					<input type="checkbox" name="is_draft" {{ $training->is_draft==1 ? 'checked': '' }}>
				</div>
				<div class="hidden" id="count">{{ $count }}</div>												
									
				<!-- {{ Form::submit('Save', array('class' => 'btn btn-primary')) }} -->
				<div class="col-md-12 ">
									<div class="form-group pull-right">
						                <button class="btn btn-primary" id="save">
											<i class="fa fa-save"></i>
											Submit 
										</button>
		                 				<a class="btn btn-default" href="{{ URL::to('mun_trainings') }}">Cancel</a>		
									</div>
								</div>
								<div class="clearfix"></div>
                <!-- <a class="btn bg-navy" href="{{ URL::to('mun_trainings') }}">Close</a> -->
				{{ Form::close() }}
			</div>
		</div>
		<script>
			$(document).ready(function(){
			 function trunc (i)
                    {
                        var j = Math.round(i * 100);
                        return Math.floor(j / 100) + (j % 100 > 0 ? "." + p(j % 100) : "");
                    }
			//when the Add Filed button is clicked
			  $('.dates').datetimepicker({
                    pick12HourFormat: false,
                    pickTime:false
              });
                        	$("input.start_date").blur(function(){
                            				var elem = $(this);
                            				var actual_start = elem.val();



                            });

            $('input.start_date').change(function(){
                            var now  = $(this).val();
                            var then =  $("input.end_date").val();
                            var  start = moment(moment(then),"MM/DD/YYYY")
                            var end = moment(moment(now),"MM/DD/YYYY")
                            var second = 1000, minute = 60 * second, hour = 60 * minute, day = 24 * hour;
                            var sec = new Date(end).getTime() - new Date(start).getTime();
                            var totalRemainingHours = trunc(Math.ceil(sec / day) + 1);
                $('.duration').val(totalRemainingHours);
            });
             $('input.end_date').change(function(){
                            var now  = $("input.start_date").val();
                            var then = $(this).val();
                            var  start = moment(moment(now),"MM/DD/YYYY")
                            var end = moment(moment(then),"MM/DD/YYYY")
                            var second = 1000, minute = 60 * second, hour = 60 * minute, day = 24 * hour;
                            var sec = new Date(end).getTime() - new Date(start).getTime();
                            var totalRemainingHours = trunc(Math.ceil(sec / day) + 1);

                            $('.duration').val(totalRemainingHours);
                        });

			var count = $("#count").text();
			
			$("#add").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items").append('<div class="input-group"><div class="form-inline"><input type="text" class="form-control" name="trainor_name[]" onchange="checkduplicate(this)" placeholder="Trainor Name" id="'+count+'" required><input type="text" class="form-control" name="designation[]" placeholder="Designation" id="designation:'+count+'" onchange="checkduplicate(this)" required><input type="text" class="form-control" name="topics_discussed[]" placeholder="Topic" id="topic:'+count+'" onchange="checkduplicate(this)" required></div><div class="input-group-addon"><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div><br></div>');
				count++;
			});

			$("body").on("click", ".delete", function (e) {
				$(this).parent("div").parent("div").remove();
			});
			
			
			$("input[name='no_atnmale']").change(function (e) {
				$("input[name='no_ipmale']").attr('max', $("input[name='no_atnmale']").val());
				$("input[name='no_slpmale']").attr('max', $("input[name='no_atnmale']").val());
				$("input[name='total_atn']").val(parseInt($("input[name='no_atnmale']").val())+parseInt($("input[name='no_atnfemale']").val()));
			});
			
			$("input[name='no_atnfemale']").change(function (e) {
				$("input[name='no_ipfemale']").attr('max', $("input[name='no_atnfemale']").val());
				$("input[name='no_slpfemale']").attr('max', $("input[name='no_atnfemale']").val());
				$("input[name='total_atn']").val(parseInt($("input[name='no_atnmale']").val())+parseInt($("input[name='no_atnfemale']").val()));
			});
			
			$("input[name='no_ipmale']").change(function (e) {
				$("input[name='total_ip']").val(parseInt($("input[name='no_ipmale']").val())+parseInt($("input[name='no_ipfemale']").val()));
			});
			
			$("input[name='no_ipfemale']").change(function (e) {
				$("input[name='total_ip']").val(parseInt($("input[name='no_ipmale']").val())+parseInt($("input[name='no_ipfemale']").val()));
			});
			
			$("input[name='no_slpmale']").change(function (e) {
				$("input[name='total_slp']").val(parseInt($("input[name='no_slpmale']").val())+parseInt($("input[name='no_slpfemale']").val()));
			});
			
			$("input[name='no_slpfemale']").change(function (e) {
				$("input[name='total_slp']").val(parseInt($("input[name='no_slpmale']").val())+parseInt($("input[name='no_slpfemale']").val()));
			});
			
			$("input[name='brgy_checkbox[]']").change(function (e) {
				
				$("input[name='brgy_rep_count']").val($("input[name='brgy_checkbox[]']:checked").length);
				$("input[name='brgy_rep_percent']").val((parseInt($("input[name='brgy_checkbox[]']:checked").length)/parseInt($("input[name='brgy_count']").val())*100).toFixed(2) + "%");
				
				

				if($("#"+this.id).is(":checked")){
					$("#pax_"+this.id.substr(this.id.indexOf("_") + 1)).removeAttr('disabled');
				}
				else{
					$("#pax_"+this.id.substr(this.id.indexOf("_") + 1)).attr('disabled','disabled');
					$("#pax_"+this.id.substr(this.id.indexOf("_") + 1)).val('0');	
				}

			});
			
			});
			
			function checkduplicate(e){
				
				var count = -1;
				
				var id = e.id;
				id = id.substr(id.indexOf(":") + 1);
				var name = document.getElementById(id);
				var designation = document.getElementById("designation:"+id).value;
				var topic = document.getElementById("topic:"+id).value;
				var names = document.getElementsByName('trainor_name[]');
				var designations = document.getElementsByName('designation[]');
				var topics = document.getElementsByName('topics_discussed[]');
				for (i=0; i<names.length; i++) {
    				if(name.value === names[i].value && designation === designations[i].value && topic === topics[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					name.setCustomValidity("Duplicate Entry not allowed");
				}else{
					name.setCustomValidity("");
				}
				
			}
		</script>
@stop