@extends('layouts.default')

@section('username')
	{{ $username }}
@stop


@section('content')
	    <div class="row">
		    <div class="col-md-12">
				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
				<a href="{{ URL::to('mun_trainings') }}" class="btn btn-default" >Go back</a>
				

        		
				{{ Form::open(array('url' => 'mun_trainings/submit_participant', 'method' => 'POST', 'role' => 'form')) }}
				{{ Form::reset('Reset', array('class' => 'btn btn-danger pull-right ', 'id' => 'reset','style'=>'margin:0 10px')) }}
				{{ Form::submit('Add Participant', array('class' => 'btn btn-primary pull-right','style'=>'margin:0 10px')) }}
				<button type="button" class="btn btn-success pull-right"  style="margin:0 10px" data-toggle="modal" data-target="#myModal">
        			Volunteer’s List
        		</button>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-6">
						<div class="hidden">
							{{ Form::text('beneficiary_id', Input::old('beneficiary_id'), array('class' => 'form-control', 'id' => 'beneficiary_id')) }}
							{{ Form::text('is_volunteer', Input::old('is_volunteer'), array('class' => 'form-control', 'id' => 'is_volunteer')) }}
						</div> 
						<div class="form-group">
							{{ Form::label('barangay', 'Barangay') }}
							<span class="tag hidden">(Volunteer)</span>
							{{ Form::select('psgc_id', $brgy_list, Input::old('barangay'), array('class' => 'form-control', 'required', 'id' => 'barangay')) }}
						</div>
						<div class="form-group">
							{{ Form::label('lastname', 'Last Name') }}
							{{ Form::text('lastname', Input::old('lastname'), array('class' => 'form-control', 'required')) }}
						</div>

						<div class="form-group">
							{{ Form::label('firstname', 'First Name') }}
							{{ Form::text('firstname', Input::old('firstname'), array('class' => 'form-control', 'required')) }}
						</div>
						<div class="form-group">
							{{ Form::label('midlename', 'Middle Initial') }}
							{{ Form::text('middlename', Input::old('firstname'), array('class' => 'form-control','maxlength'=>'4','id' => 'middlename','maxlength'=>'4')) }}
						</div>

				
						<div class="form-group">
							{{ Form::label('age', 'Age') }}
							{{ Form::number('age', Input::old('age'), array('class' => 'form-control', 'id' => 'age','min'=>'15','max'=>'99', 'required')) }}
                        </div>
						<div class="form-group">
							{{ Form::label('Sex', 'Sex') }}
							{{ Form::select('sex', array(''=>'Select Gender','Male' => 'Male', 'Female' => 'Female'), Input::old('sex'), array('class' => 'form-control', 'id' => 'sex', 'required')) }}

						</div>

						<div class="form-group">
                            {{ Form::label('sector', 'Sector') }}
                            {{ Form::text('sector_text', Input::old('sector'), array('class' => 'form-control hidden','id'=>'sectors')) }}
                            <div class="table-responsive sector-table">
                                <table class="table table-striped table-bordered table-hover ">
                                    @foreach($sector_list as $sector)

                                            <div class = "form-control"><input type="checkbox" name="sector[]" value="{{$sector}}">&nbsp{{$sector}}</div>

                                    @endforeach
                                </table>
                            </div>
                        </div>
						<div class="form-inline">
							{{ Form::checkbox('is_ip', '1', '' , array( 'id' => 'is_ip', '')) }} 
							{{ Form::label('is_ip', 'IP') }}
						</div>
						<div class="form-inline">
							{{ Form::checkbox('is_pantawid', '1', '' , array( 'id' => 'is_pantawid', '')) }} 
							{{ Form::label('is_pantawid', 'Pantawid') }}
						</div>
						<div class="form-inline">
							{{ Form::checkbox('is_slp', '1', '' , array( 'id' => 'is_slp', '')) }} 
							{{ Form::label('is_slp', 'SLP') }}
						</div>					
						<div class="form-inline">
							{{ Form::checkbox('is_lguofficial', '1', '' , array( 'id' => 'is_lguofficial','')) }} 
							{{ Form::label('is_lguofficial', 'LGU Official') }}
						</div>		
						{{--<div class="form-inline">--}}
							{{--{{ Form::checkbox('is_volunteer', '1', '' , array( 'id' => 'is_volunteer', '')) }} --}}
							{{--{{ Form::label('is_volunteer', 'Volunteer') }}--}}
						{{--</div>					--}}
						
				
				</div>
				{{ Form::hidden('reference_no', $id)}}
				
				
				{{ Form::close() }}
				
				
			</div>
		</div>	
		
		
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Volunteers List</h4>
                    </div>
                    <div class="modal-body">
                    	{{ Datatable::table()->addColumn( 'firstname','lastname','middle initial','birthdate','option')->setUrl(URL::to('api/volunteer?cycle_id='.$cycle_id.'&program_id='.$program_id))->render(); }}
					</div>
	            </div>
            </div>
        </div>
		
		<script>
		$(document).ready(function(){
				$('#dataTables-example').dataTable();
				
				$.fn.use_volunteer = function(vol){
								 $('.tag').removeClass('hidden');
					$(".form-control").attr('readonly', 'readonly');
					$("#lastname").attr('value', vol.find(".lastname").text());
					$("#firstname").attr('value', vol.find(".firstname").text());
					$("#middlename").attr('value', vol.find(".middlename").text());
					$("#sex").val(
							function(){
								var sex = vol.find(".sex").text();
								if(sex[0]=="F"){
									return "Female";
								}else{
									return "Male";
								}
							}
						);
					$("#age").attr('value',  moment().diff(vol.find(".birthdate").text(), 'years'));
//                    $("#sex").attr('disabled','disabled');
					$("#is_volunteer").attr('value', '1');
					$("#beneficiary_id").attr('value', vol.find(".beneficiary_id").text());
					
					$('#no_children').attr('value',vol.find(".no_children").text());
					$('#civil_status').attr('value',vol.find(".civil_status").text());
					$('#address').attr('value',vol.find(".address").text());
					$('#contact_no').attr('value',vol.find(".contact_no").text());
					$('#educ_attainment').attr('value',vol.find(".educ_attainment").text());
					$('#occupation').attr('value',vol.find(".occupation").text());
										$('#sectors').val(vol.find('.sectors').text());
                    if(vol.find('.sectors').text()!=''){
                        $('.sector-table').addClass('hidden');
                        $('#sectors').removeClass('hidden');
                    }
					vol.find('.is_ip').text() == 1?$('#is_ip').prop('checked','checked') : $('#is_ip').removeAttr('checked')
					vol.find('.is_ipleader').text() == 1?$('#is_ipleader').prop('checked','checked') : $('#is_ipleader').removeAttr('checked')
					vol.find('.is_pantawid').text() == 1?$('#is_pantawid').prop('checked','checked') : $('#is_pantawid').removeAttr('checked')
					vol.find('.is_slp').text() == 1?$('#is_slp').prop('checked','checked') : $('#is_slp').removeAttr('checked')
					vol.find('.is_lguofficial').text() == 1?$('#is_lguofficial').prop('checked','checked') : $('#is_lguofficial').removeAttr('checked')

					$('#barangay').val(vol.find(".psgc_id").text());
					$("#myModal").modal('toggle');

					
//					setAge();
				};
				
				$("#reset").click(function(){
					$(".form-control").attr('value', "");
					$(".form-control").removeAttr('readonly');
					$('.sector-table').removeClass('hidden');
                    $('#sectors').addClass('hidden');
				});
				
				$("#birthdate").change(function(){
//					setAge();
					
				});
				$("#is_ip").change(function(){
					if($("#is_ip").is(":checked")){
						$("#is_ipleader").removeAttr('disabled');
					}
					else{
						$("#is_ipleader").attr('disabled','disabled');
						$("#is_ipleader").prop('checked', false);	
					}
				});
			});
		
		function setAge(){
				var today = new Date();
				var birthdate = new Date(document.getElementById("birthdate").value);
				var age = today.getFullYear() - birthdate.getFullYear();
    			var m = today.getMonth() - birthdate.getMonth();
    			if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
        			age--;
    			}
    			document.getElementById("age").value = age;
    			
    			e = document.getElementById("birthdate");
    			if(age < 15){	
					e.setCustomValidity("Age must be 15+");
				}else{
					e.setCustomValidity("");
				}
			}
		</script>
@stop