@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
			<div class="col-md-12">
				<h4>Add MLPRAP</h4>
			</div>
		</div>
	  
	    <div class="row">
		    <div class="col-md-12">
				
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif

				
				{{ Form::open(array('route' => 'mlprap.store', 'method' => 'POST')) }}
				{{ Form::token() }}

				<div class="col-md-4 left-border">

					<div class="form-group">

						{{ Form::label('year', 'From Year') }}
						{{ Form::number('for_year', '', array('class' => 'form-control from', 'required','min'=>'2013')) }} <br>
						{{ Form::label('year', 'To Year') }}
                        {{ Form::number('to_year', '', array('class' => 'form-control to', 'required','min'=>'2013')) }} <br>



						{{ Form::label('date_prepared', 'Date Prepared') }}
						<div class='input-group date' id='datetimepicker10'>
			                {{ Form::text('date_prepared', '', array('class' => 'form-control','required')) }} <br>
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>

				            
					</div>

					{{ Form::submit('Add', array('class' => 'btn btn-primary')) }}				
					{{ HTML::linkRoute('mlprap.index', "Cancel", array(), array('class' => 'btn btn-default')) }}					

				</div>
					
				{{ Form::close() }}
				
			</div>
		</div>
		
        <script>
            $('.to').change(function(){
                var el = $(this).val();
                $('.from').attr('max',el);
            });
             $('.from').change(function(){
                    var el = $(this).val();
                    $('.to').attr('min',el);
                });
        </script>
		
		
		
@stop



<?php 

// <!--  Script for dependent dropdowns   -->
// <script>
// $(document).ready(function($){
// 	$('#province').change(function(){

// 		var province = $('#province option:selected').text();

// 		$.get("{{ route('mlprap.dropdown')}}", { option: province }, 
// 			function(data) {

// 				var numbers = $('#municipality');
// 					numbers.empty();

// 				$.each(data, function(key, value) {   
// 					numbers.append($("<option></option>").attr("value",key).text(value)); 
// 				});
//         	}
//         );
//     });
// });
// </script>

// {{ Form::label('province', 'Province') }} <br>
// {{ Form::select('province', array('0' => 'Select Province') + $province_names, 's', array('class' => 'form-control', 'required')) }} <br>

// {{ Form::label('municipality', 'Municipality') }} <br>
// {{ Form::select('municipality', array(), 's', array('class' => 'form-control', 'required')) }} <br>
?>