@extends('layouts.default')

@section('username')
	
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')

    <!-- /. ROW  -->
    <hr />
    <div class="row">
	    <div class="col-md-12">
			<!-- Advanced Tables -->
			
	    	<div class="panel panel-default">
	    		<div class="panel-heading">
	    			{{ $title }}
	    			 @if($position->position=='ACT')
                                 			    <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                                 			    <span class="hidden module">mlprap</span>
                                    @endif 
			        {{ HTML::linkRoute('mlprap.create', 'Add New', array(), array('class'=>'btn btn-primary pull-right')) }} 
			        <div class="clearfix"></div>
	    		</div>

				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
                                    <th>
                                      @if($position->position!='encoder')
                                                Select all
                                                 <input type="checkbox" id="selecctall"/>
                                      @endif
                                     </th>
									<th>Year</th>
									<th>Date Prepared</th>
									<th>Details</th>
									
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($mlpraps as  $mlprap)
									<tr>
									    <td>
									     @if($position->position!='encoder')
                                           @if($mlprap->is_draft==0)
                                             {{ $reference->approval_status($mlprap->action_id , 'ACT') }}
                                           @else
                                               <span class="label label-warning">draft</span>
                                           @endif
                                         @endif
									    </td>
										<td>
											{{ $mlprap->for_year }}
										</td>
										<td>
											{{ date('m/d/Y', strtotime($mlprap->date_prepared))}} 
										</td>
										<td>
											{{ HTML::linkRoute('mlprapdetails.show', 'View Action Plans', $mlprap->action_id, array('class'=>'btn btn-success view')) }}
										</td>

										<td>
	    								@if( !is_review($mlprap->action_id) )
											{{ HTML::linkRoute('mlprap.edit', 'Edit', $mlprap->action_id, array('class'=>'btn btn-info')) }}
										@endif
										</td>

										<td>
	    								@if( !is_review($mlprap->action_id) )
											
											{{ HTML::linkRoute('mlprap.delete', 'Delete', $mlprap->action_id, array('class'=>'btn btn-danger',
															'onclick'=>"return confirm('Are you sure you want to delete this?');")) }}
										@endif
										</td>

										
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>

@stop