@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
			<div class="col-md-12">
				<h4>Edit MLPRAP</h4>
			</div>
		</div>
	  
	    <div class="row">
		    <div class="col-md-12">
				
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
				{{ Form::open(array('route' => array('mlprap.update'), 'method' => 'PUT')) }}
				{{ Form::token() }}

				<div class="col-md-4 left-border">

					<div class="form-group">
						{{--@if( $mlprapdetails_count > 0)--}}
						    {{--{{ Form::label('year', 'Year') }}--}}
							{{--{{ Form::text('for_year', $years[ $year_key ], array('class' => 'form-control', 'required', 'readonly')) }} <br>--}}
							{{--{{ Form::label('year', 'To Year') }}--}}
                            {{--{{ Form::select('to_year', [''=>'Select Year']+$years, 's', array('class' => 'form-control', 'required','readonly')) }} <br>--}}

						{{--@else--}}
							{{--{{ Form::label('year', 'Year') }}--}}
							{{--{{ Form::select('year', [''=>'Select Year']+$years, $year_key, array('class' => 'form-control', 'required')) }} <br>--}}
							{{--{{ Form::label('year', 'To Year') }}--}}
                             {{--{{ Form::select('to_year', [''=>'Select Year']+$years, $to_year, array('class' => 'form-control', 'required','readonly')) }} <br>--}}

						{{--@endif--}}
								{{ Form::label('year', 'From Year') }}
                                {{ Form::number('for_year', $mlprap->for_year, array('class' => 'form-control from', 'required','min'=>'2013')) }} <br>
                                {{ Form::label('year', 'To Year') }}
                                {{ Form::number('to_year', $mlprap->to_year, array('class' => 'form-control to', 'required','min'=>'2013')) }} <br>

						{{ Form::label('date_prepared', 'Date Prepared') }}
						<div class='input-group date' id='datetimepicker10'>
			                {{ Form::text('date_prepared', date('m/d/Y', strtotime($mlprap->date_prepared)), array('class' => 'form-control','required')) }} <br>
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>  
					</div>

					{{ Form::hidden('mlprap_id', $mlprap->action_id) }}
					{{ Form::hidden('mlprapdetails_count', $mlprapdetails_count) }}

					<div class="box-body">
			 			
			 				<div class="form-group">
								<label>Save as draft</label>
							  <input type="checkbox" name="is_draft" {{ $mlprap->is_draft==1 ? 'checked': '' }}>
							</div>


					
					{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}				
					{{ HTML::linkRoute('mlprap.index', "Cancel", array(), array('class' => 'btn btn-default')) }}					

				</div>
					
				{{ Form::close() }}
		
@stop