@extends('layouts.default')

@section('content')


<a class="btn btn-default" href="{{ URL::to('spi-erslist',$spi_erslist['project_id']) }}">Go Back</a>
<div class="row">
    <div class="col-md-12">
    	
		<div class="panel panel-default">

    		<div class="panel-heading">
    			Details

    		</div>
			<div class="panel-body">
				<form role="form" id="spi-adders_modal_form">
					
					
					<div class="form-group col-xs-2 col-sm-2">
						<label for="in-datestart_cont">Period Start Date</label>
		                <input class="form-control" required="required" type="text" 
		                	   id="in-datestart" name="in-datestart" placeholder="When is/was the starting date?" 
		                	   value="{{date("m/d/Y", strtotime($spi_erslist->date_start))}}">
					</div>  
					
					<div class="form-group col-xs-2 col-sm-2">
						<label for="in-dateending_cont">Period End Date</label>
		                <input class="form-control" required="required" type="text" 
		                	   id="in-dateending" name="in-dateending" placeholder="When is/was the end date?" 
		                	   value="{{date("m/d/Y", strtotime($spi_erslist->date_ending))}}">
					</div>  

					<div class="form-group col-xs-2 col-md-2">
						<label for="in-datereport_cont">Date of Reporting</label>
		                <input class="form-control" required="required" type="text" 
		                	   id="in-datereport" name="in-datereport" placeholder="When is/was the reporting date?" 
		                	   value="{{date("m/d/Y", strtotime($spi_erslist->date_reporting))}}">
					</div>  

					<div class="form-group col-xs-12 col-md-2">
						<label for="in-datereport_cont">Days</label>
						<?php
									$date_start  = strtotime($spi_erslist->date_start);
									$date_ending = strtotime($spi_erslist->date_ending);
									$date_diff   = ($date_ending - $date_start)/(60*60*24) + 1;
		                echo "<input class='form-control' required='required' type='text' readonly='readonly' 
		                	   id='in-days' name='in-days'  
		                	   value= $date_diff >";
									
						?>
					</div>
				</form>
			</div>
		</div>
		<div class="panel panel-default">
    		<div class="panel-heading">
    			<h5 class="col-xs-6 col-md-6" style="padding-left: 0;">Workers</h5>
					<a id="spi-addnewworker_modal_show" class="btn btn-primary pull-right" style="margin-left: 5px;" data-value="{{ URL::to('spi-worker/generateid')}}">
						<i class="fa fa-plus"></i> Add New Worker
					</a>

					<a id="spi-addworker_modal_show" href="" class="btn btn-primary pull-right">
						<i class="glyphicon glyphicon-plus"></i> Add Existing Worker
					</a>
				<div class="clearfix"></div>
    		</div>
			<div class="panel-body">
				@if ($create_status !== null)
					@if ($create_status === TRUE)
						<div class="alert alert-success" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-ok"></i> New Worker successfully added!</strong>
						</div>
					@elseif ($create_status === FALSE)
						<div class="alert alert-danger" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-remove"></i> Worker can not be added</strong> 
							Please contact DSWD about this incident.
						</div>
					@endif
				@endif
				@if ($destroy_status !== null)
					@if ($destroy_status === TRUE)
						<div class="alert alert-success" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-ok"></i> Worker successfully removed!</strong>
						</div>
					@elseif ($destroy_status === FALSE)
						<div class="alert alert-danger" role="alert" style="margin-left: 0">
							<strong><i class="glyphicon glyphicon-remove"></i> Worker can not be removed</strong> 
							Please contact DSWD about this incident.
						</div>
					@endif
				@endif
						<div id="update_worker_alert" class="alert alert-success" role="alert" style="margin-left: 0; display: none">
							<strong><i class="glyphicon glyphicon-ok"></i> Worker successfully updated!</strong>
						</div>
				<table class="table table-striped table-bordered dataTable" id="dataTables-example" aria-describedby="dataTables-example_info">
					<thead>
						<tr>
							<th id="dataTables-example-action" style="width: 10%; text-align: center;">Action</th>
							<th style="width: 20%;">Worker</th>
							<th style="width: 15%;">Work</th>
							<th style="width: 10%;">Total Labor</th>
							<th style="width: 10%;">Plan - Cash</th>
							<th style="width: 10%;">Plan - Bayanihan</th>
							<th style="width: 10%;">Actual - Cash</th>
							<th style="width: 15%;">Actual - Bayanihan</th>
						</tr>
					</thead>
					
					<tbody>
					<?php $current_workers = array(); ?>
						@foreach($workers as $worker)
						<?php $current_workers[$worker->worker_id] = true; ?>
						<tr data-rowid="{{$worker->id}}">
							<td style="text-align: center;min-width: 131px;">
								<a href="{{ URL::to('spi-ers/'.$worker->id) }}" class="btn btn-xs btn-info spi-ers_showdetails">
									<i class="glyphicon glyphicon-chevron-down"></i>Edit
								</a>
								<a href="" class="btn btn-xs btn-info spi-ers_hidedetails" style="display: none;">
									<i class="glyphicon glyphicon-chevron-up"></i>Edit
								</a>
								<a href="{{ URL::to('spi-ers/'.$worker->id) }}" 
									class="btn btn-xs btn-danger spi-ersworker_modal-del_show">
									<i class="glyphicon glyphicon-trash"></i>Delete
								</a>
							</td>
							<td>
								{{$worker->firstname . ' ' . $worker->lastname}}
							</td>
							<td class="work" data-value="{{  $worker->nature_work  }}">
								{{$worker->current_work}}
							</td>
							<?php
								$total = round( ( $worker->rate_hour * $worker->work_hours ) + ( $worker->rate_day * $worker->work_days),2);
							?>
							<td class="total" data-value="{{ $total }}" 
								data-ratehour="{{ $worker->rate_hour }}" data-rateday="{{ $worker->rate_day }}"
								data-workhours="{{ empty($worker->work_hours)? 0 : $worker->work_hours }}" data-workdays="{{ $worker->work_days }}">
								<input type="text"  readonly="readonly" disabled="disabled" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" 
									value="{{ currency_format($total) }}">
									
							</td>
							<td class="plan_cash" data-value="{{ $worker->plan_cash }}">
								<input type="text"  readonly="readonly" disabled="disabled" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" 
									value="{{ currency_format($worker->plan_cash) }}">
							</td>
							<td  class="plan_lcc" data-value="{{ $worker->plan_lcc }}">
								<input type="text"   readonly="readonly" disabled="disabled" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" 
									value="{{ currency_format($worker->plan_lcc) }}">
							</td>
							<td class="actual_cash" data-value="{{ $worker->actual_cash }}">
								<input type="text"  readonly="readonly" disabled="disabled" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01"
									value="{{ currency_format($worker->actual_cash) }}">
							</td>
							<td class="actual_lcc" data-value="{{  $worker->actual_lcc  }}">
								<input type="text" readonly="readonly" disabled="disabled" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" 
									value="{{ currency_format($worker->actual_lcc) }}">
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			
				{{ $workers->links() }}
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="spi-addworker_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">{{$spi_profile_name }} <small>Workers</small></h4>
			</div>
			<div class="modal-body">
				<table class="table table-striped table-bordered table-hover dataTable" id="dataTables-example2" aria-describedby="dataTables-example_info">
					<thead>
						<tr>
							<th style="width: 5%;"></th>
							<th style="width: 10%;">Worker ID</th>
							<th style="width: 35%;">Name</th>
							<th style="width: 10%;">Gender</th>
							<th style="width: 10%;">Age</th>
							<th style="width: 10%;">Work</th>
							<th style="width: 10%;">Hourly Rate</th>
							<th style="width: 10%;">Daily Rate</th>
						</tr>
					</thead>
					<tbody>
						@foreach($municipal_workers as $worker)
						<tr data-rowid="{{$worker->worker_id}}">
							<td style="text-align: center; ">
								<input type="checkbox" class="in-worker_ids" name="in-worker_ids[]" value="{{$worker->worker_id}}"
									<?php echo (array_key_exists($worker->worker_id, $current_workers)) ? 'disabled': ''; ?> >
							</td>
							<td>
								{{$worker->worker_id}}
							</td>
							<td>
								{{$worker->firstname . ' ' . $worker->middlename . ' ' . $worker->lastname}}
							</td>
							<td>
								{{($worker->sex == 0) ? 'Male' : 'Female'}}
							</td>
							<td>
								{{$worker->age}}
							</td>
							<td>
								{{$worker->nature_work}}
							</td>
							<td>
								<?php
									setlocale(LC_MONETARY, "es_ES");
									$locale = localeconv();
									echo $locale['currency_symbol'], number_format($worker->rate_hour, 2, $locale['decimal_point'], $locale['thousands_sep']);
								?>
							</td>
							<td>
								<?php
									setlocale(LC_MONETARY, "es_ES");
									$locale = localeconv();
									echo $locale['currency_symbol'], number_format($worker->rate_day, 2, $locale['decimal_point'], $locale['thousands_sep']);
								?>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<input id="record_id" type="hidden" name="record_id" value="{{$record_id}}">
				<a id="spi-ers_addersrecords" class="btn btn-success" href="{{ URL::to('spi-ers/')}}">
					<i class="glyphicon glyphicon-save"></i> Add to ERS
				</a>

				<a class="btn btn-link pull-left" href="{{ URL::to('spi-subworkers/'.$project_id) }}">
					 Cant find Worker? Assign Workers to Project <i class="glyphicon glyphicon-new-window"></i>
				</a>
				<a data-dismiss="modal" class="btn bg-navy" href="">Close</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="spi-ersworker_modal-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
				<form>
					<input type="hidden" id="del-id" name="del-id" value="">
					<p class="col-xs-2" style="text-align: right;">
						<i class="glyphicon glyphicon-question-sign" style="color: #f4543c; font-size: 30px;"></i>
					</p>
					<p class="col-xs-10" style="font-size: 15px; padding-top: 5px; padding-bottom: 5px;">Are you sure you want to remove 
						 this worker?</p>
					<div class="clearfix"></div>
				</form>
			</div>
			<div class="modal-footer">
				<a id="spi-ersworker_modal_delform_submit" href="" class="btn btn-danger">Yes</a>
				<a href="" class="btn bg-navy" data-dismiss="modal">No</a>
			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="spi-addnewworker_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #f5f5f5; border-radius: 5px;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">New Worker for this Project</h4>
			</div>
			<div class="modal-body">
				<form id="spi-addworker_modal_formx">
					<div class="form-group col-xs-12 col-md-12">
						<label for="in-workerid">Worker ID</label>
		                <input class="form-control" required="required" type="text" 
		                	   id="in-workerid" name="in-workerid" value="" disabled="disabled"> 
			                <!-- 
						<div class='input-group' id='in-workerid_cont'>
			                <input class="form-control" required="required" type="text" 
			                	   id="in-workerid" name="in-workerid" value="" disabled="disabled"> 
			                <br>
			                <span class="input-group-btn">
			                	<button id="generate_id" class="btn btn-default" style="cursor: pointer;" data-value="{{ URL::to('spi-worker/generateid')}}">Generate</button>
			                </span>
			            </div>
			                //-->
					</div>  

					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-firstname">First Name</label>
						<input type="text" class="form-control" id="in-firstname">
					</div>  
					
					<div class="form-group  col-xs-4 col-sm-4">
						<label for="in-lastname">Last Name</label>
						<input type="text" class="form-control" id="in-lastname">
					</div>  

					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-middlename">Middle Name</label>
						<input type="text" class="form-control" id="in-middlename">
					</div>
					
					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-sex">Sex</label>
						<select id="in-sex" class="form-control">
						  <option value="">	</option>
						  <option value="0">Male</option>
						  <option value="1">Female</option>
						</select>
					</div>  
					
					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-age">Age</label>
						<input type="number" class="form-control" id="in-age">
					</div>  
					
					<div class="form-group col-xs-4 col-md-4">
						<label for="in-datebday_cont">Birthday</label>
						<div class='input-group date' id='in-datebday_cont'>
			                <input class="form-control" required="required" type="text" 
			                	   id="in-birthday" name="in-birthday" value=""> <br>
			                <span class="input-group-addon"><span class="fa fa-calendar">
			                      </span>
			                </span>
			            </div>
					</div>  

					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-natureofwork">Nature of Work</label>
						{{--<select id="in-natureofwork" class="form-control">--}}
						  {{--<option value="">	</option>--}}
						  {{--<option value="Foreman/Project Supervisor">Foreman/Project Supervisor</option>--}}
						  {{--<option value="Carpenter">Carpenter</option>--}}
						  {{--<option value="Mason">Mason</option>--}}
						  {{--<option value="Plumber">Plumber</option>--}}
						  {{--<option value="Electrician">Electrician</option>--}}
						  {{--<option value="Laborer/Helper">Laborer/Helper</option>--}}
						  {{--<option value="Other Skilled">Other Skilled</option>--}}
						  {{--<option value="Other Unskilled">Other Unskilled</option>--}}
						{{--</select>--}}

						{{ Form::select('',$natureofworklists,'',['class'=>'form-control','id'=>'in-natureofwork']) }}

					</div>
					<div class="form-group col-xs-4 col-sm-4">
							<label for="in-natureofwork">Type of Work</label>
							{{ Form::select('',$typeofworklists,'',['class'=>'form-control','id'=>'in-typework','readonly','style'=>'  text-transform: capitalize;'] ) }}
							{{--<select class="form-control" type="text" id="in-typework"/>--}}
					</div>

					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-hourrate">Hourly Rate</label>
						{{--<input type="number" class="form-control" id="in-hourrate">--}}
						{{ Form::number('','',['class'=>'form-control','id'=>'in-hourrate','step'=>'any']) }}
					</div>

					<div class="form-group col-xs-4 col-sm-4">
						<label for="in-dailyrate">Daily Rate</label>
						{{--<input type="number" class="form-control" id="in-dailyrate">--}}

						{{ Form::number('','',['class'=>'form-control','id'=>'in-dailyrate','step'=>'any']) }}
					</div>  

				    <div class="form-group">
				    	<div class="col-xs-12 col-md-12">
				      		<div class="">

				        		{{ Form::checkbox('in-pantawid',0,0,['id'=>'in-pantawid']) }}
				        		<label>
				        			Pantawid Pamilya
				        		</label>
				      		</div>
				      		<div class="">
				      		    {{ Form::checkbox('in-ip',0,0,['id'=>'in-ip']) }}
				        		<label>
				        			IP
				        		</label>
				      		</div>
				      		<div class="">
				      		    {{ Form::checkbox('in-slp',0,0,['id'=>'in-slp']) }}
                                <label>
                                    SLP
                                </label>
                            </div>
				    	</div>
				  	</div>
					<div class="form-group">
				    	<div class="col-xs-6 col-md-6">

				    	</div>
				  	</div>

					<div class="clearfix"></div>
				</form>
			</div>
		   <div class="modal-footer">
		   		<input type="hidden" class="form-control" id="in-projectid" value="{{$project_id}}">
				<button id="spi-addnewworker_modal_form_submit" class="btn btn-success confirm" data-value="{{ URL::to('spi-subworkers/' . $project_id)}}">Submit</button>
				<button class="btn closes bg-navy" data-dismiss="modal">Close</button>
		   </div>
		</div>
	</div>
</div>
@include('modals.ersedit')
<script>
	{{--function format (nTr, url) {--}}
	    {{--var table_html = '' +--}}
	    {{--'<table class="table further_details" border="0" style="padding-left:50px;" data-rowid="'+ nTr.data('rowid')+ '">'+--}}
	    	{{--'<thead> '+--}}
	    		{{--'<tr><th colspan="4">Worker Rate and Attendance (*Formula : Daily rate * Base work + hourly rate ) '+--}}
	    			{{--'<a href="'+ url +'" class="btn btn-sm btn-primary pull-right spi-ers_editersrecords">'+--}}
						{{--'<i class="glyphicon glyphicon-save"></i> Apply Changes'+--}}
					{{--'</a>'+--}}
	    		{{--'</th></tr>'+--}}
	    	{{--'</thead>'+--}}
	    	{{--'<tbody>'+--}}
		        {{--'<tr>'+--}}
		            {{--'<th style="width: 15%;">Work</th>'+--}}
                    {{--'<td>{{ Form::select('',$natureofworklists,'',['class'=>'form-control']--}}
                    {{--) }} </td>'+--}}
		            {{--'<th style="width: 15%;">Daily Rate:</th>'+--}}
		            {{--'<td><input type="number" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-dailyrate in-currency" '+--}}
		            	{{--'value="' + nTr.find('td.total').data('rateday') + '">' +--}}
		            {{--'</td>'+--}}
		            {{--'<th style="width: 15%;">Days Worked:</th>'+--}}
		            {{--'<td><input type="number" value="0" style="width: 90%;" min=0 step="0.01" class="in-daysworked in-time" '+--}}
		            	{{--'value="' + nTr.find('td.total').data('workdays') + '">' +--}}
		            {{--'</td>'+--}}
		        {{--'</tr>'+--}}
		        {{--'<tr>'+--}}
		            {{--'<th style="width: 15%;">Hourly Rate:</th>'+--}}
		            {{--'<td><input type="number" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-hourlyrate in-currency" '+--}}
		            	{{--'value="' + nTr.find('td.total').data('ratehour') + '">' +--}}
		            {{--'</td>'+--}}
		            {{--'<th style="width: 15%;">Hours Worked:</th>'+--}}
		            {{--'<td><input type="number" value="0" style="width: 90%;" min=0 step="0.01" class="in-hoursworked in-time" '+--}}
		            	{{--'value="' + nTr.find('td.total').data('workhours') + '">' +--}}
		            {{--'</td>'+--}}
		        {{--'</tr>'+--}}
				{{--'<tr>'+--}}
		            {{--'<th style="width: 15%;">Total Labor:</th>'+--}}
		            {{--'<td colspan="3"><input type="number" style="width: 95%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-total" readonly disabled '+--}}
		            	{{--'value="' + nTr.find('td.total').data('value') + '">' +--}}
		            {{--'</td>'+--}}
		        {{--'</tr>'+--}}
		        {{--'<tr>'+--}}
		            {{--'<th style="width: 15%;">Plan Cash:</th>'+--}}
		            {{--'<td><input type="number" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-plancash in-currency" '+--}}
		            	{{--'value="' + nTr.find('td.plan_cash').data('value') + '">' +--}}
		            {{--'</td>'+--}}
		            {{--'<th style="width: 15%;">Actual Cash:</th>'+--}}
		            {{--'<td><input type="number" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-actualcash in-time" '+--}}
		            	{{--'value="' + nTr.find('td.actual_cash').data('value') + '">' +--}}
		            {{--'</td>'+--}}
		        {{--'</tr>'+--}}
		        {{--'<tr>'+--}}
		            {{--'<th style="width: 15%;">Plan Bayanihan:</th>'+--}}
		            {{--'<td><input type="number" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-planbayanihan in-currency" '+--}}
		            	{{--'value="' + nTr.find('td.plan_lcc').data('value') + '">' +--}}
		            {{--'</td>'+--}}
		            {{--'<th style="width: 15%;">Actual Bayanihan:</th>'+--}}
		            {{--'<td><input type="number" style="width: 90%;" pattern="[0-9]+([\.|,][0-9]+)?" min=0 step="0.01" class="in-actualbayanihan in-time" '+--}}
		            	{{--'value="' + nTr.find('td.actual_lcc').data('value') + '">' +--}}
		            {{--'</td>'+--}}
		        {{--'</tr>'+--}}
		    {{--'</tbody>'+--}}
	    {{--'</table>';--}}

	    {{--return table_html;--}}
	{{--}--}}
    $(document).ready(function () {
	    // var $table1 = $('#dataTables-example').dataTable();
	    $('#dataTables-example2').dataTable();
	    $('#dataTables-example-action').css('width', '10%');
 /* extra Controller */


		/* A logic Controller Computes the EDIT WORKER Based on the formula given*/ 
        var computator = {
            compute : function(){
            	var totaldays = parseFloat(  ( parseFloat($('.in-dailyrate').val() ) * parseFloat( $('.in-daysworked').val() ) ) ) ;
            	var totalhours = parseFloat( $('.in-hourlyrate').val() ) * parseFloat( $('.in-hoursworked ').val() );
                $('.in-total').val( (totaldays + totalhours).toFixed(2) );
            }
        };

        var validator = {
        	plancash : function(){

        		var total =  parseFloat($('.in-total').val()); // get value of the total labor

        		var plancash = parseFloat($('.in-plancash ').val());

        		if(plancash > total){
        			alert('Plan cash is greater than the total labor');
        			$('.in-plancash ').val('')
        		}else{
        			var plan = total - plancash.toFixed(2);
	        		$('.in-planbayanihan').val(plan);
        		}
        	},
        	planbayanihan : function(){

        		var total =  parseFloat($('.in-total').val()); // get value of the total labor

        		var planbayanihan = $('.in-planbayanihan ').val();
        		if(planbayanihan > total){
        			alert('Plan Bayanihan is greater than the total labor');
        			$('.in-planbayanihan ').val('')
        		}else{
        			$('.in-plancash').val((total - planbayanihan).toFixed(2));
        		}
        	},
        	actualcash : function(){

        		var total =  parseFloat($('.in-total').val()); // get value of the total labor

        		var plancash = parseFloat($('.in-actualcash ').val());

        		if(plancash > total){
        			alert('Actual cash is greater than the total labor');
        			$('.in-actualcash ').val('')
        		}else{

	        		$('.in-actualbayanihan').val((total - plancash).toFixed(2));
        		}
        	},
        	actualbayanihan : function(){

        		var total =  parseFloat($('.in-total').val()); // get value of the total labor

        		var planbayanihan = parseFloat($('.in-actualbayanihan ').val());
        		if(planbayanihan > total)
        		{
        			alert('Actual Bayanihan is greater than the total labor');
        			$('.in-actualbayanihan ').val('')
        		}
        		else
        		{
        			$('.in-actualcash').val( (total - planbayanihan).toFixed(2) );
        		}
        	},
        }
        /* END OF THE Computation*/

    	setTimeout(function(){
    		$('div.alert').slideUp();
    	}, 5000);

		$('.in-time, .in-currency').keydown(function(event) {
				if ( event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
				}
				else {
					if (event.keyCode < 48 || event.keyCode > 57 ) {
						event.preventDefault();	
					}	
				}
			});


		$('#in-age').keydown(function(event) {
				if (event.keyCode == 8 || event.keyCode == 9) {
				}
				else {
					if (event.keyCode < 48 || event.keyCode > 57 ) {
						event.preventDefault();	
					}	
				}
			});

		$('#in-hourrate, #in-dailyrate').keydown(function(event) {
				if ( event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
				}
				else {
					if (event.keyCode < 48 || event.keyCode > 57 ) {
						event.preventDefault();	
					}	
				}
			});
	    /*
	     * ADDING a worker on ERS
	     */
		$('#spi-addworker_modal_show').on('click', function()
		{
			$('#spi-addworker_modal').modal('show');
			console.log(computator);
			return false;
		});

		$('#spi-ers_addersrecords').on('click', function(event)
		{
			var $workers = $('.in-worker_ids:checked');
			var worker_id = [];
			for(i=0; i < $workers.length; ++i)
			{
				worker_id[i] = $workers.eq(i).val();
			}

			var $url = $(event.currentTarget).attr('href'); 
			$.ajax({
	  			type: "POST",
	  			url:  $url,
	  			data: { 
	  				'in-recordid' : $('#record_id').val(),
	  				'in-workerid' : worker_id 
	  			}
			})
	  		.done(function( msg ) {
	    		$('#spi-addworker_modal').modal('hide');
	    		window.location = window.location.href;
	  		});
			return false;
		});
		/* end of ADDING a worker on ERS */

	    /*
	     * UPDATING a worker on ERS
	     */

	    $('#dataTables-example td .spi-ers_showdetails').on('click', function (event) {
	    	var url = $(this).attr('href');
	    	$(this).hide();
	    	$(this).siblings('.spi-ers_hidedetails').show();
	        var nTr           = $(this).parents('tr');
            console.log(computator.compute());
            // console.log(parseFloat(nTr.find('td.total').data('rateday')).toFixed(2));
            $('.in-naturework').val(nTr.find('.work').data('value'));
            $('.in-dailyrate').val(parseFloat(nTr.find('td.total').data('rateday')).toFixed(2));
            $('.in-daysworked').val(parseFloat(nTr.find('td.total').data('workdays')).toFixed(2) | 0);
            $('.in-hourlyrate').val(parseFloat(nTr.find('td.total').data('ratehour')).toFixed(2));
            $('.in-hoursworked').val(parseFloat(nTr.find('td.total').data('workhours')).toFixed(2));
            $('.in-total').val(parseFloat(nTr.find('td.total').data('value')).toFixed(2));


            $('.in-plancash').val(parseFloat(nTr.find('td.plan_cash').data('value')).toFixed(2));


            $('.in-actualcash').val(parseFloat(nTr.find('td.actual_cash').data('value')).toFixed(2));
            $('.in-planbayanihan').val(parseFloat(nTr.find('td.plan_lcc').data('value')).toFixed(2));
            $('.in-actualbayanihan').val(parseFloat(nTr.find('td.actual_lcc').data('value') ).toFixed(2));
            $('.spi-ers_editersrecords').attr('href',url);

	        $('#ers_edit').modal('show');
//            $table1.fnOpen( nTr[0], format(nTr, url), 'details' );

	        return false;
	    } );
		
		$('.in-hoursworked').change(function(){
			$('.in-plancash').val(0);
			$('.in-actualcash').val(0);
			$('.in-planbayanihan').val(0);
			$('.in-actualbayanihan').val(0);
		});
		
		$('.in-daysworked').change(function(){
			$('.in-plancash').val(0);
			$('.in-actualcash').val(0);
			$('.in-planbayanihan').val(0);
			$('.in-actualbayanihan').val(0);
		});

		$('#dataTables-example td .spi-ers_hidedetails').on('click', function (event) {
			$(this).hide();
			$(this).siblings('.spi-ers_showdetails').show();
	        var nTr = $(this).parents('tr')[0];
//	        $table1.fnClose( nTr );

	        return false;
	    } );

		$('#dataTables-example tbody').on('change', '.in-dailyrate, .in-daysworked, .in-hourlyrate, .in-hoursworked', function(event)
		{
			var $tbody = $(event.currentTarget).parents('tbody');
			var $total =  $tbody.find('.in-total');
			var daily_rate   = parseFloat($tbody.find('.in-dailyrate').val()).toFixed(2);
			var daysworked   = parseFloat($tbody.find('.in-daysworked').val()).toFixed(2);
			var hourly_rate  = parseFloat($tbody.find('.in-hourlyrate').val()).toFixed(2);
			var hours_worked = parseFloat($tbody.find('.in-hoursworked').val()).toFixed(2);
			var total  = (daily_rate * daysworked) + (hourly_rate * hours_worked);
			$total.val(total);
		});

		$('.spi-ers_editersrecords').on('click', function(event)
		{
			$url            = $(event.currentTarget).attr('href');
			var $tbody      = $(event.currentTarget).parent().parent().find('tbody');
			var work        = $tbody.find('.in-naturework').val();
			var rate_day    = $tbody.find('.in-dailyrate').val();
			var work_days   = $tbody.find('.in-daysworked').val();
			var rate_hours  = $tbody.find('.in-hourlyrate').val();
			var work_hours  = $tbody.find('.in-hoursworked').val();
			var total       = $tbody.find('.in-total').val();
			var plan_cash   = $tbody.find('.in-plancash').val();
			var actual_cash = $tbody.find('.in-actualcash').val();
			var plan_lcc    = $tbody.find('.in-planbayanihan').val();
			var actual_lcc  = $tbody.find('.in-actualbayanihan').val();
			$.ajax({
		  		type: "PUT",
		  		url: $url,
				data: {
				    'in-natureofwork'    : work,
	  				'in-rateday'         : rate_day,
	  				'in-workdays'        : work_days,
	  				'in-ratehours'       : rate_hours,
	  				'in-workhours'       : work_hours,
	  				'in-plancash'        : plan_cash,
	  				'in-actualcash'      : actual_cash,
	  				'in-planbayanihan'   : plan_lcc,
	  				'in-actualbayanihan' : actual_lcc
				}
			})
		  	.done(function( msg ) {
		    	$('#dataTables-example td .spi-ers_hidedetails').trigger('click');
		    	var $current_row = $('tr[data-rowid="' + $(event.currentTarget).parents('table').data('rowid') + '"]');
		    	$current_row.find('td.total').data('value', total).data('ratehour', rate_hours).data('rateday', rate_day)
		    		.data('workhours', work_hours).data('workdays', work_days).find('input').val(total);
		    	$current_row.find('td.plan_cash').data('value', plan_cash).find('input').val(plan_cash);
		    	$current_row.find('td.plan_lcc').data('value', plan_lcc).find('input').val(plan_lcc);
		    	$current_row.find('td.actual_cash').data('value', actual_cash).find('input').val(actual_cash);
		    	$current_row.find('td.actual_lcc').data('value', actual_lcc).find('input').val(actual_lcc);

		    	$('#update_worker_alert').slideDown();
		    	setTimeout(function(){
		    		$('#update_worker_alert').slideUp();
		    	}, 5000);
		 	}).success(function(){
		 	  $('#ers_edit').modal('hide');
		 	  window.location.reload();
		 	});

			return false;
		});
		/* end of ADDING a worker on ERS */
	    /*
	     *  DELETING a worker on ERS
	     */
		$('.spi-ersworker_modal-del_show').on('click', function(event){
			var id = $(event.currentTarget).parents('tr').data('rowid');
			$('#del-id').val(id);
			$('#spi-ersworker_modal_delform_submit').attr('href', $(event.currentTarget).attr('href'));
			$('#spi-ersworker_modal-del').modal('show');
			return false;
	  	});

		$('#spi-ersworker_modal_delform_submit').on('click', function(event)
		{
			var $url = $(event.currentTarget).attr('href');

			$.ajax({
		  		type: "DELETE",
		  		url: $url,
				data: {
	  				'in-recordid' : $('#record_id').val(),
				}
			})
		  	.done(function( msg ) {
		    	$('#spi-ersworker_modal-del').modal('hide');
		    	window.location = window.location.href;
		 	});

			return false;
		});

	  	$('#spi-addnewworker_modal_show').on('click', function(event)
	  	{
	  		var $url = $(event.currentTarget).data('value');
	  		$('#spi-addnewworker_modal').modal('show');
	  		$('.modal').loading(true);

	  		$.ajax({
	  			type: "GET",
		  		url:  $url
		  	})
		  	.done(function( msg ) {
		  		  		$('.modal').loading(false);
	    		$('#in-workerid').val(msg);

	  		});

			return false;
	  	});

        /* ADD NEW WORKER SUBMIT*/
	  	$('#spi-addnewworker_modal_form_submit').on('click', function(event)
	  	{
	  		var $url = $(event.currentTarget).data('value');
	  	
			/*var $url = window.location.href; */
			$.ajax({
	  			type: "PUT",
		  		url:  $url,
		  		data: {
	  				'in-projectid'    : $('#in-projectid').val(),
	  				'in-workerid'     : $('#in-workerid').val(),
	  				'in-firstname'    : $('#in-firstname').val(),
		  			'in-lastname'     : $('#in-lastname').val(),
		  			'in-middlename'   : $('#in-middlename').val(),
		  			'in-sex'          : $('#in-sex').val(),
	   				'in-age'          : $('#in-age').val(),
	   				'in-birthday'     : $('#in-birthday').val(),
	   				'in-natureofwork' : $('#in-natureofwork').val(),
	   				'in-typeofwork'   : $('#in-typework').val(),
	   				'in-hourrate'     : parseFloat($('#in-hourrate').val()),
	   				'in-dailyrate'    : parseFloat($('#in-dailyrate').val()),
		   			'in-pantawid'     : $('#in-pantawid').prop('checked') ? 1 : 0,
		   			'in-seac'         : $('#in-seac').prop('checked') ? 1 : 0,
		   			'in-ip'     	  : $('#in-ip').prop('checked') ? 1 : 0 ,
		   			'in-slp'          : $("#in-slp").prop('checked') ? 1 :0
		  		}
			})
	  		.done(function( msg ) {
	    		//window.location = window.location.href;
	    		var new_url = $('#spi-ers_addersrecords').attr('href');
	    		var worker_id = [];
	    		worker_id[0]  = $('#in-workerid').val();

				$.ajax({
		  			type: "POST",
		  			url:  new_url,
		  			data: { 
		  				'in-recordid' : $('#record_id').val(),
		  				'in-workerid' : worker_id 
		  			}
				})
		  		.done(function( msg ) {
		    		$('#spi-addnewworker_modal').modal('hide');
		    		window.location = window.location.href;
		  		});
	  		});
		
			return false;
	  	});


        $('.in-dailyrate').change(computator.compute);
        $('.in-daysworked').change(computator.compute);
        $('.in-hourlyrate').change(computator.compute);
        $('.in-hoursworked').change(computator.compute);
        $('.in-plancash ').change(validator.plancash);
        $('.in-planbayanihan ').change(validator.planbayanihan);

        $('.in-actualcash ').change(validator.actualcash);
        $('.in-actualbayanihan ').change(validator.actualbayanihan);


	  	$('#in-natureofwork').change(function(){
	  	    var type = $(this).val();
	  	    $('.box').loading(true);
	  	    $.get('{{ URL::to('spi-subworkers/api-typeofwork') }}',{ type: type },function (res){
                $('#in-typework').val(res.work).attr("readonly","readonly");
                $('.box').loading(false);

	  	    })

	  	});
	});
</script>
@stop