@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
  <li>
  	<a class="active-menu" href="{{ URL::to('spi_profile') }}"><i class="fa fa-home fa-3x"></i> Home</a>
  </li>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    {{ HTML::linkRoute('spi_profile.show', 'Back to Sub Project Profile',
      array($spcr->project_id), array('class' => 'btn btn-default btn')) }}
         @if( !is_review($spcr->project_id) )
    {{ HTML::linkRoute('spi_profile.spcr.edit', 'Edit',
      array($spcr->project_id), array('class' => 'btn btn-info btn')) }}
      @endif
  </div>
</div>

<hr />

<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<div class="col-md-6 ">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="40%">Sub Project Name</th>
					<td>{{ $profile->project_name }}</td>
				</tr>
				<tr>
					<th>Sub Project Category</th>
					<td>{{ $profile->proj_subcategory }}</td>
				</tr>
				<tr>
					<th>Barangays Covered</th>
					<td>
						@if ($profile->mibf_refno !== "")
							{{ $proposal->proj_coverage_list() }}
						@else
							{{ $profile->brgy_coverage_list() }}
						@endif
					</td>
				</tr>
				<tr>
					<th>Municipality</th>
					<td>{{ Report::get_municipality($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Province</th>
					<td>{{ Report::get_province($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Date of MIBF-CSW</th>
					<td>{{ toDate($spcr->date_csw) }}</td>
				</tr>
				<tr>
					<th>Date of MIBF-Prioritization</th>
					<td>{{ toDate($spcr->date_prioritization) }}</td>
				</tr>
				<tr>
					<th>Total no. of HH served by the Sub Project</th>
					<td>{{ $spcr->no_households }}</td>
				</tr>
				<tr>
					<th>Total no. of Families served by the Sub Project</th>
					<td>{{ $spcr->no_families }}</td>
				</tr>
				<tr>
					<th>Total no. of 4Ps HH Beneficiaries served by the Sub Project</th>
					<td>{{ $spcr->no_pantawid_hh }}</td>
				</tr>
				<tr>
					<th>Total no. of 4Ps Families served by the Sub Project</th>
					<td>{{ $spcr->no_pantawid_fm }}</td>
				</tr>
				<tr>
					<th>Target Physical Accomplishment</th>
					<td>{{ $profile->phys_target . " " . $profile->phys_target_unit }}</td>
				</tr>
				<tr>
					<th>Actual Physical Accomplishment</th>
					<td>{{ $spcr->phys_actual }}</td>
				</tr>
				<tr>
					<th>Estimated Total Cost (MIBF Approved)</th>
					<td>
						@if ($profile->mibf_refno !== "")
							{{ $proposal->kc_amount + $proposal->lcc_amount }}
						@endif
					</td>
				</tr>
				<tr>
					<th>Actual Total Cost</th>
					<td>{{ $spcr->total_cost }}</td>
				</tr>
				<tr>
					<th>Total Grant Received</th>
					<td>{{ $profile->grant_infra + $profile->grant_training + $profile->grant_women + $profile->grant_mgmnt + $profile->grant_others }}</td>
				</tr>
				<tr>
					<th>Total Counterpart Delivered</th>
					<td>{{ $spcr->total_counterpart }}</td>
				</tr>
			</table>
		</div>
		<div class="col-md-6 ">
			<table class="table table-bordered table-striped">
			
				<tr>
					<th>Actual Total Direct Cost</th>
					<td>{{ $spcr->total_direct }}</td>
				</tr>
				<tr>
					<th>Actual Total Indirect Cost</th>
					<td>{{ $spcr->total_indirect }}</td>
				</tr>
				<tr>
					<th>Date Started</th>
					<td>{{ toDate($profile->date_started) }}</td>
				</tr>
				<tr>
                    <th>Status</th>
                    <td>{{ $spcr->completed_status }}</td>
                </tr>
				<tr>
					<th>Date of Status</th>
					<td>{{ toDate($profile->date_completed) }}</td>
				</tr>
				<tr>
					<th>Date of Inauguration</th>
					<td>{{ toDate($spcr->date_inaugurate) }}</td>
				</tr>
				<tr>
					<th>Total Population in the Barangay</th>
					<td>
						<table style="width: 100%">
							<tr>
								<th>Male</th>
								<th>Female</th>
							</tr>
							<tr>
								<td>{{ $spcr->no_male_pop }}</td>
								<td>{{ $spcr->no_female_pop }}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<th>Total Population served by the Sub Project</th>
					<td>
						<table style="width: 100%">
							<tr>
								<th>Male</th>
								<th>Female</th>
							</tr>
							<tr>
								<td>{{ $spcr->no_male_serve }}</td>
								<td>{{ $spcr->no_female_serve }}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<th>Total IP Population served by the Sub Project</th>
					<td>
						<table style="width: 100%">
							<tr>
								<th>Male</th>
								<th>Female</th>
							</tr>
							<tr>
								<td>{{ $spcr->no_male_ip }}</td>
								<td>{{ $spcr->no_female_ip }}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<th>No. of SLP Beneficiaries served by the Sub Project</th>
					<td>{{ $spcr->no_slp_hh }}</td>
				</tr>
				<tr>
					<th>No. of SLP Families Beneficiaries served by the Sub Project</th>
					<td>{{ $spcr->no_slp_family }}</td>
				</tr>
				<tr>
					<th>No. of IP HH Beneficiaries served by the Sub Project</th>
					<td>{{ $spcr->no_ip_hh }}</td>
				</tr>

				<tr>
					<th>No. of IP Families served by the Sub Project</th>
					<td>{{  $spcr->no_ip_hh_families }}</td>
				</tr>
				<tr>
					<th>Procurement mode and procedures used in the Sub Project implementation</th>
					<td>{{ SPIProcurementMode::where(array('mode_id' => $spcr->proc_mode))->first()->mode }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		Labor Generated
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped labor_generated">
				<thead>
					<tr>
						<th width="40%">Particular</th>
						<th>Number</th>
						<th>Person Days</th>
						<th>Rate/Day</th>
						<th>Total Amount Paid</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Skilled (men)</td>
						<td>{{ $spcr->no_skilled_male }}</td>
						<td>{{ $spcr->days_skilled_male }}</td>
						<td>{{ $spcr->rate_skilled_male }}</td>
						<td>0</td>
					</tr>
					<tr>
						<td>Skilled (women)</td>
						<td>{{ $spcr->no_skilled_female }}</td>
						<td>{{ $spcr->days_skilled_female }}</td>
						<td>{{ $spcr->rate_skilled_female }}</td>
						<td>0</td>
					</tr>
					<tr>
						<td>Unskilled (men)</td>
						<td>{{ $spcr->no_unskilled_male }}</td>
						<td>{{ $spcr->days_unskilled_male }}</td>
						<td>{{ $spcr->rate_unskilled_male }}</td>
						<td>0</td>
					</tr>
					<tr>
						<td>Unskilled (women)</td>
						<td>{{ $spcr->no_unskilled_female }}</td>
						<td>{{ $spcr->days_unskilled_female }}</td>
						<td>{{ $spcr->rate_unskilled_female }}</td>
						<td>0</td>
					</tr>
					<tr>
						<td class="text-right" colspan="4">Total Paid Labor</td>
						<td>0</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		calculate_total_labor();
	});

	function calculate_total_labor() {
		var tr_list = $("table.labor_generated tbody tr");
		var total = 0;

		for (var i = 0; i < tr_list.length-1; i++) {
			var num = parseInt($(tr_list[i]).find("td:nth-child(2)").html() || 0);
			var days = parseInt($(tr_list[i]).find("td:nth-child(3)").html() || 0);
			var rate = parseFloat($(tr_list[i]).find("td:nth-child(4)").html() || 0);

			var cur_total = (num * days * rate);
			$(tr_list[i]).find("td:nth-child(5)").html(cur_total);

			total += cur_total;
		}

		$(tr_list[tr_list.length-1]).find("td:last-child").html(total);
	}
</script>

@stop