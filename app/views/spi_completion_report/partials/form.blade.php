<div class="panel panel-default">
	<div class="panel-heading">
		General Information
	</div>
	<div class="panel-body">
		<div class="table-responsive">
      <div class="col-md-6 ">

			<table class="table table-bordered table-striped">
				<tr>
					<th width="40%">Sub Project Name</th>
					<td>
						{{ $profile->project_name }}
						{{ Form::hidden('project_id', $spcr->project_id) }}
					</td>
				</tr>
				<tr>
					<th>Sub Project Category</th>
					<td>{{ $profile->proj_subcategory }}</td>
				</tr>
				@if ($profile->mibf_refno !== "")
				<tr>
					<th>Barangays Covered</th>
					<td>{{ $proposal->proj_coverage_list() }}</td>
				</tr>
				@endif
				<tr>
					<th>Municipality</th>
					<td>{{ Report::get_municipality($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Province</th>
					<td>{{ Report::get_province($profile->municipal_id) }}</td>
				</tr>
				<tr>
					<th>Date of MIBF-CSW</th>
					<td>{{ toDate($spcr->date_csw) }}</td>
				</tr>
				<tr>
					<th>Date of MIBF-Prioritization</th>
					<td>{{ toDate($spcr->date_prioritization) }}</td>
				</tr>
				<tr>
					<th>Total no. of HH served by the Sub Project</th>
					<td>{{ Form::input('number', 'no_households', $spcr->no_households, ['class' => 'form-control', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>Total no. of Families served by the Sub Project</th>
					<td>{{ Form::input('number', 'no_families', $spcr->no_families, ['class' => 'form-control', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>Total no. of 4Ps HH Beneficiaries served by the Sub Project</th>
					<td>{{ Form::input('number', 'no_pantawid_hh', $spcr->no_pantawid_hh, ['class' => 'form-control', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>Total no. of 4Ps Families served by the Sub Project</th>
					<td>{{ Form::input('number', 'no_pantawid_fm', $spcr->no_pantawid_fm, ['class' => 'form-control', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>Target Physical Accomplishment</th>
					<td>{{ $profile->phys_target . " " . $profile->phys_target_unit }}</td>
				</tr>
				<tr>
					<th>Actual Physical Accomplishment</th>
					<td>{{ Form::input('number', 'phys_actual', $spcr->phys_actual, ['class' => 'form-control', 'step' => 'any', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>Estimated Total Cost (MIBF Approved)</th>
					<td>
						@if ($profile->mibf_refno !== "")
							{{ $proposal->kc_amount + $proposal->lcc_amount }}
						@endif
					</td>
				</tr>
				<tr>
					<th>Actual Total Cost</th>
					<td>{{ Form::currency('total_cost', $spcr->total_cost, ['class' => 'form-control prices', 'step' => 'any', 'min' => 0]) }}</td>
				</tr>


				<tr>
					<th>Total Grant Received</th>
					<td>{{ $profile->grant_infra + $profile->grant_training + $profile->grant_women + $profile->grant_mgmnt + $profile->grant_others }}</td>
				</tr>
				<tr>
					<th>Total Counterpart Delivered</th>
					<td>{{ Form::input('number', 'total_counterpart', $spcr->total_counterpart, ['class' => 'form-control', 'step' => 'any', 'min' => 0]) }}</td>
				</tr>
				</table>
		</div>
      <div class="col-md-6 ">
			<table class="table table-bordered table-striped">
      	
				<tr>
					<th>Actual Total Direct Cost</th>
					<td>{{ Form::currency('total_direct', $spcr->total_direct, ['class' => 'form-control prices', 'step' => 'any', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>Actual Total Indirect Cost</th>
					<td>{{ Form::currency('total_indirect', $spcr->total_indirect, ['class' => 'form-control prices', 'step' => 'any', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>Date Started</th>
					<td>{{ toDate($profile->date_started) }}</td>
				</tr>
				<tr>
				    <th>Status</th>
				    <td>{{ Form::select('completed_status',[''=>'Select Status' ,'Completed'=>'Completed', 'Completed as Built'=>'Completed as Built','Not Completed as Build'=>'Not Completed as Build','Terminated'=>'Terminated' ],$spcr->completed_status,array('class' => 'form-control' )) }}</td>
				</tr>
				<tr>
					<th>Status Date</th>
					<td>{{ Form::input('text', 'date_completed', isset($profile->date_completed) ? date("m/d/Y", strtotime($profile->date_completed)) : null, array('class' => 'form-control date','max' => date("m/d/Y"), 'required')) }}</td>
				</tr>
				<tr>
					<th>Date of Inauguration</th>
					<td>{{ Form::input('text', 'date_inaugurate', isset($spcr->date_inaugurate) ? date("m/d/Y", strtotime($spcr->date_inaugurate)) : null, array('class' => 'form-control date','max' => date("m/d/Y"))) }}</td>
				</tr>
				<tr>
					<th>Total Population in the Barangay</th>
					<td>
						<table style="width: 100%">
							<tr>
								<th>Male</th>
								<th>Female</th>
							</tr>
							<tr>
								<td>{{ Form::input('number', 'no_male_pop', $spcr->no_male_pop, ['class' => 'form-control', 'min' => 0]) }}</td>
								<td>{{ Form::input('number', 'no_female_pop', $spcr->no_female_pop, ['class' => 'form-control', 'min' => 0]) }}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<th>Total Population served by the Sub Project</th>
					<td>
						<table style="width: 100%">
							<tr>
								<th>Male</th>
								<th>Female</th>
							</tr>
							<tr>
								<td>{{ Form::input('number', 'no_male_serve', $spcr->no_male_serve, ['class' => 'form-control', 'min' => 0]) }}</td>
								<td>{{ Form::input('number', 'no_female_serve', $spcr->no_female_serve, ['class' => 'form-control', 'min' => 0]) }}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<th>Total IP Population served by the Sub Project</th>
					<td>
						<table style="width: 100%">
							<tr>
								<th>Male</th>
								<th>Female</th>
							</tr>
							<tr>
								<td>{{ Form::input('number', 'no_male_ip', $spcr->no_male_ip, ['class' => 'form-control', 'min' => 0]) }}</td>
								<td>{{ Form::input('number', 'no_female_ip', $spcr->no_female_ip, ['class' => 'form-control', 'min' => 0]) }}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<th>No. of SLP HH Beneficiaries served by the Sub Project</th>
					<td>{{ Form::input('number', 'no_slp_hh', $spcr->no_slp_hh, ['class' => 'form-control', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>No. of SLP Families Beneficiaries served by the Sub Project</th>
					<td>{{ Form::input('number', 'no_slp_family', $spcr->no_slp_family, ['class' => 'form-control', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>No. of IP HH Beneficiaries served by the Sub Project</th>
					<td>{{ Form::input('number', 'no_ip_hh', $spcr->no_ip_hh, ['class' => 'form-control', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>No. of IP Families served by the Sub Project</th>
					<td>{{ Form::input('number', 'no_ip_hh_families', $spcr->no_ip_hh_families, ['class' => 'form-control', 'min' => 0]) }}</td>
				</tr>
				<tr>
					<th>Procurement mode and procedures used in the Sub Project implementation</th>
					<td>{{ Form::select('proc_mode', $proc_modes, $spcr->proc_mode, ['class' => 'form-control']) }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>
@if ($profile->mibf_refno === "")
<div class="panel panel-default">
	<div class="panel-heading">
		Barangays Covered
	</div>
	<div class="panel-body">
		<div class="table-responsive" id="items">
			<button id="add" type="button" >Add</button>
			@foreach ($profile->barangays_covered()->get() as $brgy)
			<div class="input-group">
				{{ Form::select('barangay[]', $barangay_list, $brgy->barangay_id, array('class' => 'form-control', 'oninput'=>'checkduplicate(this)', 'required')) }}
				<div class="input-group-addon">
					<button type="button" class="delete"><i class="fa fa-trash-o"></i></button>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
@endif
<div class="panel panel-default">
	<div class="panel-heading">
		Labor Generated
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped labor_generated">
				<thead>
					<tr>
						<th width="40%">Particular</th>
						<th>Number</th>
						<th>Person Days</th>
						<th>Rate/Day</th>
						<th>Total Amount Paid</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Skilled (men)</td>
						<td>{{ Form::input('number', 'no_skilled_male', $spcr->no_skilled_male, ['class' => 'form-control', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', 'days_skilled_male', $spcr->days_skilled_male, ['class' => 'form-control', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', 'rate_skilled_male', $spcr->rate_skilled_male, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
						<td>0</td>
					</tr>
					<tr>
						<td>Skilled (women)</td>
						<td>{{ Form::input('number', 'no_skilled_female', $spcr->no_skilled_female, ['class' => 'form-control', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', 'days_skilled_female', $spcr->days_skilled_female, ['class' => 'form-control', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', 'rate_skilled_female', $spcr->rate_skilled_female, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
						<td>0</td>
					</tr>
					<tr>
						<td>Unskilled (men)</td>
						<td>{{ Form::input('number', 'no_unskilled_male', $spcr->no_unskilled_male, ['class' => 'form-control', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', 'days_unskilled_male', $spcr->days_unskilled_male, ['class' => 'form-control', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', 'rate_unskilled_male', $spcr->rate_unskilled_male, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
						<td>0</td>
					</tr>
					<tr>
						<td>Unskilled (women)</td>
						<td>{{ Form::input('number', 'no_unskilled_female', $spcr->no_unskilled_female, ['class' => 'form-control', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', 'days_unskilled_female', $spcr->days_unskilled_female, ['class' => 'form-control', 'min' => 0]) }}</td>
						<td>{{ Form::input('number', 'rate_unskilled_female', $spcr->rate_unskilled_female, ['class' => 'form-control', 'min' => 0, 'step' => 'any']) }}</td>
						<td>0</td>
					</tr>
					<tr>
						<td class="text-right" colspan="4">Total Paid Labor</td>
						<td>0</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- {{ Form::submit('Save SP Completion Report', array('class' => 'btn btn-primary')) }}
{{ HTML::linkRoute('spi_profile.show', 'Close',
  array($spcr->project_id), array('class' => 'btn bg-navy btn')) }} -->

  <div class="col-md-12 ">
						<div class="form-group pull-right">
						               {{ Form::submit('Save SP Completion Report', array('class' => 'btn btn-primary')) }}
				<!-- <a class="btn btn-default" href="{{URL::to('mun_trainings')}}">Cancel</a> -->
					{{ HTML::linkRoute('spi_profile.show', 'Cancel',
  array($spcr->project_id), array('class' => 'btn btn-default')) }}					
						</div>
					</div>
		                 			
						<div class="clearfix"></div>
{{ Form::close() }}

<script>
	$(document).ready(function() {
		// add styling
		$("input[name='date_evaluated']").change(function(){
            if(new Date($("input[name='date_evaluated']").val()) > new Date()){
                $("input[name='date_evaluated']")[0].setCustomValidity('Future date is not allowed');
            }else{
                $("input[name='date_evaluated']")[0].setCustomValidity('');
                if(new Date($("input[name='date_evaluated']").val()) < new Date($("input[name='date_completed']").val())){
                    $("input[name='date_evaluated']")[0].setCustomValidity('Evaluation date should be later than completion date');
                }else{
                    $("input[name='date_evaluated']")[0].setCustomValidity('');
                }
            }
        });

		$("table table tr td").css("padding-bottom", "5px");
		$("table table tr td").css("padding-left", "5px");
		$("table table tr th").css("padding-bottom", "5px");
		$("table table tr th").css("padding-left", "5px");

		$("table.labor_generated tr td").change(calculate_total_labor);
		calculate_total_labor();

		//when the Add Filed button is clicked
		$("#add").click(function (e) {
			console.log("here");
			//Append a new row of code to the "#items" div
			//$("#items").append($("#template").html());
			$("#items").append('<div class="input-group">{{ Form::select('barangay[]', $barangay_list, 'default', array('class' => 'form-control', 'oninput'=>'checkduplicate(this)', 'required')) }}<div class="input-group-addon"><button type="button" class="delete"><i class="fa fa-trash-o"></i></button></div></div>');
		});

		$("body").on("click", ".delete", function (e) {
			$(this).parent("div").parent("div").remove();
		});
	});

	function calculate_total_labor() {
		var tr_list = $("table.labor_generated tbody tr");
		var total = 0;

		for (var i = 0; i < tr_list.length-1; i++) {
			var num = parseInt($(tr_list[i]).find("td:nth-child(2) input").val() || 0);
			var days = parseInt($(tr_list[i]).find("td:nth-child(3) input").val() || 0);
			var rate = parseFloat($(tr_list[i]).find("td:nth-child(4) input").val() || 0);

			var cur_total = (num * days * rate);
			$(tr_list[i]).find("td:nth-child(5)").html(cur_total);

			total += cur_total;
		}

		$(tr_list[tr_list.length-1]).find("td:last-child").html(total);
	}

	function checkduplicate(e){
		var count = -1;
		var elements = document.getElementsByName('barangay[]');
		for (i=0; i<elements.length; i++) {
				if(e.value === elements[i].value){
					count++;
				}
		}
		if(count == 1){	
			e.setCustomValidity("Duplicate Entry not allowed");
		}else{
			e.setCustomValidity("");
		}
	}
</script>