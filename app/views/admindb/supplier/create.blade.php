@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	
		@if ($errors->all())
			<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
		@endif
				
		{{ Form::open(array('url' => 'supplier')) }}
		<div class="col-md-6">
		 	<div class="box box-primary">
	            <div class="box-body">

					

						<div class="form-group">
						<!-- <label for="">Region</label> -->
							
							{{ Form::input('text','region_psgc',$region,['class'=>'form-control hidden']) }}
						</div>						
						<div class="form-group">
							<!-- <label for="">Provincial</label> -->
							{{ Form::input('text','prov_psgc',$province->province_psgc,['class'=>'form-control hidden']) }}
						</div>	
						<div class="form-group">
							<label for="">Municipality (Location of Service Providers)</label>
							
							{{ Form::select('muni_psgc', $municipalities, '', array('class' => 'form-control', 'required')) }}

						</div>	
                        <div class="form-group">
							<label for="">Name of Service Provider</label>
							{{ Form::input('text','name_of_service_provider','',['class'=>'form-control','required']) }}
						</div>
						<div class="form-group">
							<label for="">Category</label>
							{{ Form::select('category',$category,'',['class'=>'form-control','required'])}}
						</div>
						<div class="form-group">
							<label for="">Address</label>
							{{ Form::input('text','address','',['class'=>'form-control','required'])}}
						</div>
						<div class="form-group">
							<label for="">Status</label>
							{{ Form::select('status',$status,'',['class'=>'form-control','required'])}}
						</div>

						<div class="form-group">
							<label for="">Remarks</label>
							{{ Form::input('text','remarks','',['class'=>'form-control','required']) }}
						</div>
	
						<div class="form-group">
							<label for="">Contact Person</label>
							{{ Form::input('text','contact_person', '' ,['class'=>'form-control','required']) }}
						</div>
						<div class="form-group">
							<label for="">Contact Number</label>
							{{ Form::input('text','contact_number', '' ,['class'=>'form-control', 'required']) }}
						</div>
						<div class="form-group">
							<label for="">Email</label>
							{{ Form::input('text','email','',['class'=>'form-control', 'required']) }}
						</div>

										
				</div>
			</div>
			
			<div class="form-group">
				{{ Form::submit('Submit ',['class'=>'btn btn-primary']) }}
			</div>
		{{ Form::close() }}
		</div>

	
@stop