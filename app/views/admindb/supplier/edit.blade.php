@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	
		@if ($errors->all())
			<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
		@endif
				
		<!-- {{ Form::open(array('url' => 'supplier/'.$supplier->provider_id.'/edit')) }} -->

		{{ Form::open(array('route' => array('supplier.update', $supplier->provider_id), 'method' => 'patch', 'role' => 'form')) }}
		
		<div class="col-md-6">
		 	<div class="box box-primary">
	            <div class="box-body">



						<div class="form-group">
							<label for="">Municipalities</label>
							
							{{ Form::select('muni_psgc', $municipalities, $supplier->muin_psgc, array('class' => 'form-control', 'required')) }}

						</div>	


						
						<div class="form-group">
						<label for="">Name of Service Provider</label>
							{{ Form::input('text','name_of_service_provider',$supplier->name_of_service_provider,['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Category</label>
							{{ Form::input('text','category',$supplier->category,['class'=>'form-control','required'])}}
						</div>
						<div class="form-group">
							<label for="">Address</label>
							{{ Form::input('text','address',$supplier->address,['class'=>'form-control','required'])}}
						</div>
						<div class="form-group">
							<label for="">Status</label>
							{{ Form::input('text','status',$supplier->status,['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Remarks</label>
							{{ Form::input('text','remarks',$supplier->remarks,['class'=>'form-control','required']) }}
						</div>
	
						<div class="form-group">
							<label for="">Contact Person</label>
							{{ Form::input('text','contact_person', $supplier->contact_person ,['class'=>'form-control','required']) }}
						</div>
						<div class="form-group">
							<label for="">Mobile Number</label>
							{{ Form::input('text','contact_number', $supplier->contact_number ,['class'=>'form-control', 'required']) }}
						</div>
						<div class="form-group">
							<label for="">Email</label>
							{{ Form::input('text','email',$supplier->email,['class'=>'form-control', 'required']) }}
						</div>





						

						
				</div>
			</div>
			

			

			<div class="form-group">
				{{ Form::submit('Submit ',['class'=>'btn btn-primary']) }}
			</div>
		{{ Form::close() }}
		</div>

		
@stop