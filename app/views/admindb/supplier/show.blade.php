@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="pull-left">
				{{ Form::open(array('url' => 'supplier/' . $supplier->provider_id)) }}
		<a class="btn btn-default" href="{{ URL::to('supplier') }}">Go Back</a>
				{{ Form::hidden('_method', 'DELETE') }}
				{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
				<a href="{{ URL::to('supplier/'.$supplier->provider_id.'/edit') }}" class="btn btn-primary " > <i class="fa fa-edit"></i>Edit</a>

				
			</div>	
		<div class="col-md-12">
		 	<div class="box box-primary">
	            <div class="box-body">

						<div class="form-group">
							<label for="">Name of Service Provider</label>
							<span>{{ $supplier->name_of_service_provider }}</span>
						</div>
						<div class="form-group">
							<label for="">Category</label>
							<span> {{ $supplier->category }}</span>
						</div>
						<div class="form-group">
							<label for="">Address</label>
							<span> {{ $supplier->address}}</span>
						</div>

						<div class="form-group">
							<label for="">Status</label>
							<span> {{ $supplier->status }}</span>
						</div>
						<div class="form-group">
							<label for="">Remarks</label>
							<span> {{ $supplier->remarks }}</span>
						</div>
						
						<div class="form-group">
							<label for="">Contact person</label>
							<span> {{ $supplier->contact_person }}</span>
						</div>
						<div class="form-group">
							<label for="">Contact Number</label>
							<span> {{ $supplier->contact_number }}</span>
						</div>
						<div class="form-group">
							<label for="">Email</label>
							<span> {{ $supplier->email }}</span>
						</div>

			</div>
			
					<div class="clearfix"></div>
				</div>
			</div>

		
		</div>

	
@stop