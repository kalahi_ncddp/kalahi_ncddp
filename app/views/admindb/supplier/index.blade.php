@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop
@section('content')
	@if ($errors->all())
		<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
	@endif

	<div class="panel panel-default">
		<div class="panel-heading">
			  {{ $title }}			
			 @if($position->position!='encoder')
			   <!--  <span class="btn btn-default pull-right" id="approved">Review</span> -->
            @endif
			<a class="btn  btn-primary pull-right" href="{{ URL::to('supplier/create') }}"><i class="fa fa-plus"></i> Add New Service Provider</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
						   
							<th>Name of Service Provider</th>
							<th>Category</th>
							<th>Address</th>
							<th>Status</th>
							<th>Remarks</th>
							
							<th>Contact Person</th>
							<th>Contact Number</th>
							<th>Email</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
						@foreach($provider as $supplier)
						<tr>
                            
							<td>{{ $supplier->name_of_service_provider }}</td>
							<td>{{ $supplier->category }} </td>
							<td>{{ $supplier->address}} </td>
							<td>{{ $supplier->status }}</td>
							<td>{{ $supplier->remarks }} </td>
							<td>{{ $supplier->contact_person }}</td>
							<td>{{ $supplier->contact_number }}</td>
							<td>{{ $supplier->email }}</td>
							<td>
								 <a class="btn btn-success btn" href="{{ URL::to('supplier/' .$supplier->provider_id	 ) }}">
		                             <i class="fa fa-eye"></i>  View Details 
		                            </a>
							</td>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
    
    </script>
@stop