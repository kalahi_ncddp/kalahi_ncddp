@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop
@section('content')
	@if ($errors->all())
		<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
	@endif

	<div class="panel panel-default">
		<div class="panel-heading">
			Records
			 @if($position->position!='encoder')
			    <span class="btn btn-default pull-right" id="approved">Review</span>
            @endif
			<a class="btn  btn-primary pull-right" href="{{ URL::to('project-coverage/create') }}"><i class="fa fa-plus"></i> Add New</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
						    @if($position->position=='ACT')
						    <th>Select all
						        <input type="checkbox" id="selecctall"/>
						    </th>
						    @endif
							<th>Employeed ID</th>
							<th>Sex</th>
							<th>Date of Birth</th>
							<th>Fund Source</th>
							
							<th>Office Division</th>
							<th>Status of Employment</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
						@foreach($staffs as $staff)
						<tr>
							<td>
                               @if($position->position=='ACT')
                                  @if($staff->is_draft==0)
                                    {{ $reference->approval_status($staff->employee_id, 'ACT') }}
                                  @else
                                      <span class="label label-warning">draft</span>
                                  @endif
                                @endif
                            </td>
                            
							<td>{{ $staff->employee_id }}</td>
							<td>{{ $staff->sex }} </td>
							<td>{{ toDate($staff->birthday) }}</td>
							<td>{{ $staff->fund_source }}</td>
							<td>{{ $staff->offices }} </td>
							<td>{{ $staff->emp_status }}</td>
							<td>
								 <a class="btn btn-success btn" href="{{ URL::to('project-coverage/' .$staff->employee_id ) }}">
		                              View Details 
		                            </a>
							</td>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
    
    </script>
@stop