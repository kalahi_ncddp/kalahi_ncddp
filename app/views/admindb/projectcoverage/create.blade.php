@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	
		@if ($errors->all())
			<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
		@endif
				
		{{ Form::open(array('url' => 'project-coverage')) }}
		<div class="col-md-12">
		 	<div class="box box-primary">
	            <div class="box-body">

						<!-- <div class="form-group">
							<label for="">Employee Id</label>
							{{ Form::input('text','employee_id','',['class'=>'form-control','required']) }}
						</div> -->
						<div class="form-group">
							<label for="">First Name</label>
							{{ Form::input('text','firstname','',['class'=>'form-control','required'])}}
						</div>
						<div class="form-group">
							<label for="">Middle Name</label>
							{{ Form::input('text','middlename','',['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Last Name</label>
							{{ Form::input('text','lastname','',['class'=>'form-control','required']) }}
						</div>
						<div class="form-group">
							<label for="">Sex</label>
							{{ Form::select('sex',[''=>'Select Gender','F'=>'Female','M'=>'Male'],'',['class'=>'form-control'])}}
						</div>
						<div class="form-group">
							<label for="">Date of Birth</label>
							{{ Form::input('text','birthday','',['class'=>'form-control date'])}}
						</div>
						<div class="form-group">
							<label for="">Place of Birth</label>
							{{ Form::input('text','place_birth','',['class'=>'form-control','required']) }}
						</div>
						<div class="form-group">
							<label for="">Current Position</label>
							{{ Form::select('curr_position', $curr_position ,'',['class'=>'form-control']) }}
						</div>
						<div class="form-group">
							<label for="">Contract start</label>
							{{ Form::input('text','contract_start', '' ,['class'=>'form-control date']) }}
						</div>
						<div class="form-group">
							<label for="">Contract end</label>
							{{ Form::input('text','contract_end', '' ,['class'=>'form-control date']) }}
						</div>
						<div class="form-group">
							<label for="">Salary Grade</label>
							{{ Form::input('text','salary_grade','',['class'=>'form-control']) }}
						</div>
						<div class="form-group">
							<label for="">Salary</label>
							{{ Form::input('text','currency','',['class'=>'form-control']) }}
						</div>

						<div class="form-group">
							<label for="">Fund Source</label>
							{{ Form::select('fund_source',[''=>'Select Fund Source']+$program,'',['class'=>'form-control']) }}
						</div>
						<div class="form-group">
							<label for="">Ofﬁce/Division/Unit</label>
							{{ Form::select('offices',[''=>'Select Office/Division/Unit']+$offices,'',['class'=>'form-control']) }}
						</div>
						<!-- <div class="form-group">
							<label for="">Office ID</label>
							{{ Form::input('text','office_id','',['class'=>'form-control']) }}
						</div> -->

						<div class="form-group">
							<label for="">Status of Employment</label>
							{{ Form::select('emp_status',[''=>'Select Status of Employment']+$statusofemployment,'',['class'=>'form-control']) }}
						</div>

						<div class="form-group">
							<label for="" name="ipc_rating">Latest IPCR Rating</label>
							{{ Form::input('number','ipc_rating','',['class'=>'form-control', 'id'=> 'test_id','min'=>'0','max'=>'100']) }}
						</div>
					

						<div class="form-group">
							<label for=""></label>
						</div>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-header">
					<h4>Employment History</h4>
					<div class="box-tools">
						
					</div>
				</div>
				<div class="box-body">
					<table class="table table-bordered">
						<thead>
							<th>Positions Held</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Option</th>
						</thead>
						<tbody class="emp_history">
							
						</tbody>
					</table>
					<span class="btn btn-primary col-md-12 add_history"><i class="fa fa-plus"></i> Add Employment History</span>
					<div class="clearfix"></div>
				</div>
			</div>

			<div class="box box-primary">
				<div class="box-header">
					<h4>Trainings Attended</h4>
					<div class="box-tools">
						
					</div>
				</div>
				<div class="box-body">
					<table class="table table-bordered">
						<thead>
							<th>Name of Trainings</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Conducted By</th>
							<th>NCDDP related Traning</th>
							<th>Option</th>
						</thead>
						<tbody class="emp_trainings">
							
						</tbody>
					</table>
					<span class="btn btn-primary col-md-12 add_training"><i class="fa fa-plus"></i> Add Training</span>
					<div class="clearfix"></div>
				</div>
			</div>

			<div class="form-group">
				{{ Form::submit('Submit ',['class'=>'btn btn-primary']) }}
			</div>
		{{ Form::close() }}
		</div>

		<script>
			$('.add_history').on('click',function(){
				var html = ' <tr> <td> <input type="text" name="position_held[]" class="form-control" /> </td> <td><input type="text" name="history_start[]" class="form-control date" /></td> <td><input type="text" name="history_end[]" class="form-control date" /></td> <td> <span class="btn"> <i class="fa fa-trash-o"></i></span> </td> </tr> ';
				$("body").find(".emp_history").append(html);

				$('.date').datetimepicker({
			            pick12HourFormat: false,
			            pickTime:false
				});
			});

			$('.add_training').on('click',function(){
				var html = ' <tr> <td> <input type="text" name="training_name[]" class="form-control" /> </td> <td><input type="text" name="training_start[]" class="form-control date" /></td> <td><input type="text" name="training_end[]" class="form-control date" /></td> <td><input type="text" name="conducted_by[]" class="form-control " /></td> <td><input type="checkbox" name="is_ncddp_related[]" /></td> <td> <span class="btn"> <i class="fa fa-trash-o"></i></span> </td> </tr> ';
				$("body").find(".emp_trainings").append(html);

				$('.date').datetimepicker({
			            pick12HourFormat: false,
			            pickTime:false
				});
			});
			$('.delete')

			 $(document).ready(function() {

			      		$("select[name='emp_status']").change(function() {
			         // alert($(this).val());
			         if($(this).val()=="Consultant/Specialist"){
			           	$("label[name='ipc_rating']").hide();
			           	
			            $("input[name='ipc_rating']").hide();
			            } else if($(this).val()=="COS/MOA Worker"){
			            $("label[name='ipc_rating']").show();
			           	
			            $("input[name='ipc_rating']").show();

			            } else if($(this).val()=="Plantilla"){
			            $("label[name='ipc_rating']").show();
			           	
			            $("input[name='ipc_rating']").show();

			            }
			          

			       });
				});

			// document.getElementById("test_id").onkeyup=function(){
			//     var input=parseInt(this.value);
			//     if(input<0 || input>100) {
			//     alert("Value should be between 0 - 100");
			// 	// $("input[name='ipc_rating']").value("100");
			// 	var current = document.getElementById("test_id");
			// 	current.value = 100	
			// }

		</script>
@stop