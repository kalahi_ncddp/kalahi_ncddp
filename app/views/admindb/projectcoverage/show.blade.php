@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<a class="btn btn-default" href="{{ URL::to('project-coverage') }}">Go Back</a>
		
			<div class="pull-right">
				{{ Form::open(array('url' => 'project-coverage/' . $staff->employee_id)) }}
				{{ Form::hidden('_method', 'DELETE') }}
				{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
				<a href="{{ URL::to('project-coverage/'.$staff->employee_id.'/edit') }}" class="btn btn-primary">EDIT</a>

				
			</div>
			<br><br>
			<div class="clearfix"></div>
		<div class="col-md-12">
		 	<div class="box box-primary">
	            <div class="box-body">

						<div class="form-group">
							<label for="">Employee Id</label>
							<span>{{ $staff->employee_id }}</span>
						</div>
						<div class="form-group">
							<label for="">First Name</label>
							<span> {{ $staff->firstname }}</span>
						</div>
						<div class="form-group">
							<label for="">Middle Name</label>
							<span> {{ $staff->middlename}}</span>
						</div>

						<div class="form-group">
							<label for="">Last Name</label>
							<span> {{ $staff->lastname }}</span>
						</div>
						<div class="form-group">
							<label for="">Sex</label>
							<span> {{ $staff->sex }}</span>
						</div>
						<div class="form-group">
							<label for="">Date of Birth</label>
							<span> {{ toDate($staff->birthday) }}</span>
						</div>
						<div class="form-group">
							<label for="">Place of Birth</label>
							<span> {{ $staff->place_birth }}</span>
						</div>
						<div class="form-group">
							<label for="">Current Position</label>
							<span> {{ $staff->curr_position }}</span>
						</div>
						<div class="form-group">
							<label for="">Contract start</label>
							<span> {{ toDate($staff->contract_start) }}</span>
						</div>
						<div class="form-group">
							<label for="">Contract end</label>
							<span> {{ toDate($staff->contract_end) }}</span>
						</div>
						<div class="form-group">
							<label for="">Salary Grade</label>
							<span> {{ $staff->salary_grade }}</span>
						</div>

						<div class="form-group">
							<label for="">Salary Currency</label>
							<span> {{ $staff->currency }}</span>
						</div>

						<div class="form-group">
							<label for="">Fund Source</label>
							<span> {{ $staff->fund_source }}</span>
						</div>
						<div class="form-group">
							<label for="">Ofﬁce/Division/Unit</label>
							<span> {{ $staff->offices }}</span>
						</div>
						<!-- <div class="form-group">
							<label for="">Office ID</label>
							<span> {{ $staff->office_id }}</span>
						</div> -->

						<div class="form-group">
							<label for="">Status of Employment</label>
							<span> {{ $staff->emp_status }}</span>
						</div>

						<div class="form-group">
							<label for="">IPC Rating</label>
							<span> {{ $staff->ipc_rating }}</span>
						</div>
					<!-- 	<div class="form-group">
							<label for="">IPCR Rating</label>
							<span> {{ $staff->ipcr_rating }}</span>
						</div> -->

						<div class="form-group">
							<label for=""></label>
						</div>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-header">
					<h4>Employment History</h4>
					<div class="box-tools">
						
					</div>
				</div>
				<div class="box-body">
					<table class="table table-bordered">
						<thead>
							<th>Positions Held</th>
							<th>Start Date</th>
							<th>End Date</th>
						</thead>
						<tbody class="emp_history">
							@foreach($emp_history as $emp)
								<tr>
									<td>{{ $emp->position }}</td>
									<td>{{ toDate($emp->startdate) }}</td>
									<td>{{ toDate($emp->enddate)}}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					<div class="clearfix"></div>
				</div>
			</div>

			<div class="box box-primary">
				<div class="box-header">
					<h4>Trainings Attended</h4>
					<div class="box-tools">
						
					</div>
				</div>
				<div class="box-body">
					<table class="table table-bordered">
						<thead>
							<th>Name of Trainings</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Conducted By</th>
							<th>NCDDP related Traning</th>
							<th>Option</th>
						</thead>
						<tbody class="emp_trainings">
								@foreach($emp_training as $emp)
								<tr>
									<td>{{ $emp->training_name }}</td>
									<td>{{ toDate($emp->startdate) }}</td>
									<td>{{ toDate($emp->enddate)}}</td>
									<td>{{ $emp->conducted_by }}</td>
									<td>{{ $emp->ncddp_related }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					<div class="clearfix"></div>
				</div>
			</div>

		
		</div>

	<script>	
		$(document).ready(function(){
				$("form").submit(function(){

					var confirmation = confirm('are you sure to this action?');
					if(confirmation){
						return true;
						
					}else{
						return false;
					}


				});
			});
	</script>
@stop