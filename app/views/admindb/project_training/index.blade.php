@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop
@section('content')
	@if ($errors->all())
		<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
	@endif

	<div class="panel panel-default">
		<div class="panel-heading">
			{{ $title }}
			 @if($position->position!='encoder')
			   <!--  <span class="btn btn-default pull-right" id="approved">Review</span> -->
            @endif
			<a class="btn  btn-primary pull-right" href="{{ URL::to('project-training/create') }}"><i class="fa fa-plus"></i> Add New Project Training</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
						   
							<th>Training Title</th>
							<th>Date Conducted</th>
							<th>No. of participants</th>
							<th>Conducted by</th>
							<th>Venue</th>
							
							<th>Level</th>
							
							<th>Training cost</th>
							<th>Fund source</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
						@foreach($project as $training)
						<tr>
                            
							<td>{{ $training->training_title }}</td>
							<td>{{ $training->date_conducted }} </td>
							<td>{{ $training->no_of_participants}} </td>
							<td>{{ $training->conducted_by }}</td>
							<td>{{ $training->venue }} </td>
							<td>{{ $training->level }}</td>

							<td>{{ $training->training_cost }}</td>
							<td>{{ $training->fund_source }}</td>
							<td>
								 <a class="btn btn-success btn" href="{{ URL::to('project-training/' .$training->training_id	 ) }}">
		                              View Details 
		                            </a>
							</td>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
    
    </script>
@stop