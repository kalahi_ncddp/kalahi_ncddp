@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	
		@if ($errors->all())
			<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
		@endif
				
		

		{{ Form::open(array('route' => array('project-training.update', $training->training_id), 'method' => 'patch', 'role' => 'form')) }}
		
		<div class="col-md-12">
		 	<div class="box box-primary">
	            <div class="box-body">



						
						<div class="form-group">
						<label for="">Training title</label>
							{{ Form::input('text','training_title',$training->training_title,['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Date conducted</label>
							{{ Form::input('text','date_conducted',$training->date_conducted,['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">No. of participants</label>
							{{ Form::input('text','no_of_participants',$training->no_of_participants,['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Conducted by</label>
							{{ Form::input('text','conducted_by',$training->conducted_by,['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Venue</label>
							{{ Form::input('text','venue',$training->venue,['class'=>'form-control','required']) }}
						</div>
						
						<div class="form-group">
							<label for="">Level</label>
							{{ Form::input('text','level',$training->level,['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Location data</label>
							{{ Form::input('text','location_data',$training->location_data,['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Training cost</label>
							{{ Form::input('text','training_cost',$training->training_cost,['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Fund Source</label>
							{{ Form::input('text','fund_source',$training->fund_source,['class'=>'form-control','required']) }}
						</div>

						




						

						
				</div>
			</div>
			

			

			<div class="form-group">
				{{ Form::submit('Submit ',['class'=>'btn btn-primary']) }}
			</div>
		{{ Form::close() }}
		</div>

		
@stop