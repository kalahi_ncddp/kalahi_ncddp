@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<a class="btn btn-default" href="{{ URL::to('project-training') }}">Go Back</a>
		<div class="pull-right">
				{{ Form::open(array('url' => 'project-training/' . $training->training_id)) }}
				{{ Form::hidden('_method', 'DELETE') }}
				{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
				<a href="{{ URL::to('project-training/'.$training->training_id.'/edit') }}" class="btn btn-primary">EDIT</a>

				
			</div>	
		<div class="col-md-12">
		 	<div class="box box-primary">
	            <div class="box-body">

						<div class="form-group">
							<label for="">Training Title</label>
							<span>{{ $training->training_title }}</span>
						</div>

						<div class="form-group">
							<label for="">Date Conducted</label>
							<span>{{ $training->date_conducted }}</span>
						</div>

						<div class="form-group">
							<label for="">Number of participants</label>
							<span>{{ $training->no_of_participants }}</span>
						</div>

						<div class="form-group">
							<label for="">Conducted by</label>
							<span>{{ $training->conducted_by }}</span>
						</div>

						<div class="form-group">
							<label for="">Venue</label>
							<span>{{ $training->venue }}</span>
						</div>
						<div class="form-group">
							<label for="">Level</label>
							<span>{{ $training->level }}</span>
						</div>
						<div class="form-group">
							<label for="">Location data</label>
							<span>{{ $training->location_data }}</span>
						</div>

						<div class="form-group">
							<label for="">Training cost</label>
							<span>{{ $training->training_cost }}</span>
						</div>

						<div class="form-group">
							<label for="">Fund source</label>
							<span>{{ $training->fund_source }}</span>
						</div>
						

			</div>
			
					<div class="clearfix"></div>
				</div>
			</div>

		
		</div>

	
@stop