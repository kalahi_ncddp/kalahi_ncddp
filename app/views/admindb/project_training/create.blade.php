@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	
		@if ($errors->all())
			<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
		@endif
				
		{{ Form::open(array('url' => 'project-training')) }}
		<div class="col-md-12">
		 	<div class="box box-primary">
	            <div class="box-body">

	            		<div class="form-group">
							<label for="">Training Title</label>
							{{ Form::input('text','training_title','',['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Date conducted</label>
							{{ Form::input('text','date_conducted','',['class'=>'form-control date'])}}
						</div>

						<div class="form-group">
							<label for="">No. of participants</label>
							<!-- {{ Form::input('text','no_of_participants','',['class'=>'form-control','required']) }} -->
							{{ Form::input('text', 'no_of_participants', Input::old('no_of_participants'), array('class' => 'form-control', 'required')) }}
					                
						</div>

						<div class="form-group">
							<label for="">Conducted by</label>
							{{ Form::input('text','conducted_by','',['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Venue</label>
							{{ Form::input('text','venue','',['class'=>'form-control','required']) }}
						</div>	
						
						<div class="form-group">
							<label for="">Level</label>
							{{ Form::select('level',[''=>'Select Level','NPMO'=>'NPMO','RPMO'=>'RPMO','SRMPO'=>'SRMPO','ACT'=>'ACT'],'',['class'=>'form-control'])}}
						</div>	

						<div class="form-group" >
							<label for="" name="location_data">Location data</label>
							<!-- {{ Form::input('text','location_data','',['class'=>'form-control','required']) }} -->
						</div>

						<div class="form-group">
							<label for=""  name="regions">Region</label>
							
							{{ Form::select('regional', $region_name, '', array('class' => 'form-control')) }}
						</div>

						<div class="form-group">
							<label for="" name="provinces">Province</label>
							{{ Form::input('text','province','',['class'=>'form-control']) }}
						</div>
						<div class="form-group">
							<label for="" name="municipals">Municipal</label>
							{{ Form::select('municipal', $municipalities, '',['class'=>'form-control']) }}
						</div>

						<div class="form-group">
							<label for="">Training cost</label>
							{{ Form::input('text','training_cost','',['class'=>'form-control','required']) }}
						</div>

						<div class="form-group">
							<label for="">Fund Source</label>
							{{ Form::select('fund_source',$program,'',['class'=>'form-control']) }}
						</div>
										
				</div>
			</div>
			
			<div class="form-group">
				{{ Form::submit('Submit ',['class'=>'btn btn-primary']) }}
			</div>

		{{ Form::close() }}
		</div>

		<script>
    $(document).ready(function() {
      $("select[name='level']").change(function() {
         // alert($(this).val());
         if($(this).val()=="NPMO"){
           	$("label[name='location_data']").hide();
           	$("label[name='regions']").hide();
           	$("label[name='provinces']").hide();
           	$("label[name='municipals']").hide();
            $("select[name='regional']").hide();
            $("select[name='municipal']").hide();
            $("input[name='province']").hide();
            
           

      }  else if($(this).val()=="RPMO"){
      		$("label[name='location_data']").show();
           	$("label[name='regions']").show();
           	$("label[name='provinces']").hide();
           	$("label[name='municipals']").hide();
            $("select[name='regional']").show();
            $("select[name='municipal']").hide();
            $("input[name='province']").hide();

      }else if($(this).val()=="SRMPO"){
      		$("label[name='location_data']").show();
           	$("label[name='regions']").show();
           	$("label[name='provinces']").show();
           	$("label[name='municipals']").hide();
            $("select[name='regional']").show();
            $("select[name='municipal']").hide();
            $("input[name='province']").show();

      } else if($(this).val()=="ACT"){
      		$("label[name='location_data']").show();
           	$("label[name='regions']").show();
           	$("label[name='provinces']").show();
           	$("label[name='municipals']").show();
            $("select[name='regional']").show();
            $("select[name='municipal']").show();
            $("input[name='province']").show();

      } else {
               
             }
      });
});


</script>

	
@stop