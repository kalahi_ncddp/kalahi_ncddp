@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('mibf') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
	        <div class="col-md-12">

				 <!-- {{ Form::open(array('url' => 'brgyprofile/' .$id. '/show_devprojs')) }} -->
				 {{ Form::open(array('url' => 'brgyprofile/' .$id. '/show_devprojs/delete', 'method' => 'POST')) }}

				 	<a class="btn  btn-small btn-info" href="{{ URL::to('brgyprofile/' .$id.'/edit_devproj') }}">
				 <i class="fa fa-edit"></i>		Edit 
				 	</a>

					<!-- {{ Form::hidden('_method', 'DELETE') }} -->
					{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}

			        <a class="btn bg-navy" href="{{ URL::to('brgyprofile/' .$devproj->profile_id. '/show_devprojs') }}">Close </a>
				 {{ Form::close() }}	 
	        </div>
	    </div>

	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
                <div class="panel panel-default">
		    		<div class="panel-heading">
		    			Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">



							<table class="table table-striped table-bordered table-hover">
							<tr>
				                <th>Project</th>
				                <td>
				                	<!-- {{ Form::text('proj_name', Input::old('proj_name'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->proj_name }}
				                </td>
				            </tr>
				            <tr>
				                <th>Location</th>
				                <td>
				                	<!-- {{ Form::text('location', Input::old('location'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->location }}
				                </td>
				            </tr>
				            <!-- <tr>
				                <th>Nature of Project</th>
				                <td>
				                	{{ Form::text('proj_nature', Input::old('proj_nature'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Project Duration</th>
				                <td>
				                	{{ Form::text('proj_duration', Input::old('proj_duration'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr> -->
				            
				            <tr>
				                <th>Scope/Coverage/Unit or Physical Target</th>
				                <td>
				                	<!-- {{ Form::text('scope_target', Input::old('scope_target'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->scope_target }}
				                </td>
				            </tr>
				            <tr>
				                <th>Cost of Project</th>
				                <td>
				                	<!-- {{ Form::text('proj_cost', Input::old('proj_cost'), array('class' => 'form-control', 'required')) }} -->
				                	{{ currency_format($devproj->proj_cost) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Source of Funds</th>
				                <td>
				                
								{{ $devproj->fund_source }}
				                </td>
				            </tr> 
				           <!--  <tr>
				                <th>Implementing Agency</th>
				                <td>
				                	{{ Form::text('impl_agency', Input::old('impl_agency'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr> -->
				            <tr>
				                <th>Cost Sharing (%)</th>
				                <td>
				                	<!-- {{ Form::input('number', 'cost_sharing', Input::old('cost_sharing'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->cost_sharing }}
				                </td>
				            </tr>        
						</table>

						<br>
						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:center"> Number of Beneficiaries  </th>
							</thead>
							<tr>
				                <th>Male</th>
				                <td>
				                	<!-- {{ Form::input('number', 'no_benemale', Input::old('no_benemale'), array('class' => 'form-control', 'min'=>'0', 'required')) }} -->
				                	{{ $devproj->no_benemale }}
				                </td>
				            </tr>
				            <tr>
				                <th>Female </th>
				                <td>
				                	<!-- {{ Form::input('number', 'no_benefemale', Input::old('no_benefemale'), array('class' => 'form-control', 'min'=>'0', 'required')) }} -->
				                	{{ $devproj->no_benefemale }}
				                </td>
				            </tr>
				             <tr>
				                <th>IPs</th>
				                <td>
				                	<!-- {{ Form::input('number', 'no_ips', Input::old('no_ips'), array('class' => 'form-control', 'min'=>'0', 'required')) }} -->
				                	{{ $devproj->no_ips }}
				                </td>
				            </tr>
				          <!--   <tr>
				                <th>Pantawid Pamilya HHS</th>
				                <td>
				                	{{ Form::input('number', 'no_pphousehold', Input::old('no_pphousehold'), array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>SLP HHS</th>
				                <td>
				                	{{ Form::input('number', 'no_slphousehold', Input::old('no_slphousehold'), array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr> -->
						</table>
						<h3>POVERTY </h3>
						<br>
						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"> Identity top three economics activities in the barangay  </th>
							</thead>
							<tr>
				                <th>1.</th>
				                <td>
				                	<!-- {{ Form::text('poverty_one', Input::old('poverty_one'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->poverty_one }}
				                </td>
				            </tr>
				            <tr>
				                <th>2. </th>
				                <td>
				                	<!-- {{ Form::text('poverty_two', Input::old('poverty_two'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->poverty_two }}
				                </td>
				            </tr>
				             <tr>
				                <th>3.</th>
				                <td>
				                	<!-- {{ Form::text('poverty_three', Input::old('poverty_three'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->poverty_three }}
				                </td>
				            </tr>
				         
						</table>

						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"> Identity major crops in the barangay </th>
							</thead>
							<tr>
				                <th>1.</th>
				                <td>
				                	<!-- {{ Form::text('crops_one', Input::old('crops_one'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->crops_one }}
				                </td>
				            </tr>
				            <tr>
				                <th>2. </th>
				                <td>
				                	<!-- {{ Form::text('crops_two', Input::old('crops_two'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->crops_two }}
				                </td>
				            </tr>
				             <tr>
				                <th>3.</th>
				                <td>
				                	<!-- {{ Form::text('crops_three', Input::old('crops_three'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->crops_three }}
				                </td>
				            </tr>
				         
						</table>

						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"> </th>
							</thead>
							<tr>
				                <th>What is the average annual household income in the barangay?</th>
				                <td>
				                	<!-- {{ Form::text('avg_annualhh', Input::old('avg_annualhh'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->avg_annualhh }}
				                </td>
				            </tr>
				            <tr>
				                <th>What is the average annual income for males? </th>
				                <td>
				                	<!-- {{ Form::text('avgannualincome_males', Input::old('avgannualincome_males'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->avgannualincome_males }}
				                </td>
				            </tr>
				             <tr>
				                <th>What is the average annual income for females?</th>
				                <td>
				                	<!-- {{ Form::text('avgannualincome_females', Input::old('avgannualincome_females'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->avgannualincome_females }}
				                </td>
				            </tr>
				             <tr>
				                <th>What is the average annual income for IPs?</th>
				                <td>
				                	<!-- {{ Form::text('avgannualincome_ips', Input::old('avgannualincome_ips'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->avgannualincome_ips }}
				                </td>
				            </tr>
				         
						</table>
						<br>
						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"> What types of road traverse through the barangay?</th>
							</thead>
							<tr>
				                <th><b> Types of Road </b></th>
				                <td>
				                <b> % </b>
				                </td>
				            </tr>
				            <tr>
				                <th>Dirt </th>
				                <td>
				                	<!-- {{ Form::input('number', 'dirt', Input::old('dirt'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->dirt }}
				                </td>
				            </tr>
				             <tr>
				                <th>Gravel</th>
				                <td>
				                	<!-- {{ Form::input('number', 'gravel', Input::old('gravel'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->gravel }}
				                </td>
				            </tr>
				             <tr>
				                <th>Asphalted</th>
				                <td>
				                	<!-- {{ Form::input('number', 'asphalted', Input::old('asphalted'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->asphalted }}
				                </td>
				            </tr>

				            <tr>
				                <th>Cemented/All Weather Road</th>
				                <td>
				                	<!-- {{ Form::input('number', 'cemented', Input::old('cemented'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->cemented }}
				                </td>
				            </tr>
				         
						</table>
						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"> What are the modes of transportation available?</th>
							</thead>
							<tr>
				                <th>1.</th>
				                <td>
				                	<!-- {{ Form::text('transpo_one', Input::old('transpo_one'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->transpo_one }}
				                </td>
				            </tr>
				            <tr>
				                <th>2.</th>
				                <td>
				                	<!-- {{ Form::text('transpo_two', Input::old('transpo_two'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->transpo_two }}
				                </td>
				            </tr>
				             <tr>
				                <th>3.</th>
				                <td>
				                	<!-- {{ Form::text('transpo_three', Input::old('transpo_three'), array('class' => 'form-control', 'required')) }} -->
				                	{{ $devproj->transpo_three }}
				                </td>
				            </tr>
				            
				         
						</table>







							
							<hr>
						</div>
					</div>
				</div>					
			</div>
		</div>
	<script>
			$("form").submit(function(){

            					var confirmation = confirm('are you sure to this action?');
            					if(confirmation){
            							return true;

            					}else{
            						return false;
            					}


            				});
		</script>
	
@stop