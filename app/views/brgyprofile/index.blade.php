@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif

	<div class="panel panel-default">
		<div class="panel-heading">
		{{ $title }}
			@if($position->position=='ACT')
                    <span class="btn btn-default pull-right" id="approved"  style="margin:0 10px">Review</span>
                    <span class="hidden module">brgyprofile</span>
             @endif
			<a class="btn  btn-primary pull-right" href="{{ URL::to('brgyprofile/create') }}"><i class="fa fa-plus"></i> Add New</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
						    <th>
                           @if($position->position=='ACT')
                            <input type="checkbox" id="selecctall"/>
                            @endif
                            </th>
							<th>Region</th>
							<th>Province</th>
							<th>Municipality</th>
							<th>Barangay</th>
							<th>Program</th>
							<th>Cycle</th>			
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $brgy_profile)
						<tr>
						    <td>
                               @if($position->position=='ACT')
                                  @if($brgy_profile->is_draft==0)
                                    {{ $reference->approval_status($brgy_profile->profile_id, 'ACT') }}
                                  @else
                                      <span class="label label-warning">draft</span>
                                  @endif
                                @endif
                            </td>
							<td>
								{{ $region = Report::get_region_by_brgy_id($brgy_profile->barangay_psgc) }}
						
							</td>
							<td>
								{{ $province = Report::get_province_by_brgy_id($brgy_profile->barangay_psgc) }}
							</td>
							<td>
								{{ $municipality = Report::get_municipality_by_brgy_id($brgy_profile->barangay_psgc) }}
							</td>
							<td>
								{{ $barangay = Report::get_barangay($brgy_profile->barangay_psgc) }}
							</td>
							<td>
								{{ $brgy_profile->program_id }}
							</td>
							<td>
								{{ $brgy_profile->cycle_id }}
							</td>
							
							 <td>
                        @if($brgy_profile->is_draft==0)

                        				 <a class="btn btn-success" href="{{ URL::to( 'brgyprofile/'.$brgy_profile->profile_id ) }}"><i class="fa fa-eye"></i> View Details
									    {{ hasComment($brgy_profile->profile_id) }}
									    </a>
                                     
                                    @else
                                     <a class="btn btn-warning" href="{{ URL::to( 'brgyprofile/'.$brgy_profile->profile_id.'/edit' ) }}">View Details( draft )
									    {{ hasComment($brgy_profile->profile_id) }}
									    </a>
                                      
                        @endif
                              
                                  </td> 


						<!-- 	<td>
		                            <a class="btn btn-success btn" href="{{ URL::to('brgyprofile/' .$brgy_profile->profile_id) }}">
		                              View Details {{ hasComment($brgy_profile->profile_id) }}
		                            </a>
	                        </td> -->
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();

      });
    </script>
@stop