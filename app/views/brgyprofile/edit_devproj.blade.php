@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	<div class="row">
	    </div>
	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
<a class="btn btn-default" href="{{ URL::to('brgyprofile/'.$id.'/show_devproj') }}">Go back</a>
				
				<div class="box box-primary">
	            	<div class="box-body">
				{{ Form::open(array('url' => 'brgyprofile/update_devproj', 'method' => 'POST', 'role' => 'form')) }}
					<div class="table-responsive">
<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<tr>
				                <th>Project</th>
				                <td>
				                	{{ Form::text('proj_name', $devproj->proj_name, array('class' => 'form-control','required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Location</th>
				                <td>
				                	{{ Form::text('location',$devproj->location, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				            <!-- <tr>
				                <th>Nature of Project</th>
				                <td>
				                	{{ Form::text('proj_nature', Input::old('proj_nature'), array('class' => 'form-control')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Project Duration</th>
				                <td>
				                	{{ Form::text('proj_duration', Input::old('proj_duration'), array('class' => 'form-control')) }}
				                </td>
				            </tr> -->
				            
				            <tr>
				                <th>Scope/Coverage/Unit or Physical Target</th>
				                <td>
				                	{{ Form::text('scope_target', $devproj->scope_target, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Cost of Project</th>
				                <td>
				                	{{ Form::currency('proj_cost',$devproj->proj_cost, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Source of Funds</th>
				                <td>
				                {{ Form::select('fund_source', array('' => 'Select Fund Source',
				                										  'Barangay Fund' => 'Barangay Fund',
				                										  'Municipal Fund' => 'Municipal Fund',
				                										  'Provincial Fund' => 'Provincial Fund',
				                										  'National Government Agencies' => 'National Government Agencies(e.g. DSWD, DA. etc)',
				                										  'ODA' => 'ODA (Foreign-assisted projects)',
				                										  'Others' => 'Others'),
																		 $devproj->fund_source ,array('class' => 'form-control')) }}
				                </td>
				            </tr> 
				           <!--  <tr>
				                <th>Implementing Agency</th>
				                <td>
				                	{{ Form::text('impl_agency', Input::old('impl_agency'), array('class' => 'form-control')) }}
				                </td>
				            </tr> -->
				            <tr>
				                <th>Cost Sharing (%)</th>
				                <td>
				                	{{ Form::input('text', 'cost_sharing', $devproj->cost_sharing, array('class' => 'form-control')) }}
				                </td>
				            </tr>        
						</table>


						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"> Identity top three economics activities in the barangay  </th>
							</thead>
							<tr>
				                <th>1.</th>
				                <td>
				                	{{ Form::text('proj_name', $devproj->poverty_one, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>2. </th>
				                <td>
				                	<!-- {{ Form::text('poverty_two', Input::old('poverty_two'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('poverty_two', $devproj->poverty_two, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				             <tr>
				                <th>3.</th>
				                <td>
				                	<!-- {{ Form::text('poverty_three', Input::old('poverty_three'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('poverty_three', $devproj->poverty_three, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				         
						</table>

						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"> Identity major crops in the barangay </th>
							</thead>
							<tr>
				                <th>1.</th>
				                <td>
				                	<!-- {{ Form::text('crops_one', Input::old('crops_one'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('crops_one', $devproj->crops_one, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>2. </th>
				                <td>
				                	<!-- {{ Form::text('crops_two', Input::old('crops_two'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('crops_two', $devproj->crops_two, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				             <tr>
				                <th>3.</th>
				                <td>
				                	<!-- {{ Form::text('crops_three', Input::old('crops_three'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('crops_three', $devproj->crops_three, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				         
						</table>

						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"> </th>
							</thead>
							<tr>
				                <th>What is the average annual household income in the barangay?</th>
				                <td>
				                	<!-- {{ Form::text('avg_annualhh', Input::old('avg_annualhh'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('avg_annualhh', $devproj->avg_annualhh, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>What is the average annual income for males? </th>
				                <td>
				                	<!-- {{ Form::text('avgannualincome_males', Input::old('avgannualincome_males'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('avgannualincome_males', $devproj->avgannualincome_males, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				             <tr>
				                <th>What is the average annual income for females?</th>
				                <td>
				                	<!-- {{ Form::text('avgannualincome_females', Input::old('avgannualincome_females'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('avgannualincome_females', $devproj->avgannualincome_females, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				             <tr>
				                <th>What is the average annual income for IPs?</th>
				                <td>
				                	<!-- {{ Form::text('avgannualincome_ips', Input::old('avgannualincome_ips'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('avgannualincome_ips', $devproj->avgannualincome_ips, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				         
						</table>
						<br>
						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"> What types of road traverse through the barangay?</th>
							</thead>
							<tr>
				                <th><b> Types of Road </b></th>
				                <td>
				                <b> % </b>
				                </td>
				            </tr>
				            <tr>
				                <th>Dirt </th>
				                <td>
				                	<!-- {{ Form::input('number', 'dirt', Input::old('dirt'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('dirt', $devproj->dirt, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				             <tr>
				                <th>Gravel</th>
				                <td>
				                	<!-- {{ Form::input('number', 'gravel', Input::old('gravel'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('gravel', $devproj->gravel, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				             <tr>
				                <th>Asphalted</th>
				                <td>
				                	<!-- {{ Form::input('number', 'asphalted', Input::old('asphalted'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('asphalted', $devproj->asphalted, array('class' => 'form-control')) }}
				                </td>
				            </tr>

				            <tr>
				                <th>Cemented/All Weather Road</th>
				                <td>
				                	<!-- {{ Form::input('number', 'cemented', Input::old('cemented'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('cemented', $devproj->cemented, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				         
						</table>
						<table class="table table-striped table-bordered table-hover">
							<thead> 
								<th colspan="2" style="text-align:left"> What are the modes of transportation available?</th>
							</thead>
							<tr>
				                <th>1.</th>
				                <td>
				                	<!-- {{ Form::text('transpo_one', Input::old('transpo_one'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('transpo_one', $devproj->transpo_one, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>2.</th>
				                <td>
				                	<!-- {{ Form::text('transpo_two', Input::old('transpo_two'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('transpo_two', $devproj->transpo_two, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				             <tr>
				                <th>3.</th>
				                <td>
				                	<!-- {{ Form::text('transpo_three', Input::old('transpo_three'), array('class' => 'form-control')) }} -->
				                	{{ Form::text('transpo_three', $devproj->transpo_three, array('class' => 'form-control')) }}
				                </td>
				            </tr>
				            
				         
						</table>
						












					<br>
				{{ Form::hidden('devproj_id', $id)}}
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{ Form::reset('Reset', array('class' => 'btn btn-danger')) }}
				{{ Form::close() }}
				</div>
			</div>
		</div>

		<script>
		    //helper function for custom validity
(function (exports) {
                            function valOrFunction(val, ctx, args) {
                                if (typeof val == "function") {
                                    return val.apply(ctx, args);
                                } else {
                                    return val;
                                }
                            }

                            function InvalidInputHelper(input, options) {
                                input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

                                function changeOrInput() {
                                    if (input.value == "") {
                                        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                                    } else {
                                        input.setCustomValidity("");
                                    }
                                }

                                function invalid() {
                                    if (input.value == "") {
                                        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                                    } else {
                                       console.log("INVALID!"); input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
                                    }
                                }

                                input.addEventListener("change", changeOrInput);
                                input.addEventListener("input", changeOrInput);
                                input.addEventListener("invalid", invalid);
                                changeOrInput();
                            }
                            exports.InvalidInputHelper = InvalidInputHelper;
                        })(window);
			$(document).ready(function(){


                $('.attendees').each(function(i,e){
                    InvalidInputHelper( e, {
                                      defaultText: "Please enter an email address!",

                                      emptyText: "Please enter an email address!",

                                      invalidText: function (input) {
                                        return 'Value must be less than or equal to the Represented Household in the Barangay (' + input.max + ')';
                                      }
                                    });
                });
                $('.family').each(function(i,e){
                                         InvalidInputHelper(e , {
                                                  defaultText: "Please enter an email address!",

                                                  emptyText: "Please enter an email address!",

                                                  invalidText: function (input) {
                                                    console.log(input);
                                                    return 'Value must be less than or equal to the Total Families in the Barangay (' + input.max + ')';
                                                  }
                                                });
                });

				$('#daterange').daterangepicker(
				  {
				    format: 'MM/DD/YYYY',
				    startDate : moment().format('MM/DD/YYYY'),
				    endDate : moment().format('MM/DD/YYYY')
				  },
				  function(start, end, label) {
				   	   	$('input[name="startdate"]').val(start.format('MM/DD/YYYY'));
				  		$('input[name="enddate"]').val(end.format('MM/DD/YYYY'));
				  }
				);
				//when the Add Filed button is clicked
				$("#add").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items").append('<div class="input-group"><input type="text" class="form-control" placeholder="Name of sitio eg. STO SITIO" name="sitio[]" oninput="checkduplicate(this)"><span class="input-group-addon"> <span class="fa fa-trash-o  delete"></span></span></div><br>');
					});

				$("body").on("click", ".delete", function (e) {
					$(this).parent().remove();
				});


				$("input[name='startdate']").change(function (e) {
					$("input[name='enddate']").attr('min', $("input[name='startdate']").val());
				});

				$("input[name='enddate']").change(function (e) {
					$("input[name='startdate']").attr('max', $("input[name='enddate']").val());
				});

				$("input[name='total_household']").change(function (e) {
					$("input[name='no_household']").attr('max', $("input[name='total_household']").val());
				});

				$("input[name='total_families']").change(function (e) {
					$("input[name='no_families']").attr('max', $("input[name='total_families']").val());
				});

				$("input[name='no_atnmale']").change(function (e) {
					$("input[name='no_ipmale']").attr('max', $("input[name='no_atnmale']").val());
					$("input[name='no_oldmale']").attr('max', $("input[name='no_atnmale']").val());
				});

				$("input[name='no_atnfemale']").change(function (e) {
					$("input[name='no_ipfemale']").attr('max', $("input[name='no_atnfemale']").val());
					$("input[name='no_oldfemale']").attr('max', $("input[name='no_atnfemale']").val());
				});

				$("input[name='no_household']").change(function (e) {
					$("input[name='no_pphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='no_slphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='no_iphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");
				});

				$("input[name='no_families']").change(function (e) {
//					$("input[name='no_ppfamilies']").attr('max', $("input[name='no_families']").val());
//					$("input[name='no_slpfamilies']").attr('max', $("input[name='no_families']").val());
//					$("input[name='no_ipfamilies']").attr('max', $("input[name='no_families']").val());
					$("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");
				});


				$("input[name='total_household']").change(function (e) {


					$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");
				});

				$("input[name='total_families']").change(function (e) {
				 $("input[name='no_ppfamilies']").attr('max', $("input[name='total_families']").val());
                                					$("input[name='no_slpfamilies']").attr('max', $("input[name='total_families']").val());
                                					$("input[name='no_ipfamilies']").attr('max', $("input[name='total_families']").val());
					$("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");
				});

               	$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");

                $("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");

			});

			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName('sitio[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}


			/* get the total in attendees */
			var total = (function($){
				return {
					
					totalPerson : function(){
						var total_ = parseInt($("input[name='no_atnfemale']").val()) + parseInt($("input[name='no_atnmale']").val()) ;
						$('.total_person').text(total_ || '0');
					},
					totalIP : function(){
						var total_ = parseInt($("input[name='no_ipfemale']").val()) + parseInt($("input[name='no_ipmale']").val()) ;
						$('.total_ip').text(total_ || '0');
					},
					totalOld : function(){
						var total_ = parseInt($("input[name='no_oldfemale']").val()) + parseInt($("input[name='no_oldmale']").val()) ;
						$('.total_old').text(total_ || '0');
					}

				}
			})(jQuery);
                total.totalPerson();
                total.totalIP();
                total.totalOld();

			$("input[name='no_atnmale']").keyup(total.totalPerson);
			$("input[name='no_ipmale']").keyup(total.totalIP);
			$("input[name='no_oldmale']").keyup(total.totalOld);
			$("input[name='no_atnfemale']").keyup(total.totalPerson);
			$("input[name='no_ipfemale']").keyup(total.totalIP);
			$("input[name='no_oldfemale']").keyup(total.totalOld);
			


		</script>
@stop