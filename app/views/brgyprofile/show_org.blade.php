@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('mibf') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
	        <div class="col-md-12">

				 {{ Form::open(array('url' => 'brgyprofile/' .$id. '/show_orgs/delete', 'method' => 'POST')) }}
				 	<a class="btn  btn-small btn-info" href="{{ URL::to('brgyprofile/' .$id.'/edit_org') }}">
				 		<i class="fa fa-edit"></i>Edit 
				 	</a>

				
					{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
					
			        <a class="btn bg-navy" href="{{ URL::to('brgyprofile/' .$brgy_org->profile_id. '/show_orgs') }}">Close </a>
				 {{ Form::close() }}	 
	        </div>
	    </div>

	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
                <div class="panel panel-default">
		    		<div class="panel-heading">
		    			Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
							<tr>
				                <th>Name</th>
				                <td>
				                	{{ $brgy_org->org_name }}

				                </td>
				            </tr>
				            <tr>
				                <th>Type of Organization</th>
				                <td>
				                	{{ $brgy_org->org_type}}
				                	
				                </td>
				            </tr>
				            <tr>
				                <th>Formal (Registered)</th>
				                <td>
				                	{{ $brgy_org->is_registered}}
				                	
				                </td>
				            </tr>
				            <tr>
				                <th>LGU-Accredited</th>
				                <td>
				                	{{ $brgy_org->is_lguaccredited}}
				                	<!-- {{ Form::select('is_lguaccredited', array('' => 'Yes or No?',1 => 'Yes', 0 => 'No'), Input::old('is_lguaccredited'), array('class' => 'form-control')) }} -->
				                </td>
				            </tr>
				            <tr>
				                <th>Advocacy</th>
				                <td>
				                	{{ $brgy_org->advocacy}}
				                	<!-- {{ Form::text('advocacy', Input::old('advocacy'), array('class' => 'form-control', 'placeholder'=>'Savings, religious, farmers, fisherfolk, women')) }} -->
				                </td>
				            </tr>
				            <tr>
				                <th>Area of operation</th>
				                <td>
				                	{{ $brgy_org->area_operation}}
				                	<!-- {{ Form::text('area_operation', Input::old('area_operation'), array('class' => 'form-control','placeholder'=>'ex. nationwide, municipal, diff. brgys., etc')) }} -->
				                </td>
				            </tr>
				            <tr>
				                <th>Years operation in brgy</th>
				                <td>
				                	{{ $brgy_org->years_operation}}
				                	<!-- {{ Form::input('number','years_operation', Input::old('years_operation'), array('class' => 'form-control','min'=>'0','placeholder'=>'')) }} -->
				               		
				                </td>
				            </tr>

				            <tr>
				                <th>Active or Inactive organizations</th>
				                <td>
				                	{{ $brgy_org->active_inactive_org}}
				                	<!-- {{ Form::select('active_inactive_org', array(''=> 'Active or Inactive?',1 => 'Active', 0 => 'Inactive'), Input::old('active_inactive_org'), array('class' => 'form-control')) }} -->
				                </td>
				            </tr>

				            <tr>
				                <th>Activities</th>
				                <td>
				                	{{ $brgy_org->activities}}
				                	<!-- {{ Form::text('activities', Input::old('activities'), array('class' => 'form-control','placeholder'=>'ex. nationwide, municipal, diff. brgys., etc')) }} -->
				                </td>
				            </tr>

				            <tr>
				                <th>Total male members from brgy</th>
				                <td>
				                	{{ $brgy_org->total_male_members_brgy}}
				                	<!-- {{ Form::input('number','total_male_members_brgy', Input::old('total_male_members_brgy'), array('class' => 'form-control','min'=>'0','placeholder'=>'')) }} -->
				                </td>
				            </tr>

				            <tr>
				                <th>Total female members from brgy</th>
				                <td>
				                	{{ $brgy_org->total_female_members_brgy}}
				                	<!-- {{ Form::input('number','total_female_members_brgy', Input::old('total_female_members_brgy'), array('class' => 'form-control','min'=>'0','placeholder'=>'')) }} -->
				                </td>
				            </tr>
				            <tr>
				                <th>Male IP members from brgy</th>
				                <td>
				                	{{ $brgy_org->male_ip_members}}
				                	<!-- {{ Form::input('number','male_ip_members', Input::old('male_ip_members'), array('class' => 'form-control','min'=>'0','placeholder'=>'')) }} -->
				                </td>
				            </tr>

				            <tr>
				                <th>Female IP members from brgy</th>
				                <td>
				                	{{ $brgy_org->female_ip_members}}
				                	<!-- {{ Form::input('number','female_ip_members', Input::old('female_ip_members'), array('class' => 'form-control','min'=>'0','placeholder'=>'ex. nationwide, municipal, diff. brgys., etc')) }} -->
				                </td>
				            </tr>
				            <tr>
				                <th>Marginalized sectors represented</th>
				                <td>
				                	{{ $brgy_org->sector_rep}}
				                	<!-- {{ Form::text('sector_rep', Input::old('sector_rep'), array('class' => 'form-control','placeholder'=>'Marginalized sectors')) }} -->
				                	<!-- {{ Form::select('sector_rep', array('Sector 1' => 'Sector 1', 'Sector 2' => 'Sector 2'), Input::old('sector_rep'), array('class' => 'form-control', 'required')) }} -->
				                </td>
				            </tr>			            
						</table>

							
						</div>
					</div>
				</div>					
			</div>
		</div>
	<script>
			$("form").submit(function(){

            					var confirmation = confirm('are you sure to this action?');
            					if(confirmation){
            							return true;

            					}else{
            						return false;
            					}


            				});
		</script>
	
@stop