@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
    <a class="btn btn-default" href="{{ URL::to('brgyprofile/'.$id) }}">Go back</a>
	<div class="panel panel-default">
		<div class="panel-heading">
			Organizations that operate in the barangay
			<a class="btn  btn-primary pull-right" href="{{ URL::to('brgyprofile/'.$id.'/create_org') }}"><i class="fa fa-plus"></i> Add New</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>Name</th>
							<th>Type of Organization</th>
							<th>Formal (Registered)</th>
							<th>LGU-accredited</th>
							<th>Advocacy/Thrust</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $brgy_org)
						<tr>
							<td>
								{{ $brgy_org->org_name }}
							</td>
							<td>
								{{ $brgy_org->org_type }}
							</td>
							@if($brgy_org->is_registered == 0)
								<td>No</td>
							@else
								<td>Yes</td>
							@endif
							@if($brgy_org->is_lguaccredited == 0)
								<td>No</td>
							@else
								<td>Yes</td>
							@endif
							<td>
								{{ $brgy_org->advocacy }}
							</td>
							<td>
	                            <a class="btn btn-success btn" href="{{ URL::to('brgyprofile/' .$brgy_org->org_id. '/show_org') }}">
	                             <i class="fa fa-eye"></i> View Details 
	                            </a>
	                        </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop