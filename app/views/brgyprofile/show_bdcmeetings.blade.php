@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
<a class="btn btn-default" href="{{ URL::to('brgyprofile/'.$id) }}">Go Back</a>
	<div class="panel panel-default">
		<div class="panel-heading">
			Past Year Activities (BA/BDC meetings)
			<a class="btn  btn-primary pull-right" href="{{ URL::to('brgyprofile/'.$id.'/create_meeting') }}"><i class="fa fa-plus"></i> Add New</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>Male resident present</th>
							<th>Female resident present</th>
							<th>IP households present</th>
							<th>other sectors present</th>

							<th>No. of male BLGU officials</th>
							<th>No. of female BLGU officials</th>
							<th>No. of male PO/CBO representatives</th>
							<th>No. of female PO/CBO representatives</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $brgy_bdcmeet)
							<tr>
								<td>
									{{ $brgy_bdcmeet->no_avgmale }}
								</td>
								<td>
									{{ $brgy_bdcmeet->no_avgfemale }}
								</td>
								<td>
									{{ $brgy_bdcmeet->no_iphouseholds }}
								</td>
								<td>
									{{ $brgy_bdcmeet->sectors_rep }}
								</td>
								<td>
									{{ $brgy_bdcmeet->no_bdcmale }}
								</td>
								<td>
									{{ $brgy_bdcmeet->no_bdcfemale }}
								</td>
								<td>
									{{ $brgy_bdcmeet->no_bdcmalerep }}
								</td>
								<td>
									{{ $brgy_bdcmeet->no_bdcfemalerep }}
								</td>
								<td>
		                            <a class="btn btn-success btn" href="{{ URL::to('brgyprofile/' .$brgy_bdcmeet->meeting_id. '/show_meeting') }}">
		                   <i class="fa fa-eye"></i>           View Details 
		                            </a>
		                        </td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop