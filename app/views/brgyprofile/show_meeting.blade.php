@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('mibf') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
	        <div class="col-md-12">

				 <!-- {{ Form::open(array('url' => 'brgyprofile/' .$id. '/show_bdcmeetings')) }} -->
				 {{ Form::open(array('url' => 'brgyprofile/' .$id. '/show_bdcmeetings/delete', 'method' => 'POST')) }}
				 	<a class="btn  btn-small btn-info" href="{{ URL::to('brgyprofile/' .$id.'/edit_meeting') }}">
				 	<i class="fa fa-edit"></i>	Edit 
				 	</a>

					<!-- {{ Form::hidden('_method', 'DELETE') }} -->
					{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}

			        <a class="btn bg-navy" href="{{ URL::to('brgyprofile/' .$meeting->profile_id. '/show_bdcmeetings') }}">Close </a>
				 {{ Form::close() }}	 
	        </div>
	    </div>

	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
                <div class="panel panel-default">
		    		<div class="panel-heading">
		    			Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">


							<table class="table table-striped table-bordered table-hover">
								<tr>
					                <th>During the past year, how many times were the BAs conducted?</th>
					                
					                	<td>{{ $meeting->bas_conducted }}</td>
					               
					                
					            </tr>
					            <tr>
					                <th>On the average, how many barangay households are present during the meetings?</th>
					                <td>
					                	<!-- {{ Form::input('number', 'no_baconducted', Input::old('no_baconducted'), array('class' => 'form-control', 'required')) }} -->
					            </tr>
					             <tr>
					             	<ul>
					            	<th><li>How many male residents were present?</li> </th>
					               
					               	<td>{{ $meeting->no_avgmale }}</td>
					               	
					               </ul>
					            </tr>
					            <tr>
					             	<ul>
					            	<th><li>How many female residents were present?</li> </th>
					              	 <td>{{ $meeting->no_avgfemale }}</td>
					               </ul>
					            </tr>
					            <tr>
					             	<ul>
					            	<th><li>How many IP households were present?</li> </th>
					                <td>{{ $meeting->no_iphouseholds }}</td>
					               </ul>
					            </tr>
					            <tr>
					             	<ul>
					            	<th><li>What other sectors were present?</li> </th>
					                <td>{{ $meeting->sectors_rep }}</td>
					               </ul>
					            </tr>
					            <tr>
					                <th>Duringthe past year, how many times did the BDC meet?</th>
					                <td>{{ $meeting->no_bdcmeet }}</td>
					                	<!-- {{ Form::input('number', 'no_baconducted', Input::old('no_baconducted'), array('class' => 'form-control', 'required')) }} -->
					            </tr>
					            <tr>
					                <th>How many, on the average, attend the BDC meetings(refer to attendance sheet)</th>
					                <td>
					                	<!-- {{ Form::input('number', 'no_baconducted', Input::old('no_baconducted'), array('class' => 'form-control', 'required')) }} -->
					            </tr>
					             <tr>
					             	<ul>
					            	<th><li>No. of male BLGU officials</li> </th>
					                <td>{{ $meeting->no_bdcmale }}</td>
					               </ul>
					            </tr>
					            <tr>
					            	<ul>
					            	<th><li>No. of female BLGU officials</li></th>
					                <td>{{ $meeting->no_bdcfemale }}</td>
					               </ul>
					            </tr>
					            
					            <tr><ul>
					            	<th><li>No. of male PO/CBO representatives</li> </th>
					                <td>{{ $meeting->no_bdcmalerep }}</td>
					            </tr></ul>
					            <tr>
					            	<ul>
					            	<th><li>No. of female PO/CBO representatives</li> </th>
					               <td>{{ $meeting->no_bdcfemalerep }}</td>
					               	<ul>
					            </tr>

					            <!-- <tr>
					                <th>How many times did the BDC meet?</th>
					                <td>
					                	{{ Form::input('number', 'no_bdcmeet', Input::old('no_bdcmeet'), array('class' => 'form-control', 'required')) }}
					                </td>
					            </tr>
					            <tr>
					                <th>What sectors were present on BAs meetings?</th>
					                <td>
					                	{{ Form::textarea('sectors_rep', Input::old('sectors_rep'), array('class' => 'form-control', 'required')) }}
					                </td>
					            </tr>	 -->	            
							</table>
							
						</div>
					</div>
				</div>					
			</div>
		</div>
	<script>
			$("form").submit(function(){

            					var confirmation = confirm('are you sure to this action?');
            					if(confirmation){
            							return true;

            					}else{
            						return false;
            					}


            				});
		</script>
	
@stop