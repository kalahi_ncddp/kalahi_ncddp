@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				<!-- if there are creation errors, they will show here -->
		
		<div class="col-md-12">
		 	<div class="box box-primary">
	            <div class="box-body">
					{{ Form::open(array('url' => 'brgyprofile')) }}
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Barangay <i class="text-red">*</i></label>
							{{ Form::select('barangay', [''=>'Select Barangay']+$brgy_list, Input::old('barangay'), array('class' => 'form-control', 'required','id'=>'barangay')) }}
							<!-- $purpose =  $purpose; -->
						</div>
						
						<div class="form-group">
							{{ Form::label('psgc_id', 'PSGC Code') }}
							{{ Form::text('psgc_id', Input::old('psgc_id'), array('class' => 'form-control', 'required', 'readonly')) }}
						</div>
						
						<div class="form-group hidden">
							{{ Form::label('kc_code', 'KC-NCDDP Code') }}
							{{ Form::text('kc_code', Input::old('kc_code'), array('class' => 'form-control')) }}
						</div>
						
						<div class="form-group">
							{{ Form::label('municipality', 'Municipality') }}
							{{ Form::text('municipality', $municipality, array('class' => 'form-control', 'readonly')) }}
						</div>
						
						<div class="form-group">
							{{ Form::label('province', 'Province') }}
							{{ Form::text('province', $province, array('class' => 'form-control', 'readonly')) }}
						</div>

						<div class="form-group">
							<label for="">Program <i class="text-red">*</i></label>
							{{ Form::select('program_id', [''=>'Select Program']+$program_list, Input::old('program'), array('class' => 'form-control', 'required')) }}
						</div>
						<div class="form-group">
							<label for="">Cycle <i class="text-red">*</i></label>
							{{ Form::select('cycle_id', [''=>'Select Cycle']+$cycle_list, Input::old('cycle_id'), array('class' => 'form-control', 'required')) }}
						</div>
						
						
						{{ Form::submit('Add Barangay Profile', array('class' => 'btn btn-primary')) }}
					
					 <!-- clear floats -->
					 <a class="btn bg-navy" href="{{ URL::to('brgyprofile')}}">Close</a>
					{{ Form::close() }}
				</div>
					 <div class="clearfix"></div>
			</div>
		</div>
		
		<script>
			$(document).ready(function(){
				$("#barangay").change(function (e) {
					$('.hidden').hide()
					$("#psgc_id").attr('value', $("#barangay").val());
					var selector = "option[value='" +$("#barangay").val()+ "']";
					$(selector).attr('selected', 'selected');
				});		

			
			});
		</script>
@stop