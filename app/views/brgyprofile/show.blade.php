@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
		@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
		
	      
	    <!-- /. ROW  -->
	    
	    
    <div class="col-md-12">
    	@if($brgy_profile->is_draft==1)
	        		 	<div class="alert alert-info">This is still a draft  <a href="{{ URL::to('brgyprofile/' .$brgy_profile->profile_id.'/edit') }}">edit the document</a> to set it in final draft</div>
	         @endif
	         @if( !is_review($brgy_profile->profile_id) )
    	{{ Form::open(array('url' => 'brgyprofile/' . $brgy_profile->profile_id)) }}
		{{ Form::hidden('_method', 'DELETE') }}
		{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
		<a class="btn  btn-small btn-info" href="{{ URL::to('brgyprofile/' .$brgy_profile->profile_id.'/edit') }}">
		 	<i class="fa fa-edit"></i>Edit
		</a>
			@endif
		<a class="btn btn-small btn-success" href="{{ URL::to('brgyprofile/'.$brgy_profile->profile_id.'/show_orgs') }}">Brgy. Organization</a>
		<a class="btn btn-small btn-success" href="{{ URL::to('brgyprofile/'.$brgy_profile->profile_id.'/show_blgu') }}">BLGU Officials Profile</a>
		<a class="btn btn-small btn-success" href="{{ URL::to('brgyprofile/'.$brgy_profile->profile_id.'/show_bdcp') }}">BDC Profile</a>
		<a class="btn btn-small btn-success" href="{{ URL::to('brgyprofile/'.$brgy_profile->profile_id.'/show_bdcmeetings') }}">BA/BDC Meetings</a>
		<a class="btn btn-small btn-success" href="{{ URL::to('brgyprofile/'.$brgy_profile->profile_id.'/show_devprojs') }}">Brgy. Dev. Projects</a>
		<a class="btn bg-navy" href="{{ URL::to('brgyprofile') }}">Close</a>


		 {{ Form::close() }}
	 	<!--Basic info-->
	 	<div class="col-md-12">

		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Basic Information</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	            <div class="box-body">
					<div class="table-responsive">
						<table class="table table-striped  table-hover">
				            <tr>
				                <th>Region</th>
				                <td>{{ $region = Report::get_region_by_brgy_id($brgy_profile->barangay_psgc) }}</td>
				            </tr>
				            <tr>
				                <th>Province</th>
				                <td>{{ $province = Report::get_province_by_brgy_id($brgy_profile->barangay_psgc) }}</td>
				            </tr>
				            <tr>
				                <th>Municipality</th>
				                <td>{{ $municipality = Report::get_municipality_by_brgy_id($brgy_profile->barangay_psgc) }}</td>
				            </tr>
						 	<tr>
				                <th>Barangay</th>
				                <td>{{ $barangay = Report::get_barangay($brgy_profile->barangay_psgc) }}</td>
				            </tr>
				             <!-- <tr>
				                <th>Cycle</th>
				                <td>{{ $brgy_profile->cycle_id }}</td>
				            </tr>
				              <tr>
				                <th>Program</th>
				                <td>{{ $brgy_profile->program_id }}</td>
				            </tr> -->
				            <tr>
				                <th>Number of Sitios/Purok</th>
				                <td>{{ $brgy_profile->no_sitios}}
			           			</td>
				            </tr>
				            <tr>
			                <th>Year Source</th>
			                <td>
			                	{{$brgy_profile->year_source}}
			                </td>
			            </tr>
						</table>
					</div>
	            </div>

	        </div>
	 	</div>
	 	
	 	
	 	
	 	<!--Population -->
    	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Population Profile</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					<table class="table table-striped table-bordered table-hover">
						<tr>
			                <th>Total number of households</th>
			                <td>
			                	{{$brgy_profile->no_households}}
			                </td>
			            </tr>
			            <tr>
			                <th>Total number of families</th>
			                <td>
			                	{{$brgy_profile->no_families}}
			                </td>
			            </tr>
			            <tr>
			            	<th>No. of male</th>
			            	<td>
			            		{{$brgy_profile->no_male}}
			            	</td>
			            </tr>
			            <tr>
			            	<th>No. of female</th>
			            	<td>
			            		{{$brgy_profile->no_female}}
			            	</td>
			            </tr>
			            <tr>
			            	<th>No. of male children ages 0-5 years old</th>
			            	<td>
			            		{{$brgy_profile->no_male0_5}}
			            	</td>
			            </tr>

			            <tr>
			            	<th>No. of female children ages 0-5 years old</th>
			            	<td>
			            		{{$brgy_profile->no_female0_5}}
			            	</td>
			            </tr>

			             <tr>
			            	<th>No. of male elementary school aged (children ages 6-12 years old)</th>
			            	<td>
			            		{{$brgy_profile->no_male6_12}}
			            	</td>
			            </tr>

			            <tr>
			            	<th>No. of female elementary school aged (children ages 6-12 years old)</th>
			            	<td>
			            		{{$brgy_profile->no_female6_12}}
			            	</td>
			            </tr>

			            <tr>
			            	<th>No. of male secondary school aged (children ages 13-16 years old)</th>
			            	<td>
			            		{{$brgy_profile->no_male13_16}}
			            	</td>
			            </tr>

			            <tr>
			            	<th>No. of female secondary school aged (children ages 13-16 years old)</th>
			            	<td>
			            		{{$brgy_profile->no_female13_16}}
			            	</td>
			            </tr>

			            <tr>
			            	<th>Total male voting population</th>
			            	<td>
			            		{{$brgy_profile->no_voting_male}}
			            	</td>
			            </tr>

			             <tr>
			            	<th>Total female voting population</th>
			            	<td>
			            		{{$brgy_profile->no_voting_female}}
			            	</td>
			            </tr>

			            <tr>
			            	<th>Total male labor force</th>
			            	<td>
			            		{{$brgy_profile->no_labor_male}}
			            	</td>
			            </tr>

			            <tr>
			            	<th>Total female labor force</th>
			            	<td>
			            		{{$brgy_profile->no_labor_female}}
			            	</td>
			            </tr>


			            


			           <!--  <tr>
			                <th>Female Headed Household in the Brgy</th>
			                <td>
			                	{{$brgy_profile->no_fmheadedhh}}
			                </td>
			            </tr>
			            <tr>
			                <th>IP Household in the Brgy</th>
			                <td>
			                	{{$brgy_profile->no_iphouseholds}}
			                </td>
			            </tr>
			            
			            <tr>
			                <th>IP Family in the Brgy</th>
			                <td>
			                	{{$brgy_profile->no_ipfamily}}
			                </td>
			            </tr>
			            
			            
			            <tr>
			                <th>Number of Persons with Disability</th>
			                <td>
			                	{{$brgy_profile->no_withdisability}}
			                </td>
			            </tr>
			            <tr>
			                <th>Number of Internally Displaced Persons</th>
			                <td>
			                	{{$brgy_profile->no_indisplaced}}
			                </td>
			            </tr> -->
			            
					</table>
					
					
					<br>
					<!-- <table class="table table-striped table-bordered table-hover">
			            <tr>
			            	<th>Headcount </th>
			                <th>Male</th>
			                <th>Female</th>
			               	<th>Total</th>
			            </tr>
			            <tr>
			            	<th>No. of Person </th>
			                <td>
			                	{{$brgy_profile->no_male}}
			               	</td>
			                <td>
			                	{{$brgy_profile->no_female}}
			                </td>
							<td class="total_person">{{ $brgy_profile->no_male + $brgy_profile->no_female}}</td>
			            </tr>
			            <tr>
			            	<th>Ages 0 to 5 years old </th>
			                <td>
			                	{{ $brgy_profile->no_male0_5}}
			               	</td>
			                <td>
			                	{{ $brgy_profile->no_female0_5}}
			                </td>
							<td class="total_0_5">{{$brgy_profile->no_male0_5 + $brgy_profile->no_female0_5 }}</td>
			            </tr>
			            <tr>
			            	<th>No. of elementary school aged children (children 6 to 12 years old) </th>
			                <td>
			                	{{ $brgy_profile->no_male6_12 }}
			               	</td>
			                <td>
			                	{{ $brgy_profile->no_female6_12 }}
			                </td>
							<td class="total_6_12">{{ $brgy_profile->no_male6_12 + $brgy_profile->no_female6_12 }}</td>
			            </tr>
			            <tr>
			            	<th>No. of secondary school aged children (children 13 to 16 years old</th>
			                <td>
			                	{{ $brgy_profile->no_male13_16 }}
			               	</td>
			                <td>
			                	{{ $brgy_profile->no_female13_16 }}
			                </td>
							<td class="total_13_16">{{ $brgy_profile->no_male13_16 + $brgy_profile->no_female13_16}}</td>
			            </tr>
			            <tr>
			            	<th>Ages 17 to 59 years old (Labor Force) </th>
			                <td>
			                	{{ $brgy_profile->no_male17_59 }}
			               	</td>
			                <td>
			                	{{ $brgy_profile->no_female17_59 }}
			                </td>
							<td class="total_17_59">{{ $brgy_profile->no_male17_59 + $brgy_profile->no_female17_59}}</td>
			            </tr>
			            <tr>
			            	<th>Ages 60 years old and above (Senior) </th>
			                <td>
			                	{{ $brgy_profile->no_male60up }}
			               	</td>
			                <td>
			                	{{ $brgy_profile->no_female60up }}
			                </td>
							<td class="total_60_up">{{ $brgy_profile->no_male60up + $brgy_profile->no_female60up}} </td>
			            </tr>
			            <tr>
			                <th> Total voting population</th>
			                <td>
			                    {{ $brgy_profile->no_voting_male }}
			                </td>
			                <td>
                                {{ $brgy_profile->no_voting_female }}
                            </td>
                            <td class="total_voting">{{ $brgy_profile->no_voting_male +  $brgy_profile->no_voting_female }}</td>
			            </tr>
					</table> -->
			 	</div>
			 </div>
		 </div>
		 
		 <!--IP Profile -->
	 	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Indigenous People</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	            <div class="box-body">
					<div class="box-body">
						<div class="table-responsive">
							<table id="items7" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<tr>
					          			<th>IP group</th>
					          			<th>Location/Sitio </th>
					          			<!-- <th>No of Male Headed Household </th>
					          			<th>No of Female Headed Household </th> -->
					          			<th>Total Households </th> 
					          			<th>Total Families </th>
					          			<th>Total Male </th>
					          			<th>Total Female </th>
					          			
			
					          		</tr>
					          	</thead>
					            @foreach($brgy_ipprof as $ipprof)
					            	<tr>
					            		<td>						            		
					            			{{$ipprof->ip_group}}
					            		</td>
					            		<td>						            		
					            			{{$ipprof->ip_location}}
					            		</td>
					            		<!-- <td>						            		
					            			{{$ipprof->ip_no_malehdhh}}
					            		</td>	
					            		<td>						            		
					            			{{$ipprof->ip_no_femalehdhh}}
					            		</td> -->
					            		<td>						            		
					            			{{$ipprof->ip_no_malehdhh}}
					            			<!-- {{$ipprof->ip_no_malehdhh+$ipprof->ip_no_femalehdhh}} -->

					            		</td>
					            		<td>						            		
					            			<!-- {{$ipprof->ip_no_male+$ipprof->ip_no_female}} -->
					            			{{$ipprof->ip_no_femalehdhh}}
					            			
					            		</td>
					            		<td>						            		
					            			{{$ipprof->ip_no_male}}
					            		</td>
					            		<td>						            		
					            			{{$ipprof->ip_no_female}}
					            		</td>
					            		
					            		
					            	</tr>



					            @endforeach
					          		<tr>
					            			<td colspan="2">
					            			TOTAL:
					            		</td>
					            		<td></td>
					            		<td></td>
					            		<td></td>
					            		<td></td>
					            		</tr>
							</table>
							<!-- <table id="items7" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<tr>
					          			<th>IP group</th>
					          			<th>Location/Sitio </th>
					          			<th>No of Male Headed Household </th>
					          			<th>No of Female Headed Household </th>
					          			<th>Total Households </th> 
					          			<th>Total Families </th>
					          			<th>Total Male </th>
					          			<th>Total Female </th>
					          			
			
					          		</tr>
					          	</thead>
							 @foreach($brgy_ipprof as $ipprof)
									
					            		
					            		<td> </td>
					            		<td> </td>
					            		<td>{{$ipprof->ip_no_male}} </td>
					            	</tr>
					          @endforeach
					      </table> -->
							<br>
					        
						</div>
					</div>
							
	            </div>

	        </div>
	 	</div>

	 	<!--Conflict Affected/ Pantawid Pamilya and SLP Beneficiaries -->
	 	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Conflict Affected/ Pantawid Pamilya and SLP Beneficiaries </h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	            <div class="box-body">
					<div class="box-body">
						<div class="table-responsive">
							<table id="items7" class="table table-striped table-bordered table-hover">
					          <div class="form-inline">
					     <!--     {{ Form::checkbox('is_brgyaffected', '0', $brgy_profile->is_brgyaffected) }}  -->
					         {{ Form::checkbox('is_brgyaffected', '1', $brgy_profile->is_brgyaffected, array( 'disabled')) }} 
							
							 {{ Form::label('is_brgyaffected', 'Is the barangay affected by armed conflict?') }}	
							&nbsp;
							
							<br>
							{{ Form::label('baragay_additiondetails', 'if Yes, Please give additional details of the armed conflict in the area')}}
						
							{{ Form::textarea('baragay_additiondetails', $brgy_profile->baragay_additiondetails, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
							<br>
							<br>

							{{ Form::label('no_pantawid_household', 'No. of Pantawid Pamilya houshold beneficiaries in the barangay')}}
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$brgy_profile->no_pantawid_household}}
							<br>
							<br>
							{{ Form::label('no_pantawid_family', 'No. of Pantawid Pamilya family beneficiaries in the barangay')}}
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $brgy_profile->no_pantawid_family }}
							<br>
							<br>
							{{ Form::label('no_slp_household', 'No. of SLP household beneficiaries in the barangay')}}
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $brgy_profile->no_slp_household }}
							<br>
							<br>
							{{ Form::label('no_slp_family', 'No. of SLP family beneficiaries in the barangay')}}
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $brgy_profile->no_slp_family }}



							</td>
			            </tr>
					</div>
					           
					          
							</table>
							<br>
					        
						</div>
					</div>
							
	            </div>

	        </div>
	 	</div>

	 	<!--Area Profile -->
	 	<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Area profile</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
	           <div class="box-body">
					<div class="form-inline">
							<!-- {{ Form::checkbox('is_poblacion', '1', $brgy_profile->is_poblacion, array( 'id' => 'is_poblacion', 'disabled')) }}  -->
							{{ Form::label('is_poblacion', 'Is this a poblacion?') }}
							{{ Form::radio('is_poblacion', 0, $brgy_profile->is_poblacion == 1 ? 1 : 0, array( 'id' => 'is_poblacionyes', 'disabled')) }} YES
							{{ Form::radio('is_poblacion', 1, $brgy_profile->is_poblacion == 0 ? 1 : 0, array( 'id' => 'is_poblacionno', 'disabled')) }} NO

							<br>
							<br>
							{{ Form::label('', 'If not, how many hours does it take to travel to Poblacion? ') }}
							{{ Form::text('hrs_totown', $brgy_profile->hrs_totown, array('class' => 'form-control', 'disabled')) }} hrs
							{{ Form::text('mins_totown', $brgy_profile->mins_totown, array('class' => 'form-control', 'disabled')) }} mins

							<br>
							<br>
							{{ Form::label('', 'No. of kilometers from the Poblacion') }}
							{{ Form::text('km_frmtown', round($brgy_profile->km_frmtown,3), array('class' => 'form-control', 'disabled')) }} kms
							
					</div>
					<br>
					<div class="form-inline">
							{{ Form::label('is_upland', 'Geographic Characteristic of the Barangay') }}	
							&nbsp;
							{{ Form::checkbox('is_upland', '', $brgy_profile->is_upland, array( 'disabled')) }} Upland
							&nbsp;
							{{ Form::checkbox('is_hilly', '1', $brgy_profile->is_hilly, array( 'disabled')) }} Hilly
							&nbsp;
							{{ Form::checkbox('is_lowland', '1', $brgy_profile->is_lowland, array( 'disabled')) }} Lowland
							&nbsp;
							{{ Form::checkbox('is_island', '1', $brgy_profile->is_island, array( 'disabled')) }} Island
							&nbsp;
							{{ Form::checkbox('is_coastal', '1', $brgy_profile->is_coastal, array( 'disabled')) }} Coastal
					</div>
					<br>
					<div class="form-inline">
							{{ Form::checkbox('is_isolated', '1', $brgy_profile->is_isolated, array( 'disabled')) }} 
							{{ Form::label('is_isolated', 'Is this barangay isolated from other barangays?') }}
					</div>
					<div class="form-inline">
							{{ Form::label('Affected by any of the following peace and social cohesion issues?') }}
					</div>
					<div class="form-inline">
							{{ Form::checkbox('is_armedconflict', '1', $brgy_profile->is_armedconflict, array( 'disabled')) }} 
							Armed conflict
					</div>
					<div class="form-inline">
							{{ Form::checkbox('is_bounddispute', '1', $brgy_profile->is_bounddispute, array( 'disabled')) }} 
							Boundary and territorial disputes
					</div>
					<div class="form-inline">
							{{ Form::checkbox('is_poldispute', '1', $brgy_profile->is_poldispute, array( 'disabled')) }} 
							Political and extra judicial killings
					</div>
					<div class="form-inline">
							{{ Form::checkbox('is_genderviolence', '1', $brgy_profile->is_genderviolence, array( 'disabled')) }} 
							Family and gender-based violence
					</div>
					<div class="form-inline">
							{{ Form::checkbox('is_rido_war', '1', $brgy_profile->is_rido_war, array( 'disabled')) }} 
							RIDO or clan wars/Tribal wars or ‘pangayao’
					</div>
					<div class="form-inline">
							{{ Form::checkbox('is_crime', '1', $brgy_profile->is_crime, array( 'disabled')) }} 
							Crime
					</div>
					<br>
					
					
					<br>
					<div class="box-body">
						<div class="table-responsive">
							<table id="items" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="4" style="text-align:center">Environmental Critical Areas </th>
					          	</thead>
					          	<tr>
					          			<th>Name of Location</th>
					          			<th>Description </th>
					          			
					          		</tr>
					            @foreach($brgy_envcrit as $envcrit)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Location" value="{{$envcrit->location}}" name="location[]" oninput="checkduplicate(this)" readonly required>
					            		</td>
					            		<td>
						            		<textarea  class="form-control" placeholder="e.g. prone to landslide" readonly name="description[]">{{$envcrit->description}}</textarea>
					            		</td>
					            		
					            		<!-- <td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td> -->
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					       
						</div>
					</div>

					<div class="table-responsive">
							<table id="items4" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="3" style="text-align:center">Sources of Funds </th>
					          	</thead>
					          		<tr>
					          			<th>Fund Source</th>
					          			<th>Amount </th>
					          			
					          		</tr>
					          	
					            @foreach($brgy_fundsource as $fundsource)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Fund Source" value="{{$fundsource->fund_source}}" name="fund_source[]" oninput="checkduplicate(this)" readonly required>
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" placeholder="Amount (PhP)" value="{{$fundsource->amount}}" name="amount[]" readonly required>
					            		</td>
					            		<!-- <td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td> -->
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					       
					</div>
							
	            </div>

	        </div>
	 	</div>
	 	
	 	<!--Financial Resources -->
		<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Financial Resources</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					<table id="budget" class="table table-striped table-bordered table-hover">
						<thead> 
							<th colspan="2" style="text-align:left"> How were the barangay funds allocated in the previous year?(Refer to Barangay Annual Investment Plan) </th>
						</thead>
						<tr>
			                <th>Environment</th>
			                <td>
			                	{{ Form::input('text', 'alloc_env', currency_format($brgy_profile->alloc_env), array('class' => 'form-control', 'min'=>'0', 'readonly'=>'readonly', 'required')) }}
			                </td>
			            </tr>
			            <tr>
			                <th>Economic sector </th>
			                <td>
			                	{{ Form::input('text', 'alloc_econ', currency_format($brgy_profile->alloc_econ), array('class' => 'form-control', 'min'=>'0', 'readonly'=>'readonly','required')) }}
			                </td>
			            </tr>
			            <tr>
			                <th>Insfrastructure</th>
			                <td>
			                	{{ Form::input('text', 'alloc_infra', currency_format($brgy_profile->alloc_infra), array('class' => 'form-control', 'min'=>'0','readonly'=>'readonly', 'required')) }}
			                </td>
			            </tr>
			            <tr>
			                <th>Social Development</th>
			                <td>
			                	{{ Form::input('text', 'alloc_basic', currency_format($brgy_profile->alloc_basic), array('class' => 'form-control', 'min'=>'0', 'readonly'=>'readonly','required')) }}
			                </td>
			            </tr>
			            <!-- <tr>
			                <th>Education</th>
			                <td>
			                	{{ Form::input('number', 'alloc_educ', $brgy_profile->alloc_educ, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
			            </tr>
			            <tr>
			                <th>Peace and order</th>
			                <td>
			                	{{ Form::input('number', 'alloc_peace', $brgy_profile->alloc_peace, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
			            </tr> -->
			            <tr>
			                <th>Institutional Sector</th>
			                <td>
			                	{{ Form::input('text', 'alloc_inst', currency_format($brgy_profile->alloc_inst), array('class' => 'form-control', 'min'=>'0','readonly'=>'readonly', 'required')) }}
			                </td>
			            </tr>
			            <tr>
			                <th> Gender and Development</th>
			                <td>
			                	{{ Form::input('text', 'alloc_gender', currency_format($brgy_profile->alloc_gender), array('class' => 'form-control', 'min'=>'0','readonly'=>'readonly', 'required')) }}
			                </td>
			            </tr>
			            <tr>
			                <th> DRRM</th>
			                <td>
			                	{{ Form::input('text', 'alloc_drrm', currency_format($brgy_profile->alloc_drrm), array('class' => 'form-control', 'min'=>'0', 'readonly'=>'readonly','required')) }}
			                </td>
			            </tr>
			            
			            <tr>
			                <th> Other Allocations</th>
			                <td>
			                	{{ Form::input('text', 'alloc_others', currency_format($brgy_profile->alloc_others), array('class' => 'form-control', 'min'=>'0','readonly'=>'readonly', 'required')) }}
			                </td>
			            </tr>
			            <tr>
			            	<th> Total </th>
			            	<td class="totalbudget"> {{ currency_format($brgy_profile->alloc_env + $brgy_profile->alloc_econ + $brgy_profile->alloc_infra + $brgy_profile->alloc_basic + $brgy_profile->alloc_educ + $brgy_profile->alloc_peace + $brgy_profile->alloc_inst + $brgy_profile->alloc_gender + $brgy_profile->alloc_drrm + $brgy_profile->alloc_others)}}</td>
			            </tr>
					</table>
					
					<br>
					
					
					
					
			 	</div>
			 </div>
		 </div>

		 <!--5 % GAD fund utilize -->
		<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">How was the 5% GAD fund utlized by the barangay?</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="table-responsive">
							<table id="items_5" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		
					          	</thead>
					          		<tr>
					          			<th>Activity</th>
					          			<th>Cost </th>
					          			<!-- <th>Option</th> -->
					          		</tr>
					          	
					            @foreach($brgy_gadfund as $gadfund)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Fund Source" value="{{$gadfund->activity}}" name="activity[]" oninput="checkduplicate(this)" readonly="readonly" required>
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" placeholder="Amount (PhP)" value="{{$gadfund->cost}}" name="cost[]" readonly="readonly" required>
					            		</td>
					            		
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					       <!--  <button id="add_5" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add GAD Fund</button> -->
					</div>
			 </div>
		 </div>
		 
		 
		 <!--Other Resources -->
		<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Other Resources</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<!-- <div class="box-body">
					
					<div class="table-responsive">
							<table id="items5" class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="6" style="text-align:center">Technical Assistance Resources </th>
					          	</thead>
					          		<tr>
					          			<th>Equipment Type</th>
					          			<th>Current Condition </th>
					          			<th>Current Capability </th>
					          			<th>Fuel, POL Product Consumption </th>
					          			<th>Prevailing Rental Sales </th>
					          			<th>Option</th>
					          		</tr>
					          	
					            @foreach($brgy_techresource as $techresource)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Equipment Type" value="{{$techresource->equipt_type}}" name="equipt_type[]" oninput="checkduplicate(this)" required>
					            		</td>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Current Condition" value="{{$techresource->curr_condition}}" name="curr_condition[]" required>
					            		</td>
					            		<td>						            		
					            			<input type="text" class="form-control" placeholder="Current Capability" value="{{$techresource->curr_capability}}" name="curr_capability[]" required>
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" placeholder="Consumption" value="{{$techresource->consumption}}" name="consumption[]" required>
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" placeholder="Rental Rate" value="{{$techresource->rental_rate}}" name="rental_rate[]" required>
					            		</td>
					            		<td>
					            			<button type="button" class="btn delete">
					            				<i class="fa fa-trash-o"></i>
					            			</button>
					            		</td>
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        <button id="add5" class="btn btn-info form-control" type="button" >
					            	<i class="fa fa-plus"></i>
					            	Add Resource</button>
					</div> -->
					
					<br>
					
					<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="6" style="text-align:center">Facility Resources </th>
					          	</thead>
					          		<tr>
					          			<th>Facility/Establishments/Services</th>
					          			<th>Please check if Avalaible? </th>
					          			<th>If none, distance to nearest facility(hours) </th>
					          			<th>Mode of Transportation </th>
					          			<!-- <th>Fare </th> -->
					          		</tr>
					          	
					            @foreach($brgy_facility as $facility)
					            	<tr>
					            		<td>						            		
					            			<input type="text" class="form-control" value="{{$facility->facility_name}}" name="facility_name[]" readonly required>
					            		</td>
					            		<td>						            		
					            			{{ Form::checkbox('facility_available[]', $facility->facility_name, $facility->facility_available , array( 'disabled')) }} 
					            		</td>
					            		<td>						            		
					            			<input type="number" class="form-control" value="{{round($facility->facility_distance,2)}}" name="facility_distance[{{$facility->facility_name}}]" readonly>
					            		</td>
					            		<td>						            		
					            			<input type="text" class="form-control" value="{{$facility->facility_transpo}}" name="facility_transpo[{{$facility->facility_name}}]" readonly >
					            		</td>
					            	
					            	</tr>
					            @endforeach
					          
							</table>
							<br>
					        
					</div>
					<br>


					<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
					          	<thead>
					          		<th colspan="6" style="text-align:center">What are the main problems in the barangay? Identify. </th>
					          	</thead>
					          		<tr>
					          			<th>Problems</th>
					          			<th>Details </th>
					          			<th>Are these problems being addressed properly? </th>
					          			<th>Remarks </th>
					          			<!-- <th>Fare </th> -->
					          		</tr>
					          	
					            
					            	<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Access/ Mobility') }}	
					            		</td>
					            		<td>						            		
												{{ Form::textarea('access_details', $brgy_profile->access_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            	
					            		</td>
					            		<td>						            		
												{{ Form::textarea('access_addressed', $brgy_profile->access_addressed, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            		
					            		</td>
					            		<td>						            		
												{{ Form::textarea('access_remarks', $brgy_profile->access_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            		
					            		
					            	</tr>

					            	<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Water and Sanitation') }}	
					            		</td>
					            		<td>						            		
												{{ Form::textarea('water_details', $brgy_profile->water_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            		</td>
					            		<td>						            		
												{{ Form::textarea('water_address', $brgy_profile->water_address, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            	
					            		</td>
					            		<td>						            		
												{{ Form::textarea('water_remarks', $brgy_profile->water_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            		
					            		</td>
					            	
					            		
					            	</tr>

					            	<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Health and Nutrition') }}	
					            		</td>
					            		<td>						            		
												{{ Form::textarea('health_details', $brgy_profile->health_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            		
					            		</td>
					            		<td>						            		
												{{ Form::textarea('health_address', $brgy_profile->health_address, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            		
					            		</td>
					            		<td>						            		
												{{ Form::textarea('health_remarks', $brgy_profile->health_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            	
					            		</td>
					            	
					            		
					            	</tr>

					            	<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Literacy/ Education') }}	
					            		</td>
					            		<td>						            		
												{{ Form::textarea('literacy_details', $brgy_profile->literacy_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            	
					            		</td>
					            		<td>						            		
												{{ Form::textarea('literacy_addressed', $brgy_profile->literacy_addressed, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            		
					            		</td>
					            		<td>						            		
												{{ Form::textarea('literacy_remarks', $brgy_profile->literacy_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            	
					            		</td>
					          
					            		
					            	</tr>

					            		<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Employment/income Generation') }}	
					            		</td>
					            		<td>						            		
												{{ Form::textarea('employment_details', $brgy_profile->employment_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					           
					            		</td>
					            		<td>						            		
												{{ Form::textarea('employment_addressed', $brgy_profile->employment_addressed, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            
					            		</td>
					            		<td>						            		
												{{ Form::textarea('employment_remarks', $brgy_profile->employment_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            	
					            		</td>
					            
					            		
					            	</tr>

					            	<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Land Ownership/Asset Distribution') }}	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('landownership_details', $brgy_profile->landownership_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('landownership_addressed', $brgy_profile->landownership_addressed, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            
					            		</td>
					            		<td>						            		
											{{ Form::textarea('landownership_remarks', $brgy_profile->landownership_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					         
					            		</td>
					            	
					            		
					            	</tr>

					            	<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Agricultural Service Facilities (i.e. post-harvest)') }}	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('agriculture_details', $brgy_profile->agriculture_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            		
					            		</td>
					            		<td>						            		
											{{ Form::textarea('agriculture_addressed', $brgy_profile->agriculture_addressed, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            		
					            		</td>
					            		<td>						            		
											{{ Form::textarea('agriculture_remarks', $brgy_profile->agriculture_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            		
					            		</td>
					            	
					            		
					            	</tr>

					            	<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Peace and Order') }}	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('peace_details', $brgy_profile->peace_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('peace_addressed', $brgy_profile->peace_addressed, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('peace_remarks', $brgy_profile->peace_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            		
					            		</td>
					           
					            		
					            	</tr>

					            	<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Environment') }}	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('environment_details', $brgy_profile->environment_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('environment_addressed', $brgy_profile->environment_addressed, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('environment_remarks', $brgy_profile->environment_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            		
					            		</td>
					            	
					            		
					            	</tr>

					            	<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Power Supply') }}	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('powersupply_details', $brgy_profile->powersupply_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('powersupply_addressed', $brgy_profile->powersupply_addressed, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('powersupply_remarks', $brgy_profile->powersupply_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            			
					            		
					            		</td>
					           
					            		
					            	</tr>

					            	<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Communications') }}	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('communication_details', $brgy_profile->communication_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            	
					            		</td>
					            		<td>
											{{ Form::textarea('communication_addressed', $brgy_profile->communication_addressed, array('class' => 'form-control','rows'=>'4', 'disabled')) }}

					            	
					            		</td>
					            		<td>						            		
											{{ Form::textarea('communication_remarks', $brgy_profile->communication_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            
					            		</td>
					          
					            		
					            	</tr>

					            	<tr>
					            		<td>						            		
					            			{{ Form::label('', 'Others, Specify	') }}	
					            		</td>
					            		<td>
											{{ Form::textarea('others_details', $brgy_profile->others_details, array('class' => 'form-control','rows'=>'4', 'disabled')) }}

					            		</td>
					            		<td>		
											{{ Form::textarea('others_addressed', $brgy_profile->others_addressed, array('class' => 'form-control','rows'=>'4', 'disabled')) }}

					            		</td>
					            		<td>		
											{{ Form::textarea('others_remarks', $brgy_profile->others_remarks, array('class' => 'form-control','rows'=>'4', 'disabled')) }}
					            						            		
					            			
					            		</td>
					            
					            	</tr>


					          
					          
							</table>
							<br>
					        
					</div>
					
					
			 	</div>
			 </div>
		 </div>
		 
		
		
		<!--Poverty Situation -->
		<div class="col-md-12">
		 	<div class="box box-primary">
		 		<div class="box-header">
		 			 <h3 class="box-title">Poverty Profile</h3>
		 			 <div class="box-tools pull-right">
                        <span class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
		 		</div>
			 	<div class="box-body">
					<br>
					<table class="table table-striped table-bordered table-hover">
			            <tr>
			            	<th>Health  </th>
			               <!--  <th>Male</th>
			                <th>Female</th> -->
			               	<th>Value</th>
			               	<th>Reference</th>
			            </tr>
			             <tr>
			            	<td>1. Proportion of children aged 0-5 years old who died:</td>
			               
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of children aged 0-5 years old who died</li></td>
			              	<td>
			                	{{ Form::input('number', 'health_number_0_5_value', $brgy_profile->health_number_0_5_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
			            </ul>
							<td>{{ Form::text('health_number_0_5_reference', $brgy_profile->health_number_0_5_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Total children aged 0-5 years old</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'children_0_5_value', $brgy_profile->children_0_5_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('children_0_5_refrence', $brgy_profile->children_0_5_refrence, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>
			           
						 <tr>
			            	<td>2. Proportion of women who died due to pregnancy-related causes</td>
			               
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of pregnant women who died due to pregnancy-related causes</li></td>
			               	<td>
			                	{{ Form::input('number', 'pregnant_died_value', $brgy_profile->pregnant_died_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
			            </ul>
							<td>{{ Form::text('pregnant_died_reference', $brgy_profile->pregnant_died_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Total pregnant women</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'pregnant_total_value', $brgy_profile->pregnant_total_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('pregnant_total_reference', $brgy_profile->pregnant_total_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			             <tr>
			            	<th>Nutrition  </th>
			            
			            </tr>
			             <tr>
			            	<td>3. Proportion of children aged 0-5 years old who are malnourished</td>
			               
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of children aged 0-5 years old who malnourished</li></td>
			                <td>
			                	{{ Form::input('number', 'malnourished_0_5value', $brgy_profile->malnourished_0_5value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
			            </ul>
							<td>{{ Form::text('malnourished_0_5reference', $brgy_profile->malnourished_0_5reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Total children aged 0-5 years old</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'total_malnourished_0_5value', $brgy_profile->total_malnourished_0_5value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('total_malnourished_0_5reference', $brgy_profile->total_malnourished_0_5reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>
			           <!--  <tr>
			            	<td>No. of women who died due to pregnancy</td>
			                <td colspan="3">
			                	{{ Form::input('number', 'no_diedpreg', $brgy_profile->no_diedpreg, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
							<td>{{ Form::text('sourse_diedpreg', $brgy_profile->sourse_diedpreg, array('class' => 'form-control')) }}
			           		</td>
			            </tr> -->
						 <tr>
			            	<th>Access to Basic Amenities</th>
			               
			            </tr>
			            
			            <tr>
			            	<td>4. Proportion of households with access to safe water</td>
			              
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of households with access to safe water(within 250m)</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'safewater_value', $brgy_profile->safewater_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('safewater_reference', $brgy_profile->safewater_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			            <ul>
			            <tr>
			            	<td><li>Total number of households</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'totalsafewater_value', $brgy_profile->totalsafewater_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('totalsafewater_reference', $brgy_profile->totalsafewater_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			            <tr>
			            	<td>5. Proportion of households with access to sanity toilet facilities</td>
			              
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of households with access to sanitary toilet facilities</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'sanity_value', $brgy_profile->sanity_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('sanity_reference', $brgy_profile->sanity_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			            <ul>
			            <tr>
			            	<td><li>Total number of households</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'totalsanity_value', $brgy_profile->totalsanity_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('totalsanity_reference', $brgy_profile->totalsanity_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>


			             <tr>
			            	<th>Shelter</th>
			               
			            </tr>
			            
			            <tr>
			            	<td>6. Proportion of households who are squatting</td>
			              
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of households who are squatting</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'squatting_value', $brgy_profile->squatting_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('squatting_reference', $brgy_profile->squatting_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			            <ul>
			            <tr>
			            	<td><li>Total number of households</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'totalsquatting_value', $brgy_profile->totalsquatting_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('totalsquatting_reference', $brgy_profile->totalsquatting_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			            <tr>
			            	<td>7. Proportion of households with living in makeshift housing</td>
			              
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of households living in makeshift housing</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'makeshift_value', $brgy_profile->makeshift_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('makeshift_reference', $brgy_profile->makeshift_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			            <ul>
			            <tr>
			            	<td><li>Total number of households</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'totalmakeshift_value', $brgy_profile->totalmakeshift_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('totalmakeshift_reference', $brgy_profile->totalmakeshift_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>


			            <tr>
			            	<th>Peace and Order</th>
			               
			            </tr>
			            
			            <tr>
			            	<td>8. Proportion of households with members victimized by crimes</td>
			              
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of households with members victimized by crimes</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'victimized_value', $brgy_profile->victimized_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('victimized_reference', $brgy_profile->victimized_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			            <ul>
			            <tr>
			            	<td><li>Total number of households</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'totalvictimized_value', $brgy_profile->totalvictimized_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('totalvictimized_reference', $brgy_profile->totalvictimized_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			            <tr>
			            	<th> Income </th>
			              
			            </tr>
			             <tr>
			            	<td>9. Proportion of households with income less the poverty threshold</td>
			              
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of households with income less than the poverty threshold</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'threshold_value', $brgy_profile->threshold_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('threshold_reference', $brgy_profile->threshold_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			             <ul>
			            <tr>
			            	<td><li>Total number of households</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'totalthreshold_value', $brgy_profile->totalthreshold_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('totalthreshold_reference', $brgy_profile->totalthreshold_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			             <tr>
			            	<td>10. Proportion of households with income less the food threshold</td>
			              
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of households with income less than the food threshold</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'incomeless_value', $brgy_profile->incomeless_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('incomeless_reference', $brgy_profile->incomeless_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			             <ul>
			            <tr>
			            	<td><li>Total number of households</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'totalincomeless_value', $brgy_profile->totalincomeless_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('totalincomeless_reference', $brgy_profile->totalincomeless_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>



			             <tr>
			            	<td>11. Proportion of households who eat less than three(3) meals days</td>
			              
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of households who eat less than three(3) meals days</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'lessthan_3_meals_value', $brgy_profile->lessthan_3_meals_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('lessthan_3_meals_reference', $brgy_profile->lessthan_3_meals_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			             <ul>
			            <tr>
			            	<td><li>Total number of households</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'totallessthan_3_meals_value', $brgy_profile->totallessthan_3_meals_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('totallessthan_3_meals_reference', $brgy_profile->totallessthan_3_meals_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			            <th>Basic Education </th>
			             <tr>
			            	<td>12. Proportion of 6-12 years old children who are not in elementary school</td>
			              
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of 6-12 years old children who are not in elementary school</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'children_6_12_elem_value', $brgy_profile->children_6_12_elem_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('children_6_12_elem_reference', $brgy_profile->children_6_12_elem_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			             <ul>
			            <tr>
			            	<td><li>Total number of children 6-12 years old</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'totalchildren_6_12_elem_value', $brgy_profile->totalchildren_6_12_elem_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('totalchildren_6_12_elem_reference', $brgy_profile->totalchildren_6_12_elem_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			            <tr>
			            	<td>13. Proportion of 13-16 years old children who are not in secondary</td>
			              
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of 13-16 years old children who are not in secondary</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'children_13_16_secondary_value', $brgy_profile->children_13_16_secondary_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('children_13_16_secondary_reference', $brgy_profile->children_13_16_secondary_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			             <ul>
			            <tr>
			            	<td><li>Total number of children 13-16 years old</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'totalchildren_13_16_secondary_value', $brgy_profile->totalchildren_13_16_secondary_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('totalchildren_13_16_secondary_reference', $brgy_profile->totalchildren_13_16_secondary_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			            <tr>
			            	<td>14. Proportion of the members of the labor force who are not working</td>
			              
			            </tr>
			            <ul>
			            <tr>
			            	<td><li>Number of labor force who are not working</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'laborforce_value', $brgy_profile->laborforce_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('laborforce_reference', $brgy_profile->laborforce_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>

			             <ul>
			            <tr>
			            	<td><li>Total number of labor force</td></li>
			               <!--  <td>
			                	{{ Form::input('number', 'no_mkidmaln', $brgy_profile->no_mkidmaln, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td> -->
			               <td>
			                	{{ Form::input('number', 'totallaborforce_value', $brgy_profile->totallaborforce_value, array('class' => 'form-control', 'min'=>'0', 'required', 'readonly'=>'readonly')) }}
			                </td>
							<!-- <td class="total_kidmaln">{{$brgy_profile->no_mkidmaln + $brgy_profile->no_fkidmaln}}</td> -->
							<td>{{ Form::text('totallaborforce_reference', $brgy_profile->totallaborforce_reference, array('class' => 'form-control', 'readonly'=>'readonly')) }}
			           		</td>
			           	</ul>
			            </tr>


			            <!-- END OF POVERTY PROFILE -->
			            <!--  <tr>
			            	<th>Education </th>
			                <th>Male</th>
			                <th>Female</th>
			               	<th>Total</th>
			               	<th>Source</th>
			            </tr>
			            <tr>
			            	<td>No. of children aged 6 to 12 who are not attending elementary school</td>
			                <td>
			                	{{ Form::input('number', 'no_mkidnosch6_12', $brgy_profile->no_mkidnosch6_12, array('class' => 'form-control', 'min'=>'0', 'max'=> $brgy_profile->no_male6_12, 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'no_fkidnosch6_12', $brgy_profile->no_fkidnosch6_12, array('class' => 'form-control', 'min'=>'0', 'max'=> $brgy_profile->no_female6_12, 'required')) }}
			                </td>
							<td class="total_kidnosch6_12">{{ $brgy_profile->no_mkidnosch6_12 +  $brgy_profile->no_fkidnosch6_12}} </td>
							<td>{{ Form::text('srce_kidnosch6_12', $brgy_profile->srce_kidnosch6_12, array('class' => 'form-control')) }}
			           		</td>
			            </tr>
			            <tr>
			            	<td>No. of children aged 13 to 16 who are not attending secondary school</td>
			                <td>
			                	{{ Form::input('number', 'no_mkidnosch13_16', $brgy_profile->no_mkidnosch13_16, array('class' => 'form-control', 'min'=>'0', 'max'=> $brgy_profile->no_male13_16, 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'no_fkidnosch13_16', $brgy_profile->no_fkidnosch13_16, array('class' => 'form-control', 'min'=>'0', 'max'=> $brgy_profile->no_male13_16, 'required')) }}
			                </td>
							<td class="total_kidnosch13_16">{{ $brgy_profile->no_mkidnosch13_16 + $brgy_profile->no_fkidnosch13_16}} </td>
							<td>{{ Form::text('srce_kidnosch13_16', $brgy_profile->srce_kidnosch13_16, array('class' => 'form-control')) }}
			           		</td>
			            </tr>
			            
			            <tr>
			            	<th>Shelter </th>
			                <th>Male-Headed</th>
			                <th>Female-Headed</th>
			               	<th>Total</th>
			               	<th>Source</th>
			            </tr>
			            <tr>
			            	<td>No. of households living in makeshift housing</td>
			                <td>
			                	{{ Form::input('number', 'male_hdmkshhouse', $brgy_profile->male_hdmkshhouse, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'fem_hdmkshouse', $brgy_profile->fem_hdmkshouse, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
							<td class="total_hdmkshouse">{{ $brgy_profile->male_hdmkshhouse +  $brgy_profile->fem_hdmkshhouse}}</td>
							<td>{{ Form::text('source_mkshouse', $brgy_profile->source_mkshouse, array('class' => 'form-control')) }}
			           		</td>
			            </tr>
			            <tr>
			            	<td>No. of households who are informal settlers</td>
			                <td>
			                	{{ Form::input('number', 'male_hdsquatters', $brgy_profile->male_hdsquatters, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'fem_hdsquatters', $brgy_profile->fem_hdsquatters, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
							<td class="total_squatters">{{$brgy_profile->male_hdsquatters + $brgy_profile->fem_hdsquatters }} </td>
							<td>{{ Form::text('source_squatters', $brgy_profile->source_squatters, array('class' => 'form-control')) }}
			           		</td>
			            </tr>
			            
			            <tr>
			            	<th>Water and Sanitation </th>
			                <th>Male-Headed</th>
			                <th>Female-Headed</th>
			               	<th>Total</th>
			               	<th>Source</th>
			            </tr>
			            <tr>
			            	<td>No. of households without access to safe water</td>
			                <td>
			                	{{ Form::input('number', 'male_hdnotoilet', $brgy_profile->male_hdnotoilet, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'fem_hdnotoilet', $brgy_profile->fem_hdnotoilet, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
							<td class="total_notoilet">{{ $brgy_profile->male_hdnotoilet +  $brgy_profile->fem_hdnotoilet}}</td>
							<td>{{ Form::text('source_notoilet', $brgy_profile->source_notoilet, array('class' => 'form-control')) }}
			           		</td>
			            </tr>
			            <tr>
			            	<td>- No. of households without access to sanitary toilet facilities</td>
			                <td>
			                	{{ Form::input('number', 'male_hdsfwater', $brgy_profile->male_hdsfwater, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'fem_hdsfwater', $brgy_profile->fem_hdsfwater, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
							<td class="total_sfwater">{{ $brgy_profile->male_hdsfwater + $brgy_profile->fem_hdsfwater}}</td>
							<td>{{ Form::text('source_safewater', $brgy_profile->source_safewater, array('class' => 'form-control')) }}
			           		</td>
			            </tr>
			            
			            <tr>
			            	<th>Income </th>
			                <th>Male-Headed</th>
			                <th>Female-Headed</th>
			               	<th>Total</th>
			               	<th>Source</th>
			            </tr>
			            <tr>
			            	<td>No. of households with income below the poverty threshold</td>
			                <td>
			                	{{ Form::input('number', 'male_hdpoverty', $brgy_profile->male_hdpoverty, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'fem_hdpoverty', $brgy_profile->fem_hdpoverty, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
							<td class="total_poverty">{{ $brgy_profile->male_hdpoverty + $brgy_profile->fem_hdpoverty}}</td>
							<td>{{ Form::text('source_poverty', $brgy_profile->source_poverty, array('class' => 'form-control')) }}
			           		</td>
			            </tr>
			            <tr>
			            	<td>No. of households with income below food threshold</td>
			                <td>
			                	{{ Form::input('number', 'male_hdblwfood', $brgy_profile->male_hdblwfood, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'fem_hdblwfood', $brgy_profile->fem_hdblwfood, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
							<td class="total_blwfood">{{$brgy_profile->male_hdblwfood + $brgy_profile->fem_hdblwfood}}</td>
							<td>{{ Form::text('source_blwfood', $brgy_profile->source_blwfood, array('class' => 'form-control')) }}
			           		</td>
			            </tr>
			            <tr>
			            	<td>No. of households who experienced food shortage</td>
			                <td>
			                	{{ Form::input('number', 'male_hdfoodshrt', $brgy_profile->male_hdfoodshrt, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'fem_hdfoodshrt', $brgy_profile->fem_hdfoodshrt, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
							<td class="total_foodshrt">{{ $brgy_profile->male_hdfoodshrt + $brgy_profile->fem_hdfoodshrt}}</td>
							<td>{{ Form::text('source_foodshort', $brgy_profile->source_foodshort, array('class' => 'form-control')) }}
			           		</td>
			            </tr>
			            
			             <tr>
			            	<th>Employment </th>
			                <th>Male</th>
			                <th>Female</th>
			               	<th>Total</th>
			               	<th>Source</th>
			            </tr>
			            <tr>
			            	<td>No. of persons in the labor force who are unemployed</td>
			                <td>
			                	{{ Form::input('number', 'no_malenowork', $brgy_profile->no_malenowork, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'no_femnowork', $brgy_profile->no_femnowork, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
							<td class="total_nowork">{{ $brgy_profile->no_malenowork + $brgy_profile->no_femnowork}}</td>
							<td>{{ Form::text('source_nowork', $brgy_profile->source_nowork, array('class' => 'form-control')) }}
			           		</td>
			            </tr>
			            <tr>
			            	<th>Peace and Order </th>
			                <th>Male</th>
			                <th>Female</th>
			               	<th>Total</th>
			               	<th>Source</th>
			            </tr>
			            <tr>
			            	<td>No. of persons who are victims of crimes</td>
			                <td>
			                	{{ Form::input('number', 'no_malecrimev', $brgy_profile->no_malecrimev, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			               	</td>
			                <td>
			                	{{ Form::input('number', 'no_femcrimev', $brgy_profile->no_femcrimev, array('class' => 'form-control', 'min'=>'0', 'required')) }}
			                </td>
							<td class="total_crimev">{{$brgy_profile->no_malecrimev + $brgy_profile->no_femcrimev }} </td>
							<td>{{ Form::text('source_crimevic', $brgy_profile->source_crimevic, array('class' => 'form-control')) }}
			           		</td>
			            </tr> -->
			            
			            
					</table>
			 	</div>
			 </div>
		 </div>
		 
		<!-- <div class="form-group">
						<label>Save as draft</label>
						<input type="checkbox" name="is_draft" {{ $brgy_profile->is_draft==1 ? 'checked': '' }}>
		</div>
		 {{ Form::submit('Save', array('class' => 'btn btn-info')) }}
		 <a href="{{ URL::to('brgyprofile') }}" class="btn btn-default">close</a> -->

		{{ Form::close() }}
		 
		
	</div>
	
	
	<div class="clearfix"></div>
	
	

		<script>
			$(document).ready(function(){
				
				$("#add").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Location" name="location[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><textarea type="text" class="form-control" name="description[]" required></textarea></td>'+
					            	// '<td><textarea type="text" class="form-control" name="effects[]" required></textarea></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});
				
				$("#add1").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items1").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Activity" name="activity[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="number" class="form-control" min="0" placeholder="Average Income" name="avg_income[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" placeholder="No. Household" name="act_hdinvolved[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" placeholder="Seasonality" name="seasonality[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});
				
				$("#add2").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items2").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Crop" name="majorcrop[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="text" class="form-control" placeholder="Volume of Production" name="production[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" placeholder="No. Household" name="crop_hdinvolved[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});
			
				$("#add3").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items3").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Mode" name="mode[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="text" class="form-control" placeholder="Cost" name="cost[]" required></td>'+
					            	'<td><input type="text" class="form-control" placeholder="Schedule" name="schedule[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});
				
				$("#add4").click(function (e) {
				//Append a new row of code to the "#items" div
					if($('#items4 tbody').children().length == 1){
							$("#items4").append('<tr><td>'+
					            	'<input type="text" class="form-control" value="IRA" placeholder="Fund Source" name="fund_source[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="number" class="form-control" placeholder="Amount (PhP)" min="0" name="amount[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
					}else{
						$("#items4").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Fund Source" name="fund_source[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="number" class="form-control" placeholder="Amount (PhP)" min="0" name="amount[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
					}
				
				});
				
				$("#add5").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items5").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Equipment Type" name="equipt_type[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="text" class="form-control" placeholder="Current Condition" name="curr_condition[]" required></td>'+
					            	'<td><input type="text" class="form-control" placeholder="Current Capability" name="curr_capability[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" placeholder="Consumption" name="consumption[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" placeholder="Rental Rate" name="rental_rate[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});
				$("#add_5").click(function (e) {
				$("#items_5").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Activity" name="activity[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="number" class="form-control" placeholder="Cost (PhP)" min="0" name="cost[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});
				
				$("#add6").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items6").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="Tenure Status" name="tenure_status[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="number" class="form-control" min="0" name="tenure_no_households[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" name="tenure_no_hhfemalehead[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" name="tenure_no_hhiphead[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});
				
				$("#add7").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items7").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="IP Group Name"  name="ip_group[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><input type="text" class="form-control" placeholder="Sitio/Purok"  name="ip_location[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" name="ip_no_malehdhh[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" name="ip_no_femalehdhh[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" name="ip_no_male[]" required></td>'+
					            	'<td><input type="number" class="form-control" min="0" name="ip_no_female[]" required></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});
				
				$("body").on("click", ".delete", function (e) {
					$(this).parent().parent().remove();
				});
				
				
				$("input[name='no_households']").change(function (e) {
					$("input[name='no_fmheadedhh']").attr('max', $("input[name='no_households']").val());
					$("input[name='no_iphouseholds']").attr('max', $("input[name='no_households']").val());
					$("input[name='crop_hdinvolved[]']").attr('max', $("input[name='no_households']").val());
					$("input[name='act_hdinvolved[]']").attr('max', $("input[name='no_households']").val());
					$("input[name='tenure_no_households[]']").attr('max', $("input[name='no_households']").val());
					
					$("input[name='male_hdmkshouse']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdsquatters']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdnotoilet']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdsfwater']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdpoverty']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdblwfood']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdfoodshrt']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
				
				}).change();
				
				$("input[name='no_male6_12']").change(function (e) {
					$("input[name='no_mkidnosch6_12']").attr('max', $("input[name='no_male6_12']").val());
				});
				
				$("input[name='no_female6_12']").change(function (e) {
					$("input[name='no_fkidnosch6_12']").attr('max', $("input[name='no_female6_12']").val());
				});
				
				$("input[name='no_male13_16']").change(function (e) {
					$("input[name='no_mkidnosch13_16']").attr('max', $("input[name='no_male13_16']").val());
				});
				
				$("input[name='no_female13_16']").change(function (e) {
					$("input[name='no_fkidnosch13_16']").attr('max', $("input[name='no_female13_16']").val());
				});
				
				$("input[name='no_fmheadedhh']").change(function (e) {
					$("input[name='fem_hdmkshouse']").attr('max', $("input[name='no_fmheadedhh']").val());
					$("input[name='fem_hdsquatters']").attr('max', $("input[name='no_fmheadedhh']").val());
					$("input[name='fem_hdnotoilet']").attr('max', $("input[name='no_fmheadedhh']").val());
					$("input[name='fem_hdsfwater']").attr('max', $("input[name='no_fmheadedhh']").val());
					$("input[name='fem_hdpoverty']").attr('max', $("input[name='no_fmheadedhh']").val());
					$("input[name='fem_hdblwfood']").attr('max', $("input[name='no_fmheadedhh']").val());
					$("input[name='fem_hdfoodshrt']").attr('max', $("input[name='no_fmheadedhh']").val());
					$("input[name='tenure_no_hhfemalehead[]']").attr('max', $("input[name='no_fmheadedhh']").val());
					
					
					$("input[name='male_hdmkshouse']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdsquatters']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdnotoilet']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdsfwater']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdpoverty']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdblwfood']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					$("input[name='male_hdfoodshrt']").attr('max', $("input[name='no_households']").val()-$("input[name='no_fmheadedhh']").val());
					
				}).change();
				
				$("input[name='no_iphouseholds']").change(function (e) {
					$("input[name='tenure_no_hhiphead[]']").attr('max', $("input[name='no_iphouseholds']").val());
					$("input[name='ip_no_malehdhh[]']").attr('max', $("input[name='no_iphouseholds']").val());
					$("input[name='ip_no_femalehdhh[]']").attr('max', $("input[name='no_iphouseholds']").val());
					
				}).change();
				
				
				$("input[name='no_families']").change(function (e) {
					$("input[name='no_ipfamily']").attr('max', $("input[name='no_families']").val());
				});
				
				$("input[name='no_male']").change(function (e) {
					$("input[name='no_male0_5']").attr('max', $("input[name='no_male']").val());
					$("input[name='no_male6_12']").attr('max', $("input[name='no_male']").val());
					$("input[name='no_male13_16']").attr('max', $("input[name='no_male']").val());
					$("input[name='no_male17_59']").attr('max', $("input[name='no_male']").val());
					$("input[name='no_male60up']").attr('max', $("input[name='no_male']").val());
					$("input[name='no_malenowork']").attr('max', $("input[name='no_male']").val());
					$("input[name='no_malecrimev']").attr('max', $("input[name='no_male']").val());
					$("input[name='ip_no_male']").attr('max', $("input[name='no_male']").val());
				});
				
				$("input[name='no_female']").change(function (e) {
					$("input[name='no_female0_5']").attr('max', $("input[name='no_female']").val());
					$("input[name='no_female6_12']").attr('max', $("input[name='no_female']").val());
					$("input[name='no_female13_16']").attr('max', $("input[name='no_female']").val());
					$("input[name='no_female17_59']").attr('max', $("input[name='no_female']").val());
					$("input[name='no_female60up']").attr('max', $("input[name='no_female']").val());
					$("input[name='no_femnowork']").attr('max', $("input[name='no_female']").val());
					$("input[name='no_femcrimev']").attr('max', $("input[name='no_female']").val());
					$("input[name='ip_no_female[]']").attr('max', $("input[name='no_female']").val());
				});
				
				$("#is_poblacion").change(function (e) {
					if($("#is_poblacion").is(":checked") ){
						$("input[name='hrs_totown']").attr('disabled','disabled');
						$("input[name='km_frmtown']").attr('disabled','disabled');
					}
					else{
					    	$("input[name='hrs_totown']").removeAttr('disabled');
                        	$("input[name='km_frmtown']").removeAttr('disabled');
					}
					// else{
					// 	$("input[name='hrs_totown']").attr('disabled','disabled');
					// 	$("input[name='km_frmtown']").attr('disabled','disabled');
				});

				$("#is_brgyaffected").change(function (e) {
					if(!$("#is_brgyaffected").is(":checked") ){
						$("textarea[name='baragay_additiondetails']").attr('disabled','disabled');
						
					}
					else{
					    	$("textarea[name='baragay_additiondetails']").removeAttr('disabled');
                        				
					}
				
				});

			});
			
			/*Check Duplicate Entry*/
			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName(e.name);
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
				
			}


			/* get the total */
			var total = (function($){
				return {
					
					totalPerson : function(){
						var total_ = parseInt($("input[name='no_male']").val()) + parseInt($("input[name='no_female']").val()) ;
						$('.total_person').text(total_ || '0');
					},
					total_0_5 : function(){
						var total_ = parseInt($("input[name='no_male0_5']").val()) + parseInt($("input[name='no_female0_5']").val()) ;
						$('.total_0_5').text(total_ || '0');
					},
					total_6_12 : function(){
						var total_ = parseInt($("input[name='no_male6_12']").val()) + parseInt($("input[name='no_female6_12']").val()) ;
						$('.total_6_12').text(total_ || '0');
					},
					total_13_16 : function(){
						var total_ = parseInt($("input[name='no_male13_16']").val()) + parseInt($("input[name='no_female13_16']").val()) ;
						$('.total_13_16').text(total_ || '0');
					},
					total_17_59 : function(){
						var total_ = parseInt($("input[name='no_male17_59']").val()) + parseInt($("input[name='no_female17_59']").val()) ;
						$('.total_17_59').text(total_ || '0');
					},
					total_60_up : function(){
						var total_ = parseInt($("input[name='no_male60up']").val()) + parseInt($("input[name='no_female60up']").val()) ;
						$('.total_60_up').text(total_ || '0');
					},
					total_died5below : function(){
						var total_ = parseInt($("input[name='no_mdied5below']").val()) + parseInt($("input[name='no_fdied5below']").val()) ;
						$('.total_died5below').text(total_ || '0');
					},
					total_kidmaln : function(){
						var total_ = parseInt($("input[name='no_mkidmaln']").val()) + parseInt($("input[name='no_fkidmaln']").val()) ;
						$('.total_kidmaln').text(total_ || '0');
					},
					total_kidnosch6_12 : function(){
						var total_ = parseInt($("input[name='no_mkidnosch6_12']").val()) + parseInt($("input[name='no_fkidnosch6_12']").val()) ;
						$('.total_kidnosch6_12').text(total_ || '0');
					},
					total_kidnosch13_16 : function(){
						var total_ = parseInt($("input[name='no_mkidnosch13_16']").val()) + parseInt($("input[name='no_fkidnosch13_16']").val()) ;
						$('.total_kidnosch13_16').text(total_ || '0');
					},
					total_hdmkshouse : function(){
						var total_ = parseInt($("input[name='male_hdmkshhouse']").val()) + parseInt($("input[name='fem_hdmkshouse']").val()) ;
						$('.total_hdmkshouse').text(total_ || '0');
					},
					total_squatters : function(){
						var total_ = parseInt($("input[name='male_hdsquatters']").val()) + parseInt($("input[name='fem_hdsquatters']").val()) ;
						$('.total_squatters').text(total_ || '0');
					},
					total_notoilet : function(){
						var total_ = parseInt($("input[name='male_hdnotoilet']").val()) + parseInt($("input[name='fem_hdnotoilet']").val()) ;
						$('.total_notoilet').text(total_ || '0');
					},
					total_sfwater : function(){
						var total_ = parseInt($("input[name='male_hdsfwater']").val()) + parseInt($("input[name='fem_hdsfwater']").val()) ;
						$('.total_sfwater').text(total_ || '0');
					},
					total_poverty : function(){
						var total_ = parseInt($("input[name='male_hdpoverty']").val()) + parseInt($("input[name='fem_hdpoverty']").val()) ;
						$('.total_poverty').text(total_ || '0');
					},
					total_blwfood : function(){
						var total_ = parseInt($("input[name='male_hdblwfood']").val()) + parseInt($("input[name='fem_hdblwfood']").val()) ;
						$('.total_blwfood').text(total_ || '0');
					},
					total_foodshrt : function(){
						var total_ = parseInt($("input[name='male_hdfoodshrt']").val()) + parseInt($("input[name='fem_hdfoodshrt']").val()) ;
						$('.total_foodshrt').text(total_ || '0');
					},
					total_nowork : function(){
						var total_ = parseInt($("input[name='no_malenowork']").val()) + parseInt($("input[name='no_femnowork']").val()) ;
						$('.total_nowork').text(total_ || '0');
					},
					total_crimev : function(){
						var total_ = parseInt($("input[name='no_malecrimev']").val()) + parseInt($("input[name='no_femcrimev']").val()) ;
						$('.total_crimev').text(total_ || '0');
					},
					total_voting : function(){
						var total_ = parseInt($("input[name='no_voting_male']").val()) + parseInt($("input[name='no_voting_female']").val()) ;
						$('.total_voting').text(total_ || '0');
					}
				}
			})(jQuery);


			$("input[name='no_male']").change(total.totalPerson);
			$("input[name='no_female']").change(total.totalPerson);
			$("input[name='no_male0_5']").change(total.total_0_5);
			$("input[name='no_female0_5']").change(total.total_0_5);
			$("input[name='no_male6_12']").change(total.total_6_12);
			$("input[name='no_female6_12']").change(total.total_6_12);
			$("input[name='no_male13_16']").change(total.total_13_16);
			$("input[name='no_female13_16']").change(total.total_13_16);
			$("input[name='no_male17_59']").change(total.total_17_59);
			$("input[name='no_female17_59']").change(total.total_17_59);
			$("input[name='no_male60up']").change(total.total_60_up);
			$("input[name='no_female60up']").change(total.total_60_up);	
			$("input[name='no_mdied5below']").change(total.total_died5below);
			$("input[name='no_fdied5below']").change(total.total_died5below);	
			$("input[name='no_mkidmaln']").change(total.total_kidmaln);
			$("input[name='no_fkidmaln']").change(total.total_kidmaln);			
			$("input[name='no_mkidnosch6_12']").change(total.total_kidnosch6_12);
			$("input[name='no_fkidnosch6_12']").change(total.total_kidnosch6_12);			
			$("input[name='no_mkidnosch13_16']").change(total.total_kidnosch13_16);
			$("input[name='no_fkidnosch13_16']").change(total.total_kidnosch13_16);			
			$("input[name='male_hdmkshhouse']").change(total.total_hdmkshouse);
			$("input[name='fem_hdmkshouse']").change(total.total_hdmkshouse);		
			$("input[name='male_hdsquatters']").change(total.total_squatters);
			$("input[name='fem_hdsquatters']").change(total.total_squatters);		
			$("input[name='male_hdnotoilet']").change(total.total_notoilet);
			$("input[name='fem_hdnotoilet']").change(total.total_notoilet);		
			$("input[name='male_hdsfwater']").change(total.total_sfwater);
			$("input[name='fem_hdsfwater']").change(total.total_sfwater);		
			$("input[name='male_hdpoverty']").change(total.total_poverty);
			$("input[name='fem_hdpoverty']").change(total.total_poverty);
			$("input[name='male_hdblwfood']").change(total.total_blwfood);
			$("input[name='fem_hdblwfood']").change(total.total_blwfood);		
			$("input[name='male_hdfoodshrt']").change(total.total_foodshrt);
			$("input[name='fem_hdfoodshrt']").change(total.total_foodshrt);		
			$("input[name='no_malenowork']").change(total.total_nowork);
			$("input[name='no_femnowork']").change(total.total_nowork);	
			$("input[name='no_malecrimev']").change(total.total_crimev);
			$("input[name='no_femcrimev']").change(total.total_crimev);
			$("input[name='no_voting_male']").change(total.total_voting);
			$("input[name='no_voting_female']").change(total.total_voting);
			
			$(function () {
    			var tbl = $('#budget');
    			tbl.find('tr').each(function () {
       		 		$(this).find('input[type=number]').bind("change", function () {
            			calculateSum();
        			});
    			});

    			function calculateSum() {
        			var tbl = $('#budget');
        			var sum = 0;
        			tbl.find('tr').each(function () {
            		$(this).find('input[type=number]').each(function () {
                		if (!isNaN(this.value) && this.value.length != 0) {
                	    	sum = sum + parseFloat(this.value);
               		 	}
            		});
					
            		$(this).find('.totalbudget').text(sum.toFixed(2));
        			});
    			}
			});
		</script>
	
@stop