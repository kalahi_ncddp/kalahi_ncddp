@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
    <a class="btn btn-default" href="{{ URL::to('brgyprofile/'.$id) }}">Go back</a>
<div class="panel panel-default">
		<div class="panel-heading">
			<!-- BLGU Official's Profile -->
			<a class="btn  btn-primary pull-right" href="{{ URL::to('brgyprofile/'.$id.'/create_person_blgu') }}"><i class="fa fa-plus"></i> Add New</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>Last Name</th>
							<th>First Name</th>
							<th>Middle Initial</th>
							<th>Sex</th>
							<th>Age</th>
							<th>Civil Status</th>
							<th>IP Group</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $brgy_blgu)
						<tr>
							<td>
								{{ $brgy_blgu->last_name }}
							</td>

							<td>
								{{ $brgy_blgu->first_name }}
							</td>

							<td>
								{{ $brgy_blgu->middle_initial }}
							</td>
							<td>
								{{ $brgy_blgu->sex }}
							</td>
							<td>
								{{ $brgy_blgu->age }}
							</td>
							<td>
								{{ $brgy_blgu->civilstatus }}
							</td>
							<td>
								{{ $brgy_blgu->ipgroup }}
							</td>
							<td>
	                            <a class="btn btn-success btn" href="{{ URL::to('brgyprofile/' .$brgy_blgu->person_id. '/show_person_blgu') }}">
	                             <i class="fa fa-eye"></i> View Details 
	                            </a>
	                        </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop