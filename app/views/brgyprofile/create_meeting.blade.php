@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
			<a class="btn btn-default" href="{{ URL::to('brgyprofile/'.$id.'/show_bdcmeetings') }}">Go back</a>
		<br>
		<br>
	    <div class="row">
		    <div class="col-md-12">
				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
						
				<div class="box box-primary">
	            	<div class="box-body">
						{{ Form::open(array('url' => 'brgyprofile/submit_meeting', 'method' => 'POST', 'role' => 'form')) }}
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<tr>
					                <th>During the past year, how many times were the BAs conducted?</th>
					                <td>
					                	{{ Form::input('number', 'bas_conducted', Input::old('bas_conducted'), array('class' => 'form-control','min'=>'0')) }}
					                </td>
					            </tr>
					            <tr>
					                <th>On the average, how many barangay households are present during the meetings?</th>
					                <td>
					                	<!-- {{ Form::input('number', 'no_baconducted', Input::old('no_baconducted'), array('class' => 'form-control')) }} -->
					            </tr>
					             <tr>
					             	<ul>
					            	<th><li>How many male residents were present?</li> </th>
					                <td>
					                	{{ Form::input('number', 'no_avgmale', Input::old('no_avgmale'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					               </ul>
					            </tr>
					            <tr>
					             	<ul>
					            	<th><li>How many female residents were present?</li> </th>
					                <td>
					                	{{ Form::input('number', 'no_avgfemale', Input::old('no_avgfemale'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					               </ul>
					            </tr>
					            <tr>
					             	<ul>
					            	<th><li>How many IP households were present?</li> </th>
					                <td>
					                	{{ Form::input('number', 'no_iphouseholds', Input::old('no_iphouseholds'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					               </ul>
					            </tr>
					            <tr>
					             	<ul>
					            	<th><li>What other sectors were present?</li> </th>
					                <td>
					                	{{ Form::input('text', 'sectors_rep', Input::old('sectors_rep'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					               </ul>
					            </tr>
					            <tr>
					                <th>During the past year, how many times did the BDC meet?</th>
					                <td>
					                	{{ Form::input('number', 'no_bdcmeet', Input::old('bdc_meet'), array('class' => 'form-control')) }}
					            </tr>
					            <tr>
					                <th>How many, on the average, attend the BDC meetings(refer to attendance sheet)</th>
					                <td>
					                	<!-- {{ Form::input('number', 'no_baconducted', Input::old('no_baconducted'), array('class' => 'form-control')) }} -->
					            </tr>
					             <tr>
					             	<ul>
					            	<th><li>No. of male BLGU officials</li> </th>
					                <td>
					                	{{ Form::input('number', 'no_bdcmale', Input::old('no_bdcmale'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					               </ul>
					            </tr>
					            <tr>
					            	<ul>
					            	<th><li>No. of female BLGU officials</li></th>
					                <td>
					                	{{ Form::input('number', 'no_bdcfemale', Input::old('no_bdcfemale'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					               </ul>
					            </tr>
					            
					            <tr><ul>
					            	<th><li>No. of male PO/CBO representatives</li> </th>
					                <td>
					                	{{ Form::input('number', 'no_bdcmalerep', Input::old('no_bdcmalerep'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					            </tr></ul>
					            <tr>
					            	<ul>
					            	<th><li>No. of female PO/CBO representatives</li> </th>
					                <td>
					                	{{ Form::input('number', 'no_bdcfemalerep', Input::old('no_bdcfemalerep'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					               	<ul>
					            </tr>

					            <!-- <tr>
					                <th>How many times did the BDC meet?</th>
					                <td>
					                	{{ Form::input('number', 'no_bdcmeet', Input::old('no_bdcmeet'), array('class' => 'form-control')) }}
					                </td>
					            </tr>
					            <tr>
					                <th>What sectors were present on BAs meetings?</th>
					                <td>
					                	{{ Form::textarea('sectors_rep', Input::old('sectors_rep'), array('class' => 'form-control')) }}
					                </td>
					            </tr>	 -->	            
							</table>

							<br>
							
					</div>
					
					
							<!-- <br>
							<table class="table table-striped table-bordered table-hover">
					            <thead> 
									<th colspan="2" style="text-align:center"> Average Number of attendees during BDC meetings  </th>
								</thead>
					            <tr>
					            	<th>No. of male BLGU officials </th>
					                <td>
					                	{{ Form::input('number', 'no_bdcmale', Input::old('no_bdcmale'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					            </tr>
					            <tr>
					            	<th>No. of female BLGU officials</th>
					                <td>
					                	{{ Form::input('number', 'no_bdcfemale', Input::old('no_bdcfemale'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					            </tr>
					            <tr>
					            	<th>No. of male PO/CBO representatives </th>
					                <td>
					                	{{ Form::input('number', 'no_bdcmalerep', Input::old('no_bdcmalerep'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					            </tr>
					            <tr>
					            	<th>No. of female PO/CBO representatives </th>
					                <td>
					                	{{ Form::input('number', 'no_bdcfemalerep', Input::old('no_bdcfemalerep'), array('class' => 'form-control', 'min'=>'0')) }}
					               	</td>
					            </tr>
							</table> -->
						</div>
						<br>
				{{ Form::hidden('id', $id)}}
				{{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
				{{ Form::reset('Reset', array('class' => 'btn btn-danger')) }}
				{{ Form::close() }}
				</div>
			</div>
		</div>

		<script>
		    //helper function for custom validity
(function (exports) {
                            function valOrFunction(val, ctx, args) {
                                if (typeof val == "function") {
                                    return val.apply(ctx, args);
                                } else {
                                    return val;
                                }
                            }

                            function InvalidInputHelper(input, options) {
                                input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

                                function changeOrInput() {
                                    if (input.value == "") {
                                        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                                    } else {
                                        input.setCustomValidity("");
                                    }
                                }

                                function invalid() {
                                    if (input.value == "") {
                                        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                                    } else {
                                       console.log("INVALID!"); input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
                                    }
                                }

                                input.addEventListener("change", changeOrInput);
                                input.addEventListener("input", changeOrInput);
                                input.addEventListener("invalid", invalid);
                                changeOrInput();
                            }
                            exports.InvalidInputHelper = InvalidInputHelper;
                        })(window);
			$(document).ready(function(){


                $('.attendees').each(function(i,e){
                    InvalidInputHelper( e, {
                                      defaultText: "Please enter an email address!",

                                      emptyText: "Please enter an email address!",

                                      invalidText: function (input) {
                                        return 'Value must be less than or equal to the Represented Household in the Barangay (' + input.max + ')';
                                      }
                                    });
                });
                $('.family').each(function(i,e){
                                         InvalidInputHelper(e , {
                                                  defaultText: "Please enter an email address!",

                                                  emptyText: "Please enter an email address!",

                                                  invalidText: function (input) {
                                                    console.log(input);
                                                    return 'Value must be less than or equal to the Total Families in the Barangay (' + input.max + ')';
                                                  }
                                                });
                });

				$('#daterange').daterangepicker(
				  {
				    format: 'MM/DD/YYYY',
				    startDate : moment().format('MM/DD/YYYY'),
				    endDate : moment().format('MM/DD/YYYY')
				  },
				  function(start, end, label) {
				   	   	$('input[name="startdate"]').val(start.format('MM/DD/YYYY'));
				  		$('input[name="enddate"]').val(end.format('MM/DD/YYYY'));
				  }
				);
				//when the Add Filed button is clicked
				$("#add").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items").append('<div class="input-group"><input type="text" class="form-control" placeholder="Name of sitio eg. STO SITIO" name="sitio[]" oninput="checkduplicate(this)"><span class="input-group-addon"> <span class="fa fa-trash-o  delete"></span></span></div><br>');
					});

				$("body").on("click", ".delete", function (e) {
					$(this).parent().remove();
				});


				$("input[name='startdate']").change(function (e) {
					$("input[name='enddate']").attr('min', $("input[name='startdate']").val());
				});

				$("input[name='enddate']").change(function (e) {
					$("input[name='startdate']").attr('max', $("input[name='enddate']").val());
				});

				$("input[name='total_household']").change(function (e) {
					$("input[name='no_household']").attr('max', $("input[name='total_household']").val());
				});

				$("input[name='total_families']").change(function (e) {
					$("input[name='no_families']").attr('max', $("input[name='total_families']").val());
				});

				$("input[name='no_atnmale']").change(function (e) {
					$("input[name='no_ipmale']").attr('max', $("input[name='no_atnmale']").val());
					$("input[name='no_oldmale']").attr('max', $("input[name='no_atnmale']").val());
				});

				$("input[name='no_atnfemale']").change(function (e) {
					$("input[name='no_ipfemale']").attr('max', $("input[name='no_atnfemale']").val());
					$("input[name='no_oldfemale']").attr('max', $("input[name='no_atnfemale']").val());
				});

				$("input[name='no_household']").change(function (e) {
					$("input[name='no_pphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='no_slphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='no_iphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");
				});

				$("input[name='no_families']").change(function (e) {
//					$("input[name='no_ppfamilies']").attr('max', $("input[name='no_families']").val());
//					$("input[name='no_slpfamilies']").attr('max', $("input[name='no_families']").val());
//					$("input[name='no_ipfamilies']").attr('max', $("input[name='no_families']").val());
					$("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");
				});


				$("input[name='total_household']").change(function (e) {


					$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");
				});

				$("input[name='total_families']").change(function (e) {
				 $("input[name='no_ppfamilies']").attr('max', $("input[name='total_families']").val());
                                					$("input[name='no_slpfamilies']").attr('max', $("input[name='total_families']").val());
                                					$("input[name='no_ipfamilies']").attr('max', $("input[name='total_families']").val());
					$("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");
				});

               	$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");

                $("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");

			});

			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName('sitio[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}


			/* get the total in attendees */
			var total = (function($){
				return {
					
					totalPerson : function(){
						var total_ = parseInt($("input[name='no_atnfemale']").val()) + parseInt($("input[name='no_atnmale']").val()) ;
						$('.total_person').text(total_ || '0');
					},
					totalIP : function(){
						var total_ = parseInt($("input[name='no_ipfemale']").val()) + parseInt($("input[name='no_ipmale']").val()) ;
						$('.total_ip').text(total_ || '0');
					},
					totalOld : function(){
						var total_ = parseInt($("input[name='no_oldfemale']").val()) + parseInt($("input[name='no_oldmale']").val()) ;
						$('.total_old').text(total_ || '0');
					}

				}
			})(jQuery);
                total.totalPerson();
                total.totalIP();
                total.totalOld();

			$("input[name='no_atnmale']").keyup(total.totalPerson);
			$("input[name='no_ipmale']").keyup(total.totalIP);
			$("input[name='no_oldmale']").keyup(total.totalOld);
			$("input[name='no_atnfemale']").keyup(total.totalPerson);
			$("input[name='no_ipfemale']").keyup(total.totalIP);
			$("input[name='no_oldfemale']").keyup(total.totalOld);
			


		
				
				$("#add4").click(function (e) {
				//Append a new row of code to the "#items" div
				$("#items4").append('<tr><td>'+
					            	'<input type="text" class="form-control" placeholder="" name="source_fund[]" oninput="checkduplicate(this)" required>'+
					            	'</td>'+
					            	'<td><textarea type="text" class="form-control" name="amount[]" required></textarea></td>'+
					            	// '<td><textarea type="text" class="form-control" name="effects[]" required></textarea></td>'+
					            	'<td><button type="button" class="btn delete"><i class="fa fa-trash-o"></i></button>'+
					            	'</td>'+
					           '</tr>');
				});
				$("body").on("click", ".delete", function (e) {
					$(this).parent().parent().remove();
				});
		</script>
@stop