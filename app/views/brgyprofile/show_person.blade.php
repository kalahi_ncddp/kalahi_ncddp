@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('mibf') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
		<div class="row">
	        <div class="col-md-12">

				 <!-- {{ Form::open(array('url' => 'brgyprofile/' .$id. '/show_bdcp')) }} -->
				  {{ Form::open(array('url' => 'brgyprofile/' .$id. '/show_bdcp/delete', 'method' => 'POST')) }}
				 	<a class="btn  btn-small btn-info" href="{{ URL::to('brgyprofile/' .$id.'/edit_person') }}">
				 	<i class="fa fa-edit"></i>	Edit 
				 	</a>

					{{ Form::hidden('first_name', $person->first_name) }}
					{{ Form::hidden('last_name', $person->last_name) }}
					{{ Form::hidden('middle_name', $person->middle_name) }}
					{{ Form::hidden('age', $person->age) }}


					{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}

			        <a class="btn bg-navy" href="{{ URL::to('brgyprofile/' .$person->profile_id. '/show_bdcp') }}">Close </a>
				 {{ Form::close() }}	 
	        </div>
	    </div>

	    <!-- /. ROW  -->
	    <hr />
	    <div class="row">
		    <div class="col-md-12">
                <div class="panel panel-default">
		    		<div class="panel-heading">
		    			Details
		    		</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
							 	<tr>
					                <th>Last Name</th>
					                <td>{{ $person->last_name }}</td>
					            </tr>

					            <tr>
					                <th>First Name</th>
					                <td>{{ $person->first_name }}</td>
					            </tr>

					            <tr>
					                <th>Middle Initial</th>
					                <td>{{ $person->middle_name }}</td>
					            </tr>
					            <tr>
					                <th>Sex</th>
					               <td>{{$person->sex}}</td>
					            </tr>
					            <tr>
					                <th>Age</th>
					                <td>{{ $person->age }}</td>
					            </tr>
					            <tr>
					                <th>Civil Status</th>
					                <td>{{ $person->civil_status }}</td>
					            </tr>
					            <tr>
					                <th>IP Group</th>
					                <td>{{ $person->ip_group }}</td>
					            </tr>
					            <tr>
					                <th>Educational Attainment</th>
					                <td>{{ $person->educ_attainment }}</td>
					            </tr>
					            <tr>
					                <th>Position</th>
					                <td>{{ $person->position }}</td>
					            </tr>
					            <tr>
					                <th>Start Date</th>
					                <td>{{ Report::change_date_format($person->startdate) }}</td>
					            </tr>
					            <tr>
					                <th>End Date</th>
					                <td>{{ Report::change_date_format($person->enddate) }}</td>
					            </tr>
					            <tr>
					                <th>Name of Non-KC Organization</th>
					                <td>{{ $person->org_nonkc }}</td>
					            </tr>
					           <!--  <tr>
					                <th>Name of KC Organization</th>
					                <td>{{ $person->kc_comm }}</td>
					            </tr> -->
					            <tr>
					                <th>Sector Represented</th>
					                <td>{{ $person->sector_rep }}</td>
					            </tr>
							</table>
						</div>
					</div>
				</div>					
			</div>
		</div>
	<script>
			$("form").submit(function(){

            					var confirmation = confirm('are you sure to this action?');
            					if(confirmation){
            							return true;

            					}else{
            						return false;
            					}


            				});
		</script>
	
@stop