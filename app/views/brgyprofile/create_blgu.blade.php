@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('sidebar')
    <li>
    	<a class="active-menu" href="{{ URL::to('reports') }}"><i class="fa fa-home fa-3x"></i> Home</a>
    </li>
@stop

@section('content')
	    <div class="row">
		    <div class="col-md-12">
				<!-- if there are creation errors, they will show here -->
				@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
				
				<div class="box box-primary">
	            	<div class="box-body">
						{{ Form::open(array('url' => 'brgyprofile/submit_org', 'method' => 'POST', 'role' => 'form')) }}
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<tr>
				                <th>Name</th>
				                <td>
				                	{{ Form::text('org_name', Input::old('org_name'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Type of Organization</th>
				                <td>
				                	{{ Form::text('org_type', Input::old('org_type'), array('class' => 'form-control','placeholder'=>'ex. PO, NGO, Private institution-csr', 'required')) }}
				                	<!-- {{ Form::select('org_type', $orgtype_list, Input::old('org_type'), array('class' => 'form-control')) }} -->
				                </td>
				            </tr>
				            <tr>
				                <th>Formal (Registered)</th>
				                <td>
				                	{{ Form::select('is_registered', array(''=> 'Yes or No?',1 => 'Yes', 0 => 'No'), Input::old('is_registered'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>LGU-Accredited</th>
				                <td>
				                	{{ Form::select('is_lguaccredited', array('' => 'Yes or No?',1 => 'Yes', 0 => 'No'), Input::old('is_lguaccredited'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Advocacy</th>
				                <td>
				                	{{ Form::text('advocacy', Input::old('advocacy'), array('class' => 'form-control', 'placeholder'=>'Savings, religious, farmers, fisherfolk, women','required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Area of operation</th>
				                <td>
				                	{{ Form::text('area_operation', Input::old('area_operation'), array('class' => 'form-control','placeholder'=>'ex. nationwide, municipal, diff. brgys., etc', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Years operation in brgy</th>
				                <td>
				                	{{ Form::input('number','years_operation', Input::old('years_operation'), array('class' => 'form-control','min'=>'0','placeholder'=>'', 'required')) }}
				               		
				                </td>
				            </tr>

				            <tr>
				                <th>Active or Inactive organizations</th>
				                <td>
				                	{{ Form::select('active_inactive_org', array(''=> 'Active or Inactive?',1 => 'Active', 0 => 'Inactive'), Input::old('active_inactive_org'), array('class' => 'form-control', 'required')) }}
				                </td>
				            </tr>

				            <tr>
				                <th>Activities</th>
				                <td>
				                	{{ Form::text('activities', Input::old('activities'), array('class' => 'form-control','placeholder'=>'ex. nationwide, municipal, diff. brgys., etc')) }}
				                </td>
				            </tr>

				            <tr>
				                <th>Total male members from brgy</th>
				                <td>
				                	{{ Form::input('number','total_male_members_brgy', Input::old('total_male_members_brgy'), array('class' => 'form-control','min'=>'0','placeholder'=>'', 'required')) }}
				                </td>
				            </tr>

				            <tr>
				                <th>Total female members from brgy</th>
				                <td>
				                	{{ Form::input('number','total_female_members_brgy', Input::old('total_female_members_brgy'), array('class' => 'form-control','min'=>'0','placeholder'=>'', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Male IP members from brgy</th>
				                <td>
				                	{{ Form::input('number','male_ip_members', Input::old('male_ip_members'), array('class' => 'form-control','min'=>'0','placeholder'=>'', 'required')) }}
				                </td>
				            </tr>

				            <tr>
				                <th>Female IP members from brgy</th>
				                <td>
				                	{{ Form::input('number','female_ip_members', Input::old('female_ip_members'), array('class' => 'form-control','min'=>'0','placeholder'=>'ex. nationwide, municipal, diff. brgys., etc', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				                <th>Marginalized sectors represented</th>
				                <td>
				                	{{ Form::text('sector_rep', Input::old('sector_rep'), array('class' => 'form-control','placeholder'=>'Marginalized sectors', 'required')) }}
				                	<!-- {{ Form::select('sector_rep', array('Sector 1' => 'Sector 1', 'Sector 2' => 'Sector 2'), Input::old('sector_rep'), array('class' => 'form-control', 'required')) }} -->
				                </td>
				            </tr>			            
						</table>

						<br>
						<!-- <table class="table table-striped table-bordered table-hover">
				            <tr>
				            	<th></th>
				                <th>Male</th>
				                <th>Female</th>
				            </tr>
				            <tr>
				            	<th>No. of members from the barangay </th>
				                <td>
				                	{{ Form::input('number', 'no_memmale', Input::old('no_memmale'), array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>
				                <td>
				                	{{ Form::input('number', 'no_memfemale', Input::old('no_memfemale'), array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				            	<th>No. of IP members from the barangay </th>
				                <td>
				                	{{ Form::input('number', 'no_ipmale', Input::old('no_ipmale'), array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>
				                <td>
				                	{{ Form::input('number', 'no_ipfemale', Input::old('no_ipfemale'), array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr>
				            <tr>
				            	<th>No. of Officers </th>
				                <td>
				                	{{ Form::input('number', 'no_ofcmale', Input::old('no_ofcmale'), array('class' => 'form-control', 'min'=>'0', 'required')) }}
				               	</td>	
				                <td>
				                	{{ Form::input('number', 'no_ofcfemale', Input::old('no_ofcfemale'), array('class' => 'form-control', 'min'=>'0', 'required')) }}
				                </td>
				            </tr>
						</table> -->
					</div>
					<br>
				{{ Form::hidden('id', $id)}}
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{ Form::reset('Reset', array('class' => 'btn btn-danger')) }}
				{{ Form::close() }}
				</div>
			</div>
		</div>

		<script>
		    //helper function for custom validity
(function (exports) {
                            function valOrFunction(val, ctx, args) {
                                if (typeof val == "function") {
                                    return val.apply(ctx, args);
                                } else {
                                    return val;
                                }
                            }

                            function InvalidInputHelper(input, options) {
                                input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

                                function changeOrInput() {
                                    if (input.value == "") {
                                        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                                    } else {
                                        input.setCustomValidity("");
                                    }
                                }

                                function invalid() {
                                    if (input.value == "") {
                                        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                                    } else {
                                       console.log("INVALID!"); input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
                                    }
                                }

                                input.addEventListener("change", changeOrInput);
                                input.addEventListener("input", changeOrInput);
                                input.addEventListener("invalid", invalid);
                                changeOrInput();
                            }
                            exports.InvalidInputHelper = InvalidInputHelper;
                        })(window);
			$(document).ready(function(){


                $('.attendees').each(function(i,e){
                    InvalidInputHelper( e, {
                                      defaultText: "Please enter an email address!",

                                      emptyText: "Please enter an email address!",

                                      invalidText: function (input) {
                                        return 'Value must be less than or equal to the Represented Household in the Barangay (' + input.max + ')';
                                      }
                                    });
                });
                $('.family').each(function(i,e){
                                         InvalidInputHelper(e , {
                                                  defaultText: "Please enter an email address!",

                                                  emptyText: "Please enter an email address!",

                                                  invalidText: function (input) {
                                                    console.log(input);
                                                    return 'Value must be less than or equal to the Total Families in the Barangay (' + input.max + ')';
                                                  }
                                                });
                });

				$('#daterange').daterangepicker(
				  {
				    format: 'MM/DD/YYYY',
				    startDate : moment().format('MM/DD/YYYY'),
				    endDate : moment().format('MM/DD/YYYY')
				  },
				  function(start, end, label) {
				   	   	$('input[name="startdate"]').val(start.format('MM/DD/YYYY'));
				  		$('input[name="enddate"]').val(end.format('MM/DD/YYYY'));
				  }
				);
				//when the Add Filed button is clicked
				$("#add").click(function (e) {
					//Append a new row of code to the "#items" div
					$("#items").append('<div class="input-group"><input type="text" class="form-control" placeholder="Name of sitio eg. STO SITIO" name="sitio[]" oninput="checkduplicate(this)"><span class="input-group-addon"> <span class="fa fa-trash-o  delete"></span></span></div><br>');
					});

				$("body").on("click", ".delete", function (e) {
					$(this).parent().remove();
				});


				$("input[name='startdate']").change(function (e) {
					$("input[name='enddate']").attr('min', $("input[name='startdate']").val());
				});

				$("input[name='enddate']").change(function (e) {
					$("input[name='startdate']").attr('max', $("input[name='enddate']").val());
				});

				$("input[name='total_household']").change(function (e) {
					$("input[name='no_household']").attr('max', $("input[name='total_household']").val());
				});

				$("input[name='total_families']").change(function (e) {
					$("input[name='no_families']").attr('max', $("input[name='total_families']").val());
				});

				$("input[name='no_atnmale']").change(function (e) {
					$("input[name='no_ipmale']").attr('max', $("input[name='no_atnmale']").val());
					$("input[name='no_oldmale']").attr('max', $("input[name='no_atnmale']").val());
				});

				$("input[name='no_atnfemale']").change(function (e) {
					$("input[name='no_ipfemale']").attr('max', $("input[name='no_atnfemale']").val());
					$("input[name='no_oldfemale']").attr('max', $("input[name='no_atnfemale']").val());
				});

				$("input[name='no_household']").change(function (e) {
					$("input[name='no_pphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='no_slphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='no_iphousehold']").attr('max', $("input[name='no_household']").val());
					$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");
				});

				$("input[name='no_families']").change(function (e) {
//					$("input[name='no_ppfamilies']").attr('max', $("input[name='no_families']").val());
//					$("input[name='no_slpfamilies']").attr('max', $("input[name='no_families']").val());
//					$("input[name='no_ipfamilies']").attr('max', $("input[name='no_families']").val());
					$("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");
				});


				$("input[name='total_household']").change(function (e) {


					$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");
				});

				$("input[name='total_families']").change(function (e) {
				 $("input[name='no_ppfamilies']").attr('max', $("input[name='total_families']").val());
                                					$("input[name='no_slpfamilies']").attr('max', $("input[name='total_families']").val());
                                					$("input[name='no_ipfamilies']").attr('max', $("input[name='total_families']").val());
					$("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");
				});

               	$("input[name='percentage_household']").attr('value', ($("input[name='no_household']").val()/$("input[name='total_household']").val()*100).toFixed(2) + "%");

                $("input[name='percentage_families']").attr('value', ($("input[name='no_families']").val()/$("input[name='total_families']").val()*100).toFixed(2) + "%");

			});

			function checkduplicate(e){
				var count = -1;
				var elements = document.getElementsByName('sitio[]');
				for (i=0; i<elements.length; i++) {
    				if(e.value === elements[i].value){
    					count++;
    				}
				}
				if(count > 0){	
					e.setCustomValidity("Duplicate Entry not allowed");
				}else{
					e.setCustomValidity("");
				}
			}


			/* get the total in attendees */
			var total = (function($){
				return {
					
					totalPerson : function(){
						var total_ = parseInt($("input[name='no_atnfemale']").val()) + parseInt($("input[name='no_atnmale']").val()) ;
						$('.total_person').text(total_ || '0');
					},
					totalIP : function(){
						var total_ = parseInt($("input[name='no_ipfemale']").val()) + parseInt($("input[name='no_ipmale']").val()) ;
						$('.total_ip').text(total_ || '0');
					},
					totalOld : function(){
						var total_ = parseInt($("input[name='no_oldfemale']").val()) + parseInt($("input[name='no_oldmale']").val()) ;
						$('.total_old').text(total_ || '0');
					}

				}
			})(jQuery);
                total.totalPerson();
                total.totalIP();
                total.totalOld();

			$("input[name='no_atnmale']").keyup(total.totalPerson);
			$("input[name='no_ipmale']").keyup(total.totalIP);
			$("input[name='no_oldmale']").keyup(total.totalOld);
			$("input[name='no_atnfemale']").keyup(total.totalPerson);
			$("input[name='no_ipfemale']").keyup(total.totalIP);
			$("input[name='no_oldfemale']").keyup(total.totalOld);
			


		</script>
@stop