@extends('layouts.default')

@section('username')
	{{ $username }}
@stop

@section('content')
	@if ($errors->all())
					<div class="alert alert-warning">{{ HTML::ul($errors->all())}}</div>
				@endif
    <a class="btn btn-default" href="{{ URL::to('brgyprofile/'.$id) }}">Go back</a>

	<div class="panel panel-default">
		<div class="panel-heading">
			What development projects have been implemented in your barangay?
			<a class="btn  btn-primary pull-right" href="{{ URL::to('brgyprofile/'.$id.'/create_devproj') }}"><i class="fa fa-plus"></i> Add New</a>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>Project</th>
							<th>Location</th>
							<th>Scope/Coverage/Unit or Physical Target</th>
							<th>Cost of Project</th>
							<th>Source of funds</th>
							<th>Details</th>
							
						</tr>
					</thead>
					<tbody>
						@foreach($data as $devproj)
						<tr>
							<td>
								{{ $devproj->proj_name }}
							</td>
							<td>
								{{ $devproj->location }}
							</td>
							<td>
								{{ $devproj->scope_target }}
							</td>
							<td>
								{{ currency_format($devproj->proj_cost) }}
							</td>
							<td>
								{{ $devproj->fund_source }}
							</td>
							<td>
	                            <a class="btn btn-success btn" href="{{ URL::to('brgyprofile/' .$devproj->devproj_id. '/show_devproj') }}">
	                         <i class="fa fa-eye"></i>     View Details 
	                            </a>
	                        </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<script>
      $(document).ready(function () {
        $('#dataTables-example').dataTable();
      });
    </script>
@stop