<?php

Route::group(['namespace'=>'AddedFeature'],function(){
	Route::get('spi_profile/{spi_profile}/utilization/',array('as' => 'spi.utilization.edit', 'uses' => 'SPUtilizationController@edit'));
	Route::get('spi_profile/{spi_profile}/utilization/create',array('as' => 'spi.utilization.create', 'uses' => 'SPUtilizationController@create'));

	Route::post('spi_profile/{spi_profile}/utilization/update',array('as' => 'spi.utilization.update', 'uses' => 'SPUtilizationController@update'));
	Route::post('spi_profile/{spi_profile}/utilization/',array('as' => 'spi.utilization.store', 'uses' => 'SPUtilizationController@store'));
	Route::get('spi_profile/{spi_profile}/utilization/delete',array('as' => 'spi.utilization.delete', 'uses' => 'SPUtilizationController@destroy'));


});
