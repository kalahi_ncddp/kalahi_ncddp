<?php namespace Repositories\Sync;



interface SyncInterface {

   public function storeData($contents);

   public function searchBA($contents, $id);
   public function searchPSA($contents, $id);
   public function searchMIBF($contents, $id);
   public function searchTraining($contents, $id);
   public function searchVolunteer($contents, $id);

   public function storeBA( $contents, $action );
   public function storePSA( $contents, $action );
   public function storeMIBF( $contents, $action );
   public function storeTraining($contents, $action);
   public function storeVolunteer($contents, $action);



}