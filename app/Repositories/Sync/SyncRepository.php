<?php namespace Repositories\Sync;
use Illuminate\Support\Facades\Session;

class SyncRepository implements SyncInterface{

	// Extra: change db_x to exists()

	// Done: LGUActivity | BA | PSA | MIBF
	// ToDo: Dup entries

	// Redo code
	// One loop to rule them all :)) DONE!

	public function storeData($contents)
	{
		/*
		*	Loop for LGUActivities (BA, PSA, MIBF)
		*/
		foreach( $contents->lguactivity as $lguactivity )
		{
			$db_lguactivity = \LguActivity::where('activity_id', $lguactivity->activity_id)->first();

			if( is_null($db_lguactivity) )											// Insert if no record yet
			{
				// echo 'New record inserted.  --  ';

				// Insert LGUActivity and Activity based on type
				\LguActivity::create( (array) $lguactivity );

				if( $lguactivity->activity_type == 'BA' )
				{
					$BA_key = $this->searchBA( $contents->bassembly, $lguactivity->activity_id);
					if( ! is_null($BA_key) )
					{
						$bassembly = $contents->bassembly[ $BA_key ];
						$this->storeBA( $bassembly, 'create' );
					}
				}
				elseif( $lguactivity->activity_type == 'PSA' )
				{
					$PSA_key = $this->searchPSA( $contents->psa, $lguactivity->activity_id);
					if( ! is_null($PSA_key) )
					{
						$psa = $contents->psa[ $PSA_key ];
						$this->storePSA( $psa, 'create' );
					}
				}
				elseif( $lguactivity->activity_type == 'MIBF' )
				{
					$MIBF_key = $this->searchMIBF( $contents->mibf, $lguactivity->activity_id);
					if( ! is_null($MIBF_key) )
					{
						$mibf = $contents->mibf[ $MIBF_key ];
						$this->storeMIBF( $mibf, 'create' );
					}
				}
			}
			else 																	// Compare ac_approved dates
			{
				if( $lguactivity->ac_approved > $db_lguactivity->ac_approved)		// More recent. Update
				{	
					// echo 'Date more recent. Record updated.  --  ';

					\LguActivity::where('activity_id', $lguactivity->activity_id)->update( (array) $lguactivity );

					if( $lguactivity->activity_type == 'BA' )
					{
						$BA_key = $this->searchBA( $contents->bassembly, $lguactivity->activity_id);
						if( ! is_null($BA_key) )
						{
							$bassembly = $contents->bassembly[ $BA_key ];
							$this->storeBA( $bassembly, 'update' );
						}
					}
					elseif( $lguactivity->activity_type == 'PSA' )
					{
						$PSA_key = $this->searchPSA( $contents->psa, $lguactivity->activity_id);
						if( ! is_null($PSA_key) )
						{
							$psa = $contents->psa[ $PSA_key ];
							$this->storePSA( $psa, 'update' );
						}
					}
					elseif( $lguactivity->activity_type == 'MIBF' )
					{
						$MIBF_key = $this->searchMIBF( $contents->mibf, $lguactivity->activity_id);
						if( ! is_null($MIBF_key) )
						{
							$mibf = $contents->mibf[ $MIBF_key ];
							$this->storeMIBF( $mibf, 'update' );
						}
					}
				}
				else 																// Ignore
				{
					// echo 'Date equal or behind  --  ';
				}
			}

		}



		/*
		*	Loop for Trainings, Volunteers
		*/

		// echo '<pre>';
		// dd( $contents);

		foreach( $contents->references as $reference )
		{
			$db_reference = \References::where('reference_no', $reference->reference_no)->first();
			
			
			if( is_null($db_reference) )											// Insert if no record yet
			{
				// echo 'New record inserted.  --  ';

				// Insert Reference 
				\References::create( (array) $reference );

				if( $reference->reference_type == 'Training')
				{
					$Training_key = $this->searchTraining( $contents->trainings, $reference->reference_no);
					if( ! is_null($Training_key) )
					{
						$training = $contents->trainings[ $Training_key ];
						$this->storeTraining( $training, 'create' );
					}
				}
				elseif( $reference->reference_type == 'Volunteer')
				{
					$Volunteer_key = $this->searchVolunteer( $contents->volunteers, $reference->reference_no);
					if( ! is_null($Volunteer_key) )
					{
						$volunteer = $contents->volunteers[ $Volunteer_key ];
						$this->storeVolunteer( $volunteer, 'create' );
					}
				}

			}
			else 																	// Compare ac_approved dates
			{

				if( $reference->ac_approved > $db_reference->ac_approved)			// More recent. Update
					{	
					// echo 'Date more recent. Record updated.  --  ';

					\References::where('reference_no', $reference->reference_no)->update( (array) $reference );

					if( $reference->reference_type == 'Training')
					{
						$Training_key = $this->searchTraining( $contents->trainings, $reference->reference_no);
						if( ! is_null($Training_key) )
						{
							$training = $contents->trainings[ $Training_key ];
							$this->storeTraining( $training, 'update' );
						}
					}
					elseif( $reference->reference_type == 'Volunteer')
					{
						$Volunteer_key = $this->searchVolunteer( $contents->volunteers, $reference->reference_no);
						if( ! is_null($Volunteer_key) )
						{
							$volunteer = $contents->volunteers[ $Volunteer_key ];
							$this->storeVolunteer( $volunteer, 'update' );
						}
					}
				}
				else 																// Ignore
				{
					// echo 'Date equal or behind  --  ';
				}
			}
		}
	}




	public function searchVolunteer($contents, $id)
	{
		foreach ($contents as $key => $volunteer)
		{
			if( $volunteer->reference_no == $id)
			{
				return $key;
			}
		}

		return null;
	}



	public function storeVolunteer($contents, $action)
	{
		$volunteer = $contents->approved_volunteer;
		$volcommmem = $contents->volcommmem;
		$beneficiary = $volunteer->beneficiary;
		$memotherorg = $beneficiary->memotherorg;
		$memothertrn = $beneficiary->memothertrn;

		if( ! \Beneficiary::where('beneficiary_id', $beneficiary->beneficiary_id)->exists() )
		{
			\Beneficiary::create( (array) $beneficiary);
		}
		else
		{
			$beneficiary = (array) $beneficiary;
			unset( $beneficiary['memotherorg']);
			unset( $beneficiary['memothertrn']);
			\Beneficiary::where('beneficiary_id', $volunteer->beneficiary_id)->update( $beneficiary);
		}

		
		if( $action == 'create')
		{
			\Volunteer::create( (array) $volunteer );
			foreach( $volcommmem as $volcom ) { \VolCommMem::create( (array) $volcom ); }

			\DB::table('kc_memotherorg')->insert( (array) $memotherorg);
			\DB::table('kc_memothertrn')->insert( (array) $memothertrn);
		}
		elseif( $action == 'update' )
		{
			$volunteer = (array) $volunteer;
			unset( $volunteer['beneficiary']);
			\Volunteer::where('volunteer_id', $volunteer['volunteer_id'])->update( $volunteer);

			// Clear
			foreach( $volcommmem as $volcom ) { \VolCommMem::where('volunteer_id', $volunteer->volunteer_id)->delete(); }
			\DB::table('kc_memotherorg')->where('beneficiary_id', $memotherorg->beneficiary_id)->delete();
			\DB::table('kc_memothertrn')->where('beneficiary_id', $memothertrn->beneficiary_id)->delete();

			// Insert
			\Volunteer::create( (array) $volunteer );
			foreach( $volcommmem as $volcom ) { \VolCommMem::create( (array) $volcom ); }

			\DB::table('kc_memotherorg')->insert( (array) $memotherorg);
			\DB::table('kc_memothertrn')->insert( (array) $memothertrn);
		}
	}










	public function searchTraining($contents, $id)
	{
		foreach ($contents as $key => $training)
		{
			if( $training->reference_no == $id)
			{
				return $key;
			}
		}

		return null;
	}



	public function storeTraining($contents, $action)
	{
		$training 			= $contents->approved_training;		// one
		$trnbrgy			= $contents->trnbrgy;				// one
		$trnparticipants	= $contents->trnparticipants;		// many
		$trainor			= $contents->trainor;				// many

		// Populate beneficiaries first
		foreach( $trnparticipants as $participant ) {
			if( ! \Beneficiary::where('beneficiary_id', $participant->beneficiary->beneficiary_id)->exists() )
			{
				// create
				\Beneficiary::create( (array) $participant->beneficiary);
			}
			else
			{
				\Beneficiary::where('beneficiary_id', $participant->beneficiary->beneficiary_id)->update( (array) $participant->beneficiary);
			}
		}

		if( $action == 'create')
		{
			\DB::table('kc_trainings')->insert( (array) $training );
			if( ! is_null($trnbrgy) )	\Trnbrgy::create( (array) $trnbrgy);
		}
		elseif( $action == 'update' )
		{
			\DB::table('kc_trainings')->where('reference_no', $training->reference_no)->update( (array) $training );
			if( ! is_null($trnbrgy) )	\Trnbrgy::where('reference_no', $trnbrgy->reference_no)->update( (array) $trnbrgy );

			// Clear records -- -_-
			foreach( $trnparticipants as $participant ) { 
				\Trnparticipants::where('reference_no', $participant->reference_no)->delete();
			}
			\Trainor::where('reference_no', $training->reference_no)->delete();
		}

		// Insert Sub-tables
		foreach( $trnparticipants as $participant ) { 
			\DB::table('kc_trnparticipants')->insert( (array) $participant );
		}
		foreach( $trainor as $train ) 					{ \Trainor::create( (array) $train ); }

	}





	public function searchBA($contents, $id)
	{
		foreach ($contents as $key => $bassembly)
		{
			if( $bassembly->activity_id == $id)
			{
				return $key;
			}
		}

		return null;
	}

	public function storeBA( $contents, $action )
	{
		$bassembly 		= $contents->approved_bassembly;
		$sitiorep		= $contents->sitiorep;
		$sectorsrep		= $contents->sectorsrep;
		$activityissues	= $contents->activityissues;

		// Insert BA
		if( $action == 'create')
		{
			\BarangayAssembly::create( (array) $bassembly );
		}
		elseif( $action == 'update' )
		{
			\BarangayAssembly::where('activity_id', $bassembly->activity_id)->update( (array) $bassembly );

			// Clear records -- -_-
			\SitioRep::where('activity_id', $bassembly->activity_id)->delete();
			\SectorsRep::where('activity_id', $bassembly->activity_id)->delete();
			\ActivityIssues::where('activity_id', $bassembly->activity_id)->delete();
		}

		// Insert Sub-tables
		foreach( $sitiorep as $sitio ) 		 { \SitioRep::create( (array) $sitio ); }
		foreach( $sectorsrep as $sector ) 	 { \SectorsRep::create( (array) $sector ); }
		foreach( $activityissues as $issue ) { \ActivityIssues::create( (array) $issue ); }
	}





	
	public function searchPSA($contents, $id)
	{
		foreach ($contents as $key => $psa)
		{
			if( $psa->activity_id == $id)
			{
				return $key;
			}
		}

		return null;
	}

	public function storePSA( $contents, $action )
	{
		$psa 			= $contents->approved_psa;
		$sitiorep		= $contents->sitiorep;
		$sectorsrep		= $contents->sectorsrep;
		$activityissues	= $contents->activityissues;
		$psaproject 	= $contents->psaproject;
		$psadocuments 	= $contents->psadocuments;

		// Insert PSA
		if( $action == 'create')
		{
			\PSAnalysis::create( (array) $psa );
		}
		elseif( $action == 'update' )
		{
			\PSAnalysis::where('activity_id', $psa->activity_id)->update( (array) $psa );

			// Clear records -- -_-
			\SitioRep::where('activity_id', $psa->activity_id)->delete();
			\SectorsRep::where('activity_id', $psa->activity_id)->delete();
			\ActivityIssues::where('activity_id', $psa->activity_id)->delete();
			\PSAProjects::where('activity_id', $psa->activity_id)->delete();
			\PSADocuments::where('activity_id', $psa->activity_id)->delete();
		}

		// Insert Sub-tables
		foreach( $sitiorep as $sitio ) 		 	{ \SitioRep::create( (array) $sitio ); }
		foreach( $sectorsrep as $sector ) 	 	{ \SectorsRep::create( (array) $sector ); }
		foreach( $activityissues as $issue ) 	{ \ActivityIssues::create( (array) $issue ); }
		foreach( $psaproject as $project ) 		{ \PSAProjects::create( (array) $project ); }
		foreach( $psadocuments as $document ) 	{ \PSADocuments::create( (array) $document ); }
	}





	public function searchMIBF($contents, $id)
	{
		foreach ($contents as $key => $mibf)
		{
			if( $mibf->activity_id == $id)
			{
				return $key;
			}
		}

		return null;
	}

	public function storeMIBF( $contents, $action )
	{
		$mibf 			= $contents->approved_mibf;
		$sectorsrep		= $contents->sectorsrep;
		$projproposal	= $contents->projproposal;
		if( $projproposal[0]->projcoverage != NULL ) {
			$projcoverage 	= $projproposal[0]->projcoverage;
		}

		// Insert MIBF
		if( $action == 'create')
		{
			\MunicipalForum::create( (array) $mibf );

			foreach( $sectorsrep as $sector ) 	 { \SectorsRep::create( (array) $sector ); }
			foreach( $projproposal as $proposal ) { \ProjProposal::create( (array) $proposal ); }

			\ProjCoverage::create( (array) $projcoverage );
		}
		elseif( $action == 'update' )
		{
			\MunicipalForum::where('activity_id', $mibf->activity_id)->update( (array) $mibf );
			
			// Clear records -- -_-
			\SectorsRep::where('activity_id', $mibf->activity_id)->delete();
			\ProjProposal::where('activity_id', $mibf->activity_id)->delete();
			
			foreach( $sectorsrep as $sector ) 	 { \SectorsRep::create( (array) $sector ); }
			foreach( $projproposal as $proposal ) { \ProjProposal::create( (array) $proposal ); }

			if( $projproposal[0]->projcoverage != NULL ) {
				\ProjCoverage::where('project_id', $projproposal[0]->project_id)->update( (array) $projcoverage );
			}
		}

	}






}