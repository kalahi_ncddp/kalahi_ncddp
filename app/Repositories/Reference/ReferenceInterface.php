<?php namespace Repositories\Reference;

interface ReferenceInterface {
    
    public function getCycles();
    public function getCycleById($id);
    public function getPrograms(); //get All programs
    public function getGrievanceForm();
    public function getIPGroupName();
    public function getDesignations();
    public function getModeFilling();
    public function getTrainingCat($lgutype);
    public function getActivityIssuesByMuni($id);
    public function getBApurpose($mode);
    public function getMIBFpurpose($mode);
    public function getConcernNature();
    public function getConcernCategory();
    public function getSubjectComplaint();
    public function getCurrentPosition();
    public function getOffices();
    public function getStatusEmployment(); 

    public function getAllRegionPSGC();
    public function getAllProvincePSGC($psgc_id);

    public function getKCcode($user);

    public function getComments();
    public function getCommentToday();

    public function getCeac($username);
    // public function insertceac();
}