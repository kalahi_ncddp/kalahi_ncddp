<?php namespace Repositories\Reference;
use Repositories\GRS\GRSInterface;
use Repositories\Reports\ReportsInterface;
use \Carbon\Carbon;

class ReferenceRepository implements  ReferenceInterface {
 
    protected $grsRepository;

    /**
     * @param GrsInterface $grsRepository
     * @param UsersInterface $usersInterface
     * @param ReferenceInterface $reference
     */

    public function __construct(GrsInterface $grsRepository ,
                                ReportsInterface $reportsInterface
                               )
    {
    
        $this->grsRepository = $grsRepository;

        $this->reports = $reportsInterface;

    }   
    /**
     * this is array returns the list of cycle doesnt need to have a model since no crud or relationship was used into this
     * it is just a reference data
     * @return array
     */
    public function getCycles()
    {
        $cycles = \DB::table('rf_cycles')->get();
        $cycle_array = [];

        foreach($cycles as $cycle)
        {
            $cycle_array[$cycle->name] = $cycle->name;
        }

        return $cycle_array;
    }

    /**
     * @description Returns the cycle of an specific municipality
     * @param $id
     * @return mixed
     */
    public function getCycleById($id)
    {
        $lguCycle = \LguCycles::where('psgc_id',$id)->first();

        return $lguCycle;
    }


    /**
     * @description get all programs in table kc_program reference only
     * @return array
     */
    public function getPrograms()
    {
        $programs = \DB::table('kc_programs')->get();
        $program_array = [];

        foreach($programs as $program)
        {
            $program_array[$program->program_id] = $program->program_id;
        }

        return $program_array;
    }

    /**
     * @return array
     */
    public function getGrievanceForm()
    {
        return [
            ''=>'Select Form',
            'intake'=>'Intake',
            'pincos'=>'Pincos'
        ];
    }

    /**
     * @return array
     */
    public function getIPGroupName()
    {
        $ipgroups = \DB::table('rf_ipgroup')->get();
        $ipgroup_array = [];
        $ipgroup_array[''] = '--';
        foreach($ipgroups as $ipgroup )
            $ipgroup_array[$ipgroup->name] = $ipgroup->name;

        return $ipgroup_array;
    }

    /**
     * @return array
     */
    public function getDesignations()
    {
        $designations = \DB::table('rf_designations')->get();
        $designations_array = [];
        $designations_array[''] = 'Select Designations';
        foreach($designations as $designation )
            $designations_array[$designation->designation] = $designation->designation;

        return $designations_array;
    }

    /**
     *
     * @return array
     */
    public function getModeFilling()
    {
        $modes = \DB::table('rf_fillingmode')->get();
        $modes_array = [];
        $modes_array[''] = 'Select Mode';
        foreach($modes as $mode )
            $modes_array[$mode->mode] = $mode->mode;

        return $modes_array;
    }

    /**
     * @description should insert if brgy or muni
     * @param $lgutype
     * @return string
     */
    public function getTrainingCat($lgutype)
    {
        $training_cat = "";
        $training_array = [];
        $mode = \Session::get('accelerated');

        if($mode == 0){
            if($lgutype=='brgy'){
                $training_cat = \DB::table('rf_trainingcat')
                    ->where('is_brgy',1)
                    ->where('is_accelerated',0)
                    ->where('is_regular',0)
                    ->get();
            }
            else if($lgutype=='muni')
            {
                $training_cat = \DB::table('rf_trainingcat')
                    ->where('is_muni',1)
                    ->where('is_accelerated',0)
                    ->where('is_regular',0)
                    ->get();
            }
        }else if($mode == 1){
            if($lgutype=='brgy'){
                $training_cat = \DB::table('rf_trainingcat')
                    ->where('is_brgy',1)->get();
            }
            else if($lgutype=='muni')
            {
                $training_cat = \DB::table('rf_trainingcat')
                    ->where('is_muni',1)
                    ->orWhere('is_accelerated',1)
                    ->where('is_regular',0)
                    ->get();
            }
        }
        $training_array[null] = 'Select Category';
        foreach ($training_cat as  $value) {
            $training_array[$value->catagory_id] = $value->description;
        }
        return $training_array;
    }
    public function getActivityIssuesByMuni($id)
    {
       $activity = \ActivityIssues::where('activity_id','like','%'.substr($id, 0,6).'%')->get();
       return $activity;
    }

    public function getMunicipalityByBarangayId($id)
    {
        dd( \Barangay::where('barangay_psgc',$id)->first());
        return \Barangay::where('barangay_psgc',$id)->first();
    }

    public function getBarangayByMuni($id)
    {
        $barangays = \Municipality::find($id)->barangay;
        

        return $barangays;

    }

    public function getBApurpose($mode)
    {
        $purpose = \DB::table('rf_bapurpose')->where('kc_mode',$mode)->lists('description','code');
        $purpose = array_merge([''=>'Select Purpose'],$purpose);
        return $purpose ;
        // $purpose = [''=>'Select Purpose']+ $purpose;
    }

    public function getMIBFpurpose($mode)
    {
        $purpose = \DB::table('rf_mibfpurpose')->where('kc_mode',$mode)->lists('purpose','code');
        $purpose = array_merge([''=>'Select Purpose'],$purpose);
        return $purpose;
    }

    public function getProvMuniBarangayByOfficeLevel($user)
    {
        $current = \User::where('psgc_id',$user)->first();

        if( $current->office_level == 'ACT')
        {
            $province_psgc = \Municipality::find($user)->province->province_psgc;
            $province_name = \Municipality::find($user)->province->province;
            $data['province'] = [ $province_psgc => $province_name ];
            $data['municipalities'] = \Municipality::where('municipality_psgc',$current->psgc_id)->lists('municipality','municipality_psgc');
            $data['barangay'] =  $this->grsRepository->getBarangayList($user);

            return $data;
        }
        else if ( $current->office_level == 'SRPMO' )
        {
            // $province_psgc = \Municipality::find($user)->province->province_psgc;
            // $province_name = \Municipality::find($user)->province->province;
            $data['province'] = \Province::where('province_psgc',$user)->lists('province','province_psgc');

            $data['municipalities'] = \Province::where('province_psgc',$user)->first()->municipality->lists('municipality','municipality_psgc');
        }
        else if ( $current->office_level == 'RPMO' )
        {

        }
        else if ( $current->office_level == 'NPMO' )
        {

        }
    }

    public function getMunicipalitiesByPSGC($psgc_id)
    {
        $province_psgc = \Municipality::find($psgc_id)->province->province_psgc;
        $province_name = \Municipality::find($psgc_id)->province->province;

        return \Municipality::where('province_psgc', $province_psgc)->lists('municipality', 'municipality_psgc');
    }

    public function getModeOfFiling()
    {
        return \DB::table('rf_fillingmode')->lists('mode','mode');
    }
    public function getConcernNature()
    {
        return \DB::table('rf_grsresolution')->lists('description','name');
    }
    public function getConcernCategory()
    {
        return \DB::table('rf_grscat')->orderBy('sort')->lists('name','name');
    }
    public function getSubjectComplaint()
    {
        return \DB::table('rf_designations')->lists('designation','designation');
    }

    public function getCommunityTrainingForPSA()
    {
        $psa = \PSAnalysis::all();
        $psgc_id = [];
        $cycles = [];
        $programs = [];
        foreach ($psa as $psas) {
            $psgc_id[] = $psas->psgc_id;
            $cycles[] = $psas->cycle_id;
            $programs[] = $psas->program_id;
        }
        
        return \Trainings::where('training_cat','PSA')->whereIn('psgc_id','!=',$psgc_id)->whereIn('cycle_id','!=',$cycles)->whereIn('program_id','!=',$programs)->get();
        
    }

    public function getCurrentPosition()
    {
        return \DB::table('rf_staffpost')->lists('position','position');
    }

    public function getOffices()
    {
        return \DB::table('rf_offices')->lists('office','office_id');
    }

    public function getStatusEmployment()
    {
        return \DB::table('rf_empstatus')->lists('emp_status','emp_status');
    }

    public function  getYears()
    {
        return [
        '2014'=>'2014',
        '2015'=>'2015',
        '2016'=>'2016',
        ];
    }

    public function getAllRegions()
    {
        return \DB::table('kc_regions')->lists('region_name','psgc_id');
    }
    public function getAllRegionPSGC()
    {
        $regions =  \Regions::get()->lists('region_name','psgc_id');
        $regions = [''=>'select region'] + $regions ;
        return $regions;
    }
    public function getAllProvincePSGC($psgc_id)
    {
        $province =  \Provinces::where('region_psgc',$psgc_id)->get()->lists('province','province_psgc');
        $province =  [''=>'select Province'] + $province ;
        return $province;
    }

    public function getKCcode($user)
    {
        $user = \User::where('username',$user)->first();
        if($user->office_level=='ACT')
        {
            $muni = \Municipality::where('municipality_psgc',$user->psgc_id)->first();

            return isset($muni->munis)?$muni->munis->kc_group:"";
        }
        else if($user->office_level=='SRPMO')
        {

        }
        else if($user->office_level=='RPMO')
        {

        }
        else if($user->office_level=='NPMO')
        {

        }
    }




    public function getGRSConcernsIntake($office_level, $psgc_id, $program, $cycle)
    {
        $concerns = $this->getConcernCategory();
        $natures = $this->getConcernNature();

        $data = []; //for storing counts

        foreach( $concerns as $concern )
        {
            $nature_count = [];

            foreach( $natures as $nature )
            {
                $count = \Intake::where('concern_category', trim($concern) )
                                ->where('concern_nature', $nature)
                                ->where('grievance_form', 'Intake')
                                ->where('psgc_id', $psgc_id)
                                ->where('cycle_id', $cycle)
                                ->where('program_id', $program)
                                ->count();

                array_push($nature_count, $count);
            }
            
            array_push($nature_count, array_sum($nature_count));
            array_push($data, $nature_count);
        }

        // column total
        $column_total = [0, 0, 0, 0];

        foreach( $data as $column )
        {
            $column_total[0] += $column[0]; // A
            $column_total[1] += $column[1]; // B
            $column_total[2] += $column[2]; // C
            $column_total[3] += $column[3]; // Total
        }

        array_push($data, $column_total);

        return $data;
    }



    public function getGRSConcernsPINCOS($office_level, $psgc_id, $program, $cycle)
    {
        $concerns = $this->getConcernCategory();
        $natures = $this->getConcernNature();

        $data = []; //for storing counts

        foreach( $concerns as $concern )
        {
            $nature_count = [];

            foreach( $natures as $nature )
            {
                $count = \Intake::where('concern_category', trim($concern) )
                                ->where('concern_nature', $nature)
                                ->where('grievance_form', 'PINCOS')
                                ->where('psgc_id', $psgc_id)
                                ->where('cycle_id', $cycle)
                                ->where('program_id', $program)
                                ->count();

                array_push($nature_count, $count);
            }
            
            array_push($nature_count, array_sum($nature_count));
            array_push($data, $nature_count);
        }

        // column total
        $column_total = [0, 0, 0, 0];

        foreach( $data as $column )
        {
            for( $i = 0; $i < sizeof($column_total); $i++ )
            {
                $column_total[$i] += $column[$i];
            }
        }

        array_push($data, $column_total);

        return $data;
    }




    public function getGRSGender($office_level, $psgc_id, $program, $cycle)
    {
        $natures = $this->getConcernNature();

        $genders = ['M', 'F', 'U', 'O'];

        // for storing counts
        // [0, 0, 0, 0, 0, 0, 0]
        // [M, F, U, O, Total, PINCOS, All]

        $data = []; 

        foreach( $natures as $nature )
        {
            $gender_count = [];

            foreach( $genders as $gender )
            {
                $count = \Intake::where('concern_nature', $nature)
                                ->where('grievance_form', 'Intake')
                                ->where('sender_sex', $gender)
                                ->where('psgc_id', $psgc_id)
                                ->where('cycle_id', $cycle)
                                ->where('program_id', $program)
                                ->count();

                array_push($gender_count, $count);
            }
            
            array_push($gender_count, array_sum($gender_count));

            // Total of PINCOS
            array_push($gender_count, \Intake::where('concern_nature', $nature)->where('grievance_form', 'PINCOS')->count());

            // All Entries
            array_push($gender_count, \Intake::where('concern_nature', $nature)->count() );
            
            array_push($data, $gender_count);
        }

        // column total
        $column_total = [0, 0, 0, 0, 0, 0, 0];

        foreach( $data as $column )
        {
            for( $i = 0; $i < sizeof($column_total); $i++)
            {
                $column_total[$i] += $column[$i];
            }
        }

        array_push($data, $column_total);

        return $data;
    }





    public function getGRSModeOfFilingIntake($office_level, $psgc_id, $program, $cycle)
    {
        $natures = $this->getConcernNature();
        $filingmodes = $this->getModeOfFiling();

        $data = []; //for storing counts

        foreach( $filingmodes as $mode )
        {
            $mode_count = [];

            foreach( $natures as $nature )
            {
                $count = \Intake::where('concern_nature', $nature)
                                ->where('grievance_form', 'Intake')
                                ->where('filing_mode', $mode)
                                ->where('psgc_id', $psgc_id)
                                ->where('cycle_id', $cycle)
                                ->where('program_id', $program)
                                ->count();

                array_push($mode_count, $count);
            }
            
            array_push($mode_count, array_sum($mode_count));
            array_push($data, $mode_count);
        }

        // column total
        $column_total = [0, 0, 0, 0];

        foreach( $data as $column )
        {
            for( $i = 0; $i < sizeof($column_total); $i++)
            {
                $column_total[$i] += $column[$i];
            }
        }

        array_push($data, $column_total);

        return $data;
    }

    public function getGRSModeOfFilingPINCOS($office_level, $psgc_id, $program, $cycle)
    {
        $natures = $this->getConcernNature();
        $filingmodes = $this->getModeOfFiling();

        $data = []; //for storing counts

        foreach( $filingmodes as $mode )
        {
            $mode_count = [];

            foreach( $natures as $nature )
            {
                $count = \Intake::where('concern_nature', $nature)
                                ->where('grievance_form', 'PINCOS')
                                ->where('filing_mode', $mode)
                                ->where('psgc_id', $psgc_id)
                                ->where('cycle_id', $cycle)
                                ->where('program_id', $program)
                                ->count();

                array_push($mode_count, $count);
            }
            
            array_push($mode_count, array_sum($mode_count));
            array_push($data, $mode_count);
        }

        // column total
        $column_total = [0, 0, 0, 0];

        foreach( $data as $column )
        {
            for( $i = 0; $i < sizeof($column_total); $i++)
            {
                $column_total[$i] += $column[$i];
            }
        }

        array_push($data, $column_total);

        return $data;
    }


    public function get_all_mibf_by_region($psgc_code, $cycle)
    {
        return \MunicipalForum::where('psgc_id', 'LIKE' , $psgc_code . '%')->where('cycle_id', $cycle)->get();
    }

    public function get_all_sp_by_mibf($activity_id)
    {
        return \ProjProposal::where('activity_id', $activity_id)->get();
    }


    // ($office_level, $psgc_id, $program, $cycle)
    public function getSPByMIBF($cycle)
    {
        // All regions or login-specific region only
        $regions = $this->getAllRegions();
        $data = [];

        foreach( $regions as $psgc_id => $name )
        {
            // kc_mode | total_sp | kc_amount | lcc_amount | total_amount
            $columns = [];

            // first two digits of psgc_id (region)
            $psgc_code = substr($psgc_id, 0, 2);

            $mibfs = $this->get_all_mibf_by_region( $psgc_code, $cycle );

            if( $mibfs->count() > 0 )
            {
                $kc_mode          = 0;
                $total_sp         = 0;
                $total_kc_amount  = 0;
                $total_lcc_amount = 0;

                foreach( $mibfs as $mibf )
                {
                    $kc_mode = $mibf['attributes']['kc_mode'];
                    
                    // SP by MIBF (activity_id) | ProjProposal
                    $projproposals = $this->get_all_sp_by_mibf($mibf['attributes']['activity_id']);

                    if( $projproposals->count() > 0 )
                    {
                        foreach( $projproposals as $proj )
                        {
                            $total_sp++;
                            $total_kc_amount  += $proj['attributes']['kc_amount'];
                            $total_lcc_amount += $proj['attributes']['lcc_amount'];
                        }
                    }
                }

                $total_amount = $total_kc_amount + $total_lcc_amount;

                array_push($columns, $kc_mode);
                array_push($columns, $total_sp);
                array_push($columns, $total_kc_amount);
                array_push($columns, $total_lcc_amount);
                array_push($columns, $total_amount);

                array_push($data, $columns);
            }
            else
            {
                array_push($data, [0,0,0,0,0]);
            }
        }

        return $data;
    }



    public function getGRSStatusIntake($office_level, $psgc_id, $program, $cycle, $grievance_form)
    {
        $municipalities = $this->getMunicipalitiesByPSGC($psgc_id);
        $natures = $this->getConcernNature();

        // A | B | C | Total | A | B | C | Total | Overall Total
        $data = [];

        foreach( $municipalities as $mun_psgc => $municipality  )
        {
            $columns = [];
            
            $resolved_total = 0;
            $pending_total  = 0;

            // Resolved
            foreach( $natures as $nature )
            {
                $count = \Intake::where('concern_nature', $nature)
                                ->where('grievance_form', $grievance_form)
                                ->where('psgc_id', $mun_psgc)
                                ->where('cycle_id', $cycle)
                                ->where('program_id', $program)
                                ->where('resolution_status', 'r')
                                ->count();

                $resolved_total += $count;

                array_push($columns, $count);
            }
            array_push($columns, $resolved_total);




            // Pending
            foreach( $natures as $nature )
            {
                $count = \Intake::where('concern_nature', $nature)
                                ->where('grievance_form', $grievance_form)
                                ->where('psgc_id', $mun_psgc)
                                ->where('cycle_id', $cycle)
                                ->where('program_id', $program)
                                ->where('resolution_status', '!=', 'r')
                                ->count();

                $pending_total += $count;
                
                array_push($columns, $count);
            }

            array_push($columns, $pending_total);
            array_push($columns, $resolved_total + $pending_total);     // Overall Total

            array_push($data, $columns);
        }

        // Column total
        $column_total = [0, 0, 0, 0, 0, 0, 0, 0, 0];

        foreach( $data as $column )
        {
            for( $i = 0; $i < sizeof($column_total); $i++)
            {
                $column_total[$i] += $column[$i];
            }
        }

        array_push($data, $column_total);

        return $data;
    }

    public function getCommentToday(){
        $start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
        $end = (new Carbon('now'))->hour(23)->minute(59)->second(59);

        $user = \Session::get('username');
        $user = \User::where('username',$user)->first();


        $region_psgc = \Municipality::find($user->psgc_id)->province->region->psgc_id;
        $province_psgc =  \Municipality::find($user->psgc_id)->province->province_psgc;

        return \Comments::whereBetween('created_at', [$start , $end])->count();
    }
    public function getComments()
    {

        $user = \Auth::user();
        $start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
        $end = (new Carbon('now'))->hour(23)->minute(59)->second(59);
        $region_psgc = \Municipality::find($user->psgc_id)->province->region->psgc_id;
        $province_psgc =  \Municipality::find($user->psgc_id)->province->province_psgc;

       $reg =  substr($province_psgc, 0, 2);

        return \Comments::where('psgc','regexp','^17')->get();
    }

    public function approval_status( $activity_id, $level = 'SRPMO')
    {
        $session_user = \Session::get('username');
//        $user_id = $this->user->getPsgcId($session_user);
        $user = \User::where('username',$session_user)->first();
        $mode = \Session::get('accelerated');
        $approved = \Approval::where( 'activity_id',$activity_id )
            ->where('kc_mode',$mode)->first();

        if($approved!=null)
        {
            if($level == 'ACT')
            {

                if ( !isset($approved->act_reviewed )  ) {
                    ?>
                    <input type="checkbox" name="check[]" class="checkbox1 " value="<?php echo $activity_id ?>"/>
                    <span class="label label-warning">Not Reviewed </span>
                    <?php
                }
                else {
                    ?>
                    <button class=" btn label label-info unreview" data-url="<?php echo url() ?>/approved/untag" data-id="<?php echo $activity_id ?>"> Reopen for editing </button>
                    <span class="label label-success">Reviewed </span>
                    <?php
                }
            }
            if( $level == 'SRPMO' )
            {
                // check if not reviewed by srpmo
                if ( !isset( $approved->srpmo_reviewed ) && $user->office_level == 'SRPMO'  ) {
                    ?>
                    <input type="checkbox" name="check[]" class="checkbox1 " value="<?php echo $activity_id ?>"/>
                    <span class="label label-warning">No </span>
                <?php
                } else if ( !isset( $approved->srpmo_reviewed ) ) {
                    ?>
                    <span class="label label-warning">No </span>
                <?php
                } else {
                    ?>
                    <span class="label label-success">Yes</span>
                <?php
                }
            }

            if( $level == 'RPMO' )
            {
                // check if not reviewed by rpmo
                if( !isset( $approved->rpmo_approved ) && !isset( $approved->srpmo_reviewed )  ) {
                    ?>
                    <span class="label label-warning">Not Yet </span>
                <?php
                }
                else if ( !isset( $approved->rpmo_approved) && $user->office_level == 'RPMO'  ) {
                    ?>
                    <input type="checkbox" name="check[]" class="checkbox1 " value="<?php echo $activity_id ?>"/>
                    <span class="label label-info">For Review </span>
                <?php
                }
                else if ( !isset( $approved->rpmo_approved) && $user->office_level == 'SRPMO'  ) {
                    ?>
                    <span class="label label-info">Not Yet </span>
                <?php
                }
                else {
                    ?>
                    <span class="label label-success">Yes</span>
                <?php
                }

            }
        }
        else
        {

            if($level == 'ACT')
            {
                if ( !isset( $approved->act_reviewed )  ) {
                    ?>
                    <input type="checkbox" name="check[]" class="checkbox1 " value="<?php echo $activity_id ?>"/>
                    <span class="label label-warning">Not Reviewed </span>
                <?php
                }
                else {
                    ?>
                    <span class="label label-success">Reviewed </span>
                <?php
                }
            }
            if( $level == 'SRPMO' )
            {
                // check if not reviewed by srpmo
                if ( !isset( $approved->srpmo_reviewed ) && $user->office_level == 'SRPMO'  ) {
                    ?>
                    <input type="checkbox" name="check[]" class="checkbox1 " value="<?php echo $activity_id ?>"/>
                    <span class="label label-warning">No </span>
                <?php
                } else if ( !isset( $approved->srpmo_reviewed ) ) {
                    ?>
                    <span class="label label-warning">No </span>
                <?php
                } else {
                    ?>
                    <span class="label label-success">Yes</span>
                <?php
                }
            }

            if( $level == 'RPMO' )
            {
                // check if not reviewed by rpmo
                if( !isset( $approved->rpmo_approved ) && !isset( $approved->srpmo_reviewed )  ) {
                    ?>
                    <span class="label label-info">No </span>
                <?php
                }
                else if ( !isset( $approved->rpmo_approved) && isset( $approved->srpmo_reviewed ) ) {
                    ?>
                    <input type="checkbox" name="check[]" class="checkbox1 " value="<?php echo $activity_id ?>"/>
                    <span class="label label-info">For Review </span>
                <?php
                }

            }

        }


    }

    public function getCeac($username)
    {
        $user = \User::where('username',$username)->first();
        $mode = \Session::get('accelerated');
        if($user->office_level == 'ACT')
        {
            return \Ceac::where('psgc_id',$user->psgc_id)
                            ->where('kc_mode',$mode)->get();
        }
        else if($user->office_level == 'SRPMO')
        {

        }
        else if($user->office_level == 'RPMO')
        {

        }
        else
        {

        }
    }


    public function insertceac($data , $activity_id , $activity_type , $activity_name = "")
    {
         $lgu_activity = array(
            'activity_id' => $activity_id,
            'activity_type' => $activity_type,
            'activity_name' => $activity_name,
            'psgc_id' => $data['psgc_id'],
            'cycle_id'=>$data['cycle_id'],
            'program_id'=>$data['program_id'],
            'startdate' => $data['start_date'],
            'enddate' => $data['end_date'],
            'kc_mode'=> \Session::get('accelerated')
            );

                $lgu = \LguActivity::where( ['activity_id'=>$activity_id,'psgc_id'=>$data["psgc_id"] ])->exists();
                if($lgu){

                    $lgu_activity = [
                                'activity_name'=>$activity_name
                    ];

                    \LguActivity::where( [
                        'activity_id'=>$activity_id,
                        'psgc_id'=>$data["psgc_id"],
                        'activity_name'=>$lgu_activity['activity_name']
                    ])->update($lgu_activity);
                
                }else{

                    \LguActivity::create($lgu_activity);

                }
   
    }



 public function approval_statusv( $activity_id, $level = 'SRPMO')
    {
        $session_user = \Session::get('username');
//        $user_id = $this->user->getPsgcId($session_user);
        $user = \User::where('username',$session_user)->first();
        $mode = \Session::get('accelerated');
        $approved = \Approval::where( 'activity_id',$activity_id )
            ->where('kc_mode',$mode)->first();

        if($approved!=null)
        {
            if($level == 'ACT')
            {

                if ( !isset($approved->act_reviewed )  ) {
                    return '<input type="checkbox" name="check[]" class="checkbox1 " value="'.$activity_id .'"/>
                    <span class="label label-warning">Not Reviewed </span>';
                }
                else {
                   return '<span class="label label-success">Reviewed </span>';
                }
            }
            if( $level == 'SRPMO' )
            {
                // check if not reviewed by srpmo
                if ( !isset( $approved->srpmo_reviewed ) && $user->office_level == 'SRPMO'  ) {
                    return '<input type="checkbox" name="check[]" class="checkbox1 " value="<?php echo $activity_id ?>"/>
                    <span class="label label-warning">No </span>';
                } else if ( !isset( $approved->srpmo_reviewed ) ) {
                    
                    return '<span class="label label-warning">No </span>';
                } else {
                   return '<span class="label label-success">Yes</span>';
                }
            }

            if( $level == 'RPMO' )
            {
                // check if not reviewed by rpmo
                if( !isset( $approved->rpmo_approved ) && !isset( $approved->srpmo_reviewed )  ) {
                    ?>
                    <span class="label label-warning">Not Yet </span>
                <?php
                }
                else if ( !isset( $approved->rpmo_approved) && $user->office_level == 'RPMO'  ) {
                    ?>
                    <input type="checkbox" name="check[]" class="checkbox1 " value="<?php echo $activity_id ?>"/>
                    <span class="label label-info">For Review </span>
                <?php
                }
                else if ( !isset( $approved->rpmo_approved) && $user->office_level == 'SRPMO'  ) {
                    ?>
                    <span class="label label-info">Not Yet </span>
                <?php
                }
                else {
                    ?>
                    <span class="label label-success">Yes</span>
                <?php
                }

            }
        }
        else
        {

            if($level == 'ACT')
            {
                if ( !isset( $approved->act_reviewed )  ) {
                    return '<input type="checkbox" name="check[]" class="checkbox1 " value="'. $activity_id .'"/>
                    <span class="label label-warning">Not Revieweds </span>';
                }
                else {
                    return '<span class="label label-success">Reviewed </span>';
                }
            }
            if( $level == 'SRPMO' )
            {
                // check if not reviewed by srpmo
                if ( !isset( $approved->srpmo_reviewed ) && $user->office_level == 'SRPMO'  ) {
                    ?>
                    <input type="checkbox" name="check[]" class="checkbox1 " value="<?php echo $activity_id ?>"/>
                    <span class="label label-warning">No </span>
                <?php
                } else if ( !isset( $approved->srpmo_reviewed ) ) {
                    ?>
                    <span class="label label-warning">No </span>
                <?php
                } else {
                    ?>
                    <span class="label label-success">Yes</span>
                <?php
                }
            }

            if( $level == 'RPMO' )
            {
                // check if not reviewed by rpmo
                if( !isset( $approved->rpmo_approved ) && !isset( $approved->srpmo_reviewed )  ) {
                    ?>
                    <span class="label label-info">No </span>
                <?php
                }
                else if ( !isset( $approved->rpmo_approved) && isset( $approved->srpmo_reviewed ) ) {
                    ?>
                    <input type="checkbox" name="check[]" class="checkbox1 " value="<?php echo $activity_id ?>"/>
                    <span class="label label-info">For Review </span>
                <?php
                }

            }

        }


    }




}