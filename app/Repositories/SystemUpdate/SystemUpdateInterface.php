<?php namespace Repositories\SystemUpdate;

interface SystemUpdateInterface {
        // update for 1.6.4
    public function updatemlcc();
    // update for 1.6.6
    public function updateIDinVol();
    public function updateSpi();
    // update for 1.6.7
    public function updateDuplicates();
    // update for 1.6.8
    public function updateLog();

    // update 1.7
    public function updateDB();

    // update 1.7.1
    public function updateGrsDB();

}