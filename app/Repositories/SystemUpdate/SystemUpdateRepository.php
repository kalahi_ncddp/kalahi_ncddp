<?php namespace Repositories\SystemUpdate;
use Repositories\GRS\GRSInterface;
use Repositories\Reports\ReportsInterface;
use \Carbon\Carbon;
use \Schema;
use \DB;

class SystemUpdateRepository implements  SystemUpdateInterface {

    public function updateERS()
    {
      DB::statement("ALTER TABLE `spi_workers` CHANGE `sex` `sex` INT(12) NULL DEFAULT NULL COMMENT 'Male = 0 ,Female = 1';");
    }

    public function updateProcurement()
    {
        $app = storage_path();
        // kc_munis

       DB::unprepared(file_get_contents($app.'/updatefiles/update_proc.sql'));

        

    }
     public function updateMIBFproject()
    {
        $app = storage_path();
        // kc_munis

       DB::unprepared(file_get_contents($app.'/updatefiles/floatfix.sql'));

        

    }
    // update for 1.6.4
    public function updatemlcc(){
        if(!Schema::hasColumn('spi_mlccmain','program_id')){
            DB::statement("ALTER TABLE spi_mlccmain ADD program_id VARCHAR(20) NULL , ADD cycle_id VARCHAR(20) NULL;");
        }
    }
            
    public function updateIDinVol(){
        if(!Schema::hasColumn('kc_volunteers','id')){
            DB::statement('ALTER TABLE kc_brgyfacility CHANGE facility_distance facility_distance FLOAT(2) NULL DEFAULT NULL;');
            DB::statement('ALTER TABLE kc_brgydevproj CHANGE cost_sharing cost_sharing VARCHAR(20) NULL DEFAULT NULL;');
            DB::statement('ALTER TABLE kc_beneficiary DROP PRIMARY KEY;');
            DB::statement('ALTER TABLE kc_beneficiary  ADD id INT NOT NULL AUTO_INCREMENT  FIRST,  ADD   PRIMARY KEY  (id);');
            DB::statement('ALTER TABLE kc_volunteers DROP PRIMARY KEY;');
            DB::statement('ALTER TABLE kc_volunteers  ADD id INT NOT NULL AUTO_INCREMENT  FIRST,  ADD   PRIMARY KEY  (id);');
        }
    }

    public function updateSpi(){
        DB::statement("ALTER TABLE spi_accomplishment_items CHANGE unit_cost unit_cost FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_accomplishment_items CHANGE quantity quantity FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_accomplishment_items CHANGE amount amount FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_accomplishment_items CHANGE p_weight p_weight FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_planned_pow_items CHANGE financial financial FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_planned_pow_items CHANGE physical physical FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_accomplishment_report CHANGE accomplished accomplished FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_summary_pow_items CHANGE cumm_plnd_physical cumm_plnd_physical FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_summary_pow_items CHANGE noncumm_actl_physical noncumm_actl_physical FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_summary_pow_items CHANGE cumm_actl_physical cumm_actl_physical FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_summary_pow_items CHANGE noncumm_plnd_financial noncumm_plnd_financial FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_summary_pow_items CHANGE cumm_actl_financial cumm_actl_financial FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_summary_pow_items CHANGE noncumm_actl_financial noncumm_actl_financial FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_summary_pow_items CHANGE slippage slippage FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_summary_pow_items CHANGE noncumm_plnd_physical noncumm_plnd_physical FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_actual_pow_items CHANGE physical physical FLOAT NULL DEFAULT NULL");
        DB::statement("ALTER TABLE spi_actual_pow_items CHANGE financial financial FLOAT NULL DEFAULT NULL");
    }

    public function updateDuplicates()
    {
        $app = storage_path();
        // kc_munis

        $kc_munis = DB::table('kc_munis')->count();
        if($kc_munis > 1000)
        {
            DB::unprepared(file_get_contents($app.'/updatefiles/kc_munis.sql'));
        }

            DB::unprepared(file_get_contents($app.'/updatefiles/duplicates.sql'));



    }

    // public function removeDuplicateInVolcommem()
    // {
    //      for($i = 0 ; $i <= 9 ; $i++){
    //         DB::select(DB::raw('delete from kc_volcommmem where id in (select id from (SELECT id FROM kc_volcommmem GROUP BY volunteer_id, committee,position HAVING COUNT(*) > 1) t)'));
    //      }
    // }

        // this will create an update log table 
    public function updateLog()
    {
            $app = storage_path();
            DB::unprepared(file_get_contents($app.'/updatefiles/log.sql'));
    }
    public function add_spi_utlization()
    {
            $app = storage_path();
            DB::unprepared(file_get_contents($app.'/updatefiles/add_utilization.sql'));
    }


    public function updateDB()
    {
            $app = storage_path();
              DB::statement('DELETE FROM  `kc_ncddp`.`kc_brgyfacility` WHERE `kc_brgyfacility`.`facility_name` = "Environmental Protection";');
              DB::statement('DELETE FROM `kc_ncddp`.`kc_brgyfacility` WHERE `kc_brgyfacility`.`facility_name` = "Postal Service";');
              
              DB::statement('DELETE FROM  `kc_ncddp`.`rf_facilities` WHERE `rf_facilities`.`name` = "Environmental Protection";');
              DB::statement('DELETE FROM `kc_ncddp`.`rf_facilities` WHERE `rf_facilities`.`name` = "Postal Service";');
              DB::statement("ALTER TABLE `kc_brgyfacility` CHANGE `facility_distance` `facility_distance` FLOAT(2) NULL DEFAULT NULL;");
              DB::statement("ALTER TABLE `kc_brgyprofile` CHANGE `km_frmtown` `km_frmtown` FLOAT(2) NULL DEFAULT NULL;");
              if(!Schema::hasColumn('kc_brgyprofile','mins_totown')){
                DB::statement("ALTER TABLE `kc_brgyprofile` ADD `mins_totown` VARCHAR(100) NULL ;");
              }
              DB::unprepared('DROP TRIGGER IF EXISTS `spi_actual_pow_items_tau`');
              DB::unprepared('DROP TRIGGER IF EXISTS `spi_actual_pow_items_tai`');
              DB::unprepared("
CREATE TRIGGER `spi_actual_pow_items_tai` AFTER INSERT ON `spi_actual_pow_items`
 FOR EACH ROW BEGIN
    DECLARE new_noncumm_actl_physical, new_noncumm_actl_financial FLOAT DEFAULT 0;
    DECLARE v_physical, v_financial, new_cumm_actl_physical, new_cumm_actl_financial, t_percent FLOAT DEFAULT 0;
    DECLARE new_acc_report_id, cur_id VARCHAR(255);
    DECLARE v_month_num FLOAT;
    /* 
      1. update current summary date
    */    
    SELECT acc_report_id, month_num
    INTO new_acc_report_id, v_month_num
    FROM spi_planned_pow_items
    WHERE id = NEW.id;

    BLOCK2: 
    BEGIN
      DECLARE before_summary_date CURSOR FOR 
      SELECT spi_actual_pow_items.physical, spi_actual_pow_items.financial 
      FROM spi_actual_pow_items
      INNER JOIN spi_planned_pow_items
      ON 
        spi_planned_pow_items.id = spi_actual_pow_items.id
      WHERE 
        spi_planned_pow_items.acc_report_id = new_acc_report_id AND
        spi_planned_pow_items.month_num < v_month_num;

      OPEN before_summary_date;
      BEGIN
        DECLARE done FLOAT DEFAULT 0;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        before_summary_date_loop: LOOP
          FETCH before_summary_date INTO v_physical, v_financial;
          IF done THEN LEAVE before_summary_date_loop;
          END IF;
          SET new_cumm_actl_physical = new_cumm_actl_physical + v_physical;
          SET new_cumm_actl_financial = new_cumm_actl_financial + v_financial;
        END LOOP before_summary_date_loop;
      END;
      CLOSE before_summary_date;
    END BLOCK2;

    SELECT id, COALESCE(noncumm_actl_physical, 0), COALESCE(noncumm_actl_financial, 0)
    INTO cur_id, new_noncumm_actl_physical , new_noncumm_actl_financial
    FROM spi_summary_pow_items 
    WHERE 
      acc_report_id = new_acc_report_id AND
      month_num = v_month_num;

    SET new_noncumm_actl_physical  = new_noncumm_actl_physical  + NEW.physical;
    SET new_noncumm_actl_financial = new_noncumm_actl_financial + NEW.financial;
    SET new_cumm_actl_physical = new_cumm_actl_physical + new_noncumm_actl_physical;
    SET new_cumm_actl_financial = new_cumm_actl_financial + new_noncumm_actl_financial;
    UPDATE spi_summary_pow_items 
    SET 
      noncumm_actl_physical  = new_noncumm_actl_physical,
      noncumm_actl_financial = new_noncumm_actl_financial,
      cumm_actl_physical     = new_cumm_actl_physical,
      cumm_actl_financial    = new_cumm_actl_financial
    WHERE id = cur_id;

    /* 
      2. update all after summary entries
    */    
    SET new_cumm_actl_physical = 0;
    SET new_cumm_actl_financial = 0;
    BLOCK3: 
    BEGIN
      DECLARE now_id VARCHAR(255);
      DECLARE v_cumm_actl_physical, v_cumm_actl_financial FLOAT DEFAULT 0;
      DECLARE after_summary_date CURSOR FOR 
      SELECT id, cumm_actl_physical, cumm_actl_financial FROM spi_summary_pow_items 
      WHERE 
        acc_report_id = new_acc_report_id AND
        month_num > v_month_num;


      OPEN after_summary_date;
      BEGIN
        DECLARE done INT DEFAULT 0;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        after_summary_date_loop: LOOP
          FETCH after_summary_date INTO now_id, v_cumm_actl_physical, v_cumm_actl_financial;
          IF done THEN LEAVE after_summary_date_loop;
          END IF;
          INSERT INTO debug values(CONCAT(now_id, '-', v_cumm_actl_physical, '-', v_cumm_actl_financial));
          SET new_cumm_actl_physical  = v_cumm_actl_physical + NEW.physical;
          SET new_cumm_actl_financial = v_cumm_actl_financial + NEW.financial;
          UPDATE spi_summary_pow_items 
          SET 
            cumm_actl_physical  = new_cumm_actl_physical,
            cumm_actl_financial = new_cumm_actl_financial
          WHERE id = now_id;
        END LOOP after_summary_date_loop;
      END;
      CLOSE after_summary_date;
    END BLOCK3;

  BLOCK4: 
    BEGIN
    DECLARE v_percent FLOAT DEFAULT 0;
    DECLARE total_percentages_per_month CURSOR FOR 
      SELECT COALESCE(cumm_actl_physical,0)
      FROM spi_summary_pow_items 
      WHERE acc_report_id = new_acc_report_id
      ORDER BY month_num DESC;
    SET t_percent = 0;
    OPEN total_percentages_per_month;
    BEGIN
      DECLARE done INT DEFAULT 0;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
      total_percentages_per_month_loop: LOOP
        FETCH total_percentages_per_month INTO v_percent;
        IF done THEN LEAVE total_percentages_per_month_loop;
        END IF;
        SET t_percent  = t_percent + v_percent;
        END LOOP total_percentages_per_month_loop;
    END;
    CLOSE total_percentages_per_month;
    
    UPDATE spi_accomplishment_report
    SET
      accomplished = t_percent
    WHERE acc_report_id = new_acc_report_id;
  END BLOCK4;
END");
  DB::unprepared("
CREATE TRIGGER `spi_actual_pow_items_tau` AFTER UPDATE ON `spi_actual_pow_items`
 FOR EACH ROW BEGIN
    DECLARE new_noncumm_actl_physical, new_noncumm_actl_financial FLOAT DEFAULT 0;
    DECLARE v_physical, v_financial, new_cumm_actl_physical, new_cumm_actl_financial, t_percent FLOAT DEFAULT 0;
    DECLARE new_acc_report_id, cur_id VARCHAR(255);
    DECLARE v_month_num INT;
    /* 
      1. update current summary date
    */    
    SELECT acc_report_id, month_num
    INTO new_acc_report_id, v_month_num
    FROM spi_planned_pow_items
    WHERE id = NEW.id;

    BLOCK2: 
    BEGIN
      DECLARE before_summary_date CURSOR FOR 
      SELECT spi_actual_pow_items.physical, spi_actual_pow_items.financial 
      FROM spi_actual_pow_items
      INNER JOIN spi_planned_pow_items
      ON 
        spi_planned_pow_items.id = spi_actual_pow_items.id
      WHERE 
        spi_planned_pow_items.acc_report_id = new_acc_report_id AND
        spi_planned_pow_items.month_num < v_month_num;

      OPEN before_summary_date;
      BEGIN
        DECLARE done INT DEFAULT 0;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        before_summary_date_loop: LOOP
          FETCH before_summary_date INTO v_physical, v_financial;
          IF done THEN LEAVE before_summary_date_loop;
          END IF;
          SET new_cumm_actl_physical = new_cumm_actl_physical + v_physical;
          SET new_cumm_actl_financial = new_cumm_actl_financial + v_financial;
        END LOOP before_summary_date_loop;
      END;
      CLOSE before_summary_date;
    END BLOCK2;

    SELECT id, COALESCE(noncumm_actl_physical, 0), COALESCE(noncumm_actl_financial, 0)
    INTO cur_id, new_noncumm_actl_physical , new_noncumm_actl_financial
    FROM spi_summary_pow_items 
    WHERE 
      acc_report_id = new_acc_report_id AND
      month_num = v_month_num;

    SET new_noncumm_actl_physical  = GREATEST(new_noncumm_actl_physical  - OLD.physical, 0) +  NEW.physical;
    SET new_noncumm_actl_financial = GREATEST(new_noncumm_actl_financial - OLD.financial, 0) + NEW.financial;
    SET new_cumm_actl_physical = new_cumm_actl_physical + new_noncumm_actl_physical;
    SET new_cumm_actl_financial = new_cumm_actl_financial + new_noncumm_actl_financial;
    UPDATE spi_summary_pow_items 
    SET 
      noncumm_actl_physical  = new_noncumm_actl_physical,
      noncumm_actl_financial = new_noncumm_actl_financial,
      cumm_actl_physical     = new_cumm_actl_physical,
      cumm_actl_financial    = new_cumm_actl_financial
    WHERE id = cur_id;

    /* 
      2. update all after summary entries
    */    
    SET new_cumm_actl_physical = 0;
    SET new_cumm_actl_financial = 0;
    BLOCK3: 
    BEGIN
      DECLARE now_id VARCHAR(255);
      DECLARE v_cumm_actl_physical, v_cumm_actl_financial FLOAT DEFAULT 0;
      DECLARE after_summary_date CURSOR FOR 
      SELECT id, cumm_actl_physical, cumm_actl_financial FROM spi_summary_pow_items 
      WHERE 
        acc_report_id = new_acc_report_id AND
        month_num > v_month_num;


      OPEN after_summary_date;
      BEGIN
        DECLARE done INT DEFAULT 0;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        after_summary_date_loop: LOOP
          FETCH after_summary_date INTO now_id, v_cumm_actl_physical, v_cumm_actl_financial;
          IF done THEN LEAVE after_summary_date_loop;
          END IF;
          INSERT INTO debug values(CONCAT(now_id, '-', v_cumm_actl_physical, '-', v_cumm_actl_financial));
          SET new_cumm_actl_physical  = GREATEST(v_cumm_actl_physical - OLD.physical, 0) + NEW.physical;
          SET new_cumm_actl_financial = GREATEST(v_cumm_actl_financial - OLD.financial, 0) + NEW.financial;
          UPDATE spi_summary_pow_items 
          SET 
            cumm_actl_physical  = new_cumm_actl_physical,
            cumm_actl_financial = new_cumm_actl_financial
          WHERE id = now_id;
        END LOOP after_summary_date_loop;
      END;
      CLOSE after_summary_date;
    END BLOCK3;

    /* UPDATE TOTAL PERCENTAGE */
  BLOCK4: 
    BEGIN
    DECLARE v_percent FLOAT DEFAULT 0;
    DECLARE total_percentages_per_month CURSOR FOR 
      SELECT COALESCE(cumm_actl_physical,0)
      FROM spi_summary_pow_items 
      WHERE acc_report_id = new_acc_report_id
      ORDER BY month_num DESC;
    SET t_percent = 0;
    OPEN total_percentages_per_month;
    BEGIN
      DECLARE done INT DEFAULT 0;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
      total_percentages_per_month_loop: LOOP
        FETCH total_percentages_per_month INTO v_percent;
        IF done THEN LEAVE total_percentages_per_month_loop;
        END IF;
        SET t_percent  = t_percent + v_percent;
        END LOOP total_percentages_per_month_loop;
    END;
    CLOSE total_percentages_per_month;
    
    UPDATE spi_accomplishment_report
    SET
      accomplished = t_percent
    WHERE acc_report_id = new_acc_report_id;
  END BLOCK4;
END");
DB::unprepared("DROP TRIGGER IF EXISTS `spi_planned_pow_items_tad`;");
DB::unprepared("

CREATE TRIGGER `spi_planned_pow_items_tad` AFTER DELETE ON `spi_planned_pow_items`
 FOR EACH ROW BEGIN
  DECLARE new_cumm_plnd_physical, new_cumm_plnd_financial FLOAT DEFAULT 0;
  DECLARE new_noncumm_plnd_physical, new_noncumm_plnd_financial FLOAT DEFAULT 0;
  DECLARE cur_id, now_id VARCHAR(255);
  DECLARE v_physical, v_financial, v_cumm_plnd_physical, v_cumm_plnd_financial FLOAT DEFAULT 0;
  DECLARE before_summary_date CURSOR FOR 
  SELECT physical, financial FROM spi_planned_pow_items 
  WHERE 
    acc_report_id = OLD.acc_report_id AND
    month_num < OLD.month_num;
  DECLARE after_summary_date CURSOR FOR 
    SELECT id, cumm_plnd_physical, cumm_plnd_financial FROM spi_summary_pow_items 
    WHERE 
      acc_report_id = OLD.acc_report_id AND
      month_num > OLD.month_num;
  /* 
    1. insert/update current summary date
  */    
  OPEN before_summary_date;
  BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    before_summary_date_loop: LOOP
      FETCH before_summary_date INTO v_physical, v_financial;
      IF done THEN LEAVE before_summary_date_loop;
      END IF;
      SET new_cumm_plnd_physical = new_cumm_plnd_physical + v_physical;
      SET new_cumm_plnd_financial = new_cumm_plnd_financial + v_financial;
    END LOOP before_summary_date_loop;
  END;
  CLOSE before_summary_date;

  SELECT id, noncumm_plnd_physical, noncumm_plnd_financial
  INTO cur_id, new_noncumm_plnd_physical , new_noncumm_plnd_financial
  FROM spi_summary_pow_items 
  WHERE 
    acc_report_id = OLD.acc_report_id AND
    month_num     = OLD.month_num;

  SET new_noncumm_plnd_physical  = new_noncumm_plnd_physical - OLD.physical;
  SET new_noncumm_plnd_financial = new_noncumm_plnd_financial - OLD.financial;
  SET new_cumm_plnd_physical     = new_cumm_plnd_physical + new_noncumm_plnd_physical;
  SET new_cumm_plnd_financial    = new_cumm_plnd_financial + new_noncumm_plnd_financial;

  UPDATE spi_summary_pow_items 
  SET 
    noncumm_plnd_physical = new_noncumm_plnd_physical,
    noncumm_plnd_financial = new_noncumm_plnd_financial,
    cumm_plnd_physical = new_cumm_plnd_physical,
    cumm_plnd_financial = new_cumm_plnd_financial
  WHERE id = cur_id;

    /* 
      2. update all after summary entries
    */    
    SET new_cumm_plnd_physical = 0;
    SET new_cumm_plnd_financial = 0;
    OPEN after_summary_date;
    BEGIN
      DECLARE done INT DEFAULT 0;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

      after_summary_date_loop: LOOP
        FETCH after_summary_date INTO now_id, v_cumm_plnd_physical, v_cumm_plnd_financial;
        IF done THEN LEAVE after_summary_date_loop;
        END IF;
        SET new_cumm_plnd_physical  = v_cumm_plnd_physical - OLD.physical;
        SET new_cumm_plnd_financial = v_cumm_plnd_financial - OLD.financial;
        UPDATE spi_summary_pow_items 
        SET 
          cumm_plnd_physical  = new_cumm_plnd_physical,
          cumm_plnd_financial = new_cumm_plnd_financial
        WHERE id = now_id;
      END LOOP after_summary_date_loop;
    END;
    CLOSE after_summary_date;

  /*
  3. DELETE summary date if no planned and actual
  */
  DELETE FROM spi_summary_pow_items 
  WHERE 
    acc_report_id = OLD.acc_report_id AND
    (noncumm_plnd_physical = 0 OR noncumm_plnd_physical IS NULL) AND
    (noncumm_plnd_financial = 0 OR noncumm_plnd_financial IS NULL) AND
    (noncumm_actl_physical = 0 OR noncumm_actl_physical IS NULL) AND
    (noncumm_actl_financial = 0 OR noncumm_actl_financial IS NULL);
END");
DB::unprepared("DROP TRIGGER IF EXISTS `spi_planned_pow_items_tai`;");
DB::unprepared("
CREATE TRIGGER `spi_planned_pow_items_tai` AFTER INSERT ON `spi_planned_pow_items`
 FOR EACH ROW BEGIN
    DECLARE new_noncumm_plnd_physical, new_noncumm_plnd_financial, new_cumm_plnd_physical, new_cumm_plnd_financial FLOAT DEFAULT 0;
    DECLARE v_physical, v_financial, v_cumm_plnd_physical, v_cumm_plnd_financial FLOAT DEFAULT 0;
    DECLARE cur_id, v_remarks, now_id VARCHAR(255);
    DECLARE count INT DEFAULT 1;
    DECLARE before_summary_date CURSOR FOR 
    SELECT physical, financial FROM spi_planned_pow_items 
    WHERE 
      acc_report_id = NEW.acc_report_id AND
      month_num < NEW.month_num;
    DECLARE after_summary_date CURSOR FOR 
    SELECT id, cumm_plnd_physical, cumm_plnd_financial FROM spi_summary_pow_items 
    WHERE 
      acc_report_id = NEW.acc_report_id AND
      month_num > NEW.month_num;
    /* 
      1. insert/update current summary date
    */    
    OPEN before_summary_date;
    BEGIN
      DECLARE done INT DEFAULT 0;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

      before_summary_date_loop: LOOP
        FETCH before_summary_date INTO v_physical, v_financial;
        IF done THEN LEAVE before_summary_date_loop;
        END IF;
        SET new_cumm_plnd_physical = new_cumm_plnd_physical + v_physical;
        SET new_cumm_plnd_financial = new_cumm_plnd_financial + v_financial;
      END LOOP before_summary_date_loop;
    END;
    CLOSE before_summary_date;

    SELECT id, noncumm_plnd_physical, noncumm_plnd_financial
    INTO cur_id, new_noncumm_plnd_physical , new_noncumm_plnd_financial
    FROM spi_summary_pow_items 
    WHERE 
      acc_report_id = NEW.acc_report_id AND
      month_num = NEW.month_num;

    IF cur_id IS NULL THEN
      SET new_noncumm_plnd_physical = NEW.physical;
      SET new_noncumm_plnd_financial = NEW.financial;
      SET new_cumm_plnd_physical = new_cumm_plnd_physical + new_noncumm_plnd_physical;
      SET new_cumm_plnd_financial = new_cumm_plnd_financial + new_noncumm_plnd_financial;

      /* 1. */
      /* 2. */
      INSERT INTO spi_summary_pow_items(acc_report_id, month_num, noncumm_plnd_physical, noncumm_plnd_financial, cumm_plnd_physical, cumm_plnd_financial, remarks) 
      VALUES (NEW.acc_report_id, NEW.month_num, new_noncumm_plnd_physical, new_noncumm_plnd_financial, new_cumm_plnd_physical, new_cumm_plnd_financial, v_remarks);
    ELSE
      SET new_noncumm_plnd_physical = new_noncumm_plnd_physical + NEW.physical;
      SET new_noncumm_plnd_financial = new_noncumm_plnd_financial + NEW.financial;
      SET new_cumm_plnd_physical = new_cumm_plnd_physical + new_noncumm_plnd_physical;
      SET new_cumm_plnd_financial = new_cumm_plnd_financial + new_noncumm_plnd_financial;

      UPDATE spi_summary_pow_items 
      SET 
        noncumm_plnd_physical = new_noncumm_plnd_physical,
        noncumm_plnd_financial = new_noncumm_plnd_financial,
        cumm_plnd_physical = new_cumm_plnd_physical,
        cumm_plnd_financial = new_cumm_plnd_financial,
        remarks = v_remarks
      WHERE id = cur_id;
    END IF;

    /* 
      2. update all after summary entries
    */    
    SET new_cumm_plnd_physical = 0;
    SET new_cumm_plnd_financial = 0;
    OPEN after_summary_date;
    BEGIN
      DECLARE done INT DEFAULT 0;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

      select FOUND_ROWS() INTO count;
      INSERT INTO debug values(count);
      after_summary_date_loop: LOOP
        FETCH after_summary_date INTO now_id, v_cumm_plnd_physical, v_cumm_plnd_financial;
        IF done THEN LEAVE after_summary_date_loop;
        END IF;
        INSERT INTO debug values(CONCAT(now_id, '-', v_cumm_plnd_physical, '-', v_cumm_plnd_financial));
        SET new_cumm_plnd_physical  = v_cumm_plnd_physical + NEW.physical;
        SET new_cumm_plnd_financial = v_cumm_plnd_financial + NEW.financial;
        UPDATE spi_summary_pow_items 
        SET 
          cumm_plnd_physical  = new_cumm_plnd_physical,
          cumm_plnd_financial = new_cumm_plnd_financial,
          remarks = count
        WHERE id = now_id;
      END LOOP after_summary_date_loop;
    END;
    CLOSE after_summary_date;
END
");
            // DB::unprepared(file_get_contents($app.'/updatefiles/spi_actual_trigger.sql'));
            $categoryexist = DB::table('spi_category')->where('sub_category','Traditional Skills Training')->exists();
            if(!$categoryexist)
            {
                DB::unprepared(file_get_contents($app.'/updatefiles/spicat.sql'));
            }
    
    }


    /*
      This update will add id for every column that has no ID
    */
    public function updateGrsDB()
    {
      $app = storage_path();

              if(!Schema::hasColumn('kc_grsbrgyinstall','id')){
                DB::statement('ALTER TABLE kc_grsbrgyinstall DROP PRIMARY KEY');
                DB::statement('ALTER TABLE `kc_grsbrgyinstall` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`) ;');
                DB::statement('ALTER TABLE `kc_grsbrgyinstall` CHANGE `reference_no` `reference_no` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;');
              }

              if(!Schema::hasColumn('kc_grsmuninstall','id')){
                DB::statement('ALTER TABLE kc_grsmuninstall DROP PRIMARY KEY');
                DB::statement('ALTER TABLE `kc_grsmuninstall` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`) ;');
                DB::statement('ALTER TABLE `kc_grsmuninstall` CHANGE `reference_no` `reference_no` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;');
              }


            if(Schema::hasColumn('kc_mlprapagencies','id')){
                  $haspri = DB::select("SELECT EXISTS( SELECT 1 FROM information_schema.columns WHERE table_schema = 'kc_ncddp' and table_name='kc_mlprapagencies' and column_key = 'PRI' ) As HasPrimaryKey ")[0]->HasPrimaryKey;

                  if($haspri==0)
                  {
                    DB::statement("ALTER TABLE `kc_mlprapagencies` ADD PRIMARY KEY(`id`);");
                  }
              
              }

              if(Schema::hasColumn('kc_mlprapleadgroups','id')){
                  $haspri = DB::select("SELECT EXISTS( SELECT 1 FROM information_schema.columns WHERE table_schema = 'kc_ncddp' and table_name='kc_mlprapleadgroups' and column_key = 'PRI' ) As HasPrimaryKey ")[0]->HasPrimaryKey;

                  if($haspri==0)
                  {
                    DB::statement("ALTER TABLE `kc_mlprapleadgroups` ADD PRIMARY KEY(`id`);");
                  }
              
              }

              if(!Schema::hasColumn('kc_grsintake','id')){
                DB::statement('ALTER TABLE kc_grsintake DROP PRIMARY KEY');
                DB::statement('ALTER TABLE `kc_grsintake` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`) ;');
                DB::statement('ALTER TABLE `kc_grsintake` CHANGE `reference_no` `reference_no` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;');
              }

              $tables = DB::select('SHOW TABLES;');
              array_splice($tables, 123,162);
              $table = [];
              foreach ($tables as $key => $value) {
                # code...
                  $haspri = DB::select("SELECT EXISTS( SELECT 1 FROM information_schema.columns WHERE table_schema = 'kc_ncddp' and table_name='".$value->Tables_in_kc_ncddp."' and column_key = 'PRI' ) As HasPrimaryKey ")[0]->HasPrimaryKey;
                  if($haspri==0)
                  {
                    $table[] = $value->Tables_in_kc_ncddp;
                  }
              }
              unset($table[0]);
              foreach ($table as $key => $value) {

                 if(!Schema::hasColumn($value,'id')){
                    DB::statement('ALTER TABLE '.$value.' ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`) ;');
                 }  
              }
           
    }
}