<?php namespace Repositories\Users;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserRepository implements  UsersInterface
{

    /**
     * @param $id
     * @return mixed
     */
    public function getOfficelevelByPsgc($id)
    {
        $user = \User::where('psgc_id', $id)->first();
        return $user->office_level;
    }

    /**
     * @param $input
     */
    public function insertNewUser($input)
    {
        $username = Session::get('username');
        $user = \User::where('username', $username)->first();
        $municipality = \Municipality::find($user->psgc_id);
        $office_level = $user->office_level;
        $input['municipality'] = $municipality->municipality;
        $input['office_level'] = $office_level;
        $input['password'] = Hash::make($input['password']);

        return \User::create($input);
    }

    public function updateUser($id, $input)
    {
        unset($input['_method']);
        unset($input['_token']);

        if($input['new_password']==""){
            unset($input['new_password']);
        }else{
            $input['password'] = Hash::make($input['new_password']);
            unset($input['new_password']);
        }
        return \User::where('id', $id)->update($input);
    }

    public function getAllEncoderByMuni($id)
    {
        return \User::where('psgc_id', $id)
            ->where('id', '!=', \Auth::user()->id)->get();
    }


    public function getEncoder($id)
    {
        return \User::find($id);
    }

    public function getEncoderById($id)
    {
        return \User::where('username', $id)->first();
    }

    public function getPsgcId($id)
    {
        return \User::where('username',$id)->first()->psgc_id;
    }

    public function generateEmployeeId($psgc)
    {
        $year = date('Y');
        $count = \Staff::where('psgc_id',$psgc)->count();
        $lastnumber = sprintf("%06d", $count+1);
        $employee = 'E'.$year.$psgc.$lastnumber;
        return $employee;   
    }

    public function getOfficelevelByUsername($username)
    {
        return \User::where('username',$username)->first()->office_level;
    }

    public function insertNewHighLevelUser($input)
    {

        $id = Session::get('username');
        // $municipality = \Municipality::find($id);
        $office_level = $this->getOfficelevelByUsername($id);

        if($office_level=="NRPMO")
        {
            $input['office_level'] = 'RPMO';
        }
        else if($office_level=='RPMO')
        {
            $input['office_level'] = 'SRPMO';
        }

        // $input['municipality'] = $municipality->municipality;
        // $input['office_level'] = $office_level;
        $input['password'] = Hash::make($input['password']);

        return \User::create($input);

    }
    public function getRegionOfficeName($user)
    {
        $user = \User::where('username',$user)->first();

        if($user->office_level=='ACT'){
            return \Municipality::where('municipality_psgc',$user->psgc_id)->first()->province->region->region_name;
        } 
        else if ($user->office_level=='SRPMO'){

        }
    }
}
