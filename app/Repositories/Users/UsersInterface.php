<?php namespace Repositories\Users;

interface UsersInterface{

    public function getOfficelevelByPsgc($id);

    public function insertNewUser($input);

    public function getAllEncoderByMuni($id);

    public function getEncoderById($id);

    public function updateUser($id, $input);

    public function getPsgcId($id);

    public function generateEmployeeId($psgc);


    public function getOfficelevelByUsername($username);

    public function insertNewHighLevelUser($input);

    public function getRegionOfficeName($user);
}