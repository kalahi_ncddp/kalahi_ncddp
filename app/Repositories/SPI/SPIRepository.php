<?php namespace Repositories\SPI;
use Illuminate\Support\Facades\Session;
use BaseController;
class SPIRepository implements SPIInterface{

  	public function getSubProjectProfile($user,$mode=0)
  	{
  		// dd($user);
      $mode = \Session::get('accelerated');  
  		$MunicipalForum = \MunicipalForum::where('psgc_id',$user)->where('kc_mode',$mode);
  		if($MunicipalForum->exists()){
        return $MunicipalForum->first()->subproject();
  		}else{
  			return \SPIProfile::where('municipal_id',$user)->where('kc_mode',$mode)->get();
  		}
  	}



}