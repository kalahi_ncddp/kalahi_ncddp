<?php namespace Repositories\GRS;
use Illuminate\Support\Facades\Session;
use BaseController;
class GRSRepository  implements GRSInterface {

    /**
     * Returns the list of barangay based on municipal ID it list the municipals barangay
     * @paran int id
     * @return array
     */
    public function getBarangayList($id)
    {
        $barangays = \Municipality::find($id)->barangay;
        $barangay_list = array();
        $barangay_list[''] = 'Select a Barangay';
        foreach($barangays as $barangay  )
        {
            $barangay_list[$barangay->barangay_psgc] = $barangay->barangay;
        }
        return $barangay_list;

    }
    /**
     * Returns the list of municipality based on region id
     * @paran int id
     * @return array
     */
    public function getMunicipalityList($id)
    {
        $municipal =  \Provinces::where('province_psgc',$id)->first()->municipality;
        $municipalities = [];

        foreach($municipal as $municipality)
        {
            $municipalities[$municipality->municipality_psgc] = $municipality->municipality;
        }
        return $municipalities;
    }

    /**
     * returns all regions
     * @return array
     */
    public function getRegionList()
    {
        $regions = \Regions::get();

        $region_array = [];

        foreach($regions as $region){
            $region_array[$region->psgc_id] = $region->region_name;
        }
        return $region_array;
    }

    public function updateIntake($data,$id)
    {
       

        if(array_key_exists('narrative',$data)){
            foreach($data['narrative'] as $key => $action)
            {
                if($action!=""){
                    $check = \Grsupdates::where('reference_no',$id)->where('action_no',$key)->exists();
                    if(!$check){
                        \Grsupdates::create([
                            'reference_no'=>$id,
                            'action_no'=>$key,
                            'date' => BaseController::setDate($data['date'][$key]),
                            'update_by'=> $data['update_by'][$key],
                            'narrative'=> $action
                        ]);
                    }else{
                        \Grsupdates::where('reference_no',$id)->where('action_no',$key)->update([
                            'date' => BaseController::setDate($data['date'][$key]),
                            'update_by'=> $data['update_by'][$key],
                            'narrative'=> $action
                        ]);
                    }
                }
            }
        }
        unset($data['narrative']);
        unset($data['action_no']);
        unset($data['update_by']);
        unset($data['date']);
        unset($data['reference_no']);
        unset($data['_method']);
        unset($data['_token']);
//        echo "<pre>";
//        dd($data);
        \Intake::where('reference_no',$id)->update($data);
//        dd(\Intake::where('reference_no',$id)->get());
    }
    /**
     * @param array $data
     * @return mixed
     */
    public function insertIntake($data = [],$sequence)
    {


        $data['reference_no'] = $this->getIntakeReferenceId($data['psgc_id'],$data['cycle_id'],2014,$sequence);
        $data['kc_mode'] = \Session::get('accelerated');
        $data['barangay_psgc'] = $data['barangay_id'];

        \Intake::create($data);

        if(array_key_exists('narrative',$data)){
            foreach($data['narrative'] as $key => $action)
            {
                if($action!=""){
                    $check = \Grsupdates::where('reference_no',$data['reference_no'])->where('action_no',$key)->exists();
                    if(!$check){
                        \Grsupdates::create([
                            'reference_no'=>$data['reference_no'],
                            'action_no'=>$key,
                            'date' => BaseController::setDate($data['date'][$key]),
                            'update_by'=> $data['update_by'][$key],
                            'narrative'=> $action
                        ]);
                    }else{
                        \Grsupdates::where('reference_no',$data['reference_no'])->where('action_no',$key)->update([
                            'date' => BaseController::setDate($data['date'][$key]),
                            'update_by'=> $data['update_by'][$key],
                            'narrative'=> $action
                        ]);
                    }
                }
            }
        }

        return $data['reference_no'];



    }

    /**
     * @param $psgc_id
     * @param $cycle_id
     * @return string
     */
    public function getIntakeReferenceId($psgc_id,$cycle_id,$year,$sequence)
    {
        $newsquence = $sequence;

        return "GI".$psgc_id.''.$newsquence;
    }


    /**
     * @param $username
     * @return string
     */
    public function getAllIntakes($username)
    {
        $intakes = \Intake::where('psgc_id','like',substr(\Auth::user()->psgc_id,0,6).'%')->get();
        return $intakes;
    }

    public function getRegionListByPsgc($user)
    {
        $user = \User::where('username',$user)->first();
        if($user->office_level == 'ACT')
        {
            return \Regions::where('psgc_id','like',substr($user->psgc_id, 0,2).'%')->lists('region_name','psgc_id');
        }
        else if($user->office_level=='SRPMO')
        {
            $region_code = substr($user->psgc_id, 0, 2);

            $code = $region_code . sprintf('%07d','');
            $region = \Regions::where('psgc_id',$code)->first();
            return [ $region->psgc_id => $region->region_name];
        }
        else if($user->office_level=='RPMO')
        {
            $region = \Regions::where('psgc_id',$user->psgc_id )->get();
            $region = $region->lists('region_name','psgc_id');
            return $region;
        }
        else{
            $region = \Regions::get();
            $region = $region->lists('region_name','psgc_id');
            return $region;
        }
    }

    public function getProvinceListByPsgc($user)
    {
        $user = \User::where('username',$user)->first();
        if($user->office_level == 'ACT')
        {
            return \Provinces::where('province_psgc','like',substr($user->psgc_id, 0,4).'%')->lists('province','province_psgc');

        }
        else if($user->office_level=='SRPMO')
        {
            $province = \Srpmo::where('srpmo_code',$user->psgc_id)->first()->province;

            return [ $province->province_psgc => $province->province];
        }
        else if($user->office_level=='RPMO')
        {
            $region = \Regions::where('psgc_id',$user->psgc_id)->first()->province();
            $region = $region->lists('province','province_psgc');
            return $region;
        }
        else
        {
            $region = \Provinces::get();
            $region = $region->lists('province','province_psgc');
            return $region;
        }
    }

    public function getProvince($user)
    {
        $user = \User::where('username',$user)->first();
        $prov = \Provinces::where('province_psgc',$user->psgc_id)->lists('province','province_psgc');
        return $prov;
    }

    public function getMunicipalityListByPsgc($user)
    {
        $user = \User::where('username',$user)->first();

        if($user->office_level == 'ACT')
        {
            return \Municipality::where('municipality_psgc','like',substr($user->psgc_id, 0,6).'%')->lists('municipality','municipality_psgc');

        }
        else if($user->office_level=='SRPMO')
        {
            $province = \Srpmo::where('srpmo_code',$user->psgc_id)->first()->municipality();
            return [''=>'']+$province->lists('municipality','municipality_psgc');
        }
        else if($user->office_level=='RPMO')
        {
            $region = \Regions::where('psgc_id',$user->psgc_id)->first()->municipals();

            return [''=>'']+$region->lists('municipality','municipality_psgc');
//            $muni = \Report::get_muni_by_region($user->psgc_id);
//
//            return $muni->lists('municipality','municipality_psgc');
        }
        else{
            $region = \Municipality::get();

            return [''=>'']+$region->lists('municipality','municipality_psgc');
        }
    }

    public function getMunicipality($user)
    {
        $user = \User::where('username',$user)->first();

        if($user->office_level == 'ACT')
        {
            return [];
        }
        else if($user->office_level=='SRPMO')
        {
            return [];
        }
        else if($user->office_level=='RPMO')
        {
            return [];
        }
    }

    public function getPsgcofGRS($input)
    {
        $psgc_id = "";
        if($input['psgc_id']==""){
            if($input["municipality"]=="")
            {
                if($input['province']=="")
                {
                    $psgc_id = $input["region"];
                }
                else
                {
                    $psgc_id = $input["province"];
                }
            }
            else
            {
                $psgc_id = $input["municipality"];
            }
        }
        else
        {
            $psgc_id = $input['psgc_id'];
        }

        return $psgc_id;
    }
}