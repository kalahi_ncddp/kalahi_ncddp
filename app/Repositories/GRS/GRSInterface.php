<?php namespace Repositories\GRS;



interface GRSInterface {

    public function getBarangayList($id);
    public function getMunicipalityList($id);
    public function getRegionList();
    public function insertIntake($input,$sequence);
    public function getAllIntakes($username);
    public function getIntakeReferenceId($psgc_id,$cycle_id,$year,$sequence);
    public function updateIntake($input,$id);
	
	
}