<?php namespace Repositories\Reports;

class ReportsRepository implements  ReportsInterface {

	public function getTotalBA($cycle = 'NCDDP-1',$psgc_id = '')
	{

		$psgc_id = \Barangay::where('muni_psgc',$psgc_id)->get()->lists('barangay_psgc');
		// dd($psgc_id);
		return \BarangayAssembly::where('cycle_id',$cycle)->whereIn('psgc_id',$psgc_id)->count();
	}

	public function getTotalPSA($cycle,$psgc_id)
	{
		return \PSAnalysis::where('cycle_id',$cycle)->where('psgc_id',$psgc_id)->count();
	}
	public function getTotalVolunteer( $cycle , $psgc_id )
	{
		return  \PSAnalysis::where('cycle_id',$cycle)->where('psgc_id',$psgc_id)->count();
	}
	public function getTotalMIBF( $cycle , $psgc_id )
	{
		return \MunicipalForum::where('cycle_id',$cycle)->where('psgc_id',$psgc_id)->count();
	}
	public function getTotalTrainings($cycle,$psgc_id)
	{
		return \Trainings::where('cycle_id',$cycle)->where('psgc_id',$psgc_id)->count();
	}
	public function getTotalGRS($cycle,$psgc_id)
	{
		return \Intake::where('cycle_id',$cycle)->where('psgc_id',$psgc_id)->count();
	}




	/*
	 *  @REPORT_DETAIL: Barangay Assembly Household Participation Rate
	 *  @description: return the household participarate of municipality based on the region
	 *  
	 *  @returns Array object of Municipalities
	 */
	public function getHouseholdParticipationRate( $office_level,$program,$cycle,$mode )
	{
		//get username
		$username = \Session::get('username');
		
		// check the office level of the current login user 

		// ACT MEANS Municipal Level 
		// IT will only produce the current login of municipality
		// it will print all the barangay assemblies other that municipality
		if($office_level == 'ACT') {


			$psgc_id = \User::where('username',$username)->first()->psgc_id; // get the psgc_id of the login user which is the psgc_id of the municipality since it is ACT level

			return \Municipality::where('municipality_psgc',$psgc_id)
								->first();
		}
		else if($office_level == 'SRPMO') {
            $psgc_id = \User::where('username',$username)->first()->psgc_id; // get the psgc_id of the login user which is the psgc_id of the municipality since it is ACT level

            return \Provinces::where('province_psgc',$psgc_id)->municipality;
		}
		if($office_level == 'RPMO') {
			 // RETURN ALL MUNICIPALLITY UNDER REGION
			// MODEL OF REGION->PROVINCE->MUNICIPALITY
//            return \Region::where()
		}
	}


	public function getTotalAttendees( $office_level,$program,$cycle,$mode )
	{
		//get username
		$username = \Session::get('username');
		
		// check the office level of the current login user 

		// ACT MEANS Municipal Level 
		// IT will only produce the current login of municipality
		// it will print all the barangay assemblies other that municipality
		if($office_level == 'ACT') {


			$psgc_id = \User::where('username',$username)->first()->psgc_id; // get the psgc_id of the login user which is the psgc_id of the municipality since it is ACT level

			return \Municipality::where('municipality_psgc',$psgc_id)
								->first();
		}
		else if($office_level == 'SRPMO') {
            $psgc_id = \User::where('username',$username)->first()->psgc_id; // get the psgc_id of the login user which is the psgc_id of the municipality since it is ACT level

//            return \Provinces::where('p')
		}
		if($office_level == 'RPMO') {
			 // RETURN ALL MUNICIPALLITY UNDER REGION
			// MODEL OF REGION->PROVINCE->MUNICIPALITY

		}
	}
	

	public function getTotalVolunteerNumber( $office_level,$program,$cycle,$mode )
	{
		//get username
		$username = \Session::get('username');
		
		// check the office level of the current login user 

		// ACT MEANS Municipal Level 
		// IT will only produce the current login of municipality
		// it will print all the barangay assemblies other that municipality
		if($office_level == 'ACT') {


			$psgc_id = \User::where('username',$username)->first()->psgc_id; // get the psgc_id of the login user which is the psgc_id of the municipality since it is ACT level

			return \Municipality::where('municipality_psgc',$psgc_id)
								->first();
		}
		else if($office_level == 'SRPMO') {
            $psgc_id = \User::where('username',$username)->first()->psgc_id; // get the psgc_id of the login user which is the psgc_id of the municipality since it is ACT level

//            return \Provinces::where('p')
		}
		// if($office_level == 'RPMO') {
		// 	 // RETURN ALL MUNICIPALLITY UNDER REGION
		// 	// MODEL OF REGION->PROVINCE->MUNICIPALITY

		// }
	}



	


	
	
  


	public function getPSADataProfile($office_level, $psgc_id, $program,$cycle)
	{
		// dd($psgc_id);
		if($office_level == 'ACT') {
			return \DB::table('kc_psanalysis')
			->leftJoin('kc_barangay', 'kc_psanalysis.psgc_id', '=', 'kc_barangay.barangay_psgc')
			->leftJoin('kc_psaproject', 'kc_psanalysis.activity_id', '=', 'kc_psaproject.activity_id')
			->select(\DB::raw('barangay as location, no_atnmale, no_atnfemale, count(`problem`) as no_problem, count(`solution`) as no_solution'))
			->where('is_draft', '=', 0)
			->where('program_id', 'LIKE', $program)
			->where('cycle_id', 'LIKE', $cycle)
			->where('kc_barangay.muni_psgc', '=', $psgc_id)
			->groupBy('kc_psanalysis.psgc_id')
			->get();
		}
		else if( $office_level == 'RPMO'){
			return \DB::table('kc_psanalysis')
			->leftJoin('kc_barangay', 'kc_psanalysis.psgc_id', '=', 'kc_barangay.barangay_psgc')
			->leftJoin('kc_psaproject', 'kc_psanalysis.activity_id', '=', 'kc_psaproject.activity_id')
			->leftJoin('kc_municipality', 'kc_barangay.muni_psgc', '=', 'kc_municipality.municipality_psgc')
			->leftJoin('kc_province', 'kc_municipality.province_psgc', '=', 'kc_province.province_psgc')
			->select(\DB::raw('CONCAT_WS(",",municipality,province) as location, no_atnmale, no_atnfemale, count(`problem`) as no_problem, count(`solution`) as no_solution'))
			->where('is_draft', '=', 0)
			->where('program_id', 'LIKE', $program)
			->where('cycle_id', 'LIKE', $cycle)
			->where('kc_province.region_psgc', '=', $psgc_id)
			->groupBy('location')
			->get();
		}
		else{
			return \DB::table('kc_psanalysis')
			->leftJoin('kc_barangay', 'kc_psanalysis.psgc_id', '=', 'kc_barangay.barangay_psgc')
			->leftJoin('kc_psaproject', 'kc_psanalysis.activity_id', '=', 'kc_psaproject.activity_id')
			->leftJoin('kc_province', 'kc_barangay.prov_psgc', '=', 'kc_province.province_psgc')
			->leftJoin('kc_regions', 'kc_province.region_psgc', '=', 'kc_regions.psgc_id')
			->select(\DB::raw('region_name as location, no_atnmale, no_atnfemale, count(`problem`) as no_problem, count(`solution`) as no_solution'))
			->where('is_draft', '=', 0)
			->where('program_id', 'LIKE', $program)
			->where('cycle_id', 'LIKE', $cycle)
			->orderBy('kc_regions.psgc_id', 'asc')
			->groupBy('location')
			->get();
		}
	}
	
	public function getGrievances($office_level, $psgc_id, $program, $cycle, $resolution_status, $grievance_intake, $arr_type){
		if($office_level == 'ACT') {
			return \DB::table('kc_grsintake')
			->leftJoin('kc_barangay', 'kc_grsintake.barangay_psgc', '=', 'kc_barangay.barangay_psgc' )
			->leftJoin('kc_municipality', 'kc_barangay.muni_psgc', '=', 'kc_municipality.municipality_psgc' )
			->leftJoin('kc_province', 'kc_municipality.province_psgc', '=', 'kc_province.province_psgc' )
			->leftjoin('kc_grsupdates', function($join){
            	$join->on('kc_grsintake.reference_no', '=', 'kc_grsupdates.reference_no')
                 ->where('kc_grsupdates.date', '=', \DB::raw('SELECT MAX(date) FROM kc_grsupdates WHERE kc_grsintake.reference_no = kc_grsupdates.reference_no'));
       		 })
       		->select(\DB::raw('concern_nature, CONCAT_WS(", ",barangay, municipality, province) as location,  concern_category, DATE(date_intake) as date_intake, DATEDIFF(if(resolution_status = "Resolved", resolution_date, CURDATE()), date_intake) as no_days, narrative_summ, actions, CONCAT_WS("- ", DATE(date), narrative) as latest_update '))
			->where('cycle_id', 'LIKE', $cycle)
			->where('program_id', 'LIKE', 'KC_NCDDP')
			// ->where('resolution_status', '=', $resolution_status)
			// ->where('grievance_form', 'LIKE', $grievance_intake)
			->where('kc_barangay.muni_psgc', '=', $psgc_id)
			->whereIn('concern_nature', $arr_type)
       		->groupBy('location')
       		->get();
		}
	}




}

