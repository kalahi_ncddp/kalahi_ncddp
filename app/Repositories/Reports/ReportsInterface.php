<?php namespace Repositories\Reports;


interface ReportsInterface {
	public function getTotalBA($cycle,$psgc_id);
	public function getTotalPSA($cycle,$psgc_id);
	public function getTotalVolunteer($cycle,$psgc_id);
	public function getTotalMIBF($cycle,$psgc_id);
	public function getTotalTrainings($cycle,$psgc_id);
	
	public function getPSADataProfile($office_level, $psgc_id, $program,$cycle);
	public function getGrievances($office_level, $psgc_id, $program, $cycle, $resolution_status, $grievance_intake, $arr_type);
	public function getHouseholdParticipationRate($office_level,$program,$cycle,$mode);
   	public function getTotalAttendees($office_level,$program,$cycle,$mode);
   	public function getTotalVolunteerNumber( $office_level,$program,$cycle,$mode );
	
   // public function getHouseholdParticipationRateInBarangay($office_level,$program,$cycle,$mode);
}