<?php
App::bind('Repositories\GRS\GRSInterface', 'Repositories\GRS\GRSRepository');
App::bind('Repositories\Reference\ReferenceInterface','Repositories\Reference\ReferenceRepository');
App::bind('Repositories\Users\UsersInterface','Repositories\Users\UserRepository');

App::bind('Repositories\Reports\ReportsInterface','Repositories\Reports\ReportsRepository');
App::bind('Repositories\Sync\SyncInterface','Repositories\Sync\SyncRepository');
App::bind('Repositories\SPI\SPIInterface','Repositories\SPI\SPIRepository');
App::bind('Repositories\SystemUpdate\SystemUpdateInterface','Repositories\SystemUpdate\SystemUpdateRepository');


