<?php

class GrsMunicipalityControllerTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testIndexResponse()
	{
		$crawler = $this->client->request('GET', '/grs-municipality');

		$view = $this->client->getResponse()->original;
		
	 	$this->assertEquals('grsmunicipality.index', $view['grsmunicipality.index']);
	}


	public function testCreateResponse()
	{
		$crawler = $this->client->request('GET', '/grs-municipality/create');
		
		 $this->assertTrue($this->client->getResponse()->isOk());
	}

	public function testStoreResponse()
	{
		$crawler = $this->client->request('POST', '/grs-municipality/create');
		
		$this->assertTrue($this->client->getResponse()->isOk());
	}

	public function testShowResponse()
	{
		// $crawler = $this->client->request('GET', '/grs-municipality');
		
		// $this->assertTrue($this->client->getResponse()->isOk());
	}

	public function testEditResponse()
	{
		$crawler = $this->client->request('GET', '/grs-municipality/destroy');
		
		$this->assertTrue($this->client->getResponse()->isOk());
	}

	public function testGRSModel()
	{
		
	}
}
